module.exports = {
    important: false,
    purge: [

        './resources/**/*.blade.php',

        './resources/**/*.js',

        './resources/**/*.vue',

    ],
    darkMode: false, // or 'media' or  'class'
    theme: {
        extend: {
            colors: {
                transparent: 'transparent',
                current: 'currentColor',
                blueBT: {
                    DEFAULT: 'var(--main-blue--bk)',
                },
                greenBT: {
                    DEFAULT: 'var(--main-green--bk)',
                },
                grayBT: {
                    DEFAULT: 'var(--main-gray--bk)',
                },
                grayDarkBT: {
                    //light: '#ff7ce5',
                    DEFAULT: 'var(--main-graydark--bk)',
                    //dark: '#ff16d1',
                },
                whiteBT: {
                    DEFAULT: 'var(--main-white--bk)',
                },
                redBT: {
                    DEFAULT: 'var(--main-red--bk)',
                },
                redDark: {
                    DEFAULT: 'var(--main-red-dark)',
                },
                gris: {
                    tabla: '#f2f2f2'
                },
                naranja: '#e2492d'
            },
            zIndex: {
                '-1': '-1',
                '99999': '99999',
            },
            fontFamily: {
                'rob-light': ['roboto-light'],
                'rob-bold': ['roboto-bold'],
                'rob-bold-italic': ['roboto-bold-italic'],
                'rob-italic': ['roboto-italic'],
                'rob-medium': ['roboto-medium'],
                'rob-medium-italic': ['roboto-medium-italic'],
                'rob-regular': ['roboto-regular'],
                'mitr': ['Mitr'],
                'roboto-slab': ['Roboto Slab'],
                'source-sans-pro': ['Source Sans Pro']
            },
            screens: {
                'print': { 'raw': 'print' }, // => @media print { ... }
            },
            width: {
                'libros': 'calc(100% - 330px)',
                'menu': '258px'
            },
            height: {
                'custom-modal': 'calc(100% - 20rem)',
                'secciones': 'calc(100% - 5rem)',
                'tabla-interna': 'calc(100vh - 25.5rem)',
                'fit': 'fit-content',
                'interno': 'calc(100vh - 10.2rem)'
            },
            maxHeight: {
                'menu': 'calc(100vh - 162px)',
                'secciones': 'calc(100% - 5rem)',
                'custom-modal': 'calc(100% - 20rem)',
                'paqueteria': 'calc(100vh - 16rem)',
            },
            minHeight: {
                'secciones': 'calc(100% - 5rem)',
                'full-print': 'calc(100vh - 3rem)',
                'interno': 'calc(100vh - 10.2rem)'
            }
        },
        screens: {
            'sm': '640px',
            // => @media (min-width: 640px) { ... }

            'md': '768px',
            // => @media (min-width: 768px) { ... }

            'lg': '1024px',
            // => @media (min-width: 1024px) { ... }

            'xl': '1280px',
            // => @media (min-width: 1280px) { ... }

            '2xl': '1536px',
            // => @media (min-width: 1536px) { ... }

            '3xl': '1800px',
            // => @media (min-width: 1536px) { ... }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require("@tailwindcss/forms")({
            strategy: 'class',
        }),
    ],
}
