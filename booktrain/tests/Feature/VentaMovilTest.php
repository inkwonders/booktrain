<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VentaMovilTest extends TestCase
{
    /**
     * Genera un pedido con libros asociados a un paquete de un colegio asignado a un usuario de venta movil
     */
    public function test_generar_pedido()
    {
        $this->assertTrue(false);
    }

    /**
     * Intenta generar un pedido con un método de pago incorrecto (ONLINE)
     */
    public function test_fail_metodo_pago_incorrecto() {
        $this->assertTrue(false);
    }
}
