<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegistroUsuarioTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use WithFaker;



    //todo ok
    public function test_valida_registro()
    {

        $nombre=$this->faker->name;
        $apellidos=$this->faker->firstName;
        $correo=$this->faker->email;
        $pass=$this->faker->password;

        $response = $this->post('/registro', [
        'name' => $nombre,
        'apellidos' => $apellidos,
        'email' => $correo,
        'email_confirmation' => $correo,
        'password' => $pass, // password
        'password_confirmation'=>$pass,

        ]);

        if($response->status() != 200) {
            $response->dump();
        }

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'success'=>true,
            'message'=>'Se ha registrado correctamente'

        ]);

    }//vacio




    public function test_valida_nombre()
    {


        $response = $this->post('/registro', [
        'name' => '',
        'apellidos' => 'FRuentes',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@gmail.com',
        'password' => '123445678', // password
        'password_confirmation'=>'123445678',

        ]);

        $response->assertStatus(422);
        $response->assertExactJson([
            'success'=>false,
            'message'=>'Su nombre es requerido'
        ]);

    }//vacio



    public function test_valida_nombre_caracteres()
    {


        $response = $this->post('/registro', [
            'name' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrs standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
            'apellidos' => 'FRuentes',
            'email' => 'email@gmail.com',
            'email_confirmation' => 'email@gmail.com',
            'password' => '123445678', // password
            'password_confirmation'=>'123445678',

        ]);

        $response->assertStatus(422);
        $response->assertJsonFragment([

            'success'=>false

        ]);

    }//caracteres


    public function test_valida_apellidos()
    {


        $response = $this->post('/registro', [
            'name' => 'Robert',
            'apellidos' => '',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@gmail.com',
        'password' => '123445678', // password
        'password_confirmation'=>'123445678',
        ]);

      $response->assertStatus(422);
        $response->assertExactJson([
            'success'=>false,
            'message'=>'Sus apellidos son requeridos'
        ]);

    }//vacio



    public function test_valida_apellidos_caracteres()
    {


        $response = $this->post('/registro', [
            'name' => 'Robert',
            'apellidos' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrs standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrs standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen bookLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrs standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
            'email' => 'email@gmail.com',
            'email_confirmation' => 'email@gmail.com',
            'password' => '123445678', // password
            'password_confirmation'=>'123445678',

        ]);

        $response->assertStatus(422);
        $response->assertJsonFragment([

            'success'=>false

        ]);

    }//caracteres

    public function test_valida_correo()
    {


        $response = $this->post('/registro', [
        'name' => 'Robert',
        'apellidos' => 'Fuentes',
        'email' => 'email-gmail.com',
        'email_confirmation' => 'email-gmail.com',
        'password' => '123445678', // password
        'password_confirmation'=>'123445678',
        ]);

      $response->assertStatus(422);
      $response->assertJsonFragment([
            'success'=>false,

        ]);

    }//vacio

    public function test_valida_correo_confirmacion()
    {


        $response = $this->post('/registro', [
        'name' => 'Robert',
        'apellidos' => 'Fuentes',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@hotmail.com',
        'password' => '123445678', // password
        'password_confirmation'=>'123445678',
        ]);

      $response->assertStatus(422);
      $response->assertJsonFragment([
            'success'=>false,

        ]);

    }//confiormacion correo

    public function test_valida_password()
    {


        $response = $this->post('/registro', [
        'name' => 'Robert',
        'apellidos' => 'Fuentes',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@hotmail.com',
        'password' => '', // password
        'password_confirmation'=>'123445678',
        ]);

      $response->assertStatus(422);
      $response->assertJsonFragment([
            'success'=>false,

        ]);

    }//confiormacion correo


    public function test_valida_password_caracteres()
    {


        $response = $this->post('/registro', [
        'name' => 'Robert',
        'apellidos' => 'Fuentes',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@hotmail.com',
        'password' => '1123', // password
        'password_confirmation'=>'123445678',
        ]);

      $response->assertStatus(422);
      $response->assertJsonFragment([
            'success'=>false,

        ]);

    }//confiormacion correo


    public function test_valida_password_confirmacion()
    {


        $response = $this->post('/registro', [
        'name' => 'Robert',
        'apellidos' => 'Fuentes',
        'email' => 'email@gmail.com',
        'email_confirmation' => 'email@hotmail.com',
        'password' => '12345678', // password
        'password_confirmation'=>'123445678999',
        ]);

      $response->assertStatus(422);
      $response->assertJsonFragment([
            'success'=>false,

        ]);

    }//confiormacion correo



}// fin test
