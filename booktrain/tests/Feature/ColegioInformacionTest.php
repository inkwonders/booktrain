<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ColegioInformacionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_informacion_colegio()
    {

        //autenticamos el usuario
        $user = User::find(1);
        Auth::attempt(['email' => $user->email, 'password' => 'inkwonders']);

        //DD-2222

        $codigo_colegio='DD-2222';

        $response = $this->post('/api_web/colegio/informacion', [
            //'_token'=>'AhBlGs8EaQqIvJ4LxaISDFp4DU5tZ8RfDKznFUIO',
            'codigo'   => $codigo_colegio,
        ]);


            //  dd($response);

            $response->assertStatus(200);

            $response->assertJsonStructure([
                'success',
                'data'=>[
                    'id',
                    'codigo',
                    'nombre',

                    'configuracion'=>[

                        'nombre_cuenta',
                        'nombre_banco',
                        'numero_cuenta',
                        'clabe',
                        'ventanilla_otros_bancos',
                        'portal_bbva',
                        'portal_otros_bancos',
                        'numero_contrato',
                        'numero_contrato_clabe',
                        'costo_envio',

                        "metodos_pago" =>[

                            '*'=>[
                            'nombre',
                            'valor',
                            'formas_pago'=>[

                            '*'=>[

                            'nombre',
                            'valor',
                            'meses',
                            'comision',
                            'minimo'

                                ]

                            ]


                            ]

                        ]//metodos pago

                        ],//configuracion

                    'logo',

                    'direcciones_entrega'=>
                        [
                            '*'=>[
                            'id',
                            'direccion_completa',
                            'calle',
                            'no_interior',
                            'no_exterior',
                            'colonia',
                            'cp',
                            'estado_id',
                            'municipio_id',
                            'referencia',

                            'estado'=>[
                                'id',
                                'nombre',
                                'nombre_corto',
                            ],

                             'municipio'=>[
                                'id',
                                'nombre',
                                ]

                            ]

                             ],//direcciones entrega

                             'direcciones_pickup'=>
                        [
                            '*'=>[
                            'id',
                            'direccion_completa',
                            'calle',
                            'no_interior',
                            'no_exterior',
                            'colonia',
                            'cp',
                            'estado_id',
                            'municipio_id',
                            'referencia',

                            'estado'=>[
                                'id',
                                'nombre',
                                'nombre_corto',
                            ],

                             'municipio'=>[
                                'id',
                                'nombre',
                                ]

                            ]

                             ],//direcciones_pickup

                           'secciones'=>[

                            '*'=>[

                              'id',
                              'nombre',
                              'niveles'=>[

                                '*'=>[

                                    'id',
                                    'nombre',
                                    'paquetes'=>[

                            '*'=>[

                                    'id',
                                    'nombre',
                                    'activo',
                                    'nivel_id',
                                    'libros'=>[

                                        '*'=>[


                                            'id',
                                            'nombre',
                                            'editorial',
                                            'edicion',
                                            'isbn',
                                            'stock',
                                            'bajo_pedido',
                                            'activo',
                                            'obligatorio',
                                            'precio',


                                        ]

                                    ]//librso

                            ]

                                    ]//paquetes

                                ]


                              ]//niveles


                            ]


                           ]//secciones
                    ],//data




            ]);

    }//Valida info dle colegio
}
