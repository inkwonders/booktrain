<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CajaVentaMovilTest extends TestCase
{
    private $token = '';
    /**
     * Inicia sesión y obtiene un token
     */
    public function test_iniciar_sesion()
    {
        $password = 'mysecretpassword';
        $user = User::factory()->create(['password' => Hash::make($password)]);

        $request = $this->postJson( route('api.login'), [
            'email' => $user->email,
            'password' => $password
        ]);

        $request->assertStatus(200)
                ->assertJson([
                    'success' => true,
                    'data' => [
                        'id' => $user->id
                    ]
                ]);

        $user->delete();
    }

    public function test_obtener_informacion_usuario() {
        $this->assertTrue(false);
    }

    public function test_obtener_informacion_caja() {
        $this->assertTrue(false);
    }

    public function test_obtener_configuracion_caja() {
        $this->assertTrue(false);
    }

    public function test_abrir_caja() {
        $this->assertTrue(false);
    }

    public function test_cerrar_caja() {
        $this->assertTrue(false);
    }

    public function test_cerrar_sesion() {
        $password = 'mysecretpassword';
        $user = User::factory()->create(['password' => Hash::make($password)]);

        $request = $this->postJson( route('api.login'), [
            'email' => $user->email,
            'password' => $password
        ]);

        $request->assertStatus(200)
                ->assertJson([
                    'success' => true,
                    'data' => [
                        'id' => $user->id
                    ]
                ]);

        $token = $request['data']['token'];

        $request = $this
            ->withHeaders([
                'Authorization' => "bearer {$token}",
                'Accept' => 'application/json'
            ])
            ->get( route('api.logout') );

        $user->delete();

        $request->assertStatus(200);
    }

    public function test_fail_iniciar_sesion_credenciales_invalidas() {
        $request = $this->postJson( route('api.login'), [
            'email' => 'fake@email.com',
            'password' => 'aRandomPassword'
        ]);

        $request->assertStatus(401)
                ->assertJson([
                    'success' => false
                ]);
    }
}
