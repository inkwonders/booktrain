<?php

namespace Tests\Feature;

use App\Models\TicketUsuario;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\User;


use Tests\TestCase;

class TicketUsuarioTest extends TestCase
{

     use WithFaker;


            //todo ok
    public function test_valida_registro(){


        //autenticamos el usuario
        $user = User::find(5);
        Auth::attempt(['email' => $user->email, 'password' => 'inkwonders']);


        $titulo=$this->faker->paragraph(3);
        $motivo=$this->faker->numberBetween(1,9);
        $descripcion=$this->faker->text;
        $telefono=$this->faker->numberBetween(1000000000, 9999999999);
        $imagen=$this->faker->imageUrl(640,480);

        //  \Storage::fake('tickes');

        Storage::fake('tickes');


         $file = UploadedFile::fake()->image('avatar.jpg');

        // $file=$this->faker->image(storage_path('app\public\images\tickets_files\avatar.jpg'), 300, 300)->size(100);

        //  dd($file);

        $response = $this
        // ->withHeaders([
        //     'Content-Type' => 'application/x-www-form-urlencoded',
        //     'Authorization'=>'Bearer OpZq2VlLbeCBcyDc3b2IOdLziIygAnRXbsHYgmxg',
        //     'Cookie'=>'XSRF-TOKEN=eyJpdiI6Ijk4NFZvUTByQ2RtS1VvQlcvZEt3Q2c9PSIsInZhbHVlIjoiQ3V6MUtYRURPZFhvNHpkT3ZBM2xMak9MWGc3dzBnVGl0NXczTm55dHFwWlJWS2ZIL1lvMTZIODlZWnp1b05mdUhhZ3BLdzVuL3dwUUxhV0xUcU9BY0FnT2xBMFVUTnVIc0FCRFhSSFFaenVvanRod050dFlaeDhzcllBNVpHVDQiLCJtYWMiOiIyOTFmMzgwYWRlOWM1OWU1N2NiZGJiNmZkOTllOWM1NzQxNmUzY2Q5ZGFiMjNkZDE5MWYzZjNjNmI5MzhlN2Q4In0%3D; laravel_session=eyJpdiI6IjRmODN5ZHJUd2xzVVIvTWJwZUxZanc9PSIsInZhbHVlIjoiREZSdmkxNWUzMXF6QlVVdDFHUER5aS9FcG1na0xtWWNiMVA1WnViRzZibzEyVnB0bDF3aUV0bzYyNGYzOGhyVkZUSGJteHkwODE2ZWY3cENoVnB2TXgrblNLKy9paE5iaFpwVTkzOXh5WktXQzNxVC8rWWFsOTE2RFNjMjZXa3YiLCJtYWMiOiJiZmIwOTBiNWJhYTkxZGY5NjhhNzYyOTgwYzEzYmQ4NGY3NmI0YmI4NjA1Mzk5YmM1NjZkMDEzZmY5YWI3NzJkIn0%3D',
        // ])
        ->post('/caso', [
            // '_token'=>'dGx3Mi8haBATkwRE7F8Ed6oDGgBECi8rdKcsXxDO',
            'titulo'   => $titulo,
            'motivo'      =>$motivo,
            'descripcion'  => $descripcion,
            'telefono'  => $telefono,
            'filenames.*'=> $file,

        ]);



        // \Storage::disk('tickes')->assertExists('avatar.jpg');



        //  dd($response);

        $ticket=TicketUsuario::all()->last();

        if($ticket->titulo==$titulo){    $response->assertStatus(302); }else{  $response->assertStatus(200); }


            //nos traemos el ultimo ticket y comparamos cn lo sdatos insertados


    }//vacio


    //falla
    public function test_valida_registro_fail(){


        //autenticamos el usuario
        $user = User::find(1);
        Auth::attempt(['email' => $user->email, 'password' => 'inkwonders']);


        $titulo=$this->faker->paragraph(2);
        $motivo=$this->faker->numberBetween(1,9);
        $descripcion=$this->faker->text;
        $telefono='';


        $response = $this->post('/caso', [
            '_token'=>'P91KYjGwuPsJ96XAD2Rs06KUh07rsqlJ9mHIj7ha',
            'titulo'   => $titulo,
            'motivo'      =>$motivo,
            'descripcion'  => $descripcion,
            'telefono'  => $telefono,


        ]);

        //dd($response);

        $ticket=TicketUsuario::all()->last();


        if(!$ticket->titulo!=$titulo){   $response->assertStatus(302); }else{   $response->assertStatus(200); }


            //nos traemos el ultimo ticket y comparamos cn lo sdatos insertados


    }//vacio



}
