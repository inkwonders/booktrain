<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class CarritoTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     //envia datos correctos al carrito
    public function test_agrega_carrito()
    {
         //autenticamos el usuario
         $user = User::find(5);
         Auth::attempt(['email' => $user->email, 'password' => 'inkwonders']);

         $response = $this->post('/api_web/pedido/carrito', [
            '_token'=>'AhBlGs8EaQqIvJ4LxaISDFp4DU5tZ8RfDKznFUIO',
            'colegio_id'   => 2,
            'paquete_id'=>4,
            'libros'=>[
                [
                'id'=>18,
                 'precio'=>5
                ]
            ]
        ]);

        //  dd($response);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'success'=>true,

        ]);
    }


    public function test_agrega_carrito_otro_colegio()
    {
         //autenticamos el usuario
         $user = User::find(5);
         Auth::attempt(['email' => $user->email, 'password' => 'inkwonders']);

         $response = $this->post('/api_web/pedido/carrito', [
            '_token'=>'AhBlGs8EaQqIvJ4LxaISDFp4DU5tZ8RfDKznFUIO',
            'colegio_id'   => 1, // cambiamos el colegio despues q la prueba anterior agrega al carrito
            'paquete_id'=>4,
            'libros'=>[
                [
                'id'=>18,
                 'precio'=>5
                ]
            ]
        ]);

        //  dd($response);
        $response->assertStatus(403);
        $response->assertJsonFragment([
            'success'=>false,

        ]);
    }
}
