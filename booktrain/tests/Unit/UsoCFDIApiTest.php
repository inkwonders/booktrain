<?php
namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UsoCFDI;

class UsoCFDIApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    public function test_usos_cfdi()
    {
        $response = $this->json('get', '/api_web/usos_cfdi', []);
        $response->assertStatus(200);
    }

}
