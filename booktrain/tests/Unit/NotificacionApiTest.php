<?php namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Notificacion;
use App\Models\Colegio;
use Illuminate\Foundation\Testing\WithFaker;

class NotificacionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    /**
     * @test
     */
    // public function test_create_notificacion()
    // {
    //     $notificacion = Notificacion::factory()->make()->toArray();

    //     $this->response = $this->json(
    //         'POST',
    //         '/api/notificacions', $notificacion
    //     );

    //     $this->assertApiResponse($notificacion);
    // }

    /**
     * @test
     */

    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // public function test_read_notificacion()
    // {
    //     // $notificacion = Notificacion::factory()->create();
    //     $colegio = Colegio::inRandomOrder()->first();

    //     $notificacion = Notificacion::where('colegio_id', $colegio->id)->where('activo', 1)->first();

    //     $response = $this->response = $this->json(
    //         'GET',
    //         '/api_web/notificaciones/notificaciones/'.$notificacion->id

    //     );
    //     $response->assertStatus(200);

    //     // $this->assertApiResponse($notificacion->toArray());
    // }

    /**
     * @test
     */
    // public function test_update_notificacion()
    // {
    //     $notificacion = Notificacion::factory()->create();
    //     $editedNotificacion = Notificacion::factory()->make()->toArray();

    //     $this->response = $this->json(
    //         'PUT',
    //         '/api/notificacions/'.$notificacion->id,
    //         $editedNotificacion
    //     );

    //     $this->assertApiResponse($editedNotificacion);
    // }

    /**
     * @test
     */
    // public function test_delete_notificacion()
    // {
    //     $notificacion = Notificacion::factory()->create();

    //     $this->response = $this->json(
    //         'DELETE',
    //          '/api/notificacions/'.$notificacion->id
    //      );

    //     $this->assertApiSuccess();
    //     $this->response = $this->json(
    //         'GET',
    //         '/api/notificacions/'.$notificacion->id
    //     );

    //     $this->response->assertStatus(404);
    // }
}
