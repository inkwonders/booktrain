<?php
namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Estado;

class EstadosApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    public function test_estados()
    {
        $response = $this->json('get', '/api_web/estados', []);
        $response->assertStatus(200);
    }

    public function test_municipios()
    {
        $estado = Estado::inRandomOrder()->first();
        $response = $this->json('get', '/api_web/estados/'.$estado->id.'/municipios', []);
        $response->assertStatus(200);
    }

}
