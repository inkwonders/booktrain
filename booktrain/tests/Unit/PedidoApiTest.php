<?php namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Pedido;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\User;
use Auth;

class PedidoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    /**
     * @test
     */
    // public function test_create_pedido()
    // {

    //     $user = User::inRandomOrder()->first();
    //     $token = Auth::fromUser($user);
    //     $pedido = Pedido::inRandomOrder()->first();
    //     $response = $this->assertJson('POST','/api_web/pedidos/carrito', [
    //         'colegio_id'   => $this->faker->text,
    //         'status'  => $this->faker->numberBetween(1,2),
    //         'usuario_id'
    //     ]);
    //     $resumenPedido = new ResumenPedido($arrayDatosResumenPedido);
    //     "pedido_id" => $pedido_id,
    //     "paquete_id" => $request->paquete_id,

    //     $detalleResumenPedido = new DetalleResumenPedido($arrayDatosDetalleResumenPedido);

    //     $response->assertStatus(200);

    //     $this->assertApiResponse($pedido);
    // }

    /**
     * @test
     */
    public function test_read_pedido()
    {
        // $pedido = Pedido::inRandomOrder()->first();
        $user = User::inRandomOrder()
            ->whereBetween('id', [1, 6])
            ->has('pedidos')
            ->first();

        $pedido = Pedido::where('usuario_id', $user->id)->first();


        $response = $this->actingAs($user)
        ->json('get', '/api_web/pedido/carrito/'.$pedido->id, []);

      $response->assertStatus(200);



    }

    /**
     * @test
     */
    // public function test_update_pedido()
    // {
    //     $pedido = Pedido::factory()->create();
    //     $editedPedido = Pedido::factory()->make()->toArray();

    //     $this->response = $this->json(
    //         'PUT',
    //         '/api/pedidos/'.$pedido->id,
    //         $editedPedido
    //     );

    //     $this->assertApiResponse($editedPedido);
    // }

    // /**
    //  * @test
    //  */
    // public function test_delete_pedido()
    // {
    //     $pedido = Pedido::factory()->create();

    //     $this->response = $this->json(
    //         'DELETE',
    //          '/api/pedidos/'.$pedido->id
    //      );

    //     $this->assertApiSuccess();
    //     $this->response = $this->json(
    //         'GET',
    //         '/api/pedidos/'.$pedido->id
    //     );

    //     $this->response->assertStatus(404);
    // }
}
