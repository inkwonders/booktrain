<?php namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Tests\ApiTestTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsuarioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    public function test_mi_informacion()
    {
        $user = User::inRandomOrder()->first();
        $response = $this->actingAs($user)
            ->json('get', '/api_web/mi-informacion-2', []);
        $response->assertStatus(200);
    }

    // public function test_mi_informacion_vacio_sin_autentificacion()
    // {
    //     $response = $this->json('get', '/api_web/mi-informacion-2', []);
    //     $response->assertStatus(500);
    // }

    public function test_registro_usuario()
    {
        $response = $this->followingRedirects()
                ->json('post', '/registro', [
                    'name' => $this->faker->name,
                    'apellidos' => $this->faker->lastName,
                    'email' => 'mail@mail.com',
                    'email_confirmation' => 'mail@mail.com',
                    'password' => '12345678',
                    'password_confirmation' => '12345678',
                ]);

        $response->assertStatus(200);
    }

    public function test_login_usuario()
    {
        $user = User::inRandomOrder()->first();
        $hash_temp = $user->password;
        $password = $this->faker->password;
        $user->password = Hash::make( $password );
        $user->save();

        $response = $this->followingRedirects()
                ->json('post', '/login', [
                    'email' => $user->email,
                    'password' => $password,
                ]);

        $user->password = $hash_temp;
        $user->save();

        $response->assertStatus(200);
    }

    // public function test_usuario_datos_ultimo_pedido()
    // {
    //     $user = User::inRandomOrder()->first();

    //     $response = $this->json('get', '/api_web/usuario/'.$user->id.'/datos-ultimo-pedido', []);
    //     $response->assertStatus(200);
    // }
}
