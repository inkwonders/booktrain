<?php

namespace Tests\Unit;

use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\User;
use App\Models\TicketUsuario;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TicketsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions, WithFaker;

    /**
     *  Route::get('/opciones', 'API\TicketsAPIController@getListaOpciones');
     *  Route::get('/{id}', 'API\TicketUsuarioAPIController@show');
     *  Route::get('/{id}/entradas', 'API\TicketUsuarioAPIController@entradasTicket');
     *  Route::post('/agregar_entrada', 'API\TicketsAPIController@agregar_entrada');
     *  Route::post('/cierra_ticket/{id}', 'API\TicketUsuarioAPIController@postCierraTicket');
     *  Route::post('/califica_ticket/{id}', 'API\TicketUsuarioAPIController@postCierraTicket');
     */
    public function test_opciones() // listo pero falta validar si no regresa nada o si truena el transformer
    {
        $response = $this->json('get', '/api_web/tickets/opciones', []);
        $response->assertStatus(200);
    }

    public function test_ticket() // listo
    {
        $ticket = TicketUsuario::inRandomOrder()->first();
        $response = $this->json('get', '/api_web/tickets/'.$ticket->id, []);
        $response->assertStatus(200);
    }

    public function test_entradas_ticket() // listo pero falta validar si no regresa nada, si no tiene entradas
    {
        $ticket = TicketUsuario::inRandomOrder()->first();
        $response = $this->json('get', '/api_web/tickets/'.$ticket->id.'/entradas', []);
        $response->assertStatus(200);
    }

    /**
     * listo pero falta regresar mensaje de error si algo truena y usar transactions
     * el status de error para validar campos marca 200 , deberia ser otro, no?
     *
     */


    // public function test_agregar_entrada()
    // {
    //     $ticket = TicketUsuario::inRandomOrder()->first();
    //     $user = User::inRandomOrder()->first();
    //     $response = $this->actingAs($user)
    //         ->json('post', '/api_web/tickets/agregar_entrada', [
    //             'descripcion'   => $this->faker->text,
    //             'caso'          => $ticket->id
    //         ]);
    //     $response->assertStatus(200);
    // }

    public function test_cerrar_ticket() // listo
    {
        $ticket = TicketUsuario::inRandomOrder()->first();
        $response = $this->json('post', '/api_web/tickets/cierra_ticket/'.$ticket->id, []);
        $response->assertStatus(200);
    }

    public function test_calificar_ticket() // listo
    {
        $ticket = TicketUsuario::inRandomOrder()->first();
        $response = $this->json('post', '/api_web/tickets/califica_ticket/'.$ticket->id, [
            'comentarios'   => $this->faker->text,
            'calificacion'  => $this->faker->numberBetween(1,2),
        ]);
        $response->assertStatus(200);
    }


}
