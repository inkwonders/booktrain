<?php namespace Tests\Repositories;

use App\Models\NivelColegio;
use App\Repositories\NivelColegioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NivelColegioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NivelColegioRepository
     */
    protected $nivelColegioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->nivelColegioRepo = \App::make(NivelColegioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->make()->toArray();

        $createdNivelColegio = $this->nivelColegioRepo->create($nivelColegio);

        $createdNivelColegio = $createdNivelColegio->toArray();
        $this->assertArrayHasKey('id', $createdNivelColegio);
        $this->assertNotNull($createdNivelColegio['id'], 'Created NivelColegio must have id specified');
        $this->assertNotNull(NivelColegio::find($createdNivelColegio['id']), 'NivelColegio with given id must be in DB');
        $this->assertModelData($nivelColegio, $createdNivelColegio);
    }

    /**
     * @test read
     */
    public function test_read_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();

        $dbNivelColegio = $this->nivelColegioRepo->find($nivelColegio->id);

        $dbNivelColegio = $dbNivelColegio->toArray();
        $this->assertModelData($nivelColegio->toArray(), $dbNivelColegio);
    }

    /**
     * @test update
     */
    public function test_update_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();
        $fakeNivelColegio = NivelColegio::factory()->make()->toArray();

        $updatedNivelColegio = $this->nivelColegioRepo->update($fakeNivelColegio, $nivelColegio->id);

        $this->assertModelData($fakeNivelColegio, $updatedNivelColegio->toArray());
        $dbNivelColegio = $this->nivelColegioRepo->find($nivelColegio->id);
        $this->assertModelData($fakeNivelColegio, $dbNivelColegio->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();

        $resp = $this->nivelColegioRepo->delete($nivelColegio->id);

        $this->assertTrue($resp);
        $this->assertNull(NivelColegio::find($nivelColegio->id), 'NivelColegio should not exist in DB');
    }
}
