<?php namespace Tests\Repositories;

use App\Models\TicketEntradaArchivo;
use App\Repositories\TicketEntradaArchivoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TicketEntradaArchivoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TicketEntradaArchivoRepository
     */
    protected $ticketEntradaArchivoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ticketEntradaArchivoRepo = \App::make(TicketEntradaArchivoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->make()->toArray();

        $createdTicketEntradaArchivo = $this->ticketEntradaArchivoRepo->create($ticketEntradaArchivo);

        $createdTicketEntradaArchivo = $createdTicketEntradaArchivo->toArray();
        $this->assertArrayHasKey('id', $createdTicketEntradaArchivo);
        $this->assertNotNull($createdTicketEntradaArchivo['id'], 'Created TicketEntradaArchivo must have id specified');
        $this->assertNotNull(TicketEntradaArchivo::find($createdTicketEntradaArchivo['id']), 'TicketEntradaArchivo with given id must be in DB');
        $this->assertModelData($ticketEntradaArchivo, $createdTicketEntradaArchivo);
    }

    /**
     * @test read
     */
    public function test_read_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();

        $dbTicketEntradaArchivo = $this->ticketEntradaArchivoRepo->find($ticketEntradaArchivo->id);

        $dbTicketEntradaArchivo = $dbTicketEntradaArchivo->toArray();
        $this->assertModelData($ticketEntradaArchivo->toArray(), $dbTicketEntradaArchivo);
    }

    /**
     * @test update
     */
    public function test_update_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();
        $fakeTicketEntradaArchivo = TicketEntradaArchivo::factory()->make()->toArray();

        $updatedTicketEntradaArchivo = $this->ticketEntradaArchivoRepo->update($fakeTicketEntradaArchivo, $ticketEntradaArchivo->id);

        $this->assertModelData($fakeTicketEntradaArchivo, $updatedTicketEntradaArchivo->toArray());
        $dbTicketEntradaArchivo = $this->ticketEntradaArchivoRepo->find($ticketEntradaArchivo->id);
        $this->assertModelData($fakeTicketEntradaArchivo, $dbTicketEntradaArchivo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();

        $resp = $this->ticketEntradaArchivoRepo->delete($ticketEntradaArchivo->id);

        $this->assertTrue($resp);
        $this->assertNull(TicketEntradaArchivo::find($ticketEntradaArchivo->id), 'TicketEntradaArchivo should not exist in DB');
    }
}
