<?php namespace Tests\Repositories;

use App\Models\FormaPago;
use App\Repositories\FormaPagoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FormaPagoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FormaPagoRepository
     */
    protected $formaPagoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->formaPagoRepo = \App::make(FormaPagoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_forma_pago()
    {
        $formaPago = FormaPago::factory()->make()->toArray();

        $createdFormaPago = $this->formaPagoRepo->create($formaPago);

        $createdFormaPago = $createdFormaPago->toArray();
        $this->assertArrayHasKey('id', $createdFormaPago);
        $this->assertNotNull($createdFormaPago['id'], 'Created FormaPago must have id specified');
        $this->assertNotNull(FormaPago::find($createdFormaPago['id']), 'FormaPago with given id must be in DB');
        $this->assertModelData($formaPago, $createdFormaPago);
    }

    /**
     * @test read
     */
    public function test_read_forma_pago()
    {
        $formaPago = FormaPago::factory()->create();

        $dbFormaPago = $this->formaPagoRepo->find($formaPago->id);

        $dbFormaPago = $dbFormaPago->toArray();
        $this->assertModelData($formaPago->toArray(), $dbFormaPago);
    }

    /**
     * @test update
     */
    public function test_update_forma_pago()
    {
        $formaPago = FormaPago::factory()->create();
        $fakeFormaPago = FormaPago::factory()->make()->toArray();

        $updatedFormaPago = $this->formaPagoRepo->update($fakeFormaPago, $formaPago->id);

        $this->assertModelData($fakeFormaPago, $updatedFormaPago->toArray());
        $dbFormaPago = $this->formaPagoRepo->find($formaPago->id);
        $this->assertModelData($fakeFormaPago, $dbFormaPago->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_forma_pago()
    {
        $formaPago = FormaPago::factory()->create();

        $resp = $this->formaPagoRepo->delete($formaPago->id);

        $this->assertTrue($resp);
        $this->assertNull(FormaPago::find($formaPago->id), 'FormaPago should not exist in DB');
    }
}
