<?php namespace Tests\Repositories;

use App\Models\Notificacion;
use App\Repositories\NotificacionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NotificacionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NotificacionRepository
     */
    protected $notificacionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->notificacionRepo = \App::make(NotificacionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_notificacion()
    {
        $notificacion = Notificacion::factory()->make()->toArray();

        $createdNotificacion = $this->notificacionRepo->create($notificacion);

        $createdNotificacion = $createdNotificacion->toArray();
        $this->assertArrayHasKey('id', $createdNotificacion);
        $this->assertNotNull($createdNotificacion['id'], 'Created Notificacion must have id specified');
        $this->assertNotNull(Notificacion::find($createdNotificacion['id']), 'Notificacion with given id must be in DB');
        $this->assertModelData($notificacion, $createdNotificacion);
    }

    /**
     * @test read
     */
    public function test_read_notificacion()
    {
        $notificacion = Notificacion::factory()->create();

        $dbNotificacion = $this->notificacionRepo->find($notificacion->id);

        $dbNotificacion = $dbNotificacion->toArray();
        $this->assertModelData($notificacion->toArray(), $dbNotificacion);
    }

    /**
     * @test update
     */
    public function test_update_notificacion()
    {
        $notificacion = Notificacion::factory()->create();
        $fakeNotificacion = Notificacion::factory()->make()->toArray();

        $updatedNotificacion = $this->notificacionRepo->update($fakeNotificacion, $notificacion->id);

        $this->assertModelData($fakeNotificacion, $updatedNotificacion->toArray());
        $dbNotificacion = $this->notificacionRepo->find($notificacion->id);
        $this->assertModelData($fakeNotificacion, $dbNotificacion->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_notificacion()
    {
        $notificacion = Notificacion::factory()->create();

        $resp = $this->notificacionRepo->delete($notificacion->id);

        $this->assertTrue($resp);
        $this->assertNull(Notificacion::find($notificacion->id), 'Notificacion should not exist in DB');
    }
}
