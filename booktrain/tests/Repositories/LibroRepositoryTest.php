<?php namespace Tests\Repositories;

use App\Models\Libro;
use App\Repositories\LibroRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LibroRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LibroRepository
     */
    protected $libroRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->libroRepo = \App::make(LibroRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_libro()
    {
        $libro = Libro::factory()->make()->toArray();

        $createdLibro = $this->libroRepo->create($libro);

        $createdLibro = $createdLibro->toArray();
        $this->assertArrayHasKey('id', $createdLibro);
        $this->assertNotNull($createdLibro['id'], 'Created Libro must have id specified');
        $this->assertNotNull(Libro::find($createdLibro['id']), 'Libro with given id must be in DB');
        $this->assertModelData($libro, $createdLibro);
    }

    /**
     * @test read
     */
    public function test_read_libro()
    {
        $libro = Libro::factory()->create();

        $dbLibro = $this->libroRepo->find($libro->id);

        $dbLibro = $dbLibro->toArray();
        $this->assertModelData($libro->toArray(), $dbLibro);
    }

    /**
     * @test update
     */
    public function test_update_libro()
    {
        $libro = Libro::factory()->create();
        $fakeLibro = Libro::factory()->make()->toArray();

        $updatedLibro = $this->libroRepo->update($fakeLibro, $libro->id);

        $this->assertModelData($fakeLibro, $updatedLibro->toArray());
        $dbLibro = $this->libroRepo->find($libro->id);
        $this->assertModelData($fakeLibro, $dbLibro->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_libro()
    {
        $libro = Libro::factory()->create();

        $resp = $this->libroRepo->delete($libro->id);

        $this->assertTrue($resp);
        $this->assertNull(Libro::find($libro->id), 'Libro should not exist in DB');
    }
}
