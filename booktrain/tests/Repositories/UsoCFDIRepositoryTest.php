<?php namespace Tests\Repositories;

use App\Models\UsoCFDI;
use App\Repositories\UsoCFDIRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UsoCFDIRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UsoCFDIRepository
     */
    protected $usoCFDIRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->usoCFDIRepo = \App::make(UsoCFDIRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->make()->toArray();

        $createdUsoCFDI = $this->usoCFDIRepo->create($usoCFDI);

        $createdUsoCFDI = $createdUsoCFDI->toArray();
        $this->assertArrayHasKey('id', $createdUsoCFDI);
        $this->assertNotNull($createdUsoCFDI['id'], 'Created UsoCFDI must have id specified');
        $this->assertNotNull(UsoCFDI::find($createdUsoCFDI['id']), 'UsoCFDI with given id must be in DB');
        $this->assertModelData($usoCFDI, $createdUsoCFDI);
    }

    /**
     * @test read
     */
    public function test_read_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();

        $dbUsoCFDI = $this->usoCFDIRepo->find($usoCFDI->id);

        $dbUsoCFDI = $dbUsoCFDI->toArray();
        $this->assertModelData($usoCFDI->toArray(), $dbUsoCFDI);
    }

    /**
     * @test update
     */
    public function test_update_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();
        $fakeUsoCFDI = UsoCFDI::factory()->make()->toArray();

        $updatedUsoCFDI = $this->usoCFDIRepo->update($fakeUsoCFDI, $usoCFDI->id);

        $this->assertModelData($fakeUsoCFDI, $updatedUsoCFDI->toArray());
        $dbUsoCFDI = $this->usoCFDIRepo->find($usoCFDI->id);
        $this->assertModelData($fakeUsoCFDI, $dbUsoCFDI->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();

        $resp = $this->usoCFDIRepo->delete($usoCFDI->id);

        $this->assertTrue($resp);
        $this->assertNull(UsoCFDI::find($usoCFDI->id), 'UsoCFDI should not exist in DB');
    }
}
