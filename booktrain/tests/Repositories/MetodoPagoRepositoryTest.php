<?php namespace Tests\Repositories;

use App\Models\MetodoPago;
use App\Repositories\MetodoPagoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MetodoPagoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MetodoPagoRepository
     */
    protected $metodoPagoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->metodoPagoRepo = \App::make(MetodoPagoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->make()->toArray();

        $createdMetodoPago = $this->metodoPagoRepo->create($metodoPago);

        $createdMetodoPago = $createdMetodoPago->toArray();
        $this->assertArrayHasKey('id', $createdMetodoPago);
        $this->assertNotNull($createdMetodoPago['id'], 'Created MetodoPago must have id specified');
        $this->assertNotNull(MetodoPago::find($createdMetodoPago['id']), 'MetodoPago with given id must be in DB');
        $this->assertModelData($metodoPago, $createdMetodoPago);
    }

    /**
     * @test read
     */
    public function test_read_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();

        $dbMetodoPago = $this->metodoPagoRepo->find($metodoPago->id);

        $dbMetodoPago = $dbMetodoPago->toArray();
        $this->assertModelData($metodoPago->toArray(), $dbMetodoPago);
    }

    /**
     * @test update
     */
    public function test_update_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();
        $fakeMetodoPago = MetodoPago::factory()->make()->toArray();

        $updatedMetodoPago = $this->metodoPagoRepo->update($fakeMetodoPago, $metodoPago->id);

        $this->assertModelData($fakeMetodoPago, $updatedMetodoPago->toArray());
        $dbMetodoPago = $this->metodoPagoRepo->find($metodoPago->id);
        $this->assertModelData($fakeMetodoPago, $dbMetodoPago->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();

        $resp = $this->metodoPagoRepo->delete($metodoPago->id);

        $this->assertTrue($resp);
        $this->assertNull(MetodoPago::find($metodoPago->id), 'MetodoPago should not exist in DB');
    }
}
