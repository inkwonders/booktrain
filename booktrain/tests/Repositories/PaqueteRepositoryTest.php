<?php namespace Tests\Repositories;

use App\Models\Paquete;
use App\Repositories\PaqueteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PaqueteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaqueteRepository
     */
    protected $paqueteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paqueteRepo = \App::make(PaqueteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_paquete()
    {
        $paquete = Paquete::factory()->make()->toArray();

        $createdPaquete = $this->paqueteRepo->create($paquete);

        $createdPaquete = $createdPaquete->toArray();
        $this->assertArrayHasKey('id', $createdPaquete);
        $this->assertNotNull($createdPaquete['id'], 'Created Paquete must have id specified');
        $this->assertNotNull(Paquete::find($createdPaquete['id']), 'Paquete with given id must be in DB');
        $this->assertModelData($paquete, $createdPaquete);
    }

    /**
     * @test read
     */
    public function test_read_paquete()
    {
        $paquete = Paquete::factory()->create();

        $dbPaquete = $this->paqueteRepo->find($paquete->id);

        $dbPaquete = $dbPaquete->toArray();
        $this->assertModelData($paquete->toArray(), $dbPaquete);
    }

    /**
     * @test update
     */
    public function test_update_paquete()
    {
        $paquete = Paquete::factory()->create();
        $fakePaquete = Paquete::factory()->make()->toArray();

        $updatedPaquete = $this->paqueteRepo->update($fakePaquete, $paquete->id);

        $this->assertModelData($fakePaquete, $updatedPaquete->toArray());
        $dbPaquete = $this->paqueteRepo->find($paquete->id);
        $this->assertModelData($fakePaquete, $dbPaquete->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_paquete()
    {
        $paquete = Paquete::factory()->create();

        $resp = $this->paqueteRepo->delete($paquete->id);

        $this->assertTrue($resp);
        $this->assertNull(Paquete::find($paquete->id), 'Paquete should not exist in DB');
    }
}
