<?php namespace Tests\Repositories;

use App\Models\DireccionEntrega;
use App\Repositories\DireccionEntregaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DireccionEntregaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DireccionEntregaRepository
     */
    protected $direccionEntregaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->direccionEntregaRepo = \App::make(DireccionEntregaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->make()->toArray();

        $createdDireccionEntrega = $this->direccionEntregaRepo->create($direccionEntrega);

        $createdDireccionEntrega = $createdDireccionEntrega->toArray();
        $this->assertArrayHasKey('id', $createdDireccionEntrega);
        $this->assertNotNull($createdDireccionEntrega['id'], 'Created DireccionEntrega must have id specified');
        $this->assertNotNull(DireccionEntrega::find($createdDireccionEntrega['id']), 'DireccionEntrega with given id must be in DB');
        $this->assertModelData($direccionEntrega, $createdDireccionEntrega);
    }

    /**
     * @test read
     */
    public function test_read_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();

        $dbDireccionEntrega = $this->direccionEntregaRepo->find($direccionEntrega->id);

        $dbDireccionEntrega = $dbDireccionEntrega->toArray();
        $this->assertModelData($direccionEntrega->toArray(), $dbDireccionEntrega);
    }

    /**
     * @test update
     */
    public function test_update_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();
        $fakeDireccionEntrega = DireccionEntrega::factory()->make()->toArray();

        $updatedDireccionEntrega = $this->direccionEntregaRepo->update($fakeDireccionEntrega, $direccionEntrega->id);

        $this->assertModelData($fakeDireccionEntrega, $updatedDireccionEntrega->toArray());
        $dbDireccionEntrega = $this->direccionEntregaRepo->find($direccionEntrega->id);
        $this->assertModelData($fakeDireccionEntrega, $dbDireccionEntrega->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();

        $resp = $this->direccionEntregaRepo->delete($direccionEntrega->id);

        $this->assertTrue($resp);
        $this->assertNull(DireccionEntrega::find($direccionEntrega->id), 'DireccionEntrega should not exist in DB');
    }
}
