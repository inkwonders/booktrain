<?php namespace Tests\Repositories;

use App\Models\ResumenPedido;
use App\Repositories\ResumenPedidoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ResumenPedidoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResumenPedidoRepository
     */
    protected $resumenPedidoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->resumenPedidoRepo = \App::make(ResumenPedidoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->make()->toArray();

        $createdResumenPedido = $this->resumenPedidoRepo->create($resumenPedido);

        $createdResumenPedido = $createdResumenPedido->toArray();
        $this->assertArrayHasKey('id', $createdResumenPedido);
        $this->assertNotNull($createdResumenPedido['id'], 'Created ResumenPedido must have id specified');
        $this->assertNotNull(ResumenPedido::find($createdResumenPedido['id']), 'ResumenPedido with given id must be in DB');
        $this->assertModelData($resumenPedido, $createdResumenPedido);
    }

    /**
     * @test read
     */
    public function test_read_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();

        $dbResumenPedido = $this->resumenPedidoRepo->find($resumenPedido->id);

        $dbResumenPedido = $dbResumenPedido->toArray();
        $this->assertModelData($resumenPedido->toArray(), $dbResumenPedido);
    }

    /**
     * @test update
     */
    public function test_update_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();
        $fakeResumenPedido = ResumenPedido::factory()->make()->toArray();

        $updatedResumenPedido = $this->resumenPedidoRepo->update($fakeResumenPedido, $resumenPedido->id);

        $this->assertModelData($fakeResumenPedido, $updatedResumenPedido->toArray());
        $dbResumenPedido = $this->resumenPedidoRepo->find($resumenPedido->id);
        $this->assertModelData($fakeResumenPedido, $dbResumenPedido->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();

        $resp = $this->resumenPedidoRepo->delete($resumenPedido->id);

        $this->assertTrue($resp);
        $this->assertNull(ResumenPedido::find($resumenPedido->id), 'ResumenPedido should not exist in DB');
    }
}
