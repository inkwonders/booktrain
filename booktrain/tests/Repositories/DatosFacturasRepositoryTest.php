<?php namespace Tests\Repositories;

use App\Models\DatosFacturas;
use App\Repositories\DatosFacturasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DatosFacturasRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DatosFacturasRepository
     */
    protected $datosFacturasRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->datosFacturasRepo = \App::make(DatosFacturasRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->make()->toArray();

        $createdDatosFacturas = $this->datosFacturasRepo->create($datosFacturas);

        $createdDatosFacturas = $createdDatosFacturas->toArray();
        $this->assertArrayHasKey('id', $createdDatosFacturas);
        $this->assertNotNull($createdDatosFacturas['id'], 'Created DatosFacturas must have id specified');
        $this->assertNotNull(DatosFacturas::find($createdDatosFacturas['id']), 'DatosFacturas with given id must be in DB');
        $this->assertModelData($datosFacturas, $createdDatosFacturas);
    }

    /**
     * @test read
     */
    public function test_read_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();

        $dbDatosFacturas = $this->datosFacturasRepo->find($datosFacturas->id);

        $dbDatosFacturas = $dbDatosFacturas->toArray();
        $this->assertModelData($datosFacturas->toArray(), $dbDatosFacturas);
    }

    /**
     * @test update
     */
    public function test_update_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();
        $fakeDatosFacturas = DatosFacturas::factory()->make()->toArray();

        $updatedDatosFacturas = $this->datosFacturasRepo->update($fakeDatosFacturas, $datosFacturas->id);

        $this->assertModelData($fakeDatosFacturas, $updatedDatosFacturas->toArray());
        $dbDatosFacturas = $this->datosFacturasRepo->find($datosFacturas->id);
        $this->assertModelData($fakeDatosFacturas, $dbDatosFacturas->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();

        $resp = $this->datosFacturasRepo->delete($datosFacturas->id);

        $this->assertTrue($resp);
        $this->assertNull(DatosFacturas::find($datosFacturas->id), 'DatosFacturas should not exist in DB');
    }
}
