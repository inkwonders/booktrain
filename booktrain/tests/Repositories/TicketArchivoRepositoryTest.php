<?php namespace Tests\Repositories;

use App\Models\TicketArchivo;
use App\Repositories\TicketArchivoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TicketArchivoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TicketArchivoRepository
     */
    protected $ticketArchivoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ticketArchivoRepo = \App::make(TicketArchivoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->make()->toArray();

        $createdTicketArchivo = $this->ticketArchivoRepo->create($ticketArchivo);

        $createdTicketArchivo = $createdTicketArchivo->toArray();
        $this->assertArrayHasKey('id', $createdTicketArchivo);
        $this->assertNotNull($createdTicketArchivo['id'], 'Created TicketArchivo must have id specified');
        $this->assertNotNull(TicketArchivo::find($createdTicketArchivo['id']), 'TicketArchivo with given id must be in DB');
        $this->assertModelData($ticketArchivo, $createdTicketArchivo);
    }

    /**
     * @test read
     */
    public function test_read_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();

        $dbTicketArchivo = $this->ticketArchivoRepo->find($ticketArchivo->id);

        $dbTicketArchivo = $dbTicketArchivo->toArray();
        $this->assertModelData($ticketArchivo->toArray(), $dbTicketArchivo);
    }

    /**
     * @test update
     */
    public function test_update_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();
        $fakeTicketArchivo = TicketArchivo::factory()->make()->toArray();

        $updatedTicketArchivo = $this->ticketArchivoRepo->update($fakeTicketArchivo, $ticketArchivo->id);

        $this->assertModelData($fakeTicketArchivo, $updatedTicketArchivo->toArray());
        $dbTicketArchivo = $this->ticketArchivoRepo->find($ticketArchivo->id);
        $this->assertModelData($fakeTicketArchivo, $dbTicketArchivo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();

        $resp = $this->ticketArchivoRepo->delete($ticketArchivo->id);

        $this->assertTrue($resp);
        $this->assertNull(TicketArchivo::find($ticketArchivo->id), 'TicketArchivo should not exist in DB');
    }
}
