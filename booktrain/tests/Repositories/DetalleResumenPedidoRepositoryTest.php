<?php namespace Tests\Repositories;

use App\Models\DetalleResumenPedido;
use App\Repositories\DetalleResumenPedidoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetalleResumenPedidoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetalleResumenPedidoRepository
     */
    protected $detalleResumenPedidoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detalleResumenPedidoRepo = \App::make(DetalleResumenPedidoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->make()->toArray();

        $createdDetalleResumenPedido = $this->detalleResumenPedidoRepo->create($detalleResumenPedido);

        $createdDetalleResumenPedido = $createdDetalleResumenPedido->toArray();
        $this->assertArrayHasKey('id', $createdDetalleResumenPedido);
        $this->assertNotNull($createdDetalleResumenPedido['id'], 'Created DetalleResumenPedido must have id specified');
        $this->assertNotNull(DetalleResumenPedido::find($createdDetalleResumenPedido['id']), 'DetalleResumenPedido with given id must be in DB');
        $this->assertModelData($detalleResumenPedido, $createdDetalleResumenPedido);
    }

    /**
     * @test read
     */
    public function test_read_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();

        $dbDetalleResumenPedido = $this->detalleResumenPedidoRepo->find($detalleResumenPedido->id);

        $dbDetalleResumenPedido = $dbDetalleResumenPedido->toArray();
        $this->assertModelData($detalleResumenPedido->toArray(), $dbDetalleResumenPedido);
    }

    /**
     * @test update
     */
    public function test_update_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();
        $fakeDetalleResumenPedido = DetalleResumenPedido::factory()->make()->toArray();

        $updatedDetalleResumenPedido = $this->detalleResumenPedidoRepo->update($fakeDetalleResumenPedido, $detalleResumenPedido->id);

        $this->assertModelData($fakeDetalleResumenPedido, $updatedDetalleResumenPedido->toArray());
        $dbDetalleResumenPedido = $this->detalleResumenPedidoRepo->find($detalleResumenPedido->id);
        $this->assertModelData($fakeDetalleResumenPedido, $dbDetalleResumenPedido->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();

        $resp = $this->detalleResumenPedidoRepo->delete($detalleResumenPedido->id);

        $this->assertTrue($resp);
        $this->assertNull(DetalleResumenPedido::find($detalleResumenPedido->id), 'DetalleResumenPedido should not exist in DB');
    }
}
