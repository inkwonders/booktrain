<?php namespace Tests\Repositories;

use App\Models\LibroPaquete;
use App\Repositories\LibroPaqueteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LibroPaqueteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LibroPaqueteRepository
     */
    protected $libroPaqueteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->libroPaqueteRepo = \App::make(LibroPaqueteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->make()->toArray();

        $createdLibroPaquete = $this->libroPaqueteRepo->create($libroPaquete);

        $createdLibroPaquete = $createdLibroPaquete->toArray();
        $this->assertArrayHasKey('id', $createdLibroPaquete);
        $this->assertNotNull($createdLibroPaquete['id'], 'Created LibroPaquete must have id specified');
        $this->assertNotNull(LibroPaquete::find($createdLibroPaquete['id']), 'LibroPaquete with given id must be in DB');
        $this->assertModelData($libroPaquete, $createdLibroPaquete);
    }

    /**
     * @test read
     */
    public function test_read_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();

        $dbLibroPaquete = $this->libroPaqueteRepo->find($libroPaquete->id);

        $dbLibroPaquete = $dbLibroPaquete->toArray();
        $this->assertModelData($libroPaquete->toArray(), $dbLibroPaquete);
    }

    /**
     * @test update
     */
    public function test_update_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();
        $fakeLibroPaquete = LibroPaquete::factory()->make()->toArray();

        $updatedLibroPaquete = $this->libroPaqueteRepo->update($fakeLibroPaquete, $libroPaquete->id);

        $this->assertModelData($fakeLibroPaquete, $updatedLibroPaquete->toArray());
        $dbLibroPaquete = $this->libroPaqueteRepo->find($libroPaquete->id);
        $this->assertModelData($fakeLibroPaquete, $dbLibroPaquete->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();

        $resp = $this->libroPaqueteRepo->delete($libroPaquete->id);

        $this->assertTrue($resp);
        $this->assertNull(LibroPaquete::find($libroPaquete->id), 'LibroPaquete should not exist in DB');
    }
}
