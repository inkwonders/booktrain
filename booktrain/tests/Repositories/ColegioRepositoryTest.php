<?php namespace Tests\Repositories;

use App\Models\Colegio;
use App\Repositories\ColegioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ColegioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ColegioRepository
     */
    protected $colegioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->colegioRepo = \App::make(ColegioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_colegio()
    {
        $colegio = Colegio::factory()->make()->toArray();

        $createdColegio = $this->colegioRepo->create($colegio);

        $createdColegio = $createdColegio->toArray();
        $this->assertArrayHasKey('id', $createdColegio);
        $this->assertNotNull($createdColegio['id'], 'Created Colegio must have id specified');
        $this->assertNotNull(Colegio::find($createdColegio['id']), 'Colegio with given id must be in DB');
        $this->assertModelData($colegio, $createdColegio);
    }

    /**
     * @test read
     */
    public function test_read_colegio()
    {
        $colegio = Colegio::factory()->create();

        $dbColegio = $this->colegioRepo->find($colegio->id);

        $dbColegio = $dbColegio->toArray();
        $this->assertModelData($colegio->toArray(), $dbColegio);
    }

    /**
     * @test update
     */
    public function test_update_colegio()
    {
        $colegio = Colegio::factory()->create();
        $fakeColegio = Colegio::factory()->make()->toArray();

        $updatedColegio = $this->colegioRepo->update($fakeColegio, $colegio->id);

        $this->assertModelData($fakeColegio, $updatedColegio->toArray());
        $dbColegio = $this->colegioRepo->find($colegio->id);
        $this->assertModelData($fakeColegio, $dbColegio->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_colegio()
    {
        $colegio = Colegio::factory()->create();

        $resp = $this->colegioRepo->delete($colegio->id);

        $this->assertTrue($resp);
        $this->assertNull(Colegio::find($colegio->id), 'Colegio should not exist in DB');
    }
}
