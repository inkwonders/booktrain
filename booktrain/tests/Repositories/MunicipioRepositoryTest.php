<?php namespace Tests\Repositories;

use App\Models\Municipio;
use App\Repositories\MunicipioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MunicipioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MunicipioRepository
     */
    protected $municipioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->municipioRepo = \App::make(MunicipioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_municipio()
    {
        $municipio = Municipio::factory()->make()->toArray();

        $createdMunicipio = $this->municipioRepo->create($municipio);

        $createdMunicipio = $createdMunicipio->toArray();
        $this->assertArrayHasKey('id', $createdMunicipio);
        $this->assertNotNull($createdMunicipio['id'], 'Created Municipio must have id specified');
        $this->assertNotNull(Municipio::find($createdMunicipio['id']), 'Municipio with given id must be in DB');
        $this->assertModelData($municipio, $createdMunicipio);
    }

    /**
     * @test read
     */
    public function test_read_municipio()
    {
        $municipio = Municipio::factory()->create();

        $dbMunicipio = $this->municipioRepo->find($municipio->id);

        $dbMunicipio = $dbMunicipio->toArray();
        $this->assertModelData($municipio->toArray(), $dbMunicipio);
    }

    /**
     * @test update
     */
    public function test_update_municipio()
    {
        $municipio = Municipio::factory()->create();
        $fakeMunicipio = Municipio::factory()->make()->toArray();

        $updatedMunicipio = $this->municipioRepo->update($fakeMunicipio, $municipio->id);

        $this->assertModelData($fakeMunicipio, $updatedMunicipio->toArray());
        $dbMunicipio = $this->municipioRepo->find($municipio->id);
        $this->assertModelData($fakeMunicipio, $dbMunicipio->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_municipio()
    {
        $municipio = Municipio::factory()->create();

        $resp = $this->municipioRepo->delete($municipio->id);

        $this->assertTrue($resp);
        $this->assertNull(Municipio::find($municipio->id), 'Municipio should not exist in DB');
    }
}
