<?php namespace Tests\Repositories;

use App\Models\TicketUsuario;
use App\Repositories\TicketUsuarioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TicketUsuarioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TicketUsuarioRepository
     */
    protected $ticketUsuarioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ticketUsuarioRepo = \App::make(TicketUsuarioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->make()->toArray();

        $createdTicketUsuario = $this->ticketUsuarioRepo->create($ticketUsuario);

        $createdTicketUsuario = $createdTicketUsuario->toArray();
        $this->assertArrayHasKey('id', $createdTicketUsuario);
        $this->assertNotNull($createdTicketUsuario['id'], 'Created TicketUsuario must have id specified');
        $this->assertNotNull(TicketUsuario::find($createdTicketUsuario['id']), 'TicketUsuario with given id must be in DB');
        $this->assertModelData($ticketUsuario, $createdTicketUsuario);
    }

    /**
     * @test read
     */
    public function test_read_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();

        $dbTicketUsuario = $this->ticketUsuarioRepo->find($ticketUsuario->id);

        $dbTicketUsuario = $dbTicketUsuario->toArray();
        $this->assertModelData($ticketUsuario->toArray(), $dbTicketUsuario);
    }

    /**
     * @test update
     */
    public function test_update_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();
        $fakeTicketUsuario = TicketUsuario::factory()->make()->toArray();

        $updatedTicketUsuario = $this->ticketUsuarioRepo->update($fakeTicketUsuario, $ticketUsuario->id);

        $this->assertModelData($fakeTicketUsuario, $updatedTicketUsuario->toArray());
        $dbTicketUsuario = $this->ticketUsuarioRepo->find($ticketUsuario->id);
        $this->assertModelData($fakeTicketUsuario, $dbTicketUsuario->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();

        $resp = $this->ticketUsuarioRepo->delete($ticketUsuario->id);

        $this->assertTrue($resp);
        $this->assertNull(TicketUsuario::find($ticketUsuario->id), 'TicketUsuario should not exist in DB');
    }
}
