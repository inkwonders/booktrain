<?php namespace Tests\Repositories;

use App\Models\SeccionColegio;
use App\Repositories\SeccionColegioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeccionColegioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeccionColegioRepository
     */
    protected $seccionColegioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seccionColegioRepo = \App::make(SeccionColegioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->make()->toArray();

        $createdSeccionColegio = $this->seccionColegioRepo->create($seccionColegio);

        $createdSeccionColegio = $createdSeccionColegio->toArray();
        $this->assertArrayHasKey('id', $createdSeccionColegio);
        $this->assertNotNull($createdSeccionColegio['id'], 'Created SeccionColegio must have id specified');
        $this->assertNotNull(SeccionColegio::find($createdSeccionColegio['id']), 'SeccionColegio with given id must be in DB');
        $this->assertModelData($seccionColegio, $createdSeccionColegio);
    }

    /**
     * @test read
     */
    public function test_read_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();

        $dbSeccionColegio = $this->seccionColegioRepo->find($seccionColegio->id);

        $dbSeccionColegio = $dbSeccionColegio->toArray();
        $this->assertModelData($seccionColegio->toArray(), $dbSeccionColegio);
    }

    /**
     * @test update
     */
    public function test_update_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();
        $fakeSeccionColegio = SeccionColegio::factory()->make()->toArray();

        $updatedSeccionColegio = $this->seccionColegioRepo->update($fakeSeccionColegio, $seccionColegio->id);

        $this->assertModelData($fakeSeccionColegio, $updatedSeccionColegio->toArray());
        $dbSeccionColegio = $this->seccionColegioRepo->find($seccionColegio->id);
        $this->assertModelData($fakeSeccionColegio, $dbSeccionColegio->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();

        $resp = $this->seccionColegioRepo->delete($seccionColegio->id);

        $this->assertTrue($resp);
        $this->assertNull(SeccionColegio::find($seccionColegio->id), 'SeccionColegio should not exist in DB');
    }
}
