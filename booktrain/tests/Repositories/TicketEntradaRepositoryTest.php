<?php namespace Tests\Repositories;

use App\Models\TicketEntrada;
use App\Repositories\TicketEntradaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TicketEntradaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TicketEntradaRepository
     */
    protected $ticketEntradaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ticketEntradaRepo = \App::make(TicketEntradaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->make()->toArray();

        $createdTicketEntrada = $this->ticketEntradaRepo->create($ticketEntrada);

        $createdTicketEntrada = $createdTicketEntrada->toArray();
        $this->assertArrayHasKey('id', $createdTicketEntrada);
        $this->assertNotNull($createdTicketEntrada['id'], 'Created TicketEntrada must have id specified');
        $this->assertNotNull(TicketEntrada::find($createdTicketEntrada['id']), 'TicketEntrada with given id must be in DB');
        $this->assertModelData($ticketEntrada, $createdTicketEntrada);
    }

    /**
     * @test read
     */
    public function test_read_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();

        $dbTicketEntrada = $this->ticketEntradaRepo->find($ticketEntrada->id);

        $dbTicketEntrada = $dbTicketEntrada->toArray();
        $this->assertModelData($ticketEntrada->toArray(), $dbTicketEntrada);
    }

    /**
     * @test update
     */
    public function test_update_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();
        $fakeTicketEntrada = TicketEntrada::factory()->make()->toArray();

        $updatedTicketEntrada = $this->ticketEntradaRepo->update($fakeTicketEntrada, $ticketEntrada->id);

        $this->assertModelData($fakeTicketEntrada, $updatedTicketEntrada->toArray());
        $dbTicketEntrada = $this->ticketEntradaRepo->find($ticketEntrada->id);
        $this->assertModelData($fakeTicketEntrada, $dbTicketEntrada->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();

        $resp = $this->ticketEntradaRepo->delete($ticketEntrada->id);

        $this->assertTrue($resp);
        $this->assertNull(TicketEntrada::find($ticketEntrada->id), 'TicketEntrada should not exist in DB');
    }
}
