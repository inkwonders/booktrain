<?php namespace Tests\Repositories;

use App\Models\Envio;
use App\Repositories\EnvioRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EnvioRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EnvioRepository
     */
    protected $envioRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->envioRepo = \App::make(EnvioRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_envio()
    {
        $envio = Envio::factory()->make()->toArray();

        $createdEnvio = $this->envioRepo->create($envio);

        $createdEnvio = $createdEnvio->toArray();
        $this->assertArrayHasKey('id', $createdEnvio);
        $this->assertNotNull($createdEnvio['id'], 'Created Envio must have id specified');
        $this->assertNotNull(Envio::find($createdEnvio['id']), 'Envio with given id must be in DB');
        $this->assertModelData($envio, $createdEnvio);
    }

    /**
     * @test read
     */
    public function test_read_envio()
    {
        $envio = Envio::factory()->create();

        $dbEnvio = $this->envioRepo->find($envio->id);

        $dbEnvio = $dbEnvio->toArray();
        $this->assertModelData($envio->toArray(), $dbEnvio);
    }

    /**
     * @test update
     */
    public function test_update_envio()
    {
        $envio = Envio::factory()->create();
        $fakeEnvio = Envio::factory()->make()->toArray();

        $updatedEnvio = $this->envioRepo->update($fakeEnvio, $envio->id);

        $this->assertModelData($fakeEnvio, $updatedEnvio->toArray());
        $dbEnvio = $this->envioRepo->find($envio->id);
        $this->assertModelData($fakeEnvio, $dbEnvio->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_envio()
    {
        $envio = Envio::factory()->create();

        $resp = $this->envioRepo->delete($envio->id);

        $this->assertTrue($resp);
        $this->assertNull(Envio::find($envio->id), 'Envio should not exist in DB');
    }
}
