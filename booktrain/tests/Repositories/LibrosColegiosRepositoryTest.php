<?php namespace Tests\Repositories;

use App\Models\LibrosColegios;
use App\Repositories\LibrosColegiosRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LibrosColegiosRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LibrosColegiosRepository
     */
    protected $librosColegiosRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->librosColegiosRepo = \App::make(LibrosColegiosRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->make()->toArray();

        $createdLibrosColegios = $this->librosColegiosRepo->create($librosColegios);

        $createdLibrosColegios = $createdLibrosColegios->toArray();
        $this->assertArrayHasKey('id', $createdLibrosColegios);
        $this->assertNotNull($createdLibrosColegios['id'], 'Created LibrosColegios must have id specified');
        $this->assertNotNull(LibrosColegios::find($createdLibrosColegios['id']), 'LibrosColegios with given id must be in DB');
        $this->assertModelData($librosColegios, $createdLibrosColegios);
    }

    /**
     * @test read
     */
    public function test_read_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();

        $dbLibrosColegios = $this->librosColegiosRepo->find($librosColegios->id);

        $dbLibrosColegios = $dbLibrosColegios->toArray();
        $this->assertModelData($librosColegios->toArray(), $dbLibrosColegios);
    }

    /**
     * @test update
     */
    public function test_update_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();
        $fakeLibrosColegios = LibrosColegios::factory()->make()->toArray();

        $updatedLibrosColegios = $this->librosColegiosRepo->update($fakeLibrosColegios, $librosColegios->id);

        $this->assertModelData($fakeLibrosColegios, $updatedLibrosColegios->toArray());
        $dbLibrosColegios = $this->librosColegiosRepo->find($librosColegios->id);
        $this->assertModelData($fakeLibrosColegios, $dbLibrosColegios->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();

        $resp = $this->librosColegiosRepo->delete($librosColegios->id);

        $this->assertTrue($resp);
        $this->assertNull(LibrosColegios::find($librosColegios->id), 'LibrosColegios should not exist in DB');
    }
}
