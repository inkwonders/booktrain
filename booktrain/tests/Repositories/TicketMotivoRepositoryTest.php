<?php namespace Tests\Repositories;

use App\Models\TicketMotivo;
use App\Repositories\TicketMotivoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TicketMotivoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TicketMotivoRepository
     */
    protected $ticketMotivoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ticketMotivoRepo = \App::make(TicketMotivoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->make()->toArray();

        $createdTicketMotivo = $this->ticketMotivoRepo->create($ticketMotivo);

        $createdTicketMotivo = $createdTicketMotivo->toArray();
        $this->assertArrayHasKey('id', $createdTicketMotivo);
        $this->assertNotNull($createdTicketMotivo['id'], 'Created TicketMotivo must have id specified');
        $this->assertNotNull(TicketMotivo::find($createdTicketMotivo['id']), 'TicketMotivo with given id must be in DB');
        $this->assertModelData($ticketMotivo, $createdTicketMotivo);
    }

    /**
     * @test read
     */
    public function test_read_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();

        $dbTicketMotivo = $this->ticketMotivoRepo->find($ticketMotivo->id);

        $dbTicketMotivo = $dbTicketMotivo->toArray();
        $this->assertModelData($ticketMotivo->toArray(), $dbTicketMotivo);
    }

    /**
     * @test update
     */
    public function test_update_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();
        $fakeTicketMotivo = TicketMotivo::factory()->make()->toArray();

        $updatedTicketMotivo = $this->ticketMotivoRepo->update($fakeTicketMotivo, $ticketMotivo->id);

        $this->assertModelData($fakeTicketMotivo, $updatedTicketMotivo->toArray());
        $dbTicketMotivo = $this->ticketMotivoRepo->find($ticketMotivo->id);
        $this->assertModelData($fakeTicketMotivo, $dbTicketMotivo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();

        $resp = $this->ticketMotivoRepo->delete($ticketMotivo->id);

        $this->assertTrue($resp);
        $this->assertNull(TicketMotivo::find($ticketMotivo->id), 'TicketMotivo should not exist in DB');
    }
}
