<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Libro;

class LibroApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_libro()
    {
        $libro = Libro::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/libros', $libro
        );

        $this->assertApiResponse($libro);
    }

    /**
     * @test
     */
    public function test_read_libro()
    {
        $libro = Libro::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/libros/'.$libro->id
        );

        $this->assertApiResponse($libro->toArray());
    }

    /**
     * @test
     */
    public function test_update_libro()
    {
        $libro = Libro::factory()->create();
        $editedLibro = Libro::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/libros/'.$libro->id,
            $editedLibro
        );

        $this->assertApiResponse($editedLibro);
    }

    /**
     * @test
     */
    public function test_delete_libro()
    {
        $libro = Libro::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/libros/'.$libro->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/libros/'.$libro->id
        );

        $this->response->assertStatus(404);
    }
}
