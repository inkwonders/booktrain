<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LibrosColegios;

class LibrosColegiosApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/libros_colegios', $librosColegios
        );

        $this->assertApiResponse($librosColegios);
    }

    /**
     * @test
     */
    public function test_read_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/libros_colegios/'.$librosColegios->id
        );

        $this->assertApiResponse($librosColegios->toArray());
    }

    /**
     * @test
     */
    public function test_update_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();
        $editedLibrosColegios = LibrosColegios::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/libros_colegios/'.$librosColegios->id,
            $editedLibrosColegios
        );

        $this->assertApiResponse($editedLibrosColegios);
    }

    /**
     * @test
     */
    public function test_delete_libros_colegios()
    {
        $librosColegios = LibrosColegios::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/libros_colegios/'.$librosColegios->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/libros_colegios/'.$librosColegios->id
        );

        $this->response->assertStatus(404);
    }
}
