<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Envio;

class EnvioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_envio()
    {
        $envio = Envio::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/envios', $envio
        );

        $this->assertApiResponse($envio);
    }

    /**
     * @test
     */
    public function test_read_envio()
    {
        $envio = Envio::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/envios/'.$envio->id
        );

        $this->assertApiResponse($envio->toArray());
    }

    /**
     * @test
     */
    public function test_update_envio()
    {
        $envio = Envio::factory()->create();
        $editedEnvio = Envio::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/envios/'.$envio->id,
            $editedEnvio
        );

        $this->assertApiResponse($editedEnvio);
    }

    /**
     * @test
     */
    public function test_delete_envio()
    {
        $envio = Envio::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/envios/'.$envio->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/envios/'.$envio->id
        );

        $this->response->assertStatus(404);
    }
}
