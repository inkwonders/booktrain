<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ResumenPedido;

class ResumenPedidoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/resumen_pedidos', $resumenPedido
        );

        $this->assertApiResponse($resumenPedido);
    }

    /**
     * @test
     */
    public function test_read_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/resumen_pedidos/'.$resumenPedido->id
        );

        $this->assertApiResponse($resumenPedido->toArray());
    }

    /**
     * @test
     */
    public function test_update_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();
        $editedResumenPedido = ResumenPedido::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/resumen_pedidos/'.$resumenPedido->id,
            $editedResumenPedido
        );

        $this->assertApiResponse($editedResumenPedido);
    }

    /**
     * @test
     */
    public function test_delete_resumen_pedido()
    {
        $resumenPedido = ResumenPedido::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/resumen_pedidos/'.$resumenPedido->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/resumen_pedidos/'.$resumenPedido->id
        );

        $this->response->assertStatus(404);
    }
}
