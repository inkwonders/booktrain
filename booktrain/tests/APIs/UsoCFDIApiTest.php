<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UsoCFDI;

class UsoCFDIApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/uso_c_f_d_is', $usoCFDI
        );

        $this->assertApiResponse($usoCFDI);
    }

    /**
     * @test
     */
    public function test_read_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/uso_c_f_d_is/'.$usoCFDI->id
        );

        $this->assertApiResponse($usoCFDI->toArray());
    }

    /**
     * @test
     */
    public function test_update_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();
        $editedUsoCFDI = UsoCFDI::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/uso_c_f_d_is/'.$usoCFDI->id,
            $editedUsoCFDI
        );

        $this->assertApiResponse($editedUsoCFDI);
    }

    /**
     * @test
     */
    public function test_delete_uso_c_f_d_i()
    {
        $usoCFDI = UsoCFDI::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/uso_c_f_d_is/'.$usoCFDI->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/uso_c_f_d_is/'.$usoCFDI->id
        );

        $this->response->assertStatus(404);
    }
}
