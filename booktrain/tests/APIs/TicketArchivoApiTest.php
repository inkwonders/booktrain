<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TicketArchivo;

class TicketArchivoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ticket_archivos', $ticketArchivo
        );

        $this->assertApiResponse($ticketArchivo);
    }

    /**
     * @test
     */
    public function test_read_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ticket_archivos/'.$ticketArchivo->id
        );

        $this->assertApiResponse($ticketArchivo->toArray());
    }

    /**
     * @test
     */
    public function test_update_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();
        $editedTicketArchivo = TicketArchivo::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ticket_archivos/'.$ticketArchivo->id,
            $editedTicketArchivo
        );

        $this->assertApiResponse($editedTicketArchivo);
    }

    /**
     * @test
     */
    public function test_delete_ticket_archivo()
    {
        $ticketArchivo = TicketArchivo::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ticket_archivos/'.$ticketArchivo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ticket_archivos/'.$ticketArchivo->id
        );

        $this->response->assertStatus(404);
    }
}
