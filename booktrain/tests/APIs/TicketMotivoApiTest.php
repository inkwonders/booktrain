<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TicketMotivo;

class TicketMotivoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ticket_motivos', $ticketMotivo
        );

        $this->assertApiResponse($ticketMotivo);
    }

    /**
     * @test
     */
    public function test_read_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ticket_motivos/'.$ticketMotivo->id
        );

        $this->assertApiResponse($ticketMotivo->toArray());
    }

    /**
     * @test
     */
    public function test_update_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();
        $editedTicketMotivo = TicketMotivo::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ticket_motivos/'.$ticketMotivo->id,
            $editedTicketMotivo
        );

        $this->assertApiResponse($editedTicketMotivo);
    }

    /**
     * @test
     */
    public function test_delete_ticket_motivo()
    {
        $ticketMotivo = TicketMotivo::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ticket_motivos/'.$ticketMotivo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ticket_motivos/'.$ticketMotivo->id
        );

        $this->response->assertStatus(404);
    }
}
