<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LibroPaquete;

class LibroPaqueteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/libro_paquetes', $libroPaquete
        );

        $this->assertApiResponse($libroPaquete);
    }

    /**
     * @test
     */
    public function test_read_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/libro_paquetes/'.$libroPaquete->id
        );

        $this->assertApiResponse($libroPaquete->toArray());
    }

    /**
     * @test
     */
    public function test_update_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();
        $editedLibroPaquete = LibroPaquete::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/libro_paquetes/'.$libroPaquete->id,
            $editedLibroPaquete
        );

        $this->assertApiResponse($editedLibroPaquete);
    }

    /**
     * @test
     */
    public function test_delete_libro_paquete()
    {
        $libroPaquete = LibroPaquete::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/libro_paquetes/'.$libroPaquete->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/libro_paquetes/'.$libroPaquete->id
        );

        $this->response->assertStatus(404);
    }
}
