<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TicketEntrada;

class TicketEntradaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ticket_entradas', $ticketEntrada
        );

        $this->assertApiResponse($ticketEntrada);
    }

    /**
     * @test
     */
    public function test_read_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ticket_entradas/'.$ticketEntrada->id
        );

        $this->assertApiResponse($ticketEntrada->toArray());
    }

    /**
     * @test
     */
    public function test_update_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();
        $editedTicketEntrada = TicketEntrada::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ticket_entradas/'.$ticketEntrada->id,
            $editedTicketEntrada
        );

        $this->assertApiResponse($editedTicketEntrada);
    }

    /**
     * @test
     */
    public function test_delete_ticket_entrada()
    {
        $ticketEntrada = TicketEntrada::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ticket_entradas/'.$ticketEntrada->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ticket_entradas/'.$ticketEntrada->id
        );

        $this->response->assertStatus(404);
    }
}
