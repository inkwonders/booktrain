<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\NivelColegio;

class NivelColegioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/nivel_colegios', $nivelColegio
        );

        $this->assertApiResponse($nivelColegio);
    }

    /**
     * @test
     */
    public function test_read_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/nivel_colegios/'.$nivelColegio->id
        );

        $this->assertApiResponse($nivelColegio->toArray());
    }

    /**
     * @test
     */
    public function test_update_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();
        $editedNivelColegio = NivelColegio::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/nivel_colegios/'.$nivelColegio->id,
            $editedNivelColegio
        );

        $this->assertApiResponse($editedNivelColegio);
    }

    /**
     * @test
     */
    public function test_delete_nivel_colegio()
    {
        $nivelColegio = NivelColegio::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/nivel_colegios/'.$nivelColegio->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/nivel_colegios/'.$nivelColegio->id
        );

        $this->response->assertStatus(404);
    }
}
