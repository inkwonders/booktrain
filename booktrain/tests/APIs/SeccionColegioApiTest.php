<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeccionColegio;

class SeccionColegioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/seccion_colegios', $seccionColegio
        );

        $this->assertApiResponse($seccionColegio);
    }

    /**
     * @test
     */
    public function test_read_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/seccion_colegios/'.$seccionColegio->id
        );

        $this->assertApiResponse($seccionColegio->toArray());
    }

    /**
     * @test
     */
    public function test_update_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();
        $editedSeccionColegio = SeccionColegio::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/seccion_colegios/'.$seccionColegio->id,
            $editedSeccionColegio
        );

        $this->assertApiResponse($editedSeccionColegio);
    }

    /**
     * @test
     */
    public function test_delete_seccion_colegio()
    {
        $seccionColegio = SeccionColegio::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/seccion_colegios/'.$seccionColegio->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/seccion_colegios/'.$seccionColegio->id
        );

        $this->response->assertStatus(404);
    }
}
