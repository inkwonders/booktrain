<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TicketEntradaArchivo;

class TicketEntradaArchivoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ticket_entrada_archivos', $ticketEntradaArchivo
        );

        $this->assertApiResponse($ticketEntradaArchivo);
    }

    /**
     * @test
     */
    public function test_read_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ticket_entrada_archivos/'.$ticketEntradaArchivo->id
        );

        $this->assertApiResponse($ticketEntradaArchivo->toArray());
    }

    /**
     * @test
     */
    public function test_update_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();
        $editedTicketEntradaArchivo = TicketEntradaArchivo::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ticket_entrada_archivos/'.$ticketEntradaArchivo->id,
            $editedTicketEntradaArchivo
        );

        $this->assertApiResponse($editedTicketEntradaArchivo);
    }

    /**
     * @test
     */
    public function test_delete_ticket_entrada_archivo()
    {
        $ticketEntradaArchivo = TicketEntradaArchivo::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ticket_entrada_archivos/'.$ticketEntradaArchivo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ticket_entrada_archivos/'.$ticketEntradaArchivo->id
        );

        $this->response->assertStatus(404);
    }
}
