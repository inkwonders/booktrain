<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Paquete;

class PaqueteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_paquete()
    {
        $paquete = Paquete::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/paquetes', $paquete
        );

        $this->assertApiResponse($paquete);
    }

    /**
     * @test
     */
    public function test_read_paquete()
    {
        $paquete = Paquete::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/paquetes/'.$paquete->id
        );

        $this->assertApiResponse($paquete->toArray());
    }

    /**
     * @test
     */
    public function test_update_paquete()
    {
        $paquete = Paquete::factory()->create();
        $editedPaquete = Paquete::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/paquetes/'.$paquete->id,
            $editedPaquete
        );

        $this->assertApiResponse($editedPaquete);
    }

    /**
     * @test
     */
    public function test_delete_paquete()
    {
        $paquete = Paquete::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/paquetes/'.$paquete->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/paquetes/'.$paquete->id
        );

        $this->response->assertStatus(404);
    }
}
