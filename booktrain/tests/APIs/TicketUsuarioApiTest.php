<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TicketUsuario;

class TicketUsuarioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ticket_usuarios', $ticketUsuario
        );

        $this->assertApiResponse($ticketUsuario);
    }

    /**
     * @test
     */
    public function test_read_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ticket_usuarios/'.$ticketUsuario->id
        );

        $this->assertApiResponse($ticketUsuario->toArray());
    }

    /**
     * @test
     */
    public function test_update_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();
        $editedTicketUsuario = TicketUsuario::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ticket_usuarios/'.$ticketUsuario->id,
            $editedTicketUsuario
        );

        $this->assertApiResponse($editedTicketUsuario);
    }

    /**
     * @test
     */
    public function test_delete_ticket_usuario()
    {
        $ticketUsuario = TicketUsuario::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ticket_usuarios/'.$ticketUsuario->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ticket_usuarios/'.$ticketUsuario->id
        );

        $this->response->assertStatus(404);
    }
}
