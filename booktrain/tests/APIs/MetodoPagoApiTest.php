<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MetodoPago;

class MetodoPagoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/metodo_pagos', $metodoPago
        );

        $this->assertApiResponse($metodoPago);
    }

    /**
     * @test
     */
    public function test_read_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/metodo_pagos/'.$metodoPago->id
        );

        $this->assertApiResponse($metodoPago->toArray());
    }

    /**
     * @test
     */
    public function test_update_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();
        $editedMetodoPago = MetodoPago::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/metodo_pagos/'.$metodoPago->id,
            $editedMetodoPago
        );

        $this->assertApiResponse($editedMetodoPago);
    }

    /**
     * @test
     */
    public function test_delete_metodo_pago()
    {
        $metodoPago = MetodoPago::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/metodo_pagos/'.$metodoPago->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/metodo_pagos/'.$metodoPago->id
        );

        $this->response->assertStatus(404);
    }
}
