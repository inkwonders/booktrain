<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Colegio;

class ColegioApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_colegio()
    {
        $colegio = Colegio::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/colegios', $colegio
        );

        $this->assertApiResponse($colegio);
    }

    /**
     * @test
     */
    public function test_read_colegio()
    {
        $colegio = Colegio::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/colegios/'.$colegio->id
        );

        $this->assertApiResponse($colegio->toArray());
    }

    /**
     * @test
     */
    public function test_update_colegio()
    {
        $colegio = Colegio::factory()->create();
        $editedColegio = Colegio::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/colegios/'.$colegio->id,
            $editedColegio
        );

        $this->assertApiResponse($editedColegio);
    }

    /**
     * @test
     */
    public function test_delete_colegio()
    {
        $colegio = Colegio::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/colegios/'.$colegio->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/colegios/'.$colegio->id
        );

        $this->response->assertStatus(404);
    }
}
