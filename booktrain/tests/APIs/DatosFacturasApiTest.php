<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DatosFacturas;

class DatosFacturasApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/datos_facturas', $datosFacturas
        );

        $this->assertApiResponse($datosFacturas);
    }

    /**
     * @test
     */
    public function test_read_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/datos_facturas/'.$datosFacturas->id
        );

        $this->assertApiResponse($datosFacturas->toArray());
    }

    /**
     * @test
     */
    public function test_update_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();
        $editedDatosFacturas = DatosFacturas::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/datos_facturas/'.$datosFacturas->id,
            $editedDatosFacturas
        );

        $this->assertApiResponse($editedDatosFacturas);
    }

    /**
     * @test
     */
    public function test_delete_datos_facturas()
    {
        $datosFacturas = DatosFacturas::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/datos_facturas/'.$datosFacturas->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/datos_facturas/'.$datosFacturas->id
        );

        $this->response->assertStatus(404);
    }
}
