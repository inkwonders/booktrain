<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetalleResumenPedido;

class DetalleResumenPedidoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detalle_resumen_pedidos', $detalleResumenPedido
        );

        $this->assertApiResponse($detalleResumenPedido);
    }

    /**
     * @test
     */
    public function test_read_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detalle_resumen_pedidos/'.$detalleResumenPedido->id
        );

        $this->assertApiResponse($detalleResumenPedido->toArray());
    }

    /**
     * @test
     */
    public function test_update_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();
        $editedDetalleResumenPedido = DetalleResumenPedido::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detalle_resumen_pedidos/'.$detalleResumenPedido->id,
            $editedDetalleResumenPedido
        );

        $this->assertApiResponse($editedDetalleResumenPedido);
    }

    /**
     * @test
     */
    public function test_delete_detalle_resumen_pedido()
    {
        $detalleResumenPedido = DetalleResumenPedido::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detalle_resumen_pedidos/'.$detalleResumenPedido->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detalle_resumen_pedidos/'.$detalleResumenPedido->id
        );

        $this->response->assertStatus(404);
    }
}
