<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DireccionEntrega;

class DireccionEntregaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/direccion_entregas', $direccionEntrega
        );

        $this->assertApiResponse($direccionEntrega);
    }

    /**
     * @test
     */
    public function test_read_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/direccion_entregas/'.$direccionEntrega->id
        );

        $this->assertApiResponse($direccionEntrega->toArray());
    }

    /**
     * @test
     */
    public function test_update_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();
        $editedDireccionEntrega = DireccionEntrega::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/direccion_entregas/'.$direccionEntrega->id,
            $editedDireccionEntrega
        );

        $this->assertApiResponse($editedDireccionEntrega);
    }

    /**
     * @test
     */
    public function test_delete_direccion_entrega()
    {
        $direccionEntrega = DireccionEntrega::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/direccion_entregas/'.$direccionEntrega->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/direccion_entregas/'.$direccionEntrega->id
        );

        $this->response->assertStatus(404);
    }
}
