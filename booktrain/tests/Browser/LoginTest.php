<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $user = User::where('email', 'josue@inkwonders.com')->first();
            $prev_hash = $user->password;
            $user->upadate(['password' => Hash::make('inkwonders')]);

            $browser->visit('/login')
                    ->type('email', 'josue@inkwonders.com')
                    ->type('password', 'inkwonders')
                    ->press('ENTRAR')
                    ->assertPathIs('/inicio');

            $user->update(['password', $prev_hash]);
        });
    }

    public function testUserLogged() {
        $this->browse(function (Browser $browser) {
            $browser->assertAuthenticatedAs(
                User::where('email', 'josue@inkwonders.com')->first()
            );
        });
    }
}
