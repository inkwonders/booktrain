<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
class ReferenciaPedido extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'referencias_pedidos';

    public $fillable = [
        'pedido_id',
        'referencia',
        'tipo',
        'dias_vigencia',
        'response',
        'pagado',
        'fecha_pago',
        'notificado',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'pedido_id' => 'integer',
        'referencia' => 'string',
        'tipo' => 'string',
        'dias_vigencia' => 'integer',
        'response' => 'string'
    ];

    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }

    // Comprobamos que la referencia no tenga valores duplicados
    public function referenciasDuplicadas() {
        return $this->hasMany(ReferenciaPedido::class, 'referencia', 'referencia');
    }

    public function consultaEstadoOpenPay() {
        if($this->pedido->metodoPago()->where('proveedor', 'OPENPAY')->count() == 0){
            Log::error('ReferenciaPedido@consultaEstadoOpenPay', ['id_pedido' => $this->pedido_id, 'id_referencia' => $this->id, 'msg' => 'El pedido no pertenece a OpenPay']);
            return null;
        }

        if(env('OPENPAY_ENABLED', 0) == 0){
            Log::debug('ReferenciaPedido@consultaEstadoOpenPay', ['msg' => 'La pasarela OpenPay no está habilitada']);
            return null;
        }

        $transactionTokenId = $this
            ->pedido
            ->pagos()
            ->where('source', $this->referencia)
            ->first()
            ->transactionTokenId;

        $cargo = app('App\Http\Controllers\OpenPayController')
            ->getCharge( $transactionTokenId );

        return $cargo;
    }

    public function consultaEstadoNetpay() {
        if($this->tipo != 'TIENDA')
            return null;

        $respuesta_previa = json_decode($this->response);

        $registro_pago = null;
        $transactionTokenId = null;

        if( $respuesta_previa ) {
            $registro_pago = $this->pedido
                ->pagos()
                ->where('transactionTokenId', $respuesta_previa->transactionTokenId)
                ->whereRaw('UNIX_TIMESTAMP(fecha_comprobacion) > (UNIX_TIMESTAMP() - 43200)')
                ->first();

            $transactionTokenId = $respuesta_previa->transactionTokenId;
        }

        if($registro_pago)
            return json_decode($registro_pago->raw_comprobacion);

        Log::info("Consultado estado de la referencia {$this->id} con el token {$transactionTokenId}");

        $base_uri = env('NETPAY_BASE_URI_SANDBOX', 'https://gateway-154.netpaydev.com/');
        if (env('NETPAY_SANDBOX_MODE', 1) == 0)
            $base_uri = env('NETPAY_BASE_URI', 'https://suite.netpay.com.mx/');

        $client = new Client(
            ['base_uri' => $base_uri]
        );

        $netpay_sk = env('NETPAY_SK_SANDBOX', 'sk_netpay_lyNzonHFhwqoMHXfMFmOILqgZjAAjUVOjisfSkikPkrDA');
        if (env('NETPAY_SANDBOX_MODE', 1) == 0)
            $netpay_sk = env('NETPAY_SK', 'sk_netpay_FlZKagTbMzpKiSZnrUuAwQFBfHvORFkFSJbVjNPoayoIs');

        $raw_response = '';

        $response = $client->get("/gateway-ecommerce/v3/transactions/{$transactionTokenId}", [
            'headers' => [
                'Authorization' => $netpay_sk,
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json'
            ]
        ]);

        $raw_response = $response->getBody()->getContents();
        $response = json_decode($raw_response);

        $this->pedido->pagos()->where('transactionTokenId', $transactionTokenId)->update([
            'raw_comprobacion' => $raw_response,
            'fecha_comprobacion' => date('Y-m-d H:i:s')
        ]);

        return $response;
    }

/**
     * Valida si el pago fue realizado correctamente con NETPAY
     * Respuestas:
     * 0 Pago exitoso
     * 1 Pago no exitoso
     * 2 Error al consultar el status
     * 3 La comprobación no aplica a este pago
     */
    public function validarNetpay() : int {
        $raw_error = null;
        if($this->tipo == 'TIENDA') {
            try {
                $response = $this->consultaEstadoNetpay();

                if($response->status == 'DONE') {
                    $this->pedido->pagos()->create([
                        'amount'                => $response->amount,
                        'source'                => $this->referencia,
                        'transactionTokenId'    => $response->transactionTokenId,
                        'status'                => 'SUCCESS',
                        'raw'                   => json_encode($response),
                        'origen'                => 'CRON'
                    ]);

                    $this->pedido->update([
                        'status' => 'PAGADO',
                        'metodo_pago' => 'TIENDA'
                    ]);

                    $this->update([
                        'pagado' => 1,
                        'fecha_pago' => Carbon::now()
                        //'response' => $raw_response
                    ]);

                    $this->pedido->sendPagoExitosoNotification();

                    // La actualización de notificación es independiente a la de pagado
                    $this->update([
                        'notificado' => 1
                    ]);

                    return 0;
                }

                if($response->status == 'IN_PROCESS') {
                    return 1;
                }

                if($response->status == 'EXPIRED') {
                    return 2;
                }

                Log::error("ReferenciaPedido@validarNetpay: Status desconocido", ['status' => $response->status]);
                return 3;
            } catch (\GuzzleHttp\Exception\ClientException $th) {
                $raw_error = $th->getResponse()->getBody()->getContents();

                Log::error("ReferenciaPedido@validarNetpay: Error al consultar el status de pago", ['mensaje' => $raw_error]);
            }

            return 4;
        } else{
           return 5;
        }
    }
}
