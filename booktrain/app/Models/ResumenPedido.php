<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="ResumenPedido",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pedido_id",
 *          description="pedido_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre_alumno",
 *          description="nombre_alumno",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="paterno_alumno",
 *          description="paterno_alumno",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="materno_alumno",
 *          description="materno_alumno",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtotal",
 *          description="subtotal",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ResumenPedido extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'resumen_pedidos';

    protected $appends = [
        'nombre_paquete'
    ];

    public $fillable = [
        'pedido_id',
        'nombre_alumno',
        'paterno_alumno',
        'materno_alumno',
        'subtotal',
        'paquete_id',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pedido_id' => 'integer',
        'nombre_alumno' => 'string',
        'paterno_alumno' => 'string',
        'materno_alumno' => 'string',
        'subtotal' => 'double',
        'paquete_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getSubtotalAttribute($value) {
        return $this->detalles->sum('total');
    }

    public function getNombrePaqueteAttribute() {
        return $this->paquete->nombre;
    }

    public function getNombreCompletoAlumnoAttribute() {
        return "{$this->nombre_alumno} {$this->paterno_alumno} {$this->materno_alumno}";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function pedido()
    {
        return $this->belongsTo(\App\Models\Pedido::class, 'pedido_id');
    }

    public function detallesResumenPedidos()
    {
        return $this->hasMany(\App\Models\DetalleResumenPedido::class, 'resumen_pedidos_id');
    }

    public function detalles()
    {
        return $this->hasMany(\App\Models\DetalleResumenPedido::class, 'resumen_pedidos_id');
    }

    public function paquete()
    {
        return $this->belongsTo(\App\Models\Paquete::class, 'paquete_id');
    }
}
