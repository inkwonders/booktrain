<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
/**
 * @SWG\Definition(
 *      definition="Libro",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="editorial",
 *          description="editorial",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="edicion",
 *          description="edicion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="isbn",
 *          description="isbn",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="stock",
 *          description="stock",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="precio",
 *          description="precio",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="activo",
 *          description="activo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Libro extends Model
{

    use HasFactory;
    use SoftDeletes;
    use HasRelationships;

    public $table = 'libros';

    public $fillable = [
        'nombre',
        'editorial',
        'edicion',
        'isbn',
        'clave_producto',
        'objeto_impuesto',
        // 'stock',
        // 'precio',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'editorial' => 'string',
        'edicion' => 'string',
        'isbn' => 'string',
        'clave_producto'=> 'string',
        'objeto_impuesto'=> 'string',
        // 'stock' => 'integer',
        // 'precio' => 'double',
        'activo' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getNombreAttribute($value) {
        return trim($value);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function (Libro $item) {
            if($item->editorial == '') $item->attributes['editorial'] = '';
            if($item->edicion == '') $item->attributes['edicion'] = '';
        });
    }

    public function colegios() {
        return $this->belongsToMany(Colegio::class, 'libros_colegios', 'libro_id', 'colegio_id')->withPivot('precio', 'obligatorio', 'activo', 'stock', 'punto_reorden', 'bajo_pedido', 'paquete_id');
    }

    public function paquetes() {
        return $this->belongsToMany(Paquete::class, 'libros_colegios', 'libro_id', 'paquete_id')->withPivot('precio', 'obligatorio', 'activo', 'stock', 'punto_reorden', 'bajo_pedido', 'paquete_id', 'colegio_id');
    }

    public function detalleResumen() {
        return $this->hasMany(DetalleResumenPedido::class, 'libro_id');
    }

    public function detalle() {
        return $this->hasMany(DetalleResumenPedido::class, 'libro_id');
    }

    public function resumen() {
        return $this->hasManyThrough(
            ResumenPedido::class,
            DetalleResumenPedido::class,
            'libro_id', // detalles_resumen_pedidos.libro_id
            'id', // resumen_pedidos.id
            'id', // libros.id
            'resumen_pedidos_id'
        );
    }

    public function pedidos() {
        return $this->hasManyDeep(
            Pedido::class,
            [
                DetalleResumenPedido::class,
                ResumenPedido::class
            ],[
                'libro_id', // detalles_resumen_pedidos.libro_id
                'id', // resumen_pedidos.id
                'id' // pedidos.id
            ],[
                'id', // libros.id
                'resumen_pedidos_id', // detalles_resumen_pedidos.resumen_pedidos_id
                'pedido_id' // resumen_pedidos.pedido_id
            ]);
    }

    public function scopeActivo($query) {
        return $query->where('libros_colegios.activo', 1);
    }
}
