<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegistroActividad extends Model
{
    protected $table = 'activity_log';

    public function usuario() {
        return $this->morphInstanceTo(User::class, 'activity_log', 'causer_type', 'causer_id', 'id');
    }

    public function registrable() {
        return $this->morphTo('causer');
    }
}
