<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Scopes\AncientScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{
    use HasFactory;

    public $table = 'password_resets';

    protected static function booted()
    {
        // Agrega esta consulta a todas las instancias de este modelo
        static::addGlobalScope('valids', function (Builder $builder) {
            $builder->where(
                'created_at',
                '>',
                now()->subMinutes(
                    env('PASSWORD_RESET_LIFE', 10)
                )
            );
        });
    }
}
