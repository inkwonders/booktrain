<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="TicketEntradaArchivo",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="entrada_id",
 *          description="entrada_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="archivo_id",
 *          description="archivo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="archivo_nombre_original",
 *          description="archivo_nombre_original",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="orden",
 *          description="orden",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TicketEntradaArchivo extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'ticket_entradas_archivos';

    public $fillable = [
        'entrada_id',
        'archivo_nombre_original',
        'url',
        'tipo_archivo',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'entrada_id' => 'integer',
        'archivo_nombre_original' => 'string',
        'url' => 'string',
        'tipo_archivo' => 'string',
        'orden' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function ticketEntrada()
    {
        return $this->hasMany(\App\Models\TicketEntrada::class, 'id', 'entrada_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    // public function ticketArchivo()
    // {
    //     return $this->hasOne(\App\Models\TicketArchivo::class, 'id', 'archivo_id');
    // }
}