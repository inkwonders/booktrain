<?php

namespace App\Models;

use App\Events\ColegioCreado;
use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Colegio",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="codigo",
 *          description="codigo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="logo",
 *          description="logo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activo",
 *          description="activo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Colegio extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'colegios';

    protected $dispatchesEvents = [
        'created' => ColegioCreado::class,
    ];

    // public static function boot()
    // {
    //     parent::boot();

    //     static::creating(function (Colegio $item) {
    //         $item->configuracion = Configuracion::whereEtiqueta('json_base_pago_colegio')->first()->valor;
    //     });
    // }

    public $fillable = [
        'codigo',
        'nombre',
        'logo',
        'activo',
        'configuracion',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'string',
        'nombre' => 'string',
        'logo' => 'string',
        // 'configuracion' => 'string',
        'activo' => 'integer'
    ];

    /**
     * Validation
     * @var array
     */
    public static $rules = [];

    public function pedidos()
    {
        return $this->hasMany(Pedido::class, 'colegio_id');
    }

    public function secciones()
    {
        return $this->hasMany(SeccionColegio::class, 'colegio_id');
    }

    public function libros()
    {
        return $this->belongsToMany(Libro::class, 'libros_colegios', 'colegio_id', 'libro_id')->withPivot('precio', 'obligatorio', 'activo', 'stock', 'punto_reorden', 'bajo_pedido', 'paquete_id')->wherePivot('deleted_at', null);
    }

    public function paquetes()
    {
        return $this->belongsToMany(Paquete::class, 'libros_colegios', 'colegio_id', 'paquete_id')->withPivot('precio', 'obligatorio', 'activo', 'stock', 'punto_reorden', 'bajo_pedido', 'libro_id', 'paquete_id');
    }

    public function notificaciones()
    {
        return $this->hasMany(Notificacion::class, 'colegio_id');
    }

    public function niveles()
    {
        return $this->hasManyThrough(NivelColegio::class, SeccionColegio::class, 'colegio_id', 'seccion_id');
    }

    public function catalogoLibros()
    {
        return $this->hasMany(LibrosColegios::class, 'colegio_id')->whereNull('paquete_id');
    }

    public function cajas() {
        return $this->hasMany(Caja::class, 'colegio_id');
    }

    /**
     * The roles that belong to the Colegio
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function direccionesEntrega()
    {
        return $this->belongsToMany(Direccion::class, 'direccion_entregas', 'colegio_id', 'direccion_id')->withPivot('tipo', 'activo');
    }

    public function direcciones()
    {
        return $this->belongsToMany(Direccion::class, 'direccion_entregas', 'colegio_id', 'direccion_id')->withPivot('tipo');
    }

    public function metodosPago()
    {
        return $this->belongsToMany(MetodoPago::class, 'metodos_pagos_colegios', 'colegio_id', 'metodo_pago_id')
            ->withPivot('activo', 'contexto');
    }

    public function formasPago()
    {
        return $this->belongsToMany(FormaPago::class, 'metodos_pagos_formas_pagos_colegios', 'colegio_id', 'forma_pago_id')
            ->withPivot('activo', 'meses', 'comision', 'minimo');
    }

    public function direccionesEntregaColegio($id)
    {
        // return $this->belongsToMany(Direccion::where('colegio_id', $id));
        return DireccionEntrega::where('colegio_id', $id)->get();
    }

    public function movimientosAlmacen()
    {
        return $this->hasMany(MovimientoAlmacen::class, 'colegio_id');
    }

    public function configuraciones()
    {
        return $this->hasMany(Configuracion::class, 'colegio_id');
    }

    public function config($etiqueta)
    {
        if (($config = $this->configuraciones()->where('etiqueta', $etiqueta)->first()))
            return $config->valor;

        return '';
    }

    public function scopeActivos($query) {
        return $query->where('colegios.activo', 1);
    }

    public function scopeActivo($query) {
        return $query->where('colegios.activo', 1);
    }

    public function getConfiguracionAttribute($value)
    {
        if ($this->configuraciones->count() == 0) return $value;

        $configuracion = collect();

        $configuracion->put('nombre_cuenta', $this->config('nombre_cuenta'));
        $configuracion->put('nombre_banco', $this->config('nombre_banco'));
        $configuracion->put('numero_cuenta', $this->config('numero_cuenta'));
        $configuracion->put('clabe', $this->config('clabe'));
        $configuracion->put('ventanilla_otros_bancos', $this->config('ventanilla_otros_bancos'));
        $configuracion->put('portal_bbva', $this->config('portal_bbva'));
        $configuracion->put('portal_otros_bancos', $this->config('portal_otros_bancos'));
        $configuracion->put('numero_contrato', $this->config('numero_contrato'));
        $configuracion->put('numero_contrato_clabe', $this->config('numero_contrato_clabe'));

        $configuracion->put('costo_envio', intval($this->config('costo_envio')));
        $configuracion->put('envio_domicilio_disponible', intval($this->config('envio_domicilio_disponible')));

        $configuracion->put(
            'metodos_pago',
            $this->metodosPago()->wherePivot('contexto', 'ONLINE')
            ->get()
            ->map(function ($metodo_pago) {
                $formas_pago = $metodo_pago->formasPago()->wherePivot('colegio_id', $metodo_pago->pivot->colegio_id)->get()->transform(function ($forma_pago) {
                    if ($forma_pago->pivot->activo == 1)
                        return [
                            //"nombre":"Pago en una sola exhibici\u00f3n","valor":"PUE","meses":0,"comision":2,"minimo":0
                            'nombre' => $forma_pago->descripcion,
                            'valor' => $forma_pago->codigo,
                            'meses' => $forma_pago->pivot->meses,
                            'comision' => $forma_pago->pivot->comision,
                            'minimo' => $forma_pago->pivot->minimo
                        ];
                })->whereNotNull();

                if ($metodo_pago->pivot->activo == 1 && $formas_pago->count() > 0)
                    return [
                        'nombre' => $metodo_pago->descripcion,
                        'valor' => $metodo_pago->codigo,
                        'formas_pago' => $formas_pago
                    ];
            })->whereNotNull()
        );

        return $configuracion;
    }

    public function getConfiguracionVentaMovilAttribute() {
        $configuracion = collect();

        $configuracion->put('nombre_cuenta', $this->config('nombre_cuenta'));
        $configuracion->put('nombre_banco', $this->config('nombre_banco'));
        $configuracion->put('numero_cuenta', $this->config('numero_cuenta'));
        $configuracion->put('clabe', $this->config('clabe'));
        $configuracion->put('ventanilla_otros_bancos', $this->config('ventanilla_otros_bancos'));
        $configuracion->put('portal_bbva', $this->config('portal_bbva'));
        $configuracion->put('portal_otros_bancos', $this->config('portal_otros_bancos'));
        $configuracion->put('numero_contrato', $this->config('numero_contrato'));
        $configuracion->put('numero_contrato_clabe', $this->config('numero_contrato_clabe'));

        $configuracion->put(
            'metodos_pago',
            $this->metodosPago()->wherePivot('contexto', 'PRESENCIAL')->get()->map(function ($metodo_pago) {
                $formas_pago = $metodo_pago->formasPago()->wherePivot('colegio_id', $metodo_pago->pivot->colegio_id)->get()->transform(function ($forma_pago) {
                    if ($forma_pago->pivot->activo == 1)
                        return [
                            //"nombre":"Pago en una sola exhibici\u00f3n","valor":"PUE","meses":0,"comision":2,"minimo":0
                            'id' => $forma_pago->id,
                            'nombre' => $forma_pago->descripcion,
                            'valor' => $forma_pago->codigo,
                            'meses' => $forma_pago->pivot->meses,
                            'comision' => $forma_pago->pivot->comision,
                            'minimo' => $forma_pago->pivot->minimo
                        ];

                    return null;
                })->whereNotNull()->values();

                if ($metodo_pago->pivot->activo == 1 && $formas_pago->count() > 0)
                    return [
                        'id' => $metodo_pago->id,
                        'nombre' => $metodo_pago->descripcion,
                        'valor' => $metodo_pago->codigo,
                        'formas_pago' => $formas_pago
                    ];

                return null;
            })->whereNotNull()->values()
        );

        return $configuracion;
    }
}
