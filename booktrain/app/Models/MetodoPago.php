<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="MetodoPago",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="codigo",
 *          description="codigo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class MetodoPago extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'metodos_pagos';

    protected static function booted()
    {
        static::addGlobalScope('disponibles', function ($builder) {
            $builder->where('metodos_pagos.disponible', 1);
        });
    }

    public $fillable = [
        'codigo',
        'descripcion',
        'orden',
        'proveedor',
        'disponible',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'codigo' => 'string',
        'descripcion' => 'string',
        'orden' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function formasPago() {
        return $this->belongsToMany(FormaPago::class, 'metodos_pagos_formas_pagos_colegios', 'metodo_pago_id', 'forma_pago_id')
            ->withPivot('meses', 'comision', 'minimo', 'activo', 'colegio_id');
    }

    public function pedidos() {
        return $this->hasMany(Pedido::class, 'metodo_pago_id');
    }
}
