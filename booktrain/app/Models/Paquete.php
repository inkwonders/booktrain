<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="Paquete",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nivel_id",
 *          description="nivel_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activo",
 *          description="activo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Paquete extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'paquetes';

    public $fillable = [
        'nivel_id',
        'nombre',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nivel_id' => 'integer',
        'nombre' => 'string',
        'activo' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function nivelColegio()
    {
        return $this->belongsTo(NivelColegio::class, 'nivel_id', 'id');
    }

    public function nivel()
    {
        return $this->belongsTo(NivelColegio::class, 'nivel_id');
    }

    public function libros()
    {
        return $this->belongsToMany(Libro::class, 'libros_colegios', 'paquete_id', 'libro_id')->withPivot('precio', 'obligatorio', 'activo', 'stock', 'punto_reorden', 'bajo_pedido');
        //return $this->hasMany(LibrosColegios::class, 'paquete_id');
    }

    public function resumenes() {
        return $this->hasMany(ResumenPedido::class, 'paquete_id');
    }

}