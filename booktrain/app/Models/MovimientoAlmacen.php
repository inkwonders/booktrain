<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovimientoAlmacen extends Model
{
    use HasFactory;

    protected $table = 'movimientos_almacenes';

    protected $fillable = [
        'libro_id',
        'tipo_movimiento_almacen_id',
        'user_id',
        'cantidad',
        'cantidad_anterior',
        'descripcion',
        'comentarios'
    ];

    public static function boot() {
        parent::boot();

        static::created(function(MovimientoAlmacen $movimiento) {
            // Comprobar el punto de reorden y notificar en caso de ser necesario
        });
    }

    public function tipo() {
        return $this->belongsTo(TipoMovimientoAlmacen::class, 'tipo_movimiento_almacen_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function libro()
    {
        return $this->belongsTo(Libro::class, 'libro_id');
    }
}
