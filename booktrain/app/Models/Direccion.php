<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="DireccionEntrega",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pedido_id",
 *          description="pedido_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="calle",
 *          description="calle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="no_exterior",
 *          description="no_exterior",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="no_interior",
 *          description="no_interior",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="colonia",
 *          description="colonia",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cp",
 *          description="cp",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="estado_id",
 *          description="estado_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="municipio_id",
 *          description="municipio_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="referencia",
 *          description="referencia",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Direccion extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'direcciones';

    public $fillable = [
        'calle',
        'no_exterior',
        'no_interior',
        'colonia',
        'cp',
        'estado_id',
        'municipio_id',
        'referencia',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'calle' => 'string',
        'no_exterior' => 'string',
        'no_interior' => 'string',
        'colonia' => 'string',
        'cp' => 'integer',
        'estado_id' => 'integer',
        'municipio_id' => 'integer',
        'referencia' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    function getDireccionCompletaAttribute()
    {
        return  "$this->calle $this->no_exterior int. $this->no_interior  $this->colonia $this->cp, " . $this->municipio->descripcion . " " . $this->estado->descripcion;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'municipio_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function pedido()
    {
        return $this->hasOne(Pedido::class, 'pedido_id');
    }

    /** Por logica de BD regrea una coleccion aunque en la practica una direccion solo pertenece a un colegio */
    public function colegios()
    {
        return $this->belongsToMany(Colegio::class, 'direccion_entregas', 'direccion_id', 'colegio_id')->withPivot('tipo');
    }
}