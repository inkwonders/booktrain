<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioPaqueteria extends Model
{
    use HasFactory;

    protected $table = 'servicios_paqueteria';

    public static function boot()
    {
        parent::boot();

        static::creating(function (ServicioPaqueteria $item) {
            // Valor predeterminado para el campo activo
            if(!isset($item->activo)){
                $item->attributes['activo'] = false;
            }
        });
    }

    protected $fillable = [
        'nombre',
        'url',
        'activo'
    ];

    public function scopeActivo($query) {
        return $query->where('activo', 1);
    }

    public function envios() {
        return $this->hasMany(Envio::class, 'paqueteria_id');
    }
}
