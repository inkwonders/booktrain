<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Luecano\NumeroALetras\NumeroALetras;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Http\Controllers\FacturamaController;
/**
 * @SWG\Definition(
 *      definition="Pedido",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="folio",
 *          description="folio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="usuario_id",
 *          description="usuario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="colegio_id",
 *          description="colegio_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre_contacto",
 *          description="nombre_contacto",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="apellidos_contacto",
 *          description="apellidos_contacto",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="celular_contacto",
 *          description="celular_contacto",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tipo_entrega",
 *          description="tipo_entrega",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="factura",
 *          description="factura",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="metodo_pago_id",
 *          description="metodo_pago_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="forma_pago_id",
 *          description="forma_pago_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="terminos_condiciones",
 *          description="terminos_condiciones",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="subtotal",
 *          description="subtotal",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="comision",
 *          description="comision",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="total",
 *          description="total",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Pedido extends Model
{
    use HasFactory;
    use SoftDeletes;
    use HasRelationships;

    public $table = 'pedidos';

    public static function boot() {
        parent::boot();

        static::saved(function(Pedido $item) {
            if($item->status != 'CARRITO' && $item->folio == null)
                $item->setSerieFolio($item);
        });
    }

    public function setSerieFolio($item = null) {
        if(!$item) $item = $this;

        $folio = Configuracion::whereEtiqueta('folio')->first();
        $folio->increment('valor');

        $item->update([
            'folio' => $folio->valor,
            'serie' => Configuracion::whereEtiqueta('serie')->first()->valor
        ]);
    }

    public $fillable = [
        'serie',
        'folio',
        'status',
        'usuario_id',
        'usuario_venta_id',
        'colegio_id',
        'direccion_id',
        'metodo_pago_id',
        'forma_pago_id',
        'envio_id',
        'impreso',
        'nombre_contacto',
        'apellidos_contacto',
        'correo_contacto',
        'celular_contacto',
        'email_pedido',
        'tipo_entrega',
        'factura',
        'facturama_invoice_id',
        'facturama_status',
        'facturama_cancellation_response',
        'referencia_factura',
        'metodo_pago',
        'forma_pago',
        'terminos_condiciones',
        'subtotal',
        'comision',
        'total',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'serie' => 'string',
        'folio' => 'string',
        'status' => 'string',
        'usuario_id' => 'integer',
        'colegio_id' => 'integer',
        'direccion_id' => 'integer',
        'envio_id' => 'integer',
        'nombre_contacto' => 'string',
        'apellidos_contacto' => 'string',
        'celular_contacto' => 'string',
        'tipo_entrega' => 'integer',
        'factura' => 'integer',
        'facturama_invoice_id'=> 'string',
        'facturama_status'=> 'string',
        'facturama_cancellation_response'=> 'string',
        'metodo_pago' => 'string',
        'forma_pago' => 'string',
        'terminos_condiciones' => 'integer',
        'subtotal' => 'double',
        'comision' => 'double',
        'total' => 'double'
    ];

    /*
     *
     */
    protected $appends = [
        'libros_devueltos'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'contacto.nombre'               => 'required',
        'contacto.apellidos'            => 'required',
        'contacto.celular'              => 'required',
        'contacto.correo'               => 'sometimes|nullable|email',
        'tipo_entrega'                  => 'sometimes|required|between:1,3',
        'id_direccion_envio'            => 'sometimes|exists:direcciones,id',
        'facturacion_requerida'         => 'required|boolean',
        'facturacion.nombre'            => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.rfc'               => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.correo'            => 'exclude_if:facturacion_requerida,false|sometimes|email',
        'facturacion.cfdi'              => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.id_cfdi'           => 'exclude_if:facturacion_requerida,false|sometimes|exists:cfdis,id',
        'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.num_interior'      => 'sometimes|exclude_if:facturacion_requerida,false|nullable',
        'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
        'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',
        'terminos_aceptados'            => 'required|accepted',
        'forma_pago'                    => 'required|in:PUE,DIFERIDO3M,DIFERIDO6M,DIFERIDO9M',
        'metodo_pago'                   => 'required|in:CREDITO,DEBITO,AMEX,BANCO,TIENDA,EFECTIVO,TERMINAL,TRANSFERENCIA,BBVADEBITOVPOS,BBVACREDITOVPOS,BBVATARJETA,OPDEBITO,OPAMEX,OPMASTERCARD,OPEFECTIVO,OPSPEI,OPBANCO,OPTIENDA,OPQRALIPAY,OPQRCODI,OPIVR',
        'direccion.calle'               => 'sometimes|required_if:tipo_entrega,2',
        'direccion.no_exterior'         => 'sometimes|required_if:tipo_entrega,2',
        'direccion.no_interior'         => 'sometimes',
        'direccion.colonia'             => 'sometimes|required_if:tipo_entrega,2',
        'direccion.cp'                  => 'sometimes|required_if:tipo_entrega,2',
        'direccion.estado'              => 'sometimes|required_if:tipo_entrega,2',
        'direccion.municipio'           => 'sometimes|required_if:tipo_entrega,2',
        'direccion.municipio_id'        => 'sometimes|required_if:tipo_entrega,2|exists:municipios,id',
        'direccion.estado_id'           => 'sometimes|required_if:tipo_entrega,2|exists:estados,id',
        'direccion.referencia'          => 'sometimes',
        'tarjeta.token'                 => 'sometimes|required_with:tarjeta',
        'tarjeta.lastFourDigits'        => 'sometimes|required_with:tarjeta|size:4',
        'tarjeta.brand'                 => 'sometimes|required_with:tarjeta',
        'tarjeta.bank'                  => 'sometimes|required_with:tarjeta',
        'tarjeta.type'                  => 'sometimes|required_with:tarjeta',
        'tarjeta.country'               => 'sometimes|required_with:tarjeta',
        'tarjeta.scheme'                => 'sometimes|required_with:tarjeta',
        'tarjeta.cardPrefix'            => 'sometimes|required_with:tarjeta',
        'tarjeta.direccion.calle'       => 'sometimes|required_with:tarjeta',
        'tarjeta.direccion.cp'          => 'sometimes|required_with:tarjeta',
        'tarjeta.direccion.colonia'     => 'sometimes|required_with:tarjeta',
        'tarjeta.direccion.estado'      => 'sometimes|required_with:tarjeta',
        'tarjeta.direccion.municipio'   => 'sometimes|required_with:tarjeta'
    ];

    public function getSubtotalAttribute ($value) {
        return $this->resumenes->sum('subtotal');
    }

    public function getNombreAttribute($value) {
        return trim($value);
    }

    /**
     * En caso de pago con tarjeta: Débito, Crédito y AMEX
     * Si usa PUE, se utiliza la comisión general, si utiliza pagos diferidos la comisión se toma de la configuración particular;
     *
     * Si el pedido ya tiene un pago anteriormente, que haya requerido una comisión (Tarjeta de crédito) y se intenta un pago en el que no aplica una comisión (referencia de banco/tienda), debe quitar la comisión del pedido
     *
     * Antes de leer la comisión del pedido debe de cambiarse su atributo 'metodo_pago' por el valor correcto
     */
    public function getComisionAttribute($value) {
        // Si el pedido tiene una forma de pago en una exibición y el método de pago es BANCO o TIENDA, la comisión es CERO
        if($this->forma_pago == 'PUE' && in_array($this->metodo_pago, ['BANCO', 'TIENDA'])) {
            // Si el pedido ya está pagado y tuvo un cargo de comisión, se regresa ese valor, en caso contrario un CERO
            if($this->attributes['comision'] > 0 && $this->attributes['status'] == 'PAGADO')
                return $this->attributes['comision'];
            //return 0;
        }

        if($this->forma_pago != null && $this->metodo_pago != null) {
            //$forma_pago = $this->colegio->metodosPago()->where('codigo', $this->metodo_pago)->first()->formasPago()->where('codigo', $this->forma_pago)->first();
            // Ignoramos si el metodo de pago no está disponible
            $metodo_pago = MetodoPago::withOutGlobalScope('disponibles')->where('codigo', $this->metodo_pago)->first();

            $forma_pago = $this
                ->colegio
                ->formasPago()
                ->wherePivot('metodo_pago_id',
                    $metodo_pago->id
                )
                ->where('codigo', $this->forma_pago)
                ->first();

            // Hasta correr seeder AsignarIDMetodoPagoYFormaPagoEnPedidos
            //$forma_pago = $this->colegio->formasPago()->wherePivot('metodo_pago_id', $this->metodo_pago_id)->wherePivot('forma_pago_id', $this->forma_pago_id)->first();

            // Se cobra la comisión del pedido contemplando el subtotal y el costo de envío si se eligió el tipo de entrega a domicilio
            // if($this->tipo_entrega == 2)
            //     $comision = ($forma_pago->pivot->comision / 100) * ($this->subtotal + $this->colegio->config('costo_envio'));
            // else
            //     $comision = ($forma_pago->pivot->comision / 100) * $this->subtotal;

            $subtotal = 0;

            foreach ($this->resumenes as $resumen) {
                foreach ($resumen->detalles as $detalle) {
                    $subtotal += $detalle->precio_libro_original * $detalle->cantidad;
                }
            }

            if($this->tipo_entrega == 2){
                $comision = ($forma_pago->pivot->comision / 100) * ($subtotal + $this->colegio->config('costo_envio'));
            }else{

                // if($forma_pago->pivot->comision == 0 || empty($forma_pago->pivot->comision)){
                //     $comision = 0;
                // }else{
                //     $comision = ($forma_pago->pivot->comision / 100) * $subtotal;
                // }

                // $forma_pago = $this->colegio->formasPago()->wherePivot('metodo_pago_id', $this->metodo_pago_id)->wherePivot('forma_pago_id', $this->forma_pago_id)->first();

                $comision = ($forma_pago->pivot->comision / 100) * $subtotal;

            }

            $this->attributes['comision'] = $comision;
            return $comision;
        }

        return 0;
    }

    /**
     * Valida si el pedido tiene inconsistencias en su información
     * Error 1: Tiene forma_pago != PUE y metodo_pago = (DEBITO, TIENDA, BANCO)
     * Error 2: El pedido se encuentra en estado CARRITO, ya tiene un folio asignado y no tiene ningún pago
     * Error 3: Los pedidos pagados en BANCO o TIENDA no deben tener comisión
     * Error 4: El pedido tiene un pago exitoso y sigue con status PROCESADO
     * Error 5: El pedido tiene cobrado el envio pero su tipo de entrega no es DOMICILIO (total - comision - subtotal = envio)
     * Error 6: El pedido no tiene un tipo de entrega DOMICILIO y tiene un envío registrado
     * Error 7: El pedido con metodo de pago TIENDA (Referencia de pago de NetPay) tiene más de 1 referencias de pago
     * Error 8: El pedido con metodo de pago TIENDA (Referencia de pago de NetPay) tiene 0 referencias de pago
     * Error 9: La referencia de pago del pedido tipo TIENDA no coincide con el pedido
     * Error 10: El pedido tiene una referencia marcada como pagada pero no tiene registros de pago
     * Error 11: El pedido tiene una referencia marcada como pagada pero no tiene registros de pago exitosos
     * Error 12: El pedido está marcado como pagado pero no tiene pagos exitosos
     * Error 13: El pedido tiene pagos cuya referencia no corresponde
     * Error 14: El pedido tiene pagos marcados como exitosos inválidos que son eventos de creación de referencias
     * Error 15: El pedido tiene pagos sin un origen
     * Error 16: El pedido esta pagado en Netpay, pero fue cancelado
     * Error 17: Pago expirado en Netpay pero el pago se registro manualmente
     */
    public function tieneErrores($exclusion = []) {
        $errores = collect();

        // $errores[] = (object)['codigo' => 'DEMO', 'descripcion' => 'Error demo'];

        if(! in_array(1, $exclusion))
            if( $this->forma_pago != 'PUE' && in_array($this->metodo_pago, ['DEBITO', 'BANCO', 'TIENDA']) )
                $errores[] = (object)['codigo' => 'ERR1', 'descripcion' => 'El método de pago no coincide con la forma de pago'];

        if(! in_array(2, $exclusion))
            if( $this->status == 'CARRITO' && $this->folio != null && $this->pagos->count() == 0 )
                $errores[] = (object)['codigo' => 'ERR2', 'descripcion' => 'El pedido tiene un folio asignado teniendo status CARRITO'];

        // if(! in_array(3, $exclusion))
        //     if( $this->comision > 0 && in_array($this->metodo_pago, ['BANCO', 'TIENDA']) )
        //          $errores[] = (object)['codigo' => 'ERR3', 'descripcion' => 'Los pedidos pagados en BANCO o TIENDA no deben tener comisión'];

        if(! in_array(4, $exclusion))
            if( $this->status == 'PROCESADO' && $this->pagos()->exitoso()->count() > 0 )
                $errores[] = (object)['codigo' => 'ERR4', 'descripcion' => 'El pedido sigue con status PROCESADO pero tiene un pago exitoso'];

        if(! in_array(5, $exclusion))
            if( ($this->total - ($this->comision + $this->subtotal)) > 0 && $this->tipo_entrega != 2 )
                $errores[] = (object)['codigo' => 'ERR5', 'descripcion' => 'El pedido tiene cobrado el envio pero su tipo de entrega no es DOMICILIO'];

        if(! in_array(4, $exclusion))
            if( $this->envio != null && $this->tipo_entrega != 2 )
                $errores[] = (object)['codigo' => 'ERR4', 'descripcion' => 'El pedido tiene un envío registrado pero no tiene un tipo de entrega DOMICILIO'];

        if(! in_array(7, $exclusion))
            if( $this->metodo_pago == 'TIENDA' && $this->referencias->count() > 1 )
                $errores[] = (object)['codigo' => 'ERR7', 'descripcion' => 'El pedido con metodo de pago TIENDA tiene más de 1 referencias de pago'];

        if(! in_array(8, $exclusion))
        if($this->metodo_pago == 'TIENDA' && $this->status != 'CARRITO') {
            if($this->referencia == null)
                $errores[] = (object)['codigo' => 'ERR8', 'descripcion' => 'El pedido con metodo de pago TIENDA no tiene una referencia de pago'];
            else {
                if(! in_array(9, $exclusion))
                    if(! Str::contains($this->referencia->response, "PED-{$this->id}-") )
                        $errores[] = (object)['codigo' => 'ERR9', 'descripcion' => 'El pedido con metodo de pago TIENDA tiene una referencia de pago que no coincide con él'];

                    //

                if($this->referencia->pagado == 1)
                    if(! in_array(10, $exclusion))
                        if($this->pagos->count() == 0)
                            $errores[] = (object)['codigo' => 'ERR10', 'descripcion' => 'El pedido tiene una referencia marcada como pagada pero no tiene registros de pago'];
                        else {
                            if(! in_array(11, $exclusion))
                                if($this->pagos()->where('status', 'success')->count() < 1)
                                    $errores[] = (object)['codigo' => 'ERR11', 'descripcion' => 'El pedido tiene una referencia marcada como pagada pero no tiene registros de pago exitosos'];
                        }
            }
        }
        if(! in_array(12, $exclusion))
            if($this->pagos()->where('status', 'SUCCESS')->count() == 0 && in_array($this->status, ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']))
                $errores[] = (object)['codigo' => 'ERR12', 'descripcion' => 'El pedido está marcado como pagado pero no tiene pagos exitosos'];

        if(! in_array(13, $exclusion)) {
            $pagos_incorrectos = $this->pagos()
                ->whereNotIn('origen', ['MANUAL', 'CARGA MASIVA', 'VENTA MOVIL'])
                ->get()
                ->filter(function($pago) {
                    return ! Str::contains($pago->raw, "PED-{$pago->pedido_id}") && ! Str::contains($pago->raw, 'BAD_REQUEST');
                });

            if($pagos_incorrectos->count() > 0) {
                $errores[] = (object)['codigo' => 'ERR13', 'descripcion' => 'El pedido tiene pagos cuya referencia no corresponde', 'data' => $pagos_incorrectos];
            }
        }

        if(! in_array(14, $exclusion)) {
            $pagos_incorrectos = $this->pagos()->where('raw', 'like', '%"event":"cep.created"%')->where('status', 'SUCCESS')->get();

            if($pagos_incorrectos->count() > 0)
                $errores[] = (object)['codigo' => 'ERR14', 'descripcion' => 'El pedido tiene pagos marcados como exitosos inválidos que son eventos de creación de referencias', 'data' => $pagos_incorrectos];
        }

        if(! in_array(15, $exclusion))
            if($this->pagos()->where('origen', null)->count() > 0)
                $errores[] = (object)['codigo' => 'ERR15', 'descripcion' => 'El pedido tiene pagos sin un origen'];

        if(! in_array(16, $exclusion)) {
            $this->referencias()->where('tipo', 'TIENDA')->get()->each(function($referencia) use (&$errores){
                // Consultamos el estado de cada referencia
                if($this->metodoPago()->where('proveedor', 'NETPAY')->count() > 0) {
                    /**
                     * {
                     *  "transactionTokenId": "6cb226db-efbf-4c79-bf54-28364c6f0ddc",
                     *  "status": "DONE",
                     *  "orderId": "C401B18A004FB06BE053250115ACA816",
                     *  "merchantReferenceCode": "PED-588-PR-2420-60b9341cd6618",
                     *  "currency": null,
                     *  "amount": 612.0,
                     *  "timeIn": "2021-06-03T19:57:17.000+0000",
                     *  "timeOut": "2021-06-05T09:00:21.000+0000",
                     *  "responseCode": null,
                     *  "responseMsg": null,
                     *  "authCode": null,
                     *  "spanRouteNumber": null,
                     *  "cardHolderName": "Camila",
                     *  "billToEmail": "camythompsonwattpad@gmail.com",
                     *  "bankName": null,
                     *  "paymentMethod": "BANORTE_PAY",
                     *  "externalReference": "1721222215131357250",
                     *  "expireDate": "2021-06-05T19:57:17.000+0000",
                     *  "dayToExpirePayment": 2
                     * }
                     */
                    $estado_referencia_netpay = $referencia->consultaEstadoNetpay();
                    // Comprobamos que el pedido no esté cancelado con pagos exitosos en netpay
                    if($this->status == 'CANCELADO' && $estado_referencia_netpay->status == 'DONE') {
                        $errores[] = (Object) ['codigo' => 'ERR16', 'descripcion' => 'El pedido esta pagado en Netpay, pero fue cancelado'];
                    }

                    $pago_manual = $this->pagos()->where('origen', 'MANUAL')->first();

                    if($estado_referencia_netpay->status == 'EXPIRED' && $pago_manual) {
                        $errores[] = (Object) ['codigo' => 'ERR17', 'descripcion' => 'Expirado en Netpay pero el pago se registro manualmente por ' . $pago_manual->raw];
                    }
                }
            });
        }

        return $errores;
    }

    /**
     * Acciones de corrección para los errores identificados con el método tieneErrores()
     * ACC1:
     * ACC2:
     * ACC3:
     * ACC4:
     * ACC5:
     * ACC6:
     * ACC7:
     * ACC8:
     * ACC9:
     * ACC10: Se cambiará status de pagado en referencia a falso
     * ACC11: Se cambiará status de pagado en referencia a falso
     * ACC12: Comprueba que el pedido no haya sido enviado y si no tiene pagos exitosos cambia su estad a PROCESADO
     * ACC13: Comprueba los pagos creados con una llamada al API o un WEBHOOK que no corresponden al pedido y los elimina
     * ACC14: El registro de pago es una notificación de creación de referencia de pago en tienda de netpay, se eliminará
     * ACC15: El pedido tiene pagos sin un origen, se asignará un origen según su contenido
    */
    public function corregirErrores($acciones = [], $sujeto = null) {
        $resultados = [];

        if(in_array(1, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC1', 'descripcion' => '', 'success' => true];

        if(in_array(2, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC2', 'descripcion' => '', 'success' => true];

        if(in_array(3, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC3', 'descripcion' => '', 'success' => true];

        if(in_array(4, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC4', 'descripcion' => '', 'success' => true];

        if(in_array(5, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC5', 'descripcion' => '', 'success' => true];

        if(in_array(6, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC6', 'descripcion' => '', 'success' => true];

        if(in_array(7, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC7', 'descripcion' => '', 'success' => true];

        if(in_array(8, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC8', 'descripcion' => '', 'success' => true];

        if(in_array(9, $acciones))
            $resultados[] = (Object)['codigo' => 'ACC9', 'descripcion' => '', 'success' => true];

        if(in_array(10, $acciones)) {
            $resultados[] = (Object)['codigo' => 'ACC10', 'descripcion' => "Se cambiará status de pagado en referencia {$this->referencia->id} a falso", 'success' => true];
            $this->referencia->update([
                'pagado' => 0
            ]);
        }

        if(in_array(11, $acciones)) {
            $resultados[] = (Object)['codigo' => 'ACC11', 'descripcion' => "Cambiando status de pagado en referencia {$this->referencia->id} a falso", 'success' => true];
            $this->referencia->update([
                'pagado' => 0
            ]);
        }

        if(in_array(12, $acciones)) {
            //'CARRITO','PAGO EN REVISION','PAGO RECHAZADO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'
            if(in_array($this->status, ['CANCELADO', 'ENVIADO','ENTREGADO', 'ENTREGA PARCIAL','ENTREGA LIQUIDADA'])) {
                $resultados[] = (Object)['codigo' => 'ACC12', 'descripcion' => "El pedido {$this->id} ya fue entregado ($this->status), no puede cambiarse su estado", 'success' => false];
            }else{
                if($this->status != 'CARRITO' && $this->pagos()->where('status', 'SUCCESS')->count() == 0) {
                    $resultados[] = (Object)['codigo' => 'ACC12', 'descripcion' => "Cambiando estado del pedido {$this->id}, de {$this->status} a PROCESADO", 'success' => true];

                    $this->update([
                        'status' => 'PROCESADO'
                    ]);
                }
            }
        }

        if(in_array(13, $acciones)) {
            // Esta acción elimina los pagos que no corresponden al pedido
            $pagos_incorrectos = $this->pagos()->whereIn('origen', ['API', 'WEBHOOK'])->get()->filter(function($pago) {
                return ! Str::contains($pago->raw, "PED-{$pago->pedido_id}-");
            });

            $pagos_incorrectos->each(function($pago) {
                Log::debug("Pedido::corregirErrores", ['accion' => 13, 'descripcion' => "Eliminando pago {$pago->id}", 'data' => $pago]);
                $resultados[] = (Object)['codigo' => 'ACC13', 'descripcion' => "Eliminando pago {$pago->id}", 'data' => $pago, 'success' => true];
                $pago->delete();
            });
        }

        if(in_array(14, $acciones)) {
            $this->pagos()->where('raw', 'like', '%cep.created%')->get()->each(function($pago) use (&$resultados) {
                $resultados[] = (Object)['codigo' => 'ACC14', 'descripcion' => "El registro de pago {$pago->id} es una notificación de creación de referencia de pago en tienda de netpay, se eliminará", 'success' => true];
                $pago->delete();
            });
        }

        if(in_array(15, $acciones)) {
            $this->pagos()->where('origen', null)->get()->each(function($pago) use ($resultados) {
                $origen = '';

                if($pago->source == 'Registro de pago de forma manual')
                $origen = 'MANUAL';

            if(Str::length($pago->raw) < 145 && Str::length($pago->raw) > 140)
                $origen = 'CARGA MASIVA';

            if(Str::contains($pago->raw, '"type":"debit"'))
                $origen = 'API';

            if(Str::contains($pago->raw, '"httpStatus":"BAD_REQUEST"'))
                $origen = 'API';

            if(Str::contains($pago->raw, 'CardToken not found with token'))
                $origen = 'API';

            if($pago->source == 'venta_movil')
                $origen = 'VENTA MOVIL';

            if($pago->status_3ds != null)
                $origen = 'WEBHOOK';

            if(Str::startsWith($pago->source, 'token_'))
                $origen = 'API';

            if(Str::contains($pago->raw, '"transactionStatus":"IN_PROCESS"'))
                $origen = 'WEBHOOK';

            if(Str::contains($pago->raw, '"event":"cep.paid"'))
                $origen = 'WEBHOOK';

            // if(Str::contains($pago->raw, '"event":"cep.created"'))
            //     $origen = 'WEBHOOK';

            // Este tipo de origen cambia cuando el status sea recibido desde el webhook
            if($pago->status == 'REVIEW')
                $origen = 'API';

            if(Str::contains($pago->raw, '"paymentMethod":"BANORTE_PAY"'))
                $origen = 'WEBHOOK';

                $resultados[] = (Object)['codigo' => 'ACC15', 'descripcion' => "El pago {$pago->id} no tiene un origen, se asignará {$origen} según su contenido", 'success' => true];
                $pago->update(['origen' => $origen]);
            });
        }

        return $resultados;
    }

    public function getTotalAttribute($value) {
        $costo_envio = 0;

        //if($this->envio != null)
        if($this->envio != null && $this->tipo_entrega == 2)
            $costo_envio = $this->envio->costo;

        $this->total = $this->subtotal + $this->comision + $costo_envio;

        return round($this->subtotal + $this->comision + $costo_envio, 2);
    }

    public function getTotalLetraAttribute() {
        $total = $this->total;

        $formatter = new NumeroALetras();
        $formatter->apocope = true;
        $formatter->conector = '';

        $montoLetra = preg_replace('!\s+!', ' ',
            $formatter->toMoney($total, 0, 'PESOS')
        );

        // Dejamos solo los decimales
        $centavos = $total - floor($total);
        // Redondeamos a 2 decimales > 0.6999999 = 0.70
        $centavos = round($centavos, 2);
        // Multiplicamos por 100 > 0.73 = 73 centavos
        $centavos = 100 * $centavos;
        // Completamos los ceros a la izquierda
        $centavos = str_pad($centavos, 2, '0', STR_PAD_LEFT);

        return "$montoLetra $centavos/100 M.N.";
    }

    /**
     * Descuenta las unidades del stock al que pertenece el pedido y envía el correo correspondiente
     */
    public function procesar($enviar_correos = true) {
        // Salida por venta
        $tipo_movimiento_almacen = TipoMovimientoAlmacen::where('clave', 'SAL01')->first();

        $this->libros->each(function($libro) use ($tipo_movimiento_almacen) {
            // Obtenemos el stock actual
            $stock_actual = $this
                ->colegio
                ->libros()
                ->sharedLock()
                ->wherePivot('paquete_id', null)
                ->wherePivot('libro_id', $libro->id)
                ->first()->pivot->stock;

            // Guardamos el nuevo stock
            $this->colegio->libros()
                ->wherePivot('paquete_id', null)
                ->wherePivot('libro_id', $libro->id)
                ->lockForUpdate()
                ->updateExistingPivot($libro->id, [
                    'stock' => $stock_actual - $libro->cantidad
                ]);

            $monto = $libro->precio * $libro->cantidad;
            // Registramos los movimientos por libro en el almacén
            $this->colegio->movimientosAlmacen()->create([
                'libro_id' => $libro->id,
                'tipo_movimiento_almacen_id' => $tipo_movimiento_almacen->id,
                'user_id' => $this->usuario_id,
                'cantidad' => $libro->cantidad,
                'cantidad_anterior' => $stock_actual,
                'descripcion' => "{$tipo_movimiento_almacen->descripcion}, pedido {$this->id}, {$this->serie}-{$this->folio} por la cantidad de {$monto}"
            ]);
        });

        if($this->status == 'PAGADO' && $this->factura ==1){


            if(empty($this->facturama_invoice_id)){


        Log::debug('Pedido@procesar: verificamso el id de facturama antes de generar el comprobante, estatus: '.$this->facturama_invoice_id);

            $facturacion=new FacturamaController();
            $facturacion->generateCfdi($this->id);

            }


        }//crea factura

        if($enviar_correos) {
            try {
                if($this->status == 'PAGADO')
                    $this->sendPagoExitosoNotification();

                if($this->status == 'PROCESADO')
                    $this->user->sendPedidoRealizadoNotification($this);

                return true;
            } catch (\Throwable $th) {
                Log::error("Error al enviar el correo de notificación de pedido realizado", [$th->getMessage()]);
            }

            return false;
        }
    }

    // Cambiamos el estado del pedido y regresamos el stock
    public function cancelar() {
        if($this->status == 'PROCESADO' || $this->status == 'PAGO EN REVISION' || $this->status == 'PAGADO') {
            // Entrada por devolución
            $tipo_movimiento_almacen = TipoMovimientoAlmacen::where('clave', 'ENT04')->first();
            $this->libros->each(function($libro) use ($tipo_movimiento_almacen) {
                if($libro->devuelto == false) {
                    // Obtenemos el stock actual
                    $stock_actual = $this
                    ->colegio
                    ->libros()
                    ->sharedLock()
                    ->wherePivot('paquete_id', null)
                    ->wherePivot('libro_id', $libro->id)
                    ->first()->pivot->stock;

                    // Guardamos el nuevo stock
                    $this->colegio->libros()
                    ->wherePivot('paquete_id', null)
                    ->wherePivot('libro_id', $libro->id)
                    ->lockForUpdate()
                    ->updateExistingPivot($libro->id, [
                        'stock' => $stock_actual + $libro->cantidad
                    ]);

                    $monto = $libro->precio * $libro->cantidad;

                    // Registramos los movimientos por libro en el almacén
                    $this->colegio->movimientosAlmacen()->create([
                        'libro_id' => $libro->id,
                        'tipo_movimiento_almacen_id' => $tipo_movimiento_almacen->id,
                        'user_id' => $this->usuario_id,
                        'cantidad' => $libro->cantidad,
                        'cantidad_anterior' => $stock_actual,
                        'descripcion' => "{$tipo_movimiento_almacen->descripcion}, pedido {$this->id}, {$this->serie}-{$this->folio} por la cantidad de $ {$monto}"
                    ]);

                    // Marcamos el detalle del pedido como devuelto
                    DetalleResumenPedido::find($libro->detalle_id)->update([
                        'devuelto' => 1
                    ]);
                }
            });

            $this->update(['status' => 'CANCELADO']);
        }
    }

    /**
     * Elimina un pedido y sus relaciones no indispensables
     */
    public function eliminar() {
        $this->resumenes()->each(function($resumen) {
            $resumen->detalles()->each(function($detalles) {
                $detalles->delete();
            });

            $resumen->delete();
        });

        $this->envio()->each(function($envio) {
            $envio->delete();
        });
        //por cada pedido:
        $this->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        //return $this->hasOne(User::class, 'id', 'usuario_id');
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function usuarioVenta()
    {
        return $this->belongsTo(User::class, 'usuario_venta_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function colegio()
    {
        //return $this->hasOne(Colegio::class, 'id', 'colegio_id');
        return $this->belongsTo(Colegio::class, 'colegio_id');
    }

    public function referencia()
    {
        return $this->hasOne(ReferenciaPedido::class, 'pedido_id');
    }

    // Para comprobar que no haya más de 1 referencia por pedido
    public function referencias() {
        return $this->hasMany(ReferenciaPedido::class, 'pedido_id');
    }

    public function pagos() {
        return $this->hasMany(RegistroPago::class, 'pedido_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function direccion()
    {
        // return $this->hasOne(Direccion::class, 'id', 'direccion_id');
        return $this->belongsTo(Direccion::class, 'direccion_id');
    }

    public function resumenes()
    {
        return $this->hasMany(ResumenPedido::class, 'pedido_id');
    }

    public function envio()
    {
        // return $this->hasOne(Envio::class, 'pedido_id');
        return $this->morphOne(Envio::class, 'enviable');
    }

    public function envios() {
        return $this->hasMany(Envio::class, 'pedido_id');
    }

    public function cupon() {
        return $this->hasOne(Cupon::class, 'pedido_id');
    }

    public function datosFactura() {
        return $this->hasOne(DatosFacturas::class, 'pedido_id');
    }

    public function cajas() {
        return $this->belongsToMany(Caja::class, 'pedidos_cajas', 'caja_id', 'pedido_id');
    }

    public function caja() {
        return $this->cajas()->first();
    }

    public function formaPago() {
        return $this->belongsTo(FormaPago::class, 'forma_pago_id');
    }

    public function metodoPago() {
        return $this->belongsTo(MetodoPago::class, 'metodo_pago_id');
    }

    public function getLibrosAttribute() {
        return $this->resumenes->pluck('detalles')->collapse()
        ->map(function($detalle) {
            $detalle->libro->cantidad = $detalle->cantidad;
            $detalle->libro->precio = $detalle->precio_libro;
            $detalle->libro->envio = $detalle->envio;
            $detalle->libro->devuelto = $detalle->devuelto;
            $detalle->libro->detalle_id = $detalle->id;
            $detalle->libro->nivel = $detalle->resumen->paquete->nombre;
            $detalle->libro->nivel_id = $detalle->resumen->paquete->nivel_id;

            return $detalle->libro;
        });
    }

    public function detalles() {
        return $this->hasManyThrough(
            DetalleResumenPedido::class,
            ResumenPedido::class,
            'pedido_id',
            'resumen_pedidos_id'
        );
    }

    public function niveles() {
        return $this->hasManyDeep(
            NivelColegio::class,
            [
                'resumen_pedidos',
                'paquetes'
            ],
            [
                'pedido_id', // resumen_pedidos.pedido_id
                'id', // paquetes.id
                'id' // niveles_colegios.id
            ],
            [
                'id', // pedidos.id
                'paquete_id', // resumen_pedidos.paquete_id
                'nivel_id' // paquetes.nivel_id
            ]
        );
    }

    // Regresa la lista de envios que tiene el pedido y sus detalles de resumen en caso de que las hubiera
    public function getEnviosAttribute() {
        return collect([
                $this->envio
            ])
            ->concat(
                $this->resumenes->pluck('detalles')->collapse()->pluck('envio')
            )
            ->unique('guia')
            ->whereNotNull();
    }

    public function getDescripcionTipoEntregaAttribute() {
        switch ($this->tipo_entrega) {
            case 1: return 'Colegio';
            case 2: return 'Domicilio';
            case 3: return 'Pickup';
            case 4: return 'Digital';
            default: return 'N/A';
        }
    }

    public function getDescuentoDevolucionAttribute() {
        $descuento_devolucion = 0;
        $this->resumenes->pluck('detalles')->collapse()
        ->map(function($detalle) use (&$descuento_devolucion){
            $descuento_devolucion += ($detalle->cantidad * $detalle->descuento_devolucion);
        });
        return $descuento_devolucion;
    }

    public function getLibrosDevueltosAttribute() {
        $no_libros = 0;
        $this->resumenes->pluck('detalles')->collapse()
        ->map(function($detalle) use (&$no_libros){
            if ($detalle->devuelto == 1) {
                $no_libros += $detalle->cantidad;
            }
        });
        return $no_libros;
    }

    function sendPagoExitosoNotification() {
        try {

            // Registramos que ya se notifico al usuario en la referencia de pago en caso de tenerla
            if (!is_null($this->referencia))
                $this->referencia()->update(['notificado' => 1]);

            $this->user->notify(new \App\Notifications\PagoExitosoNotification($this));

            return true;
        } catch (\Throwable $th) {
            Log::error("sendPagoExitosoNotification: Error al notificar al usuario", ['message' => $th->getMessage()]);
        }
        return false;
    }

    function sendPedidoEnCaminoNotification() {
        try {
            // TODO Validar que el metodo de entrega sea 2 y que el pedido tenga un envío, paquetería y/o un número de guia antes de intentar enviar la notificación
            if($this->tipo_entrega == 2 && $this->envio != null) {
                $this->user->notify(new \App\Notifications\PedidoEnCaminoNotification($this));

                return true;
            }else{
                Log::error('Pedido@sendPedidoEnCaminoNotification: El pedido no tiene envíos que notificar', ['pedido' => $this->id, 'folio' => "{$this->serie}-{$this->folio}"]);

                return false;
            }
        } catch (\Throwable $th) {
            Log::error("sendPedidoEnCaminoNotification: Error al notificar al usuario", [
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'pedido' => $this->id,
                'folio' => "{$this->serie}-{$this->folio}"
                // 'trace' => $th->getTraceAsString()
            ]);
        }
        return false;
    }
    public function sendPagoExitosoVentaMovilNotification()
    {

        try {
            $this->user->notify(new \App\Notifications\PagoExitosoVentaMovilNotification($this));
        } catch (\Throwable $th) {
            Log::error("sendPagoExitosoVentaMovilNotification: Error al notificar al usuario", ['message' => $th->getMessage()]);
        }
    }

    /**
     * Devuelve el pago considerando el motor de pago utilizado
     */
    public function devolverPago($descripcion = '', $monto = null) : bool {
        $metodos_pago_validos = MetodoPago::whereIn('codigo', ['BBVADEBITOVPOS', 'BBVACREDITOVPOS'])->get()->pluck('id')->toArray();

        if($this->status == 'CANCELADO') {
            Log::error('Pedido@devolverPago', ['error' => "El pedido {$this->id} está cancelado y no puede ser reembolsado"]);

            return false;
        }

        if( in_array($this->metodo_pago_id,
            // Usamos como referencia el modelo de método de pago en vez del campo METODO_PAGO del pedido para en un futuro dejar de utilizarlo
            $metodos_pago_validos
        )) {
            if(in_array($this->status, ['PAGADO'])) {
                $pago = $this->pagos()->where('status', 'SUCCESS')->first();
                if($pago) {
                    if($monto < 0 || $monto > $this->total)
                        $monto = $this->total;

                    Log::debug('Pedido@devolverPago', ['pedido' => $this, 'pago' => $pago, 'descripcion' => $descripcion, 'monto' => $monto]);

                    DB::beginTransaction();
                    try {
                        $devolucion = app('App\Http\Controllers\BbvaController')->makeRefund($pago->transactionTokenId, $descripcion, $monto);
                        Log::debug('Pedido@devolverPago', ['devolucion' => $devolucion]);

                        if($devolucion) {
                            // Debe tener un nuevo estado ?
                            $pago->update([
                                'status' => 'FAILED'
                            ]);

                            // El estado del pedido debe cambiar ?
                            $this->update([
                                'status' => 'CANCELADO'
                            ]);

                            Log::debug('Pedido@devolverPago', ['Pedido devuelto']);
                            DB::commit();
                        }else{

                            DB::rollback();
                            return false;
                        }


                        return true;
                    } catch (\Throwable $th) {
                        Log::error('Pedido@devolverPago', ['error' => $th]);
                    }

                    DB::rollback();
                }
            }
        }else{
            Log::error('Pedido@devolverPago', ['error' => "El pedido {$this->id} no tiene un método de pago valido para devoluciones"]);
        }
        return false;
    }

    /**
     * Calculo del Algoritmo 10 BBVA
     *
     * validador online: http://cie.adquiramexico.com
     *
     */
    public function generarReferenciaAlgoritmo10BBVA($dias = 2) {

        $debug = false;

        $pedido = $this;

        // las posiciones libres son los digitos que se pueden usar para identificar la referencia (hasta 12)
        $posiciones_libres = $pedido->id;

        if ($debug) {
            print("posiciones_libres: $posiciones_libres <br>");
        }

        $now = Carbon::now()->addDays($dias);

        if ($debug) {
            print("expira: $now <br>");
        }

        /*
         * A) Calcular fecha condensada base 2014
         */

        //   1) Al año se le resta el valor base 2014
        $anio = $now->year - 2014;

        //   2) El resultado de la resta del punto anterior se multiplica por 372
        $op1 = $anio * 372;

        //   3) Al valor del mes se le resta una unidad.
        $mes = $now->month - 1;

        //   4) El resultado de la resta del punto 3 se multiplica por 31
        $op2 = $mes * 31;

        //   5) Al valor del día se le resta una unidad
        $op3 = $now->day - 1;

        //   6) Finalmente, para conformar la fecha condensada base 2014, se suman los resultados de los puntos 2, 4 y 5.
        $fecha_condensada = $op1 + $op2 + $op3;

        if ($debug) {
            print("fecha_condensada: $fecha_condensada <br>");
        }

        /*
         * B) Cálculo para obtener digito verificador de importe.
         */

        //  1) Se excluyen separadores de milésimas y decimales (comas y puntos).
        $importe = number_format((float)$pedido->total, 2, '.', '');

        if ($debug) {
            print("importe: $importe <br>");
        }

        $importe = (int) $importe * 100;

        //  2) A cada uno de los dígitos del punto 1, se asignan los siguientes ponderadores de derecha a izquierda 7, 3 y 1 de forma consecutiva
        //     siempre iniciando la secuencia con el número 7.
        $array = array_map('intval', str_split($importe));

        //     (si hubiera una tercia incompleta la completamos insertando ceros a la izquierda, ej. para el numero 9000, debe quedar 009|000)
        $len = count($array) - 1;
        $modulo = $len % 3;

        if ($modulo == 0) {
            //rellenamos dos huecos
            array_unshift($array, 0, 0);
        }

        if ($modulo == 1) {
            //rellenamos un hueco
            array_unshift($array, 0);
        }

        $sum_importes = 0;
        foreach ($array as $index => $digito){

            $modulo = $index % 3;

            switch ($modulo) {
                case 0:
                    $multiplicador = 1;
                    break;
                case 1:
                    $multiplicador = 3;
                    break;
                case 2:
                    $multiplicador = 7;
                    break;
            }

            if ($debug) {
                print("$digito - " . ($multiplicador) . "<br>");
            }

        //  3) Se multiplica cada uno de los dígitos por su correspondiente ponderador.
        //  4) Se suman los productos de las multiplicaciones del punto 3.
            $sum_importes += ($digito * $multiplicador);
        }

        //  5) El resultado de la suma se divide entre el divisor 10 y el residuo obtenido corresponderá al digito verificador del importe.
        $digito_verificador_importe = $sum_importes % 10;
        //print("digito_verificador_importe: $digito_verificador_importe <br>");

        /*
         * Cálculo para generar los dígitos verificadores de la referencia
         */

        //  1) Para calcular el digito verificador, se toma el contenido de la referencia, desde el primer digito, hasta donde concluye el digito verificador del importe,
        //     y se coloca el siguiente valor libre (un digito, en este ejemplo se coloca el valor 2). A cada valor se le asigna un ponderador 11, 13, 17, 19 y 23
        //     de derecha a izquierda sucesivamente.

        //     (concatenamos las posiciones libres, la fecha dondensada , el digito verificador de importe y un valor libre de un solo digito)
        $digito_libre = 2;
        $cadena = $posiciones_libres . $fecha_condensada . $digito_verificador_importe . $digito_libre;

        if ($debug) {
            print("cadena: $cadena <br>");
        }

        $array = array_map('intval', str_split($cadena ));

        //     (si hubiera una quinta incompleta la completamos insertando ceros a la izquierda, ej. para el numero 358421698764271872, debe quedar 00358|42169|87642|71872)
        $len = count($array) - 1;
        $modulo = $len % 5;

        if ($modulo == 0) {
            //rellenamos cuatro huecos
            array_unshift($array, 0, 0, 0, 0);
        }

        if ($modulo == 1) {
            //rellenamos tres huecos
            array_unshift($array, 0, 0, 0);
        }

        if ($modulo == 2) {
            //rellenamos dos huecos
            array_unshift($array, 0, 0);
        }

        if ($modulo == 3) {
            //rellenamos un hueco
            array_unshift($array, 0);
        }

        $sum_importes = 0;
        foreach ($array as $index => $digito){

            $modulo = $index % 5;

            switch ($modulo) {
                case 0:
                    $multiplicador = 23;
                    break;
                case 1:
                    $multiplicador = 19;
                    break;
                case 2:
                    $multiplicador = 17;
                    break;
                case 3:
                    $multiplicador = 13;
                    break;
                case 4:
                    $multiplicador = 11;
                    break;
            }

            if ($debug) {
                print("$digito - " . ($multiplicador) . "<br>");
            }

        //  2) Se multiplican los dígitos de la referencia y sus ponderadores:
        //  3) Se suman los resultados de la multiplicación del punto 2:
            $sum_importes += ($digito * $multiplicador);
        }

        //  4) Al resultado del punto 3 se le dividirá entre 97 y se tomará el residuo:
        $digito_verificador_referencia = $sum_importes % 97;

        //  5) Al residuo del punto 4 se le sumará 1, si el resultado es un dígito se le agregará un cero a la izquierda:
        $digito_verificador_referencia += 1;

        if ($digito_verificador_referencia < 10) {
            $digito_verificador_referencia = "0" . $digito_verificador_referencia;
        }

        if ($debug) {
            print("digito_verificador_referencia: $digito_verificador_referencia <br>");
        }

        $referencia = $posiciones_libres . $fecha_condensada .  $digito_verificador_importe . "2" .  $digito_verificador_referencia;

        if ($debug) {
            print("posiciones_libres:" . $posiciones_libres . ", fecha_condensada: " . $fecha_condensada .  ", digito_verificador_importe: ". $digito_verificador_importe . " -2-, digito_verificador_referencia: " .  $digito_verificador_referencia . "<br>");
            print("referencia: $referencia <br>");
        }

        return $referencia;
    }
}
