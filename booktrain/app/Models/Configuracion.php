<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Configuracion",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="etiqueta",
 *          description="etiqueta",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="valor",
 *          description="valor",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Configuracion extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'configuraciones';

    public $fillable = [
        'configuracion_padre_id',
        'colegio_id',
        'categoria',
        'etiqueta',
        'valor',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'etiqueta' => 'string'
        // 'valor' => 'object'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    public function configuracionPadre()
    {
        return $this->belongsTo(Configuracion::class, 'configuracion_padre_id');
    }

    public function configuraciones()
    {
        return $this->hasMany(Configuracion::class, 'configuracion_padre_id');
        // ->withDefault(['valor' => '', 'activo' => false, 'categoria_padre_id' => null, 'etiqueta' => '', 'categoria' => 'OTROS']);
    }

    public function scopeBase($query)
    {
        return $query->where('colegio_id', null);
    }

    public function scopeConfiguracion($query, $etiqueta)
    {
        return $query->where('etiqueta', $etiqueta)->first();
    }

    public function config($etiqueta)
    {
        if (($config = $this->configuraciones()->where('etiqueta', $etiqueta)->first()))
            return $config->valor;

        return '';
    }
}
