<?php

namespace App\Models;

use Eloquent as Model;
use App\Notifications\TicketCreatedDoneNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="TicketUsuario",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="usuario_id",
 *          description="usuario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="folio",
 *          description="folio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ticket_motivo_id",
 *          description="ticket_motivo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="calificacion",
 *          description="calificacion",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="comentario_calificacion",
 *          description="comentario_calificacion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fecha_calificacion",
 *          description="fecha_calificacion",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TicketUsuario extends Model
{
    use HasFactory;
    use Notifiable;
    use SoftDeletes;

    public $table = 'ticket_usuarios';

    public $fillable = [
        'usuario_id',
        'folio',
        'status',
        'titulo',
        'ticket_motivo_id',
        'descripcion',
        'calificacion',
        'comentario_calificacion',
        'fecha_calificacion',
        'telefono',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'usuario_id' => 'integer',
        'folio' => 'string',
        'status' => 'integer',
        'titulo' => 'string',
        'ticket_motivo_id' => 'integer',
        'calificacion' => 'integer',
        'fecha_calificacion' => 'datetime',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(\App\Models\User::class, 'id', 'usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function ticketMotivo()
    {
        return $this->hasOne(\App\Models\TicketMotivo::class, 'id', 'ticket_motivos_id');
    }

    public function ticketArchivos()
    {
        return $this->hasMany(\App\Models\TicketArchivo::class, 'ticket_usuario_id', 'id');
    }

    public function archivos()
    {
        return $this->hasMany(\App\Models\TicketArchivo::class, 'ticket_usuario_id', 'id');
    }

    public function entradas()
    {
        return $this->hasMany(\App\Models\TicketEntrada::class, 'ticket_usuario_id', 'id');
    }

    public function textoEstatus($status)
    {
        $txt = "";
        $clase = "";

        switch ($status) {
            case '0':
                $txt = "Cerrado";
                $clase = "cerrado";
                break;
            case '1':
                $txt = "Nuevo";
                $clase = "nuevo";
                break;
            case '2':
                $txt = "Respuesta Cliente";
                $clase = "cliente";
                break;
            case '3':
                $txt = "Respuesta Soporte";
                $clase = "soporte";
                break;
            default:
                # code...
                break;
        }

        return array('clase' => $clase, 'texto' => $txt);
    }

    // public function sendTicketCreatedSuccessNotification()
    // {
    //     $this->notify(new \App\Notifications\TicketCreatedDoneNotification);
    // }

}