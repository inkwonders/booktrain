<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Backpack\CRUD\app\Models\Traits\CrudTrait; // <------------------------------- this one
use Spatie\Permission\Traits\HasRoles; // <---------------------- and this one
use App\Notifications\TicketCreatedDoneNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable  implements MustVerifyEmail
{
    use HasFactory, Notifiable, CrudTrait, HasRoles, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'apellidos',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pedidos()
    {
        return $this->hasMany(Pedido::class, 'usuario_id');
    }

    public function ventas() {
        return $this->hasMany(Pedido::class, 'usuario_venta_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function datosFacturacion()
    {
        return $this->hasMany(DatosFacturas::class, 'user_id');
    }

    public function tickets()
    {
        return $this->hasMany(TicketUsuario::class, 'usuario_id');
    }

    public function entradas()
    {
        return $this->hasMany(TicketEntrada::class, 'ticket_usuario_id');
    }

    public function cajas() {
        return $this->hasMany(Caja::class, 'user_id');
    }

    public function registroActividad() {
        return $this->morphMany(RegistroActividad::class, 'registrable', 'causer_type', 'causer_id');
    }

    // notificaciones de tickets
    public function sendTicketCreatedSuccessNotification($ticket)
    {
        $this->notify(new \App\Notifications\TicketCreatedDoneNotification($ticket));
    }
    public function sendTicketEntradaUsuarioNotification($ticket)
    {
        $this->notify(new \App\Notifications\EntradaTicketUsuarioNotification($ticket));
    }
    public function sendTicketEntradaSoporteNotification($ticket)
    {
        $this->notify(new \App\Notifications\EntradaTicketSoporteNotification($ticket));
    }

    // termina notificaciones de tickets

    public function sendUserCreatedSuccessNotification()
    {
        $this->notify(new \App\Notifications\UserRegisterDoneNotification);
    }

    ////////////////notificaciones de los pedidos del usuario /////////////////////////////
    public function sendPurchaseSuccessNotification($carrito)
    {
        $this->notify(new \App\Notifications\PurchaseSuccessNotification($carrito));
    }

    public function sendPagoExitosoVentaMovilNotification($pedido)
    {
        $this->notify(new \App\Notifications\PagoExitosoVentaMovilNotification($pedido));
    }

    public function sendPagoExitosoNotification($pedido)
    {
        $this->notify(new \App\Notifications\PagoExitosoNotification($pedido));
    }
    public function sendPedidoRealizadoNotification($carrito)
    {
        $this->notify(new \App\Notifications\PedidoRealizadoNotification($carrito));
    }

    public function sendPedidoEnCaminoNotification($pedido)
    {
        $this->notify(new \App\Notifications\PedidoEnCaminoNotification($pedido));
    }

    public function sendPedidoEnvioParcialEnCaminoNotification($pedido, $no_guia, $paqueteria, $libros_envio) {
        $this->notify(new \App\Notifications\PedidoEnvioParcialEnCaminoNotification($pedido, $no_guia, $paqueteria, $libros_envio));
    }

////////////////termina notificaciones de los pedidos del usuario /////////////////////////////
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmailQueued);
    }

    public function verificarCuenta()
    {
        $this->email_verified_at = now();
        $this->save();
        $this->sendUserCreatedSuccessNotification();
    }
    public function getRole($id)
    {
        return User::where('id', $id)->with('roles')->first();
    }
    public function getAllRoles()
    {
        return \Spatie\Permission\Models\Role::all();
    }
    public function getFullName()
    {
        return $this->attributes['name'] . ' ' . $this->attributes['apellidos'];
    }

    public function getNombreCompletoAttribute() {
        return "{$this->name} {$this->apellidos}";
    }
}
