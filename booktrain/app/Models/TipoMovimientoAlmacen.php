<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoMovimientoAlmacen extends Model
{
    use HasFactory;

    protected $fillable = ['clave', 'naturaleza', 'privado', 'descripcion'];
    public $timestamps = false;

    protected $table = 'tipos_movimientos_almacenes';
}