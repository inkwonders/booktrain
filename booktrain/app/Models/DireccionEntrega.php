<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DireccionEntrega extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'direccion_entregas';

    public $fillable = [
        'colegio_id',
        'direccion_id',
        'tipo',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'colegio_id' => 'integer',
        'direccion_id' => 'integer',
        'tipo' => 'string'
    ];
}