<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    public $fillable = [
        'user_id',
        'colegio_id',
        'status',
        'fecha_apertura',
        'fecha_cierre'
    ];

    use HasFactory;

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function operaciones()
    {
        return $this->hasMany(OperacionCaja::class, 'caja_id');
    }

    public function pedidos() {
        return $this->belongsToMany(Pedido::class, 'pedidos_cajas', 'caja_id' , 'pedido_id')->withTimestamps();
    }

    public function scopeAbierta($query) {
        return $query->whereStatus('ABIERTA')->first();
    }

    public function colegio() {
        return $this->belongsTo(Colegio::class, 'colegio_id');
    }
}
