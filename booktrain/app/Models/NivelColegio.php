<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @SWG\Definition(
 *      definition="NivelColegio",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="seccion_id",
 *          description="seccion_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activo",
 *          description="activo",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class NivelColegio extends Model
{

    use HasFactory;

    public $table = 'niveles_colegios';




    public $fillable = [
        'seccion_id',
        'nombre',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'seccion_id' => 'integer',
        'nombre' => 'string',
        'activo' => 'integer'
    ];

    protected $appends = [
        'nombre_completo'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function seccionColegio()
    {
        return $this->belongsTo(SeccionColegio::class, 'seccion_id', 'id');
    }

    public function seccion()
    {
        return $this->belongsTo(SeccionColegio::class, 'seccion_id');
    }

    public function paquetes()
    {
        return $this->hasMany(Paquete::class, 'nivel_id');
    }

    public function getNombreCompletoAttribute()
    {
        if (isset($this->seccion_id))
            return $this->nombre . '-' . $this->seccionColegio->nombre;
        return '';
    }
}
