<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="Envio",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pedido_id",
 *          description="pedido_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="fecha_recepcion",
 *          description="fecha_recepcion",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="fecha_estimada",
 *          description="fecha_estimada",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="horarios_entrega",
 *          description="horarios_entrega",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="costo",
 *          description="costo",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="guia",
 *          description="guia",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="paqueteria",
 *          description="paqueteria",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Envio extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'envios';

    public $fillable = [
        'pedido_id',
        'status',
        'fecha_recepcion',
        'fecha_estimada',
        'horarios_entrega',
        'costo',
        'guia',
        'paqueteria',
        'paqueteria_id',
        'fecha_envio',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'status' => 'string',
        'fecha_recepcion' => 'date',
        'fecha_estimada' => 'date',
        'horarios_entrega' => 'string',
        'costo' => 'double',
        'guia' => 'string',
        'paqueteria' => 'string',
        'paqueteria_id' => 'integer',
        'fecha_envio' => 'date',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    // public function pedidoR()
    // {
    //     return $this->belongsTo(Pedido::class, 'pedido_id');
    // }

    public function pedido() {
        return $this->morphInstanceTo(Pedido::class, 'envio', 'App\Models\Pedido', 'enviable_id', 'id');
    }

    public function detalle() {
        return $this->morphInstanceTo(DetalleResumenPedido::class, 'envio', 'App\Models\DetalleResumenPedido', 'enviable_id', 'id');
    }

    public function paqueteria() {
        return $this->belongsTo(ServicioPaqueteria::class, 'paqueteria_id')->withDefault([
            'nombre' => 'Paqueteria no definida',
            'url' => env('APP_URL'),
            'activo' => true
        ]);
    }

    // Relación polimorfica
    public function enviable() {
        return $this->morphTo(__FUNCTION__, 'enviable_type', 'enviable_id');
    }

//     public function getCostoAttribute($value) {
// // dd($this->pedidoR->tipo_entrega, $this->pedidoR->tipo_entrega != 2, $value);
//         // distitnto de comicilio
// //dd($this->pedido->tipo_entrega);
//         $ff = $this->pedido;

//         // if ($this->pedidoR->tipo_entrega != 2) {
//         //     //return $value;
//         // }

//         return $value;

//     }
}
