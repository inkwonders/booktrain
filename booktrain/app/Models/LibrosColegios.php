<?php

namespace App\Models;

use App\Models\Libro;

use Eloquent as Model;
use App\Models\Colegio;
use App\Models\Paquete;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="LibrosColegios",
 *      required={""},
 *      @SWG\Property(
 *          property="libro_id",
 *          description="libro_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="colegio_id",
 *          description="colegio_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="precio",
 *          description="precio",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="paquete_id",
 *          description="paquete_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class LibrosColegios extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'libros_colegios';


    protected $appends = [
        'isbn'
    ];

    public $fillable = [
        'libro_id',
        'colegio_id',
        'precio',
        'paquete_id',
        'obligatorio',
        'activo',
        'bajo_pedido',
        'stock',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libro_id' => 'integer',
        'colegio_id' => 'integer',
        'precio' => 'double',
        'paquete_id' => 'integer',
        'obligatorio' => 'integer',
        'activo' => 'integer',
        'bajo_pedido' => 'integer',
        'stock' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function libro()
    {
        return $this->belongsTo(Libro::class, 'libro_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function colegio()
    {
        return $this->hasOne(Colegio::class, 'colegio_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function paquetes()
    {
        return $this->hasMany(Paquete::class, 'paquete_id');
    }

    public function datosLibro()
    {
        return $this->hasOne(Libro::class, 'id','libro_id');
    }

    public function getISBNAttribute()  {
        return  $this->libro->isbn;
    }
}