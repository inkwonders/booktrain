<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="TicketArchivo",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ticket_usuario_id",
 *          description="ticket_usuario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="archivo",
 *          description="archivo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="archivo_nombre_original",
 *          description="archivo_nombre_original",
 *          type="string",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="orden",
 *          description="orden",
 *          type="string",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tipo_archivo",
 *          description="tipo_archivo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TicketArchivo extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'ticket_archivos';

    public $fillable = [
        'ticket_usuario_id',
        'archivo',
        'archivo_nombre_original',
        'orden',
        'tipo_archivo',
        'url',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ticket_usuario_id' => 'integer',
        'archivo' => 'string',
        'archivo_nombre_original' => 'string',
        'orden' => 'integer',
        'tipo_archivo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function ticketUsuarios()
    {
        return $this->hasMany(\App\Models\TicketUsuario::class, 'id', 'ticket_usuario_id');
    }
}