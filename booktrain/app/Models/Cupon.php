<?php

namespace App\Models;

use Faker\Factory;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cupon extends Model
{
    use HasFactory;

    protected $table = 'cupones';

    protected $guarded = [
        'codigo',
        'status'
    ];

    protected $fillable = [
        'valor',
        'caducidad'
    ];

    protected $casts = [
        'caducidad' => 'datetime'
    ];

    public static function boot() {
        parent::boot();

        static::creating(function(Cupon $cupon) {
            $codigo = '';
            do {
                $codigo = Factory::create()->bothify('????-###-?#?#');
            }while(Cupon::where('codigo', $codigo)->count() > 0);

            $cupon->attributes['codigo'] = strtoupper($codigo);
        });

        static::retrieved(function(Cupon $cupon) {
            if($cupon->caducidad != null && $cupon->caducidad->isPast() && $cupon->pedido_id == null) {
                $cupon->status = 'VENCIDO';
                $cupon->attributes['status'] = 'VENCIDO';
            }
        });
    }

    public function afectarPedido() {
        $descuento = $this->valor / 100;

        if($this->pedido != null && $this->status == 'DISPONIBLE') {
            Log::debug("Cupon::afectarPedido", ['pedido' => $this->pedido, 'cupon' => $this]);

            $this->pedido->resumenes->each(function($resumen) use ($descuento) {
                $resumen->detalles->each(function($detalle) use ($descuento) {
                    if($descuento >= 0 && $descuento <= 100) {
                        $detalle->update([
                            'precio_libro' => $detalle->precio_libro - ($detalle->precio_libro * ($descuento))
                        ]);
                    }
                });

                $resumen->subtotal = $resumen->subtotal - ($resumen->subtotal * $descuento);
                $resumen->save();
            });

            $this->pedido->attributes['subtotal'] = $this->pedido->subtotal - ($this->pedido->subtotal * $descuento);

            // En este punto se calcula desde el pedido para contemplar el costo de envío y la comisión
            $this->pedido->attributes['total'] = $this->pedido->total;

            $this->pedido->save();

            $this->status = 'UTILIZADO';
            $this->touch();

            return $this->save();
        }

        Log::error("Cupon::afectarPedido", ['error' => 'El cupon ya esta en uso', 'cupon' => $this]);

        return false;
    }

    public function pedido() {
        return $this->belongsTo(Pedido::class, 'pedido_id', 'id');
    }
}
