<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroPago extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'registros_pagos';

    public $fillable = [
        'amount',
        'referencia',
        'origen',
        'source',
        'transactionTokenId',
        'status',
        'raw',
        'status_3ds',
        'confirm',
        'fecha_comprobacion',
        'raw_comprobacion'
    ];

    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }

    public function scopeExitoso($query) {
        return $query->where('status', 'SUCCESS');
    }
}
