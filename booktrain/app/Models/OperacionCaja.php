<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperacionCaja extends Model
{
    use HasFactory;

    protected $table = 'operaciones_cajas';

    protected $attributes = [
        'billete_1000' => 0,
        'billete_500' => 0,
        'billete_200' => 0,
        'billete_100' => 0,
        'billete_50' => 0,
        'billete_20' => 0,
        'conteo_monedas' => 0,
        'monto_monedas' => 0,
        'observaciones' => ''
    ];

    protected $fillable = [
        'tipo',
        'billete_1000',
        'billete_500',
        'billete_200',
        'billete_100',
        'billete_50',
        'billete_20',
        'conteo_monedas',
        'monto_monedas',
        'observaciones'
    ];

    public function caja()
    {
        return $this->belongsTo(Caja::class, 'caja_id');
    }

    public function getTotalAttribute()
    {
        $sumatoria = 0;

        $sumatoria += $this->billete_1000 * 1000;
        $sumatoria += $this->billete_500 * 500;
        $sumatoria += $this->billete_200 * 200;
        $sumatoria += $this->billete_100 * 100;
        $sumatoria += $this->billete_50 * 50;
        $sumatoria += $this->billete_20 * 20;
        $sumatoria += $this->monto_monedas;

        return $sumatoria;
    }
}
