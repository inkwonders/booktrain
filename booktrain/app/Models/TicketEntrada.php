<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="TicketEntrada",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ticket_usuario_id",
 *          description="ticket_usuario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="usuario_id",
 *          description="usuario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="tipo_entrada",
 *          description="tipo_entrada",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="comentario",
 *          description="comentario",
 *          type="string"
 *      ),
 *
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TicketEntrada extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'ticket_entradas';

    public $fillable = [
        'ticket_usuario_id',
        'usuario_id',
        'tipo_entrada',
        'comentario',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ticket_usuario_id' => 'integer',
        'usuario_id' => 'integer',
        'tipo_entrada' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function ticketUsuario()
    {
        return $this->hasOne(\App\Models\TicketUsuario::class, 'id', 'ticket_usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(\App\Models\User::class, 'id', 'usuario_id');
    }

    public function archivos()
    {
        return $this->hasMany(\App\Models\TicketEntradaArchivo::class, 'entrada_id', 'id');
    }

    public function ticketEntradaArchivos()
    {
        return $this->hasMany(\App\Models\TicketEntradaArchivo::class, 'entrada_id', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(\App\Models\TicketUsuario::class, 'ticket_usuario_id');
    }



}