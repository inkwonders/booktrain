<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="DetalleResumenPedido",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="resumen_pedidos_id",
 *          description="resumen_pedidos_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="libro_id",
 *          description="libro_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="precio_libro",
 *          description="precio_libro",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class DetalleResumenPedido extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'detalles_resumen_pedidos';

    public $fillable = [
        'libro_id',
        'precio_libro',
        'cantidad',
        'entregado',
        'fecha_entrega',
        'enviado',
        'devuelto',
        'razon_devolucion',
        'descuento_devolucion',
        'porcentaje_descuento_devolucion',
        'fecha_devolucion',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                 => 'integer',
        'resumen_pedidos_id' => 'integer',
        'cantidad'           => 'integer',
        'libro_id'           => 'string',
        'precio_libro'       => 'double',
        'fecha_devolucion'   => 'datetime',
    ];

    protected $appends = ['total'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    // Valor default de los atributos
    protected $attributes = [
        'cantidad' => 1
    ];

    protected static function booted() {
        static::created(function ($item) {
            $item->resumenPedido->update([
                'subtotal' => $item->resumenPedido->detalles->map(function($detalle) {
                    return $detalle->precio_libro * $detalle->cantidad;
                })->sum()
            ]);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function resumenPedido()
    {
        return $this->belongsTo(ResumenPedido::class, 'resumen_pedidos_id');
    }

    public function resumen()
    {
        return $this->belongsTo(ResumenPedido::class, 'resumen_pedidos_id');
    }

    public function pedido() {
        return $this->hasManyThrough(
            Pedido::class,
            ResumenPedido::class,
            'id', // resumen_pedidos.id
            'id', // pedidos.id
            'resumen_pedidos_id', // detalle_resumen_pedidos.resumen_pedidos_id
            'pedido_id' // resumen_pedidos.pedido_id
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function libro() {
        return $this->belongsTo(Libro::class, 'libro_id');
    }

    public function envio() {
        return $this->morphOne(Envio::class, 'enviable');
    }

    public function getPrecioLibroAttribute($value) {
        if ($this->devuelto == 1) {
            return $value - $this->descuento_devolucion;
        } else {
            return $this->attributes['precio_libro'];
        }
    }

    public function getPrecioLibroOriginalAttribute($value) {
        return $this->attributes['precio_libro'];
    }

    public function getTotalAttribute() {
        return $this->cantidad * $this->precio_libro;
    }
}
