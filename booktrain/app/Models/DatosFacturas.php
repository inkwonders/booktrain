<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @SWG\Definition(
 *      definition="DatosFacturas",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pedido_id",
 *          description="pedido_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="razon_social",
 *          description="razon_social",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rfc",
 *          description="rfc",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="correo",
 *          description="correo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="id_cfdi",
 *          description="id_cfdi",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cp",
 *          description="cp",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="metodo_pago_id",
 *          description="metodo_pago_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="forma_pago_id",
 *          description="forma_pago_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class DatosFacturas extends Model
{

    use HasFactory;
    use SoftDeletes;

    public $table = 'datos_facturas';




    public $fillable = [
        'pedido_id',
        'user_id',
        'razon_social',
        'rfc',
        'regimen_fiscal',
        'correo',
        'id_cfdi',
        'cp',
        'calle',
        'num_exterior',
        'num_interior',
        'colonia',
        'municipio',
        'estado',
        'metodo_pago',
        'forma_pago',
        'metodo_pago_id',
        'forma_pago_id',
        'facturama_error',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pedido_id' => 'integer',
        'user_id' => 'integer',
        'razon_social' => 'string',
        'rfc' => 'string',
        'regimen_fiscal'=> 'string',
        'correo' => 'string',
        'id_cfdi' => 'integer',
        'cp' => 'string',
        'metodo_pago_id' => 'integer',
        'forma_pago_id' => 'integer',
        'facturama_error'=> 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function metodoPago()
    {
        return null; //$this->hasOne(MetodoPago::class, 'id', 'metodo_pago_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function formaPago()
    {
        return null; //$this->hasOne(FormaPago::class, 'id', 'forma_pago_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function cfdis()
    {
        return $this->belongsTo(UsoCFDI::class, 'id_cfdi');
    }

}
