<?php

namespace App\Repositories;

use App\Models\LibroPaquete;
use App\Repositories\BaseRepository;

/**
 * Class LibroPaqueteRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:42 pm UTC
*/

class LibroPaqueteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        // 'libro_id',
        'paquete_id',
        'obligatorio',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LibroPaquete::class;
    }
}
