<?php

namespace App\Repositories;

use App\Models\TicketUsuario;
use App\Repositories\BaseRepository;

/**
 * Class TicketUsuarioRepository
 * @package App\Repositories
 * @version April 12, 2021, 11:35 am CDT
*/

class TicketUsuarioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'usuario_id',
        'folio',
        'status',
        'titulo',
        'ticket_motivo_id',
        'descripcion',
        'calificacion',
        'comentario_calificacion',
        'fecha_calificacion',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketUsuario::class;
    }
}
