<?php

namespace App\Repositories;

use App\Models\UsoCFDI;
use App\Repositories\BaseRepository;

/**
 * Class UsoCFDIRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:50 pm UTC
*/

class UsoCFDIRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'descripcion',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UsoCFDI::class;
    }
}
