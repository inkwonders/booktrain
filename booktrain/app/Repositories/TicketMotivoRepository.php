<?php

namespace App\Repositories;

use App\Models\TicketMotivo;
use App\Repositories\BaseRepository;

/**
 * Class TicketMotivoRepository
 * @package App\Repositories
 * @version April 12, 2021, 11:26 am CDT
*/

class TicketMotivoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketMotivo::class;
    }
}
