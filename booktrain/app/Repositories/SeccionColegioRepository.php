<?php

namespace App\Repositories;

use App\Models\SeccionColegio;
use App\Repositories\BaseRepository;

/**
 * Class SeccionColegioRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:24 pm UTC
*/

class SeccionColegioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'colegio_id',
        'nombre',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeccionColegio::class;
    }
}
