<?php

namespace App\Repositories;

use App\Models\TicketEntrada;
use App\Repositories\BaseRepository;

/**
 * Class TicketEntradaRepository
 * @package App\Repositories
 * @version April 12, 2021, 11:50 am CDT
*/

class TicketEntradaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ticket_usuario_id',
        'usuario_id',
        'tipo_entrada',
        'comentario',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketEntrada::class;
    }
}
