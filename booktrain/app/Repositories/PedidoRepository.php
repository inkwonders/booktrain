<?php

namespace App\Repositories;

use App\Models\Pedido;
use App\Repositories\BaseRepository;

/**
 * Class PedidoRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:49 pm UTC
*/

class PedidoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'folio',
        'status',
        'usuario_id',
        'colegio_id',
        'nombre_contacto',
        'apellidos_contacto',
        'celular_contacto',
        'tipo_entrega',
        'factura',
        'metodo_pago_id',
        'forma_pago_id',
        'terminos_condiciones',
        'subtotal',
        'comision',
        'total',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pedido::class;
    }
}
