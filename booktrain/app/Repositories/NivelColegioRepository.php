<?php

namespace App\Repositories;

use App\Models\NivelColegio;
use App\Repositories\BaseRepository;

/**
 * Class NivelColegioRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:29 pm UTC
*/

class NivelColegioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'seccion_id',
        'nombre',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NivelColegio::class;
    }
}
