<?php

namespace App\Repositories;

use App\Models\Colegio;
use App\Repositories\BaseRepository;

/**
 * Class ColegioRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:18 pm UTC
*/

class ColegioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'nombre',
        'logo',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Colegio::class;
    }
}
