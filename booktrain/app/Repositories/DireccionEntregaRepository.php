<?php

namespace App\Repositories;

use App\Models\DireccionEntrega;
use App\Repositories\BaseRepository;

/**
 * Class DireccionEntregaRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:19 pm UTC
*/

class DireccionEntregaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pedido_id',
        'user_id',
        'calle',
        'no_exterior',
        'no_interior',
        'colonia',
        'cp',
        'estado_id',
        'municipio_id',
        'referencia',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DireccionEntrega::class;
    }
}
