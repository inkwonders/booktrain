<?php

namespace App\Repositories;

use App\Models\LibrosColegios;
use App\Repositories\BaseRepository;

/**
 * Class LibrosColegiosRepository
 * @package App\Repositories
 * @version April 6, 2021, 5:09 pm UTC
*/

class LibrosColegiosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libro_id',
        'colegio_id',
        'precio',
        'paquete_id',
        'obligatorio',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LibrosColegios::class;
    }
}
