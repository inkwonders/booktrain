<?php

namespace App\Repositories;

use App\Models\Envio;
use App\Repositories\BaseRepository;

/**
 * Class EnvioRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:52 pm UTC
*/

class EnvioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pedido_id',
        'fecha_recepcion',
        'fecha_estimada',
        'horarios_entrega',
        'costo',
        'guia',
        'paqueteria',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Envio::class;
    }
}
