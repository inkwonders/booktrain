<?php

namespace App\Repositories;

use App\Models\Paquete;
use App\Repositories\BaseRepository;

/**
 * Class PaqueteRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:38 pm UTC
*/

class PaqueteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nivel_id',
        'nombre',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Paquete::class;
    }
}
