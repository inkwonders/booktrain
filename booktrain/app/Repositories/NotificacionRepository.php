<?php

namespace App\Repositories;

use App\Models\Notificacion;
use App\Repositories\BaseRepository;

/**
 * Class NotificacionRepository
 * @package App\Repositories
 * @version April 20, 2021, 9:50 am CDT
*/

class NotificacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'colegio_id',
        'titulo',
        'aviso',
        'activo',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notificacion::class;
    }
}
