<?php

namespace App\Repositories;

use App\Models\DatosFacturas;
use App\Repositories\BaseRepository;

/**
 * Class DatosFacturasRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:48 pm UTC
*/

class DatosFacturasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pedido_id',
        'user_id',
        'razon_social',
        'rfc',
        'correo',
        'id_cfdi',
        'cp',
        'metodo_pago_id',
        'forma_pago_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DatosFacturas::class;
    }
}
