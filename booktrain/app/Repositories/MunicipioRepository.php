<?php

namespace App\Repositories;

use App\Models\Municipio;
use App\Repositories\BaseRepository;

/**
 * Class MunicipioRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:52 pm UTC
*/

class MunicipioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion',
        'created_at',
        'updated_at',
        'estado_id',
        'lon',
        'lat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Municipio::class;
    }
}
