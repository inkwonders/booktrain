<?php

namespace App\Repositories;

use App\Models\FormaPago;
use App\Repositories\BaseRepository;

/**
 * Class FormaPagoRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:50 pm UTC
*/

class FormaPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'descripcion',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FormaPago::class;
    }
}
