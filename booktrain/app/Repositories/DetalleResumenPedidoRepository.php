<?php

namespace App\Repositories;

use App\Models\DetalleResumenPedido;
use App\Repositories\BaseRepository;

/**
 * Class DetalleResumenPedidoRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:57 pm UTC
*/

class DetalleResumenPedidoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'resumen_pedidos_id',
        'libro_id',
        'precio_libro',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetalleResumenPedido::class;
    }
}
