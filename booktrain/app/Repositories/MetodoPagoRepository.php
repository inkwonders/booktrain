<?php

namespace App\Repositories;

use App\Models\MetodoPago;
use App\Repositories\BaseRepository;

/**
 * Class MetodoPagoRepository
 * @package App\Repositories
 * @version April 6, 2021, 4:50 pm UTC
*/

class MetodoPagoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'codigo',
        'descripcion',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MetodoPago::class;
    }
}
