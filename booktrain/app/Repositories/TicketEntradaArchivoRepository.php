<?php

namespace App\Repositories;

use App\Models\TicketEntradaArchivo;
use App\Repositories\BaseRepository;

/**
 * Class TicketEntradaArchivoRepository
 * @package App\Repositories
 * @version April 12, 2021, 12:06 pm CDT
*/

class TicketEntradaArchivoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entrada_id',
        'archivo_nombre_original',
        'url',
        'tipo_archivo',
        'orden',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketEntradaArchivo::class;
    }
}
