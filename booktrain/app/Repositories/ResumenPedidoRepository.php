<?php

namespace App\Repositories;

use App\Models\ResumenPedido;
use App\Repositories\BaseRepository;

/**
 * Class ResumenPedidoRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:55 pm UTC
*/

class ResumenPedidoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pedido_id',
        'nombre_alumno',
        'paterno_alumno',
        'materno_alumno',
        'subtotal',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResumenPedido::class;
    }
}
