<?php

namespace App\Repositories;

use App\Models\Configuracion;
use App\Repositories\BaseRepository;

/**
 * Class ConfiguracionRepository
 * @package App\Repositories
 * @version April 6, 2021, 5:14 pm UTC
*/

class ConfiguracionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'etiqueta',
        'valor',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Configuracion::class;
    }
}
