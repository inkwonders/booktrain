<?php

namespace App\Repositories;

use App\Models\Libro;
use App\Repositories\BaseRepository;

/**
 * Class LibroRepository
 * @package App\Repositories
 * @version April 5, 2021, 10:32 pm UTC
*/

class LibroRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'editorial',
        'edicion',
        'isbn',
        'stock',
        // 'precio',
        'activo'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Libro::class;
    }
}
