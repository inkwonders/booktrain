<?php

namespace App\Repositories;

use App\Models\TicketArchivo;
use App\Repositories\BaseRepository;

/**
 * Class TicketArchivoRepository
 * @package App\Repositories
 * @version April 12, 2021, 11:43 am CDT
*/

class TicketArchivoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ticket_usuario_id',
        'archivo',
        'archivo_nombre_original',
        'orden',
        'tipo_archivo',
        'url',
        'created_at',
        'updated_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketArchivo::class;
    }
}
