<?php

namespace App\Imports;

use App\Models\ReferenciaPedido;
use Maatwebsite\Excel\Concerns\ToModel;

use Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class BancomerImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        $col_fecha_operacion = 0;
        $col_concepto = 1;
        $col_referencia = 2;
        $col_referencia_ampliada = 3;

        if (is_null($row) || is_null($row[0])) {
            return;
        }

        //validamos el formato: $col_concepto = CE00000000000100011279
        $ex = explode('CE', $row[$col_concepto]);

        if (count($ex) != 2) {
            return;
        }

        $referencia = (int) $ex[1];

        if ($referencia == 0) {
            return;
        }

        $referencia_pedido = ReferenciaPedido::where("referencia", $referencia)->first();

        if (is_null($referencia_pedido)) {
            return;
        }

        //si llega aqui es por que encontro la referencia del pedido

        $fecha_operacion = Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject(intval($row[$col_fecha_operacion])));

        $json = [
            "fecha_operacion" => $fecha_operacion->format('Y-m-d'),
            "concepto" => $row[$col_concepto],
            "referencia" => $row[$col_referencia],
            "referencia_apliada" => $row[$col_referencia_ampliada],
        ];

        DB::beginTransaction();

        //actualizamos la referencia del pedido
        $referencia_pedido->response = json_encode($json);
        $referencia_pedido->pagado = 1;
        $referencia_pedido->fecha_pago = $fecha_operacion;

        try {
            $referencia_pedido->save();
        } catch (\Exception $e) {
            Log::error('Error al actualizar la referencia del pedido: ' . $e->getMessage);
            DB::rollback();
        }

        //actualizamos el estado del pedido

        $pedido = $referencia_pedido->pedido;
        $pedido->status = 'PAGADO';

        try {
            $pedido->save();
        } catch (\Exception $e) {
            Log::error('Error al actualizar el estatus del pedido: ' . $e->getMessage);
            DB::rollback();
        }

        DB::commit();

    }
}
