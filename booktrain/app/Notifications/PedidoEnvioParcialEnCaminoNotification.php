<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PedidoEnvioParcialEnCaminoNotification extends Notification
{
    use Queueable;

    protected $pedido;
    protected $no_guia;
    protected $paqueteria;
    protected $libros_envio;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pedido, $no_guia, $paqueteria, $libros_envio)
    {
        $this->pedido = $pedido;
        $this->no_guia = $no_guia;
        $this->paqueteria = $paqueteria;
        $this->libros_envio = $libros_envio;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $img_url = public_path()."/assets/img/check.png";
        $img_logo = public_path()."/assets/img/logo.png";
        return (new MailMessage)
        ->subject("Envío parcial en camino: {$this->pedido->serie}{$this->pedido->folio}")
            ->view('emails.envio_parcial_en_camino', [
                'pedido' => $this->pedido,
                'no_guia' => $this->no_guia,
                'paqueteria' => $this->paqueteria,
                'libros_envio' => $this->libros_envio,
                'img_logo' => $img_logo,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
