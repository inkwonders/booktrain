<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class PagoExitosoVentaMovilNotification extends Notification
{
    use Queueable;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected  $pedido;

    public function __construct($pedido)
    {
        //
        $this->pedido = $pedido;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $img_url = public_path()."/assets/img/check.png";
        $img_logo = public_path()."/assets/img/logo.png";

        // dd($this->pedido->toArray());
        return (new MailMessage)
        ->subject(Lang::get('Pago Exitoso: '. $this->pedido->serie . $this->pedido->folio))
        ->bcc('pedidosweb@booktrain.com.mx')
        ->bcc('pedidosweb@provesa.mx')
        ->bcc('coordinacionsac1@provesa.mx')
        ->bcc($this->pedido->email_pedido)
        ->view('emails.pago_venta_movil', [
            'pedido' => $this->pedido,
            'img_logo' => $img_logo
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
