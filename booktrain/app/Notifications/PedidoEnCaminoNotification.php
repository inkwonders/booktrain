<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class PedidoEnCaminoNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected  $pedido;

    public function __construct($pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $img_url = public_path()."/assets/img/check.png";
        $img_logo = public_path()."/assets/img/logo.png";

        $paqueteria = $this->pedido->envio ? $this->pedido->envio->paqueteria()->first() : null;

        return (new MailMessage)
            ->subject(
                Lang::get('Pedido en camino: '. $this->pedido->serie . $this->pedido->folio)
            )
            ->from('pedidosweb@provesa.mx', 'Pedidos Booktrain')
            ->view('emails.pedido_en_camino', [
                'logo' => $img_logo,
                'entrega_paqueteria' => ($paqueteria != null),
                'no_guia' => $this->pedido->envio ? $this->pedido->envio->guia : '',
                'url_paqueteria' => $paqueteria ? $paqueteria->url : '',
                'serie' => $this->pedido->serie,
                'folio' => $this->pedido->folio,
                'fecha' => $this->pedido->created_at->format('d / m / Y'),
                'estado_pago' => $this->pedido->status,
                'metodo_pago' => $this->pedido->metodo_pago,
                'forma_pago' => $this->pedido->forma_pago,
                'nombre_contacto' => "{$this->pedido->nombre_contacto} {$this->pedido->apellidos_contacto}",
                'telefono_contacto' => $this->pedido->celular_contacto,
                'correo' => $this->pedido->user->email,
                'descripcion_tipo_entrega' => $this->pedido->descripcion_tipo_entrega,
                'direccion' => (Object) [
                    'calle' => $this->pedido->direccion->calle,
                    'no_exterior' => $this->pedido->direccion->no_exterior,
                    'no_interior' => $this->pedido->direccion->no_interior,
                    'colonia' => $this->pedido->direccion->colonia,
                    'cp' => $this->pedido->direccion->cp,
                    'municipio' => $this->pedido->direccion->municipio,
                    'estado' => $this->pedido->direccion->estado,
                    'referencia' => $this->pedido->direccion->referencia,
                ],
                'solicito_facturacion' => ($this->pedido->factura && $this->pedido->datosFactura != null),
                'datos_facturacion' => (Object) [
                    'razon_social' => $this->pedido->datosFactura ? $this->pedido->datosFactura->razon_social : '',
                    'rfc' => $this->pedido->datosFactura ? $this->pedido->datosFactura->rfc : '',
                    'correo' => $this->pedido->datosFactura ? $this->pedido->datosFactura->correo : '',
                    'uso_cfdi' => $this->pedido->datosFactura ? $this->pedido->datosFactura->uso_cfdi : '',
                ],
                'resumenes' => $this->pedido->resumenes->map(function($resumen) {
                    return (Object) [
                        'nombre' => $resumen->nombre_alumno,
                        'apellido_paterno' => $resumen->paterno_alumno,
                        'apellido_materno' => $resumen->materno_alumno,
                        'escuela' => $resumen->pedido->colegio->nombre,
                        'grado' => "{$resumen->paquete->nivelColegio->nombre} {$resumen->paquete->nivelColegio->seccion->nombre}",
                        'detalles' => $resumen->detalles->map(function($detalle) {
                                return (Object) [
                                    'libro' => $detalle->libro->nombre,
                                    'isbn' => $detalle->libro->isbn,
                                    'cantidad' => $detalle->cantidad,
                                    'precio' => number_format($detalle->precio_libro, 2)
                                ];
                            })
                        ];
                }),
                'costo_envio' => $this->pedido->envio ? $this->pedido->envio->costo : 0,
                'comision' => number_format($this->pedido->comision, 2),
                'subtotal' => number_format($this->pedido->subtotal, 2),
                'total' => number_format($this->pedido->total),
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
