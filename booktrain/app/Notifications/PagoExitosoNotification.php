<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class PagoExitosoNotification extends Notification
{
    use Queueable;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected  $pedido;

    public function __construct($pedido)
    {
        //
        $this->pedido = $pedido;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line( $this->carrito);
        $img_url = public_path()."/assets/img/check.png";
        $img_logo = public_path()."/assets/img/logo.png";
        return (new MailMessage)
        ->subject(Lang::get('Pago Exitoso: '. $this->pedido->serie . $this->pedido->folio))
        ->bcc('pedidosweb@booktrain.com.mx')
        ->bcc('coordinacionsac1@provesa.mx')
        ->bcc('pedidosweb@provesa.mx')
        ->view('emails.pago_exitoso', [

            'pedido' => $this->pedido,
            'img_logo' => $img_logo

        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
