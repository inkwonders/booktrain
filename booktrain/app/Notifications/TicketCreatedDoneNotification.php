<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\User;
use App\Models\TicketUsuario;
use Illuminate\Support\Facades\Lang;

class TicketCreatedDoneNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected  $ticket;

    public function __construct($ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line( $this->carrito);

        $img_url = public_path()."/assets/img/mensaje.png";
        $img_logo = public_path()."/assets/img/logo.png";
        return (new MailMessage)
        ->subject(Lang::get('Se ha creado tu ticket de soporte #'.$this->ticket->id))
        // ->bcc('pedidosweb@booktrain.com.mx')
            ->view('emails.ticketEnviado', [

                'img_url' => $img_url,
                'img_logo' => $img_logo,
                'ticket' =>  $this->ticket

            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
