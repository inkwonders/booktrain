<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;


class EntradaTicketUsuarioNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected  $ticket;

    public function __construct($ticket)
    {
        //
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        $img_url = public_path()."/assets/img/mensaje.png";
        $img_logo = public_path()."/assets/img/logo.png";
        return (new MailMessage)
        ->subject(Lang::get('Se ha creado tu entrada en ticket de soporte #'.$this->ticket->ticketUsuario->id))
            ->view('emails.ticketEntradaUsuario', [

                'img_url' => $img_url,
                'img_logo' => $img_logo,
                'ticket' =>  $this->ticket

            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
