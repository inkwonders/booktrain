<?php

namespace App\Notifications;

use App\Mail\NotificacionPuntoReorden;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

class LibrosReordenNotification extends Notification
{
    use Queueable;
    private $libros;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($libros)
    {
        $this->libros = $libros;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($email)
    {
        $fecha = Carbon::now();

        return (new MailMessage($email))
            ->subject("Hay {$this->libros->count()} libro(s) que ha(n) superado el punto de reorden")
            // ->from('admin@booktrain.com', 'Plataforma Booktrain')
            ->view('emails.notificacion_punto_reorden', [
                'fecha' => $fecha->format('d-m-Y H:m:s'),
                'libros' => $this->libros
            ])
            // ->attachFromStorage('/path/to/file', 'reporte.xlsx', [
            //     'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            // ]) // Adjuntamos el archivo en excel
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
