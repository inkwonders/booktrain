<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\TicketMotivo;

class TicketMotivoTransformer extends Fractal\TransformerAbstract
{

	public function transform(TicketMotivo $ticketMotivo)
	{
	    return [
	        'id'      => (int) $ticketMotivo->id,
	        'nombre'   => $ticketMotivo->nombre,
            'orden'   => $ticketMotivo->orden
	    ];
	}




}
