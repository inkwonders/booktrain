<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\TicketArchivo;

class TicketArchivoTransformer extends Fractal\TransformerAbstract
{

	public function transform(TicketArchivo $ticketArchivo)
	{
	    return [
	        'id'      => (int) $ticketArchivo->id,
	        'archivo'   => $ticketArchivo->url,
            'nombre'   => $ticketArchivo->archivo_nombre_original,
            'orden'   => $ticketArchivo->orden,
            'tipo'   => $ticketArchivo->tipo_archivo,
            // 'url'   => $ticketArchivo->url
	    ];
	}

}
