<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\ServicioPaqueteria;

class ServicioPaqueteriaTransformer extends Fractal\TransformerAbstract
{

	public function transform(ServicioPaqueteria $servicio_paqueteria)
	{
	    return [
	        'id'      => (int) $servicio_paqueteria->id,
            'nombre'   => $servicio_paqueteria->nombre,
            'url'   => $servicio_paqueteria->url
	    ];
	}

}
