<?php
namespace App\Transformers;

use App\Models\TicketUsuario;
use League\Fractal;

class TicketUsuarioTransformer extends Fractal\TransformerAbstract
{

    protected $availableIncludes = [
        'usuario'
    ];

    protected $defaultIncludes = [
        'motivo','archivos'
    ];


	public function transform(TicketUsuario $ticketUsuario)
	{

	    return [
	        'id'      => (int) $ticketUsuario->id,
            'folio'   => $ticketUsuario->folio,
	        'status'   => $ticketUsuario->status,
            'titulo'   => $ticketUsuario->titulo,
            'descripcion'   => $ticketUsuario->descripcion,
            'calificacion'   => $ticketUsuario->calificacion,
            'comentario_calificacion'   => $ticketUsuario->comentario_calificacion,
            'fecha_calificacion'   => $ticketUsuario->fecha_calificacion,
            'telefono'   => $ticketUsuario->telefono,
            'usuario_id'   => $ticketUsuario->usuario_id,
            'ticket_motivos_id'   => $ticketUsuario->ticket_motivos_id,
            'created_at'   => date_format($ticketUsuario->created_at,"d / m / Y"),

	    ];
	}

    public function includeUsuario(TicketUsuario $ticketUsuario)
    {
        return $this->Item($ticketUsuario->user, new UserTransformer($ticketUsuario));
    }

    public function includeMotivo(TicketUsuario $ticketUsuario)
    {
        return $this->Item($ticketUsuario->ticketMotivo, new TicketMotivoTransformer($ticketUsuario));
    }

    public function includeArchivos(TicketUsuario $ticketUsuario)
    {
        return $this->Collection($ticketUsuario->ticketArchivos, new TicketArchivoTransformer($ticketUsuario));
    }

}
