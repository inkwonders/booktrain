<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\Libro;
use Illuminate\Support\Facades\Auth;

class LibrosTransformer extends Fractal\TransformerAbstract
{

    function __construct($colegio_id = null, $paquete_id = null)
    {
       $this->colegio_id = $colegio_id;
       $this->paquete_id = $paquete_id;
    }

	public function transform(Libro $libro)
	{

        $colegio_id = $this->colegio_id;
        $paquete_id = $this->paquete_id;

        // Obtenemos el libro correspondiente al colegio SIN paquete para leer el stock y si es bajo pedido
        $libroColegio = $libro->colegios()
                        ->with('libros', function($query) use ($colegio_id) {
                            $query->where('paquete_id', '=', null);
                            $query->where('colegio_id', $colegio_id);
                        })
                        ->whereHas('libros', function($query) use ($colegio_id) {
                            $query->where('paquete_id', '=', null);
                            $query->where('colegio_id', $colegio_id);
                        })->first();
        /*$libroColegio = $libro->colegios()
                        ->whereHas('libros', function($query) use ($colegio_id) {
                            $query->wherePaqueteId(null);
                            $query->where('colegio_id',$colegio_id);
                        })->first();*/

        $libroPaquete = $libro->colegios()
                        ->wherePivot('paquete_id', $paquete_id)
                        ->with('libros', function($query) use ($colegio_id, $paquete_id) {
                            $query->wherePaqueteId($paquete_id);
                            $query->where('colegio_id',$colegio_id);
                        })
                        ->first();

        /*$libroPaquete = $libro->colegios()
                        ->whereHas('libros', function($query) use ($colegio_id, $paquete) {
                            $query->wherePaqueteId($paquete->id);
                            $query->where('colegio_id',$colegio_id);
                        })->first();*/
        $obligatorio = $libroPaquete->pivot->obligatorio;

        if($obligatorio) {

            // Comprobamos que el usuario no haya comprado otro libro igual antes
            // $obligatorio = Auth::user()
            //                 ->pedidos()
            //                 //->where('status', 'PAGADO')
            //                 ->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'])
            //                 ->get()
            //                 ->pluck('libros')
            //                 ->collapse()
            //                 ->where('id', $libro->id)
            //                 ->isEmpty();// Si el arreglo está vacío entonces el libro sigue siendo obligatorio

                            $anio=date('Y');

                            $obligatorio = Auth::user()
                            ->pedidos()
                            //->where('status', 'PAGADO')
                            ->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'])

                            ->WhereHas('resumenes.detallesResumenPedidos', function ($query) use($libro,$anio) {

                                return $query->where('libro_id',$libro->id)

                                            ->whereYear('created_at',$anio);

                                })//valida q haya comprado el libro obligatorio en ese año si si ya no lo obliga
                            ->get()
                            ->pluck('libros')
                            ->collapse()
                            ->where('id', $libro->id)
                            ->isEmpty();// Si el arreglo está vacío entonces el libro sigue siendo obligatorio
        }

        return [
	        'id'           => (int) $libro->id,
	        'nombre'       => $libro->nombre,
            'editorial'    => $libro->editorial,
            'edicion'      => $libro->edicion,
            'isbn'         => $libro->isbn,
            'clave_producto'  => $libro->clave_producto,
            'objeto_impuesto'  => $libro->objeto_impuesto,
            //'stock'        => is_null($libroColegio)? 0 : $libroColegio->pivot->stock,
            'stock'        => $libroColegio->pivot->stock,
            'bajo_pedido'  => $libroColegio->pivot->bajo_pedido,
            //'activo'       => $libroColegio->pivot->activo,
            'activo'       => $libroPaquete->pivot->activo,
            'obligatorio'  => $obligatorio,
            'precio'       => $libroPaquete->pivot->precio
	    ];
	}

}
