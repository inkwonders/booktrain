<?php
namespace App\Transformers;

use App\Models\FormaPago;
use League\Fractal;

class FormaPagoTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [

    ];

	public function transform(FormaPago $datos)
	{
	    return [
	        'id'      => (int) $datos->id,
	        'codigo'   => $datos->codigo,
            'descripcion'   => $datos->descripcion
	    ];
	}

}
