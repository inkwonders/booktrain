<?php
namespace App\Transformers;

use App\Models\Estado;
use League\Fractal;

class EstadoTransformer extends Fractal\TransformerAbstract
{

    protected $availableIncludes = [
        'municipios'
    ];

	public function transform(Estado $estado)
	{
	    return [
	        'id'      => (int) $estado->id,
            'nombre'   => $estado->descripcion,
	        'nombre_corto'   => $estado->abrev
	    ];
	}

    public function includeMunicipios(Estado $estado)
    {
        return $this->Collection($estado->municipios, new MunicipioTransformer($estado));
    }

}
