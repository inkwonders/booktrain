<?php
namespace App\Transformers;

use App\Models\DetalleResumenPedido;
use App\Models\Pedido;
use League\Fractal;

class DetallesResumenPedidoColegioViewTransformer extends Fractal\TransformerAbstract
{
    /** falta terminar de obtener detalles del transformer */
	public function transform(DetalleResumenPedido $detalles)
	// {

        // public function transform(Pedido $detalles)
	   {

        // if($detalles->status != 'CARRITO')
     if($detalles->resumenPedido->pedido->status != 'CARRITO')
            return [
                'id'      => (int) $detalles->id,
                'isbn'                  => $detalles->libro->isbn,
                'editorial'             => $detalles->libro->editorial,
                'nombre_colegio'        => $detalles->resumenPedido->pedido->colegio->nombre,
                'titulo'                => $detalles->libro->nombre,
                'obligatorio'           => $detalles->libro->paquetes->first()->pivot->obligatorio,
                'Grado'                 => $detalles->libro->paquetes->first()->nombre,
                'precio_lista'          => '',
                'precio_alumno'         =>  $detalles->libro->paquetes->first()->pivot->precio,
                'descuento_colegio_alumno'   => '',
                'pedidos_cantidad'       => $detalles->libro->paquetes->first()->pivot->bajo_pedido,
                'punto_reorden'          => '5', //campo desconocido de donde proviene
                'entrega_cantidad'       => '30', //campo desconocido de donde proviene
                'venta_mostrador'        => $detalles->resumenPedido->pedido->tipo_entrega,
                'devoluciones_cantidad'   => '',//campo desconocido de donde proviene
                'venta_real'              => $detalles->resumenPedido->pedido->comision,
                'valor_pedido'            => $detalles->resumenPedido->pedido->total,
                'valor_devolucion'        => '',//campo desconocido de donde proviene
                'valor_real_venta'        => $detalles->resumenPedido->pedido->total,
                'descuento_otorgado'      => '',

                    # code...



            ];
        else
            return [];
	}

}
