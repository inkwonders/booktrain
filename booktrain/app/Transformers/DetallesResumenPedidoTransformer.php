<?php
namespace App\Transformers;

use App\Models\DetalleResumenPedido;
use League\Fractal;

class DetallesResumenPedidoTransformer extends Fractal\TransformerAbstract
{
    function __construct($paquete_id = null, $colegio_id = null)
    {
       $this->paquete_id = $paquete_id;
       $this->colegio_id = $colegio_id;
    }

    public function transform(DetalleResumenPedido $detalleResumen)
	{
        $paquete_id = $this->paquete_id;
        $colegio_id = $this->colegio_id;

        /**
         * Se hace una subconsulta para traer los paquetes del libro
         * donde el paquete_id sea igual al paquete_id que estamos recibiendo
         * ya obteniendo eso podemos acceder a los datos de ese libro_colegio
        */

        // Obtenemos el libro correspondiente al paquete indicado
        $libro = $detalleResumen->libro()
            ->with('paquetes', function($query) use ($paquete_id, $colegio_id) {
                $query->wherePaqueteId($paquete_id);
                $query->where('colegio_id', $colegio_id);
            })
            ->whereHas('paquetes', function($query) use ($paquete_id, $colegio_id) {
                $query->wherePaqueteId($paquete_id);
                $query->where('colegio_id', $colegio_id);
            })->first();

        // Obtenemos el libro correspondiente al colegio indicado que NO TIENE un paquete, para obtener el stock del libro en el colegio
        $libroColegio = $detalleResumen->libro()
            ->with('colegios', function($query) use ($colegio_id) {
                $query->where('paquete_id','=', null);
                $query->where('colegio_id', $colegio_id);
            })
            ->whereHas('colegios', function($query) use ($colegio_id) {
                $query->where('paquete_id','=', null);
                $query->where('colegio_id', $colegio_id);
            })
            ->first();

	    return [
	        'id'            => (int) $detalleResumen->id,
            "nombre"        => $libro->nombre,
            "isbn"          => $libro->isbn,
	        'precio'        => $detalleResumen->precio_libro,
            'stock'         => $libroColegio->colegios->first()->pivot->stock,
            'bajo_pedido'   => $libroColegio->colegios->first()->pivot->bajo_pedido,
            //'bajo_pedido'   => $libro->paquetes->first()->pivot->bajo_pedido,
            'cantidad'      => $detalleResumen->cantidad,
            'obligatorio'   => $libro->paquetes->first()->pivot->obligatorio,
            'activo'        => $libro->paquetes->first()->pivot->activo,
	    ];
	}

}
