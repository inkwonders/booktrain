<?php
namespace App\Transformers;

use App\Models\MetodoPago;
use League\Fractal;

class MetodoPagoTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [

    ];

	public function transform(MetodoPago $datos)
	{
	    return [
	        'id'      => (int) $datos->id,
	        'codigo'   => $datos->codigo,
            'descripcion'   => $datos->descripcion
	    ];
	}

}
