<?php
namespace App\Transformers;

use App\Transformers\DireccionTransformer;
use App\Models\Pedido;
use League\Fractal;

class UltimoPedidoTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'datosFacturacion','direccionEntrega'
    ];

    protected $defaultIncludes = [];

	public function transform(Pedido $pedido)
	{
	    return [
            'contacto' => [
                'nombre' => $pedido->nombre_contacto,
                'apellidos' => $pedido->apellidos_contacto,
                'celular' => $pedido->celular_contacto,
            ]
        ];
	}

    // ------------------------------------------------
    public function includeDatosFacturacion(Pedido $pedido)
    {
        if($pedido != null && $pedido->user->datosFacturacion->count() > 0)
            return $this->item(
                $pedido->user->datosFacturacion->last(), new DatosFacturasTransformer($pedido)
            );
        else
            return $this->null();
    }

    public function includeDireccionEntrega(Pedido $pedido)
    {
        if($pedido->envio != null)
            return $this->item(
                $pedido->direccion, new DireccionTransformer()
            );
        else
            return $this->null();
    }
}
