<?php
namespace App\Transformers;

use App\Models\DetalleResumenPedido;
use League\Fractal;

class DetallesResumenPedidoXLSTransformer extends Fractal\TransformerAbstract
{
    /** falta terminar de obtener datos del transformer */
	public function transform(DetalleResumenPedido $detalle)
	{
        if($detalle->resumenPedido->pedido->status != 'CARRITO')
            return [
                'no_pedido'             => $detalle->resumenPedido->pedido->serie . $detalle->resumenPedido->pedido->folio,
                'fecha_compra'          => date_format($detalle->resumenPedido->pedido->created_at,"d / m / Y"),
                'nombre_colegio'        => $detalle->resumenPedido->pedido->colegio->nombre,
                'seccion'               => $detalle->resumenPedido->paquete->nivelColegio->nombre,
                'isbn'                  => $detalle->libro->isbn,
                'titulo'                => $detalle->libro->nombre,
                'piezas'                => $detalle->cantidad,
                'nombre_alumno'         => $detalle->resumenPedido->nombre_alumno.' '.$detalle->resumenPedido->paterno_alumno.' '.$detalle->resumenPedido->materno_alumno,
                'surtido'               => ($detalle->resumenPedido->pedido->status != 'ENTREGADO' ? 'POR SURTIR' : 'SURTIDO'),
                'nombre_contacto'       => $detalle->resumenPedido->pedido->nombre_contacto.' '.$detalle->resumenPedido->pedido->apellidos_contacto,
                'telefono'              => ($detalle->resumenPedido->pedido->detalleFactura != null ? $detalle->resumenPedido->pedido->detalleFactura->telefono : ''),
                'correo'                => ($detalle->resumenPedido->pedido->detalleFactura != null ? $detalle->resumenPedido->pedido->detalleFactura->correo : ''),
                'rfc'                   => ($detalle->resumenPedido->pedido->detalleFactura != null ? $detalle->resumenPedido->pedido->detalleFactura->rfc : ''),
                'razon_social'          => ($detalle->resumenPedido->pedido->detalleFactura != null ? $detalle->resumenPedido->pedido->detalleFactura->razon_social : ''),
                'monto'                 => $detalle->precio_libro * $detalle->cantidad,
                'metodo_pago'           => $detalle->resumenPedido->pedido->metodo_pago,
                'forma_pago'            => $detalle->resumenPedido->pedido->forma_pago,
                'comision'              => $detalle->resumenPedido->pedido->comision,
                'costo_envio'           => ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->costo : ''),
                'factura'               => '',
                'tipo_entrega'          => $detalle->resumenPedido->pedido->tipo_entrega,
                'calle'                 => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->calle : '') ,
                'no_exterior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_exterior : '') ,
                'no_interior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_interior : ''),
                'colonia'               => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->colonia : ''),
                'cp'                    => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->cp : ''),
                'estado'                => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->estado->descripcion : ''),
                'municipio'             => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->municipio->descripcion : ''),
                'referencia'            => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->referencia : ''),
                'fecha_recepcion'       => ($detalle->resumenPedido->pedido->envio != null ? date_format($detalle->resumenPedido->pedido->envio->fecha_recepcion,"d / m / Y") : ''),
                'paqueteria'            => ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->paqueteria : ''),
                'guia'                  => ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->guia : '') ,
                'estatus_envio'         => ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->status : '') ,
                'nombre'                => '',
                'fecha'                 => ''
            ];
        else
            return [];
	}

}
