<?php
namespace App\Transformers;

use App\Models\DetalleResumenPedido;
use App\Models\Libro;
use App\Models\Pedido;
use App\Models\Colegio;
use App\Models\Paquete;
use App\Models\ResumenPedido;
use Database\Seeders\DetallesResumenPedidosSedder;
use League\Fractal;

class DetallesResumenPedidoColegioXLSTransformer extends Fractal\TransformerAbstract
{


        protected $inicio;
        protected $fin;
        protected $grado_id;
        protected $isbn_req;
        protected $colegio_id;

    function __construct($inicio,$fin,$grado_id,$isbn_req,$colegio_id)
    {
        $this->inicio = $inicio;
        $this->fin = $fin;
        $this->grado_id = $grado_id;
        $this->isbn_req = $isbn_req;
        $this->colegio_id = $colegio_id;
    }


    public function transform(Libro $libro)
	{

        $colegio = Colegio::find($this->colegio_id);

        $pedidos = $colegio->pedidos()
            ->where('created_at', '<', $this->fin)
            ->where('created_at', '>', $this->inicio)
            ->with(['resumenes' => function($query) {

                $query->whereHas('paquete', function($query) {
                    if($this->grado_id != 0 || $this->grado_id != '0')
                        $query->where('nivel_id', $this->grado_id);
                });

                $query->with(['detalles' => function ($query) {
                    $query->whereHas('libro', function($query) {
                        if($this->isbn_req != 0 || $this->isbn_req != '0')
                            $query->where('isbn', $this->isbn_req);
                    });
                }]);
            }]);

        // Para CADA LIBRO asignado a un colegio, obtener los pedidos en los que aparece entre dos fechas dadas
        // Contar la cantidad de pedidos en los que aparece
        // Punto de reorden en el colegio
        // Cantidad de pedidos que tengan el status entregado
        // Cantidad de pedidos que tengan el status cancelado
        // Acumulado del total de los pedidos
        // Acumulado del total de los pedidos cancelados
        // Acumulado del total de los pedidos pagado o entregado

        $libros_colegio = $colegio->libros()
            ->wherePivot('paquete_id', null)
            ->with([
                'paquetes.nivel.seccion' => function($query) use ($colegio) {
                    $query->where('colegio_id', $colegio->id);
                },
                'paquetes.resumenes.pedido' => function($query) use ($colegio) {
                    $query->where('colegio_id', $colegio->id);
                    $query->where('created_at', '<', $this->fin);
                    $query->where('created_at', '>', $this->inicio);
                }
            ])->get();

        $libros = $libros_colegio->transform(function($libro) {
            $pedidos = $libro->paquetes->pluck('resumenes')->collapse()->pluck('pedidos');
            $venta = $libro->paquetes->pluck('resumenes')->collapse()->whereNotIn('pedidos.status', ['PROCESANDO', 'CANCELADO']);
            $valor_pedido = $venta->sum('cantidad') * $libro->paquete->pivot->precio;
            $valor_devolucion = 0;
            return [
                'id'    => $libro->id,
                'isbn'  => $libro->isbn,
                'grado' => $libro->paquetes->first()->nivel->secciones()->first()->nombre,// Revisar más a fondo
                'precio_lista' => 0,
                'precio_alumno' => $libro->paquete->pivot->precio,
                'punto_reorden' => $libro->pivot->punto_reorden,
                'cantidad_pedidos' => $pedidos->count(),
                'cantidad_entrega' => $pedidos->where('status', 'ENTREGADO')->count(),
                'cantidad_devoluciones' => $pedidos->where('status', 'CANCELADO')->count(),
                'venta_mostrador' => 0,
                // Suma de libros de los resumenes que tengan pedidos pagados o entregados
                'venta_real' => $venta->sum('cantidad'),
                // 'venta_real' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count(),

                // El precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por la suma de libros en los resumenes cuyo pedido sea entregado o pagado
                'valor_pedido' => $valor_pedido,
                // 'valor_pedido' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio,

                // El precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por el numero de libros que tienen una entrada a su almacén por el concepto de devolución
                'valor_devolucion' => $valor_devolucion, // Se necesita registrar las entradas y salidas al almacén
                // 'valor_devolucion' => $pedidos->whereIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio,

                // Precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por la cantidad de libros en la venta real, menos la suma de devoluciones
                'valor_real_venta' => $valor_pedido - $valor_devolucion
                // 'valor_real_venta' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio
            ];
        });

dd($libros->toArray());
        //$pedidos = $pedidos->get();
        dd($pedidos->with('resumenes.detalles.libro')->get());


        // Obtener los pedidos de un libro dado un colegio entre dos fechas

        $colegio = Colegio::where('id', $this->colegio_id)->first();
        $pedidos = $colegio->pedidos()
        ->where('created_at', '<', $this->fin)
        ->where('created_at', '>', $this->inicio)
        ->with(['resumenes' => function($query)  use ($libro) {
            $query->with(['detalles' => function ($query)  use ($libro) {
                $query->whereHas('libro', function($query) use ($libro) {
                    if($libro->isbn != 0 || $libro->isbn != '0')
                        $query->where('isbn', $libro->isbn);
                })->count();
            }]);
        }]);

        $detalle_resumen_cantidad = DetalleResumenPedido::where('libro_id', $libro->id)->whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])->get()->unique('pedido_id')->count();
        $detalle_resumen_conteo = DetalleResumenPedido::where('libro_id', $libro->id)->whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])->get()->count();

        return [
            'id'                    => (int) $libro->id,
            'isbn'                  => $libro->isbn,
            'editorial'             => $libro->editorial,
            // 'nombre_colegio'        => $this->colegio->nombre,
            'titulo'                => $libro->nombre,
            'obligatorio'           => $libro->paquetes()->first()->pivot->obligatorio  != null ? 'OBLIGATORIO' : 'NO OBLIGATORIO',
            'Grado'                 => $libro->paquetes()->first()->nivelColegio->nombre,
            'precio_lista'          => '', //en blanco
            'precio_alumno'         =>  $libro->paquetes()->first()->pivot->precio,
            'descuento_colegio_alumno'   => '',//en blanco

            'pedidos_cantidad'       =>  $detalle_resumen_conteo,
            'punto_reorden'          => $libro->paquetes()->first()->pivot->punto_reorden != null ? $libro->paquetes()->first()->pivot->punto_reorden : '0',

            'entrega_cantidad'       => $pedidos->get()->where('status','ENTREGADO')->count(),//campo desconocido de donde proviene
            'venta_mostrador'         =>  '', //aun no se cuenta con este campo
            'devoluciones_cantidad'   => '',//campo desconocido de donde proviene

            'venta_real'              => $detalle_resumen_cantidad,
            'valor_pedido'            => number_format($libro->paquetes->first()->pivot->precio * $detalle_resumen_cantidad, 2),
            'valor_devolucion'        => ResumenPedido::where('paquete_id', $libro->paquetes()->first()->pivot->id)->first(),
            'valor_real_venta'        => number_format($libro->paquetes->first()->pivot->precio * $detalle_resumen_cantidad, 2),
            'descuento_otorgado'      => '',//en blanco
        ];

	}
}
