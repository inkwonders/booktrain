<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\LibrosColegios;

class LibrosColegiosTransformer extends Fractal\TransformerAbstract
{

    protected $defaultIncludes = [
        'informacion'
    ];

	public function transform(LibrosColegios $libroColegio)
	{
	    return [
	        'id'      => (int) $libroColegio->id,
	        'precio'   => $libroColegio->precio,
            'colegio_id'   => $libroColegio->colegio_id,
            'paquete_id'   => $libroColegio->paquete_id,
            'obligatorio'   => $libroColegio->obligatorio,
            'activo'   => $libroColegio->activo
	    ];
	}


    public function includeInformacion(LibrosColegios $libroColegio)
    {
        return $this->Item($libroColegio->datosLibro, new LibrosTransformer($libroColegio->colegio_id));
    }

}
