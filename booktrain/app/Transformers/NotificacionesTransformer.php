<?php
namespace App\Transformers;

use App\Models\Notificacion;
use App\Models\UsoCFDI;
use League\Fractal;

class NotificacionesTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [

    ];

	public function transform(Notificacion $notificacion)
	{
	    return [
	        'id'      => (int) $notificacion->id,
	        'texto'   => $notificacion->aviso,
	    ];
	}

}
