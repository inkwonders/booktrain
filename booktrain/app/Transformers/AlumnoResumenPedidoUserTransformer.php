<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\ResumenPedido;

class AlumnoResumenPedidoUserTransformer extends Fractal\TransformerAbstract
{
	public function transform(ResumenPedido $detalle)
	{
	    return [
	        'nombre'             => $detalle->nombre_alumno,
	        'apellido_paterno'   => $detalle->paterno_alumno,
	        'apellido_materno'   => $detalle->materno_alumno,
	    ];
	}
}
