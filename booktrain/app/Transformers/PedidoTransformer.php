<?php
namespace App\Transformers;

use App\Models\Pedido;
use Auth;
use League\Fractal;

class PedidoTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'resumenes', 'referencia10'
    ];

	public function transform(Pedido $pedido)
	{
	    return [
	        'id'                => (int) $pedido->id,
            'subtotal'          => $pedido->subtotal,
            'total'             => $pedido->total,
            'usuario_id'        => $pedido->usuario_id,
            'colegio_id'        => $pedido->colegio_id,
            'colegio_codigo'    => $pedido->colegio->codigo,
            'nombre_colegio'    => $pedido->colegio->nombre,
            'email'             => Auth::user()->email
	    ];
	}

    public function includeResumenes(Pedido $pedido)
    {
        return $this->Collection($pedido->resumenes, new ResumenesPedidoTransformer($pedido));
    }

    public function includeReferencia10(Pedido $pedido) {
        return $this->primitive($pedido->referencia_algoritmo10_b_b_v_a);
    }

}
