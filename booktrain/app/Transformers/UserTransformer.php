<?php
namespace App\Transformers;

use App\Models\User;
use App\Models\ResumenPedido;
use League\Fractal;

class UserTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'alumnos_resumen_pedido',
        'direccion_ultimo_pedido',
        'datos_facturacion_ultimo_pedido'
    ];

	public function transform(User $user)
	{
	    return [
	        'id'      => (int) $user->id,
	        'nombre'   => $user->name,
            'apellido'   => $user->apellidos,
            'correo'   => $user->email
	    ];
	}

    public function includeAlumnosResumenPedido(User $user) {
        $ultimo_pedido = $user->pedidos->where('status', '!=', 'CANCELADO')->where('status', '!=', 'CARRITO')->sortByDesc('id')->first();

        if (is_null($ultimo_pedido)) {
            return null;
        }

        $resumenes = $ultimo_pedido->resumenes;

        return $this->collection($resumenes, new AlumnoResumenPedidoUserTransformer);
    }

    public function includeDireccionUltimoPedido(User $user) {

        $ultimo_pedido = $user->pedidos->where('status', '!=', 'CANCELADO')->where('status', '!=', 'CARRITO')->where('tipo_entrega', 2)->sortByDesc('id')->first();

        if (is_null($ultimo_pedido)) {
            return null;
        }

        return $this->item($ultimo_pedido->direccion, new DireccionTransformer);

    }

    public function includeDatosFacturacionUltimoPedido(User $user) {
        if($user != null && $user->datosFacturacion->count() > 0)
            return $this->item($user->datosFacturacion->last(), new DatosFacturasTransformer);
        else
            return null;
    }
}
