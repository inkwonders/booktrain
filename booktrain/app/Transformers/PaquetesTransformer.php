<?php
namespace App\Transformers;

use App\Models\NivelColegio;
use App\Models\Paquete;
use League\Fractal;
use App\Transformers\LibrosColegiosTransformer;

class PaquetesTransformer extends Fractal\TransformerAbstract
{

    protected $defaultIncludes = [
        'libros'
    ];

	public function transform(Paquete $paquete)
	{
	    return [
	        'id'       => (int) $paquete->id,
	        'nombre'   => $paquete->nombre,
            'activo'   => $paquete->activo,
            'nivel_id' => $paquete->nivel_id,
	    ];
	}

    public function includeLibros(Paquete $paquete)
    {
        return $this->Collection(
            //$paquete->libros()->wherePivot('activo', 1)->get(), new LibrosTransformer($paquete->nivelColegio->seccionColegio->colegio->id, $paquete->id)
            $paquete->libros()->wherePivot('activo', 1)->get(), new LibrosTransformer($paquete->nivelColegio->seccionColegio->colegio_id, $paquete->id)
        );
    }

}
