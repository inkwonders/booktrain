<?php
namespace App\Transformers;

use App\Models\TicketEntrada;
use League\Fractal;

class TicketEntradaTransformer extends Fractal\TransformerAbstract
{

    protected $availableIncludes = [
        'usuario'
    ];

    protected $defaultIncludes = [
        'archivos'
    ];

	public function transform(TicketEntrada $ticketEntrada)
	{
	    return [
	        'id'      => (int) $ticketEntrada->id,
            'tipo'   => $ticketEntrada->tipo_entrada,
	        'comentario'   => $ticketEntrada->comentario,
            'alta'   => $ticketEntrada->created_at
	    ];
	}

    public function includeUsuario(TicketEntrada $ticketEntrada)
    {
        return $this->Item($ticketEntrada->user, new UserTransformer($ticketEntrada));
    }

    public function includeArchivos(TicketEntrada $ticketEntrada)
    {
        return $this->Collection($ticketEntrada->ticketEntradaArchivos, new TicketEntradaArchivoTransformer($ticketEntrada));
    }

}
