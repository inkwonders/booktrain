<?php

namespace App\Transformers;

use App\Models\NivelColegio;
use League\Fractal;

class NivelesColegioTransformer extends Fractal\TransformerAbstract
{

    protected $defaultIncludes = [
        'paquetes'
    ];

    public function transform(NivelColegio $nivel)
    {
        return [
            'id'      => (int) $nivel->id,
            'nombre'   => $nivel->nombre
        ];
    }


    public function includePaquetes(NivelColegio $nivel)
    {
        return $this->Collection($nivel->paquetes, new PaquetesTransformer($nivel));
    }
}
