<?php
namespace App\Transformers;

use App\Models\Pedido;
use League\Fractal;

class PedidoPagadoTransformer extends Fractal\TransformerAbstract
{

	public function transform(Pedido $pedido)
	{
	    return [
	        'id'      => (int) $pedido->id,
            'pedido'  => $pedido->serie . $pedido->folio,
            'correo'  => $pedido->user->email,
	    ];
	}

}
