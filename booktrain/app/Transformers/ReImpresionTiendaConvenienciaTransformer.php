<?php
namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal;
use App\Models\Pedido;
use Luecano\NumeroALetras\NumeroALetras;

class ReImpresionTiendaConvenienciaTransformer extends Fractal\TransformerAbstract
{
	public function transform(Pedido $pedido)
	{
	    return [
            'fecha_limite'             => Carbon::parse($pedido->updated_at)->addDays(2)->format('d/m/Y'),
	        'nombre'                   => $pedido->user->name.' '.$pedido->user->apellidos,
            'monto'                    => number_format($pedido->total, 2),
            'monto_letra'              => $pedido->total_letra,
            'referencia'               => $pedido->referencia,
            'codigo_producto'          => $pedido->serie.$pedido->folio,
            'colegio_id'               => $pedido->colegio->id,
            'colegio_codigo'           => $pedido->colegio->codigo
	    ];
	}

}
