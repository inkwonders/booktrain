<?php
namespace App\Transformers;

use App\Models\UsoCFDI;
use League\Fractal;

class UsoCFDITransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [

    ];

	public function transform(UsoCFDI $datos)
	{
	    return [
	        'id'      => (int) $datos->id,
	        'codigo'   => $datos->codigo,
            'descripcion'   => $datos->descripcion
	    ];
	}

}
