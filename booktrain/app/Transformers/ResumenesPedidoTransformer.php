<?php
namespace App\Transformers;
use App\Models\ResumenPedido;
use League\Fractal;

class ResumenesPedidoTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
        'detalles'
    ];



	public function transform(ResumenPedido $resumenPedido)
	{
	    return [
	        'id'                => (int) $resumenPedido->id,
            "grado"             => $resumenPedido->paquete->nivelColegio->nombre,
            "seccion"           => $resumenPedido->paquete->nivelColegio->seccionColegio->nombre,
            "nombre_paquete"    => $resumenPedido->paquete->nombre,
            'subtotal'          => $resumenPedido->subtotal,
            'alumno'            => [
                                    'nombre'    => $resumenPedido->nombre_alumno,
                                    'paterno'   => $resumenPedido->paterno_alumno,
                                    'materno'   => $resumenPedido->materno_alumno
                                ],
	    ];
	}

    public function includeDetalles(ResumenPedido $resumenPedido)
    {
        return $this->Collection($resumenPedido->detalles, new DetallesResumenPedidoTransformer( $resumenPedido->paquete->id, $resumenPedido->pedido->colegio_id ));
    }

}
