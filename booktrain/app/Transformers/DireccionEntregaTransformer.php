<?php
namespace App\Transformers;

use App\Models\DireccionEntrega;
use League\Fractal;

class DireccionEntregaTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [

    ];

    protected $defaultIncludes = [
        'estado', 'municipio'
    ];

	public function transform(DireccionEntrega $direccion)
	{
	    return [
	        'id'      => (int) $direccion->id,
	        'calle'   => $direccion->calle,
            'no_exterior'   => $direccion->no_exterior,
            'no_interior'   => $direccion->no_interior,
            'colonia'   => $direccion->colonia,
            'cp'   => $direccion->cp
	    ];
	}

    public function includeEstado(DireccionEntrega $direccion)
    {
        return $this->item($direccion->estado, new EstadoTransformer($direccion));
    }

    public function includeMunicipio(DireccionEntrega $direccion)
    {
        return $this->item($direccion->municipio, new MunicipioTransformer($direccion));
    }

}
