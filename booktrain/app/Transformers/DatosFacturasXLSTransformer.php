<?php
namespace App\Transformers;

use App\Models\DatosFacturas;
use League\Fractal;

class DatosFacturasXLSTransformer extends Fractal\TransformerAbstract
{

	public function transform(DatosFacturas $datos)
	{
	    return [
	        // 'id'      => (int) $datos->id,
            'no_pedido'    => $datos->pedido->serie . $datos->pedido->folio,
            'fecha_compra' => date_format($datos->created_at,"d / m / Y"),
            'subtotal'     => number_format($datos->pedido->subtotal, 2),
            'costo_envio'  => ($datos->pedido->envio != null ? number_format($datos->pedido->envio->costo, 2) : '') ,
            'comision'     => number_format($datos->pedido->comision),
            'total'        => number_format($datos->pedido->total, 2),
            // 'forma_pago' => $datos->forma_pago,
            // 'metodo_pago' => $datos->metodo_pago,
            'forma_pago'   => $datos->metodo_pago,
            'metodo_pago'  => $datos->pedido->metodo_pago,
            'nombre_colegio' => $datos->pedido->colegio->nombre,
            'uso_cfdi' => $datos->cfdis->descripcion,
	        'razon_social'   => $datos->razon_social,
            'rfc'   => $datos->rfc,
            'correo'   => $datos->correo,
            'telefono'   => $datos->pedido->celular_contacto,
	    ];
	}

}
