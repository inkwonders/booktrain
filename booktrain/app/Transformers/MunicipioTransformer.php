<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\Municipio;

class MunicipioTransformer extends Fractal\TransformerAbstract
{

	public function transform(Municipio $municipio)
	{
	    return [
	        'id'      => (int) $municipio->id,
	        'nombre'   => $municipio->descripcion,
            // 'latitud'   => $municipio->lat,
            // 'longitud'   => $municipio->lon
	    ];
	}




}
