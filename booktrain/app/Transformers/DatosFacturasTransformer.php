<?php
namespace App\Transformers;

use App\Models\DatosFacturas;
use App\Models\FormaPago;
use League\Fractal;

class DatosFacturasTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'usuario'
    ];

    protected $defaultIncludes = [
        'usoCFDI','metodoPago','formaPago'
    ];

	public function transform(DatosFacturas $datos)
	{
	    return [
	        'id'      => (int) $datos->id,
	        'razon_social'   => $datos->razon_social,
            'rfc'   => $datos->rfc,
            'correo'   => $datos->correo,
            'cp' => $datos->cp
	    ];
	}

    public function includeUsoCFDI(DatosFacturas $datos)
    {
        return $this->item($datos->cfdis, new UsoCFDITransformer($datos));
    }

    public function includeMetodoPago(DatosFacturas $datos)
    {
        return $this->primitive($datos->metodo_pago);
        // return $this->item($datos->metodoPago, new MetodoPagoTransformer($datos));
    }

    public function includeFormaPago(DatosFacturas $datos)
    {
        return $this->primitive($datos->forma_pago);
        // return $this->item($datos->formaPago, new FormaPagoTransformer($datos));
    }

    public function includeUsuario(DatosFacturas $datos)
    {
        return $this->Item($datos->user, new UserTransformer($datos));
    }

    // public function includeContactoUltimoPedido(DatosFacturas $datos)
    // {
    //     return $this->Item($datos->user, new UserTransformer($datos));
    // }

    public function includeDireccionUltimoPedido(DatosFacturas $datos)
    {

        if(!empty($datos->user)){
            return $this->Item($datos->user, new UserTransformer($datos));
        }else{
            return null;
        }
    }
}