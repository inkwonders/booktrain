<?php
namespace App\Transformers;

use App\Models\TicketMotivo;
use League\Fractal;

class OpcionesTicketTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [];
    protected $defaultIncludes = [];

	public function transform(TicketMotivo $motivo)
	{
	    return [
	        'id'            => (int) $motivo->id,
            'texto'         => $motivo->nombre,
	        'habilitado'    => true
	    ];
	}
}