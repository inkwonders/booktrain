<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\SeccionColegio;

class SeccionesColegioTransformer extends Fractal\TransformerAbstract
{

    protected $defaultIncludes = [
        'niveles'
    ];

    public function transform(SeccionColegio $seccion)
	{

	    return [
	        'id'       => (int) $seccion->id,
	        'nombre'   => $seccion->nombre
	    ];
	}

    public function includeNiveles(SeccionColegio $seccion)
    {
        return $this->Collection($seccion->niveles->where('activo', 1), new NivelesColegioTransformer($seccion));
    }

}
