<?php
namespace App\Transformers;

use League\Fractal;
use App\Models\TicketEntradaArchivo;

class TicketEntradaArchivoTransformer extends Fractal\TransformerAbstract
{

	public function transform(TicketEntradaArchivo $ticketArchivo)
	{
	    return [
	        'id'      => (int) $ticketArchivo->id,
	        'archivo'   => $ticketArchivo->url,
            'nombre'   => $ticketArchivo->archivo_nombre_original,
            'tipo'   => $ticketArchivo->tipo_archivo,
            'orden'   => $ticketArchivo->orden
	    ];
	}

}
