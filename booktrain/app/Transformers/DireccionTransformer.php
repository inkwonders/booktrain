<?php
namespace App\Transformers;

use App\Models\Direccion;
use League\Fractal;

class DireccionTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [];

    protected $defaultIncludes = ['estado', 'municipio'];

	public function transform(Direccion $direccion)
	{
	    return [
            "id"                    => $direccion->id,
            "direccion_completa"    => $direccion->direccionCompleta,
            "calle"                 => $direccion->calle,
            "no_interior"           => $direccion->no_interior,
            "no_exterior"           => $direccion->no_exterior,
            "colonia"               => $direccion->colonia,
            "cp"                    => $direccion->cp,
            "estado_id"             => $direccion->estado_id,
            "municipio_id"          => $direccion->municipio_id,
            'referencia'            => $direccion->referencia
        ];
	}

    public function includeEstado(Direccion $direccion)
    {
        return $this->item($direccion->estado, new EstadoTransformer($direccion));
    }

    public function includeMunicipio(Direccion $direccion)
    {
        return $this->item($direccion->municipio, new MunicipioTransformer($direccion));
    }
}
