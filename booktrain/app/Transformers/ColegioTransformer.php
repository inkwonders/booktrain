<?php
namespace App\Transformers;

use App\Models\Colegio;
use League\Fractal;

class ColegioTransformer extends Fractal\TransformerAbstract
{

    protected $availableIncludes = [
        'secciones'
    ];

    protected $defaultIncludes = [
        'direcciones_entrega','direcciones_pickup'
    ];

	public function transform(Colegio $colegio)
	{
        // $configuraciones = $colegio->configuraciones()
        //     ->withCount('configuraciones')
        //     ->doesntHave('configuracionPadre')
        //     ->get()
        //     ->keyBy('etiqueta')
        //     ->transform(function($config) {
        //         if($config->configuraciones_count == 0)
        //             return $config->valor;

        //         return $config->configuraciones->keyBy('etiqueta')->map(function($c) {
        //             return $c->valor;
        //         });
        //     });

        $configuracion = $colegio->configuracion;


        // Quitamos los elementos de los métodos de pago que no estén activos
        // foreach ($configuracion->metodos_pago as $key_metodo_pago => $metodo_pago) {
        //     if(isset($metodo_pago->activo))
        //         if($metodo_pago->activo == false)
        //             unset($configuracion->metodos_pago[$key_metodo_pago]);


        //     foreach ($metodo_pago->formas_pago as $key_forma_pago => $forma_pago) {
        //         if(isset($forma_pago->activo))
        //             if($forma_pago->activo == false)
        //                 unset($configuracion->metodos_pago[$key_metodo_pago]->formas_pago[$key_forma_pago]);
        //     }
        // }

        // $direcciones = $colegio->direccionesEntrega->filter(function($direccion) {
        //     if(isset($direccion->pivot->activo))
        //         if($direccion->pivot->activo == 0) return false;

        //     return true;
        // })->partition(function($e){return $e->pivot->tipo == 'COLEGIO'; });

        // $direcciones->first() // COLEGIO
        // $direcciones->last() // PICK UP

	    return [
	        'id'            => (int) $colegio->id,
            'codigo'        => $colegio->codigo,
	        'nombre'        => $colegio->nombre,
            'configuracion' => $configuracion,
            'logo'          => $colegio->logo // url('/assets/img/' . $colegio->logo)
	    ];
	}

    public function includeSecciones(Colegio $colegio)
    {
        return $this->Collection($colegio->secciones->where('activo', 1), new SeccionesColegioTransformer($colegio));
    }

    public function includeDireccionesEntrega(Colegio $colegio)
    {
        //$direcciones = $colegio->direccionesEntrega()->wherePivot('tipo','COLEGIO')->get();
        $direcciones = $colegio->direccionesEntrega->filter(function($direccion) {
            if(isset($direccion->pivot->activo))
                if($direccion->pivot->activo == 0) return false;

            return true;
        })->partition(function($e) {
            return $e->pivot->tipo == 'COLEGIO';
        })->first();

        return $this->Collection($direcciones, new DireccionTransformer());
    }

    public function includeDireccionesPickup(Colegio $colegio)
    {
        //$direcciones = $colegio->direccionesEntrega()->wherePivot('tipo','PICK UP')->get();
        $direcciones = $colegio->direccionesEntrega->filter(function($direccion) {
            if(isset($direccion->pivot->activo))
                if($direccion->pivot->activo == 0) return false;

            return true;
        })->partition(function($e) {
            return $e->pivot->tipo == 'PICK UP';
        })->first();

        return $this->Collection($direcciones, new DireccionTransformer());
    }

}
