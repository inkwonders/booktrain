<?php
namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal;
use App\Models\Pedido;
use Luecano\NumeroALetras\NumeroALetras;

class ReferenciaPedidosTransformer extends Fractal\TransformerAbstract
{
    protected $availableIncludes = [
        'referencia_interbancaria', 'referencia_tienda'
    ];

	public function transform(Pedido $pedido)
	{
	    return [
            'fecha_limite'             => Carbon::parse($pedido->updated_at)->addDays(2)->format('d/m/Y'),
	        'nombre'                   => $pedido->user->name.' '.$pedido->user->apellidos,
            'monto'                    => number_format($pedido->total, 2),
            'monto_letra'              => $pedido->total_letra,
            'nombre_cuenta'            => $pedido->colegio->config('nombre_cuenta'),
            'nombre_banco'             => $pedido->colegio->config('nombre_banco'),
            'ventanilla_otros_bancos'  => $pedido->colegio->config('ventanilla_otros_bancos'),
            'portal_bbva'              => $pedido->colegio->config('portal_bbva'),
            'portal_otros_bancos'      => $pedido->colegio->config('portal_otros_bancos'),
            'numero_contrato'          => $pedido->colegio->config('numero_contrato'),
            'numero_contrato_clabe'    => $pedido->colegio->config('numero_contrato_clabe'),
            'codigo_producto'          => $pedido->serie.$pedido->folio,
	    ];
	}

    public function includeReferenciaInterbancaria(Pedido $pedido) {
        return $this->primitive($pedido->generarReferenciaAlgoritmo10BBVA());
    }

    public function includeReferenciaTienda(Pedido $pedido) {
        return $this->primitive(
            $pedido->referencia()->where('tipo', 'TIENDA')->first()->referencia
        );
    }
}
