<?php

namespace App\Events;

use App\Models\Colegio;
use App\Models\Configuracion;
use App\Models\MetodoPago;
use App\Models\FormaPago;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ColegioCreado
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Colegio $colegio)
    {
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'nombre_cuenta','valor' => Configuracion::base()->where('etiqueta', 'nombre_cuenta')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'nombre_banco','valor' => Configuracion::base()->where('etiqueta', 'nombre_banco')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_cuenta','valor' => Configuracion::base()->where('etiqueta', 'numero_cuenta')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'clabe','valor' => Configuracion::base()->where('etiqueta', 'clabe')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'ventanilla_otros_bancos','valor' => Configuracion::base()->where('etiqueta', 'ventanilla_otros_bancos')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'portal_bbva','valor' => Configuracion::base()->where('etiqueta', 'portal_bbva')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'portal_otros_bancos','valor' => Configuracion::base()->where('etiqueta', 'portal_otros_bancos')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_contrato','valor' => Configuracion::base()->where('etiqueta', 'numero_contrato')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_contrato_clabe','valor' => Configuracion::base()->where('etiqueta', 'numero_contrato_clabe')->first()->valor]);
        $colegio->configuraciones()->create(['categoria' => 'ENVIOS','etiqueta' => 'costo_envio','valor' => 0]);
        $colegio->configuraciones()->create(['categoria' => 'ENVIOS','etiqueta' => 'envio_domicilio_disponible','valor' => 0]);
        $colegio->configuraciones()->create(['categoria' => 'OTRO', 'etiqueta' => 'correo_comercial_provesa', 'valor' => '']);
        $colegio->configuraciones()->create(['categoria' => 'OTRO', 'etiqueta' => 'correo_coordinador_colegio', 'valor' => '']);
        $colegio->configuraciones()->create(['categoria' => 'OTRO', 'etiqueta' => 'correo_coordinador_provesa', 'valor' => '']);

        $metodos_pago_base = MetodoPago::all();
        $formas_pago_base = FormaPago::all();

        foreach ($metodos_pago_base as $metodo_pago_base) {
            // Agregamos los metodos de pago al colegio
            $colegio->metodosPago()
                ->attach($metodo_pago_base->id, [
                    'activo' => true,
                    'contexto' => in_array ($metodo_pago_base->id, [6, 7, 8]) ? 'PRESENCIAL' : 'ONLINE'
                ]);

            // Si el colegio no tiene registrado el metodo de pago, entonces lo cargamos con todas las formas de pago base desactivadas
            foreach ($formas_pago_base as $forma_pago_base) {
                switch ($forma_pago_base->codigo) {
                    case 'PUE':
                        $colegio->metodosPago()
                        ->wherePivot('metodo_pago_id', $metodo_pago_base->id)
                        ->first()
                        ->formasPago()
                            ->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => false]);
                    break;
                    case 'DIFERIDO3M':
                        if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                            $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 3, 'comision' => 2, 'minimo' => 300, 'activo' => false]);
                    break;
                    case 'DIFERIDO6M':
                        if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                            $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 6, 'comision' => 6, 'minimo' => 600, 'activo' => false]);
                    break;
                    case 'DIFERIDO9M':
                        if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                            $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 9, 'comision' => 10, 'minimo' => 900, 'activo' => false]);
                    break;
                }
            }
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }
}
