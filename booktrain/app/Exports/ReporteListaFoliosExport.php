<?php

namespace App\Exports;

use App\Models\Pedido;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReporteListaFoliosExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    private $pedidos;
    private $status;
    private $factura;
    private $metodo_pagos;
    private $forma_pagos;
    private $forma_entrega;
    private $inicio;
    private $fin;


    public function __construct(/*$inicio , $fin*/)
    {
        /*$this->inicio = $inicio;
        $this->fin = $fin;*/

    }

    public function headings(): array
    {
        return [
            [
                'Fecha',
                'Folio',
                'Total',
                'Estatus',
                'Colegio',
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_columns_yellow = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],

                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'FFE080']
                    ],

                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];



                // at row 1, insert 2 rows
                //$event->sheet->insertNewRowBefore(1,2);

                // merge cells for full-width
                //$event->sheet->mergeCells('A1:O1');
                // $event->sheet->mergeCells('A2:J2');
                //$event->sheet->mergeCells('A2:O2');


                //$event->sheet->setCellValue('A1', 'REPORTE FENERAL DE PEDIDOS ' .$this->inicio .' a '.$this->fin .'.');


                // assign cell styles
                //$event->sheet->getStyle('A1')->applyFromArray($styles_negritas);



                //$event->sheet->getStyle('A2:L2')->applyFromArray($bordes);
                $event->sheet->getStyle('A1:M1')->applyFromArray($styles_subheaders);

            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $last_folio = Pedido::whereNotNull('folio')->get()->last()->folio;

        $array_pedidos = [];
        $count = 1;

        while ($last_folio >= 2001) {

            $pedido = Pedido::where('folio', $last_folio)->first();

            $response = [
                'fecha'          => $count,
                'folio'          => "PR" . $last_folio,
                'total'          => '',
                'estatus'        => '',
                'colegio'        => '',
            ];

            if (!is_null($pedido)) {
                $response = [
                    'fecha'          => $pedido->created_at,
                    'folio'          => $pedido->serie . $pedido->folio,
                    'total'          => number_format($pedido->total, 2),
                    'estatus'        => $pedido->status,
                    'colegio'        => $pedido->colegio->nombre
                ];

                /*$response = [
                    'fecha'          => '',
                    'folio'          => "PR-" . $last_folio,
                    'total'          => '',
                    'estatus'        => '',
                    'colegio'        => '',
                ];*/
            } else {
                $count += 1;
            }

            $last_folio -= 1;


            array_push($array_pedidos, $response);
        }

        return collect($array_pedidos);
    }
}
