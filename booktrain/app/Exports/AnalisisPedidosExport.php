<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class AnalisisPedidosExport implements
    FromCollection,
    WithStyles,
    ShouldAutoSize,
    WithStrictNullComparison
{

    public $pedidos;

    public function __construct($pedidos)
    {
        $this->pedidos = $pedidos;
    }

    public function columnWidths(): array
    {
        return [
            'P' => 200,
            'U' => 200,
            'V' => 200,
        ];
    }

    public function styles(Worksheet $sheet) : array
    {
        $sheet->mergeCells('A1:G1');
        $sheet->mergeCells('H1:J1');
        $sheet->mergeCells('K1:P1');
        $sheet->mergeCells('Q1:S1');
        $sheet->mergeCells('T1:U1');


        return [
            'A'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'bdc0bf']
                ]
            ],
            'A1'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'bdc0bf']
                ]
            ],
            'A2:G2'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => '56c1fe']
                ]
            ],
            'H2:J2'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => '72fce9']
                ]
            ],
            'K2:P2'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => '88f94e']
                ]
            ],
            'Q2:S2'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'fff056']
                ]
            ],
            'T2:V2'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ff968c']
                ]
            ]
        ];
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                'Pedido', // A
                '', // B
                '', // C
                '', // D
                '', // E
                '', // F
                '', // G
                'Referencias pedidos', // H
                '', // I
                '', // J
                'Registros pagos', // K
                '', // L
                '', // M
                '', // N
                '', // O
                '', // P
                'Netpay', // Q
                '', // R
                '', // S
                'Errores', // T
                '', // U
                'Log', // V
            ],
            [
                'Folio',                  // A
                'Colegio',                // B
                'Status',                 // C
                'Total',                  // D
                'Metodo Pago',            // E
                'Forma Pago',             // F
                'Fecha Orden',            // G
                'Referencia',             // H
                'Tipo',                   // I
                'Pertenece al pedido',    // J
                'Origen',                 // K
                'Monto',                  // L
                'Status',                 // M
                'Concide',                // N
                'Fecha de Pago',          // O
                'Raw',                    // P
                'Status',                 // Q
                'Merchan Reference Code', // R
                'Pertenece al Pedido',    // S
                '#Errores',               // T
                'Descripcion',            // U
                'Logs',                   // V
            ],
            $this->pedidos
        ]);
    }
}
