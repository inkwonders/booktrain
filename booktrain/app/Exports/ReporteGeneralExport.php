<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReporteGeneralExport implements
    FromCollection,
    WithStyles,
    ShouldAutoSize
{
    private $pedidos;

    public function __construct($pedidos, $fecha_inicial, $fecha_final, $nombre_colegio, $nombre_grado, $nombre_isbn, $estado_pedido)
    {
        $this->descripcion = "REPORTE DE {$fecha_inicial} A {$fecha_final}, {$nombre_colegio}, {$nombre_grado}, {$nombre_isbn}";
        $this->pedidos = $pedidos;
    }

    public function properties(): array
    {
        return [
            'creator'        => 'Booktrain',
            // 'lastModifiedBy' => 'Patrick Brouwers',
            'title'          => 'Reporte general',
            'description'    => $this->descripcion,
            // 'subject'        => 'Invoices',
            'keywords'       => 'reporte, general, booktrain',
            // 'category'       => 'Invoices',
            // 'manager'        => 'Patrick Brouwers',
            'company'        => 'Booktrain',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A1:AI1');
        $sheet->mergeCells('A2:H2');
        $sheet->mergeCells('I2:M2');
        $sheet->mergeCells('N2:AC2');
        $sheet->mergeCells('AD2:AN2');
        $sheet->mergeCells('AO2:AQ2');

        return [
            // Style the first row as bold text.
            'V'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            'AP' => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            'AQ' => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            1    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_NONE],
                    'font' => ['bold' => true]],
            2    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER],
                    'font' => ['bold' => true],
                    ],
            3    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER],
                    'font' => ['bold' => true],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                        ]
                    ],
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                $this->descripcion
            ],
            [
                'PEDIDO','','','','','','','',
                'CONTACTO','','','','',
                'FACTURACION','','','','','','', '', '', '', '','','','','','',
                'DISTRIBUCIÓN','','','','','','','','','','',
                'COLEGIO / PICK UP',''
            ],
            [
                'No. Pedido',
                'Fecha',
                'Colegio',
                'Grado',
                'Sección',
                'ISBN',
                'Título',
                'Piezas',
                'Nombre alumno',
                'Estado pedido',
                'Entregado',
                'Nombre contacto',
                'Teléfono',
                'Correo',
                'RFC',
                'Razón social',
                'Correo',
                'Precio libro',
                'Método de pago',
                'Forma de pago',
                'Comisión',
                'Costo envío',
                'Factura*',

                'Calle',
                'Num. exterior',
                'Num. interior',
                'Colonia',
                'Municipio',
                'Codigo Postal',
                'Estado',


                'Tipo de entrega',
                'Dirección',
                'Colonia',
                'C.P',
                'Estado',
                'Municipio',
                'Referencia',
                'Fecha de envío',
                'Tipo de envío',
                'No. de guía',
                'Estado del envío',
                'Nombre*',
                'Fecha*',
            ],
            $this->pedidos->map(function($pedido) {
                return $pedido->resumenes->map(function($resumen, $index_resumen) use ($pedido) {
                    return $resumen->detalles->map(function($detalle, $index_detalle) use ($resumen, $index_resumen, $pedido) {
                        return [
                            "{$pedido->serie}{$pedido->folio}",
                            $pedido->created_at->format('d / m / Y'),
                            $pedido->colegio->nombre,
                            $resumen->paquete->nivel->nombre,
                            $resumen->paquete->nivel->seccion->nombre,
                            "{$detalle->libro->isbn} ",
                            $detalle->libro->nombre,
                            $detalle->cantidad,
                            $resumen->nombre_completo_alumno,
                            $pedido->status,
                            $detalle->entregado ==0 ? 'NO':'SI',
                            "{$pedido->nombre_contacto} {$pedido->apellidos_contacto}",
                            $pedido->celular_contacto,
                            $pedido->user->email,
                            isset($pedido->datosFactura) ? $pedido->datosFactura->rfc : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->razon_social : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->correo : '',
                            $detalle->precio_libro,
                            $pedido->metodo_pago,
                            $pedido->forma_pago,
                            ($index_resumen == 0 && $index_detalle == 0) ? $pedido->comision : '',
                            ($index_resumen == 0 && $index_detalle == 0 && $pedido->tipo_entrega == 2) ? (isset($pedido->envio) ? $pedido->envio->costo : '') : '',
                            '',

                            isset($pedido->datosFactura) ? $pedido->datosFactura->calle : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->num_exterior : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->num_interior : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->colonia : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->municipio : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->cp : '',
                            isset($pedido->datosFactura) ? $pedido->datosFactura->estado : '',

                            $pedido->descripcion_tipo_entrega,
                            isset($pedido->direccion) ? $pedido->direccion->calle . ' #' . $pedido->direccion->no_exterior . ($pedido->direccion->no_interior != '' ? ', Int. ' . $pedido->direccion->no_interior : '' ) : '',
                            isset($pedido->direccion) ? $pedido->direccion->colonia : '',
                            isset($pedido->direccion) ? $pedido->direccion->cp : '',
                            isset($pedido->direccion) ? $pedido->direccion->estado->descripcion : '',
                            isset($pedido->direccion) ? $pedido->direccion->municipio->descripcion : '',
                            isset($pedido->direccion) ? $pedido->direccion->referencia : '',
                            isset($pedido->envio) ? (isset($pedido->envio->fecha_envio) ? $pedido->envio->fecha_envio->format('d / m / Y') : '') : '',
                            isset($pedido->envio) ? $pedido->envio->paqueteria  : '',
                            isset($pedido->envio) ? $pedido->envio->guia  : '',
                            isset($pedido->envio) ? $pedido->envio->status  : '',
                            '',
                            '',
                        ];
                    });
                })->collapse();
            })->collapse()
        ]);
    }
}
