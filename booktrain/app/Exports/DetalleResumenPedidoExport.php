<?php

namespace App\Exports;

use League\Fractal\Manager;
use App\Transformers\Serializer;
use App\Models\DetalleResumenPedido;
use App\Models\Colegio;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Transformers\DetallesResumenPedidoXLSTransformer;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class DetalleResumenPedidoExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{
    private $inicio;
    private $fin;
    private $colegio_id;
    private $isbn_req;
    private $grado_id;
    private $estatus;

    public function __construct(  $inicio,   $fin,   $colegio_id,   $isbn_req,   $grado_id, $estatus)
    {
        $this->inicio = $inicio;
        $this->fin  = $fin;
        $this->colegio_id  = $colegio_id;
        $this->isbn_req  = $isbn_req;
        $this->grado_id  = $grado_id;
        $this->estatus  = $estatus;
    }

    public function headings(): array
    {
        return [
            [
                'No. Pedido',
                'Fecha de compra',
                'Nombre del Colegio',
                'Grado',
                'ISBN',
                'Título',
                'Piezas',
                'Nombre Alumno',
                'Estatus',
                'Nombre Contacto',
                'Teléfono',
                'Correo',
                'RFC',
                'Razon Social',
                'Monto',
                'Método de Pago',
                'Forma de Pago',
                'Comisión',
                'Costo envío',
                'Factura',
                'Tipo Entrega',
                'Calle',
                'No. exterior',
                'No. interior',
                'Colonia',
                'C. P.',
                'Estado',
                'Municipio',
                'Referencia',
                'Fecha Envío',
                'Tipo Envío',
                'No. Guía',
                'Estatus',
                'Nombre',
                'Fecha'
            ]
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING,
            'AF' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING,
            'AD' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_columns_yellow = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],

                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'FFE080']
                    ],

                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:AI1');
                $event->sheet->mergeCells('A2:I2');
                $event->sheet->mergeCells('J2:L2');
                $event->sheet->mergeCells('M2:T2');
                $event->sheet->mergeCells('U2:AC2');
                $event->sheet->mergeCells('AD2:AI2');

                // assign cell values
                if($this->grado_id == 0 || $this->grado_id == '0'){
                    $grado = 'TODOS';
                  }
                  else{
                    $grado = \App\Models\Paquete::where('id', $this->grado_id)->first()->nombre;
                  }
                $event->sheet->setCellValue('A1', 'REPORTE GENERAL '.$this->inicio .' a '.$this->fin .','. ($this->colegio_id != null ? \App\Models\Colegio::find($this->colegio_id)->first()->nombre : 'Todos').', Grado: '.$grado.' , ISBN: '.($this->isbn_req != 0 ? $this->isbn_req : 'TODOS').' .');
                $event->sheet->setCellValue('A2', 'PEDIDO');
                $event->sheet->setCellValue('J2', 'CONTACTO');
                $event->sheet->setCellValue('M2', 'FACTURACIÓN');
                $event->sheet->setCellValue('U2', 'DISTRIBUCIÓN');
                $event->sheet->setCellValue('AD2', 'COLEGIO / PICK UP');

                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);
                $event->sheet->getStyle('A2')->applyFromArray($styles_headers);
                $event->sheet->getStyle('J2')->applyFromArray($styles_headers);
                $event->sheet->getStyle('M2')->applyFromArray($styles_headers);
                $event->sheet->getStyle('U2')->applyFromArray($styles_headers);
                $event->sheet->getStyle('AD2:AI2')->applyFromArray($styles_headers);

                $event->sheet->getStyle('T4:T500')->applyFromArray($styles_columns_yellow);
                $event->sheet->getStyle('AH4:AH500')->applyFromArray($styles_columns_yellow);
                $event->sheet->getStyle('AI4:AI500')->applyFromArray($styles_columns_yellow);

                $event->sheet->getStyle('A2:AE2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:AI3')->applyFromArray($styles_subheaders);
            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {


        if( $this->colegio_id != '' ||  $this->colegio_id != ''){
            $colegio = Colegio::where('id',   $this->colegio_id)->first();
         }
         else
         {
            $colegio = '';
         }


        $detalles = DetalleResumenPedido::whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])

        ->whereHas('resumenPedido.pedido', function($query) use ($colegio){
            if( $this->colegio_id != '' ||  $this->colegio_id != ''){
                $query->where('colegio_id', $colegio->id);
             }
             if($this->estatus != '' || $this->estatus != ''){

                if($this->estatus == 0 || $this->estatus == '0')
                $query->where('status','=', 'PAGADO');
                if($this->estatus == 1 || $this->estatus == '1')
                $query->where('status','=', 'ENVIADO');
                if($this->estatus == 2 || $this->estatus == '2')
                $query->where('status','=', 'ENTREGADO');
                if($this->estatus == 3 || $this->estatus == '3')
                $query->where('status','=', 'ENVIO PARCIAL');
                if($this->estatus == 4 || $this->estatus == '4')
                $query->where('status','=', 'ENVIO LIQUIDADO');
             }
             else
             {
                $query->where('status','<>', 'CARRITO');
             }


        })
        ->whereHas('resumenPedido.detalles.libro', function ($query){
                if($this->isbn_req != 0 || $this->isbn_req != '0')
                    $query->where('isbn', $this->isbn_req);
        })
        ->whereHas('resumenPedido.detalles.libro.paquetes.nivel', function ($query){
                if($this->grado_id != 0 || $this->grado_id != '0')
                $query->where('nivel_id', $this->grado_id);
        });


        $detalles =  $detalles->get();


     if($this->isbn_req != "0"){

        return $detalles->transform(function($detalle)  {

            if($detalle->libro->isbn == $this->isbn_req){

                if($detalle->resumenPedido->pedido->tipo_entrega  == 1){
                    $tipo_entrega = 'Colegio';
                }

                else if ($detalle->resumenPedido->pedido->tipo_entrega  == 2){
                    $tipo_entrega = 'Domicilio';
                }

                else if ($detalle->resumenPedido->pedido->tipo_entrega  == 3){
                    $tipo_entrega = 'Pick-Up';
                }

                while ($detalle->libro->isbn === $this->isbn_req) {

                    return [

                        'no_pedido'             => $detalle->resumenPedido->pedido->serie . $detalle->resumenPedido->pedido->folio,
                        'fecha_compra'          => date_format($detalle->resumenPedido->pedido->created_at,"d / m / Y"),
                        'nombre_colegio'        => $detalle->resumenPedido->pedido->colegio->nombre,
                        'seccion'               => $detalle->resumenPedido->paquete->nivelColegio->nombre,
                        'isbn'                  => $detalle->libro->isbn,
                        'titulo'                => $detalle->libro->nombre,
                        'piezas'                => $detalle->cantidad,
                        'nombre_alumno'         => $detalle->resumenPedido->nombre_alumno.' '.$detalle->resumenPedido->paterno_alumno.' '.$detalle->resumenPedido->materno_alumno,
                        'estatus'               => $detalle->resumenPedido->pedido->status,
                        'nombre_contacto'       => $detalle->resumenPedido->pedido->nombre_contacto.' '.$detalle->resumenPedido->pedido->apellidos_contacto,
                        'telefono'              => ($detalle->resumenPedido->pedido->celular_contacto != null ? $detalle->resumenPedido->pedido->celular_contacto : ''),
                        // 'correo'                => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->correo : ''),
                        'correo'                => $detalle->resumenPedido->pedido->user->email,
                        'rfc'                   => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->rfc : ''),
                        'razon_social'          => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->razon_social : ''),
                        // 'monto'                 => number_format($detalle->resumenPedido->pedido->total, 2),
                        'monto'                 => number_format($detalle->precio_libro, 2),
                        'metodo_pago'           => $detalle->resumenPedido->pedido->metodo_pago,
                        'forma_pago'            => $detalle->resumenPedido->pedido->forma_pago,
                        'comision'              => ($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id ? number_format($detalle->resumenPedido->pedido->comision,2) : ''),
                        'costo_envio'           => ($detalle->resumenPedido->pedido->envio != null ? ($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id ? number_format($detalle->resumenPedido->pedido->envio->costo,2) : '') : ''),
                        'factura'               => '',
                        'tipo_entrega'          => $tipo_entrega,
                        'calle'                 => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->calle : '') ,
                        'no_exterior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_exterior : '') ,
                        'no_interior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_interior : ''),
                        'colonia'               => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->colonia : ''),
                        'cp'                    => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->cp : ''),
                        'estado'                => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->estado->descripcion : ''),
                        'municipio'             => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->municipio->descripcion : ''),
                        'referencia'            => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->referencia : ''),
                        'fecha_recepcion'       => ($detalle->enviado == 1 ? $detalle->envio->fecha_envio :
                                                                              ($detalle->resumenPedido->pedido->envio != null ?
                                                                              $detalle->resumenPedido->pedido->envio->fecha_envio : '' )),
                        'paqueteria'            => ($detalle->enviado == 1 ? $detalle->envio->paqueteria :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->paqueteria : '' )),
                        'guia'                  => ($detalle->enviado == 1 ? $detalle->envio->guia :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->guia : '' )) ,
                        'estatus_envio'         => ($detalle->enviado == 1 ? $detalle->envio->status :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->status : '' )) ,
                        'nombre'                => '',
                        'fecha'                 => ''
                        ];
                }

            }
            else
            {
                return[];

            }


        });
      }
      else
      {
        return $detalles->transform(function($detalle)  {

                if($detalle->resumenPedido->pedido->tipo_entrega  == 1){
                    $tipo_entrega = 'Colegio';
                }

                else if ($detalle->resumenPedido->pedido->tipo_entrega  == 2){
                    $tipo_entrega = 'Domicilio';
                }

                else if ($detalle->resumenPedido->pedido->tipo_entrega  == 3){
                    $tipo_entrega = 'Pick-Up';
                }

                    return [

                        'no_pedido'             => $detalle->resumenPedido->pedido->serie . $detalle->resumenPedido->pedido->folio,
                        'fecha_compra'          => date_format($detalle->resumenPedido->pedido->created_at,"d / m / Y"),
                        'nombre_colegio'        => $detalle->resumenPedido->pedido->colegio->nombre,
                        'seccion'               => $detalle->resumenPedido->paquete->nivelColegio->nombre,
                        'isbn'                  => $detalle->libro->isbn,
                        'titulo'                => $detalle->libro->nombre,
                        'piezas'                => $detalle->cantidad,
                        'nombre_alumno'         => $detalle->resumenPedido->nombre_alumno.' '.$detalle->resumenPedido->paterno_alumno.' '.$detalle->resumenPedido->materno_alumno,
                        'surtido'               => $detalle->resumenPedido->pedido->status,
                        'nombre_contacto'       => $detalle->resumenPedido->pedido->nombre_contacto.' '.$detalle->resumenPedido->pedido->apellidos_contacto,
                        'telefono'              => ($detalle->resumenPedido->pedido->celular_contacto != null ? $detalle->resumenPedido->pedido->celular_contacto : ''),
                        // 'correo'                => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->correo : ''),
                        'correo'                => $detalle->resumenPedido->pedido->user->email,
                        'rfc'                   => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->rfc : ''),
                        'razon_social'          => ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->razon_social : ''),
                        // 'monto'                 => number_format($detalle->resumenPedido->pedido->total,2),
                        'monto'                 => number_format($detalle->precio_libro, 2),
                        'metodo_pago'           => $detalle->resumenPedido->pedido->metodo_pago,
                        'forma_pago'            => $detalle->resumenPedido->pedido->forma_pago,
                        'comision'              => ($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id ? number_format($detalle->resumenPedido->pedido->comision,2) : ''),
                        'costo_envio'           =>($detalle->resumenPedido->pedido->envio != null ? ($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id ? number_format($detalle->resumenPedido->pedido->envio->costo,2) : '') : ''),
                        'factura'               => '',
                        'tipo_entrega'          => $tipo_entrega,
                        'calle'                 => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->calle : '') ,
                        'no_exterior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_exterior : '') ,
                        'no_interior'           => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_interior : ''),
                        'colonia'               => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->colonia : ''),
                        'cp'                    => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->cp : ''),
                        'estado'                => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->estado->descripcion : ''),
                        'municipio'             => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->municipio->descripcion : ''),
                        'referencia'            => ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->referencia : ''),
                        'fecha_recepcion'       => ($detalle->enviado == 1 ? $detalle->envio->fecha_envio :
                                                                              ($detalle->resumenPedido->pedido->envio != null ?
                                                                              $detalle->resumenPedido->pedido->envio->fecha_envio : '' )),
                        'paqueteria'            => ($detalle->enviado == 1 ? $detalle->envio->paqueteria :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->paqueteria : '' )),
                        'guia'                  => ($detalle->enviado == 1 ? $detalle->envio->guia :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->guia : '' )) ,
                        'estatus_envio'         => ($detalle->enviado == 1 ? $detalle->envio->status :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->status : '' )) ,
                        'nombre'                => '',
                        'fecha'                 => ''
                        ];


         });
       }
    }
}
