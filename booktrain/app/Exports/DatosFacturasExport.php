<?php

namespace App\Exports;

use League\Fractal\Manager;
use App\Models\DatosFacturas;
use App\Models\Colegio;
use App\Transformers\Serializer;
use League\Fractal\Resource\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Transformers\DatosFacturasXLSTransformer;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Carbon\Carbon;

use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class DatosFacturasExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{

    private $inicio;
    private $fin;
    private $colegio_id;


    public function __construct(  $inicio,   $fin,   $colegio_id)
    {
        $this->inicio = $inicio;
        $this->fin  = $fin;
        $this->colegio_id  = $colegio_id;

    }

    public function headings(): array
    {
        return [
            // '#',
            'Número de Pedido',
            'Fecha de Compra',
            'Subtotal',
            'Costo Envío',
            'Comisión Bancaria',
            'Total',
            'Forma de Pago',
            'Método de Pago',
            'NOMBRE DEL COLEGIO',
            'USO DE CFDI',
            'Razón Social',
            'RFC',
            'Correo',
            'Teléfono',

            'Calle',
            'Num. exterior',
            'Num. interior',
            'Colonia',
            'Municipio',
            'Codigo Postal',
            'Estado'

        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
            'D' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
            'F' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
            'E' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC,
            // 'AD' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:U1');
                // $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('K2:U2');


                // assign cell values
                $event->sheet->setCellValue('A1', 'REPORTE GENERAL DE FACTURACIÓN '.$this->inicio .' a '.$this->fin .','. ($this->colegio_id != null ? \App\Models\Colegio::find($this->colegio_id)->first()->nombre : 'Todos los colegios').'.');


                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);


                $event->sheet->getStyle('A2:U2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:U3')->applyFromArray($styles_subheaders);
            },
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {



        $colegio = Colegio::where('id',  $this->colegio_id)->first();

        $facturas = DatosFacturas::whereBetween('created_at',[$this->inicio.' 00:00:00',$this->fin.' 23:59:59'])
        ->whereHas('pedido.colegio', function($query) use ($colegio){
            if($colegio != null)
            $query->where('id', $colegio->id);

        })
        ->get();

        // $fractal = new Manager();

        // $datosFacturas = $fractal->setSerializer(new Serializer())
        //     ->createData(new Collection($facturas, new DatosFacturasXLSTransformer))->toArray();

        // return collect($datosFacturas);
        return $facturas->transform(function($dato)
	    {
            return [
                // 'id'      => (int) $datos->id,
                'no_pedido'    => $dato->pedido->serie . $dato->pedido->folio,
                'fecha_compra' => date_format($dato->created_at,"d / m / Y"),
                'subtotal'     => number_format($dato->pedido->subtotal,2),
                'costo_envio'  => ($dato->pedido->envio != null ? number_format($dato->pedido->envio->costo,2) : '') ,
                'comision'     => number_format($dato->pedido->comision,2),
                'total'        => number_format($dato->pedido->total,2),
                'forma_pago'   => $dato->metodo_pago,
                'metodo_pago'  => $dato->pedido->metodo_pago,
                'nombre_colegio' => $dato->pedido->colegio->nombre,
                'uso_cfdi' => $dato->cfdis->descripcion,
                'razon_social'   => $dato->razon_social,
                'rfc'   => $dato->rfc,
                'correo'   => $dato->correo,
                'telefono'   => $dato->pedido->celular_contacto,

                'calle'   => $dato->calle,
                'num_exterior'   => $dato->num_exterior,
                'num_interior'   => $dato->num_interior,
                'colonia'   => $dato->colonia,
                'municipio'   => $dato->municipio,
                'cp'   => $dato->cp,
                'estado'   => $dato->estado
            ];
        });
    }
}
