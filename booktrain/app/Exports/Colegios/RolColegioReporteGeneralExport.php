<?php

namespace App\Exports\Colegios;

use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RolColegioReporteGeneralExport implements
    FromCollection,
    WithStyles,
    ShouldAutoSize
{
    private $pedidos;

    public function __construct($pedidos, $fecha_inicial, $fecha_final, $nombre_colegio, $nombre_grado, $nombre_isbn, $estado_pedido)
    {
        $this->descripcion = "REPORTE DE {$fecha_inicial} A {$fecha_final}, {$nombre_colegio}, {$nombre_grado}, {$nombre_isbn}";
        $this->pedidos = $pedidos;
    }

    public function properties(): array
    {
        return [
            'creator'        => 'Booktrain',
            // 'lastModifiedBy' => 'Patrick Brouwers',
            'title'          => 'Reporte general',
            'description'    => $this->descripcion,
            // 'subject'        => 'Invoices',
            'keywords'       => 'reporte, general, booktrain',
            // 'category'       => 'Invoices',
            // 'manager'        => 'Patrick Brouwers',
            'company'        => 'Booktrain',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->mergeCells('A1:O1');

        $sheet->mergeCells('A2:H2');
        $sheet->mergeCells('I2:K2');
        $sheet->mergeCells('L2:N2');

        return [
            // Style the first row as bold text.
            1    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'fill' => [
                    'fillType' => Fill::FILL_NONE],
                    'font' => ['bold' => true]],
            2    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER],
                    'font' => ['bold' => true],
                    ],
            3    => [
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THICK,
                        'color' => ['argb' => '000000'],
                    ],
                ],
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER],
                    'font' => ['bold' => true],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                        ]
                    ],
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                $this->descripcion
            ],
            [
                // 'PEDIDO','','','','','','','',
                // 'CONTACTO','','','','',
                // 'FACTURACION','','','','','','','','',
                // 'DISTRIBUCIÓN','','','','','','','','','','',
                // 'COLEGIO / PICK UP',''
                'PEDIDO','','','','','','','',
                'CONTACTO','','',
                'FACTURACION','', '',
                'DISTRIBUCIÓN'
            ],
            [
                'No. Pedido',
                'Fecha',
                'Colegio',
                'Grado',
                'Sección',
                'ISBN',
                'Título',
                'Piezas',
                'Nombre alumno',
                'Estado pedido',
                'Nombre contacto',
                // 'Teléfono',
                // 'Correo',
                // 'RFC',
                // 'Razón social',
                // 'Correo',
                'Precio libro',
                'Método de pago',
                'Forma de pago',
                // 'Comisión',
                // 'Costo envío',
                // 'Factura*',
                'Tipo de entrega',
                // 'Dirección',
                // 'Colonia',
                // 'C.P',
                // 'Estado',
                // 'Municipio',
                // 'Referencia',
                // 'Fecha de envío',
                // 'Tipo de envío',
                // 'No. de guía',
                // 'Estado del envío',
                // 'Nombre*',
                // 'Fecha*',
            ],
            $this->pedidos->map(function($pedido) {
                return $pedido->resumenes->map(function($resumen) use ($pedido) {
                    return $resumen->detalles->map(function($detalle, $index_detalle) use ($resumen, $pedido) {
                        return [
                            "{$pedido->serie}{$pedido->folio}",
                            $pedido->created_at->format('d / m / Y'),
                            $pedido->colegio->nombre,
                            $resumen->paquete->nivel->nombre,
                            $resumen->paquete->nivel->seccion->nombre,
                            "{$detalle->libro->isbn} ",
                            $detalle->libro->nombre,
                            $detalle->cantidad,
                            $resumen->nombre_completo_alumno,
                            $pedido->status,
                            "{$pedido->nombre_contacto} {$pedido->apellidos_contacto}",
                            // $pedido->celular_contacto,
                            // $pedido->user->email,
                            // isset($pedido->datosFactura) ? $pedido->datosFactura->rfc : '',
                            // isset($pedido->datosFactura) ? $pedido->datosFactura->razon_social : '',
                            // isset($pedido->datosFactura) ? $pedido->datosFactura->correo : '',
                            $detalle->precio_libro,
                            $pedido->metodo_pago,
                            $pedido->forma_pago,
                            // $index_detalle == 0 ? $pedido->comision : '',
                            // $index_detalle == 0 ? (isset($pedido->envio) ? $pedido->envio->costo : '') : '',
                            // '',
                            $pedido->descripcion_tipo_entrega,
                            // isset($pedido->direccion) ? $pedido->direccion->calle . ' #' . $pedido->direccion->no_exterior . ($pedido->direccion->no_interior != '' ? ', Int. ' . $pedido->direccion->no_interior : '' ) : '',
                            // isset($pedido->direccion) ? $pedido->direccion->colonia : '',
                            // isset($pedido->direccion) ? $pedido->direccion->cp : '',
                            // isset($pedido->direccion) ? $pedido->direccion->estado->descripcion : '',
                            // isset($pedido->direccion) ? $pedido->direccion->municipio->descripcion : '',
                            // isset($pedido->direccion) ? $pedido->direccion->referencia : '',
                            // isset($pedido->envio) ? (isset($pedido->envio->fecha_envio) ? $pedido->envio->fecha_envio->format('d / m / Y') : '') : '',
                            // isset($pedido->envio) ? $pedido->envio->paqueteria  : '',
                            // isset($pedido->envio) ? $pedido->envio->guia  : '',
                            // isset($pedido->envio) ? $pedido->envio->status  : '',
                            // '',
                            // '',
                        ];
                    });
                })->collapse();
            })->collapse()
        ]);
    }
}
