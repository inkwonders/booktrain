<?php

namespace App\Exports\Colegios;

use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RolColegioReporteColegioExport implements
    FromCollection,
    WithStyles,
    ShouldAutoSize,
    WithStrictNullComparison
{

    private $libros;
    private $inicio;
    private $fin;
    private $colegio;
    private $grado;
    private $isbn;
    private $descripcion;

    public function __construct($libros, $inicio, $fin, $colegio, $grado, $isbn)
    {
        $this->descripcion = "REPORTE DE {$colegio->nombre}, DEL {$inicio} AL {$fin}, {$grado} y {$isbn}";

        $this->libros = $libros;
        $this->inicio = $inicio;
        $this->fin = $fin;
        $this->colegios = $colegio;
        $this->grado = $grado;
        $this->isbn = $isbn;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
                [
                    $this->descripcion
                ],
                [
                    'ISBN',
                    // 'EDITORIAL',
                    'TÍTULO',
                    'GRADO',
                    'SECCIÓN',
                    // 'PRECIO DE LISTA',
                    'OBLIGATORIO/OPCIONAL',
                    'PRECIO A ALUMNO',
                    // '% DESCUENTO COLEGIO / ALUMNO',
                    'PEDIDOS (CANTIDAD)',
                    // 'PUNTO DE REORDEN',
                    // 'ENTREGA (CANTIDAD)',
                    'VENTA DE MOSTRADOR',
                    // 'DEVOLUCIONES (CANTIDAD)',
                    // 'VENTA REAL',
                    // 'VALOR DEL PEDIDO',
                    // 'VALOR DE DEVOLUCIÓN',
                    // 'VALOR REAL DE VENTA',
                    // 'DESCUENTO OTORGADO'
                ],
                $this->libros->map(function($libro) {
                    return [
                        "{$libro->isbn} ",
                        // '',
                        $libro->titulo,
                        $libro->grado,
                        $libro->seccion,
                        // '',
                        $libro->obligatorio,
                        $libro->precio,
                        // '',
                        $libro->pedidos,
                        // $libro->reorden,
                        // $libro->entrega,
                        $libro->mostrador,
                        // $libro->devoluciones,
                        // $libro->venta_real,
                        // $libro->valor_pedido,
                        // $libro->valor_devolucion,
                        // $libro->valor_venta_real,
                        // ''
                    ];
                })
            ]
        );
    }
    public function properties(): array
    {
        return [
            'creator'        => 'Booktrain',
            // 'lastModifiedBy' => 'Patrick Brouwers',
            'title'          => 'Reporte colegio',
            'description'    => $this->descripcion,
            // 'subject'        => 'Invoices',
            'keywords'       => 'reporte, colegio, booktrain',
            // 'category'       => 'Invoices',
            // 'manager'        => 'Patrick Brouwers',
            'company'        => 'Booktrain',
        ];
    }

    public function styles(Worksheet $sheet) : array
    {
        // $sheet->mergeCells('A1:S1');
        $sheet->mergeCells('A1:O1');

        // $sheet->mergeCells('A2:H2');
        // $sheet->mergeCells('I2:M2');
        // $sheet->mergeCells('N2:V2');
        // $sheet->mergeCells('W2:AG2');
        // $sheet->mergeCells('AH2:AI2');

        return [
            // 'A1:S1' => [
            'A1:O1' => [
                'fill' => [
                    'fillType' => Fill::FILL_NONE,
                ],
                'font' => ['bold' => true]
            ],
            // 'A2:S2' => [
            'A2:O2' => [
                'fill' => [
                    'fillType' => Fill::FILL_NONE,
                ],
                'font' => ['bold' => true]
            ],
            // 'B'  => [
            //     'fill' => [
            //         'fillType' => Fill::FILL_SOLID,
            //         'color' => ['argb' => 'ffe080']
            //     ]
            // ],
            // 'F'  => [
            //     'fill' => [
            //         'fillType' => Fill::FILL_SOLID,
            //         'color' => ['argb' => 'ffe080']]],
            // 'I'  => [
            //     'fill' => [
            //         'fillType' => Fill::FILL_SOLID,
            //         'color' => ['argb' => 'ffe080']]],
            // 'S'  => [
            //     'fill' => [
            //         'fillType' => Fill::FILL_SOLID,
            //         'color' => ['argb' => 'ffe080']]],
        ];
    }
}
