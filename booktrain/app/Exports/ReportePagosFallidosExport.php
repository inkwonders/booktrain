<?php

namespace App\Exports;

use App\Models\RegistroPago;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;


class ReportePagosFallidosExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{
    private $pedidos;
    private $status;
    private $factura;
    private $metodo_pagos;
    private $forma_pagos;
    private $forma_entrega;
    private $inicio;
    private $fin;


    public function __construct($inicio, $fin)
    {
        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function headings(): array
    {
        return [
            [
                'Fecha',
                'Folio',
                'Total',
                'Estatus',
                'Nombre',
                'Correo',
                'Teléfono',
                'Colegio',
                'Fecha Pago',
                'Terminación',
                'Monto',
                'Motivo Rechazo',
                'Motivo rechazo 3DS',
                'Motivo rechazo confirmación pago',
                'Estado'
            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_columns_yellow = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],

                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'FFE080']
                    ],

                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];




                $event->sheet->getStyle('A1:O1')->applyFromArray($styles_subheaders);
            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $pagos_fallidos = RegistroPago::whereIn('status', ['FAILED', 'REVIEW', 'CHARGEABLE', 'WAIT_THREEDS'])
            ->whereBetween('created_at', [$this->inicio, $this->fin])
            ->orderBy('created_at', 'desc')
            ->with('pedido')
            ->get();


        return $pagos_fallidos->transform(function ($pago) {

            $pedido = $pago->pedido()->withTrashed()->first();

            $info = (object) $pago->raw;
            $scalar = json_decode($info->scalar);


            if (isset($scalar->error)){
                $error_pago = $scalar->error;
            }
            else{
                $error_pago = '';
            }

            if($pago->status_3ds != null || $pago->status_3ds != ''){
                $info_3ds = (object) $pago->status_3ds;
                $scalar_3ds = json_decode($info_3ds->scalar);
                if (isset($scalar_3ds->responseMsg)) {
                    $error_pago_3ds = $scalar_3ds->responseMsg;
                } else {
                    $error_pago_3ds = '';
                }
            }
            else{
                $error_pago_3ds = '';
            }

            if($pago->confirm != null || $pago->confirm != ''){
                $info_confirm = (object) $pago->confirm;
                $scalar_confirm = json_decode($info_confirm->scalar);
                if (isset($scalar_confirm->error)) {
                    $error_pago_confirm = $scalar_confirm->error;
                } else {
                    $error_pago_confirm = '';
                }
            }
            else{
                $error_pago_confirm = '';
            }


            try {
                $terminacion = $scalar->paymentSource->card->lastFourDigits;
            } catch (\Exception $e) {
                $terminacion = '';
            }

            return [
                'fecha'          => !is_null($pedido)? date_format($pedido->created_at, "d / m / Y") : '',
                'folio'          => !is_null($pedido)? $pedido->serie . $pedido->folio : '',
                'total'          => !is_null($pedido)? number_format($pedido->total, 2) : '',
                'estatus'        => !is_null($pedido)? $pedido->status : '',
                'nombre'         => !is_null($pedido)? $pedido->nombre_contacto . '' . $pedido->apellidos_contacto : '',
                'correo'         => !is_null($pedido)? $pedido->user->email : '',
                'teléfono'       => !is_null($pedido)? $pedido->celular_contacto : '',
                'colegio'        => !is_null($pedido)? $pedido->colegio->nombre : '',
                'fecha_pago'     => $pago->created_at,
                'terminacion'    => $terminacion,
                'monto'          => number_format($pago->amount,2),
                'motivo_rechazo' => $error_pago,
                'motivo_3ds'     => $error_pago_3ds,
                'motivo_confirm' => $error_pago_confirm,
                'status'         => $pago->status

            ];

        });
    }
}
