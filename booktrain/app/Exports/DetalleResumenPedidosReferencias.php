<?php

namespace App\Exports;

use App\Models\Pedido;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
class DetalleResumenPedidosReferencias extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{
    private $status;
    private $facturacion;
    private $metodo_pago;
    private $forma_pago;
    private $tipo_entrega;
    private $inicio;
    private $fin;

    public function __construct( $status, $facturacion, $metodo_pago,  $forma_pago, $tipo_entrega,  $inicio , $fin)
    {
        $this->status = $status;
        $this->facturacion = $facturacion;
        $this->metodo_pago = $metodo_pago;
        $this->forma_pago = $forma_pago;
        $this->tipo_entrega = $tipo_entrega;
        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function headings(): array
    {
        return [
            [
                'Folio',
                'Fecha',
                'Nombre',
                'Correo',
                'Teléfono',
                'Solicito Factura',
                'Factura',
                'Referencia',
                'Método de pago',
                'Forma de pago',
                'Monto Cobrado',
                'Monto recibido',
                'Colegio',
                'Fecha pago',
                'Entrega',
                'Estatus'
            ]
        ];
    }

    public function columnFormats(): array
    {
        return [
            'G' => \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING,
            'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:O1');
                // $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('A2:O2');
                $event->sheet->setCellValue('A1', 'REPORTE GENERAL DE PEDIDOS ' .$this->inicio .' a '.$this->fin .'.');

                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);
                $event->sheet->getStyle('A2:X2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:X3')->applyFromArray($styles_subheaders);

            },
        ];
    }

    public function collection()
    {
        return Pedido::query()
            ->where('status', '!=','CARRITO')
            ->when($this->inicio, function($query, $inicio) {
                $query->where('created_at', '>=', "{$inicio} 00:00:00");
            })
            ->when($this->fin, function($query, $fin) {
                $query->where('created_at', '<=', "{$fin} 23:59:59");
            })
            ->when($this->status, function($query, $status) {
                $query->where('status', $status);
            })
            ->when($this->metodo_pago, function($query, $metodo_pago) {
                $query->where('metodo_pago', $metodo_pago);
            })
            ->when($this->forma_pago, function($query, $forma_pago) {
                $query->where('forma_pago', $forma_pago);
            })
            ->when($this->tipo_entrega, function($query, $tipo_entrega) {
                $query->where('tipo_entrega', $tipo_entrega);
            })
            ->when($this->facturacion, function($query, $facturacion) {
                $query->where('factura', ($facturacion == 1 ? 1 : 0) );
            })
            ->orderBy('created_at', 'DESC')
            ->with('colegio', 'pagos', 'user', 'referencia', 'formaPago', 'metodoPago')
            ->get()
            ->transform(function($pedido)  {
                $pago = $pedido->pagos->where('status', 'SUCCESS')->first();

                return [
                    'folio'              => $pedido->serie . $pedido->folio,
                    'fecha'              => $pedido->created_at,
                    'nombre'             => "{$pedido->nombre_contacto} {$pedido->apellidos_contacto}",
                    'correo'             => ($pedido->email_pedido == null ? $pedido->user->email : $pedido->email_pedido),
                    'teléfono'           => $pedido->celular_contacto,
                    'factura'            => $pedido->factura == 1 ? 'SI' : 'NO',
                    'referencia_factura' => $pedido->referencia_factura,
                    'referencia'         => $pedido->referencia != null ? "{$pedido->referencia->referencia}" : '',
                    'metodo_pago'        => $pedido->metodo_pago,
                    'forma_pago'         => $pedido->forma_pago,
                    'monto_cobrado'      => $pedido->total,
                    'monto_recibido'     => $pago != null ? $pago->amount : '',
                    'colegio'            => $pedido->colegio->nombre,
                    'fecha_pago'         => $pago != null ? $pago->created_at : '',
                    'entrega'            => $pedido->descripcion_tipo_entrega,
                    'estatus'            => $pedido->status,
                ];
        });
    }
}
