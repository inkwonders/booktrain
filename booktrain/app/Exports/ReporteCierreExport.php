<?php

namespace App\Exports;

use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ReporteCierreExport implements
    FromCollection,
    WithStyles,
    ShouldAutoSize,
    WithStrictNullComparison
{
    public $libros;
    public $inicio;
    public $fin;
    public $colegio;
    public $grado;
    public $isbn;
    public $descripcion;

    public function __construct($libros, $inicio, $fin, $colegio, $grado, $isbn)
    {
        $this->descripcion = "REPORTE DE {$colegio->nombre}, DEL {$inicio} AL {$fin}, {$grado} y {$isbn}";

        $this->libros = $libros;
        $this->inicio = $inicio;
        $this->fin = $fin;
        $this->colegio = $colegio;
        $this->grado = $grado;
        $this->isbn = $isbn;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect([
            [
                $this->descripcion
            ],
            [
                'ISBN',
                'EDITORIAL',
                'TÍTULO',
                'OBLIGATORIO/OPCIONAL',
                'GRADO',
                'SECCIÓN',
                'PRECIO DE LISTA',
                'PRECIO A ALUMNO',
                '% DESCUENTO COLEGIO / ALUMNO',
                'DESCUENTO EDITORIAL',
                'COSTO',
                'DESCUENTO UNITARIO',
                'PEDIDOS (CANTIDAD)',
                'PUNTO DE REORDEN',
                'ENTREGA (CANTIDAD)',
                'VENTA DE MOSTRADOR',
                'DEVOLUCIONES (CANTIDAD)',
                'VENTA REAL',
                'VALOR DEL PEDIDO',
                'VALOR DE DEVOLUCIÓN',
                'VALOR REAL DE VENTA',
                'COSTO (COMPRA)',
                'DESCUENTO OTORGADO (Bonificación)',
                'GM',
                '% Utilidad'
            ],
            $this->libros->map(function($libro) {
                return [
                    $libro->isbn,
                    '', // Editorial > B
                    $libro->titulo,
                    $libro->obligatorio ? 'Obligatorio' : 'Opcional',
                    $libro->grado,
                    $libro->seccion,
                    '', // Precio de lista > G
                    $libro->precio,

                    '', // % descuento > I
                    '', // Descuento editorial > J
                    '', // Costo > K
                    '', // Descuento unitario L

                    $libro->pedidos,
                    $libro->reorden,

                    $libro->entrega,
                    $libro->mostrador,
                    $libro->devoluciones,

                    $libro->venta_real,
                    $libro->valor_pedido,
                    $libro->valor_devolucion,
                    $libro->valor_venta_real,
                    '', // Costo de compra > V
                    '', // Descuento otorgado > W
                    '', // GM > X
                    '', // % Utilidad > Y
                ];
            })
        ]);
    }

    public function styles(Worksheet $sheet) : array
    {
        // $sheet->mergeCells('A1:S1');
        $sheet->mergeCells('A1:Y1');

        // $sheet->mergeCells('A2:H2');
        // $sheet->mergeCells('I2:M2');
        // $sheet->mergeCells('N2:V2');
        // $sheet->mergeCells('W2:AG2');
        // $sheet->mergeCells('AH2:AI2');

        return [
            // 'A1:S1' => [
            'A1:Y1' => [
                'fill' => [
                    'fillType' => Fill::FILL_NONE,
                ],
                'font' => ['bold' => true]
            ],
            // 'A2:S2' => [
            'A2:Y2' => [
                'fill' => [
                    'fillType' => Fill::FILL_NONE,
                ],
                'font' => ['bold' => true]
            ],
            'B'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            'G'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            'I:L'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
            'V:Y'  => [
                'fill' => [
                    'fillType' => Fill::FILL_SOLID,
                    'color' => ['argb' => 'ffe080']]],
        ];
    }
}
