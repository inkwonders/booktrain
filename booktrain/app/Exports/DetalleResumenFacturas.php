<?php

namespace App\Exports;

use App\Models\DatosFacturas;
use App\Models\Pedido;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class DetalleResumenFacturas extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{

    private $inicio;
    private $fin;

    public function __construct($inicio , $fin)
    {

        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function headings(): array
    {
        return [
            [
                'Pedido',
                'Razón Social',
                'RFC',
                'Correo',
                'Dirección',
                'CP',
                'CFDI',
                'Método de pago',
                'Forma de pago',
                'Fecha',
            ]
        ];
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:O1');
                // $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('A2:O2');
                $event->sheet->setCellValue('A1', 'REPORTE GENERAL DE FACTURAS ' .$this->inicio .' a '.$this->fin .'.');

                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);
                $event->sheet->getStyle('A2:X2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:X3')->applyFromArray($styles_subheaders);

            },
        ];
    }

    public function collection()
    {
        return DatosFacturas::query()
            ->when($this->inicio, function($query, $inicio) {
                $query->where('created_at', '>=', "{$inicio} 00:00:00");
            })
            ->when($this->fin, function($query, $fin) {
                $query->where('created_at', '<=', "{$fin} 23:59:59");
            })
            ->orderBy('created_at', 'DESC')
            ->get()
            ->transform(function($pedido)  {
                return [

                    'Pedido'             => $pedido->pedido_id,
                    'Razón Social'       => $pedido->razon_social,
                    'RFC'                => $pedido->rfc,
                    'Correo'             => $pedido->correo,
                    'Dirección'          => $pedido->calle.' '.$pedido->num_exterior.' '.$pedido->num_interior.' '.$pedido->colonia.' '.$pedido->municipio.' '.$pedido->estado,
                    'CP'                 => $pedido->cp,
                    'CFDI'               => $pedido->cfdis->descripcion,
                    'Método de pago'     => $pedido->pedido->metodo_pago,
                    'Forma de pago'      => $pedido->pedido->formaPago->descripcion,
                    'Fecha'              => $pedido->created_at,
                ];

        });
    }
}
