<?php

namespace App\Exports;

use App\Models\Colegio;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class DetalleResumenPedidoColegioExport  extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithCustomValueBinder
{
    private $inicio;
    private $fin;
    private $colegio_id;
    private $isbn_req;
    private $grado_id;

    public function __construct(  $inicio,   $fin,   $colegio_id,   $isbn_req,   $grado_id)
    {
        $this->inicio = $inicio;
        $this->fin  = $fin;
        $this->colegio_id  = $colegio_id;
        $this->isbn_req  = $isbn_req;
        $this->grado_id  = $grado_id;
    }

    public function headings(): array
    {
        return [
            [
                '#',
                'ISBN',
                //'Editorial',
                'Título',
                'Obligatorio / Opcional',
                'Grado',
                //'Pecio de Lista',
                'Precio Alumno',
                //'% Descuento Colegio / Alumno',
                'Pedidos Cantidad',
                'Punto Reorden',
                'Entrenga Cantidad',
                'Venta Mostrador',
                'Devoluciones Cantidad',
                'Venta Real',
                'Valor Pedido',
                'Valor de Devolución',
                'Valor real venta',
                //'Descuento otorgado',

            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_columns_yellow = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],

                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'FFE080']
                    ],

                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:O1');
                // $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('K2:O2');
                // $event->sheet->mergeCells('N2:U2');
                // $event->sheet->mergeCells('V2:AC2');
                // $event->sheet->mergeCells('AD2:AE2');

                // assign cell values
                if($this->grado_id == 0 || $this->grado_id == '0'){
                  $grado = 'TODOS';
                }
                else{
                  $grado = \App\Models\Paquete::where('id', $this->grado_id)->first()->nombre;
                }

                // if($this->isbn_req == 0 || $this->isbn_req == '0'){
                //    $isbn ='TODOS';
                // }
                // else{
                //     $isbn = \App\Models\Libro::where('isbn', $this->isbn_req)->first()->isbn;
                // }

                $event->sheet->setCellValue('A1', 'REPORTE GENERAL '.$this->inicio .' a '.$this->fin .','. \App\Models\Colegio::find($this->colegio_id)->first()->nombre.', Grado: '.$grado.' , ISBN: '.$this->isbn_req.' .');
                // $event->sheet->setCellValue('A2', 'PEDIDO');
                // $event->sheet->setCellValue('K2', 'CONTACTO');
                // $event->sheet->setCellValue('N2', 'FACTURACIÓN');
                // $event->sheet->setCellValue('V2', 'DISTRIBUCIÓN');
                // $event->sheet->setCellValue('AD2', 'COLEGIO / PICK UP');

                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);

                // $event->sheet->getStyle('C4:C500')->applyFromArray($styles_columns_yellow);
                // $event->sheet->getStyle('G4:G500')->applyFromArray($styles_columns_yellow);
                // $event->sheet->getStyle('I4:I500')->applyFromArray($styles_columns_yellow);
                // $event->sheet->getStyle('S4:S500')->applyFromArray($styles_columns_yellow);

                $event->sheet->getStyle('A2:O2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:O3')->applyFromArray($styles_subheaders);
            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $colegio = Colegio::find($this->colegio_id);

        $pedidos = $colegio->pedidos()
        ->whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])
        // ->where('created_at', '<', $this->fin)
        // ->where('created_at', '>', $this->inicio)
        ->with(['resumenes' => function($query) {

            $query->whereHas('paquete', function($query){
                if($this->grado_id != 0 || $this->grado_id != '0')
                    $query->where('nivel_id', $this->grado_id);
            });

            $query->with(['detalles' => function ($query) {
                $query->whereHas('libro', function($query) {
                    if($this->isbn_req != 0 || $this->isbn_req != '0')
                        $query->where('isbn', $this->isbn_req);
                });
            }]);
        }]);
    $pedidos = $pedidos->get();


    $libros = $pedidos->pluck('libros')->collapse();

    $libros = $colegio->libros()->wherePivot('paquete_id', null)->with([
        'paquetes.nivel.seccion' => function($query) use ($colegio) {
            $query->where('colegio_id', $colegio->id);
        },
        'paquetes.resumenes.pedido' => function($query) use ($colegio) {
            $query->where('colegio_id', $colegio->id);
            $query->whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59']);
            // $query->where('created_at', '<', $this->fin);
            // $query->where('created_at', '>', $inicio);
        }
    ]);

    // ISBN especifico
    if($this->isbn_req != 0 || $this->isbn_req != '0')
        $libros = $libros->where('isbn', $this->isbn_req);

    if($this->grado_id != 0 || $this->grado_id != '0')
        $libros = $libros->whereHas('paquetes', function($query)  {
            $query->where('nivel_id', $this->grado_id);
        });


        $libros = $libros->get();

         return $libros->transform(function($libro)  {
            $pedidos = $libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull();
            $venta =   $libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->whereNotIn('status', ['PROCESANDO', 'CANCELADO']);
            // $valor_pedido = $venta->sum('cantidad') * $libro->paquete->pivot->precio;
            $valor_devolucion = 0;
            return [
                'id'                    => $libro->id,
                'isbn'                  => $libro->isbn,
                //'editorial'             => $libro->editorial,
                'titulo'                => $libro->nombre,
                'obligatorio'           => $libro->paquetes->isEmpty() == null ? ($libro->paquetes()->first()->pivot->obligatorio  != null ? 'OBLIGATORIO' : 'NO OBLIGATORIO') : '',
                'grado'                 => $libro->paquetes->isEmpty() == null ? ($libro->paquetes->first()->nivel->nombre) : '',
                //'precio_lista'          => 0,
                'precio_alumno'         => $libro->paquetes->isEmpty() == null ? (number_format($libro->paquetes()->first()->pivot->precio,2)) : '',
                //'descuento_colegio_alumno'   => '',//en blanco
                'cantidad_pedidos'      => $libro->paquetes->isEmpty() == null ? ($libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->count()) : '',
                'punto_reorden'         => $libro->paquetes->isEmpty() == null ? ($libro->paquetes()->first()->pivot->punto_reorden) : '',
                'cantidad_entrega'      => $libro->paquetes->pluck('resumenes')->collapse()
                                            ->pluck('pedido')->whereNotNull()->where('usuario_venta_id','=',null)
                                            ->pluck('resumenes')->collapse()->pluck('detallesResumenPedidos')->collapse()
                                            ->where('entregado',1)->where('libro_id',$libro->id)->count(),
                'venta_mostrador'       => $libro->paquetes->isEmpty() == null ? ($libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('usuario_venta_id','<>',null)->count()) :'',
                'cantidad_devoluciones' => $libro->paquetes->isEmpty() == null ? ($libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('status', 'CANCELADO')->count()) : '',
                // Suma de libros de los resumenes que tengan pedidos pagados o entregados
                'venta_real'            => $libro->paquetes->isEmpty() == null ? ($libro->detalleResumen->where('entregado',1)->count()) : '',
                // 'venta_real' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count(),

                // El precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por la suma de libros en los resumenes cuyo pedido sea entregado o pagado
                'valor_pedido'          => $libro->paquetes->isEmpty() == null ? (number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2)) : '',
                // 'valor_pedido' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio,

                // El precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por el numero de libros que tienen una entrada a su almacén por el concepto de devolución
                'valor_devolucion'      => $valor_devolucion, // Se necesita registrar las entradas y salidas al almacén
                // 'valor_devolucion' => $pedidos->whereIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio,

                // Precio del libro en el paquete correspondiente al nivel y a la sección dada, multiplicado por la cantidad de libros en la venta real, menos la suma de devoluciones
                'valor_real_venta'       => $libro->paquetes->isEmpty() == null ? (number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2)) : '',
                // 'valor_real_venta' => $pedidos->whereNotIn('status', ['PROCESANDO', 'CANCELADO'])->count() * $libro->paquete->pivot->precio
                //'descuento_otorgado'      => '',//en blanco
            ];
        });

    }
}
