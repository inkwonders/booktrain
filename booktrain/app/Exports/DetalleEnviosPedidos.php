<?php

namespace App\Exports;

use App\Models\Pedido;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Carbon\Carbon;

class DetalleEnviosPedidos implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{

    private $inicio;
    private $fin;


    public function __construct( $inicio , $fin)
    {

        $this->inicio = $inicio;
        $this->fin = $fin;

    }

    public function headings(): array
    {
        return [
            [
                'Folio',
                'Fecha',
                'Estatus pedido',
                'Nombre',
                'Correo',
                'Teléfono',
                'Colegio',
                'Fecha pago',
                'Entrega',
                'Estatus envío',
                'Calle',
                'No. Exterior',
                'No. Interior',
                'Colonia',
                'C.P.',
                'Estado',
                'Municipio',
                'Referencias',
                'Fecha de recepción',
                'Fecha estimada',
                'Horarios',
                'Costo',
                'Guía',
                'Paqueteria',


            ]
        ];
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {

                // set up a style array for cell formatting
                $styles_headers = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $bordes = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_subheaders = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ],
                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'C8C8C8']
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THICK,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                ];

                $styles_columns_yellow = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER
                    ],

                    'fill' => [
                        'fillType'  => Fill::FILL_SOLID,
                        'color' => ['argb' => 'FFE080']
                    ],

                ];

                $styles_negritas = [
                    'font' => [
                        'name'      =>  'Helvetic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];



                // at row 1, insert 2 rows
                $event->sheet->insertNewRowBefore(1, 2);

                // merge cells for full-width
                $event->sheet->mergeCells('A1:X1');
                // $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('A2:X2');


                $event->sheet->setCellValue('A1', 'REPORTE GENERAL DE ENVÍOS ' .$this->inicio .' a '.$this->fin .'.');


                // assign cell styles
                $event->sheet->getStyle('A1')->applyFromArray($styles_negritas);



                $event->sheet->getStyle('A2:X2')->applyFromArray($bordes);
                $event->sheet->getStyle('A3:X3')->applyFromArray($styles_subheaders);

            },
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $pedidos = Pedido::whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])
        ->where('tipo_entrega',2)
        ->whereIn('status',['ENVIADO','ENTREGADO'])
        ->get()->sortByDesc('folio');

         return $pedidos->transform(function($pedido)  {
            if ($pedido->tipo_entrega == 1)
            {
                $tipo_de_entrega = 'Colegio';
            }
            else if ($pedido->tipo_entrega == 2)
            {
                $tipo_de_entrega = 'Domicilio';
            }
            else
            {
                $tipo_de_entrega = 'PickUp';
            }

            return [
                'folio'          => $pedido->serie.$pedido->folio,
                'fecha'          => $pedido->created_at != null ? (date_format($pedido->created_at,"d / m / Y")) : '',
                'estatus'        => $pedido->status,
                'nombre'         => $pedido->nombre_contacto.''.$pedido->apellidos_contacto ,
                'correo'         => $pedido->user->email,
                'teléfono'       => $pedido->celular_contacto,
                'colegio'        => $pedido->colegio->nombre,
                'fecha_pago'     => $pedido->pagos->where('status','SUCCESS')->pluck('created_at')->first(),
                'entrega'        => $tipo_de_entrega ,//$pedido->tipo_entrega == 1,
                'estatus_envio'     => $pedido->envio->status,
                'calle' =>   $pedido->direccion->calle,
                'no_exterior' => $pedido->direccion->no_exterior,
                'no_interior' => $pedido->direccion->no_interior,
                'colonia' =>    $pedido->direccion->colonia,
                'cp' => $pedido->direccion->cp,
                'estado' =>  $pedido->direccion->estado->descripcion,
                'municipio' =>   $pedido->direccion->municipio->descripcion,
                'referencia' =>  $pedido->direccion->referencia,
                'fecha_recepcion'     =>  ($pedido->envio->fecha_recepcion != null ? date_format($pedido->envio->updated_at,"d / m / Y")  : ''),
                'fecha_estimada'  => ($pedido->envio->fecha_estimada != null ? $pedido->envio->fecha_estimada : ''),
                'horarios' => ($pedido->envio->horarios_entrega != null ? $pedido->envio->horarios_entrega : ''),
                'costo' =>  ($pedido->envio->costo != null ? $pedido->envio->costo : ''),
                'guia'  => ($pedido->envio->guia != null ? $pedido->envio->guia : ''),
                'paqueteria' => ($pedido->envio->paqueteria != null ? $pedido->envio->paqueteria : ''),

            ];
        });

    }
}
