<?php

namespace App\Console\Commands;

use App\Models\Pedido;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RevisaEstadoPagos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pagos:revisar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa las referencias de pedidos tipo TIENDA que no estén pagados para compensar las llamadas de WEBHOOK fallidas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Revisa las referencias de pedidos tipo TIENDA que no estén pagados
     * Obtenemos los pedidos de máximo 10 días de antiguedad con referencia tipo TIENDA que no estén pagados
     * Comprueba el estado del registro de pago
     * Si el nuevo estado es PAGADO,
     *
     * @return int
     */
    public function handle()
    {
        $dias_margen_comprobacion = 5;

        $pedidos = Pedido::whereHas('referencia', function($query) use ($dias_margen_comprobacion) {
            $query->where('tipo', 'TIENDA');
            $query->where('pagado', 0);
            $query->whereRaw("TO_DAYS( created_at ) >= ( TO_DAYS(NOW() ) - {$dias_margen_comprobacion})");
            $query->withCount('referenciasDuplicadas');
            //$query->having('referencias_duplicadas_count', '=', 1); // Validamos que no tengan referencias duplicadas
        })
        ->with(['referencia' => function($query) {
            $query->withCount('referenciasDuplicadas');
        }])
        ->where('status', 'PROCESADO')
        ->get();

        $this->info("Revisando estado de pago de {$pedidos->count()} pedidos");
        Log::info("Revisando estado de pago de {$pedidos->count()} pedidos");

        $resultados = [
            'correctos' => 0,
            'fallidos' => 0,
            'total' => $pedidos->count()
        ];

        try {
            $pedidos->each(function($pedido, $index) use (&$resultados) {
                if($pedido->referencia->referencias_duplicadas_count > 1) {
                    $this->error("Referencias duplicadas: el pedido {$pedido->id} ({$pedido->serie}{$pedido->folio}) tiene una referencia duplicada: {$pedido->referencia->referencia} x {$pedido->referencia->referencias_duplicadas_count}");
                    Log::error("Referencias duplicadas: el pedido {$pedido->id} ({$pedido->serie}{$pedido->folio}) tiene una referencia duplicada: {$pedido->referencia->referencia} x {$pedido->referencia->referencias_duplicadas_count}");
                    $resultados['fallidos'] ++;
                }else{
                    $resultado = $pedido->referencia->validarNetpay();

                    switch ($resultado) {
                        case 0: $resultado_str = 'Pago exitoso'; $resultados['correctos'] ++;break;
                        case 1: $resultado_str = 'Pago en espera'; $resultados['fallidos'] ++;break;
                        case 2: $resultado_str = 'Pago expirado'; $resultados['fallidos'] ++;break;
                        case 3: $resultado_str = 'Error, estatus desconocido'; $resultados['fallidos'] ++;break;
                        case 4: $resultado_str = 'Error al consultar el status'; $resultados['fallidos'] ++;break;
                        case 5: $resultado_str = 'La comprobación no aplica a este pago'; $resultados['fallidos'] ++;break;
                    }

                    if($resultado == 0) {
                        $this->info("{$index} - Pedido {$pedido->serie}{$pedido->folio} ({$pedido->id}), resultado: {$resultado_str}");
                        Log::info("{$index} - Pedido {$pedido->serie}{$pedido->folio} ({$pedido->id}), resultado: {$resultado_str}");
                    }else{
                        $this->error("{$index} - Pedido {$pedido->serie}{$pedido->folio} ({$pedido->id}), resultado: {$resultado_str}");
                        Log::error("{$index} - Pedido {$pedido->serie}{$pedido->folio} ({$pedido->id}), resultado: {$resultado_str}");
                    }

                    activity()
                        ->performedOn($pedido)
                        //->causedBy(1)
                        ->log("Realizando revisión de estado del pago en NetPay, {$index} - Pedido {$pedido->serie}{$pedido->folio}, resultado: {$resultado_str}");
                }
            });

        } catch (\Throwable $th) {
            Log::error('RevisaEstadoPagos', [$th]);
        }

        $this->info("Se comprobaron {$resultados['total']} pedidos, {$resultados['correctos']} exitosos y {$resultados['fallidos']} fallidos");
        Log::info("Se comprobaron {$resultados['total']} pedidos, {$resultados['correctos']} exitosos y {$resultados['fallidos']} fallidos");
    }
}
