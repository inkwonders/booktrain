<?php

namespace App\Console\Commands;

use App\Models\Pedido;
use Illuminate\Console\Command;

class ComprobarErroresPedidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pedidos:comprobar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comprueba los pedidos usando reglas de validación definidas para encontrar inconsistencias';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pedidos = Pedido::with('referencia', 'referencias', 'pagos', 'resumenes', 'resumenes.detalles')
        ->where('created_at', '>', '2021-07-01')
        ->get();

        $this->info("Comprobando errores en {$pedidos->count()} pedidos");

        $pedidos_errores = collect();
        $contador = 0;

        $excepciones = [1,2,3,4,5,6,7,8,9,10,11,15];

        $this->info('Excepciones de validación: ' . json_encode($excepciones) );

        $pedidos->each(function($pedido) use (&$pedidos_errores, &$contador, $excepciones) {
            $errores = $pedido->tieneErrores($excepciones);

            $no_error = 0;

            if(sizeof($errores) > 0)
                $pedidos_errores[$pedido->id] = (Object) ['pedido' => $pedido, 'errores' => $errores];

            foreach ($errores as $error) {
                $contador ++;
                $no_error ++;
                $this->error("[{$pedido->id}] {$pedido->created_at} {$pedido->serie}-{$pedido->folio} {$pedido->status} ({$no_error}) {$error->codigo}: {$error->descripcion}");
            }
        });

        $this->info("Se encontraron {$contador} errores en {$pedidos_errores->count()} pedidos");

        // Corregimos los errores en los pedidos

        $correcciones_exitosas = 0;
        $correcciones_fallidas = 0;

        $pedidos_errores->each(function($sujeto) use (&$correcciones_exitosas, &$correcciones_fallidas) {
            // Todas las acciones
            $acciones = $sujeto->errores->pluck('codigo')->map(function($codigo_error) {
                return substr($codigo_error, 3);
            })->toArray();

            // $acciones = [0];

            $resultados = $sujeto->pedido
                ->corregirErrores($acciones, $sujeto);

            foreach ($resultados as $resultado) {
                if($resultado->success == true){
                    $correcciones_exitosas ++;
                    $this->info("[{$sujeto->pedido->id}] {$sujeto->pedido->serie}-{$sujeto->pedido->folio} Corrección {$resultado->codigo} exitosa: {$resultado->descripcion}");
                }else{
                    $correcciones_fallidas ++;
                    $this->error("[{$sujeto->pedido->id}] {$sujeto->pedido->serie}-{$sujeto->pedido->folio} Corrección {$resultado->codigo} fallida: {$resultado->descripcion}");
                }
            }
        });

        $this->info("Se realizaron {$correcciones_exitosas} correcciones exitosas y {$correcciones_fallidas} correcciones fallidas, en {$pedidos_errores->count()} pedidos");
    }
}
