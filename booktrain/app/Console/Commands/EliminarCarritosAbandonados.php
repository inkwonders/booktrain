<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Pedido;

class EliminarCarritosAbandonados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eliminar_pedidos:carrito_abandonado';

    /**
     * The console Elimina los pedidos en estado CARRITO que tengan más de 3 días.
     *
     * @var string
     */
    protected $description = 'Elimina los pedidos en estado CARRITO que tengan más de 3 días';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // codigo para eliminar pedidos

        DB::beginTransaction();
        try {
            $this->info(" ---- Entrando a eliminar_pedidos:carrito_abandonado ---- ");
            Log::debug(" ---- Entrando a eliminar_pedidos:carrito_abandonado ---- ");

            $pedidos = Pedido::whereIn('status', ['CARRITO'])
                ->whereRaw("(TO_DAYS(CURRENT_DATE) - TO_DAYS(created_at)) >= 3")
                ->get();

            $this->info("Se encontraron " . count($pedidos) . " carritos abandonados");
            Log::debug("Se encontraron " . count($pedidos) . " carritos abandonados");

            // Recuperamos el stock de los libros del pedido
            $pedidos->each(function($pedido) {
                $pedido->eliminar();
            });

        } catch (\Throwable $th) {
            DB::rollback();
            // Reportar el error
            $this->error('Ocurrio un error al eliminar los registros:' . $th->getMessage());
            Log::debug('Ocurrio un error al eliminar los registros:' . $th->getMessage());
        }

        DB::commit();

        $this->info('Se eliminaron correctamente los carritos abandonados.');
        Log::debug(" ---- Se eliminaron correctamente los carritos abandonados. ----");
    }
}
