<?php

namespace App\Console\Commands;

use App\Models\Pedido;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BatchEnvioNotificacionPago extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'BatchEnvioNotificacionPago';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description hola';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info(" ---- Entrando a BatchEnvioNotificacionPago ---- ");
        Log::debug(" ---- Entrando a BatchEnvioNotificacionPago ---- ");

        $pedidos = Pedido::whereHas('referencia', function($query) {
            $query->where('notificado', 0);
            $query->where('pagado', 1);
        })
        ->take(100)
        ->get()
        ->each(function($pedido) {
            $this->info("Notificando a {$pedido->user->email} por el pedido {$pedido->id} y la referencia {$pedido->referencia->referencia} tipo {$pedido->referencia->tipo} del dia {$pedido->referencia->created_at}");
            Log::debug("Notificando a {$pedido->user->email} por el pedido {$pedido->id} y la referencia {$pedido->referencia->referencia} tipo {$pedido->referencia->tipo} del dia {$pedido->referencia->created_at}");

            $pedido->sendPagoExitosoNotification();
        });

        $this->info(" ---- Se enviaron {$pedidos->count()} notificaciones ---- ");
        Log::debug(" ---- Se enviaron {$pedidos->count()} notificaciones ---- ");
    }
}
