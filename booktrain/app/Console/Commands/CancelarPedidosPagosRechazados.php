<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\Pedido;

class CancelarPedidosPagosRechazados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelar_pedidos:pago_abandonado';

    /**
     * The console Se ejecuta cada minuto cancela los pedidos que tengan un único pago en estado de revisión con más de 10 minutos de antiguedad.
     *
     * @var string
     */
    protected $description = 'Se ejecuta cada minuto cancela los pedidos que tengan un único pago en estado de revisión con más de 10 minutos de antiguedad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // codigo para cancelar pedidos con pagos rechazados
        DB::beginTransaction();
        try {
            $this->info(" ---- Entrando a cancelar_pedidos:pago_abandonado ---- ");
            Log::debug(" ---- Entrando a cancelar_pedidos:pago_abandonado ---- ");

            $pedidos = Pedido::whereIn('status', ['PAGO EN REVISION'])
                ->whereHas('pagos', function($query) {
                    $query->where('status', 'REVIEW');
                    $query->whereRaw("(UNIX_TIMESTAMP(CURRENT_TIMESTAMP) - UNIX_TIMESTAMP(created_at)) >= " . (10 * 60)); // Debe tener más de 10 min de antiguedad, según NETPAY
                })// El registro de pago debe estar en revisión
                ->has('pagos', '=', 1) // Un solo registro de pago en total
                ->get();

            $this->info("Se encontraron " . count($pedidos) . " pedidos");
            Log::debug("Se encontraron " . count($pedidos) . " pedidos");

            // Recuperamos el stock de los libros del pedido
            $pedidos->each(function($pedido) {
                $this->info("Cancelando pedido (" . $pedido->id. ") ". $pedido->serie . "-" .$pedido->folio);
                Log::debug("Cancelando pedido (" . $pedido->id. ") ". $pedido->serie . "-" .$pedido->folio);
                $pedido->cancelar();

                $pedido->update(['status' => 'PAGO RECHAZADO']);
                //$pedido->referencia()->delete();
                //$pedido->pagos()->delete();
            });

        } catch (\Throwable $th) {
            DB::rollback();
            // Reportar el error
            $this->error('Ocurrio un error al actualizar los registros:' . $th->getMessage());
            Log::debug('Ocurrio un error al actualizar los registros:' . $th->getMessage());
        }

        DB::commit();

        $pedidos_fallidos = Pedido::whereIn('status', ['PAGO EN REVISION'])
            ->whereRaw("(UNIX_TIMESTAMP(CURRENT_TIMESTAMP) - UNIX_TIMESTAMP(created_at)) >= " . (10 * 60)) // Debe tener más de 10 min de antiguedad, según NETPAY
            ->has('pagos', '>', 1) // Pedidos en estado de revisión que tengan más de 1 pago y más de 10 minutos se deben notificar al sistema
            ->get();

        if($pedidos_fallidos->isNotEmpty()) {
            $this->error("Se encontraron {$pedidos_fallidos->count()} pedidos que no se pueden procesar por que tienen más de 1 pago y su estado es PAGO EN REVISION");
            Log::error("Se encontraron {$pedidos_fallidos->count()} pedidos que no se pueden procesar por que tienen más de 1 pago y su estado es PAGO EN REVISION");

            $pedidos_fallidos->each(function($pedido) {
                $this->error("ID: {$pedido->id} Serie: {$pedido->serie}-{$pedido->folio} Cantidad:{$pedido->total} Fecha:{$pedido->created_at} Pagos:{$pedido->pagos->count()} ({$pedido->pagos->pluck('status')->join(', ', ' y ')})");
                Log::error("ID: {$pedido->id} Serie: {$pedido->serie}-{$pedido->folio} Cantidad:{$pedido->total} Fecha:{$pedido->created_at} Pagos:{$pedido->pagos->count()} ({$pedido->pagos->pluck('status')->join(', ', ' y ')})");
            });
        }

        $this->info('Se ejecuto correctamente la cancelacion de pedidos con pagos rechazados.');
        Log::debug(" ---- Se ejecuto correctamente la cancelacion de pedidos con pagos rechazados. ----");
    }
}
