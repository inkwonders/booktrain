<?php

namespace App\Console\Commands;

use App\Models\Pedido;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

use MercadoPago;

class RevisaEstadoPagosMp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pagosmp:revisar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa las referencias de pedidos tipo TIENDA de MP que no estén pagados para compensar las llamadas de WEBHOOK fallidas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Revisa las referencias de pedidos tipo TIENDA que no estén pagados
     * Obtenemos los pedidos de máximo 10 días de antiguedad con referencia tipo TIENDA que no estén pagados
     * Comprueba el estado del registro de pago
     * Si el nuevo estado es PAGADO,
     *
     * @return int
     */
    public function handle()
    {

        $pedidos=Pedido::where('metodo_pago','MERCADO PAGO')
                         ->where('status','PROCESADO')
                         ->WhereHas('pagos', function ($query)  {

                             return $query->where('status','IN_PROCESS');

                             })

                         ->withCount(['pagos' => function ($query) {
                            return $query->where('status','IN_PROCESS');
                         }])

                             ->get();

        $this->info("Revisando pedidos que posiblemente no fueron actualizados por mercado pago, total de ".$pedidos->count()." pagos");
        Log::info("Revisando pedidos que posiblemente no fueron actualizados por mercado pago, total de ".$pedidos->count()." pagos");

        $resultados = [
            'correctos' => 0,
            'fallidos' => 0,
            'total' => $pedidos->count()
        ];

        try {


            DB::beginTransaction();

                foreach ($pedidos as $pedido) {


                $payment_id=null;

                $referencia=$pedido->serie."-".$pedido->folio;
                $payment_id=$pedido->pagos[0]->transactionTokenId;


                $reponse= Http::get("https://api.mercadopago.com/v1/payments/$payment_id"."?access_token=".config('services.mercadopago.token'));



                $reponse=json_decode($reponse);

                $resultado=$reponse->status;

                //muestra los estatus
                //  $this->info("Estatus ".$resultado);

                // $resultado='approved';

                switch ($resultado) {

                    case 'approved':
                        $resultado_str = 'Pago exitoso';
                        $resultados['correctos'] ++;

                        $pedido->pagos[0]->update(['status' => 'SUCCESS']);
                        $pedido->update([ 'status' => 'PAGADO' ]);

                        $pedido->procesar();

                        // Notificamos al usuario que el pago fue exitoso
                        $pedido->sendPagoExitosoNotification();

                        DB::commit();

                        break;

                    case 'in_process':
                    case 'pending': $resultado_str = 'Pago en espera'; $resultados['fallidos'] ++;break;

                    case 'rejected':
                    case 'cancelled':
                    case '404':
                    $resultado_str = 'Pago rechazado o no encontrado'; $resultados['fallidos'] ++;break;

                }



                }//pedidos




        } catch (\Throwable $th) {
            Log::error('RevisaEstadoPagosMp', [$th]);
        }

        $this->info("Se comprobaron {$resultados['total']} pedidos de mercado pago, {$resultados['correctos']} exitosos y {$resultados['fallidos']} fallidos");
        Log::info("Se comprobaron {$resultados['total']} pedidos de mercado pago, {$resultados['correctos']} exitosos y {$resultados['fallidos']} fallidos");
    }
}
