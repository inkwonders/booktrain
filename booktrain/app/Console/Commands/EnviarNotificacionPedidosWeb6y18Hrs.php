<?php

namespace App\Console\Commands;

use App\Models\Colegio;
use App\Models\User;
use App\Notifications\LibrosReordenNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class EnviarNotificacionPedidosWeb6y18Hrs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar_notificacion_pedidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manda llamar una notificación que envia un correo a pedidosweb@booktrain.com a las 6 y a las 18 hrs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /**
         * debe ser un reporte de todos los productos que alcanzaron su punto de reorden, agrupados por colegio, se dede de mostrar el ISBN, nombre, stock y punto de reorden
         */

        $colegios = Colegio::whereHas('libros', function($query) {
            $query->where('paquete_id', null);
            $query->where('punto_reorden', '>', 0);
            $query->whereRaw('stock < punto_reorden');
        })
        ->with(['libros' => function($query) {
            $query->where('paquete_id', null);
            $query->where('punto_reorden', '>', 0);
            $query->whereRaw('stock < punto_reorden');

            $query->with('detalleResumen.resumen.pedido');

        }, 'configuraciones'])
        ->get()
        ->map(function($colegio) {
            return $colegio->libros->map(function($libro) use ($colegio) {
                $ultima_venta = $libro->detalleResumen()
                    // ->where('entregado', 1)
                    ->whereHas('resumen', function($query) use ($colegio) {
                        $query->whereHas('pedido', function($query) use ($colegio) {
                            $query->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']);
                            $query->where('colegio_id', $colegio->id);
                        });
                    })
                    ->orderBy('created_at', 'DESC')
                    ->first();

                return (Object) [
                    'colegio_id'                    => $colegio->id,
                    'colegio'                       => $colegio->nombre,
                    'isbn'                          => $libro->isbn,
                    'nombre'                        => $libro->nombre,
                    'stock'                         => $libro->pivot->stock,
                    'punto_reorden'                 => $libro->pivot->punto_reorden,
                    'bajo_pedido'                   => $libro->pivot->bajo_pedido,
                    'ultima_venta'                  => $ultima_venta ? $ultima_venta->created_at->format('d-m-Y') : 'N/D'
                ];
            });
        })
        ->collapse()
        ->groupBy('colegio_id');

        foreach ($colegios as $colegio_id => $libros) {
            $colegio = Colegio::where('id', $colegio_id)->first();

            $correo_comercial_provesa   = $colegio->config('correo_comercial_provesa');
            $correo_coordinador_provesa = $colegio->config('correo_coordinador_provesa');
            $correo_coordinador_colegio = $colegio->config('correo_coordinador_colegio');

            if($libros->count() > 0) {

                if($correo_comercial_provesa != '') {
                    Log::debug("Enviando notificación por correo a {$correo_comercial_provesa}", ['libros' => $libros]);
                    $this->info("Enviando notificación por correo a {$correo_comercial_provesa}: {$libros->count()} libros");

                    try {
                        \Notification::route('mail', [
                            $correo_comercial_provesa => 'Plataforma Booktrain',
                        ])->notify(new LibrosReordenNotification($libros));
                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_comercial_provesa);
                    } catch (\Throwable $th) {
                        $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }

                if($correo_coordinador_provesa != '') {
                    Log::debug("Enviando notificación por correo a {$correo_coordinador_provesa}", ['libros' => $libros]);
                    $this->info("Enviando notificación por correo a {$correo_coordinador_provesa}: {$libros->count()} libros");

                    try {
                        \Notification::route('mail', [
                            $correo_coordinador_provesa => 'Plataforma Booktrain',
                        ])->notify(new LibrosReordenNotification($libros));
                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_coordinador_provesa);
                    } catch (\Throwable $th) {
                        $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }


                if($correo_coordinador_colegio != '') {
                    Log::debug("Enviando notificación por correo a {$correo_coordinador_colegio}", ['libros' => $libros]);
                    $this->info("Enviando notificación por correo a {$correo_coordinador_colegio}: {$libros->count()} libros");

                    try {
                        \Notification::route('mail', [
                            $correo_coordinador_colegio => 'Plataforma Booktrain',
                        ])->notify(new LibrosReordenNotification($libros));
                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_coordinador_colegio);
                    } catch (\Throwable $th) {
                        $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }
            }
        }
    }
}
