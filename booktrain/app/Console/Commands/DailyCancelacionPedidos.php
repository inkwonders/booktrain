<?php

namespace App\Console\Commands;

use App\Models\Pedido;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Transactions\DbTransactionHandler;

class DailyCancelacionPedidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cancelacionPedido:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command que actualizará el status de los pedidos a cancelado si lleva 10 dias o mas de creado y status en Carrito o Procesado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // codigo para cancelar pedidos
        DB::beginTransaction();
        try {
            $this->info(" ---- Entrando a ancelacionPedido:daily ---- ");
            Log::debug(" ---- Entrando a ancelacionPedido:daily ---- ");

            $pedidos = Pedido::whereIn('status', ['PROCESADO'])
                ->whereRaw("(TO_DAYS(CURRENT_DATE) - TO_DAYS(created_at)) >= 11")
                ->get();

                $this->info("Se encontraron " . count($pedidos) . " pedidos");
                Log::debug("Se encontraron " . count($pedidos) . " pedidos");

            // Recuperamos el stock de los libros del pedido
            $pedidos->each(function($pedido) use ($pedidos) {

                //por cada pedido:
                $pedido->cancelar();
            });

        } catch (\Throwable $th) {
            DB::rollback();
            // Reportar el error
            $this->error('Ocurrio un error al actualizar los registros:' . $th->getMessage());
            Log::debug('Ocurrio un error al actualizar los registros:' . $th->getMessage());
        }

        DB::commit();

        $this->info('Se ejecuto correctamente la cancelacion de pedidos diaria.');
        Log::debug(" ---- Se ejecuto correctamente la cancelacion de pedidos diaria. ----");
    }
}
