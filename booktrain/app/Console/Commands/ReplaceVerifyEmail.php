<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReplaceVerifyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reemplazar:verify';

    /**
     * The console Reemplaza el archivo vendor\laravel\framework\src\Illuminate\Auth\Notifications\VerifyEmail.php por uno personalizado.
     *
     * @var string
     */
    protected $description = 'Reemplaza el archivo vendor\laravel\framework\src\Illuminate\Auth\Notifications\VerifyEmail.php por uno personalizado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        file_put_contents(
            base_path('vendor/laravel/framework/src/Illuminate/Auth/Notifications/VerifyEmail.php'),
            file_get_contents(
                storage_path('private/scripts/VerifyEmail.php')
            )
        );
    }
}
