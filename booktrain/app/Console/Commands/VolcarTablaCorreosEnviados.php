<?php

namespace App\Console\Commands;

use Alchemy\Zippy\Zippy;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class VolcarTablaCorreosEnviados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'volcar:correos_enviados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Vacía el contenido de la tabla de correos enviados a un archivo plano en storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $consulta = DB::table(DB::raw("information_schema.TABLES"))
            ->select(
                DB::raw("ROUND((DATA_LENGTH + INDEX_LENGTH) ) AS size")
            )
            ->where("TABLE_SCHEMA", env('DB_DATABASE'))
            ->where("TABLE_NAME", 'sent_emails')
            ->first();

        $this->info('Iniciando volcado de información, el tamaño inicial de la tabla es ' . number_format($consulta->size / 1024, 2) . ' KB');

        $contador = 0;
        $path = storage_path('sent_emails');

        if(is_dir($path) == false) {
            $this->info("El directorio '{$path}' no existe, se creará");

            mkdir($path);
        }

        DB::table('sent_emails')
        ->where('content', '!=', null)
        ->cursor()
        ->each(function($sent_email) use ($path, &$contador) {
                try {
                    $fp = fopen( "{$path}/{$sent_email->id}-{$sent_email->recipient_email}.html", 'w' );

                    fputs($fp, $sent_email->content);

                    fclose($fp);

                    $contador ++;
                } catch (\Throwable $th) {
                    $this->error('Error al escribir el archivo de volcado: ' . $th->getMessage());
                    return 0;
                }
            });
        DB::beginTransaction();

        try {
            DB::table('sent_emails')
            ->where('content', '!=', null)
            ->update([
                'content' => null
            ]);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $this->error('Error al cambiar el contenido de correo' . $th->getMessage());

            return 0;
        }

        $this->info("Se volcaron {$contador} correos");

        $zippy = Zippy::load();

        $path_zip = storage_path('volcado_emails_' . time() . '.zip');

        $zippy->create(
            $path_zip,
            [
                'folder' => $path
            ],
            true // Recursivo
        );

        $size = number_format(filesize($path_zip) / 1024, 2);
        $this->info("Se comprimió el resultado en {$path_zip}, {$size} KBytes");

        $contador = 0;

        $dir = opendir($path);
        while (false !== ($current = readdir($dir))) {
            if($current != '.' && $current != '..') {
                unlink("{$path}/{$current}");
                $contador ++;
            }
        }

        closedir($dir);
        rmdir($path);

        $this->info('Revisando tabla: ' . (DB::statement('CHECK TABLE sent_emails;') ? 'OK' : 'Error'));
        $this->info('Analizando tabla: ' . (DB::statement('ANALYZE TABLE sent_emails;') ? 'OK' : 'Error'));
        $this->info('Optimizando tabla: ' . (DB::statement('OPTIMIZE TABLE sent_emails;') ? 'OK' : 'Error'));

        $consulta = DB::table(DB::raw("information_schema.TABLES"))
        ->select(
            DB::raw("ROUND((DATA_LENGTH + INDEX_LENGTH) ) AS size")
        )
        ->where("TABLE_SCHEMA", env('DB_DATABASE'))
        ->where("TABLE_NAME", 'sent_emails')
        ->first();

        $this->info('El tamaño final de la tabla es ' . number_format($consulta->size / 1024, 2) . ' KB');

        $this->info("Se eliminaron {$contador} archivos temporales");
    }
}
