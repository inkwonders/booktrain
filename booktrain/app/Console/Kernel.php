<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\DailyCancelacionPedidos::class,
        Commands\BatchEnvioNotificacionPago::class,
        Commands\CancelarPedidosPagosRechazados::class,
        Commands\EliminarCarritosAbandonados::class,
        Commands\EnviarNotificacionPedidosWeb6y18Hrs::class,
        Commands\RevisaEstadoPagos::class,
        Commands\RevisaEstadoPagosMp::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cancelacionPedido:daily')->dailyAt('00:00');
        $schedule->command('BatchEnvioNotificacionPago')->hourly();
        $schedule->command('cancelar_pedidos:pago_abandonado')->hourly();
        $schedule->command('eliminar_pedidos:carrito_abandonado')->dailyAt('01:00');
        $schedule->command('enviar_notificacion_pedidos')->dailyAt('17:00');
        $schedule->command('enviar_notificacion_pedidos')->dailyAt('06:00');
        $schedule->command('pagos:revisar')->dailyAt('08:30');
        $schedule->command('pagosmp:revisar')->dailyAt('18:00');
        $schedule->command('pagosmp:revisar')->dailyAt('00:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
