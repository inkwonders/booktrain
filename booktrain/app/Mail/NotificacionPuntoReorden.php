<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificacionPuntoReorden extends Mailable
{
    use Queueable, SerializesModels;

    public $fecha;
    public $libros;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fecha, $libros)
    {
        $this->fecha = $fecha;
        $this->libros = $libros;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notificacion_punto_reorden');
    }
}
