<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Livewire\Component;
use App\Models\DetalleResumenPedido;
use App\Models\ResumenPedido;

class ContenedorReportePedidosGeneral extends Component
{
    public $modal_reorden = false;
    public $modal_status = false;
    public $pedido_modal;
    public $pedido_modal_status;
    public $envios_parciales;
    protected $listeners = ['modalDetallesPedido', 'modalCambioEstatus'];

    public function modalDetallesPedido($pedido_id)
    {
        $this->pedido_modal = Pedido::find($pedido_id);
        $this->modal_reorden = true;
        $this->envios_parciales = [];
        foreach ($this->pedido_modal->resumenes as $resumen) {
            foreach ($resumen->detallesResumenPedidos as $detalle) {
                if (!empty($detalle->envio)) {
                    array_push($this->envios_parciales, $detalle->envio);
                }
            }
        }
    }

    public function modalCambioEstatus($pedido_status_id)
    {
        $this->pedido_modal_status = Pedido::where('id', $pedido_status_id)->first();
        $this->modal_status = true;
    }

    public function cerrarModal()
    {
        $this->modal_reorden = false;
    }
    public function cerrarModalStatus()
    {
        $this->modal_status = null;
    }

    public function render()
    {
        return view('livewire.contenedor-reporte-pedidos-general');
    }
}
