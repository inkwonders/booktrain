<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Notificacion;
use Livewire\Component;

class NuevoAvisoColegio extends Component
{
    public Notificacion $notificacion;
    public Colegio $colegio;

    protected $listeners = ['editAvisoColegio'];

    protected $rules = [
        'notificacion.titulo' => 'required',
        'notificacion.aviso' => 'required',
        'notificacion.activo' => 'sometimes'
    ];

    public function mount() {
        $this->notificacion = new Notificacion;
    }

    public function editAvisoColegio(Notificacion $notificacion) {
        $this->notificacion = $notificacion;
    }

    public function save() {
        $this->notificacion->activo = boolval($this->notificacion->activo);

        $this->colegio->notificaciones()->save(
            $this->notificacion
        );
        $this->emit('refreshListaNotificaciones');
        $this->notificacion = new Notificacion;
    }

    public function render()
    {
        return view('livewire.nuevo-aviso-colegio');
    }
}
