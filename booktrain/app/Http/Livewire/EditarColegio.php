<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Configuracion;
use Illuminate\Support\Facades\Log;

class EditarColegio extends Component
{
    use WithFileUploads;

    public $colegio;
    public $logo;

    public $correo_coordinador_colegio;
    public $correo_comercial_provesa;
    public $correo_coordinador_provesa;

    protected $rules = [
        'colegio.codigo' => 'required|alpha_num',
        'colegio.nombre' => 'required',
        'colegio.activo' => 'sometimes',
        'correo_comercial_provesa' => 'sometimes|email',
        'correo_coordinador_colegio' => 'sometimes|email',
        'correo_coordinador_provesa' => 'sometimes|email',
    ];

    public function mount($colegio)
    {
        if($colegio == null) {
            $this->colegio = new Colegio;

            $this->correo_comercial_provesa = Configuracion::where('etiqueta', 'correo_coordinador_colegio')->first()->valor;
            $this->correo_coordinador_provesa = Configuracion::where('etiqueta', 'correo_comercial_provesa')->first()->valor;
        }else{
            $this->correo_comercial_provesa = $colegio->config('correo_comercial_provesa');
            $this->correo_coordinador_colegio = $colegio->config('correo_coordinador_colegio');
            $this->correo_coordinador_provesa = $colegio->config('correo_coordinador_provesa');
        }
    }

    public function render()
    {
        return view('livewire.editar-colegio');
    }

    public function save()
    {
        $nombre_archivo = "logo_" . $this->colegio->codigo;
        if( ! isset($this->colegio->activo))
            $this->colegio->activo = 0;

        $this->validate();

        if($this->colegio->exists) {

            Log::debug('entro a editar colegio');

            if($this->logo != null){
                $this->colegio->logo = '/storage/' . "logo_" . $this->colegio->codigo . '_' . $this->logo->getClientOriginalName();
                $this->logo->storeAs('public', "logo_" . $this->colegio->codigo . '_' . $this->logo->getClientOriginalName());
            }

            Log::debug('si tiene imagen:',  ['imagen' => $this->colegio->logo ]);

            $this->colegio->save();

            $this->colegio->configuraciones()->where('etiqueta', 'correo_comercial_provesa')->update(['valor' => $this->correo_comercial_provesa]);
            $this->colegio->configuraciones()->where('etiqueta', 'correo_coordinador_provesa')->update(['valor' => $this->correo_coordinador_provesa]);
            $this->colegio->configuraciones()->where('etiqueta', 'correo_coordinador_colegio')->update(['valor' => $this->correo_coordinador_colegio]);

            $this->emit('swalAlert', ['title' => 'Cambios guardados', 'text' => "Se editó correctamente el colegio {$this->colegio->codigo}", 'icon' => 'success']);

            activity()
            ->performedOn($this->colegio)
            ->causedBy(Auth::user()->id)
            ->log('Se editó el colegio: ' . $this->colegio->codigo);
        }else{

            Log::debug('entro a agregar colegio');

            $this->validate([
                'logo' => 'image',
                'colegio.codigo' => 'unique:colegios,codigo'
            ]);

            $this->colegio->logo = '/storage/' . "logo_" . $this->colegio->codigo . '_' . $this->logo->getClientOriginalName();
            $this->logo->storeAs('public', "logo_" . $this->colegio->codigo . '_' . $this->logo->getClientOriginalName());

            Log::debug('si tiene imagen:',  ['imagen' => $this->colegio->logo ]);

            $this->colegio->save();

            $this->colegio->configuraciones()->where('etiqueta', 'correo_comercial_provesa')->update(['valor' => $this->correo_comercial_provesa]);
            $this->colegio->configuraciones()->where('etiqueta', 'correo_coordinador_provesa')->update(['valor' => $this->correo_coordinador_provesa]);
            $this->colegio->configuraciones()->where('etiqueta', 'correo_coordinador_colegio')->update(['valor' => $this->correo_coordinador_colegio]);

            $this->emit('swalAlert', ['title' => 'Colegio creado', 'text' => "Se registró correctamente el colegio {$this->colegio->codigo}", 'icon' => 'success']);

            activity()
            ->performedOn($this->colegio)
            ->causedBy(Auth::user()->id)
            ->log('Se creó el colegio: ' . $this->colegio->codigo);

            $this->colegio = new Colegio;
            $this->logo = null;
            $this->correo_comercial_provesa = '';
            $this->correo_coordinador_provesa = '';
            $this->correo_coordinador_colegio = '';
        }
    }
}
