<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Notificacion;
use Livewire\Component;

class ListaAvisosColegio extends Component
{
    public $avisos;
    public Colegio $colegio;

    protected $listeners = ['refreshListaNotificaciones'];

    public function mount() {
        $this->refreshListaNotificaciones();
    }

    public function refreshListaNotificaciones() {
        $this->avisos = $this->colegio->notificaciones;
    }

    public function delete(Notificacion $notificacion) {
        $notificacion->delete();
        $this->emit('refreshListaNotificaciones');
    }

    public function render()
    {
        return view('livewire.lista-avisos-colegio');
    }
}
