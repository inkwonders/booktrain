<?php

namespace App\Http\Livewire;

use App\Models\Caja;
use App\Models\Colegio;
use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Backpack\PermissionManager\app\Models\Permission;
use Backpack\PermissionManager\app\Models\Role;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\Facades\Auth;

class EditarUsuario extends Component
{
    public User $user;
    public $new_password = "";
    public $password_confirmation = "";
    public $mensaje_error = "";
    public $roles_elegidos = [];
    public $roles_usuario = [];
    public $rol_usuario;
    public $roles;
    public $permisos;
    public $permisos_elegidos = [];
    public $permisos_usuario = [];
    public $colegios;
    public $colegio_asignado;

    public $mostrar_admin = false;
    public $mostrar_colegios = false;

    // public $usuario_venta_movil = false;
    public $caja_abierta = false;
    // public $colegio_elegido = 0;

    protected $listeners = [];

    protected $rules = [
        'user.name' => 'required',
        'user.email' => 'required',
        'user.apellidos' => 'required',
        // 'new_password' => 'sometimes|size:6',
        // 'password_confirmation' => 'sometimes|size:6'
    ];

    public function mount(User $user)
    {
        $this->mostrar_admin = false;
        $this->mostrar_colegios = false;
        $this->colegios = Colegio::all();
        $this->user = $user;
        $this->roles = Role::all();
        $this->permisos = Permission::get()->unique()->whereNotIn(
            'name',
            $this->colegios->pluck('codigo')
        );
        $this->rol_usuario = $user->roles()->first()->name;

        if ($this->user->hasRole(['Colegio', 'Venta Movil'])) {
            $colegio_asignado = $this->user->permissions->whereIn(
                'name',
                $this->colegios->pluck('codigo')
            )->first();

            if ($colegio_asignado)
                $this->colegio_asignado = $colegio_asignado->name;
            else
                $this->colegio_asignado = 0;
        }

        if ($this->rol_usuario == "Admin") {
            $this->mostrar_admin = true;
        }

        if ($this->rol_usuario == "Colegio" ||  $this->rol_usuario == "Venta Movil") {
            $this->mostrar_colegios = true;
        }

        if ($this->rol_usuario == "Venta Movil") {
            if($colegio = Colegio::whereIn('codigo', $this->user->getDirectPermissions()->pluck('name'))->first()) {
                $check_caja = $this->user->cajas()->where('colegio_id', $colegio->id)->get()->last();
                if($check_caja && $check_caja->status == "ABIERTA") {
                    $this->caja_abierta = true;
                }
            }
        }

        $this->refrescaRolesUsuario();
    }

    public function refrescaRolesUsuario()
    {
        $this->mostrar_admin = false;
        $this->mostrar_colegios = false;
        if ($this->rol_usuario == "Admin") {
            $this->mostrar_admin = true;
        }

        if ($this->rol_usuario == "Colegio" ||  $this->rol_usuario == "Venta Movil") {
            $this->mostrar_colegios = true;
        }

        $user = $this->user;
        $this->permisos_usuario = $this->permisos->map(function ($permiso_base) use ($user) {
            return [
                'id' => $permiso_base->id,
                'nombre' => $permiso_base->name,
                // 'heredado' => $user->roles->pluck('permissions')->collapse()->contains('name', $permiso_base->name) ? 1 : 0,
                'heredado' => 0,
                'activo' => $user->hasDirectPermission($permiso_base->name)
                // 'activo' => $user->hasPermissionTo($permiso_base->name)
            ];
        })
            ->keyBy('id')
            ->toArray();
    }

    public function updatedColegioAsignado()
    {
        if ($this->caja_abierta) {
            $this->emit('swalAlert', ['title' => 'Error', 'text' => 'No se puede cambiar de colegio debido a que el usuario tiene una caja abierta.', 'icon' => 'error']);
        }
    }

    public function updated($name, $value)
    {
        if ($value == "Admin" || $value ==  "Colegio" || $value ==  "Venta Movil" || $value ==  "Cliente") {
            if ($this->caja_abierta) {
                $this->emit('swalAlert', ['title' => 'Error', 'text' => 'No se puede cambiar el rol del usuario debido a que tiene una caja abierta.', 'icon' => 'error']);
            }
            $this->mostrar_admin = false;
            $this->mostrar_colegios = false;

            if ($value == "Admin")
                $this->mostrar_admin = true;

            if ($value == "Colegio" || $value == "Venta Movil")
                $this->mostrar_colegios = true;
        }


        $props = explode('.', $name);

        if ($props[0] == 'permisos_usuario') {
            $this->permisos_usuario[$props[1]]['activo'] = $value;
        }

        if ($props[0] == 'rol_usuario') {
            $this->rol_usuario = $value;
        }
    }

    public function render()
    {
        return view('livewire.editar-usuario');
    }

    public function save()
    {
        if (!$this->caja_abierta) {
            $this->user->syncRoles($this->rol_usuario);

            // Eliminamos o asignamos los permisos no heredados
            foreach ($this->permisos_usuario as $permiso_usuario) {
                if ($permiso_usuario['heredado'] == false)
                    if ($permiso_usuario['activo'])
                        $this->user->givePermissionTo($permiso_usuario['nombre']);
                    else
                        $this->user->revokePermissionTo($permiso_usuario['nombre']);
            }
        }

        $this->refrescaRolesUsuario();

        $this->mensaje_error = '';
        if ($this->new_password != "") {
            if ($this->new_password == $this->password_confirmation) {
                if (strlen($this->new_password) < 6) {
                    $this->mensaje_error = "La contraseña debe ser mayor a 6 caracteres";
                } else {
                    $this->user->password =  Hash::make($this->new_password);
                    // $this->validate();
                    $this->user->save();
                    $this->dispatchBrowserEvent('editar_correcto');
                    $user = new User;
                    activity()
                        ->performedOn($user)
                        ->causedBy(Auth::user()->id)
                        ->log('Editó un usuario - (' . $this->user->email . ')');
                }
            } else {
                $this->mensaje_error = "Las contraseñas no coinciden";
            }
        } else {
            // $this->validate();
            $this->user->save();
            $this->dispatchBrowserEvent('editar_correcto');
            $user = new User;
            activity()
                ->performedOn($user)
                ->causedBy(Auth::user()->id)
                ->log('Editó un usuario - (' . $this->user->email . ')');
        }
        if (!$this->caja_abierta) {
            if ($this->colegio_asignado != '') {
                // Si el permiso no existe, lo creamos
                Permission::firstOrCreate(['name' => $this->colegio_asignado]);

                // Validamos que el rol del usuario sea Colegio
                if ($this->user->hasRole(['Colegio', 'Venta Movil']))
                    $this->user->syncPermissions($this->colegio_asignado);
            }
        }
    }
}
