<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\TipoMovimientoAlmacen;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NuevoMovimientoAlmacen extends Component
{
    public $modal_confirmar_movimiento = false;
    public $tipos_movimientos;
    public $select_tipo_movimiento = 0;
    public $colegios;
    public $select_colegio_1;
    public $select_colegio_2;
    public $filtro = '';
    public $comentarios = '';
    public $libros_movimiento = [];

    protected $rules = [
        'select_tipo_movimiento' => 'numeric|min:0'
    ];

    public function mount()
    {
        $this->tipos_movimientos = TipoMovimientoAlmacen::get();
        $this->colegios = Colegio::all();
    }

    public function filtrarLibros()
    {
        $colegio = null;
        if ($this->select_tipo_movimiento == 99) {
            // Traspaso de libros entre colegios

            if ($this->select_colegio_2 != null && $this->select_colegio_1 != null && $this->select_colegio_1 != $this->select_colegio_2) {
                // Otro tipo de movimiento
                $colegio = Colegio::where('id', $this->select_colegio_1)->first();
            }
        } else {
            if ($this->select_colegio_2 != null) {
                // Otro tipo de movimiento
                $colegio = Colegio::where('id', $this->select_colegio_2)->first();
            }
        }

        if (null !== $colegio) {
            $this->libros_movimiento = $colegio
                ->libros()
                ->wherePivot('paquete_id', null)
                ->get()
                ->transform(function ($libro) {
                    return [
                        'id' => $libro->id,
                        'isbn' => $libro->isbn,
                        'titulo' => $libro->nombre,
                        'seleccionado' => false,
                        'cantidad' => 0,
                        'stock' => $libro->pivot->stock,
                        'max' => $libro->pivot->stock
                    ];
                })
                ->toArray();
        }
    }

    public function updated($name, $value)
    {
        $params = explode('.', $name);

        if ($params[0] == 'libros_movimiento') {
            if ($params[2] == 'cantidad' && $value < 0)
                $this->libros_movimiento[$params[1]]['cantidad'] = 0;

            if ($this->select_tipo_movimiento == 99) {
                if ($this->libros_movimiento[$params[1]]['cantidad'] > $this->libros_movimiento[$params[1]]['max'])
                    $this->libros_movimiento[$params[1]]['cantidad'] = $this->libros_movimiento[$params[1]]['max'];

                if ($this->libros_movimiento[$params[1]]['cantidad'] < 0)
                    $this->libros_movimiento[$params[1]]['cantidad'] = 0;

                $this->libros_movimiento[$params[1]]['seleccionado'] = ($this->libros_movimiento[$params[1]]['cantidad'] > 0);
            }
        }
    }

    public function disminuirCantidadMovimiento($id)
    {
        if ($this->libros_movimiento[$id]['cantidad'] > 0) {
            $this->libros_movimiento[$id]['cantidad'] = $this->libros_movimiento[$id]['cantidad'] - 1;

            if ($this->libros_movimiento[$id]['cantidad'] == 0)
                $this->libros_movimiento[$id]['seleccionado'] = false;
        }
    }

    public function aumentarCantidadMovimiento($id)
    {
        $delta = 1;
        // En caso de ser transferencia el límite es el stock
        if ($this->select_tipo_movimiento == 99) {
            if ($this->libros_movimiento[$id]['cantidad'] >= $this->libros_movimiento[$id]['max'])
                $delta = 0;
        }

        if ($delta > 0) {
            $this->libros_movimiento[$id]['cantidad'] = $this->libros_movimiento[$id]['cantidad'] + $delta;
            $this->libros_movimiento[$id]['seleccionado'] = true;
        }
    }

    public function confirmarMovimiento()
    {
        $operaciones = Collect($this->libros_movimiento)->where('seleccionado', true);

        if ($this->comentarios == '')
            $this->emit('swalAlert', [
                'title' => 'Error',
                'text' => 'Los comentarios sobre la operación son obligatorios',
                'icon' => 'error'
            ]);
        else
            if ($this->select_tipo_movimiento == 99) {
            if (app('App\Http\Controllers\AlmacenController')->traspasoInventarios($this->select_colegio_1, $this->select_colegio_2, $operaciones, $this->comentarios)) {

                $movimiento = new TipoMovimientoAlmacen();
                activity()
                    ->performedOn($movimiento)
                    ->causedBy(Auth::user()->id)
                    ->log('Realizó un cambió en almacén de tipo: TRANSPASO. Colegio origen:' . Colegio::find($this->select_colegio_1)->nombre . ' | Colegio destino: ' . Colegio::find($this->select_colegio_2)->nombre);
                // Exito
                $this->modal_confirmar_movimiento = false;
                $this->select_colegio_1 = 0;
                $this->select_colegio_2 = 0;
                $this->libros_movimiento = [];
                $this->comentarios = '';

                // Mostrar swal2 de exito
                $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'Los inventarios fueron afectados correctamente', 'icon' => 'success']);


                // $tipos_movimientos->firstWhere('id', $select_tipo_movimiento)->naturaleza
            } else {
                $this->emit('swalAlert', ['title' => 'Error', 'text' => 'Las operaciones no pudieron ser ejecutadas', 'icon' => 'error']);
            }
        } else {
            if (app('App\Http\Controllers\AlmacenController')->movimientoManual($this->select_colegio_2, $this->select_tipo_movimiento, $operaciones, $this->comentarios)) {
                $movimiento = new TipoMovimientoAlmacen();
                activity()
                    ->performedOn($movimiento)
                    ->causedBy(Auth::user()->id)
                    ->log('Realizó un cambió en almacén de tipo: ' . TipoMovimientoAlmacen::find($this->select_tipo_movimiento)->naturaleza . ' | Tipo de entrada: "' . TipoMovimientoAlmacen::find($this->select_tipo_movimiento)->descripcion . '", para el colegio: ' . Colegio::find($this->select_colegio_2)->nombre);

                $this->modal_confirmar_movimiento = false;
                $this->select_colegio_1 = 0;
                $this->select_colegio_2 = 0;
                $this->libros_movimiento = [];
                $this->comentarios = '';

                $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El inventario fue afectado correctamente', 'icon' => 'success']);
            } else {
                $this->emit('swalAlert', ['title' => 'Error', 'text' => 'Las operaciones no pudieron ser ejecutadas', 'icon' => 'error']);
            }
        }
    }

    public function render()
    {
        return view('livewire.nuevo-movimiento-almacen');
    }
}
