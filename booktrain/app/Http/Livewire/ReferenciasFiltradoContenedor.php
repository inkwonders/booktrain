<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ReferenciasFiltradoContenedor extends Component
{
    public function render()
    {
        return view('livewire.referencias-filtrado-contenedor');
    }
}
