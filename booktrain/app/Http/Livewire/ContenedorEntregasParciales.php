<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Livewire\Component;
use App\Models\DetalleResumenPedido;
use App\Models\ResumenPedido;

class ContenedorEntregasParciales extends Component
{
    public $modal_reorden = false;
    public $pedido_modal;
    public $switch_activo;
    public $libros_arreglo=[];
    protected $listeners = ['modalDetallesPedido'];

    public function modalDetallesPedido($pedido_id) {
        $this->pedido_modal = Pedido::where('id', $pedido_id)->first();
        $this->modal_reorden = true;
    }
    public function save(DetalleResumenPedido $detalle_resumen_pedido)
    {
            foreach ($this->libros_arreglo as $libro)
            {
                $libro_actualizar = DetalleResumenPedido::find($libro);
                if($libro_actualizar->entregado == 1){
                    $libro_actualizar->entregado = 0;
                    Pedido::where('id', $libro_actualizar->resumenPedido->pedido->id)->update(['status' => 'ENTREGA PARCIAL']);
                }else{
                    $libro_actualizar->entregado=1;
                    Pedido::where('id', $libro_actualizar->resumenPedido->pedido->id)->update(['status' => 'ENTREGA PARCIAL']);
                }
                $libro_actualizar->save();
            }

                // se busca el id de alguno de los libros clikados en la tabla de talle de resumen pedido
                $resumen_pedido = DetalleResumenPedido::find($libro);

                // ya que se encuentra se busca el id del pedido en resumen pedido para saber cuantos resumenes tiene ese pedido, puede tener uno o mas
                $resumenes_de_pedidos = ResumenPedido::where('pedido_id',$resumen_pedido->resumenPedido->pedido_id)->get();


                $esta_entregado = true;

                foreach ($resumenes_de_pedidos as $resumen_pedido) {
                    foreach ($resumen_pedido->detalles as $detalle) {
                        if ($detalle->entregado == 0) {
                            $esta_entregado = false;
                        }
                    }
                }

                if($esta_entregado == true){
                    Pedido::where('id', $resumenes_de_pedidos->first()->pedido->id)->update(['status' => 'ENTREGADO']);
                }

            $this->cerrarModal();
            return redirect()->to('/tabla_entregas_parciales');
    }

    public function cerrarModal() {
        $this->modal_reorden = null;
    }

    public function libros_entregar($id_libro){
        array_push($this->libros_arreglo, $id_libro);
    }

    public function render()
    {
        return view('livewire.contenedor-entregas-parciales');
    }
}
