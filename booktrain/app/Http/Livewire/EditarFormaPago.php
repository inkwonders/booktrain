<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Livewire\Component;

class EditarFormaPago extends Component
{
    public $colegio = '';
    public $nombre = '';
    public $clave = '';
    public $meses = '';
    public $comision = '';
    public $minimo = '';
    public $activo = '';
    public $forma_pago;
    public $forma_pago_id;
    public $metodo_pago_id;

    protected $rules = [
        'meses'      => 'required',
        'comision'   => 'required',
        'minimo'     => 'required'
    ];

    public function mount($colegio, $metodo_pago_id, $forma_pago_id)
    {
        $forma_pago = $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_id)->first()->formasPago()->wherePivot('forma_pago_id', $forma_pago_id)->first();

        $this->nombre = $forma_pago->descripcion;
        $this->clave = $forma_pago->codigo;
        $this->meses = $forma_pago->pivot->meses;
        $this->comision = $forma_pago->pivot->comision;
        $this->minimo = $forma_pago->pivot->minimo;
        $this->activo = $forma_pago->pivot->activo;

        $this->forma_pago = $forma_pago;
    }

    public function save()
    {
        $this->validate();

        $this->colegio->metodosPago()->wherePivot('metodo_pago_id', $this->metodo_pago_id)->first()->formasPago()
            ->updateExistingPivot($this->forma_pago_id, [
                'meses' => $this->meses,
                'comision' => $this->comision,
                'minimo' => $this->minimo,
                'activo' => $this->activo,
            ]);

        $this->emit('swalAlert', ['title' => 'Forma de pago actualizada', 'text' => "La forma de pago '{$this->forma_pago->descripcion}' fue actualizada correctamente", 'icon' => 'success']);

        // $configuracion = $this->colegio->configuracion;
        // if (isset($configuracion->metodos_pago[$this->metodo_pago_id]->formas_pago[$this->forma_pago_id])) {
        //     $configuracion->metodos_pago[$this->metodo_pago_id]->formas_pago[$this->forma_pago_id]->comision = (float) $this->comision;
        //     $configuracion->metodos_pago[$this->metodo_pago_id]->formas_pago[$this->forma_pago_id]->minimo = (float)$this->minimo;
        // }

        // $this->colegio->update([
        //     'configuracion' => $configuracion
        // ]);

        $this->emit('refreshColegio');
    }

    public function render()
    {
        return view('livewire.editar-forma-pago');
    }
}
