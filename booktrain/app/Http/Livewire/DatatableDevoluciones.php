<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use App\Models\ServicioPaqueteria;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class DatatableDevoluciones extends DataTableComponent
{
    public $model = Pedido::class;

    public $pedidos;
    public $detalle_resumen_pedido;
    public $pedido_id;
    public $select_inicio;
    public $select_fin;
    public $modal_devoluciones = false;
    public $pedido_modal;
    public $libros_pendientes_devolucion = 0;
    public $total_libros_pedido = 0;
    public $filtro_devuelto = 1;
    public $inicio = '';
    public $fin = '';
    public $selectFaltaMaterial = false;
    public $mensaje_devolucion = '';
    public $comentariosDevolucion = '';

    public $table_headers = 'livewire.header-devoluciones';

    public $libros_devueltos = [];

    protected $rules = [
        'no_guia' => 'required|min:6',
        'paqueteria' => 'required|exists:servicios_paqueteria,id',
    ];

    public function mount()
    {
        $this->inicio = date('Y-m-d');
        $this->fin = date('Y-m-d');
    }

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function inicio($inicio)
    {
        $this->inicio = $inicio;
    }
    public function fin($fin)
    {
        $this->fin = $fin;
    }


    public function getDevolucionDisponibleProperty() {
        return collect($this->libros_devueltos)->collapse()->map(function($libro) {
            return ($libro['seleccionado'] && ! $libro['devuelto']);
        })->sum() > 0;
    }

    function columns(): array
    {
        return [
            Column::make('Estado'),
            Column::make('Folio', 'folio')             ->sortable()->searchable(),
            Column::make('Fecha','created_at')         ->sortable()->searchable(),
            Column::make('Devoluciones'),
            Column::make('Nombre', 'nombre_contacto')  ->sortable()->searchable(),
            Column::make('Colegio'),
        ];
    }

    public function query(): Builder
    {
        $query = Pedido::query()
            ->whereIn('status',['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'])
            ->when($this->filtro_devuelto, function($query, $devuelto) {
                if($devuelto == 1)
                $query->whereHas('resumenes.detalles', function($query) {
                    $query->where('devuelto', 1);
                });
            })
            ->whereBetween('created_at', ["{$this->inicio} 00:00:00", "{$this->fin} 23:59:59"])
            ->orderByRaw("CAST(folio as UNSIGNED) DESC");

        return $query;
    }

    public function rowView(): string
    {
        return ('livewire.datatable-devoluciones');
    }

    public function modalsView(): string
    {
        return 'livewire.modal-datatable-devoluciones';
    }

    public function getComisionDevolucionProperty() {
        if($this->pedido_modal != null) {

            if( ! $this->selectFaltaMaterial)
                return collect($this->libros_devueltos)->collapse()->map(function($detalle) {
                    if($detalle['seleccionado'])
                        return $detalle['comision_devolucion'];

                    return 0;
                })->sum();
        }

        return 0;
    }

    public function modalEnvioParcial(Pedido $pedido)
    {
        $this->total_libros_pedido = $pedido->detalles->count();
        $this->libros_pendientes_devolucion = $this->total_libros_pedido - $pedido->detalles()->where('devuelto', 1)->count();

        $es_venta_movil = $pedido->usuario_venta_id != null;

        $this->libros_devueltos = $pedido->resumenes->mapWithKeys(function($resumen) use ($pedido, $es_venta_movil) {

            return [
                $resumen->id => $resumen->detalles->mapWithKeys(function ($detalle) use ($pedido, $es_venta_movil) {

                    $comision = 0;
                    if( in_array($pedido->status, ['PAGADO', 'ENVIADO', 'ENVIO PARCIAL', 'ENTREGADO', 'ENVIO LIQUIDADO', 'ENTREGA PARCIAL', 'ENTREGA LIQUIDADA']) ) {
                        $comision = 0.05; // 5% del costo del libro
                    }

                    if( ! $es_venta_movil)
                        if($detalle->enviado) {
                            $comision = 0.10; // 10% del costo del libro
                        }

                    return [
                        $detalle->id =>
                        [
                            'id'                    => $detalle->id,
                            'libro'                 => "{$detalle->libro->nombre}" . ($detalle->cantidad > 1 ? " ({$detalle->cantidad})" : ""),
                            'isbn'                  => $detalle->libro->isbn,
                            'libro_id'              => $detalle->libro_id,
                            'cantidad'              => $detalle->cantidad,
                            'seleccionado'          => 0 || $detalle->devuelto,
                            'enviado'               => $detalle->enviado,
                            'entregado'             => $detalle->entregado,
                            'precio_libro'          => $detalle->precio_libro,
                            'devuelto'              => $detalle->devuelto,
                            'fecha_devolucion'      => $detalle->fecha_devolucion ? $detalle->fecha_devolucion->format('d/m/Y') : '',
                            'fecha_entrega'         => $detalle->fecha_entrega,
                            'comision_devolucion'   => $detalle->devuelto ? 0 : $comision * $detalle->precio_libro * $detalle->cantidad,
                            'comision'              => $detalle->devuelto ? 0 : $comision,
                            'descuento_devolucion'  => $detalle->devuelto ? 0 : $detalle->descuento_devolucion,
                            'causa_devolucion'      => $detalle->razon_devolucion
                        ]
                    ];
                })
            ];
        });

        $this->pedido_modal = $pedido;
        $this->modal_devoluciones = true;
    }

    public function save() {
        if( ! $this->devolucion_disponible)
            return;

        DB::beginTransaction();

        try {
            $detalles = collect($this->libros_devueltos)
                ->collapse()
                ->where('seleccionado', 1)
                ->where('devuelto', 0)
                ->keyBy('id');

            $this->pedido_modal
                ->detalles()
                ->whereIn('detalles_resumen_pedidos.id', $detalles->pluck('id') )
                ->get()
                ->each(function($detalle) use ($detalles) {
                    // Registramos el nuevo precio en el detalle del resumen
                    $porcentaje_descuento_devolucion = 1;

                    if( $this->selectFaltaMaterial == false )
                        $porcentaje_descuento_devolucion = (1 - $detalles[$detalle->id]['comision']);

                    $descuento_devolucion = $porcentaje_descuento_devolucion * $detalle->precio_libro * $detalle->cantidad;

                    $es_venta_movil = $this->pedido_modal->usuario_venta_id != null;

                    if($this->selectFaltaMaterial)
                        $razon_devolucion = 'FALTA DE STOCK';
                    else
                        if($es_venta_movil)
                            $razon_devolucion = 'DEVOLUCION EN VENTA MOVIL';
                        else
                            $razon_devolucion = 'DEVOLUCION EN VENTA ONLINE';

                    $detalle->update([
                        'devuelto' => 1,
                        'descuento_devolucion' => $descuento_devolucion,
                        'porcentaje_descuento_devolucion' => $porcentaje_descuento_devolucion,
                        'fecha_devolucion' => now(),
                        'razon_devolucion' => $razon_devolucion
                    ]);

                    // Obtenemos el último stock del libro en el colegio
                    $libro_almacen = $this->pedido_modal->colegio->libros()->wherePivot('paquete_id', null)->where('libros.id', $detalle->libro_id)->first();

                    // Registramos la devolución con un movimiento del almacén
                    $this->pedido_modal->colegio->movimientosAlmacen()->create([
                        'libro_id'                   => $detalle->libro_id,
                        'tipo_movimiento_almacen_id' => 4, // Entrada por devolución
                        'user_id'                    => Auth::user()->id,
                        'cantidad'                   => $detalle->cantidad,
                        'cantidad_anterior'          => $libro_almacen->pivot->stock,
                        'descripcion'                => "Devolución en el pedido {$this->pedido_modal->serie}{$this->pedido_modal->folio} ({$this->pedido_modal->id}) por el libro {$detalle->libro->nombre} ({$detalle->libro_id}), {$razon_devolucion}",
                        'comentarios'                => $this->comentariosDevolucion == '' ? 'Sin comentarios' : $this->comentariosDevolucion
                    ]);

                    // Aumentamos el stock del almacén por la devolución
                    $this->pedido_modal->colegio->libros()->wherePivot('paquete_id', null)->updateExistingPivot($detalle->libro_id, [
                        'stock' => $libro_almacen->pivot->stock + $detalle->cantidad
                    ]);

                    activity()
                        ->performedOn($detalle)
                        ->causedBy(Auth::user()->id)
                        ->log("Registró devolución del pedido {$this->pedido_modal->serie}-{$this->pedido_modal->folio}, se devolvieron {$detalle->cantidad} unidades del libro {$detalle->libro->isbn} ({$detalle->libro->nombre}) por {$razon_devolucion}");

                });

            DB::commit();

            $this->emit('swalAlert', [
                'title' => 'Devolución registrada',
                'text' => "Se registró la devolución en el pedido {$this->pedido_modal->serie}-{$this->pedido_modal->folio}",
                'icon' => 'success'
            ]);

            activity()
                ->performedOn($this->pedido_modal)
                ->causedBy(Auth::user()->id)
                ->withProperties(['detalles' => $detalles])
                ->log("Se termino de registrar la devolución del pedido {$this->pedido_modal->serie}-{$this->pedido_modal->folio}, con el mensaje: {$this->comentariosDevolucion}");

            $this->paqueteria = null;
            $this->no_guia = '';
            $this->modal_devoluciones = false;
            $this->pedido_modal = null;
            $this->comentariosDevolucion = '';

            $this->emit('refreshDatatable');
        } catch (\Throwable $th) {
            DB::rollback();

            Log::error('DatatableDevoluciones', [$th]);

            $this->emit('swalAlert', [
                'title' => 'Ocurrió un error',
                'text' => "No se pudo registrar la devolucion, error: {$th->getMessage()}",
                'icon' => 'error'
            ]);
        }
    }
}
