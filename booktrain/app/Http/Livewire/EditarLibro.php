<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Libro;
use Illuminate\Support\Facades\Auth;

class EditarLibro extends Component
{
    public Libro $libro;
    public $modo_edicion = TRUE;

    protected $listeners = ['editLibro'];

    protected $rules = [
        'libro.isbn' => 'required|max:17|min:13',
        'libro.nombre' => 'required',
        'libro.activo' => 'required',
    ];

    public function mount(Libro $libro)
    {
        $this->libro = $libro;
    }

    public function render()
    {
        return view('livewire.editar-libro');
    }

    public function editLibro(Libro $libro)
    {
        $this->libro = $libro;
        $this->modo_edicion = true;
    }

    public function save()
    {
        $this->libro->editorial = "";
        $this->libro->edicion = "";

        if (!$this->libro->exists)
            $this->validate([
                'libro.isbn' => 'unique:libros,isbn',
            ]);

        $this->validate();

        $libro = new libro;
        activity()
            ->performedOn($libro)
            ->causedBy(Auth::user()->id)
            ->log('Editó un libro: ' . $this->libro->isbn);
        $this->libro->save();

        $this->emit('swalAlert', [
            'title' => 'Registro exitoso',
            'text' => "Los cambios se registraron correctamente en el libro {$this->libro->nombre}",
            'icon' => 'success'
        ]);

        $this->libro = new Libro;
        $this->libro->activo = true;
        $this->modo_edicion = false;
    }
}
