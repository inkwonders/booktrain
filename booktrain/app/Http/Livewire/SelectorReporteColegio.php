<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Livewire\Component;
use App\Models\SeccionColegio;

class SelectorReporteColegio extends Component
{
    public $colegios = [];
    public $colegio_id;
    public $grado_id;
    public $libro_id;
    public $cargando_reporte = false;
    public $fecha_inicial = '';
    public $fecha_final = '';

    protected $listeners = ['setFecha'];

    public function setFecha($wich, $what) {
        if($wich == 'inicial')
            $this->fecha_inicial = $what;

            if($wich == 'final')
            $this->fecha_final = $what;
    }

    public function getGradosProperty() {
        if( $this->colegio_id == 0 || $this->colegio_id == null )
            return [];

        return Colegio::find($this->colegio_id)->secciones->pluck('niveles')->collapse();
    }

    public function getLibrosProperty() {
        if( $this->colegio_id == 0 || $this->colegio_id == null )
            return [];

        return Colegio::find($this->colegio_id)->libros()->wherePivot('paquete_id', null)->get();
    }

    public function getValidaFormularioProperty() {
        return $this->fecha_inicial != '' && $this->fecha_final != '' && $this->colegio_id != 0;
    }

    public function getNombreColegioProperty() {
        if($this->colegio_id == '')
            return 'TODOS LOS COLEGIOS';

        return Colegio::find($this->colegio_id)->nombre;
    }

    public function getNombreGradoProperty() {
        if($this->grado_id == '')
            return 'TODOS LOS GRADOS';

        return SeccionColegio::find($this->grado_id)->nombre;
    }

    public function getNombreIsbnProperty() {
        if($this->libro_id == '')
            return 'TODOS LOS ISBN';

        return $this->libro_id;
    }

    public function mount() {
        $this->colegios = Colegio::get();
        $this->fecha_inicial = date('Y-m-1');
        $this->fecha_final = date('Y-m-d');
    }

    public function descargarExcel() {
        $codigo_colegio = Colegio::find($this->colegio_id)->codigo;

        return app('App\Http\Controllers\ReporteController')
            ->excelColegio(
                $codigo_colegio,
                $this->fecha_inicial,
                $this->fecha_final,
                $this->grado_id ? $this->grado_id : 0,
                $this->libro_id ? $this->libro_id : 0
            );
    }

    public function mostrarReporte() {
        $this->emit('swalShowLoading');
        $this->cargando_reporte = true;

        $codigo_colegio = Colegio::find($this->colegio_id)->codigo;

        return redirect()
            ->route('admin.reportes.colegio.table', [
                $codigo_colegio,
                $this->fecha_inicial,
                $this->fecha_final,
                $this->grado_id ? $this->grado_id : 0,
                $this->libro_id ? $this->libro_id : 0
            ]);
    }

    public function render()
    {
        return view('livewire.selector-reporte-colegio');
    }
}
