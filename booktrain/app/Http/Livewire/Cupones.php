<?php

namespace App\Http\Livewire;

use App\Models\Cupon;
use Carbon\Carbon;
use Livewire\Component;

class Cupones extends Component
{
    public $modal_nuevo_cupon = false;
    public $valor;
    public $caducidad;

    protected $listeners = ['crearCupon'];

    public function crearCupon() {
        $this->validate([
            'valor' => 'required|numeric|min:1|max:100'
        ]);

        $caducidad = null;

        if($this->caducidad != '') {
            $caducidad = Carbon::createFromTimestamp(strtotime($this->caducidad));
        }

        $nuevo_cupon = Cupon::create([
            'valor' => $this->valor,
            'caducidad' => $caducidad,
            'updated_at' => null
        ]);

        $this->emit('swalAlert', [
            'title' => 'Cupón creado exitosamente',
            'text' => "El código de cupón es {$nuevo_cupon->codigo}",
            'icon' => 'success'
        ]);

        $this->valor = 0;
        $this->caducidad = '';
        $this->modal_nuevo_cupon = false;
        $this->emit('refreshDatatable');
    }

    public function render()
    {
        return view('livewire.cupones');
    }
}
