<?php

namespace App\Http\Livewire;

use App\Models\Caja;
use App\Models\Colegio;
use App\Models\Pedido;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class PedidosVentaMovil extends Component
{
    public $colegio;
    public $usuarios_cajas = [];
    public $pedidos;
    public $filtrar_caja = 0;

    public function mount()
    {
        $this->colegio =  Colegio::where('codigo', Auth::user()->getDirectPermissions()->first()->name)->first();
        $cajas_abiertas = Caja::where('colegio_id', $this->colegio->id)->where('status', 'ABIERTA')->get();
        foreach ($cajas_abiertas as $caja) {
            $usuario = User::find($caja->user_id);
            array_push($this->usuarios_cajas, $usuario);
        }
        $this->pedidos = Pedido::where('colegio_id', $this->colegio->id)
            ->where('status', '!=', 'CARRITO')
            ->whereNotNull('usuario_venta_id')
            ->orderBy('id', 'DESC')->get();
    }

    public function updatedFiltrarCaja()
    {
        if ($this->filtrar_caja == 0 || $this->filtrar_caja == "0") {
            $this->pedidos = Pedido::where('colegio_id', $this->colegio->id)
                ->where('status', '!=', 'CARRITO')
                ->whereNotNull('usuario_venta_id')
                ->orderBy('id', 'DESC')->get();
        } else {
            $today = date("Y-m-d");
            $caja = Caja::where('colegio_id', $this->colegio->id)
                ->where('status', 'ABIERTA')
                ->where('user_id', $this->filtrar_caja)
                ->first();
            $this->pedidos = Pedido::where('usuario_venta_id',  $this->filtrar_caja)
                ->where('colegio_id', $this->colegio->id)
                ->where('status', '!=', 'CARRITO')
                ->whereBetween('created_at', [$caja->fecha_apertura, $today . ' 23:59:59'])
                ->orderBy('id', 'DESC')->get();
        }
    }

    public function render()
    {
        return view('livewire.pedidos-venta-movil');
    }
}
