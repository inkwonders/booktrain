<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditarLibroPaquete extends Component
{
    public $paquete;
    public $libro;

    protected $listeners = ['editarLibro'];

    protected $rules = [
        'libro.pivot.precio' => '',
        'libro.pivot.obligatorio' => ''
    ];

    public function save() {

        $this->paquete->libros()->wherePivot('paquete_id', $this->paquete->id)->wherePivot('colegio_id', $this->paquete->nivel->seccion->colegio->id)->updateExistingPivot($this->libro->id, [
            'precio' => (int) $this->libro->pivot['precio'],
            'obligatorio' => (int) $this->libro->pivot['obligatorio']
        ]);

        $this->emit('refreshListaPaquetes');
        $this->libro = null;
    }

    public function editarLibro($libro_id) {
        $this->libro = $this->paquete->nivel->seccion->colegio->libros()->wherePivot('paquete_id', $this->paquete->id)->wherePivot('libro_id', $libro_id)->first();
    }

    public function render()
    {
        return view('livewire.editar-libro-paquete');
    }
}
