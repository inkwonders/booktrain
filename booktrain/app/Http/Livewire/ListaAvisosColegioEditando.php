<?php
namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Notificacion;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ListaAvisosColegioEditando extends DataTableComponent
{
    public $model = Notificacion::class;
    public $searchable = "Buscar";
    public $avisos;
    public Colegio $colegio;
    public $notificacion_elegida;

    public function toggleActivo(Notificacion $notificacion)
    {
        $notificacion->update([
            'activo' => !$notificacion->activo
        ]);
        $status = ($notificacion->activo) ? 'activa' : 'inactiva';
        $aviso = new Notificacion;
        activity()
            ->performedOn($aviso)
            ->causedBy(Auth::user()->id)
            ->log('Cambió el status de la notificacion "' . $notificacion->titulo . '" del colegio ' . $this->colegio->codigo . ' a ' . $status);
    }

    function columns(): array
    {
        return [
            Column::make('titulo', 'titulo')
                ->sortable()
                ->searchable()
                ->addClass('font-rob-bold'),
            Column::make('aviso', 'aviso')
                ->sortable()
                ->searchable()
                ->addClass('font-rob-bold w-72 whitespace-normal'),
            Column::make('activo', 'activo')
                ->sortable()
                ->addClass('font-rob-bold'),
            Column::make('editar')
                ->addClass('hidden'),
            Column::make('eliminar')
                ->addClass('hidden')
        ];
    }
    public function query(): Builder
    {
        return Notificacion::query()->where('colegio_id', $this->colegio->id);
    }

    public function rowView(): string
    {
        return ('livewire.lista-avisos-colegio-editando');
    }

    public function beforeDelete($notificacion_elegida)
    {
        $this->notificacion_elegida = $notificacion_elegida;
        session()->flash('message', '¿Está seguro de eliminar este aviso?');
    }
    public function delete(Notificacion $notificacion)
    {
        $notificacion->delete();
        $this->emit('swalAlert', ['title' => 'Aviso eliminado', 'text' => 'El aviso ha sido eliminado exitosamente', 'icon' => 'success']);
        $aviso = new Notificacion;
        activity()
            ->performedOn($aviso)
            ->causedBy(Auth::user()->id)
            ->log('Eliminó el aviso "' . $notificacion->titulo . '" del colegio ' . $this->colegio->codigo);
    }
}
