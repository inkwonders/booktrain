<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Illuminate\Support\Facades\DB;


class DtListaUsuarios extends DataTableComponent
{
    public $model = User::class;
    public $searchable = "Buscar";

    public function roles($id)
    {
        return User::where('id', $id)->with('roles')->first();
    }
    public function verificar(User $usuario)
    {
        $usuario->verificarCuenta();
    }

    public function activo(User $usuario)
    {
        $usuario->activo = !$usuario->activo;

        if ($usuario->activo == 1) {
            $status = "activo";
        } else {
            $status = "inactivo";
        }

        $user = new User;
        activity()
            ->performedOn($user)
            ->causedBy(Auth::user()->id)
            ->log('Cambió el estatus de ' . $usuario->email . ' a ' . $status);
        $usuario->save();
    }

    function columns(): array
    {
        return [
            Column::make('nombre', 'name')
                ->sortable()
                ->searchable()
                ->addClass('font-rob-bold'),
            Column::make('correo', 'email')
                ->sortable()
                ->searchable()
                ->addClass('font-rob-bold'),
            Column::make('activo', 'activo')
                ->sortable()
                ->addClass('font-rob-bold'),
            Column::make('permisos', 'roles_name')
                ->sortable()
                ->addClass('font-rob-bold'),
            Column::make(' ', 'email_verified_at')
                ->sortable(),
            Column::make(' '),

        ];
    }
    public function query(): Builder
    {



        $query = User::query()
        ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
        ->select(
            DB::raw('users.id as id'),
            DB::raw('roles.name as roles_name'),
            DB::raw('users.name as name'),
            DB::raw('users.apellidos as apellidos'),
            DB::raw('users.email as email'),
            DB::raw('users.activo as activo'),
            DB::raw('users.email_verified_at as email_verified_at'),
        )
        ->where('model_type', '=', 'App\Models\User')
        ->where('activo', '<>', null);

        // $query = User::where([
        //     ['activo', '<>', null],
        // ])->has('roles');
       return $query;
    }

    public function rowView(): string
    {
        return ('livewire.dt-lista-usuarios');
    }
}





//
