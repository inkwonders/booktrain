<?php

namespace App\Http\Livewire;

use App\Models\Libro;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EditarLibroColegio extends Component
{
    public $colegio;
    public $paquete_id;
    public $libro_id;

    public $libro;

    public function render() {
        return view('livewire.editar-libro-colegio');
    }

    public function mount() {
        $libro_colegio = $this->colegio
            ->libros()
            ->wherePivot('paquete_id', null)
            ->where('libros.id', $this->libro_id)
            ->first();

        $libro_paquete = $this->colegio
            ->libros()
            ->wherePivot('paquete_id', $this->paquete_id)
            ->where('libros.id', $this->libro_id)
            ->first();

        $this->libro = [
            'id'          => $libro_colegio->id,
            'nombre'      => $libro_colegio->nombre,
            'isbn'        => $libro_colegio->isbn,
            'activo'      => $libro_colegio->pivot->activo,
            'obligatorio' => $libro_paquete->pivot->obligatorio,
            'precio'      => $libro_paquete->pivot->precio,
            'bajo_pedido' => $libro_colegio->pivot->bajo_pedido,
        ];
    }

    public function save() {
        $this->validate([
            'libro.activo'      => 'sometimes|required',
            'libro.bajo_pedido' => 'sometimes|required',
            'libro.precio'      => 'required'
        ]);

        $this->colegio
            ->libros()
            ->wherePivot('paquete_id', null)
            ->updateExistingPivot($this->libro_id, [
                'bajo_pedido' => $this->libro['bajo_pedido'],
                'activo'      => $this->libro['activo'],
            ]);

        $this->colegio
            ->libros()
            ->wherePivot('paquete_id', $this->paquete_id)
            ->updateExistingPivot($this->libro_id, [
                'obligatorio' => $this->libro['obligatorio'],
                'precio'      => $this->libro['precio'],
            ]);

        $libro = Libro::find($this->libro_id);

        activity()
            ->performedOn($libro)
            ->causedBy(Auth::user()->id)
            ->withProperties(['libro' => $this->libro])
            ->log("Se editó el libro {$libro->nombre} ($libro->id) del colegio {$this->colegio->nombre} ({$this->colegio->id}) en el paquete {$this->paquete_id}");

        $this->emit('swalAlert', [
            'title' => 'Cambios guardados',
            'text'  => "Se editó correctamente el libro {$libro->nombre}",
            'icon'  => 'success'
        ]);
    }
}
