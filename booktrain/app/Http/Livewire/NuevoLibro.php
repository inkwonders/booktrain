<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Libro;
use Illuminate\Support\Facades\Auth;

class NuevoLibro extends Component
{
    public Libro $libro;

    protected $rules = [
        'libro.isbn' => 'required|max:17|min:13|unique:libros,isbn',
        'libro.nombre' => 'required',
        'libro.activo' => 'sometimes|required',
    ];

    public function mount()
    {
        $this->libro = new Libro;
        $this->libro->activo = true;
    }

    public function render()
    {
        return view('livewire.nuevo-libro');
    }

    public function save()
    {
        $this->validate();

        $this->libro->save();
        $libro = new Libro;
        activity()
            ->performedOn($libro)
            ->causedBy(Auth::user()->id)
            ->log('Creó un nuevo libro: ' . $this->libro->isbn);

        $this->emit('swalAlert', [
            'title' => 'Registro exitoso',
            'text' => "El libro {$this->libro->nombre} ha sido registrado correctamente con el ISBN: {$this->libro->isbn}",
            'icon' => 'success'
        ]);

        $this->libro = new Libro;
        $this->libro->activo = true;
    }
}
