<?php

namespace App\Http\Livewire;

use App\Models\RegistroActividad;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class DatatableLog extends DataTableComponent
{
    public $inicio;
    public $fin;
    public $usuario;

    public function columns(): array
    {
        return [
            Column::make('Fecha', 'created_at')
                ->sortable()
                ->searchable(),
            Column::make('Nombre', 'name')
                ->sortable()
                ->searchable(function(Builder $query, $searchTerm) {
                    $query->orWhere('users.name', 'like', "%$searchTerm%");
                }),
            Column::make('Apellidos', 'apellidos')
                ->sortable()
                ->searchable(function(Builder $query, $searchTerm) {
                    $query->orWhere('users.apellidos', 'like', "%$searchTerm%");
                }),
            Column::make('Email', 'email')
                ->sortable()
                ->searchable(function(Builder $query, $searchTerm) {
                    $query->orWhere('users.email', 'like', "%$searchTerm%");
                }),
            Column::make('Descripcion', 'description')
                ->sortable()
                ->searchable(),
        ];
    }

    public function query(): Builder
    {
        return RegistroActividad::query()
            ->select(
                'users.email',
                'users.name',
                'users.apellidos',
                'activity_log.description',
                'activity_log.created_at'
            )
            ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id')
            ->when($this->inicio, function($query, $inicio) {
                $query->where('activity_log.created_at', '>', "{$inicio} 00:00:00");
            })
            ->when($this->fin, function($query, $fin) {
                $query->where('activity_log.created_at', '<', "{$fin} 23:59:59");
            })
            ->when($this->usuario, function($query, $usuario) {
                $query->where('activity_log.causer_id', $usuario);
            })
            ->orderBy('activity_log.created_at', 'DESC');
    }
}
