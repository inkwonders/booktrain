<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Colegio;
use App\Models\Pedido;
use App\Models\SeccionColegio;

/**
 * Reporte de Libros de pedidos
 * Se puede filtrar por grados y colegios
 * Cada renglón representa un libro mostrando por pedido, colegio y grado su información
 * Condición 1: La comisión del pago solo debe aparecer en el primer articulo
 * Condición 2: El costo de envío solo debe aparecer en el primer articulo
 * Condición 3: Si el detalle del resumen del pedido tiene un envío, se dará prioridad a su información sobre la información de envío del pedido
 */
class ReporteGeneral extends Component
{
    public $cargando_reporte = false;
    public $colegios;
    public $colegio_id;
    public $grado_id;
    public $libro_id;
    public $estado_pedido;
    public $fecha_inicial;
    public $fecha_final;
    public $pedidos = [];

    protected $listeners = ['setFecha'];

    public function setFecha($wich, $what) {
        if($wich == 'inicial')
            $this->fecha_inicial = $what;

            if($wich == 'final')
            $this->fecha_final = $what;
    }

    public function getGradosProperty() {
        if( $this->colegio_id == 0 || $this->colegio_id == null )
            return [];

        return Colegio::find($this->colegio_id)->secciones->pluck('niveles')->collapse();
    }

    public function getLibrosProperty() {
        if( $this->colegio_id == 0 || $this->colegio_id == null )
            return [];

        return Colegio::find($this->colegio_id)->libros()->wherePivot('paquete_id', null)->get();
    }

    public function getValidaFormularioProperty() {
        return $this->fecha_inicial != '' && $this->fecha_final != '';
    }

    public function getNombreColegioProperty() {
        if($this->colegio_id == '')
            return 'TODOS LOS COLEGIOS';

        return Colegio::find($this->colegio_id)->nombre;
    }

    public function getNombreGradoProperty() {
        if($this->grado_id == '')
            return 'TODOS LOS GRADOS';

        return SeccionColegio::find($this->grado_id)->nombre;
    }

    public function getNombreIsbnProperty() {
        if($this->libro_id == '')
            return 'TODOS LOS ISBN';

        return $this->libro_id;
    }

    public function mount() {
        $this->colegios = Colegio::get();
        $this->fecha_inicial = date('Y-m-1');
        $this->fecha_final = date('Y-m-d');
    }

    public function descargarExcel() {
        return app('App\Http\Controllers\ReporteController')->excelGeneral($this->fecha_inicial, $this->fecha_final, $this->colegio_id ? $this->colegio_id : 0, $this->grado_id ? $this->grado_id : 0, $this->libro_id ? $this->libro_id : 0, $this->estado_pedido ? $this->estado_pedido : 0);
    }

    public function mostrarReporte() {
        $this->emit('swalShowLoading');
        $this->cargando_reporte = true;
        return redirect()->route('admin.reportes.general.table', [$this->fecha_inicial, $this->fecha_final, $this->colegio_id ? $this->colegio_id : 0, $this->grado_id ? $this->grado_id : 0, $this->libro_id ? $this->libro_id : 0, $this->estado_pedido ? $this->estado_pedido : 0]);
    }

    public function render()
    {
        return view('livewire.reporte-general');
    }
}
