<?php

namespace App\Http\Livewire;

use App\Models\Libro;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ListaLibros extends DataTableComponent
{
    public $model = Libros::class;

    protected $listeners = ['refreshList'];

    public $libros;
    public $filtro;
    public $libro_delete;
    public $ocultar_boton = false;

    public function mount()
    {
        $this->refreshList();
    }

    public function refreshList()
    {
        $this->libros = Libro::where('nombre', 'like', '%' . $this->filtro . '%')->get();
    }

    public function delete()
    {
        if ($this->libro_delete->paquetes()->get()->count() == 0 && $this->libro_delete->colegios()->get()->count() == 0) {
            $this->libro_delete->delete();

            activity()
                ->performedOn($this->libro_delete)
                ->causedBy(Auth::user()->id)
                ->log('Eliminó un libro: ' . $this->libro_delete->isbn);

            $this->emit('swalAlert', ['title' => 'Libro eliminado', 'text' => 'El libro ha sido eliminado exitosamente', 'icon' => 'success']);
        } else {
            $this->ocultar_boton = true;
            $this->emit('swalAlert', ['title' => 'Error al eliminar', 'text' => 'El libro no pude ser eliminado debido a que está asociado a un colegio y/o paquete.', 'icon' => 'error']);
        }
    }

    public function before_delete(Libro $libro)
    {
        $this->libro_delete = $libro;
        session()->flash('message_' . $libro->id, '¿Está seguro que desea eliminar este libro?');
    }

    public function editar(Libro $libro)
    {
        $this->libro = $libro;
        $this->emit('editLibro', $libro->id);
        return redirect()->to('admin/editar_libro');
    }

    public function cancelar()
    {
        return redirect()->to('admin/libros');
    }

    function columns(): array
    {
        return [
            Column::make('ISBN', 'isbn')
                ->sortable()
                ->searchable(),
            Column::make('Nombre', 'nombre')
                ->sortable()
                ->searchable(),
                Column::make('Activo', 'activo')
                ->sortable()
                ->searchable(),
            Column::make('Colegios'),
            Column::blank(),
            Column::blank()

        ];
    }

    public function query(): Builder
    {
        return Libro::query();
    }

    public function rowView(): string
    {
        return ('livewire.lista-libros');
    }
}
