<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use App\Models\RegistroPago;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Builder;
use App\Exports\DetalleResumenPedidosReferencias;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class Pedidos extends DataTableComponent
{
    protected $listeners = [
        'buscar',
        'modificar_status',
        'setFecha',
        'activar_cancelar_status',
        'enviar_correo',
        'modalDetallesPedido',
        'modificar_referencia_factura',
        'onSwalResult'
    ];

    public $model = Pedido::class;

    public $table_headers = 'private.admin.header_pedidos';

    public $searchable = "Buscar";
    public $status;
    public $facturacion;
    public $metodo_pago;
    public $forma_pago;
    public $tipo_entrega;
    public $inicio;
    public $fin;
    public $modal_reorden = false;

    public $modal_factura = false, $modal_factura_datos;

    public function mount() {
        $this->inicio = date('Y-m-d');
        $this->fin = date('Y-m-d');
    }

    public function getMetodosPagoProperty() {
        return MetodoPago::all();
    }

    public function getFormasPagoProperty() {
        return FormaPago::all();
    }

    public function setFecha($which, $what) {
        switch ($which) {
            case 'inicio': $this->inicio = $what; break;
            case 'fin': $this->fin = $what; break;
        }
    }

    public function onSwalResult($result, $extras) {
        // dd($result, $extras);

        if(isset($extras) && $result['isConfirmed'] == true && isset($extras['pedido_id']))
            try {
                DB::beginTransaction();

                $pedido = Pedido::find($extras['pedido_id']);

                switch ($extras['action']) {
                    case 'enviarCorreoConfirmacionPago':
                        $pedido->sendPagoExitosoNotification();

                        $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El correo se ha enviado satisfactoriamente', 'icon' => 'success']);

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Reenvió el correo de confirmación del pedido: '{$pedido->serie}-{$pedido->folio}'");
                    break;

                    case 'asignarReferenciaFactura':
                        $pedido->update(['referencia_factura' => $result['value']]);

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Se asigno la factura '{$pedido->referencia_factura}' al pedido {$pedido->serie}-{$pedido->folio}");

                            $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'La referencia se actualizó correctamente', 'icon' => 'success']);
                    break;

                    case 'cambiarEstadoPedido':
                        $estado_anterior = $pedido->status;

                        switch($pedido->status) {
                            // Pasa a CANCELADO o PAGADO
                            case 'PROCESADO':
                                switch ($result['value']) {
                                    case 'CANCELADO':
                                        $pedido->cancelar();

                                        // Si hay una referencia de pago se maraca como NO PAGADA
                                        $pedido->referencia()->update(['pagado' => 0]);
                                    break;

                                    case 'PAGADO':
                                        // Marcamos el pedido como pagado
                                        $pedido->update(['status' => 'PAGADO']);

                                        // Si hay una referencia, se marca esta última como pagada
                                        $pedido->referencia()->update(['pagado' => 1]);

                                        // Si no hay pagos exitosos, se crea un registro de pago exitoso MANUAL
                                        if( ! $pedido->pagos()->exitoso()->first())
                                            $pedido->pagos()->create([
                                                'amount'             => $pedido->total,
                                                'source'             => 'Registro de pago de forma manual',
                                                'status'             => 'SUCCESS',
                                                'transactionTokenId' => auth()->user()->id,
                                                'raw'                => auth()->user()->email,
                                                'origen'             => 'MANUAL',
                                            ]);
                                    break;
                                }
                            break;

                            // Pasa a CANCELADO
                            case 'PAGADO':
                                $pedido->cancelar();

                                // Si hay una referencia de pago se maraca como NO PAGADA
                                $pedido->referencia()->update(['pagado' => 0]);
                            break;

                            // Pasa a Procesado
                            case 'CANCELADO':
                                $pedido->update([
                                    'status' => 'PROCESADO'
                                ]);

                                $pedido->procesar();
                            break;
                        }

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Se cambio el estado del pedido {$pedido->serie}-{$pedido->folio} de {$estado_anterior} a {$pedido->status}");

                        $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El estado del pedido se actualizó correctamente', 'icon' => 'success']);
                    break;
                }

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                Log::error('Ocurrió un error al procesar el callback en el componente de livewire Pedidos@onSwalResult', ['result' => $result, 'extras' => $extras, 'linea' => $th->getLine(), 'message' => $th->getMessage()]);

                $this->emit('swalAlert', [
                    'title' => 'Error en la operación',
                    'text' => "Ocurrió un error al realizar la operación: {$th->getMessage()}, reintente más tarde.",
                    'icon' => 'error'
                ]);
            }
    }

    public function exportar() {
        return Excel::download(new DetalleResumenPedidosReferencias(
            $this->status,
            $this->facturacion,
            $this->metodo_pago,
            $this->forma_pago,
            $this->tipo_entrega,
            $this->inicio,
            $this->fin
        ), "Reporte de pedidos de {$this->inicio} a {$this->fin}.xlsx");
    }

    public function modalDetallesPedido(Pedido $pedido) {
        $this->pedido_modal = $pedido;
        $this->modal_reorden = true;
    }

    public function cambiarEstadoPedido(Pedido $pedido) {
        switch ($pedido->status) {
            // Pasa a CANCELADO o PAGADO
            case 'PROCESADO':
                $this->emit('swalAlert', [
                    'title'              => 'Cambiar estado',
                    'text'               => "Seleccione el nuevo estado del pedido {$pedido->serie}{$pedido->folio} (PROCESADO)",
                    'icon'               => 'question',
                    'showCancelButton'   => true,
                    'confirmButtonText'  => 'Cambiar',
                    'cancelButtonText'   => 'Cancelar',
                    'inputPlaceholder'   => 'Seleccione uno',
                    'input'              => 'select',
                    'inputOptions' => [
                        'CANCELADO' => 'Cambiar a CANCELADO',
                        'PAGADO'    => 'Cambiar a PAGADO'
                    ]
                ], [
                    'action' => 'cambiarEstadoPedido',
                    'pedido_id' => $pedido->id
                ]);
            break;

            // Pasa a CANCELADO
            case 'PAGADO':
                $this->emit('swalAlert', [
                    'title'              => 'Cancelar pedido',
                    'text'               => "¿Desea cambiar el estado del pedido {$pedido->serie}{$pedido->folio} de 'PAGADO' a 'CANCELADO'?",
                    'icon'               => 'question',
                    'showCancelButton'   => true,
                    'confirmButtonText'  => 'Cancelar pedido',
                    'confirmButtonColor' => '#ff2222',
                    'cancelButtonText'   => 'Regresar'
                ], [
                    'action'    => 'cambiarEstadoPedido',
                    'pedido_id' => $pedido->id
                ]);
            break;

            // Pasa a Procesado
            case 'CANCELADO':
                $this->emit('swalAlert', [
                    'title'             => 'Activar pedido',
                    'text'              => "¿Desea cambiar el estado del pedido {$pedido->serie}{$pedido->folio} de 'CANCELADO' a 'PROCESADO'?",
                    'icon'              => 'question',
                    'showCancelButton'  => true,
                    'confirmButtonText' => 'Activar',
                    'cancelButtonText'  => 'Cancelar'
                ], [
                    'action'    => 'cambiarEstadoPedido',
                    'pedido_id' => $pedido->id
                ]);
            break;
        }
    }

    public function enviarCorreoConfirmacionPago(Pedido $pedido) {
        $this->emit('swalAlert', [
            'title'             => 'Confirmación de pago',
            'text'              => "¿Desea enviar el correo de confirmación de pago del pedido {$pedido->serie}{$pedido->folio} al correo {$pedido->user->email}?",
            'icon'              => 'question',
            'showCancelButton'  => true,
            'confirmButtonText' => 'Enviar',
            'cancelButtonText'  => 'Cancelar'
        ], [
            'action' => 'enviarCorreoConfirmacionPago',
            'pedido_id' => $pedido->id
        ]);
    }

    public function asignarReferenciaFactura(Pedido $pedido) {
        $this->emit('swalAlert', [
            'title'             => 'Referencia de factura',
            'text'              => "Introduzca la referencia de facturación del pedido {$pedido->serie}{$pedido->folio} del día {$pedido->created_at->format('d-m-Y')} con el RFC {$pedido->datosFactura->rfc}",
            'icon'              => 'question',
            'showCancelButton'  => true,
            'confirmButtonText' => 'Asignar',
            'cancelButtonText'  => 'Cancelar',
            'input'             => 'text',
            'inputPlaceholder'  => 'Referencia de facturación',
            'inputValue'        => $pedido->referencia_factura
        ], [
            'action' => 'asignarReferenciaFactura',
            'pedido_id' => $pedido->id
        ]);
    }

    function columns(): array
    {
        return [
            Column::make('Estado'           , 'status'),
            Column::make('Folio'            , 'folio')->searchable(),
            Column::make('Fecha'            , 'created_at')->searchable(),
            Column::make('Nombre'           , 'nombre_contacto')->searchable(),
            Column::make('Correo'           , 'email_pedido')->searchable(),
            Column::make('Teléfono'         , 'celular_contacto')->searchable(),
            Column::make('Solicito Factura' , 'factura')->searchable(),
            Column::make('Timbrado' , 'facturama_invoice_id'),
            // Column::make('Factura'          , 'referencia_factura')->searchable(),
            // Column::make('Referencia'       , 'referencia'),
            Column::make('Método de pago'   , 'metodo_pago')->searchable(),
            Column::make('Forma de pago'    , 'forma_pago')->searchable(),
            Column::make('Monto cobrado'    , 'total')->searchable(),
            Column::make('Monto recibido'),
            Column::make('Colegio'          , 'colegio_id')->searchable(),
            Column::make('Fecha pago'),
            Column::make('Entrega'),
            Column::make('Cambio de Estatus'),
            Column::make('Correo de confirmación'),
            Column::make('Pedido')
        ];
    }

    public function query(): Builder
    {
        $query = Pedido::query()
            ->with('pagos')
            ->where('status', '!=','CARRITO')
            ->when($this->inicio, function($query, $inicio) {
                $query->where('created_at', '>=', "{$inicio} 00:00:00");
            })
            ->when($this->fin, function($query, $fin) {
                $query->where('created_at', '<=', "{$fin} 23:59:59");
            })
            ->when($this->status, function($query, $status) {
                $query->where('status', $status);
            })
            ->when($this->metodo_pago, function($query, $metodo_pago) {
                $query->where('metodo_pago', $metodo_pago);
            })
            ->when($this->forma_pago, function($query, $forma_pago) {
                $query->where('forma_pago', $forma_pago);
            })
            ->when($this->tipo_entrega, function($query, $tipo_entrega) {
                $query->where('tipo_entrega', $tipo_entrega);
            })
            ->when($this->facturacion, function($query, $facturacion) {
                $query->where('factura', ($facturacion == 1 ? 1 : 0) );
            })
            ->orderBy('created_at', 'DESC');

        return $query;
    }

    public function rowView(): string {
        return ('livewire.pedidos');
    }

    public function modalsView() : string {
        return 'private.admin.modals_pedidos';
    }

    public function verFactura($id_fact){

        $query = Pedido::find($id_fact);

        $this->modal_factura = true;

        $this->modal_factura_datos = $query;

    }

    public function cerrarFactura(){

        $this->modal_factura = false;

    }

}
