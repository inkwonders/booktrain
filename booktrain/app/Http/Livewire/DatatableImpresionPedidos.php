<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class DatatableImpresionPedidos extends DataTableComponent
{
    public $model = Pedido::class;

    public $filtro_fecha = '';
    public $fecha_impresion = '';
    public $table_headers = 'livewire.header-impresion-pedidos';
    public $pedido_impreso = false;
    protected $rules = [];
    public array $perPageAccepted = [20, 30, 40];
    public int $perPage = 25;

    public function mount()
    {
        $this->filtro_fecha = date('Y-m-d');
        $this->fecha_impresion = date('Y-m-d');
    }

    protected $listeners = [
        'filtroFecha',
        'fechaImpresion'
    ];

    public function fechaImpresion($fecha) {
        $this->fecha_impresion = $fecha;

        //verificar que en la fecha no se haya impreso los pedidos, si ya se imprimio se muestra mensaje y se deshabilita el botón

        $pedidos_del_dia = $this->getImpresionQuery($this->fecha_impresion)->first();

        if (!is_null($pedidos_del_dia) && $pedidos_del_dia->total_pedidos == $pedidos_del_dia->pedidos_impresos) {
            $this->pedido_impreso = true;
            $this->emit('swalAlert', [
                'title' => 'Imprimir',
                'text' => "Los pedidos del {$this->fecha_impresion} ya fueron impresos",
                'icon' => 'warning'
            ]);
        } else {
            $this->pedido_impreso = false;
        }
    }

    public function filtroFecha($fecha)
    {
        $this->filtro_fecha = $fecha;
        $this->page = 0;
    }

    public function imprimir()
    {
        return redirect()->route('admin.pedidos.imprimir', $this->fecha_impresion);
    }

    function columns(): array
    {
        return [
            Column::make('Estado'),
            Column::make('Colegio', 'colegio_id'),
            // Column::make('Total', 'total_pedidos'),
            // Column::make('Impresos', 'pedidos_impresos'),
            // Column::make('No impresos', 'pedidos_no_impresos'),
            Column::make('Fecha', 'fecha')->sortable(),
            Column::blank(),
        ];
    }

    private function getImpresionQuery($date) {
        /**
         * Lista de pedidos agrupados por el dìa en que fueron pagados
         */
        $query = Pedido::query()
            ->select(
                DB::raw('COUNT(*) as total_pedidos'), // Total
                DB::raw('sum(pedidos.impreso) as pedidos_impresos'), // Pedidos impresos
                DB::raw('(COUNT(*) - sum(pedidos.impreso)) as pedidos_no_impresos'),// Pedidos no impresos
                DB::raw('TO_DAYS(pagos.updated_at) as dia'),
                DB::raw("DATE(pagos.updated_at) as fecha"),
                DB::raw("pedidos.colegio_id as colegio_id"),
                DB::raw("(SELECT nombre FROM colegios
                                WHERE id=pedidos.colegio_id) as colegio_nombre")
                // DB::raw("(SELECT nombre from colegios WHERE id=pedidos.colegio_id as colegio_nombre)")
                // DB::raw("colegio.nombre as colegio_nombre")
            )
            // ->Join('colegios as colegio', 'colegio.id', '=', 'pedidos.colegio_id')
            // ->select(
            //     DB::raw("colegio.nombre as colegio_nombre")
            // )
            ->leftJoin('registros_pagos as pagos', 'pagos.pedido_id', '=', 'pedidos.id')

            ->where('pagos.status', 'SUCCESS')
            ->whereHas('pagos', function($query)  {
                $query->where('pagos.status', 'SUCCESS');
            })
            //'CARRITO','PAGO EN REVISION','PAGO RECHAZADO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'
            //->whereIn('status',['PAGADO','ENVIADO','ENTREGADO','CANCELADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'])
            ->whereNotIn('pedidos.status',['CANCELADO'])
            ->having(
                'dia', '=', DB::raw("to_days('{$date}')")
            )
            ->groupBy('colegio_id','dia', 'fecha')->orderBy('pedidos_no_impresos','desc');

        // dd($query->get());

        return $query;
    }

    public function query(): Builder
    {
        return $this->getImpresionQuery($this->filtro_fecha);
    }

    public function rowView(): string
    {
        return 'livewire.datatable-impresion-pedidos';
    }

}

