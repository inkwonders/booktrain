<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EditarFormaPagoEditando extends Component
{
    public Colegio $colegio;
    public $metodo_pago_id;
    public $forma_pago_id;
    public $forma_pago;
    public $nombre;


    protected $rules = [
        'forma_pago.comision' => 'required|numeric',
        'forma_pago.minimo' => 'required|numeric',
        // 'forma_pago.activo' => 'required',
    ];


    public function mount()
    {
        $metodo_pago = MetodoPago::find($this->metodo_pago_id);

        $forma_pago = $this->colegio
            ->formasPago()
            ->wherePivot('metodo_pago_id', $this->metodo_pago_id)
            ->wherePivot('forma_pago_id', $this->forma_pago_id)
            ->first();

        $this->forma_pago = [
            'id' => $forma_pago->id,
            'comision' => $forma_pago->pivot->comision,
            'minimo' => $forma_pago->pivot->minimo,
            'activo' => $forma_pago->pivot->activo,
        ];

        $this->nombre = "{$metodo_pago->descripcion} - {$forma_pago->descripcion}";
    }

    public function save()
    {
        $this->validate();

        $this->colegio->metodosPago()->wherePivot('metodo_pago_id', $this->metodo_pago_id)->first()->formasPago()
            ->wherePivot('colegio_id', $this->colegio->id)
            ->updateExistingPivot($this->forma_pago['id'], [
                'comision' => $this->forma_pago['comision'],
                'minimo' => $this->forma_pago['minimo'],
                'activo' => $this->forma_pago['activo'],
            ]);
/**
 *      $metodo_pago = $this->colegio
 *           ->formasPago()
 *           ->wherePivot('metodo_pago_id', $this->metodo_pago_id)
 *           ->wherePivot('forma_pago_id', $this->forma_pago['id'])
 *           ->first();

 *       $metodo_pago->updateExistingPivot($this->forma_pago['id'], [
 *               'comision' => $this->forma_pago['comision'],
 *               'minimo' => $this->forma_pago['minimo'],
 *               'activo' => $this->forma_pago['activo'],
 *           ]);
*/
        $this->emit('swalAlert', ['title' => 'Éxito', 'text' => 'La forma de pago se actualizó correctamente.', 'icon' => 'success']);

        activity()
            ->performedOn(FormaPago::find($this->forma_pago['id']))
            ->causedBy(Auth::user()->id)
            ->log('Editó la forma de pago "' . $this->nombre . ' del colegio ' . $this->colegio->codigo);
    }


    public function render()
    {
        return view('livewire.editar-forma-pago-editando');
    }
}
