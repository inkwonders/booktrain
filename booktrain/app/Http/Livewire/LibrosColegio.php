<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Libro;
use Livewire\Component;

use Illuminate\Support\Str;

class LibrosColegio extends Component
{
    public $filtro = "";
    public $colegio;
    public $modal_reorden;
    public $punto_reorden = 0;
    public $modal_reorden_libro = null;
    private $colegio_id;

    protected $listeners = ['seleccionarColegio'];

    protected $rules = [
        'punto_reorden' => 'numeric|min:0'
    ];

    function mount() {
        // $this->colegio = Colegio::find($this->select);
        //$this->refreshList();
    }

    public function seleccionarColegio($colegio_id) {
        $this->colegio_id = $colegio_id;
        $this->refreshList();
    }

    public function editarPuntoReorden($libro_id) {
        $this->modal_reorden_libro = $this->colegio->libros()->wherePivot('paquete_id', null)->wherePivot('libro_id', $libro_id)->first();
        $this->punto_reorden = $this->modal_reorden_libro->pivot->punto_reorden;

        $this->modal_reorden = true;
    }

    public function incrementaPuntoReorden() {
        $this->punto_reorden ++;
    }

    public function decrementaPuntoReorden() {
        $this->punto_reorden --;
    }

    public function guardarPuntoReorden() {
        if($this->punto_reorden > -1) {
            $this->colegio
                ->libros()
                ->wherePivot('paquete_id')
                ->updateExistingPivot($this->modal_reorden_libro->id,
                    ['punto_reorden' => $this->punto_reorden]);

            $this->emit('refreshList');
            $this->punto_reorden = 0;
        }

        $this->cerrarModal();
    }

    public function cerrarModal() {
        $this->modal_reorden = null;
    }

    public function refreshList() {
        $this->colegio = Colegio::where('id', $this->colegio_id)->first();
    }

    public function getLibrosProperty() {
        if($this->colegio == null) return [];

        $filtro = $this->filtro;

        return $this->colegio->libros()
            ->wherePivot('paquete_id', null)
            ->get()
            ->filter(function($libro) use ($filtro) {
                if($filtro == '')
                    return true;

                return Str::contains($libro->isbn, $filtro) || Str::contains($libro->nombre, $filtro);
            })
            ->reverse();
    }

    public function render()
    {
        return view('livewire.libros-colegio');
    }
}