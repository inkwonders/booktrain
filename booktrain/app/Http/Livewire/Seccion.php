<?php

namespace App\Http\Livewire;

use App\Models\NivelColegio;
use App\Models\Paquete;
use App\Models\SeccionColegio;
use Livewire\Component;

class Seccion extends Component
{
    public SeccionColegio $seccion;
    public $nivel;

    public $id_seccion;
    public $paquete;

    protected $listeners = ['editNivel', 'refresh', 'editPaquete'];

    public function editPaquete(Paquete $paquete) {
        $this->paquete = $paquete;
    }

    function editNivel($nivel_id) {
        if($this->nivel)
            $this->nivel = null;
        else
            $this->nivel = NivelColegio::find($nivel_id);
    }

    function mount() {
        $this->seccion = SeccionColegio::find($this->id_seccion);
    }

    function refresh() {
        $this->seccion->refresh();
    }

    public function render()
    {
        return view('livewire.seccion');
    }
}
