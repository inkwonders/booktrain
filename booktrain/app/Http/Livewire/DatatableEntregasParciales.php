<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
// use App\Models\DetalleResumenPedido;
use Illuminate\Database\Eloquent\Builder;
// use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class DatatableEntregasParciales extends DataTableComponent
{
    public $model = Pedido::class;

    public $pedidos;
    public $detalle_resumen_pedido;
    public $pedido_id;
    public $select_inicio;
    public $select_fin;

    public function mount(){
        $this->select_inicio = date("Y-m-d");
        $this->select_fin = date("Y-m-d");
    }

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function inicio($select_inicio) {
        $this->select_inicio = $select_inicio;
    }
    public function fin($select_fin) {
        $this->select_fin = $select_fin;
    }

    function columns(): array
    {
        return [

            Column::make('Folio', 'folio')
                ->sortable()
                ->searchable(),
            Column::make('Tipo Entrega')
                ->sortable(),
            Column::make('Fecha','created_at')
                ->sortable()
                ->searchable(),
            Column::make('ENTREGAR / LIQUIDAR'),
            Column::make('Nombre', 'nombre_contacto')
                ->sortable()
                ->searchable(),
            // Column::make('Correo', 'email_pedido')
            //     ->sortable()
            //     ->searchable(),
            // Column::make('Teléfono', 'celular_contacto')
            //     ->sortable(),
            // Column::make('Factura')
            //     ->sortable(),
            // Column::make('Referencia')
            //     ->sortable(),
            // Column::make('Método de pago')
            //     ->sortable(),
            // Column::make('Forma de pago')
            //     ->sortable(),
            // Column::make('Monto cobrado')
            //     ->sortable(),
            // Column::make('Monto recibido')
            //     ->sortable(),
            Column::make('Colegio')
                ->sortable(),
            // Column::make('Fecha pago')
            //     ->sortable(),
            Column::make('Estatus')
                ->sortable(),
            Column::make('Entrega Parcial'),


        ];
    }



    public function query(): Builder
    {

        $pedidos =  Pedido::query()
            ->whereNotIn('status',['CARRITO','CANCELADO', 'PROCESADO','PAGO RECHAZADO','PAGO EN REVISION','ENVIADO']);
            // ->where('created_at','<', $this->select_fin.' 00:00:00')

            if($this->select_inicio != '') $pedidos->where('created_at','>=', $this->select_inicio.' 00:00:00');
            if($this->select_fin != '') $pedidos->where('created_at','<=', $this->select_fin.' 23:59:59');


        return $pedidos->orderByDesc('folio');





    }

    public function rowView(): string
    {
        return ('livewire.datatable-entregas-parciales');

    }
}
