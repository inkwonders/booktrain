<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Notificacion;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NuevoAvisoColegioEditando extends Component
{
    public Notificacion $notificacion;
    public Colegio $colegio;

    protected $listeners = ['editAvisoColegio'];

    protected $rules = [
        'notificacion.titulo' => 'required',
        'notificacion.aviso' => 'required',
        'notificacion.activo' => 'sometimes'
    ];

    public function mount()
    {
        $this->notificacion = new Notificacion;
        $this->notificacion->activo = true;
    }

    public function editAvisoColegio(Notificacion $notificacion)
    {
        $this->notificacion = $notificacion;
    }

    public function save()
    {
        $this->notificacion->activo = boolval($this->notificacion->activo);

        $this->colegio->notificaciones()->save(
            $this->notificacion
        );
        $aviso = new Notificacion;
        activity()
            ->performedOn($aviso)
            ->causedBy(Auth::user()->id)
            ->log('Agregó un aviso "' . $this->notificacion->titulo . '" para el colegio ' . $this->colegio->codigo);

        $this->emit('swalAlert', [
            'title' => 'Registro exitoso',
            'text' => "El aviso '{$this->notificacion->titulo}' ha sido registrado correctamente en el colegio {$this->colegio->nombre}",
            'icon' => 'success'
        ]);

        $this->notificacion = new Notificacion;
        $this->notificacion->activo = true;
    }

    public function render()
    {
        return view('livewire.nuevo-aviso-colegio-editando');
    }
}
