<?php

namespace App\Http\Livewire;

use App\Models\NivelColegio;
use App\Models\SeccionColegio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;


class EditarSeccionColegio extends Component
{

    public $colegio;
    public $seccion_id;
    public $seccion;
    public $editar_nivel = false;
    public $nivel;

    public $activo;
    public $nombre;

    public $nombre_seccion;
    public $activo_seccion;

    public $error;

    public $niveles_seccion_nuevos = [];
    public $nuevo_nivel;

    public function mount()
    {
        $this->seccion = SeccionColegio::find($this->seccion_id);
        $this->nombre_seccion = $this->seccion->nombre;
        $this->activo_seccion = $this->seccion->activo;
    }
    public function render()
    {
        return view('livewire.editar-seccion-colegio');
    }

    public function editar($nivel_id)
    {
        $this->nivel = NivelColegio::find($nivel_id);
        $this->editar_nivel = true;
        $this->nombre = $this->nivel->nombre;
        $this->activo = $this->nivel->activo;
    }

    public function delete(NivelColegio $nivel)
    {
        //validamos si tiene un paquete asociado
        $paquete = $nivel->paquetes()->first();

        if (is_null($paquete)) {
            $nivel->delete();
            $this->dispatchBrowserEvent('eliminar');

            activity()
                ->performedOn($nivel)
                ->causedBy(Auth::user()->id)
                ->log('Eliminó el grado "' . $nivel->nombre . '" de la seccion "' . $this->seccion->nombre . '" del colegio ' . $this->colegio->codigo);
        } else {

            if ($paquete->libros()->count() == 0) {
                $nivel->delete();
                $this->dispatchBrowserEvent('eliminar');

            } else {
                $this->dispatchBrowserEvent('relaciones');
            }
        }
    }
    public function guardarNivel()
    {
        array_push($this->niveles_seccion_nuevos, $this->nuevo_nivel);
        $this->nuevo_nivel = "";
    }

    public function quitarNivel($id)
    {
        array_splice($this->niveles_seccion_nuevos, $id, 1);
    }

    public function save()
    {
        if ($this->nombre != "" || $this->nombre != null) {
            $this->nivel->nombre = $this->nombre;
            $this->nivel->activo = $this->activo;
            $this->nivel->save();
            $this->dispatchBrowserEvent('editar_correcto');

            activity()
                ->performedOn($this->nivel)
                ->causedBy(Auth::user()->id)
                ->log('Editó el grado "' . $this->nombre . '" de la sección "' . $this->seccion->nombre . '" del colegio ' . $this->colegio->codigo);

        } else {
            $this->error = "El campo nombre es requerido";
        }
    }
    public function save_seccion()
    {
        if ($this->nombre_seccion != "" || $this->nombre_seccion != null) {

            $this->seccion->update([
                'nombre' => $this->nombre_seccion,
                'activo' => $this->activo_seccion
            ]);

            // $this->seccion->save();
            if (sizeof($this->niveles_seccion_nuevos) != 0) {
                foreach ($this->niveles_seccion_nuevos as $nivel) {
                    $nuevo_nivel = new NivelColegio;
                    $nuevo_nivel->nombre = $nivel;
                    $nuevo_nivel->activo = 1;
                    $nuevo_nivel->seccion_id = $this->seccion->id;
                    $nuevo_nivel->save();
                    $nivel = new SeccionColegio;
                    activity()
                        ->performedOn($nivel)
                        ->causedBy(Auth::user()->id)
                        ->log('Agregó el grado "' . $nuevo_nivel->nombre . '" de la sección "' . $this->nombre_seccion . '" del colegio ' . $this->colegio->codigo);
                }
            }

            activity()
                ->performedOn($this->seccion)
                ->causedBy(Auth::user()->id)
                ->log('Editó la sección "' . $this->nombre_seccion . '" del colegio ' . $this->colegio->codigo);
            $this->dispatchBrowserEvent('editar_correcto');

        } else {
            $this->error = "El campo nombre es requerido";
        }
    }
}
