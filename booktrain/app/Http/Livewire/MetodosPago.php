<?php

namespace App\Http\Livewire;

use App\Models\MetodoPago;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class MetodosPago extends DataTableComponent
{
    public $modal_metodo_pago = null;
    public $metodo_pago = null;

    protected $rules = [
        'modal_metodo_pago' => 'sometimes',
        'metodo_pago.disponible' => 'sometimes',
        'metodo_pago.descripcion' => 'sometimes',
    ];

    public function columns(): array
    {
        return [
            Column::make('Codigo', 'codigo')->searchable(),
            Column::make('Descripción', 'descripcion')->searchable(),
            Column::make('Proveedor', 'proveedor'),
            Column::make('Formas de pago', ''),
            Column::make('Pedidos', ''),
            Column::make('Disponible', 'disponible'),
            Column::blank()
        ];
    }

    public function query()
    {
        return MetodoPago::withoutGlobalScope('disponibles');
    }

    public function rowView(): string
    {
        return 'livewire.metodos-pago-row';
    }

    public function editar($id_metodo_pago) {
        $this->metodo_pago = MetodoPago::withoutGlobalScope('disponibles')->find($id_metodo_pago);
        $this->modal_metodo_pago = true;
    }

    public function modalsView(): string
    {
        return 'livewire.metodos-pago-modal';
    }

    public function guardar() {
        $this->validate();

        $this->metodo_pago->save();
        $this->modal_metodo_pago = null;
    }
}
