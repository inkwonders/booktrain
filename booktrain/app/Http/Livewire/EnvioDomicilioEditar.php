<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class EnvioDomicilioEditar extends Component
{
    public Colegio $colegio;
    public $costo_envio;
    public function mount()
    {
        $this->costo_envio = $this->colegio->configuraciones()->where('etiqueta', 'costo_envio')->first()->valor;
    }
    public function updatedCostoEnvio()
    {
        $this->costo_envio = intval($this->costo_envio);
    }

    public function save()
    {
        $id =  $this->colegio->configuraciones()->where('etiqueta', 'costo_envio')->first()->id;
        DB::table('configuraciones')
            ->where('id', $id)
            ->update(['valor' => $this->costo_envio]);
        $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El costo de envío se modificó satisfactoriamente', 'icon' => 'success']);
    }
    public function render()
    {
        return view('livewire.envio-domicilio-editar');
    }
}
