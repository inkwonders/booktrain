<?php

namespace App\Http\Livewire;

use App\Models\NivelColegio;
use App\Models\SeccionColegio;
use Livewire\Component;

class NuevoNivel extends Component
{
    public NivelColegio $nivel;
    public SeccionColegio $seccion;

    protected $rules = [
        'nivel.nombre' => 'required',
        'nivel.activo' => 'required'
    ];

    protected $listeners = 'editNivel';

    public function editNivel(NivelColegio $nivel) {
        $this->nivel = $nivel;
    }

    function mount() {
        $this->nivel = new NivelColegio();
    }

    public function save() {
        $this->validate();
        $this->seccion->niveles()->save(
            $this->nivel
        );

        $this->nivel = new NivelColegio();
        $this->emit('refreshListaNiveles');
    }

    public function render()
    {
        return view('livewire.nuevo-nivel');
    }
}
