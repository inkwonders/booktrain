<?php

namespace App\Http\Livewire;

use App\Models\Paquete;
use App\Models\Libro;

use Livewire\Component;

class LibrosPaquetes extends Component
{
    public Paquete $paquete;

    public $libros;
    public $filtro;

    protected $listeners = ['refreshListaPaquetes'];

    function mount() {
        $this->refreshListaPaquetes();
    }

    public function refreshListaPaquetes() {
        $this->libros = $this->paquete->nivel->seccion->colegio->libros()->wherePivot('paquete_id', null)->where('nombre', 'like', '%' . $this->filtro . '%')->get();
    }

    public function asignar(Libro $libro) {
        $this->paquete
                ->libros()
                ->wherePivot('colegio_id', $this->paquete->nivel->seccion->colegio->id)
                ->attach($libro->id, ['obligatorio' => 0, 'activo' => 1, 'stock' => 0, 'bajo_pedido' => 0, 'precio' => 0, 'colegio_id' => $this->paquete->nivel->seccion->colegio->id]);

        $this->emit('refreshListaNiveles');
        $this->refreshListaPaquetes();
    }

    public function obligatorio($libro_id, $obligatorio) {
        $this->paquete->nivel->seccion->colegio->libros()->wherePivot('paquete_id', $this->paquete->id)->wherePivot('libro_id', $libro_id)->updateExistingPivot($libro_id, ['obligatorio' => $obligatorio]);
        $this->refreshListaPaquetes();
    }

    public function remover(Libro $libro) {
        $this->paquete
            ->libros()
            ->wherePivot('colegio_id', $this->paquete->nivel->seccion->colegio->id)
            ->detach($libro->id);

        $this->emit('refreshListaNiveles');
        $this->refreshListaPaquetes();
    }

    public function render()
    {
        return view('livewire.libros-paquetes');
    }
}
