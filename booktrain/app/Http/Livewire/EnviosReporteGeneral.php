<?php

namespace App\Http\Livewire;

use App\Models\Envio;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class EnviosReporteGeneral extends DataTableComponent
{
    public $table_headers = 'private.table_headers_envios';

    public $fecha_inicial;
    public $fecha_final;
    public $modal_envio;
    public $pedido_modal;

    protected $listeners = ['setFecha'];

    public function setFecha($which, $what) {
        if($which == 'inicial') $this->fecha_inicial = $what;
        if($which == 'final') $this->fecha_final = $what;
    }

    protected $rules = [
        'modal_envio' => 'sometimes'
    ];

    public function mount() {
        $this->fecha_inicial = now()->format('Y-m-d');
        $this->fecha_final = now()->format('Y-m-d');
    }

    public function columns(): array
    {
        return [
            Column::make('Estado del pedido', 'estado')
                ->sortable()
                ->searchable(function(Builder $query, $search) {
                    $query->orWhere('pedidos.status', 'LIKE', "%$search%");
                }),
            Column::make('Guía', 'guia')
                ->sortable()
                ->searchable(),
            Column::make('Folio')
                ->sortable()
                ->searchable(function(Builder $query, $search) {
                    $query->orWhere('folio', 'LIKE', "%$search%");
                }),
            Column::make('Tipo', 'tipo_envio')
                ->sortable(),
            Column::make('Fecha', 'fecha_envio')
                ->sortable(
                    function(Builder $query, $direction) {
                        return $query->orderBy('envios.fecha_envio', $direction);
                    }
                )
                ->searchable(),
            Column::make('Paquetería')
                ->searchable(function(Builder $query, $search) {
                    $query->whereHas('paqueteria', function($query) use ($search) {
                        $query->where('nombre', 'like', "%$search%");
                    })
                    ->orWhere('paqueteria', $search);
                }),
            Column::blank()
        ];
    }

    public function updated($attr, $value) {
        $props = explode('.', $attr);

        switch ($props[0]) {
            // case 'fecha_inicial':break;
            // case 'fecha_final':break;

            default:
                $this->emit('refreshDatatable');
                break;
        }
    }

    public function query(): Builder
    {
        return Envio::query()
            ->select('envios.*',
                DB::raw('pedidos.status as estado'),
                DB::raw("CASE SUBSTRING(enviable_type, 12) WHEN 'Pedido' THEN 'Completo' WHEN 'DetalleResumenPedido' THEN 'Parcial' ELSE SUBSTRING(enviable_type, 12) END AS tipo_envio"),
                DB::raw("(SELECT CONCAT(serie, folio) FROM pedidos WHERE pedidos.id = pedido_id LIMIT 1) AS folio"),
                // DB::raw("(SELECT status FROM pedidos WHERE pedidos.id = pedido_id LIMIT 1) AS estado")
            )
            ->join('pedidos', 'envios.pedido_id', '=', 'pedidos.id')
            ->when($this->fecha_inicial, function($query, $fecha_inicial) {
                $query->where('envios.created_at', '>', "{$fecha_inicial} 00:00:00");
            })
            ->when($this->fecha_final, function($query, $fecha_final) {
                $query->where('envios.created_at', '<', "{$fecha_final} 23:59:59");
            })
            ->whereNotNull('guia');
    }

    public function rowView(): string
    {
        return 'livewire.envios-reporte-general';
    }

    public function modalsView(): string
    {
        return 'livewire.modal-envios-reporte-general';
    }

    public function modalDetalles(Envio $envio) {
        if($envio->pedido == null)
            $this->pedido_modal = $envio->detalle->resumen->pedido;
        else
            $this->pedido_modal = $envio->pedido;

        $this->modal_envio = $envio;
    }

}
