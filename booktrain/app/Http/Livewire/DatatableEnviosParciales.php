<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use App\Models\ServicioPaqueteria;
use Illuminate\Support\Facades\DB;
use App\Models\DetalleResumenPedido;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class DatatableEnviosParciales extends DataTableComponent
{
    public $model = Pedido::class;

    public $pedidos;
    public $detalle_resumen_pedido;
    public $pedido_id;
    public $select_inicio;
    public $select_fin;
    public $modal_envio_parcial = false;
    public $pedido_modal;
    public $paqueteria;
    public $no_guia;
    public $libros_pendientes_envio = 0;
    public $total_libros_pedido = 0;

    public $libros_envio = [];

    protected $rules = [
        'no_guia' => 'required|min:6',
        'paqueteria' => 'required|exists:servicios_paqueteria,id',
    ];

    public function mount()
    {
        $this->select_inicio = date("Y-m-d");
        $this->select_fin = date("Y-m-d");
    }

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function getPaqueteriasProperty()
    {
        return ServicioPaqueteria::activo()->get();
    }

    public function inicio($select_inicio)
    {
        $this->select_inicio = $select_inicio;
    }
    public function fin($select_fin)
    {
        $this->select_fin = $select_fin;
    }

    function columns(): array
    {
        return [
            Column::make('Estado'),
            Column::make('Folio', 'folio')             ->sortable()->searchable(),
            Column::make('Fecha','created_at')         ->sortable()->searchable(),
            Column::make('Envio Parcial'),
            Column::make('Nombre', 'nombre_contacto')  ->sortable()->searchable(),
            Column::make('Colegio')                    ->sortable(),
        ];
    }

    public function query(): Builder
    {
        $inicio = $this->select_inicio;
        $fin = $this->select_fin;

        return Pedido::query()
            ->where('tipo_entrega', 2)
            ->whereIn('status',['ENTREGA PARCIAL','PAGADO','ENVIO PARCIAL'])
            ->with(['resumenes' => function($query) {
                $query->withCount([
                    'detalles as total_libros',
                    'detalles as libros_enviados' => function($query) {
                        $query->where('enviado', 1);
                    }
                ]);
            }])
            ->when($inicio, function($query) use ($inicio) {
                $query->where('created_at','>=', "{$inicio} 00:00:00");
            })
            ->when($fin, function ($query) use ($fin) {
                $query->where('created_at', '<=', "{$fin} 23:59:59");
            })->orderByRaw("CAST(folio as UNSIGNED) DESC");
    }

    public function rowView(): string
    {
        return ('livewire.datatable-envios-parciales');
    }

    public function modalsView(): string
    {
        return 'livewire.modal-datatable-envios-parciales';
    }

    public function modalEnvioParcial(Pedido $pedido)
    {
        $this->total_libros_pedido = 0;
        $this->libros_pendientes_envio = 0;

        $this->libros_envio = $pedido->resumenes->mapWithKeys(function($resumen) {
            return [
                $resumen->id => $resumen->detalles->mapWithKeys(function ($detalle) {
                    $this->total_libros_pedido++;

                    if ($detalle->enviado == 0)
                        $this->libros_pendientes_envio++;

                    return [
                        $detalle->id =>
                        [
                            'id'           => $detalle->id,
                            'libro'        => "{$detalle->libro->nombre}" . ($detalle->cantidad > 1 ? " ({$detalle->cantidad})" : ""),
                            'isbn'         => $detalle->libro->isbn,
                            'libro_id'     => $detalle->libro_id,
                            'cantidad'     => $detalle->cantidad,
                            'seleccionado' => 0 || $detalle->enviado,
                            'enviado'      => $detalle->enviado,
                            'entregado'    => $detalle->entregado,
                        ]
                    ];
                })
            ];
        });

        $this->pedido_modal = $pedido;
        $this->modal_envio_parcial = true;
    }

    public function save() {
        if($this->libros_pendientes_envio == 0) return;

        $this->validate(
            $this->rules,
            [
                'no_guia.required'      => 'La guía es obligatoria',
                'paqueteria.required'   => 'Debe seleccionar una paquetería'
            ]
        );

        $envio_parcial = false;

        $hoy = now()->format('Y-m-d');

        DB::beginTransaction();

        try {
            foreach (collect($this->libros_envio) as $resumen_id => $detalles) {
                foreach ($detalles as $detalle_id => $libro) {
                    if ($libro['seleccionado'] && $libro['enviado'] == 0) {
                        $envio_parcial = true;
                        $detalle_resumen_pedido = DetalleResumenPedido::find($detalle_id);

                        $detalle_resumen_pedido->envio()->create([
                            'pedido_id'     => $this->pedido_modal->id,
                            'guia'          => $this->no_guia,
                            'status'        => 'PROCESANDO',
                            'paqueteria_id' => $this->paqueteria,
                            'fecha_envio'   => $hoy,
                            'costo'         => $this->pedido_modal->colegio->config('costo_envio')
                        ]);

                        $detalle_resumen_pedido->update([
                            'enviado' => 1
                        ]);
                    }
                }
            }

            $pedido = Pedido::whereId($this->pedido_modal->id)->with(['resumenes' => function($query) {
                $query->withCount([
                    'detalles as total_libros',
                    'detalles as libros_enviados' => function($query) {
                        $query->where('enviado', 1);
                    }
                ]);
            }])->first();

            $envio_registrado = false;

            // SI se han enviado todos los libros del pedido cambiar su estado a 'ENVIADO'
            $total_libros = $pedido->resumenes->pluck('total_libros')->sum();
            $libros_enviados = $pedido->resumenes->pluck('libros_enviados')->sum();

            if($total_libros > 0 && $total_libros == $libros_enviados) {
                $pedido->update(['status' => 'ENVIADO']);
                $envio_registrado = true;
            }else{
                if($envio_parcial){
                    $envio_registrado = true;
                    $pedido->update(['status' => 'ENVIO PARCIAL']);
                }
            }

            if($envio_registrado)
                $this->emit('swalAlert', [
                    'title' => 'Envio registrado',
                    'text' => "Se registró el envío con la guía {$this->no_guia} en el pedido {$pedido->serie}-{$pedido->folio}",
                    'icon' => 'success'
                ]);

            activity()
                ->performedOn($pedido)
                ->causedBy(Auth::user()->id)
                ->log("Registró envio parcial del pedido {$this->pedido_modal->serie}-{$this->pedido_modal->folio} con guía: {$this->no_guia}");

            $servicio_paqueteria = ServicioPaqueteria::find($this->paqueteria);

            $libros = collect($this->libros_envio)->collapse()->filter(function($libro) {
                return $libro['enviado'] == 0 && $libro['seleccionado'] == 1;
            })->map(function($libro) {
                return (Object) [
                    'id' => $libro['libro_id'],
                    'isbn' => $libro['isbn'],
                    'nombre' => $libro['libro'],
                    'cantidad' => $libro['cantidad'],
                ];
            })->values();

            $pedido->user->sendPedidoEnvioParcialEnCaminoNotification($pedido, $this->no_guia, $servicio_paqueteria, $libros);

            $this->paqueteria = null;
            $this->no_guia = '';
            $this->modal_envio_parcial = false;
            $this->pedido_modal = null;

            $this->emit('refreshDatatable');
        } catch (\Throwable $th) {
            DB::rollback();

            Log::error('DatatableEnviosParciales', [$th]);

            $this->emit('swalAlert', [
                'title' => 'Ocurrió un error',
                'text' => "No se pudo registrar el envío, error: {$th->getMessage()}",
                'icon' => 'error'
            ]);
        }

        DB::commit();
    }
}
