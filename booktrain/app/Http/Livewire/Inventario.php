<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Livewire\Component;

class Inventario extends Component
{
    public $colegios;
    public $colegio;

    public function mount() {
        $this->colegios = Colegio::all();
    }

    public function updatedColegio($value) {
        $this->emit('seleccionarColegio', $value);
    }

    public function render()
    {
        return view('livewire.inventario');
    }
}