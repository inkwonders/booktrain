<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Configuracion;
use App\Models\MetodoPago;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ConfiguracionPago extends Component
{
    public $colegio;
    public $metodos_pago;

    public function getMetodosPagoProperty()
    {
        $colegio = $this->colegio;

        $iterador = 1;
        return $this->colegio->metodosPago->map(function ($metodo_pago) use ($colegio, &$iterador) {
            return [
                'id' => $metodo_pago->id,
                'nombre' => $metodo_pago->descripcion,
                'codigo' => $metodo_pago->codigo,
                'proveedor' => ($metodo_pago->proveedor == null ? 'N/A' : $metodo_pago->proveedor),
                'contexto' => $metodo_pago->pivot->contexto,
                'formas_pago' => $metodo_pago->formasPago()->wherePivot('colegio_id', $colegio->id)->get()->map(function ($forma_pago) use ($metodo_pago, &$iterador) {
                    return [
                        'index'          => $iterador++,
                        'id'             => $forma_pago->id,
                        'nombre'         => $forma_pago->descripcion,
                        'codigo'         => $forma_pago->codigo,
                        'comision'       => $forma_pago->pivot->comision,
                        'minimo'         => $forma_pago->pivot->minimo,
                        'activo'         => $forma_pago->pivot->activo
                    ];
                })->keyBy('id')->toArray()
            ];
        })->keyBy('id')->toArray();
    }

    public function updated($name, $value)
    {
        $props = explode('.', $name);

        if ($props[0] == 'metodos_pago') {
            $this->colegio->formasPago()
                ->wherePivot('metodo_pago_id', $props[1])
                ->updateExistingPivot($props[3], [
                    'activo' => $value
                ]);

            $metodo_pago = $this->metodos_pago[$props[1]]['nombre'];
            $forma_pago = $this->metodos_pago[$props[1]]['formas_pago'][$props[3]]['nombre'];
            $activo =  $value ? 'Activo' : 'Inactivo';

            $metodo = new MetodoPago;
            activity()
                ->performedOn($metodo)
                ->causedBy(Auth::user()->id)
                ->log('Cambió el status del método de pago "' . $metodo_pago . '-' . $forma_pago . '" a ' . $activo . ' del colegio ' . $this->colegio->codigo);

            // $this->emit('swalAlert', [
            //     'title' => 'Cambio registrado correctamente',
            //     'text' => "Se actualizó correctamente {$metodo_pago} - {$forma_pago} a $activo",
            //     'icon' => 'success'
            // ]);
        }
    }

    public function mount()
    {
        $this->metodos_pago = $this->getMetodosPagoProperty();
    }

    public function render()
    {
        return view('livewire.configuracion-pago');
    }
}
