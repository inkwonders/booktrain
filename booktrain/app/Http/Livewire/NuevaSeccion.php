<?php

namespace App\Http\Livewire;

use App\Models\SeccionColegio;
use App\Models\Colegio;
use Livewire\Component;

class NuevaSeccion extends Component
{
    public SeccionColegio $seccion;
    public Colegio $colegio;

    protected $listeners = ['editSeccion'];

    protected $rules = [
        'seccion.nombre' => 'required',
        'seccion.activo' => 'required'
    ];

    public function mount() {
        $this->seccion = new SeccionColegio;
    }

    public function editSeccion($id_seccion) {
        $this->seccion = SeccionColegio::find($id_seccion);
    }

    public function save() {
        $this->validate();

        $this->colegio->secciones()->save($this->seccion);
        $this->seccion = new SeccionColegio;
        $this->emit('refreshColegio');
        $this->emit('refreshSecciones');
    }

    public function render()
    {
        return view('livewire.nueva-seccion');
    }
}
