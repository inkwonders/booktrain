<?php

namespace App\Http\Livewire;

use Livewire\Component;
class ContenedorEnviosParciales extends Component
{
    public function render()
    {
        return view('livewire.contenedor-envios-parciales');
    }
}
