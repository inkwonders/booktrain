<?php

namespace App\Http\Livewire;

use App\Models\SeccionColegio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ListaSeccionesEditando extends Component
{
    public $secciones;
    public $colegio;
    public $max_cantidad_niveles = 0;
    public $seccion;
    public $ocultar_botones = false;

    protected $listeners = ['refreshSecciones'];

    public function refreshSecciones()
    {
        $this->secciones = $this->colegio->secciones;
        $this->max_cantidad_niveles = $this->colegio->secciones()->withCount('niveles')->get()->max('niveles_count');
        if ($this->max_cantidad_niveles == 0) {   // Se supone que esto nunca debería ser cero y si es cero es porque la información está mal cargada en la BD
            $this->max_cantidad_niveles = 1;
        }
    }

    public function mount()
    {
        $this->refreshSecciones();
    }

    public function delete()
    {

        if ($this->seccion->niveles()->count() >= 1) {
            session()->flash('message', 'La sección no puede ser eliminada debido a que tiene grados o pedidos asociados.');
            $this->ocultar_botones = true;
        } else {
            $this->seccion->delete();
            $this->dispatchBrowserEvent('eliminar');
            $seccion = new SeccionColegio;
            activity()
                ->performedOn($seccion)
                ->causedBy(Auth::user()->id)
                ->log('Eliminó la sección "' . $this->seccion->nombre . '" del colegio ' . $this->colegio->codigo);
        }
    }
    public function beforeDelete(SeccionColegio $seccion)
    {
        $this->seccion = $seccion;
        session()->flash('message', '¿Está seguro de eliminar esta seccion?');
    }

    public function render()
    {
        return view('livewire.lista-secciones-editando');
    }
}
