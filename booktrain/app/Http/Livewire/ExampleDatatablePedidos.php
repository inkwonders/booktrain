<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Livewire\Component;
use Rappasoft\LaravelLivewireTables\Views\Filter;

// class ExampleDatatablePedidos extends DataTableComponent
class ExampleDatatablePedidos extends DataTableComponent
{
    public $model = Pedido::class;
    public $formData;
    public $status;
    public $factura;
    public $metodo_pagos;
    public $forma_pagos;
    public $forma_entrega;
    public $form_search;
    public $inicio;
    public $fin;


    public function verificar(Pedido $usuario)
    {
        // $usuario->verificarCuenta();
    }

    public function activo(Pedido $usuario)
    {
        // if ($usuario->activo == 1) {
        //     $usuario->activo = 0;
        // } else {
        //     $usuario->activo = 1;
        // }
        // $usuario->save();
    }

    function columns(): array
    {
        return [
            // Column::make('id', 'id')
            //     ->sortable()
            //     ->searchable(),
            // Column::make('folio', 'folio')
            //     ->sortable()
            //     ->searchable(),
            // Column::make('Nombre', 'nombre_contacto')
            //     ->sortable(),
            // Column::make('Apellidos', 'apellidos_contacto')
            //     ->sortable(),
            // Column::make('total', 'total')
            //     ->sortable(),
            // Column::make('Metodo de pago', 'metodo_pago')

            Column::make('Folio', 'folio')
                ->sortable()->searchable(),
            Column::make('Fecha','created_at')
                ->sortable()->searchable(),
            Column::make('Nombre', 'nombre_contacto')
                ->sortable()->searchable(),
            Column::make('Correo', 'email_pedido')
                ->sortable()->searchable(),
            Column::make('Teléfono', 'celular_contacto')
                ->sortable()->searchable(),
            Column::make('Factura')
                ->sortable(),
            Column::make('Referencia')
                ->sortable(),
            Column::make('Método de pago')
                ->sortable(),
            Column::make('Forma de pago')
                ->sortable(),
            Column::make('Monto cobrado')
                ->sortable(),
            Column::make('Monto recibido')
                ->sortable(),
            Column::make('Colegio')
                ->sortable(),
            Column::make('Fecha pago')
                ->sortable(),
            Column::make('Entrega')
                ->sortable(),
            Column::make('Estatus')
                ->sortable(),
            Column::make('Cambio de Estatus')
                ->sortable(),
            Column::make('Correo de confirmación')
                ->sortable(),

        ];
    }
    public array $filters = [
        [
            'filter' => 'year',
            'field' => 'worker_start_year',
        ],
        [
            'filter' => 'date',
            'field' => 'application_date',
        ],
    ];
    public function query(): Builder
    {

        $pedidos = Pedido::query()
        ->where('created_at','>', request('inicio').' 00:00:00')
        ->where('created_at','<', request('fin').' 00:00:00')
        ->where('status','<>','CARRITO')
        ->orderByDesc('folio');


        if (request('status') != 0 ||  request('status') != '0')
        $pedidos = $pedidos->where('status',  request('status'));

    if (request('factura') == 1 ||  request('factura') == '1')
        $pedidos = $pedidos->where('factura', request('factura'));

    if (request('factura') == 2 ||  request('factura') == '2')
        $pedidos = $pedidos->where('factura', null);

    if (request('metodo_pagos') != 0 ||  request('metodo_pagos') != '0')
        $pedidos = $pedidos->where('metodo_pago',  request('metodo_pagos'));

    if (request('forma_pagos') != 0 ||  request('forma_pagos') != '0')
        $pedidos = $pedidos->where('forma_pago',  request('forma_pagos'));

    if (request('forma_entrega') != 0 ||  request('forma_entrega') != '0')
        $pedidos = $pedidos->where('tipo_entrega',  request('forma_entrega'));


        return $pedidos;
    }

    public function rowView(): string
    {
        return ('livewire.example-datatable-pedidos');
    }
}
