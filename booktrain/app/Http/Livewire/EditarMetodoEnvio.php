<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Direccion;
use App\Models\DireccionEntrega;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EditarMetodoEnvio extends Component
{
    public Colegio $colegio;
    public $metodo_envio;
    public Direccion $direccion;
    public $estados;
    public $municipios;

    public $estado_elegido;
    public $municipio_elegido;

    protected $rules = [
        'direccion.calle' => 'required',
        'direccion.no_exterior' => 'required',
        'direccion.no_interior' => 'required',
        'direccion.colonia' => 'required',
        'direccion.estado_id' => 'required',
        'direccion.municipio_id' => 'required',
        'direccion.referencia' => 'required',
    ];

    public function mount()
    {
        $this->direccion = Direccion::where('id', $this->metodo_envio)->first();
        $this->estados = Estado::all();
        $this->municipios = Municipio::where('estado_id', $this->direccion->estado_id)->get();

        $this->estado_elegido = $this->direccion->estado_id;
        $this->municipio_elegido = $this->direccion->municipio_id;
    }

    public function render()
    {
        return view('livewire.editar-metodo-envio');
    }

    public function changeMunicipios()
    {
        $this->municipios = Municipio::where('estado_id', $this->estado_elegido)->get();
    }

    public function save()
    {
        $this->direccion->estado_id = $this->estado_elegido;
        $this->direccion->municipio_id = $this->municipio_elegido;
        $this->direccion->save();
        $this->dispatchBrowserEvent('editar_correcto');
        $direccion_entrega = DireccionEntrega::where('direccion_id', $this->direccion->id)->first();

        activity()
            ->performedOn($direccion_entrega)
            ->causedBy(Auth::user()->id)
            ->log('Editó la dirección "' . $direccion_entrega->tipo . '" del colegio ' . $this->colegio->codigo);
    }
}
