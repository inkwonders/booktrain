<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Direccion;
use Livewire\Component;

class EditarDireccionEditando extends Component
{
    public Direccion $direccion;
    public Colegio $colegio;

    public $estados;
    public $municipios;

    protected $listeners = ['selectEstado'];

    protected $rules = [
        'direccion.calle'           => 'required',
        'direccion.colonia'         => 'required',
        'direccion.no_exterior'     => 'required',
        'direccion.no_interior'     => 'required',
        'direccion.referencia'      => 'required',
        'direccion.estado_id'       => 'required',
        'direccion.municipio_id'    => 'required',
        'direccion.pivot.tipo'      => 'required'
    ];

    function mount()
    {
        $this->estados = Estado::all();
        $this->municipios = Municipio::where('estado_id', $this->direccion->estado_id)->get();
    }

    function selectEstado()
    {
        $this->municipios = $this->direccion->estado->municipios;
    }

    public function save()
    {
        $this->validate();
        $pivot = $this->direccion->pivot;
        dd($pivot);
        unset($this->direccion->pivot);

        $this->direccion->pivot = $pivot;
        $this->colegio
            ->direcciones()
            ->updateExistingPivot($this->direccion->id, [
                'tipo' => $this->direccion->pivot['tipo']
            ]);
        $this->emit('refresh');
    }

    public function render()
    {
        return view('livewire.editar-direccion-editando');
    }
}
