<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Libro;
use App\Models\LibrosColegios;
use App\Models\Paquete;
use App\Models\NivelColegio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Mockery\Undefined;

class LibrosColegioEditando extends Component
{
    public $asignar_libro = false;
    public $editar_libro_asignado = false;
    public $libro_asignado;
    public $datos_libro_asignado;
    public Colegio $colegio;

    public $seccion_elegida;
    public $nivel_elegido;

    public $precio;
    public $obligatorio = 0;
    public $bajo_pedido = 0;
    public $activo;

    public $paquete;

    public $buscar;

    function mount()
    {
    }

    public function getLibrosProperty()
    {
        $colegio = $this->colegio;

        return Libro::withCount(['colegios as asignado_a_colegio' => function ($query) use ($colegio) {
            $query->where('colegio_id', $colegio->id);
            $query->where('paquete_id', null);
        }])
            ->where('nombre', 'like', '%' . $this->buscar . '%')
            ->orWhere('isbn', 'like', '%' . $this->buscar . '%')
            ->orderBy('asignado_a_colegio', 'DESC')
            ->orderBy('isbn', 'DESC')
            ->get();
    }

    public function getSeccionesProperty()
    {
        return $this->colegio->secciones()
            ->where('activo', 1)
            ->get();
    }

    // Regresamos solo los niveles que no tengan asignado el libro seleccionado
    public function getNivelesProperty()
    {
        if ($this->seccion_elegida == 0)
            return [];

        $libro = $this->libro_asignado;
        $colegio = $this->colegio;

        return $this->colegio->secciones()
            ->find($this->seccion_elegida)
            ->niveles()
            ->where('activo', 1)
            ->whereDoesntHave('paquetes', function ($query) use ($colegio, $libro) {
                $query->whereHas('libros', function ($query) use ($colegio, $libro) {
                    $query->where('libros_colegios.colegio_id', $colegio->id);
                    $query->where('libros_colegios.libro_id', $libro->id);
                });
            })
            ->get();
    }

    public function updatedPrecio()
    {
        $output = preg_replace('/[^0-9]/', '', $this->precio);
        $this->precio = $output;
    }

    public function asignarLibro(Libro $libro)
    {
        $this->libro_asignado = null;

        $this->limpiaCampos();
        $this->asignar_libro = true;

        $this->libro_asignado = $libro;
    }

    public function limpiaCampos()
    {
        $this->nivel_elegido = 0;
        $this->seccion_elegida = 0;
        $this->precio = 0;
        $this->obligatorio = false;
        $this->bajo_pedido = false;

        $this->editar_libro_asignado = false;
        $this->asignar_libro = false;
    }

    public function editarLibroAsignado($libro_id)
    {
        $this->libro_asignado = $this->colegio->libros()->wherePivot('paquete_id', null)->where('libro_id', $libro_id)->first();

        $this->activo = $this->libro_asignado->pivot->activo;
        $this->bajo_pedido = $this->libro_asignado->pivot->bajo_pedido;

        $this->asignar_libro = false;
        $this->editar_libro_asignado = true;
    }

    public function render()
    {
        return view('livewire.libros-colegio-editando');
    }
    public function save()
    {
        if ($this->asignar_libro) {
            // Asignamos un nuevo libro
            $this->validate([
                'seccion_elegida' => 'required',
                'nivel_elegido' => 'required',
                'precio' => 'required',
                'bajo_pedido' => 'required',
                'obligatorio' => 'required'
            ]);

            $nivel_elegido = $this->nivel_elegido;
            $seccion_elegida = $this->seccion_elegida;

            // Verificamos que el libro este asignado al colegio
            $libro_asignado_colegio = $this->colegio
                ->libros()
                ->wherePivotNull('paquete_id')
                ->wherePivot('libro_id', $this->libro_asignado->id)
                ->count() > 0;

            if ($libro_asignado_colegio == false) {
                // Creamos la asociación base del libro con el colegio
                $this->colegio->libros()->attach($this->libro_asignado->id, [
                    'paquete_id' => null,
                    'stock' => 0,
                    'activo' => true,
                    'bajo_pedido' => $this->bajo_pedido,
                    'punto_reorden' => 0,
                    // 'obligatorio' => null,
                    'precio' => 0
                ]);
            }

            $paquetes = $this->colegio
                ->paquetes()
                ->wherePivotNotNull('paquete_id')
                ->wherePivot('libro_id', $this->libro_asignado->id)
                ->whereHas('nivel', function ($query) use ($nivel_elegido, $seccion_elegida) {
                    $query->where('id', $nivel_elegido);
                    $query->whereHas('seccion', function ($query) use ($seccion_elegida) {
                        $query->where('id', $seccion_elegida);
                    });
                })
                ->get();

            if ($nivel_elegido > 0 && $seccion_elegida > 0)
                if ($paquetes->count() > 0) {
                    // Ya existe un paquete asociado al colegio, nivel y sección
                    $this->emit('swalAlert', [
                        'title' => 'Error al asignar el libro',
                        'text' => 'El libro ya se encuentra asociado a la sección y nivel elegidos de este colegio',
                        'icon' => 'error'
                    ]);
                } else {
                    $paquete = Paquete::where('nivel_id', $nivel_elegido)->first();

                    if (null == $paquete)
                        $paquete = Paquete::create([
                            'nivel_id' => $nivel_elegido,
                            'nombre' => NivelColegio::find($nivel_elegido)->nombre,
                            'activo' => 1
                        ]);

                    // No existe el libro en los paquetes del colegio, lo asignamos
                    $this->colegio->libros()->attach($this->libro_asignado->id, [
                        'paquete_id' => $paquete->id,
                        'stock' => 0,
                        'activo' => true,
                        'bajo_pedido' => $this->bajo_pedido,
                        'punto_reorden' => 0,
                        'obligatorio' => $this->obligatorio,
                        'precio' => $this->precio
                    ]);

                    $this->emit('swalAlert', [
                        'title' => 'Asociación exitosa',
                        'text' => 'El libro ha sido asignado exitosamente al colegio',
                        'icon' => 'success'
                    ]);

                    $this->limpiaCampos();
                }

            activity()
                ->performedOn($this->libro_asignado)
                ->causedBy(Auth::user()->id)
                ->log('Asignó el libro ' . $this->libro_asignado->isbn . ' al colegio ' . $this->colegio->codigo);
        } else {
            // Editando libro ya asignado
            $this->validate([
                'activo' => 'sometimes|required',
                'bajo_pedido' => 'sometimes|required'
            ]);

            $this->colegio->libros()->wherePivot('paquete_id', null)
                ->updateExistingPivot($this->libro_asignado->id, [
                    'activo' => boolval($this->activo),
                    'bajo_pedido' => boolval($this->bajo_pedido)
                ]);

            $this->emit('swalAlert', [
                'title' => 'Actualización correcta',
                'text' => "Se actualizo el registro del libro {$this->libro_asignado->nombre} - ISBN:{$this->libro_asignado->isbn}",
                'icon' => 'success'
            ]);

            $this->limpiaCampos();

            activity()
                ->performedOn($this->libro_asignado)
                ->causedBy(Auth::user()->id)
                ->log('Editó el libro ' . $this->libro_asignado->isbn . ' del colegio ' . $this->colegio->codigo);
        }
    }
}
