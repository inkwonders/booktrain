<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Domains\User\Exports\UserExport;
use App\Domains\Auth\Models\User;

class ReportePedidosGeneral extends DataTableComponent
{
    protected $listeners = ['fechas' => 'buscarFecha',
                            'status' => 'buscarStatus',
                            'factura' => 'buscarFactura',
                            'metodo_pagos' => 'buscarMetodo_pagos',
                            'forma_pagos' => 'buscarForma_pagos',
                            'forma_entrega' => 'buscarForma_entrega',
                            'modalDetallesPedido',
                            'exportar' => 'exportarExcel'];
    public $inicio = null;
    public $fin = null;
    public $status = null;
    public $factura= null;
    public $metodo_pagos = null;
    public $forma_pagos = null;
    public $forma_entrega = null;
    public $modal_reorden = false;
    public $pedido_id;
    public $pedido_modal;

    public function buscarFecha($fecha_inicio, $fecha_fin)
    {
        $this->inicio = $fecha_inicio;
        $this->fin = $fecha_fin;
        $this->query();
    }
    public function buscarStatus($status)
    {
        $this->status = $status;
        $this->query();
    }
    public function buscarFactura($factura)
    {
        $this->factura = $factura;
        $this->query();
    }
    public function buscarMetodo_pagos($metodo_pagos)
    {
        $this->metodo_pagos = $metodo_pagos;
        $this->query();
    }
    public function buscarForma_pagos($forma_pagos)
    {
        $this->forma_pagos = $forma_pagos;
        $this->query();
    }
    public function buscarForma_entrega($forma_entrega)
    {
        $this->forma_entrega = $forma_entrega;
        $this->query();
    }
    public function modalDetallesPedido($pedido_id) {
        $this->pedido_modal = Pedido::where('id', $pedido_id)->first();
        $this->modal_reorden = true;
    }
    public function cerrarModal() {
        $this->modal_reorden = false;
    }

    public function columns(): array
    {
        return [
            Column::make('Folio', 'folio')
                ->sortable()->searchable(),
            Column::make('Fecha','created_at')
                ->sortable()->searchable(),
            Column::make('Nombre', 'nombre_contacto')
                ->sortable()->searchable(),
            Column::make('Correo', 'email_pedido')
                ->sortable()->searchable(),
            Column::make('Teléfono', 'celular_contacto')
                ->sortable()->searchable(),
            Column::make('Factura','factura')
                ->sortable()->searchable(),
            Column::make('Referencia','referencia')
                ->sortable(),
            Column::make('Método de pago','metodo_pago')
                ->sortable()->searchable(),
            Column::make('Forma de pago','forma_pago')
                ->sortable()->searchable(),
            Column::make('Monto cobrado','total')
                ->sortable()->searchable(),
            Column::make('Monto recibido')
                ->sortable(),
            Column::make('Colegio', 'colegio_id')
                ->sortable()->searchable(),
            Column::make('Fecha pago')
                ->sortable(),
            Column::make('Entrega')
                ->sortable(),
            Column::make('Estatus')
                ->sortable(),
            Column::make('Cambio de Estatus')
                ->sortable(),
            Column::make('Correo de confirmación')
                ->sortable(),
            Column::make('Pedido')

        ];
    }

    public function query(): Builder
    {
        if ($this->inicio == null && $this->fin == null) {
            $hoy = Carbon::today()->format('Y-m-d');
            $pedido =  Pedido::query()
                ->whereBetween('created_at', [$hoy . ' 00:00:00', $hoy . ' 23:59:59'])->orderByDesc('folio')
                ->whereNotIn('status', ['CARRITO']);
                return $pedido;
        } else {
            $pedido =  Pedido::whereBetween('created_at', [$this->inicio . ' 00:00:00', $this->fin . ' 23:59:59'])->orderByDesc('folio')
                ->whereNotIn('status', ['CARRITO']);

        }
            if ($this->status != 0 ||  $this->status != '0')
                $pedido = $pedido->where('status',  $this->status);
            if ($this->factura == 1 ||  $this->factura == '1')
                $pedido = $pedido->where('factura', $this->factura);

            if ($this->factura == 2 ||  $this->factura == '2')
                $pedido = $pedido->where('factura', null);

            if ($this->metodo_pagos != 0 ||  $this->metodo_pagos != '0')
                $pedido = $pedido->where('metodo_pago',  $this->metodo_pagos);

            if ($this->forma_pagos != 0 ||  $this->forma_pagos != '0')
                $pedido = $pedido->where('forma_pago',  $this->forma_pagos);

            if ($this->forma_entrega != 0 ||  $this->forma_entrega != '0')
                $pedido = $pedido->where('tipo_entrega',  $this->forma_entrega);

        return $pedido;


    }

    public function rowView(): string
    {
        return 'livewire.reporte-pedidos-general';
    }

}
