<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Livewire\Component;
use App\Models\User;
use Backpack\PermissionManager\app\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;



class NuevoUsuario extends Component
{
    public User $user;
    public $new_password = "";
    public $password_confirmation = "";
    public $mensaje_error = "";
    public $roles_elegidos = "Cliente";
    public $permisos_elegidos = [];
    public $roles;
    public $permisos;
    public $mostrar_permisos_admin = false;
    public $elegir_colegio = false;
    public $colegios;

    protected $listeners = [];

    protected $rules = [
        'user.name' => 'required',
        'user.email' => 'required',
        'user.apellidos' => 'required'
    ];

    public function mount(User $user)
    {

        $this->user = new User;
        $this->roles = $user->getAllRoles();

        $this->colegios = Colegio::all();
        $this->permisos = Permission::get()->unique()->whereNotIn(
            'name',
            $this->colegios->pluck('codigo')
        );
    }

    public function render()
    {
        return view('livewire.nuevo-usuario');
    }

    public function updatedRolesElegidos()
    {
        $this->elegir_colegio = false;
        $this->mostrar_permisos_admin = false;

        if ($this->roles_elegidos == "Admin") {
            $this->mostrar_permisos_admin = true;
        }
        if ($this->roles_elegidos == "Colegio" || $this->roles_elegidos == "Venta Movil") {
            $this->elegir_colegio = true;
            $this->permisos_elegidos = 0;
        }
    }

    public function save()
    {

        if (!empty($this->roles_elegidos)) {

            if ($this->roles_elegidos == "Colegio" || $this->roles_elegidos == "Venta Movil") {
                $buscar_permiso =  Permission::where('name', $this->permisos_elegidos)->first();

                if ($buscar_permiso == null) {
                    $permiso_nuevo = new Permission;
                    $permiso_nuevo->name = $this->permisos_elegidos;
                    $permiso_nuevo->save();
                }
            }

            $check_correo = User::where('email', $this->user->email)->get();
            if ($check_correo->count() != 0) {
                $this->mensaje_error = "Ya existe una cuenta asociada a este correo, favor de verificarlo.";
            } else {
                if ($this->new_password != "") {
                    if ($this->new_password == $this->password_confirmation) {
                        if (strlen($this->new_password) < 6) {
                            $this->mensaje_error = "La contraseña debe ser mayor a 6 caracteres.";
                        } else {
                            $this->user->password =  Hash::make($this->new_password);
                            $this->validate();
                            $this->user->save();
                            $user = new User;
                            activity()
                                ->performedOn($user)
                                ->causedBy(Auth::user()->id)
                                ->log('Creó un nuevo usuario: ' . $this->user->email);
                            $this->dispatchBrowserEvent('actualizarListaUsuarios');
                            $this->user->syncRoles($this->roles_elegidos);
                            $this->user->syncPermissions($this->permisos_elegidos);
                            return redirect()->to('/admin/nuevo_usuario');
                        }
                    } else {
                        $this->mensaje_error = "Las contraseñas no coinciden.";
                    }
                } else {
                    $this->mensaje_error = "La contraseña es obligatoria.";
                    // $2y$10$9lwrZ9K1nDJVzFshxCufzeFG24GzalcautqGSI0bKW1esBYEtwiIi
                }
            }
        } else {
            $this->mensaje_error = "El usuario debe tener al menos un permiso.";
        }
    }
}
