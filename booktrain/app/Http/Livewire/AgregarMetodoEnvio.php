<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Direccion;
use App\Models\DireccionEntrega;
use App\Models\Estado;
use App\Models\Municipio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
class AgregarMetodoEnvio extends Component
{
    public Colegio $colegio;
    public $metodo_envio;
    public $direccion;
    public $estados;
    public $municipios;

    protected $rules = [
        'direccion.calle'        => 'required',
        'direccion.no_exterior'  => 'required',
        'direccion.cp'           => 'required',
        'direccion.no_interior'  => '',
        'direccion.colonia'      => 'required',
        'direccion.estado_id'    => 'required',
        'direccion.municipio_id' => 'required',
        'direccion.referencia'   => 'max:500',
    ];

    public function mount()
    {
        $this->direccion = new Direccion;
        $this->estados = Estado::all();
        $this->direccion->estado_id = 0;
        $this->direccion->municipio_id = 0;
    }

    function selectEstado()
    {
        $this->municipios = Municipio::where('estado_id', $this->direccion->estado_id)->get();
    }

    public function render()
    {
        return view('livewire.agregar-metodo-envio');
    }

    public function save()
    {
        $this->validate();
        $this->direccion->save();
        $direccion_entrega = new DireccionEntrega;
        $direccion_entrega->colegio_id = $this->colegio->id;
        $direccion_entrega->direccion_id = $this->direccion->id;
        if ($this->metodo_envio == 'pick_up') {
            $direccion_entrega->tipo = 'PICK UP';
        } else {
            $direccion_entrega->tipo = 'COLEGIO';
        }
        $metodo = new DireccionEntrega;
        activity()
            ->performedOn($direccion_entrega)
            ->causedBy(Auth::user()->id)
            ->log('Agregó el método de envío "' . $direccion_entrega->tipo . '" al colegio ' . $this->colegio->codigo);
        $direccion_entrega->save();

        return redirect()->to('/admin/colegios_envio/' . $this->colegio->id);
    }
}
