<?php

namespace App\Http\Livewire;

use App\Models\Paquete;
use Livewire\Component;

class ListaPaquetes extends Component
{
    public $nivel;
    public $seccion;
    public $paquetes;

    protected $listeners = ['refreshListaPaquetes'];

    public function mount() {
        $this->refresh();
    }

    public function delete(Paquete $paquete) {
        $paquete->delete();

        $this->emit('refreshListaPaquetes');
        $this->emit('refreshListaNiveles');
    }

    public function refreshListaPaquetes() {
        $this->refresh();
    }

    public function refresh() {
        $this->paquetes = $this->nivel->paquetes;
    }

    public function render()
    {
        return view('livewire.lista-paquetes');
    }
}
