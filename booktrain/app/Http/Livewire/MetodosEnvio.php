<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Colegio;
use App\Models\DireccionEntrega;
use App\Models\Estado;
use App\Models\Municipio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MetodosEnvio extends Component
{
    public Colegio $colegio;

    public function mount()
    {

    }

    public function render()
    {
        return view('livewire.metodos-envio');
    }

    public function estado($id)
    {
        $estado = Estado::where('id', $id)->first();
        return $estado->descripcion;
    }

    public function municipio($id)
    {
        $municipio = Municipio::where('id', $id)->first();
        return $municipio->descripcion;
    }

    public function status($direccion_id)
    {
        // $metodos_envio_colegio = $this->colegio->direccionesEntrega()->count();
        $direccion_entrega = DireccionEntrega::where('colegio_id', $this->colegio->id)
            ->where('direccion_id', $direccion_id)
            ->first();

        if ($direccion_entrega->activo == 1) {
            // $direccion_entrega->activo = 0;
            DB::table('direccion_entregas')
                ->where('colegio_id', $this->colegio->id)
                ->where('direccion_id', $direccion_id)
                ->update(['activo' => 0]);
            $status = "inactivo";
        } else {
            // $direccion_entrega->activo = 1;
            DB::table('direccion_entregas')
                ->where('colegio_id', $this->colegio->id)
                ->where('direccion_id', $direccion_id)
                ->update(['activo' => 1]);
            $status = "activo";
        }

        activity()
            ->performedOn($direccion_entrega)
            ->causedBy(Auth::user()->id)
            ->log('Cambió el status del método de envío "' . $direccion_entrega->tipo . '" del colegio ' . $this->colegio->codigo . ' a ' . $status);
        return redirect()->to('/admin/colegios_envio/' . $this->colegio->id);
    }

    public function statusEnvioDomicilio($id, $status)
    {
        DB::table('configuraciones')
            ->where('id', $id)
            ->update(['valor' => !$status]);

        $status = ($status) ? 'inactivo' : 'activo';

        activity()
            ->performedOn($this->colegio)
            ->causedBy(Auth::user()->id)
            ->log("Cambió el status del método de envío a domicilio del colegio {$this->colegio->codigo} a {$status}");

        return redirect()->to('/admin/colegios_envio/' . $this->colegio->id);
    }
}
