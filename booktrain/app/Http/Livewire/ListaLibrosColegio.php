<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\LibrosColegios;
use App\Models\Libro;
use App\Models\NivelColegio;
use App\Models\Paquete;
use App\Models\SeccionColegio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ListaLibrosColegio extends Component
{
    public Colegio $colegio;
    public $catalogo_libros;
    public $libro_id;
    public $paquete_id;

    public function mount()
    {
        $colegio_id = $this->colegio->id;

        $this->catalogo_libros = $this->colegio->libros()->whereNotNull('paquete_id')->withCount(['paquetes as tiene_ventas' => function ($query) use ($colegio_id) {
            $query->where('libros_colegios.colegio_id', $colegio_id);
            $query->whereNotNull('libros_colegios.paquete_id');
            $query->whereHas('resumenes', function ($query) {
                $query->whereHas('detalles', function ($query) {
                    $query->whereRaw('detalles_resumen_pedidos.libro_id = libros_colegios.libro_id');
                });
                $query->has('pedido');
            });
        }])->get();
    }
    public function render()
    {
        return view('livewire.lista-libros-colegio');
    }
    public function seccion($id)
    {
        $seccion = Paquete::where('id', $id)->first()->NivelColegio->seccion;
        return $seccion;
    }
    public function grado($id)
    {
        $grado = Paquete::where('id', $id)->first()->NivelColegio;
        return $grado;
    }

    public function libroPivote($id, $colegio_id)
    {
        $libro_pivote = LibrosColegios::where('libro_id', $id)->where('colegio_id', $colegio_id)->where('paquete_id', null)->first();
        return $libro_pivote;
    }

    public function libroColegio($libro_id, $colegio_id, $paquete_id)
    {
        $libro_colegio = LibrosColegios::where('libro_id', $libro_id)->where('colegio_id', $colegio_id)->where('paquete_id', $paquete_id)->first();
        return $libro_colegio;
    }

    public function beforeDelete($id_libro, $paquete_id)
    {
        $this->libro_id = $id_libro;
        $this->paquete_id = $paquete_id;
        $this->catalogo_libros = $this->colegio->libros()->whereNotNull('paquete_id')->get();

        session()->flash('message', '¿Está seguro de eliminar este libro?');
    }

    public function cancelar()
    {
        return redirect()->to('admin/lista_libros_colegio/' . $this->colegio->id);
    }

    /**
     * Eliminar la asociación del libro con el colegio y su paquete (grado o nivel), siempre y cuando, no se tenga ventas de ese libro y el paquete (grado o nivel) que se quiere eliminar
     */
    public function delete()
    {
        $colegio_id = $this->colegio->id;
        $paquete_id = $this->paquete_id;
        $libro_id = $this->libro_id;

        $libro = Libro::where('libros.id', $libro_id)->withCount(['paquetes as tiene_ventas' => function ($query) use ($colegio_id, $paquete_id, $libro_id) {
            $query->where('libros_colegios.colegio_id', $colegio_id);
            $query->where('libros_colegios.paquete_id', $paquete_id);
            $query->whereHas('resumenes', function ($query) use ($libro_id) {
                $query->whereHas('detalles', function ($query) use ($libro_id) {
                    $query->where('libro_id', $libro_id);
                });
                $query->has('pedido');
            });
        }])->first();

        if ($libro->tiene_ventas > 0) {
            $this->catalogo_libros = $this->colegio->libros()->whereNotNull('paquete_id')->get();
            $this->dispatchBrowserEvent('relaciones');
        } else {
            $this->catalogo_libros = $this->colegio->libros()->whereNotNull('paquete_id')->get();
            if (DB::table('libros_colegios')->where('libro_id', $this->libro_id)->where('colegio_id', $this->colegio->id)->count() == 2) {
                //DB::table('libros_colegios')->where('libro_id', $this->libro_id)->where('colegio_id', $this->colegio->id)->delete();
                $libro_colegio = LibrosColegios::where('libro_id', $this->libro_id)->where('colegio_id', $this->colegio->id)->where('paquete_id', $this->paquete_id)->first();;

                if (!is_null($libro_colegio)) {
                    $libro_colegio->forceDelete();
                }
            } else {
                $libro_colegio = LibrosColegios::where('libro_id', $this->libro_id)->where('colegio_id', $this->colegio->id)->where('paquete_id', $this->paquete_id)->first();

                if (!is_null($libro_colegio)) {
                    $libro_colegio->forceDelete();
                }
            }
            activity()
                ->performedOn($libro)
                ->causedBy(Auth::user()->id)
                ->log('Eliminó el libro ' . $libro->isbn . ' del colegio ' . $this->colegio->codigo);
            return redirect()->to('admin/lista_libros_colegio/' . $this->colegio->id);
        }
    }
}
