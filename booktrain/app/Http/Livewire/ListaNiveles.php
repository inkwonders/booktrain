<?php

namespace App\Http\Livewire;

use App\Models\NivelColegio;
use Livewire\Component;

class ListaNiveles extends Component
{
    public $seccion;
    public $niveles;

    protected $listeners = ['refreshListaNiveles'];

    public function refreshListaNiveles() {
        $this->refresh();
    }

    function mount() {
        $this->refresh();
    }

    public function editNivel($nivel_id) {
        $this->emit('editNivel', $nivel_id);
    }

    public function delete(NivelColegio $nivel) {
        $nivel->delete();
        $this->emit('refreshListaNiveles');
    }

    public function refresh() {
        $this->niveles = $this->seccion->niveles;
    }

    public function render()
    {
        return view('livewire.lista-niveles');
    }
}
