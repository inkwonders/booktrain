<?php

namespace App\Http\Livewire;

use App\Models\TicketUsuario;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
// use Illuminate\Support\Facades\Auth;

class Tikets extends DataTableComponent
{
    // private $usuario = Auth::class;
    public $model = TicketUsuario::class;

    public string $defaultSortColumn = 'id';
    public string $defaultSortDirection = 'desc';

    function columns(): array
    {
        return [
            Column::make('#', 'id')
                ->sortable()
                ->searchable(),
            Column::make('ESTATUS', 'status')
                ->sortable(),
                // ->searchable(),
            Column::make('TÍTULO', 'titulo')
                ->sortable()
                ->searchable(),
            Column::make('MOTIVO', 'ticket_motivos_id')
                ->sortable()
                ->searchable(),
            Column::make('FECHA ÚLTIMA ENTRADA', 'updated_at')
                ->sortable()
                ->searchable(),
            Column::make('USUARIO ÚLTIMA ENTRADA', 'usuario_id')
                ->sortable()
                ->searchable(),
            Column::make('ACCIONES')
        ];
    }

    public function query(): Builder
    {
        $usuario = Auth::user();

        if($usuario->hasRole('Admin'))
            return TicketUsuario::query();
        else
            return TicketUsuario::where('usuario_id', $usuario->id);
    }

    public function rowView(): string
    {
        return ('livewire.tikets');
    }
}
