<?php

namespace App\Http\Livewire;

use Livewire\WithFileUploads;
use App\Models\Colegio as ModelsColegio;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\SeccionColegio;
use Livewire\Component;

class ColegioEditando extends Component
{
    use WithFileUploads;

    public ModelsColegio $colegio;

    public $logo;
    public $costo_envio;

    protected $listeners = ['refreshColegio' => '$refresh'];

    protected $rules = [
        'colegio.codigo' => 'required',
        'colegio.nombre' => 'required',
        'colegio.activo' => 'required',
        'costo_envio'   => 'required'
    ];

    public function delete()
    {
        $this->colegio->delete();
        $this->emit('refreshList');
    }

    public function mount()
    {
        $this->costo_envio = $this->colegio->config('costo_envio');
    }

    public function save()
    {
        $this->validate();
        $configuracion = $this->colegio->configuracion;
        $configuracion->costo_envio = $this->costo_envio;
        $this->colegio->configuracion = $configuracion;
        $this->colegio->save();

        if ($this->logo) {
            $this->colegio->logo = '/storage/' . $this->logo->getClientOriginalName();
            $this->logo->storeAs('public', $this->logo->getClientOriginalName());
        }

        $this->colegio->save();
    }

    public function render()
    {
        return view('livewire.colegio-editando');
    }
}
