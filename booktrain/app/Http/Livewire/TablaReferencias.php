<?php

namespace App\Http\Livewire;

use App\Models\ReferenciaPedido;
use Livewire\Component;


class TablaReferencias extends Component
{
    public $inicio;
    public $fin;

    protected $rules = [
        'inicio' => 'required',
        'fin' => 'required',
    ];

    public function render()
    {
        return view('livewire.tabla-referencias');
    }

}
