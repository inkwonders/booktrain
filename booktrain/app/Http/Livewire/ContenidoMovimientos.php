<?php

namespace App\Http\Livewire;

// use Livewire\Component;
use App\Models\MovimientoAlmacen;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ContenidoMovimientos extends DataTableComponent
{
    public $model = MovimientoAlmacen::class;

    public function columns(): array
    {
        return [
            Column::make('Fecha', 'created_at')
                ->sortable()
                ->searchable(),
            Column::make('Tipo', 'tipo.descripcion')
                // ->sortable()
                ->searchable(),
            Column::make('Usuario', 'usuario.name')
                // ->sortable()
                ->searchable(),
            Column::make('Cantidad', 'cantidad')
                ->sortable()
                ->searchable(),
            Column::make('Libro', 'libro.nombre')
                // ->sortable()
                ->searchable(),
            Column::make('Comentarios', 'comentarios')
                ->sortable()
                ->searchable(),
            Column::make('Descripción', 'descripcion')
                ->sortable()
                ->searchable(),
        ];
    }

    // Se elimina id libro y cantidad anterior, se ignora el tipo de movimiento 7 y se muestra la fecha

    public function query(): Builder
    {
        return MovimientoAlmacen::query()
            ->whereNotIn('tipo_movimiento_almacen_id', ['7'])
            ->orderByDesc('created_at');
    }

    public function rowView() : string
    {
        return 'livewire.contenido-movimientos';
    }
}
