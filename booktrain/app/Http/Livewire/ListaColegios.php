<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class ListaColegios extends Component
{
    protected $listeners = ['refreshListaColegios', 'onSwalResult'];

    public $colegio;
    public $colegios;
    public $filtro;

    public function mount()
    {
        $this->refreshListaColegios();
    }

    public function metodos_envio_activos($colegio)
    {
        $metodos_activos = 0;
        $envio_domicilio = $colegio->configuraciones()->where('etiqueta', 'envio_domicilio_disponible')->first()->valor;
        if ($envio_domicilio)
            $metodos_activos = 1;

        $metodos_envio = $colegio->direccionesEntrega->count();
        $metodos_activos = $metodos_activos + $metodos_envio;
        return $metodos_activos;
    }

    public function refreshListaColegios()
    {
        $this->colegios = Colegio::where('nombre', 'like', '%' . $this->filtro . '%')->get();
    }

    public function delete(Colegio $colegio)
    {
        $this->emit('swalAlert', [
            'title' => 'Eliminar colegio',
            'text' => "¿Desea eliminar el colegio {$colegio->nombre}?",
            'icon' => 'question',
            'showCancelButton' => true,
            'cancelButtonText' => 'Cancelar',
            'confirmButtonText' => 'Eliminar',
            'confirmButtonColor' => '#eb4343'
        ], ['deleteColegio' => $colegio->id] );
    }

    public function onSwalResult($result, $callback) {
        if($result['isConfirmed'] == true && isset($callback)) {
            $colegio = Colegio::find($callback['deleteColegio']);
            $codigo = $colegio->codigo;

            try {
                if ($colegio && sizeof($colegio->catalogoLibros()->get()) >= 1
                || sizeof($colegio->pedidos()->get()) >= 1
                || sizeof($colegio->secciones()->get()) >= 1
                || sizeof($colegio->libros()->get()) >= 1
                || sizeof($colegio->notificaciones()->get()) >= 1
                || sizeof($colegio->niveles()->get()) >= 1
                || sizeof($colegio->direccionesEntrega()->get()) >= 1) {
                    $this->emit('swalAlert', ['title' => 'Error al eliminar', 'text' => "El colegio {$codigo} no puede ser eliminado debido a que tiene secciones o pedidos asociados.", 'icon' => 'error']);
                } else {
                    $colegio->delete();

                    activity()
                    ->performedOn($colegio)
                    ->causedBy(Auth::user()->id)
                    ->log('Eliminó un colegio: ' . $codigo);

                    $this->emit('swalAlert', ['title' => 'Colegio borrado', 'text' => "Se eliminó correctamente al colegio {$codigo}", 'icon' => 'success']);

                    $this->refreshListaColegios();
                }
            } catch (\Throwable $th) {
                Log::error('ListaColegios::deleteColegio', [$th]);
                $this->emit('swalAlert', ['title' => 'Error al eliminar', 'text' => "Error desconocido: {$th->getMessage()}", 'icon' => 'error']);
            }
        }
    }

    public function render()
    {
        return view('livewire.lista-colegios');
    }
}
