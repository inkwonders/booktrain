<?php

namespace App\Http\Livewire;

use App\Models\Pedido;
use Livewire\Component;

class ListaPedidos extends Component
{
    public $pedidos;
    protected $listeners = ['refreshListaPedidos'];

    public function mount() {
        $this->refreshListaPedidos();
    }

    public function procesar(Pedido $pedido) {
        $pedido->update(['status' => 'PROCESADO']);
        $pedido->procesar(false);

        $this->dispatchBrowserEvent('actualizarListaPedidos');
    }

    public function refreshListaPedidos() {
        $this->pedidos = Pedido::all();
    }

    public function render()
    {
        return view('admin.pedidos.index');
    }
}
