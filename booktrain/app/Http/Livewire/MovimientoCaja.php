<?php

namespace App\Http\Livewire;

use App\Models\Caja;
use App\Models\Colegio;
use Livewire\Component;
use App\Models\OperacionCaja;
use Illuminate\Support\Facades\Log;

class MovimientoCaja extends Component
{
    public $usuario;
    public $colegio;
    public $caja;
    public $operacion_caja;
    public $observaciones;

    protected $listeners = [
        'onSwalResult'
    ];

    public function onSwalResult($result, $callbackID) {
        switch($callbackID) {
            case 'cb_cierre':
                return redirect()->route('venta_movil');
            case 'cb_apertura':
                return redirect()->route('venta_movil.movimiento_caja');
        }
    }

    protected $rules = [
        'operacion_caja.billete_20'     => 'required|int|min:0',
        'operacion_caja.billete_50'     => 'required|int|min:0',
        'operacion_caja.billete_100'    => 'required|int|min:0',
        'operacion_caja.billete_200'    => 'required|int|min:0',
        'operacion_caja.billete_500'    => 'required|int|min:0',
        'operacion_caja.billete_1000'   => 'required|int|min:0',
        'operacion_caja.conteo_monedas' => 'required|int|min:0',
        'operacion_caja.monto_monedas'  => 'required|int|min:0',
        'operacion_caja.observaciones'  => 'required|string'
    ];

    public function mount()
    {
        $this->usuario = Auth()->user();
        $this->colegio = Colegio::whereIn('codigo', $this->usuario->permissions->pluck('name'))->first();

        // Este campo es nulo antes de guardar la caja por que se define desde la estructura de la tabla
        $this->caja = $this->usuario->cajas()->firstOrNew([
            'colegio_id' => $this->colegio->id,
            'user_id' => $this->usuario->id,
            'status' => 'ABIERTA'
        ]);

        $this->operacion_caja = new OperacionCaja;
    }

    public function save()
    {
        try {
            if($this->caja->exists) {
                // Caja existente, Cierre
                $this->caja->update([
                    'status' => 'CERRADA',
                    'fecha_cierre' => date("Y-m-d H:i:s")
                ]);

                $this->operacion_caja->tipo = 'CIERRE';

                $this->caja->operaciones()->save($this->operacion_caja);

                $this->emit('swalAlert', [
                    'title' => 'Caja cerrada',
                    'text' => 'La operación de cierre se realizó exitosamente',
                    'icon' => 'success'
                ], 'cb_cierre');
            }else{
                $this->caja->colegio_id = $this->colegio->id;
                $this->caja->user_id = $this->usuario->id;
                $this->caja->status = 'ABIERTA';

                // Caja nueva: Apertura
                $this->caja->save();
                $this->caja->touch();

                $this->operacion_caja->tipo = 'APERTURA';

                $this->caja->operaciones()->save($this->operacion_caja);

                $this->emit('swalAlert', [
                    'title' => 'Caja abierta',
                    'text' => 'La caja fue abierta exitosamente, ahora puede recibir operaciones de venta',
                    'icon' => 'success'
                ], 'cb_apertura');
            }
        } catch (\Throwable $th) {
            Log::error("LivewireMovimientoCaja", [$th]);
            $this->emit('swalAlert', [
                'title' => 'Oops',
                'text' => 'No se pudo realizar la operación, reintente más tarde',
                'icon' => 'error'
            ]);
        }
    }

    public function render()
    {
        return view('livewire.movimiento-caja');
    }
}
