<?php

namespace App\Http\Livewire;

use App\Models\Colegio;
use App\Models\Notificacion;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class EditarAviso extends Component
{
    public Notificacion $notificacion;
    public Colegio $colegio;

    protected $rules = [
        'notificacion.titulo' => 'required',
        'notificacion.aviso' => 'required',
        'notificacion.activo' => 'sometimes'
    ];

    public function mount(Notificacion $notificacion, Colegio $colegio)
    {
        $this->notificacion = $notificacion;
        $this->colegio = $colegio;
    }

    public function save()
    {
        $this->notificacion->activo = boolval($this->notificacion->activo);

        $this->colegio->notificaciones()->save(
            $this->notificacion
        );
        $this->dispatchBrowserEvent('editar_correcto');

        activity()
            ->performedOn($this->notificacion)
            ->causedBy(Auth::user()->id)
            ->log("Editó el aviso {$this->notificacion->titulo} del colegio {$this->colegio->codigo}");
    }

    public function render()
    {
        return view('livewire.editar-aviso');
    }
}
