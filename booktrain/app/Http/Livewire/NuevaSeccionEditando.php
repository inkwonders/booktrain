<?php

namespace App\Http\Livewire;

use App\Models\SeccionColegio;
use App\Models\NivelColegio;
use App\Models\Colegio;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NuevaSeccionEditando extends Component
{
    public SeccionColegio $seccion;
    public Colegio $colegio;

    public $nuevo_nivel = '';
    public $niveles = [];

    protected $listeners = ['editSeccion'];

    protected $rules = [
        'seccion.nombre' => 'required',
        'niveles' => 'required|min:1',
        'seccion.activo' => 'sometimes'
    ];

    public function mount()
    {
        $this->seccion = new SeccionColegio;
        $this->seccion->activo = 1;
    }

    public function editSeccion($id_seccion)
    {
        $this->seccion = SeccionColegio::find($id_seccion);
    }

    public function save()
    {
        $this->validate();

        if($this->seccion->activo == null)
            $this->seccion->activo = 0;

        $this->colegio->secciones()
            ->save($this->seccion);
        $niveles_colegio = array_map(function ($nivel) {
            return [
                'nombre' => $nivel,
                'activo' => 1
            ];
        }, $this->niveles);

        $this->seccion->niveles()->createMany(
            $niveles_colegio
        );

        $this->dispatchBrowserEvent('agregar_correcto');
        $seccion = new SeccionColegio;
        activity()
            ->performedOn($seccion)
            ->causedBy(Auth::user()->id)
            ->log('Creó una nueva sección: "' . $this->seccion->nombre . '" para el colegio ' . $this->colegio->codigo);


        $this->seccion = new SeccionColegio;
        $this->niveles = [];
        $this->nuevo_nivel = '';
    }

    public function quitarNivel($id)
    {
        array_splice($this->niveles, $id, 1);
    }

    public function guardarNivel()
    {
        $this->niveles[] = $this->nuevo_nivel;
        $this->nuevo_nivel = '';
    }

    public function render()
    {
        return view('livewire.nueva-seccion-editando');
    }
}
