<?php

namespace App\Http\Livewire;

use App\Models\NivelColegio;
use App\Models\Paquete;
use App\Models\SeccionColegio;
use Livewire\Component;

class NuevoPaquete extends Component
{
    public NivelColegio $nivel;
    public SeccionColegio $seccion;
    public Paquete $paquete;

    protected $listeners = ['editPaquete'];

    public function editPaquete(Paquete $paquete) {
        $this->paquete = $paquete;
    }

    function mount() {
        $this->paquete = new Paquete;
    }

    protected $rules = [
        'paquete.nombre' => 'required',
        'paquete.activo' => 'required'
    ];

    public function save() {
        $this->validate();
        $this->nivel->paquetes()->save($this->paquete);

        $this->paquete = new Paquete;

        $this->emit('refreshListaPaquetes');
        $this->emit('refreshListaNiveles');
    }

    public function render()
    {
        return view('livewire.nuevo-paquete');
    }
}
