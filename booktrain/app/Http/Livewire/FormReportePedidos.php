<?php

namespace App\Http\Livewire;

use Livewire\Component;

class FormReportePedidos extends Component
{
    public $model = Pedido::class;

    public $formData;
    public $status;
    public $factura;
    public $metodo_pagos;
    public $forma_pagos;
    public $forma_entrega;
    public $form_search;
    public $inicio;
    public $fin;


    public function inicio($inicio){
        $this->inicio = $inicio;
        $this->emit('inicio', $inicio);
      }


    public function render()
    {
        return view('livewire.form-reporte-pedidos');
    }


}
