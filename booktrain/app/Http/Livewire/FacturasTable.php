<?php

namespace App\Http\Livewire;

use App\Exports\DetalleResumenFacturas;
use App\Models\Pedido;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use App\Models\RegistroPago;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Builder;
use App\Exports\DetalleResumenPedidosReferencias;
use App\Models\DatosFacturas;
use AWS\CRT\HTTP\Request;
use Exception;
use Facturama\Exception\RequestException;
use Illuminate\Support\Facades\Storage;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;

class FacturasTable extends DataTableComponent
{
    protected $listeners = [
        'setFecha',
        'onSwalResult'

    ];

    public $model = DatosFacturas::class;

    public $table_headers = 'private.admin.header_facturas';

    public $searchable = "Buscar";
    public $inicio;
    public $fin;
    public $modal_reorden = false;
    public $error_facturama, $modal_facturama = false;

    public $modal_factura = false, $modal_factura_datos;
    public $modal_descarga = false, $modal_descarga_documento;
    public $modal_cancelar = false, $modal_cancelar_documento, $razon_cancelacion, $pedido_id, $cancelacion_exitosa = false;

    public function mount() {
        $this->inicio = date('Y-m-d');
        $this->fin = date('Y-m-d');
    }

    public function setFecha($which, $what) {
        switch ($which) {
            case 'inicio': $this->inicio = $what; break;
            case 'fin': $this->fin = $what; break;
        }
    }

    public function exportar() {
        return Excel::download(new DetalleResumenFacturas(
            $this->inicio,
            $this->fin
        ), "Reporte de Facturas de {$this->inicio} a {$this->fin}.xlsx");
    }

    function columns(): array
    {
        return [

            Column::make('Timbrado', 'pedidos.facturama_invoice_id')->sortable(),
            Column::make('Referencia' , 'pedidos.referencia_factura')->sortable(),
            Column::make('Folio', 'datos_facturas.pedido_id')->sortable(),
            Column::make('Estatus', 'datos_facturas.pedido_id')->sortable(),
            Column::make('Razón Social', 'datos_facturas.razon_social')->sortable()->searchable(),
            Column::make('RFC', 'datos_facturas.rfc')->sortable()->searchable(),
            Column::make('Correo', 'datos_facturas.correo')->sortable()->searchable(),
            Column::make('Direccion', 'datos_facturas.calle')->sortable(),
            Column::make('cp', 'datos_facturas.cp')->sortable()->searchable(),
            Column::make('CFDI', 'datos_facturas.id_cfdi')->sortable()->searchable(),
            Column::make('Metodo Pago', 'datos_facturas.metodo_pago_id')->sortable(),
            Column::make('Forma Pago', 'datos_facturas.forma_pago_id')->sortable(),
            Column::make('Fecha', 'datos_facturas.created_at')->sortable()->searchable(),
            Column::make('Descargas', ''),

        ];
    }

    public function onSwalResult($result, $extras) {

        if(isset($extras) && $result['isConfirmed'] == true && isset($extras['pedido_id']))
            try {
                DB::beginTransaction();

                $pedido = Pedido::find($extras['pedido_id']);

                switch ($extras['action']) {
                    case 'enviarCorreoConfirmacionPago':
                        $pedido->sendPagoExitosoNotification();

                        $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El correo se ha enviado satisfactoriamente', 'icon' => 'success']);

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Reenvió el correo de confirmación del pedido: '{$pedido->serie}-{$pedido->folio}'");
                    break;

                    case 'asignarReferenciaFactura':
                        $pedido->update(['referencia_factura' => $result['value']]);

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Se asigno la factura '{$pedido->referencia_factura}' al pedido {$pedido->serie}-{$pedido->folio}");

                            $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'La referencia se actualizó correctamente', 'icon' => 'success']);
                    break;

                    case 'cambiarEstadoPedido':
                        $estado_anterior = $pedido->status;

                        switch($pedido->status) {
                            // Pasa a CANCELADO o PAGADO
                            case 'PROCESADO':
                                switch ($result['value']) {
                                    case 'CANCELADO':
                                        $pedido->cancelar();

                                        // Si hay una referencia de pago se maraca como NO PAGADA
                                        $pedido->referencia()->update(['pagado' => 0]);
                                    break;

                                    case 'PAGADO':
                                        // Marcamos el pedido como pagado
                                        $pedido->update(['status' => 'PAGADO']);

                                        // Si hay una referencia, se marca esta última como pagada
                                        $pedido->referencia()->update(['pagado' => 1]);

                                        // Si no hay pagos exitosos, se crea un registro de pago exitoso MANUAL
                                        if( ! $pedido->pagos()->exitoso()->first())
                                            $pedido->pagos()->create([
                                                'amount'             => $pedido->total,
                                                'source'             => 'Registro de pago de forma manual',
                                                'status'             => 'SUCCESS',
                                                'transactionTokenId' => auth()->user()->id,
                                                'raw'                => auth()->user()->email,
                                                'origen'             => 'MANUAL',
                                            ]);
                                    break;
                                }
                            break;

                            // Pasa a CANCELADO
                            case 'PAGADO':
                                $pedido->cancelar();

                                // Si hay una referencia de pago se maraca como NO PAGADA
                                $pedido->referencia()->update(['pagado' => 0]);
                            break;

                            // Pasa a Procesado
                            case 'CANCELADO':
                                $pedido->update([
                                    'status' => 'PROCESADO'
                                ]);

                                $pedido->procesar();
                            break;
                        }

                        activity()
                            ->performedOn($pedido)
                            ->causedBy(Auth::user()->id)
                            ->log("Se cambio el estado del pedido {$pedido->serie}-{$pedido->folio} de {$estado_anterior} a {$pedido->status}");

                        $this->emit('swalAlert', ['title' => 'Exito', 'text' => 'El estado del pedido se actualizó correctamente', 'icon' => 'success']);
                    break;
                }

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                Log::error('Ocurrió un error al procesar el callback en el componente de livewire Pedidos@onSwalResult', ['result' => $result, 'extras' => $extras, 'linea' => $th->getLine(), 'message' => $th->getMessage()]);

                $this->emit('swalAlert', [
                    'title' => 'Error en la operación',
                    'text' => "Ocurrió un error al realizar la operación: {$th->getMessage()}, reintente más tarde.",
                    'icon' => 'error'
                ]);
            }
    }

    private function getInstance()
    {

        try {


            Log::debug('FacturamaController@getInstance', ['Conexion con Facturama']);

            $production=boolval(env('FACTURAMA_DEVELOPMENT'));

            $facturama = new \Facturama\Client(env('FACTURAMA_USERNAME'), env('FACTURAMA_PASSWORD'));

            if($production){

                $url_prod=env('FACTURAMA_INVOICE_URL');

                 $facturama->setApiUrl($url_prod);

            }

                //   dd($facturama);

            return $facturama;

        } catch (RequestException $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);

        } catch (Exception $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);
        }
    }

    public function imprimir($accion,$id_facturama){


        $this->modal_descarga = true;

        try{

            $id=$id_facturama;

            switch ($accion) {
                case 1:

                    $format='pdf';

                break;

                case 2:

                    $format='xml';

                break;

                case 3:

                    $format='acuse';

                break;

            }

            if($format=='acuse'){

                $dir="acuse/";

                $format='pdf';

                $nombre_archivo=$id.'_acuse.'.$format;

             }else{

                $dir="cfdi/";  $nombre_archivo=$id.'.'.$format;

            }

            $facturama = $this->getInstance();

            $type = env('FACTURAMA_INVOICE_TYPE');

            $params = [];

            $result = $facturama->get($dir.$format.'/'.$type.'/'.$id, $params);

            $this->modal_descarga_documento = $nombre_archivo;

            Storage::disk('public')->put('xml_pdf/'.$nombre_archivo, base64_decode(end($result)));

         } catch (Exception $e) {

            Log::debug('FacturamaController@getInvoiceFiles', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInvoiceFiles ERROR on the transaction Exception: ',$array);

            return response()->json($array);

        }

    }


    public function download(){

        $this->modal_descarga = false;

        return Storage::disk('public')->download('xml_pdf/'.$this->modal_descarga_documento);

    }

    public function getInvoiceFiles(Request $request){



        Log::debug('FacturamaController@generateCfdi', ['Obtenemos datos'=> $request->all()]);


                        $request->validate([

                            'facturama_id'  => 'required|exists:pedidos,facturama_invoice_id',

                            'format'       =>  'in:xml,pdf'

                        ]);


         try{

            $id=$request->facturama_id;

            $format=$request->format;

            $facturama= $this->getInstance();

            $type = env('FACTURAMA_INVOICE_TYPE');

            $params = [];

            $result = $facturama->get('cfdi/'.$format.'/'.$type.'/'.$id, $params);

            // dd($result);

            // dd(storage_path('app/xml_pdf/'.$id.'.'.$format));

           $myfile = fopen(storage_path('app/xml_pdf/').$id.'.'.$format, 'a+');

            fwrite($myfile, base64_decode(end($result)));

            fclose($myfile);

            printf('<pre>%s<pre>', var_export(true));

         } catch (Exception $e) {

            Log::debug('FacturamaController@getInvoiceFiles', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInvoiceFiles ERROR on the transaction Exception: ',$array);

            return response()->json($array);

        }

    }

    public function timbrado($accion,  $pedido){

        $pedido = DatosFacturas::find($pedido);

        if($accion == 1){

            $this->modal_facturama = true;

            $this->error_facturama = $pedido->facturama_error;

        }else{

            $this->modal_facturama = false;

            $this->error_facturama = '';

        }


    }

    public function asignarReferenciaFactura(Pedido $pedido) {

        $this->emit('swalAlert', [
            'title'             => 'Referencia de factura',
            'text'              => "Introduzca la referencia de facturación del pedido {$pedido->serie}{$pedido->folio} del día {$pedido->created_at->format('d-m-Y')} con el RFC {$pedido->datosFactura->rfc}",
            'icon'              => 'question',
            'showCancelButton'  => true,
            'confirmButtonText' => 'Asignar',
            'cancelButtonText'  => 'Cancelar',
            'input'             => 'text',
            'inputPlaceholder'  => 'Referencia de facturación',
            'inputValue'        => $pedido->referencia_factura
        ], [
            'action' => 'asignarReferenciaFactura',
            'pedido_id' => $pedido->id
        ]);
    }

    public function cancelar($pedido){

        $this->modal_cancelar = true;

        $this->pedido_id  = $pedido;

    }

    public function delete($pedido_id){

        try {

        DB::beginTransaction();

        $pedido_id=$pedido_id;

        $pedido=Pedido::find($pedido_id);

        $id=$pedido->facturama_invoice_id;

        $facturama= $this->getInstance();

        $motive=$this->razon_cancelacion;

        $params =

            [

                'motive'=>$motive,

                'uuidReplacement'=>'null'

            ];

        $ruta=env('FACTURAMA_INVOICE_CANCELLATION');


        $result = $facturama->delete($ruta.$id, $params);

        //actualizamos el pedido

        $pedido->facturama_status=$result->Status;

        $invoice_data=json_encode($result);

        $pedido->facturama_cancellation_response=$invoice_data;

        $pedido->save();

        DB::commit();

        $this->cancelacion_exitosa = true;

        } catch (RequestException $e) {

            Log::debug('FacturamaController@deleteInvoice', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@deleteInvoice ERROR on the transaction Exception: ',$array);

            return response()->json($array);

        }

    }

    public function query(): Builder
    {
        $query = DatosFacturas::query()
                ->select(
                    'datos_facturas.id',
                    'datos_facturas.pedido_id',
                    'datos_facturas.user_id',
                    'datos_facturas.razon_social',
                    'datos_facturas.rfc',
                    'datos_facturas.regimen_fiscal',
                    'datos_facturas.correo',
                    'datos_facturas.cp',
                    'datos_facturas.calle',
                    'datos_facturas.num_exterior',
                    'datos_facturas.num_interior',
                    'datos_facturas.colonia',
                    'datos_facturas.municipio',
                    'datos_facturas.estado',
                    'datos_facturas.forma_pago',
                    'datos_facturas.metodo_pago',
                    'datos_facturas.id_cfdi',
                    'datos_facturas.metodo_pago_id',
                    'datos_facturas.forma_pago_id',
                    'datos_facturas.facturama_error',
                    'datos_facturas.facturama_error',
                    'datos_facturas.created_at',
                )
                ->when($this->inicio, function($query, $inicio) {
                    $query->where('datos_facturas.created_at', '>=', "{$inicio} 00:00:00");
                })
                ->when($this->fin, function($query, $fin) {
                    $query->where('datos_facturas.created_at', '<=', "{$fin} 23:59:59");
                })
                ->join('pedidos', 'pedidos.id', '=', 'datos_facturas.pedido_id')
                // ->join('pedidos', 'datos_facturas.pedido_id', '=', 'pedidos.id')
            ->orderBy('datos_facturas.created_at', 'DESC');

        return $query;
    }

    public function rowView(): string {
        return ('private.admin.facturas.table');
    }


    // public function render()
    // {
    //     return view('private.admin.facturas.table');
    // }
}
