<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

class ListaUsuarios extends Component
{
    // protected $listeners = ['refreshList'];
    public $usuarios;
    public $filtro;
    public $nombre_completo;
    // public $user_role;

    public function mount()
    {
        $this->refreshList();
    }
    public function refreshList()
    {
        $this->usuarios = User::where('name', 'like', '%' . $this->filtro . '%')->get();
    }

    public function verificar(User $usuario)
    {
        $usuario->verificarCuenta();
        $this->dispatchBrowserEvent('actualizarListaUsuarios');
        // $this->emit('refreshList');
    }

    public function activo(User $usuario)
    {
        if ($usuario->activo == 1) {
            $usuario->activo = 0;
        } else {
            $usuario->activo = 1;
        }
        $usuario->save();
        $this->dispatchBrowserEvent('actualizarListaUsuarios');
    }

    public function render()
    {
        return view('livewire.lista-usuarios');
    }
}
