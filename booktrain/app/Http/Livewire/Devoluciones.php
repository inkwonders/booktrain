<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Devoluciones extends Component
{
    public function render()
    {
        return view('livewire.devoluciones');
    }
}
