<?php

namespace App\Http\Livewire;

// use Livewire\Component;
use App\Models\ReferenciaPedido;
use App\Models\Pedido;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class ReferenciasFiltrado extends DataTableComponent
{
    public $model = ReferenciaPedido::class;

    public $select_inicio;
    public $select_fin;

    public function mount(){
        $this->select_inicio = date("Y-m-d");
        $this->select_fin = date("Y-m-d");
    }

    protected $listeners = [
        'inicio',
        'fin'
    ];

    public function inicio($select_inicio) {
        $this->select_inicio = $select_inicio;
    }
    public function fin($select_fin) {
        $this->select_fin = $select_fin;
    }

    function columns(): array
    {
        return [
            Column::make('Folio')
                ->sortable()
                ->searchable(),
            Column::make('Status')
                ->sortable()
                ->searchable(),
            Column::make('Nombre', 'nombre_contacto')
                ->sortable()
                ->searchable(),
            Column::make('Apellido', 'apellidos_contacto')
                ->sortable()
                ->searchable(),
            Column::make('Celular', 'celular_contacto')
                ->sortable()
                ->searchable(),
            Column::make('Referencia', 'referencia.referencia')
                // ->sortable()
                ->searchable(),
            Column::make('Fecha de pago', 'referencia.fecha_pago')
                // ->sortable()
                ->searchable(),
            Column::make('Importe total', 'total')
                ->sortable()
                ->searchable(),
        ];
    }

    /**
     * Debe mostrara los pedidos que tienen una referencia de pago de tipo banco y cuyo status es SUCCSSS
     */
    public function query(): Builder
    {
        $referencias = Pedido::query()
        ->whereHas('referencia', function($query) {
            $query->where('tipo', 'BANCARIA');
            $query->where('pagado', 1);
            if($this->select_inicio != '') {
                $query->where('fecha_pago', '>=', $this->select_inicio.' 00:00:00');
            }
            if($this->select_fin != '') {
                $query->where('fecha_pago', '<=', $this->select_fin.' 23:59:59');
            }
        })
        ->whereHas('pagos', function($query) {
            $query->where('status', 'SUCCESS');
        })->with([
            'referencia' => function($query) {
                $query->where('tipo', 'BANCARIA');
                $query->where('pagado', 1);
            },
            'pagos' => function($query) {
                $query->where('status', 'SUCCESS');
            }
        ]);

        return $referencias;
    }

    public function rowView(): string
    {
        return ('livewire.referencias-filtrado');
    }
}
