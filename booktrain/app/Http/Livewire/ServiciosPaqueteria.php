<?php

namespace App\Http\Livewire;

use App\Models\ServicioPaqueteria;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ServiciosPaqueteria extends Component
{
    public ServicioPaqueteria $nueva_paqueteria;
    public $servicios_paqueteria;
    public $editando = false;

    protected $listeners = [
        'editarServicioPaqueteria'
    ];

    public function editarServicioPaqueteria(ServicioPaqueteria $paqueteria)
    {
        $this->nueva_paqueteria = $paqueteria;
        $this->editando = true;
    }

    public function mount()
    {
        $this->nueva_paqueteria = new ServicioPaqueteria();
        $this->actualizaLista();
    }

    public function actualizaLista()
    {
        $this->servicios_paqueteria = ServicioPaqueteria::all();
    }

    protected $rules = [
        'nueva_paqueteria.nombre' => 'required',
        'nueva_paqueteria.url' => 'required',
        'nueva_paqueteria.activo' => 'sometimes|required'
    ];

    public function save()
    {
        $this->validate();
        $this->nueva_paqueteria->save();
        $paqueteria = new ServicioPaqueteria();
        if ($this->editando) {
            activity()
                ->performedOn($paqueteria)
                ->causedBy(Auth::user()->id)
                ->log('Editó la paquetería "' . $this->nueva_paqueteria->nombre . '"');
        } else {
            activity()
                ->performedOn($paqueteria)
                ->causedBy(Auth::user()->id)
                ->log('Agregó una nueva paquetería "' . $this->nueva_paqueteria->nombre . '"');
        }
        $this->nueva_paqueteria = new ServicioPaqueteria();
        $this->actualizaLista();
        $this->editando = false;
    }

    public function render()
    {
        return view('livewire.servicios-paqueteria');
    }
}
