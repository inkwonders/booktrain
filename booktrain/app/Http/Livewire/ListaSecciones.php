<?php

namespace App\Http\Livewire;

use App\Models\SeccionColegio;
use Livewire\Component;

class ListaSecciones extends Component
{
    public $secciones;
    public $colegio;

    protected $listeners = ['refreshSecciones'];

    public function refreshSecciones() {
        $this->secciones = $this->colegio->secciones;
    }

    public function mount() {
        $this->refreshSecciones();
    }

    public function delete(SeccionColegio $seccion) {
        /*$seccion->niveles()->each(function($nivel) {
            $nivel->paquetes()->libros()->each(function($libro) {
                $libro->detach();
            });
            $nivel->paquetes()->delete();
            $nivel->delete();
        });*/
        $seccion->delete();

        $this->emit('refreshSecciones');
    }

    public function render()
    {
        return view('livewire.lista-secciones');
    }
}
