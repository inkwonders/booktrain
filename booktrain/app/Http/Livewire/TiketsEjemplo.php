<?php

namespace App\Http\Livewire;

// use Livewire\Component;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class TiketsEjemplo extends DataTableComponent
{
    public $model = User::class;
    public function roles($id)
    {
        return User::where('id', $id)->with('roles')->first();
    }
    public function verificar(User $usuario)
    {
        $usuario->verificarCuenta();
    }

    public function activo(User $usuario)
    {
        if ($usuario->activo == 1) {
            $usuario->activo = 0;
        } else {
            $usuario->activo = 1;
        }
        $usuario->save();
    }

    function columns(): array
    {
        return [
            Column::make('nombre', 'name')
                ->sortable()
                ->searchable(),
            Column::make('correo', 'email')
                ->sortable()
                ->searchable(),
            Column::make('activo', 'activo')
                ->sortable(),
            Column::make('permisos')
                ->sortable(),
            Column::make('verificar', 'email_verified_at')
                ->sortable(),
            Column::make('editar')

        ];
    }
    public function query(): Builder
    {
        return User::query();
    }

    // public function render()
    public function rowView(): string
    {
        // return view('livewire.tikets-ejemplo');
        return ('livewire.tikets-ejemplo');
    }
}
