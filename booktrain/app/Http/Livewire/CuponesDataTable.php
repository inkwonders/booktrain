<?php

namespace App\Http\Livewire;

use App\Domains\User\Exports\UserExport;
use App\Models\Cupon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;

class CuponesDataTable extends DataTableComponent
{
    public function columns(): array
    {
        return [
            Column::make('Codigo', 'codigo')->searchable()->sortable(),
            Column::make('Status', 'cupon.status')->sortable(function(Builder $query, $direction) {
                return $query->orderBy('cupones.status', $direction);
            }),
            Column::make('Caducidad', 'cupon.caducidad')->searchable()->sortable(),
            Column::make('Valor', 'valor')->searchable()->sortable(),
            Column::make('Pedido', 'pedido.folio')->sortable(function(Builder $query, $direction) {
                return $query->orderBy('pedidos.folio', $direction);
            }),
            // Column::make('Fecha de uso', 'updated_at')->searchable()->sortable(),
            Column::make('Creación', 'created_at')->searchable()->sortable(),

        ];
    }

    public function query(): Builder
    {
        return Cupon::query()
            ->select(
                'cupones.id',
                'cupones.pedido_id',
                'cupones.codigo',
                'cupones.valor',
                'cupones.tipo',
                'cupones.status',
                'cupones.caducidad',
                'cupones.updated_at',
                'cupones.created_at',
                'pedidos.folio', 'pedidos.serie'
                )
            ->leftJoin('pedidos', 'pedidos.id', '=', 'cupones.pedido_id');
    }

    public function rowView(): string
    {
        return 'livewire.cupones-data-table';
    }
}
