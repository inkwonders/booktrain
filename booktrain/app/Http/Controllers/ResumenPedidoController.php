<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use GuzzleHttp\Psr7\Request;
use App\Models\ResumenPedido;
use Illuminate\Support\Facades\Auth;
use App\DataTables\ResumenPedidoDataTable;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ResumenPedidoRepository;
use App\Http\Requests\CreateResumenPedidoRequest;
use App\Http\Requests\UpdateResumenPedidoRequest;
use Illuminate\Support\Facades\Request as FacadesRequest;

class ResumenPedidoController extends AppBaseController
{
    /** @var  ResumenPedidoRepository */
    private $resumenPedidoRepository;

    public function __construct(ResumenPedidoRepository $resumenPedidoRepo)
    {
        $this->resumenPedidoRepository = $resumenPedidoRepo;
    }

    /**
     * Display a listing of the ResumenPedido.
     *
     * @param ResumenPedidoDataTable $resumenPedidoDataTable
     * @return Response
     */
    public function index(ResumenPedidoDataTable $resumenPedidoDataTable)
    {
        return $resumenPedidoDataTable->render('resumen_pedidos.index');
    }

    /**
     * Show the form for creating a new ResumenPedido.
     *
     * @return Response
     */
    public function create()
    {
        return view('resumen_pedidos.create');
    }

    /**
     * Store a newly created ResumenPedido in storage.
     *
     * @param CreateResumenPedidoRequest $request
     *
     * @return Response
     */
    public function store(CreateResumenPedidoRequest $request)
    {
        $input = $request->all();

        $resumenPedido = $this->resumenPedidoRepository->create($input);

        Flash::success('Resumen Pedido saved successfully.');

        return redirect(route('resumenPedidos.index'));
    }

    /**
     * Display the specified ResumenPedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            Flash::error('Resumen Pedido not found');

            return redirect(route('resumenPedidos.index'));
        }

        return view('resumen_pedidos.show')->with('resumenPedido', $resumenPedido);
    }

    /**
     * Show the form for editing the specified ResumenPedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            Flash::error('Resumen Pedido not found');

            return redirect(route('resumenPedidos.index'));
        }

        return view('resumen_pedidos.edit')->with('resumenPedido', $resumenPedido);
    }

    /**
     * Update the specified ResumenPedido in storage.
     *
     * @param  int              $id
     * @param UpdateResumenPedidoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResumenPedidoRequest $request)
    {
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            Flash::error('Resumen Pedido not found');

            return redirect(route('resumenPedidos.index'));
        }

        $resumenPedido = $this->resumenPedidoRepository->update($request->all(), $id);

        Flash::success('Resumen Pedido updated successfully.');

        return redirect(route('resumenPedidos.index'));
    }

    /**
     * Remove the specified ResumenPedido from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            Flash::error('Resumen Pedido not found');

            return redirect(route('resumenPedidos.index'));
        }

        $this->resumenPedidoRepository->delete($id);

        Flash::success('Resumen Pedido deleted successfully.');

        return redirect(route('resumenPedidos.index'));
    }

    public function editResumen(UpdateResumenPedidoRequest $request, $id)
    {

        $user = Auth::user();
        $usuario_id = $user->id;

        $resumenPedido = ResumenPedido::find($id);

        if(empty($resumenPedido)){
            $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
            return response()->json(['success' => false, 'msg' => 'No existe', 'data' => $pedidoInformacion], 404);
        }else{

            if($resumenPedido->pedido->usuario_id != $usuario_id){

                $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                return response()->json(['success' => false, 'msg' => 'No pertenece al usuario', 'data' => $pedidoInformacion], 403);

            }else{

                $resumenPedido->nombre_alumno = $request->nombre;
                $resumenPedido->paterno_alumno = $request->paterno;

                if(!empty($request->materno)){
                    $resumenPedido->materno_alumno = $request->materno;
                }

                try {
                    $resumenPedido->save();
                } catch (\Throwable $th) {
                    $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                    return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                }

                $resumenPedido->pedido->update([
                    'subtotal' => $resumenPedido->pedido->getSubtotalAttribute($resumenPedido->pedido->subtotal),
                    'total' => $resumenPedido->pedido->getTotalAttribute($resumenPedido->pedido->total),
                ]);

                $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                return response()->json(['success' => true, 'data' => $pedidoInformacion], 200);

            }

        }

    }

    public function deleteResumen($id)
    {
        $eliminaPedido = false;

        $user = Auth::user();
        $usuario_id = $user->id;

        $resumenPedido = ResumenPedido::find($id);

        if(empty($resumenPedido)){
            return response()->json(['success' => false, 'msg' => 'No existe'], 404);
        }else{

            if($resumenPedido->pedido->usuario_id != $usuario_id){
                $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                return response()->json(['success' => false, 'msg' => 'No pertenece al usuario', 'data' => $pedidoInformacion], 403);
            }else{
                $noResumenes = ResumenPedido::where('pedido_id','=',$resumenPedido->pedido_id)->count();
                $pedido = $resumenPedido->pedido;

                if ($pedido->status != 'CARRITO') {
                    $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                    return response()->json(['success' => false, 'msg' => 'No puedes eliminar información de un pedido PROCESADO o PAGADO', 'data' => $pedidoInformacion], 400);
                }

                /*if($noResumenes == 1){
                    $eliminaPedido = true;
                    // se elimina todo el pedido
                    try {
                        $resumenPedido->pedido->delete();
                    } catch (\Throwable $th) {
                        $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                        return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                    }
                }else{
                    // elimina solo este resumen
                    try {
                        $resumenPedido->detalles()->delete();
                        $resumenPedido->delete();

                    } catch (\Throwable $th) {
                        $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                        return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                    }
                }*/

                try {
                    $resumenPedido->detalles()->delete();
                    $resumenPedido->delete();
                } catch (\Throwable $th) {
                    $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                    return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                }

                if($noResumenes == 1){
                    $eliminaPedido = true;

                    if($pedido->status != 'PAGADO') {
                        $pedido->envio()->delete();
                    }

                    $pedido->delete();
                }
            }

            if($eliminaPedido){
                $data = array('carrito_vacio' => true);
                return response()->json(['success' => true, 'data' => $data], 200);
            }else{

                $resumenPedido->pedido->update([
                    'subtotal' => $resumenPedido->pedido->getSubtotalAttribute($resumenPedido->pedido->subtotal),
                    'total' => $resumenPedido->pedido->getTotalAttribute($resumenPedido->pedido->total),
                ]);

                $pedidoInformacion = PedidoController::datosCarrito($resumenPedido->pedido);
                return response()->json(['success' => true, 'data' => $pedidoInformacion], 200);
            }

        }

    }
}
