<?php

namespace App\Http\Controllers;

use Response;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Pedido;
use GuzzleHttp\Client;
use App\Models\Colegio;
use Illuminate\Http\File;
use Laracasts\Flash\Flash;
use League\Fractal\Manager;
use App\Models\RegistroPago;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use App\Models\ResumenPedido;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use App\Models\ServicioPaqueteria;
use Illuminate\Support\Facades\DB;
use App\DataTables\PedidoDataTable;
use Illuminate\Support\Facades\Log;
use App\Models\DetalleResumenPedido;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\PedidoRepository;
use App\Transformers\PedidoTransformer;
use Illuminate\Support\Facades\Storage;
use App\Exports\ReporteListaFoliosExport;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreatePedidoRequest;
use App\Http\Requests\UpdatePedidoRequest;
use App\Exports\ReportePagosFallidosExport;
use App\Http\Controllers\AppBaseController;
use App\Imports\UploadPedidosEntregadosImport;

use App\Notifications\LibrosReordenNotification;
use Exception;

class PedidoController extends AppBaseController
{

    private $excel;

    /** @var  PedidoRepository  */
    private $pedidoRepository;

    public function __construct(PedidoRepository $pedidoRepo)
    {
        $this->pedidoRepository = $pedidoRepo;
    }

    /**
     * Display a listing of the Pedido.
     *
     * @param PedidoDataTable $pedidoDataTable
     * @return Response
     */
    public function index()
    {
        return view('private.admin.pedidos');
    }


    public function facturas()
    {
        return view('private.admin.facturas.index');
    }
    /**
     * Show the form for creating a new Pedido.
     *
     * @return Response
     */
    public function create()
    {
        return view('pedidos.create');
    }

    /**
     * Store a newly created Pedido in storage.
     *
     * @param CreatePedidoRequest $request
     *
     * @return Response
     */
    public function store(CreatePedidoRequest $request)
    {

        $datos = $request->all();

        $configuracion = Configuracion::all()->first();


        $serie = $configuracion->etiqueta;
        $folio = $configuracion->valor + 1;
        $status = 'PROCESADO';
        $usuario = Auth::user();

        $reglas = [
            'nombre_contacto' => 'required|string|max:100',
            'apellidos_contacto' => 'required|string|max:100',
            'celular_contacto' => 'required|string|max:10',
            'tipo_entrega' => 'required|integer',
            'factura' => 'required|integer',
            'terminos_condiciones' => 'required|integer',
            'subtotal' => 'required|numeric',
            'comision' => 'required|numeric',
            'total' => 'required|numeric',
            'colegio_id' => 'required|integer',
            'metodo_pago_id' => 'required|integer',
            'forma_pago_id' => 'required|integer'
        ];

        $validador = Validator::make($datos, $reglas);

        if ($validador->fails()) {
            return $this->sendError($validador->errors()->first(), 400);
        }

        $pedido = new Pedido($datos);

        $pedido->serie = $serie;
        $pedido->folio = $folio;
        $pedido->status = $status;
        // $pedido->usuario_id = $usuario;
        $pedido->usuario_id = '1';

        DB::beginTransaction();

        try {
            $pedido->save();
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->sendError($th->getMessage(), 400);
        }

        $configuracion->valor = $folio;

        try {
            $configuracion->save();
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->sendError($th->getMessage(), 400);
        }

        $resumenesPedido = json_decode($request->resumen_pedido);

        foreach ($resumenesPedido as $datosResumenPedido) {
            // $resumenPedido = app(ResumenPedidoController::class)->store($value);

            $arrayDatosResumenPedido = array(
                "pedido_id" => $pedido->id,
                "nombre_alumno" => $datosResumenPedido->nombre_alumno,
                "paterno_alumno" => $datosResumenPedido->paterno_alumno,
                "materno_alumno" => $datosResumenPedido->materno_alumno,
                "paquete_id" => $datosResumenPedido->paquete_id,
                "subtotal" => $datosResumenPedido->subtotal
            );

            $resumenPedido = new ResumenPedido($arrayDatosResumenPedido);

            try {
                $resumenPedido->save();
            } catch (\Throwable $th) {
                DB::rollBack();
                return $this->sendError($th->getMessage(), 400);
            }

            foreach ($datosResumenPedido->detalle_resumen_pedido as $datosLibro) {

                $arrayDatosDetalleResumenPedido = array(
                    'libro_id' => $datosLibro->libro_id,
                    'precio_libro' => $datosLibro->precio_libro,
                    'cantidad' => $datosLibro->cantidad
                );
                $detalleResumenPedido = new DetalleResumenPedido($arrayDatosDetalleResumenPedido);
                $detalleResumenPedido->resumen_pedidos_id = $resumenPedido->id;
                try {
                    $detalleResumenPedido->save();
                } catch (\Throwable $th) {
                    DB::rollBack();
                    return $this->sendError($th->getMessage(), 400);
                }
            }
        }

        DB::commit();

        return $this->sendSuccess('Informacion registrada.');


        // return response()->json(['success' => true, 'data' => $pedido], 200);

        // $pedido = $this->pedidoRepository->create($input);

        // Flash::success('Pedido saved successfully.');

        // return redirect(route('pedidos.index'));
    }

    /**
     * Display the specified Pedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        return view('pedidos.show')->with('pedido', $pedido);
    }

    /**
     * Show the form for editing the specified Pedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        return view('pedidos.edit')->with('pedido', $pedido);
    }

    /**
     * Update the specified Pedido in storage.
     *
     * @param  int              $id
     * @param UpdatePedidoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePedidoRequest $request)
    {
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        $pedido = $this->pedidoRepository->update($request->all(), $id);

        Flash::success('Pedido updated successfully.');

        return redirect(route('pedidos.index'));
    }

    /**
     * Remove the specified Pedido from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            Flash::error('Pedido not found');

            return redirect(route('pedidos.index'));
        }

        $this->pedidoRepository->delete($id);

        Flash::success('Pedido deleted successfully.');

        return redirect(route('pedidos.index'));
    }

    /**
     * Remove the specified Pedido from storage.
     *
     *
     * @return Response
     */
    public function pedidosUsuario()
    {
        // dd(Auth::user()->pedidos->where('status', '!=', 'CARRITO')->sortByDesc('id'));
        return view('private.mispedidos')->with(
            'pedidos',
            Auth::user()->pedidos->where('status', '!=', 'CARRITO')->sortByDesc('id')
        );
    }

    /**
     * Remove the specified Pedido from storage.
     *
     *
     * @return Response
     */
    public function datosPedido($id)
    {
        $usuario = Auth::user();
        if ($usuario->hasRole('Venta Movil')) {
            $venta_movil = true;
            $referencia = DB::table('registros_pagos')->where('pedido_id', $id)->first()->raw;
            $referencia = json_decode($referencia);
        } else {
            $venta_movil = false;
            $referencia = "";
        }
        $pedido = Pedido::find($id);
        if (is_null($pedido)) {
            return view('errors.404');
        }

        $datos_facturacion = $pedido->datosFactura;
        $direccion_entrega = $pedido->direccion;

        //TODO: Verificar que funcione cuando tenga mas guias de pedidos
        $guia_pedido = $pedido->envio;
        $envios_parciales = [];
        foreach ($pedido->resumenes as $resumen) {
            foreach ($resumen->detallesResumenPedidos as $detalle) {
                if (!empty($detalle->envio)) {
                    array_push($envios_parciales, $detalle->envio);
                }
            }
        }




        //TODO: Preguntar sobre comisiones
        $subtotal = $pedido->resumenes->sum('subtotal');

        $costo_envio = 0;
        if ($guia_pedido) $costo_envio = $guia_pedido->costo;

        $total = ($subtotal + $costo_envio) + $pedido->comision; //* 1.16;

        return view('private.datoscompra')->with(compact('pedido', 'datos_facturacion', 'direccion_entrega', 'guia_pedido', 'subtotal', 'total', 'venta_movil', 'referencia', 'envios_parciales'));
    }

    public function storeCarrito(CreatePedidoRequest $request)
    {
        $user = Auth::user();
        $carrito = Pedido::where([['status', '=', 'CARRITO'], ['usuario_id', '=', $user->id]])->first();

        /** Se crea el pedido si no existe ninguno en carrito */
        if (empty($carrito))
            $carrito = new Pedido;
        else
            if ($request->colegio_id != $carrito->colegio_id)
            return $this->sendError('Carrito existente de otro colegio', 403);

        $carrito->colegio_id = $request->colegio_id;
        $carrito->status = 'CARRITO';
        $carrito->usuario_id = $user->id;
        if( $user->roles->pluck('name')->contains('Venta Movil') )
            $carrito->usuario_venta_id = $user->id;

        DB::beginTransaction();

        try {
            $carrito->touch();
            $carrito->save();

            /** Se agrega el resumen del pedido */
            $resumen = $carrito->resumenes()->create([
                'paquete_id' => $request->paquete_id
            ]);

            /** Se recorren los N libros que traiga para agregralos al detalle del resumen */
            foreach ($request->libros as $libro) {
                $resumen->detalles()->create([
                    'libro_id' => $libro['id'],
                    'precio_libro' => $libro['precio']
                ]);
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('PedidoController@storeCarrito', ['error' => $th->getMessage(), 'linea' => $th->getLine(), 'request' => $request->all() ]);
            // Log::debug($th);
            return $this->sendError($th->getMessage(), 400);
        }

        DB::commit();

        return response()->json(['success' => true, 'data' => $carrito->id], 200);
    }

    public function showCarrito($id)
    {

        $user = Auth::user();

        // $user = User::find(3);

        //with('resumenes.detalles.libro', 'resumenes.paquete.nivelColegio.seccionColegio')->
        $pedido = $user->pedidos()->whereId($id)->first();

        if (empty($pedido)) {
            //return $this->sendError('No se encontro el registro', 403);
            return view('private.home')->with('errors', 'No se encontró el registro');
        }

        $pedidoInformacion = self::datosCarrito($pedido);


        return response()->json(['success' => true, 'data' => $pedidoInformacion], 200);
    }

    public function viewCarrito()
    {

        $user = Auth::user();

        $carrito = Pedido::where([['status', '=', 'CARRITO'], ['usuario_id', '=', $user->id]])->first();

        if (empty($carrito))
            return view('private.carrito')->with('carrito_vacio', true);
        else
            return view('private.carrito')->with('pedido_id', $carrito->id);
    }

    public static function datosCarrito($pedido)
    {
        $fractal = new Manager();

        $informacion_pedido = $fractal->setSerializer(new Serializer())
            ->parseIncludes(['resumenes'])
            ->createData(new Item($pedido, new PedidoTransformer))->toArray();

        return $informacion_pedido;
    }

    public function getPagar($pedido_id)
    {
        $usuario = Auth::user();
        $pedido = $usuario->pedidos()->whereId($pedido_id)->first();
        // $pedido = $this->pedidoRepository->allQuery(['id' => $pedido_id])->first();

        if (empty($pedido)) {
            return redirect()->route('inicio')->with('error', 'No se encontró el pedido');
        }

        $pasarelas = [];

        if(env('OPENPAY_ENABLED', 0 == 1))
            $pasarelas['openpay'] = [ env('OPENPAY_MERCHANT_ID'), env('OPENPAY_PUBLIC_KEY'), env('OPENPAY_SANDBOX_MODE', 1) ];

        return view('private.pedidos.pagar')->with('pedido', $pedido)->with('pasarelas', $pasarelas);
    }


    public function reciboReferencia(Request $request, $pedido_id)
    {

        $user = Auth::user();

        $pedido = $user->pedidos()->whereId($pedido_id)->first();

        if (empty($pedido)) {
            return view('errors.403');
        } else {
            return view('private.reimprimir-comprobante')->with('id', $pedido_id);
        }
    }

    public function reporteReferencias(Request $request)
    {
        $request->status;
        $request->factura;
        $request->metodo_pagos;
        $request->forma_pagos;
        $request->forma_entrega;
        $request->inicio;
        $request->fin;
        $request->cambio;

        if ($request->inicio != null && $request->fin != null) {
            $pedidos = Pedido::whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->fin . ' 23:59:59'])
                // ->where('metodo_pago',['TIENDA','BANCO','DEBITO'])
                ->whereNotIn('status', ['CARRITO'])
                // ->whereNotIn('colegio_id', [1,2])
                ->get()->sortByDesc('folio');

            if ($request->status != 0 ||  $request->status != '0')
                $pedidos = $pedidos->where('status',  $request->status);

            if ($request->factura == 1 ||  $request->factura == '1')
                $pedidos = $pedidos->where('factura', $request->factura);

            if ($request->factura == 2 ||  $request->factura == '2')
                $pedidos = $pedidos->where('factura', null);

            if ($request->metodo_pagos != 0 ||  $request->metodo_pagos != '0')
                $pedidos = $pedidos->where('metodo_pago',  $request->metodo_pagos);

            if ($request->forma_pagos != 0 ||  $request->forma_pagos != '0')
                $pedidos = $pedidos->where('forma_pago',  $request->forma_pagos);

            if ($request->forma_entrega != 0 ||  $request->forma_entrega != '0')
                $pedidos = $pedidos->where('tipo_entrega',  $request->forma_entrega);
        } else {

            $request->inicio = Carbon::today()->format('Y-m-d');
            $pedidos = Pedido::whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->inicio . ' 23:59:59'])
                ->whereNotIn('status', ['CARRITO'])
                // ->whereNotIn('colegio_id', [1,2])
                ->get()->sortByDesc('folio');
        }

        $status = $request->status;
        $factura = $request->factura;
        $metodo_pagos = $request->metodo_pagos;
        $forma_pagos = $request->forma_pagos;
        $forma_entrega = $request->forma_entrega;
        $inicio = $request->inicio;
        $fin = $request->fin;

        return view('private.reporte.referencias')
            ->with('pedidos', $pedidos)
            ->with('status', $status)
            ->with('factura', $factura)
            ->with('metodo_pagos', $metodo_pagos)
            ->with('forma_pagos', $forma_pagos)
            ->with('forma_entrega', $forma_entrega)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }

    public function envioCorreoConfirmacionPago(Request $request)
    {
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::find($pedido_id);

        $pedido->sendPagoExitosoNotification();


        $date_pedio_change = date_format($pedido->created_at, "Y-m-d");
        $request->inicio = Carbon::today();
        $pedido_refresh = Pedido::whereBetween('created_at', [$date_pedio_change  . ' 00:00:00', $date_pedio_change  . ' 23:59:59'])
            ->whereNotIn('status', ['CARRITO'])
            ->get()->sortByDesc('folio');


        $status = $request->status;
        $factura = $request->factura;
        $metodo_pagos = $request->metodo_pagos;
        $forma_pagos = $request->forma_pagos;
        $forma_entrega = $request->forma_entrega;
        $inicio = $request->inicio;
        $fin = $request->fin;

        return view('private.reporte.referencias')
            ->with('pedidos', $pedido_refresh)
            ->with('status', $status)
            ->with('factura', $factura)
            ->with('metodo_pagos', $metodo_pagos)
            ->with('forma_pagos', $forma_pagos)
            ->with('forma_entrega', $forma_entrega)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }

    public function reporteReferenciasStatus(Request $request)
    {

        $request->validate([
            'estatus' => 'required',
        ], [
            'estatus.required' => 'Es necesario que seleccione el cambio de estatus de un pedido PROCESADO, puede seleccionar PAGADO o CANCELADO',
        ]);

        //Se busca el pedido que se manda por id desde el blade
        $pedido = Pedido::find($request->pedido_id);

        //Se recibe el request del estatus
        //Si el estatus es igual a cancelado y diferente de pagado y procesado entra
        if ($request->estatus == 'CANCELADO') {
            //Se llama a la funcion de cancelar para cancelar el pedido
            $pedido->cancelar();
            //Termina el proceso
        }
        //Si el request del estatus es igual a pagado o procesado entra
        if ($request->estatus == 'PAGADO' || $request->estatus == 'PROCESADO') {
            //se busca si el pedido tiene un registro de pago en la base de datos asociado a su id
            $registro_pago = $pedido->pagos->where('pedido_id', $pedido->id)->last(); //last en  lugar de first
            //Si su registro de pago es diferente de nulo entra
            if ($registro_pago  != null || $registro_pago  != '') {
                //Si el estatus del registro de pago es igual a review o failed entra
                if ($registro_pago->status == 'REVIEW' || $registro_pago->status == 'FAILED') {
                    //Se cambia el estatus del pedido a PROCESADO
                    $pedido->status = $request->estatus;
                }
                //Si el estatus del registro de pago es SUCCESS entra
                if ($registro_pago->status == 'SUCCESS') {
                    // Si el estatus del pedido es igual a procesado realiza la funcion de procesar
                    //para que regrese los libros al pedido y los reste del stock
                    if ($request->estatus == 'PROCESADO') {
                        $pedido->procesar();
                    }
                    //Se cambia directamente el estatus a pagado ya que cuenta con un registro de pago
                    //en success
                    $pedido->status = 'PAGADO';
                }
            }
            //si el registro del pago es NULO entra en el else
            else {
                //Se verifica que el pedido tenga una referencia diferente de
                //de lo contrario crea un registro de pago
                if ($pedido->referencia != null || $pedido->referencia != '') {
                    //si existe una referencia para este pedido entra
                    $referencia = $pedido->referencia;
                    //se hace update del campo pagado en la referencia del pedido
                    $referencia->pagado = 1;
                    DB::beginTransaction();
                    try {
                        $referencia->touch();
                        $referencia->save();
                    } catch (\Throwable $th) {
                        DB::rollBack();
                        return $this->sendError($th->getMessage(), 400);
                    }
                    DB::commit();
                    //sale para el sigueinte punto
                }
                //Se crea el registro de pago
                $registro_pago_referencia = new RegistroPago();
                $registro_pago_referencia->pedido_id = $pedido->id;
                $registro_pago_referencia->amount = $pedido->total;
                $registro_pago_referencia->source = 'Registro de pago de forma manual';
                $registro_pago_referencia->status = 'SUCCESS';
                $registro_pago_referencia->transactionTokenId = auth()->user()->id;
                $registro_pago_referencia->raw = auth()->user()->email;
                $registro_pago_referencia->origen = 'MANUAL';
                DB::beginTransaction();
                try {
                    //se guarda registro de pago
                    $registro_pago_referencia->touch();
                    $registro_pago_referencia->save();
                } catch (\Throwable $th) {
                    DB::rollBack();
                    return $this->sendError($th->getMessage(), 400);
                }
                DB::commit();
                //se cambia el estatus del pedido a PROCESADO
                $pedido->status = $request->estatus;
            }
            DB::beginTransaction();
            try {
                //se guardan los cambios del pedido, del cambio de estatus a procesado
                $pedido->touch();
                $pedido->save();
            } catch (\Throwable $th) {
                DB::rollBack();
                return $this->sendError($th->getMessage(), 400);
            }

            DB::commit();
            //si el estatus es procesado realiza la funcion procesar
            if ($pedido->status == 'PROCESADO') {
                $pedido->procesar();
            }
        }

        $id_pedido_change = Pedido::find($request->pedido_id)->first();
        $date_pedio_change = date_format($id_pedido_change->created_at, "Y-m-d");
        $request->inicio = Carbon::today();
        $pedido_refresh = Pedido::whereBetween('created_at', [$date_pedio_change  . ' 00:00:00', $date_pedio_change  . ' 23:59:59'])
            ->whereNotIn('status', ['CARRITO'])
            ->get()->sortByDesc('folio');


        $status = $request->status;
        $factura = $request->factura;
        $metodo_pagos = $request->metodo_pagos;
        $forma_pagos = $request->forma_pagos;
        $forma_entrega = $request->forma_entrega;
        $inicio = $request->inicio;
        $fin = $request->fin;

        return view('private.reporte.referencias')
            ->with('pedidos', $pedido_refresh)
            ->with('status', $status)
            ->with('factura', $factura)
            ->with('metodo_pagos', $metodo_pagos)
            ->with('forma_pagos', $forma_pagos)
            ->with('forma_entrega', $forma_entrega)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }

    /*
    * Recibe un Excel con la lista de pedidos entregados
    */
    public function uploadPedidosEntregados(Request $request)
    {
        $pedidos = [];

        foreach ($request->files as $nombre => $contenido) {
            if (!$contenido->isValid()) {
                Log::error("Error al subir el archivo {$nombre} es invalido");
                return response()->json(['success' => false, 'msg' => 'El archivo es invalido'], 503);
            }

            if (!($contenido->guessExtension() == "xlsx" || $contenido->guessExtension() == "xls" || $contenido->guessExtension() == "csv")) {
                Log::error("Error al subir el archivo, solo se aceptan hojas con formato xslx, xsl o csv");
                return response()->json(['success' => false, 'msg' => 'Solo se aceptan hojas con formato xslx, xsl o csv'], 503);
            }

            $file = $request->file($nombre)->store('temp_uploads', 'local');

            $array = Excel::toCollection(new UploadPedidosEntregadosImport, storage_path('app/' . $file ));

            $pedidos_no_identificados = 0;
            $pedidos_entregados = 0;

            foreach ($array->first() as $element) {
                $ex = explode('PR', $element->first());

                if (count($ex) != 2) {
                    continue;
                }

                $folio = (int) $ex[1];

                if ($folio == 0) {
                    continue;
                }

                $pedido = Pedido::where("serie", "PR")->where("folio",  $folio)->whereNotIn('status', ['ENTREGADO'])->first();

                if (is_null($pedido)) {

                    $pedidos_no_identificados += 1;

                    $pedido = new \stdClass();
                    $pedido->id = 0;
                    $pedido->serie = 'PR';
                    $pedido->folio = strval($folio);
                    $pedido->status = 'UNDEFINED';
                    $pedido->nombre_contacto = '';
                    $pedido->apellidos_contacto = '';
                    $pedido->email_pedido = '';
                    $pedido->celular_contacto = '';
                    $pedido->tipo_entrega = 0;
                    $pedido->factura = NULL;
                    $pedido->terminos_condiciones = 0;
                    $pedido->subtotal = NULL;
                    $pedido->comision = NULL;
                    $pedido->total = NULL;
                    $pedido->usuario_id = 0;
                    $pedido->colegio_id = 0;
                    $pedido->direccion_id = NULL;
                    $pedido->metodo_pago = NULL;
                    $pedido->forma_pago = NULL;

                    array_push($pedidos, $pedido);

                    continue;
                }

                if ($pedido->status == 'PAGADO' || $pedido->status == 'ENVIADO') {
                    $pedido->update(['status' => 'ENTREGADO']);
                    $pedidos_entregados += 1;
                }

                $c = collect([$pedido]);

                $c = $c->transform(function ($item) {
                    $item->makeHidden('resumenes');
                    $item->makeHidden('colegio');
                    return $item;
                });

                array_push($pedidos, $c->first());

                activity()
                ->performedOn($pedido)
                ->causedBy(Auth::user()->id)
                ->log('Cambio el estao del pedido a ENTREGADO de forma masiva');
            }
        }

        activity()
            ->causedBy(Auth::user()->id)
            ->log('Realizó una entrega de pedidos masiva.');

        return response()->json(['success' => true, 'message' => 'Pedidos modificados: ' . $pedidos_entregados . ' Pedidos no identificados: ' . $pedidos_no_identificados, 'total_count' => count($pedidos), 'pedidos' => $pedidos], 200);
    }

    /**
    * Recibe un Excel con la lista de pedidos enviados
    * Ejemplo:
    * | PEDIDO |    GUIA     |
    * | PR5125 | 01231568789 |
    */
    public function uploadPedidosEnviados(Request $request)
    {
        $request->validate([
            'archivo'       => 'required|file|mimes:xlsx,xls,csv',
            'paqueteria_id' => 'required|exists:servicios_paqueteria,id'
        ]);

        $folios = Excel::toCollection(new UploadPedidosEntregadosImport, $request->file('archivo'))
        ->first() // Primera hoja
        //->skip(1) // Omitimos la primer fila
        ->mapWithKeys(function($row) {
            // if(!strstr($row[0], 'PR'))
            //     return null;

            $folio = str_replace('PR', '', $row[0]);
            return [
                $folio => [
                    'folio' => $folio,
                    'guia'  => $row[1]
                ]
            ];
        })
        ->whereNotNull();
        $servicio_paqueteria = ServicioPaqueteria::find($request->paqueteria_id);
        $fecha_envio         = Carbon::today()->format('Y-m-d');

        DB::beginTransaction();

        try {
            $pedidos_enviados = Pedido::whereIn('folio', $folios->keys())->whereNotIn('status', ['ENVIADO'])
            ->get()
            ->each(function($pedido) use ($folios, $servicio_paqueteria, $fecha_envio) {
                $costo_envio = $pedido->colegio->config('costo_envio');
                $guia        = $folios[ $pedido->folio ] ['guia'];

                if (is_null($pedido->envio))
                    $pedido->envio()->create([
                        'pedido_id'     => $pedido->id, // Este campo es necesario por la relación polimorfica del modelo de envíos
                        'status'        => 'ENVIADO',
                        'costo'         => $costo_envio,
                        'guia'          => $guia,
                        'paqueteria'    => $servicio_paqueteria->nombre,
                        'paqueteria_id' => $servicio_paqueteria->id,
                        'fecha_envio'   => $fecha_envio,
                    ]);
                else
                    $pedido->envio->update([
                        'status'        => 'ENVIADO',
                        'costo'         => $costo_envio,
                        'guia'          => $guia,
                        'paqueteria'    => $servicio_paqueteria->nombre,
                        'paqueteria_id' => $servicio_paqueteria->id,
                        'fecha_envio'   => $fecha_envio,
                    ]);

                // Marcamos todos los detalles del pedido como entregados
                $pedido->detalles->each(function($detalle) use ($fecha_envio) {
                    $detalle->update([
                        'enviado' => 1,
                        'fecha_envio' => $fecha_envio
                    ]);
                });

                if($pedido->tipo_entrega == 2 && $pedido->envio != null) {
                    $pedido->update([
                        'status' => 'ENVIADO'
                    ]);

                    activity()
                    ->performedOn($pedido)
                    ->causedBy(Auth::user()->id)
                    ->log("Alta de un envio en el pedido con el id {$pedido->id}: {$pedido->serie}{$pedido->folio} y la guía {$guia}");

                } else {
                    activity()
                    ->performedOn($pedido)
                    ->causedBy(Auth::user()->id)
                    ->log("Error al notificar el envío en el pedido con el id {$pedido->id}: {$pedido->serie}{$pedido->folio} y la guía {$guia}, se intenta asignar un numero de guía a un pedido que no tiene un tipo de entrega de envío");
                }

                $pedido->sendPedidoEnCaminoNotification();
            });
        } catch (\Throwable $th) {
            DB::rollback();
            Log::debug('PedidoController@uploadPedidosEnviados Error al cargar los envíos', [$th->getLine(), $th->getMessage()]);
            return response()->json(['success' => false, 'error' => $th->getMessage()], 503);
        }

        DB::commit();

        $pedidos_no_identificados = $pedidos_enviados->count() - $folios->count();

        return response()->json([
                'success'     => true,
                'message'     => "Pedidos enviados: {$pedidos_enviados->count()} Pedidos no identificados: {$pedidos_no_identificados}",
                'total_count' => $folios->count(),
                'pedidos'     => $pedidos_enviados
            ], 200);
    }



    public function notificacionPedido(Pedido $pedido)
    {
        return $pedido->sendPedidoEnCaminoNotification();
    }

    public function reporteEnviosGenerales(Request $request)
    {


        $inicio = $request->inicio;
        $fin = $request->fin;

        if ($request->inicio != null && $request->fin != null) {
            $pedidos = Pedido::whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59'])
                // ->where('tipo_entrega',2)
                ->whereIn('status', ['ENVIADO', 'ENTREGADO', 'ENVIO PARCIAL', 'ENVIO LIQUIDADO'])
                ->get()->sortByDesc('folio');
        } else {
            $request->inicio = Carbon::today()->format('Y-m-d');
            $pedidos = Pedido::whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->inicio . ' 23:59:59'])
                // ->where('tipo_entrega',2)
                ->whereIn('status', ['ENVIADO', 'ENTREGADO', 'ENVIO PARCIAL', 'ENVIO LIQUIDADO'])
                ->get()->sortByDesc('folio');
        }
        return view('private.envios_reporte_general')
            ->with('pedidos', $pedidos)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }


    public function testReferenciaBBVA(Request $request, $pedido_id)
    {
        return Pedido::find($pedido_id)->generarReferenciaAlgoritmo10BBVA();
    }

    public function reportePagosrechazadosTest(Request $request)
    {

        if (!$request->has('inicio')) {
            $request->inicio = Carbon::now()->subDays(3650);
        }

        if (!$request->has('fin')) {
            $request->fin = Carbon::now();
        }

        return Excel::download(new ReportePagosFallidosExport($request->inicio, $request->fin), 'reportePagosFallidos.xlsx');
    }

    public function reporteListaFoliosTest()
    {
        return Excel::download(new ReporteListaFoliosExport(), 'reporteListaFolios.xlsx');
    }

    public function reportePedidosFallidos(Request $request)
    {
        if ($request->inicio != null && $request->fin != null) {
            $pagos_fallidos = RegistroPago::whereIn('status', ['FAILED', 'REVIEW', 'CHARGEABLE', 'WAIT_THREEDS'])
                ->whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->fin . ' 23:59:59'])
                ->orderBy('created_at', 'desc')
                ->get();

            $pagos_fallidos = $pagos_fallidos->transform(function ($pago) {

                $pedido = $pago->pedido()->withTrashed()->first();
                // $pedido = json_decode($pago->pedido);


                $info = (object) $pago->raw;
                $scalar = json_decode($info->scalar);
                if (isset($scalar->error)) {
                    $error_pago = $scalar->error;
                } else {
                    $error_pago = '';
                }

                if($pago->status_3ds != null || $pago->status_3ds != ''){
                    $info_3ds = (object) $pago->status_3ds;
                    $scalar_3ds = json_decode($info_3ds->scalar);
                    if (isset($scalar_3ds->responseMsg)) {
                        $error_pago_3ds = $scalar_3ds->responseMsg;
                    } else {
                        $error_pago_3ds = '';
                    }
                }
                else{
                    $error_pago_3ds = '';
                }

                if($pago->confirm != null || $pago->confirm != ''){
                    $info_confirm = (object) $pago->confirm;
                    $scalar_confirm = json_decode($info_confirm->scalar);
                    if (isset($scalar_confirm->error)) {
                        $error_pago_confirm = $scalar_confirm->error;
                    } else {
                        $error_pago_confirm = '';
                    }
                }
                else{
                    $error_pago_confirm = '';
                }

                try {
                    $terminacion = $scalar->paymentSource->card->lastFourDigits;
                } catch (\Exception $e) {
                    $terminacion = '';
                }
                return [
                    'fecha'          => !is_null($pedido) ? date_format($pedido->created_at, "d / m / Y") : '',
                    'folio'          => !is_null($pedido) ? $pedido->serie . $pedido->folio : '',
                    'total'          => !is_null($pedido) ? number_format($pedido->total, 2) : '',
                    'estatus'        => !is_null($pedido) ? $pedido->status : '',
                    'nombre'         => !is_null($pedido) ? $pedido->nombre_contacto . '' . $pedido->apellidos_contacto : '',
                    'correo'         => !is_null($pedido) ? $pedido->user->email : '',
                    'teléfono'       => !is_null($pedido) ? $pedido->celular_contacto : '',
                    'colegio'        => !is_null($pedido) ? $pedido->colegio->nombre : '',
                    'fecha_pago'     => $pago->created_at,
                    'terminacion'    => $terminacion,
                    'monto'          => number_format($pago->amount),
                    'motivo_rechazo' => $error_pago,
                    'motivo_3ds'     => $error_pago_3ds,
                    'motivo_confirm'     => $error_pago_confirm,
                    'status'         => $pago->status
                ];
            });
            return view('private.reporte.tabla_pagos_fallidos')
                ->with('pagos_fallidos', $pagos_fallidos)
                ->with('request', $request);
        } else {
            return view('private.reporte.tabla_pagos_fallidos')
                ->with('pagos_fallidos', null);
        }
    }

    public function getListaEntregaPedidos()
    {
        return view('private.admin.entregasParciales.index');
    }

    public function cambioDeStatusPedido(Request $request)
    {
        $request->pedido_id;
        $request->estatus;
        $request->id_detalle_pedido;

        if ($request->estatus == 'ENTREGADO') {
            foreach ($request->id_detalle_pedido as $key => $val) {
                DetalleResumenPedido::where('id', $val)
                    ->update(['entregado' => 1]);
            }
        }

        $pedido = Pedido::where('id', $request->pedido_id)->first();

        $pedido->update(['status' => $request->estatus]);

        activity()
            ->performedOn($pedido)
            ->causedBy(Auth::user()->id)
            ->log('Realizó una entrega parcial del pedido con folio: ' . $pedido->serie . '-' . $pedido->folio);



        return back();
    }

    public function imprimirPedidos($fecha_impresion)
    {
        //https://packagist.org/packages/barryvdh/laravel-dompdf
        //https://github.com/barryvdh/laravel-dompdf
        //https://styde.net/genera-pdfs-en-laravel-con-el-componente-dompdf/

        $date = $fecha_impresion;

        $pedidos = Pedido::whereHas('pagos', function($query) use ($date) {
            $query->where('status', 'SUCCESS');
            $query->whereBetween('created_at', [$date . ' 00:00:00', $date . ' 23:59:59']);
        })
        //'CARRITO','PAGO EN REVISION','PAGO RECHAZADO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'
        ->whereNotIn('status',['CANCELADO'])
        ->orderBy('created_at', 'desc');

        $pedidos->update([
            'impreso' => 1
        ]);

        $pedidos = $pedidos->get();

        activity()
            ->causedBy(Auth::user()->id)
            ->log("Imprimió los pedidos del día {$date}");

        $pdf = \PDF::loadView('private.admin.almacen.pedidos.diario', ['pedidos' => $pedidos, 'date' => $date]);

        $pdf->getDomPDF()->set_options(['dpi' => 150, 'defaultFont' => 'arial', 'enable_php' => true]);
        $pdf->getDomPDF()->set_paper('Letter', 'portrait');

        return $pdf->download($date . '.pdf');
    }

    public function imprimirPedidoscolegio($fecha_impresion, $colegio_id)
    {
        //https://packagist.org/packages/barryvdh/laravel-dompdf
        //https://github.com/barryvdh/laravel-dompdf
        //https://styde.net/genera-pdfs-en-laravel-con-el-componente-dompdf/

        $date = $fecha_impresion;

        $pedidos = Pedido::where('colegio_id',$colegio_id)
        ->whereHas('pagos', function($query) use ($date) {
            $query->where('status', 'SUCCESS');
            $query->whereBetween('updated_at', [$date . ' 00:00:00', $date . ' 23:59:59']);
        })
        //'CARRITO','PAGO EN REVISION','PAGO RECHAZADO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA'
        ->whereNotIn('status',['CANCELADO'])
        ->orderBy('updated_at', 'desc');

        $pedidos->update([
            'impreso' => 1
        ]);

        $pedidos = $pedidos->get();

        activity()
            ->causedBy(Auth::user()->id)
            ->log("Imprimió los pedidos del día {$date}");

        $pdf = \PDF::loadView('private.admin.almacen.pedidos.diario', ['pedidos' => $pedidos, 'date' => $date]);

        $pdf->getDomPDF()->set_options(['dpi' => 150, 'defaultFont' => 'arial', 'enable_php' => true]);
        $pdf->getDomPDF()->set_paper('Letter', 'portrait');

        return $pdf->download($date . '-'.$colegio_id.'.pdf');
    }

    public function reportePedidosGenerales(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;

        if ($request->inicio != null && $request->fin != null) {
            $pedidos = Pedido::whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59'])
                ->whereNotIn('status', ['CARRITO'])
                ->get()->sortByDesc('folio');
        } else {
            $request->inicio = Carbon::today()->format('Y-m-d');
            $pedidos = Pedido::whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->inicio . ' 23:59:59'])
                ->whereNotIn('status', ['CARRITO'])
                ->get()->sortByDesc('folio');
        }
        return view('private.reporte.reporte_pedidos_generales')
            ->with('pedidos', $pedidos)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }

    public function pedidosGeneralConfirmacionPago(Request $request)
    {
        $pedido_id = $request->pedido_id;
        $pedido = Pedido::find($pedido_id);
        $pedido->sendPagoExitosoNotification();


        $date_pedio_change = date_format($pedido->created_at, "Y-m-d");
        $request->inicio = Carbon::today();
        $pedido_refresh = Pedido::whereBetween('created_at', [$date_pedio_change  . ' 00:00:00', $date_pedio_change  . ' 23:59:59'])
            ->whereNotIn('status', ['CARRITO'])
            ->get()->sortByDesc('folio');


        $status = $request->status;
        $factura = $request->factura;
        $metodo_pagos = $request->metodo_pagos;
        $forma_pagos = $request->forma_pagos;
        $forma_entrega = $request->forma_entrega;
        $inicio = $request->inicio;
        $fin = $request->fin;

        // return view('private.reporte.reporte_pedidos_generales')
        return redirect()->to('reporte_pedidos/pedidos_general')
            ->with('pedidos', $pedido_refresh)
            ->with('status', $status)
            ->with('factura', $factura)
            ->with('metodo_pagos', $metodo_pagos)
            ->with('forma_pagos', $forma_pagos)
            ->with('forma_entrega', $forma_entrega)
            ->with('inicio', $inicio)
            ->with('fin', $fin);
    }

    // Devuelve la vista con la pasarela de pago para venta movil
    public function getVentaMovil()
    {
        return view('venta_movil.index');
    }

    // Regresa vista con pedidos del usuario venta movil
    public function pedidosVentaMovil()
    {
        return view('venta_movil.pedidos-venta-movil');
    }

    public function pruebaEnvioMailVentaMovil()
    {
        $pedidos = Pedido::find(4491);

        return view('emails.pago_venta_movil')
            ->with('pedido', $pedidos);
    }

    public function pruebaEnvioPuntoReorden()
    {
        /**
         * debe ser un reporte de todos los productos que alcanzaron su punto de reorden, agrupados por colegio, se dede de mostrar el ISBN, nombre, stock y punto de reorden
         */
        // // $email_pedidos_web = 'pedidosweb@booktrain.com.mx';
        // $email_pedidos_web = 'mario@inkwonders.com';

        // $user = User::where('email', $email_pedidos_web)->first();

        // $libros = Colegio::whereHas('libros', function ($query) {
        //     $query->where('paquete_id', null);
        //     $query->where('punto_reorden', '>', 0);
        //     $query->whereRaw('stock < punto_reorden');
        // })
        //     ->with('libros', function ($query) {
        //         $query->where('paquete_id', null);
        //         $query->where('punto_reorden', '>', 0);
        //         $query->whereRaw('stock < punto_reorden');
        //     })
        //     ->get()
        //     ->map(function ($colegio) {
        //         return $colegio->libros->map(function ($libro) use ($colegio) {
        //             $ultima_venta = $libro->detalleResumen()
        //                 ->where('entregado', 1)
        //                 ->whereHas('resumenPedido', function ($query) use ($colegio) {
        //                     $query->whereHas('pedido', function ($query) use ($colegio) {
        //                         $query->whereIn('status', ['PAGADO', 'ENVIADO', 'ENTREGADO', 'ENVIO PARCIAL', 'ENVIO LIQUIDADO', 'ENTREGA PARCIAL', 'ENTREGA LIQUIDADA']);
        //                         $query->where('colegio_id', $colegio->id);
        //                     });
        //                 })
        //                 ->orderBy('created_at', 'DESC')
        //                 ->first();

        //             return (object) [
        //                 'colegio'       => $colegio->nombre,
        //                 'isbn'          => $libro->isbn,
        //                 'nombre'        => $libro->nombre,
        //                 'stock'         => $libro->pivot->stock,
        //                 'punto_reorden' => $libro->pivot->punto_reorden,
        //                 'bajo_pedido'   => $libro->pivot->bajo_pedido,
        //                 'ultima_venta'  => $ultima_venta ? $ultima_venta->created_at->diffForHumans() : 'N/D'
        //             ];
        //         });
        //     })->collapse();
        $fecha = Carbon::now();
        // $notificacion = new LibrosReordenNotification( $libros );
        // $notificacion->toMail($email_pedidos_web);
        // return view('emails.notificacion_punto_reorden')
        //     ->with('libros', $libros)
        //     ->with('fecha', $fecha);
        //////////////////////////////////////////////////////////////////

        $colegios = Colegio::where('id',77)->whereHas('libros', function($query) {
            $query->where('paquete_id', null);
            $query->where('punto_reorden', '>', 0);
            $query->whereRaw('stock < punto_reorden');
        })
        ->with(['libros' => function($query) {
            $query->where('paquete_id', null);
            $query->where('punto_reorden', '>', 0);
            $query->whereRaw('stock < punto_reorden');

            $query->with('detalleResumen.resumen.pedido');

        }, 'configuraciones'])
        ->get()
        ->map(function($colegio) {
            return $colegio->libros->map(function($libro) use ($colegio) {
                $ultima_venta = $libro->detalleResumen()
                    // ->where('entregado', 1)
                    ->whereHas('resumen', function($query) use ($colegio) {
                        $query->whereHas('pedido', function($query) use ($colegio) {
                            $query->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']);
                            $query->where('colegio_id', $colegio->id);
                        });
                    })
                    ->orderBy('created_at', 'DESC')
                    ->first();

                // dd($libro->detalleResumen()
                //     // ->where('entregado', 1)
                //     ->whereHas('resumen', function($query) use ($colegio) {
                //         $query->whereHas('pedido', function($query) use ($colegio) {
                //             $query->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']);
                //             $query->where('colegio_id', $colegio->id);
                //         });
                //     })
                //     ->orderBy('created_at', 'DESC')
                //     ->first());

                return (Object) [
                    'colegio_id'                    => $colegio->id,
                    'colegio'                       => $colegio->nombre,
                    'isbn'                          => $libro->isbn,
                    'nombre'                        => $libro->nombre,
                    'stock'                         => $libro->pivot->stock,
                    'punto_reorden'                 => $libro->pivot->punto_reorden,
                    'bajo_pedido'                   => $libro->pivot->bajo_pedido,
                    'ultima_venta'                  => $ultima_venta ? $ultima_venta->created_at : 'N/D'
                ];
            });
        })
        ->collapse()
        ->groupBy('colegio_id');



        foreach ($colegios as $colegio_id => $libros) {
            $colegio = Colegio::where('id', $colegio_id)->first();
            $correo_comercial_provesa   = $colegio->config('correo_comercial_provesa');
            $correo_coordinador_provesa = $colegio->config('correo_coordinador_provesa');
            $correo_coordinador_colegio = $colegio->config('correo_coordinador_colegio');

            if($libros->count() > 0) {

                if($correo_comercial_provesa != '') {
                    Log::debug("Enviando notificación por correo a {$correo_comercial_provesa}", ['libros' => $libros]);
                    // $this->info("Enviando notificación por correo a {$correo_comercial_provesa}: {$libros->count()} libros");

                    try {
                        // \Notification::route('mail', [
                        //     $correo_comercial_provesa => 'Plataforma Booktrain',
                        // ])->notify(new LibrosReordenNotification($libros));

                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_comercial_provesa);

                        // dd($correo_comercial_provesa);
                        return view('emails.notificacion_punto_reorden')
                                 ->with('libros', $libros)
                                 ->with('fecha', $fecha);
                    } catch (\Throwable $th) {
                         $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }

                if($correo_coordinador_provesa != '') {
                    Log::debug("Enviando notificación por correo a {$correo_coordinador_provesa}", ['libros' => $libros]);
                    // $this->info("Enviando notificación por correo a {$correo_coordinador_provesa}: {$libros->count()} libros");

                    try {
                        // \Notification::route('mail', [
                        //     $correo_coordinador_provesa => 'Plataforma Booktrain',
                        // ])->notify(new LibrosReordenNotification($libros));
                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_coordinador_provesa);
                        // dd($notificacion);
                        return view('emails.notificacion_punto_reorden')
                                 ->with('libros', $libros)
                                 ->with('fecha', $fecha);
                    } catch (\Throwable $th) {
                        $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }

                if($correo_coordinador_colegio != '') {
                    Log::debug("Enviando notificación por correo a {$correo_coordinador_colegio}", ['libros' => $libros]);
                    // $this->info("Enviando notificación por correo a {$correo_coordinador_colegio}: {$libros->count()} libros");

                    try {
                        // \Notification::route('mail', [
                        //     $correo_coordinador_colegio => 'Plataforma Booktrain',
                        // ])->notify(new LibrosReordenNotification($libros));
                        // $notificacion = new LibrosReordenNotification( $libros );
                        // $notificacion->toMail($correo_coordinador_colegio);
                        // dd($notificacion);
                        return view('emails.notificacion_punto_reorden')
                                 ->with('libros', $libros)
                                 ->with('fecha', $fecha);
                    } catch (\Throwable $th) {
                        $this->error("El email no pudo ser enviado: {$th}");
                        Log::error('NotificacionReorden', [ $th ]);
                    }
                }
            }

        }
    }

    public function getInvoiceFiles(Request $request){



        Log::debug('FacturamaController@generateCfdi', ['Obtenemos datos'=> $request->all()]);


                        $request->validate([

                            'facturama_id'  => 'required|exists:pedidos,facturama_invoice_id',

                            'format'       =>  'in:xml,pdf'

                        ]);


         try{

            $id=$request->facturama_id;

            $format=$request->format;

            $facturama= $this->getInstance();

            $type = env('FACTURAMA_INVOICE_TYPE');

            $params = [];

            $result = $facturama->get('cfdi/'.$format.'/'.$type.'/'.$id, $params);

            // dd($result);

            // dd(storage_path('app/xml_pdf/'.$id.'.'.$format));

           $myfile = fopen(storage_path('app/xml_pdf/').$id.'.'.$format, 'a+');

            fwrite($myfile, base64_decode(end($result)));

            fclose($myfile);

            printf('<pre>%s<pre>', var_export(true));

         } catch (Exception $e) {

            Log::debug('FacturamaController@getInvoiceFiles', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInvoiceFiles ERROR on the transaction Exception: ',$array);

            return response()->json($array);

        }

    }

    private function getInstance()
    {

        try {


            Log::debug('FacturamaController@getInstance', ['Conexion con Facturama']);

            $production=boolval(env('FACTURAMA_DEVELOPMENT'));

            $facturama = new \Facturama\Client(env('FACTURAMA_USERNAME'), env('FACTURAMA_PASSWORD'));

            if($production){

                $url_prod=env('FACTURAMA_INVOICE_URL');

                 $facturama->setApiUrl($url_prod);

            }

                //   dd($facturama);

            return $facturama;

        } catch (RequestException $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);

        } catch (Exception $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);
        }
    }

    public function imprimirFactura($accion,$id)
    {

        try{

            $id=$id;

            switch ($accion) {
                case 1:

                    $format='pdf';

                break;

                case 2:

                    $format='xml';

                break;

                case 3:

                    $format='acuse';

                break;

            }

            if($format=='acuse'){

                $dir="acuse/";

                $format='pdf';

                $nombre_archivo=$id.'_acuse.'.$format;

             }else{

                $dir="cfdi/";  $nombre_archivo=$id.'.'.$format;

            }

            $facturama = $this->getInstance();

            $type = env('FACTURAMA_INVOICE_TYPE');

            $params = [];

            $result = $facturama->get($dir.$format.'/'.$type.'/'.$id, $params);

            $this->modal_descarga_documento = $nombre_archivo;

            Storage::disk('public')->put('xml_pdf/'.$nombre_archivo, base64_decode(end($result)));

            return Storage::disk('public')->download('xml_pdf/'.$this->modal_descarga_documento);


         } catch (Exception $e) {

            Log::debug('FacturamaController@getInvoiceFiles', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInvoiceFiles ERROR on the transaction Exception: ',$array);

            return response()->json($array);

        }

    }
}
