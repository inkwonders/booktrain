<?php

namespace App\Http\Controllers;

use App\DataTables\EnvioDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateEnvioRequest;
use App\Http\Requests\UpdateEnvioRequest;
use App\Repositories\EnvioRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Envio;
use App\Models\Pedido;
use App\Models\DetalleResumenPedido;
use Response;
use Carbon\Carbon;

class EnvioController extends AppBaseController
{
    /** @var  EnvioRepository */
    private $envioRepository;

    public function __construct(EnvioRepository $envioRepo)
    {
        $this->envioRepository = $envioRepo;
    }

    /**
     * Display a listing of the Envio.
     *
     * @param EnvioDataTable $envioDataTable
     * @return Response
     */
    public function index(EnvioDataTable $envioDataTable)
    {
        return $envioDataTable->render('envios.index');
    }

    /**
     * Show the form for creating a new Envio.
     *
     * @return Response
     */
    public function create()
    {
        return view('envios.create');
    }

    /**
     * Store a newly created Envio in storage.
     *
     * @param CreateEnvioRequest $request
     *
     * @return Response
     */
    public function store(CreateEnvioRequest $request)
    {
        $input = $request->all();

        $envio = $this->envioRepository->create($input);

        Flash::success('Envio saved successfully.');

        return redirect(route('envios.index'));
    }

    /**
     * Display the specified Envio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            Flash::error('Envio not found');

            return redirect(route('envios.index'));
        }

        return view('envios.show')->with('envio', $envio);
    }

    /**
     * Show the form for editing the specified Envio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            Flash::error('Envio not found');

            return redirect(route('envios.index'));
        }

        return view('envios.edit')->with('envio', $envio);
    }

    /**
     * Update the specified Envio in storage.
     *
     * @param  int              $id
     * @param UpdateEnvioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnvioRequest $request)
    {
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            Flash::error('Envio not found');

            return redirect(route('envios.index'));
        }

        $envio = $this->envioRepository->update($request->all(), $id);

        Flash::success('Envio updated successfully.');

        return redirect(route('envios.index'));
    }

    /**
     * Remove the specified Envio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            Flash::error('Envio not found');

            return redirect(route('envios.index'));
        }

        $this->envioRepository->delete($id);

        Flash::success('Envio deleted successfully.');

        return redirect(route('envios.index'));
    }

    public function funcionActualizaFechaEnvioConUpdatedAt()
    {
        $envios = Envio::all()->where('guia','<>', null);

        foreach ($envios as $envio) {
            $id = $envio->id;
            $updated = $envio->updated_at;
            $envio->where('id', $id)->update(['fecha_envio' => $updated]);
            $envio->save();
        }

    }
    public function enviosParciales() {
        return view('private.admin.enviosParciales.index');
    }

    public function cambioDeStatusPedido(Request $request) {
        $request->pedido_id;
        $request->estatus;
        $request->id_detalle_pedido;

        if($request->estatus == 'ENTREGADO'){
            foreach ($request->id_detalle_pedido as $key => $val)
            {
                DetalleResumenPedido::where('id', $val)
                    ->update(['entregado' => 1]);
            }
        }

        $id_pedido_change = Pedido::where('id', $request->pedido_id)
        ->update(['status' => $request->estatus]);


        return back();
    }
}
