<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use League\Fractal\Manager;
use App\Models\NivelColegio;
use App\Transformers\Serializer;
use League\Fractal\Resource\Collection;
use App\DataTables\NivelColegioDataTable;
use App\Http\Controllers\AppBaseController;
use App\Repositories\NivelColegioRepository;
use App\Transformers\NivelesColegioTransformer;
use App\Http\Requests\CreateNivelColegioRequest;
use App\Http\Requests\UpdateNivelColegioRequest;

class NivelColegioController extends AppBaseController
{
    /** @var  NivelColegioRepository */
    private $nivelColegioRepository;

    public function __construct(NivelColegioRepository $nivelColegioRepo)
    {
        $this->nivelColegioRepository = $nivelColegioRepo;
    }

    /**
     * Display a listing of the NivelColegio.
     *
     * @param NivelColegioDataTable $nivelColegioDataTable
     * @return Response
     */
    public function index(NivelColegioDataTable $nivelColegioDataTable)
    {
        return $nivelColegioDataTable->render('nivel_colegios.index');
    }

    /**
     * Show the form for creating a new NivelColegio.
     *
     * @return Response
     */
    public function create()
    {
        return view('nivel_colegios.create');
    }

    /**
     * Store a newly created NivelColegio in storage.
     *
     * @param CreateNivelColegioRequest $request
     *
     * @return Response
     */
    public function store(CreateNivelColegioRequest $request)
    {
        $input = $request->all();

        $nivelColegio = $this->nivelColegioRepository->create($input);

        Flash::success('Nivel Colegio saved successfully.');

        return redirect(route('nivelColegios.index'));
    }

    /**
     * Display the specified NivelColegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            Flash::error('Nivel Colegio not found');

            return redirect(route('nivelColegios.index'));
        }

        return view('nivel_colegios.show')->with('nivelColegio', $nivelColegio);
    }

    /**
     * Show the form for editing the specified NivelColegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            Flash::error('Nivel Colegio not found');

            return redirect(route('nivelColegios.index'));
        }

        return view('nivel_colegios.edit')->with('nivelColegio', $nivelColegio);
    }

    /**
     * Update the specified NivelColegio in storage.
     *
     * @param  int              $id
     * @param UpdateNivelColegioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNivelColegioRequest $request)
    {
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            Flash::error('Nivel Colegio not found');

            return redirect(route('nivelColegios.index'));
        }

        $nivelColegio = $this->nivelColegioRepository->update($request->all(), $id);

        Flash::success('Nivel Colegio updated successfully.');

        return redirect(route('nivelColegios.index'));
    }

    /**
     * Remove the specified NivelColegio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            Flash::error('Nivel Colegio not found');

            return redirect(route('nivelColegios.index'));
        }

        $this->nivelColegioRepository->delete($id);

        Flash::success('Nivel Colegio deleted successfully.');

        return redirect(route('nivelColegios.index'));
    }

    public function transformNivelesColegio()
    {

        $fractal = new Manager();


        $nivelesColegios = NivelColegio::all();
        $nivelesColegios = $fractal->setSerializer(new Serializer())
            ->createData( new Collection( $nivelesColegios, new NivelesColegioTransformer ) )->toArray();

        dd($nivelesColegios);

    }

}
