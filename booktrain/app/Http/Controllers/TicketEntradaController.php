<?php

namespace App\Http\Controllers;

use App\DataTables\TicketEntradaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTicketEntradaRequest;
use App\Http\Requests\UpdateTicketEntradaRequest;
use App\Repositories\TicketEntradaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TicketEntradaController extends AppBaseController
{
    /** @var  TicketEntradaRepository */
    private $ticketEntradaRepository;

    public function __construct(TicketEntradaRepository $ticketEntradaRepo)
    {
        $this->ticketEntradaRepository = $ticketEntradaRepo;
    }

    /**
     * Display a listing of the TicketEntrada.
     *
     * @param TicketEntradaDataTable $ticketEntradaDataTable
     * @return Response
     */
    public function index(TicketEntradaDataTable $ticketEntradaDataTable)
    {
        return $ticketEntradaDataTable->render('ticket_entradas.index');
    }

    /**
     * Show the form for creating a new TicketEntrada.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_entradas.create');
    }

    /**
     * Store a newly created TicketEntrada in storage.
     *
     * @param CreateTicketEntradaRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketEntradaRequest $request)
    {
        $input = $request->all();

        $ticketEntrada = $this->ticketEntradaRepository->create($input);

        Flash::success('Ticket Entrada saved successfully.');

        return redirect(route('ticketEntradas.index'));
    }

    /**
     * Display the specified TicketEntrada.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            Flash::error('Ticket Entrada not found');

            return redirect(route('ticketEntradas.index'));
        }

        return view('ticket_entradas.show')->with('ticketEntrada', $ticketEntrada);
    }

    /**
     * Show the form for editing the specified TicketEntrada.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            Flash::error('Ticket Entrada not found');

            return redirect(route('ticketEntradas.index'));
        }

        return view('ticket_entradas.edit')->with('ticketEntrada', $ticketEntrada);
    }

    /**
     * Update the specified TicketEntrada in storage.
     *
     * @param  int              $id
     * @param UpdateTicketEntradaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketEntradaRequest $request)
    {
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            Flash::error('Ticket Entrada not found');

            return redirect(route('ticketEntradas.index'));
        }

        $ticketEntrada = $this->ticketEntradaRepository->update($request->all(), $id);

        Flash::success('Ticket Entrada updated successfully.');

        return redirect(route('ticketEntradas.index'));
    }

    /**
     * Remove the specified TicketEntrada from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            Flash::error('Ticket Entrada not found');

            return redirect(route('ticketEntradas.index'));
        }

        $this->ticketEntradaRepository->delete($id);

        Flash::success('Ticket Entrada deleted successfully.');

        return redirect(route('ticketEntradas.index'));
    }
}
