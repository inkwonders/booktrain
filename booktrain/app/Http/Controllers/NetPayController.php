<?php

namespace App\Http\Controllers;

use Exception;
use Normalizer;
use App\Models\Pedido;
use GuzzleHttp\Client;
use App\Models\Direccion;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\ReferenciaPedido;
use League\Fractal\Resource\Item;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
//use League\Fractal\Serializer\Serializer;
use App\Transformers\Serializer;
use App\Transformers\PedidoPagadoTransformer;
use App\Http\Requests\API\UpdatePedidoAPIRequest;
use App\Transformers\ReferenciaPedidosTransformer;

class NetPayController extends AppBaseController
{
    /**
     * Gestiona los pagos del formulario de pago
     */
    public function postPagar(UpdatePedidoAPIRequest $request, $pedido_id) {
        $user = Auth::user();

        $pedido = $user->pedidos()->where('status', 'CARRITO')->find($pedido_id); // Solo se procesan los pedidos que estén en el carrito
        $pago = $request->all();

        if (empty($pedido))
            return response()->json(['success' => false, 'message' => 'El pedido no existe o es inválido.'], 404);

        if(( $request->metodo_pago == 'CREDITO' || $request->metodo_pago == 'AMEX' ) && $request->forma_pago != 'PUE' && $pago['tarjeta']['type'] == 'debit') {
            return response()->json(['success' => false, 'message' => 'No se admite un pago a MSI con tarjetas de débito'], 400);
        }

        //Log::debug("Aplicación de cargos", ['metodo_pago' => $request->metodo_pago, "forma_pago" => $request->forma_pago, "tarjeta" => $pago['tarjeta']['type']]);

        // Almacenamos la información del pedido
        // Si el pago procede continuamos a editar el registro del pedido para agregar su información del pago
        DB::beginTransaction();

        try {
            $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->first();
            $forma_pago = FormaPago::where('codigo', $request->forma_pago)->first();

            $pedido->update([
                'nombre_contacto'       => $request->contacto['nombre'],
                'apellidos_contacto'    => $request->contacto['apellidos'],
                'celular_contacto'      => $request->contacto['celular'],
                'correo_contacto'       => $request->contacto['correo'],
                'tipo_entrega'          => $request->tipo_entrega,
                'terminos_condiciones'  => $request->terminos_aceptados,
                // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                'metodo_pago'           => $request->metodo_pago,
                'forma_pago'            => $request->forma_pago,
                'status'                => 'PROCESADO',
                'metodo_pago_id'        => $metodo_pago->id,
                'forma_pago_id'         => $forma_pago->id,
            ]);

            if($request->facturacion_requerida) {
                // SEGUN EL SAT
                // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T, DEBITO, ID 3 = T. CREDITO

                $forma_pago_factura = 1;
                $metodo_pago_factura = 1;

                switch($request->forma_pago) {
                    case 'PUE': $metodo_pago_factura = 1; break;
                    default: $metodo_pago_factura = 2; break;
                }

                switch($request->metodo_pago) {
                    case 'TIENDA': $forma_pago_factura = 1; break;
                    case 'DEBITO': $forma_pago_factura = 2; break;
                    case 'CREDITO': $forma_pago_factura = 3; break;
                    case 'AMEX': $forma_pago_factura = 3; break;
                }

                // Eliminamos los datos anteriores
                $datos_factura = $pedido->datosFactura;
                if($datos_factura != null) $datos_factura->delete();

                $metodo_pago = null;

                switch ($metodo_pago_factura) {
                    case 1: $metodo_pago = 'PUE';break;
                    case 2: $metodo_pago = 'PDD';break;
                }

                $forma_pago = null;

                switch ($forma_pago_factura) {
                    case 1: $forma_pago = '01-EFECTIVO';break;
                    case 2: $forma_pago = '04-TARJETA CREDITO';break;
                    case 3: $forma_pago = '28-TARJETA DEBITO';break;
                }

                $pedido->datosFactura()->create([
                    'user_id'        => $pedido->usuario_id,
                    'id_cfdi'        => $request->facturacion['id_cfdi'],
                    'metodo_pago_id' => $metodo_pago_factura,
                    'forma_pago_id'  => $forma_pago_factura,
                    'forma_pago'     => $forma_pago,
                    'metodo_pago'    => $metodo_pago,
                    'razon_social'   => $request->facturacion['razon_social'],
                    'rfc'            => $request->facturacion['rfc'],
                    'correo'         => $request->facturacion['correo'],
                    'cp'             => $request->facturacion['cp'],
                    'calle'          => $request->facturacion['calle'],
                    'num_exterior'   => $request->facturacion['num_exterior'],
                    'num_interior'   => $request->facturacion['num_interior'],
                    'colonia'        => $request->facturacion['colonia'],
                    'municipio'      => $request->facturacion['municipio'],
                    'estado'         => $request->facturacion['estado'],
                    'regimen_fiscal' => $request->facturacion['regimen_fiscal'],

                ]);

                $pedido->update(['factura' => 1]);
            }

            $direccion = $pedido->direccion()->dissociate(); // <<<< Verificar que eso sea correcto

            if ($request->tipo_entrega == 2) { // a domicilio
                $direccion = Direccion::create([
                        'calle'        => $request->input('direccion.calle', ''),
                        'no_exterior'  => $request->input('direccion.no_exterior', ''),
                        'no_interior'  => $request->input('direccion.no_interior', ''),
                        'colonia'      => $request->input('direccion.colonia', ''),
                        'cp'           => $request->input('direccion.cp', ''),
                        'estado_id'    => $request->input('direccion.estado_id', ''),
                        'municipio_id' => $request->input('direccion.municipio_id', ''),
                        'referencia'   => $request->input('direccion.referencia', '')
                    ]);

                    $pedido->direccion()->associate($direccion);
                    $pedido->save();

                    // Eliminamos la información del envio anterior
                    $envio = $pedido->envio;
                    if($envio != null) $envio->delete();

                    // Creamos el registro en la tabla de envios
                    $pedido->envio()->create([
                        'pedido_id' => $pedido->id,
                        'status' => 'PROCESANDO',
                        'costo' => $pedido->colegio->config('costo_envio')
                    ]);
            }else {
                $pedido->direccion()->associate(Direccion::find($request->id_direccion_envio));
            }
            $pedido->save();

        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->sendError($th->getMessage(), 503);
        }

        DB::commit();

        /** se vuelve a obtener el pedido para obtener los datos actualizados */
        $pedido->refresh();

        // PREPARAMOS EL OBJETO DE CONFIGURACIÓN DEL PAGO
        $configuracion_pago = (Object) [
            // "source"        => $pago['tarjeta']['token'],
            // "paymentMethod" => "card",
            "amount"        => "$pedido->total",
            "description"   => "Compra de libros",
            "currency"      => "MXN",
            "billing" => [
                "firstName"                => $pago['contacto']['nombre'],
                "lastName"                 => $pago['contacto']['apellidos'],
                "email"                    => $pedido->user->email, //$pago['contacto']['email'],
                "phone"                    => $pago['contacto']['celular'],
                "merchantReferenceCode"    => uniqid("PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-")
            ],
            "ship" => (Object) [
                "city"           => $pago['direccion']['municipio'],//"La Magdalena Contreras",
                "country"        => "MX",
                "firstName"      => $pago['contacto']['nombre'],//"Juan Diego Correa",
                "lastName"       => $pago['contacto']['apellidos'],//"",
                "phoneNumber"    => $pago['contacto']['celular'],//"1234567890",
                "postalCode"     => '', //$pago['contacto']['cp'],//"10000",
                "state"          => $pago['direccion']['estado'],//"Ciudad de M\u00e9xico",
                "street1"        => $pago['direccion']['calle'],//"Primera",
                "street2"        => $pago['direccion']['colonia'],//"Centro",
                "shippingMethod" => "CUSTOM" //$pago['direccion']['']//"flatrate_flatrate" // <<<<<<<<<<<<<<<<<<<<<Anotar aquí el método de envío
            ],
            "redirect3dsUri" => route('netpay.3ds.continuar'),
            "saveCard"       => false
        ];

        // DETERMINAMOS EL TIPO DE PAGO QUE SE VA A REALIZAR

        $base_uri = env('NETPAY_BASE_URI_SANDBOX', 'https://gateway-154.netpaydev.com/');
        if(env('NETPAY_SANDBOX_MODE', 1) == 0)
            $base_uri = env('NETPAY_BASE_URI', 'https://suite.netpay.com.mx/');

        $client = new Client(
            ['base_uri' => $base_uri]
        );

        // CASO 1: Referencia para tienda de conveniencia
        if($pedido->metodo_pago == 'TIENDA') {
            $configuracion_pago->paymentMethod = 'cash';

            try {
                $netpay_sk = env('NETPAY_SK_SANDBOX', '');
                if(env('NETPAY_SANDBOX_MODE', 1) == 0)
                    $netpay_sk = env('NETPAY_SK', '');

                $raw_response = '';

                Log::debug("PedidoAPIControlle@postPagar (TIENDA)", ['pedido_id' => $pedido->id, 'configuracion_pago_referencia_tienda' => $configuracion_pago]);

                // Aplicamos los cargos con la información recibida
                $response = $client->post('/gateway-ecommerce/v3/charges', [
                    'headers' => [
                        'Authorization' => $netpay_sk,
                        'Accept'        => 'application/json',
                        'Content-Type'  => 'application/json'
                    ],
                    'body' => json_encode($configuracion_pago)
                ]);

                // Entre estas dos lineas se ejecuta la recepción de la llamda del webhook con event cep.created

                $raw_response = $response->getBody()->getContents();
                $response = json_decode($raw_response);
                Log::debug("Aplicación de cargos (respuesta de Netpay):", ['response' => $response]);
            } catch (\GuzzleHttp\Exception\ClientException $th) {
                $raw_error = $th->getResponse()->getBody()->getContents();
                $error = json_encode($raw_error);
                Log::error("Error al aplicar cargos", ['error' => $error, 'message' => $th->getMessage()]);

                DB::rollback();

                $pedido->update([ 'status' => 'CARRITO' ]);
                return $this->sendError($error, $th->getCode());
            }

            Log::debug("Se creara la referencia: ", [
                'pedido_id'     => $pedido->id,
                'referencia'    => $response->paymentSource->cashDto->reference,
                'tipo'          => 'TIENDA',
                'dias_vigencia' => $response->paymentSource->cashDto->expireInDays,
                'pagado'        => 0,
                'fecha_pago'    => NULL,
                'response'      => $raw_response,
            ]);

            $referencia_previa = ReferenciaPedido::where('referencia', $response->paymentSource->cashDto->reference)->get();

            if($referencia_previa->count() > 0) {
                Log::error("NetPayController@postPagar", ['error' => 'Ya existe una referencia igual registrada', 'anterior' => $referencia_previa->pluck('id')]);

                return $this->sendError('Error al generar la referencia, reintente más tarde', 503 );
            }else{
                $pedido->referencia()->create([
                    'referencia'    => $response->paymentSource->cashDto->reference,
                    'tipo'          => 'TIENDA',
                    'dias_vigencia' => $response->paymentSource->cashDto->expireInDays,
                    'pagado'        => 0,
                    'fecha_pago'    => NULL,
                    'response'      => $raw_response,
                ]);

                $pedido->update([ 'status' => 'PROCESADO' ]);
            }
        }

        // CASO 2: Pago con tarjeta / Pago con 3DS
        if( in_array( $pedido->metodo_pago, ['CREDITO', 'DEBITO', 'AMEX'] )) {
            $configuracion_pago->paymentMethod = 'card';
            $configuracion_pago->source = $pago['tarjeta']['token'];

            $configuracion_pago->billing['address'] = (Object) [
                "city"       => normalizer_normalize($pago['tarjeta']['direccion']['municipio'], Normalizer::FORM_D),
                "country"    => 'MX', // http://utils.mucattu.com/iso_3166-1.html
                "postalCode" => $pago['tarjeta']['direccion']['cp'],
                "state"      => normalizer_normalize($pago['tarjeta']['direccion']['estado'], Normalizer::FORM_D),// Usar catalogo
                "street1"    => $pago['tarjeta']['direccion']['calle'],
                "street2"    => $pago['tarjeta']['direccion']['colonia']
            ];

            if(in_array( $pedido->metodo_pago, ['CREDITO', 'AMEX'] ) && in_array( $pago['tarjeta']['type'], ['credit', 'unknown'])) {
                $configuracion_pago->installments = (Object) [
                    'plan' => [
                        "count" => $pago['meses'],
                        "interval" => "month"
                    ]
                ];
            }

            try {
                $netpay_sk = env('NETPAY_SK_SANDBOX', '');
                if(env('NETPAY_SANDBOX_MODE', 1) == 0)
                    $netpay_sk = env('NETPAY_SK', '');

                Log::debug(   "Se envía a Netpay: ", ['body' => json_encode($configuracion_pago)]   );

                // Aplicamos los cargos con la información recibida
                $raw_response = $client->post('/gateway-ecommerce/v3/charges', [
                    'headers' => [
                        'Authorization' => $netpay_sk,
                        'Accept'        => 'application/json',
                        'Content-Type'  => 'application/json'
                    ],
                    'body' => json_encode($configuracion_pago)
                ])->getBody()->getContents();

                Log::debug("Cargo a tarjeta", ['raw_response' => $raw_response]);
                 //{"source":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","amount":51.0,"description":"Compra de libros","status":"success","transactionTokenId":"7716e730-61e3-459c-bfc9-56de57befe72","redirect3dsUri":"http://booktrain.test/mis-pedidos/","returnUrl":null,"paymentMethod":"card","currency":"MXN","createdAt":"2021-05-07T16:26:22.949+0000","error":null,"installments":null,"ship":{"city":"Aguascalientes","country":"MX","firstName":"sfsfsd","lastName":"sdfsfsd","phoneNumber":"1651561544","postalCode":"","state":"Aguascalientes","street1":"Bolivar","street2":"algarin","shippingMethod":"CUSTOM"},"client":null,"saveCard":false,"instegrationsdk":null,"integrationSdkVersion":null,"cvv":null,"merchantRefCode":null,"seamless":false,"paymentSource":{"cardDefault":false,"card":{"token":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","expYear":"22","expMonth":"11","lastFourDigits":"0002","cardHolderName":"dfgdfgdfg dfgdgd","brand":"visa","deviceFingerPrint":"1620404762040","ipAddress":"2806:103e:21:20a9:7839:8cf8:1dd3:af47","bank":"Test bank","type":"debit","country":"unknown","scheme":"unknown","cardPrefix":"400000","preAuth":false,"vault":false,"simpleUse":false,"integrationSdkVersion":"1.0.0"},"source":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","type":"card"},"billing":{"firstName":"sfsfsd","lastName":"sdfsfsd","email":"accept@netpay.com.mx","phone":"1651561544","ipAddress":"2806:103e:21:20a9:7839:8cf8:1dd3:af47","merchantReferenceCode":"compra-","address":{"city":"Singuilucan","country":"MX","postalCode":"dfgdfgd","state":"Hidalgo","street1":"gdgdfgdf","street2":"dfgdfgdf"}}}
                $response = json_decode($raw_response);
            } catch (\GuzzleHttp\Exception\ClientException $th) {
                $raw_error = $th->getResponse()->getBody()->getContents();
                $error = json_decode($raw_error);
                Log::error("Cargo a tarjeta Error", ['error' => $raw_error, 'message' => $th->getMessage()]);

                // Guardamos la información del pago en el pedido
                $pedido->pagos()->create([
                    'amount'                => $pedido->total,
                    'source'                => '',
                    'transactionTokenId'    => '',
                    'status'                => 'FAILED',
                    'raw'                   => $raw_error,
                    'origen'                => 'API'
                ]);

                $pedido->update([ 'status' => 'CARRITO' ]);
                //"{\"timestamp\":\"2021-06-24T18:53:44.913+0000\",\"status\":400,\"error\":\"Bad Request\",\"message\":\"Error obtaining authentication credentials\",\"path\":\"\/gateway-ecommerce\/v3\/charges\"}"
                return $this->sendError($error, $th->getCode());
            }

            // Guardamos la información del pago en el pedido
            $pedido->pagos()->create([
                'amount'                => $pedido->total,
                'source'                => $response->source,
                'transactionTokenId'    => $response->transactionTokenId,
                'status'                => $response->status,
                'raw'                   => $raw_response,
                'origen'                => 'API'
            ]);

            // Pago aprobado
            if($response->status == 'success'){
                $pedido->update([
                    'status' => 'PAGADO',
                    'comision' => $pedido->comision // Calculamos la comisión y la guardamos
                 ]);
            }else{
                DB::commit();
            }

            // Obtenemos la información de pago y comprobamos el status de este, si no procede respondemos como error en el pago
            if($response->error != null){
                $pedido->update(['status' => 'CARRITO']);
                DB::commit();
                return $this->sendError( $response->error, 503 );
            }

            // Requiere 3DS
            if($response->status == 'review'){
                $pedido->update(['status' => 'PAGO EN REVISION']);
                DB::commit();
                return $this->sendResponse(['action' => '3DS', 'returnUrl' => $response->returnUrl, 'success' => true], 'Se requiere 3DS');
            }

            // Pago rechazado
            if($response->status == 'failed') {
                $pedido->update(['status' => 'PAGO RECHAZADO']);
                DB::commit();
                return $this->sendError('La transacción no fue aprobada: ' . $response->error, 403 );
            }
        }

        DB::commit();

        return $this->procesarPedido($pedido);
    }

    /**
     * Procesamos el pedido después del pago y regresamos la respuesta según la naturaleza de este
     */
    private function procesarPedido($pedido) {
        DB::beginTransaction();
        try {
            $pedido->save();
            $pedido->refresh();
            $pedido->procesar();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::debug("procesarPedido", ['message' => $th->getMessage()]);
            return $this->sendError('Error al procesar el pedido: ' . $th->getMessage(), 503);
        }

        DB::commit();

        $comision = $pedido->comision;
        $total = $pedido->total;

        $pedido->update([
            'comision' => $comision,
            'total' => $total
        ]);

        // DETERMINAMOS EL TIPO DE RESPUESTA A REGRESAR
        $fractal = new Manager();
        $informacion = $fractal->setSerializer(new Serializer());

        // CASO 1: Referencia para tienda de conveniencia
        if($pedido->metodo_pago == 'TIENDA')
            $datosPedido = $informacion
                ->parseIncludes(['referencia_tienda'])
                ->createData(new Item($pedido, new ReferenciaPedidosTransformer))
                ->toArray();

        // CASO 2: Pago con tarjeta
        if($pedido->metodo_pago == 'CREDITO' || $pedido->metodo_pago == 'DEBITO' || $pedido->metodo_pago == 'AMEX')
            $datosPedido = $informacion->createData(new Item($pedido, new PedidoPagadoTransformer))->toArray();

        return $this->sendResponse($datosPedido, 'Pedido ' . $pedido->id . ' actualizado correctamente');
    }

    /**
     * Recibe la petición del webhook para continuar la petición del 3DS
     */
    public function getWebhook3DS(Request $request)
    {
        $transactionTokenId = $request->get('transaction_token', '');
        Log::debug("Entrando a WebHook 3DS", $request->all());

        // BUSCAMOS EL PAGO
        $base_uri = env('NETPAY_BASE_URI_SANDBOX', 'https://gateway-154.netpaydev.com/');
        if (env('NETPAY_SANDBOX_MODE', 1) == 0)
            $base_uri = env('NETPAY_BASE_URI', 'https://suite.netpay.com.mx/');

        $client = new Client(
            ['base_uri' => $base_uri]
        );

        try {
            $netpay_sk = env('NETPAY_SK_SANDBOX', '');
            if (env('NETPAY_SANDBOX_MODE', 1) == 0)
                $netpay_sk = env('NETPAY_SK', '');

            $raw_response = '';
            $response = $client->get("/gateway-ecommerce/v3/transactions/$transactionTokenId", [
                'headers' => [
                    'Authorization' => $netpay_sk,
                    'Accept'        => 'application/json',
                    'Content-Type'  => 'application/json'
                ]
            ]);

            $raw_response = $response->getBody()->getContents();
            $response = json_decode($raw_response);

            $pedido = Pedido::whereStatus('PAGO EN REVISION')
                ->whereHas('pagos', function ($query) use ($transactionTokenId) {
                    // Filtramos el pedido correspondiente a la referencia
                    $query->where('transactionTokenId', $transactionTokenId);

                    // Validamos que el pago contenga la cadena de comprobación correspondiente al pedido
                    $query->whereRaw('raw like concat("%PED-", pedidos.id, "-%")');
                })
                ->first();

            // El pedido no existe o ha caducado
            if ( ! $pedido)
                return view('private.resultado3ds')->with(['success' => false, 'message' => 'Tiempo de espera agotado']);

            // Guardamos la consulta del estatus
            $pedido
                ->pagos()
                ->where('transactionTokenId', $transactionTokenId)
                ->update(['status_3ds' => $raw_response]);

            Log::debug("Respuesta 3DS", ['response' => $response]);
        } catch (\GuzzleHttp\Exception\ClientException $th) {
            Log::error("Error al revisar estado de 3DS", ['Mensaje' => $th->getMessage()]);
            $error = json_decode($th->getResponse()->getBody()->getContents());

            return view('private.resultado3ds')->with(['success' => false, 'error' => $error->message, 'message' => $th->getMessage()]);
        }

        /**
         * {
         *   "transactionTokenId": "d6937b5f-8806-44ba-a32f-439e60adf42d"
         *   "status": "CHARGEABLE"
         *   "merchantReferenceCode": "-"
         *   "currency": null
         *   "amount": 100.0
         *   "timeIn": "2021-05-12T08:12:29.000+0000"
         *   "timeOut": "2021-05-12T08:12:54.000+0000"
         *   "responseCode": null
         *   "responseMsg": "Success"
         *   "authCode": null
         *   "spanRouteNumber": "0002"
         *   "cardHolderName": "ghhfghfgh dfhdfhf"
         *   "billToEmail": "review@netpay.com.mx"
         *   "bankName": "JPMORGAN CHASE BANK"
         *   "paymentMethod": "card"
         *   "externalReference": null
         *   "expireDate": null
         *   "dayToExpirePayment": null
         *  }
         */
        if ($response->status == 'DONE') {
            if($pedido->status == 'PAGO EN REVISION') {
                $pedido->update(['status' => 'PAGADO']);
            }

            if($pedido->status == 'CANCELADO') {
                $pedido->update(['status' => 'PAGADO']);

                // Este caso no debería existir, consultar si es necesario procesar el pedido después de esto
                // $pedido->procesar();
                Log::error('NetPayController@getWebhook3DS: Discrepancia con el pedido', ['pedido_id' => $pedido->id, 'token' => $transactionTokenId, 'error' => 'El pedido está marcado como cancelado pero el pago es exitoso']);
            }

            // Esperando a que termine la pantalla de 3DS
            return view('private.resultado3ds')->with(['success' => true, 'message' => 'El pago ya ha sido procesado']); // Este código indica que el servidor ha recibido la solicitud y aún se encuentra procesandola, por lo que no hay respuesta disponible.
        }

        if ($response->status == 'WAIT_THREEDS') {
            // Esperando a que termine la pantalla de 3DS
            return view('private.resultado3ds')->with(['success' => false, 'message' => 'El proceso 3DS aun no termina']); // Este código indica que el servidor ha recibido la solicitud y aún se encuentra procesandola, por lo que no hay respuesta disponible.
        }

        if ($response->status == 'CHARGEABLE') {
            // PROCEDEMOS A CONFIRMAR EL CARGO
            try {
                $netpay_sk = env('NETPAY_SK_SANDBOX', '');
                if (env('NETPAY_SANDBOX_MODE', 1) == 0)
                    $netpay_sk = env('NETPAY_SK', '');

                $raw_response = '';

                $response = $client->post("/gateway-ecommerce/v3/charges/$transactionTokenId/confirm", [
                    'headers' => [
                        'Authorization' => $netpay_sk,
                        'Accept'        => 'application/json',
                        'Content-Type'  => 'application/json'
                    ]
                ]);

                $raw_response = $response->getBody()->getContents();
                $response = json_decode($raw_response);

                //guardamos la confirmación del pago
                $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->update(['confirm' => $raw_response]);

                Log::debug("Respuesta a confirmación 3DS", ['response' => $response]);
            } catch (\Exception $th) {
                //DB::rollback();

                Log::error("Error al confirmar 3DS", ['mensaje' => $th->getMessage()]);

                return view('private.resultado3ds')->with(['success' => false, 'message' => $th->getMessage()]);
            }

            //Analisis del resultado de la confirmación
            //$pago = $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->first();

            if ($response->status == "success") {

                Log::debug("El resultado de la confirmación de 3DS fue satisfactorio, se procede a procesar el pedido");

                $pedido->update(['status' => 'PAGADO']);

                // Continuamos el proceso de pago
                $pedido->procesar();

                //TODO: cambiar el estatus del pago de REVIEW a SUCCESS (verificar si al pasar la confirmación del pago si sale en la consulta como SUCCESS)
                $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->update(['status' => 'SUCCESS']);
                return view('private.resultado3ds')->with(['success' => true, 'message' => 'El pago ha sido procesado correctamente']); //"OK";
            }

            if ($response->status == "failed") {
                Log::error("El resultado de la confirmación de 3DS fue NO satisfactorio");

                $pedido->cancelar();
                $pedido->update(['status' => 'PAGO RECHAZADO']);

                //TODO: Evaluar si se agrega un nuevo status (REJECT)
                $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->update(['status' => 'FAILED']);
                return view('private.resultado3ds')->with(['success' => false, 'message' => $response->error]);
            }

            //Si llega aqui es por que el resultado de la confirmación es diferente de success o failed y es algo anormal
            Log::error("El resultado de la confirmación de 3DS obtuvo una respuesta inesperada");
            return view('private.resultado3ds')->with(['success' => false, 'message' => $response->error]);
        }

        if ($response->status == 'REJECT') {
            Log::error("La consulta del estatus obtuvo un status de REJECT (" . $response->responseMsg . ")");

            $pedido->cancelar();

            $pedido->update(['status' => 'PAGO RECHAZADO']);

            //TODO: Evaluar si se agrega un nuevo status (REJECT)
            $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->update(['status' => 'FAILED']);

            return view('private.resultado3ds')->with(['success' => false, 'message' => 'La tarjeta fue rechazada (' . $response->responseMsg . ')']);
        }
    }

    /**
     * =======================================================================================
     * Recibe la petición del webhook informandonos de la creación o pago de una referencia
     * =======================================================================================
     *
     * Ejemplo de referencia creada:
     *
     * {
     * "data": {
     *     "transactionId": "1f297cb0-5e1a-4023-8268-def10246710a",
     *     "reference": "1161630012130519211",
     *     "transactionType": "BANORTEPAY",
     *     "amount": 300
     *     },
     *     "event": "cep.paid",
     *     "type": "cep",
     *     "createdAt": "1592930288"
     * }
     *
     * Ejemplo de referencia pagada:
     *
     * {
     *    "data": {
     *        "transactionId": "1fc8f6ae-ab01-448a-b4d6-52950327ae43",
     *        "reference": "1311222215131407214",
     *        "merchantReferenceCode": "PED-852-PR-2550-60bf8a63a1438",
     *        "transactionType": "BANORTEPAY",
     *        "transactionStatus": "DONE",
     *        "durationInDays": 0,
     *        "amount": 612,
     *        "sessionId": null
     *    },
     *    "event": "cep.paid",
     *    "type": null,
     *    "createdAt": 1623315622665,
     *    "transactionTokenId": "1fc8f6ae-ab01-448a-b4d6-52950327ae43"
     * }
     */
    public function webhook(Request $request) {
        Log::info('NetPayController@webhook', $request->all());

        try {
            $webhook_request = json_decode(json_encode($request->all()), FALSE);
        } catch (Exception $e) {
            Log::error('NetPayController@webhook Error al decodificar los datos recibidos: ' . $e->getMessage());
            return response()->json(['success' => false, 'message' => 'Error al decodificar los datos recibidos'], 200);
        }

        switch($webhook_request->event) {
            case 'cep.paid':
                // Evento de pago de una referencia de tienda
                try {
                    $referencia_pago = $webhook_request->data->reference;

                    $pedido = Pedido::with([
                        'referencias' => function($query) use ($referencia_pago) {
                            $query->where('referencia', $referencia_pago );
                            $query->where('pagado', 0);
                        }
                    ])
                    ->whereHas('referencias', function($query) use ($referencia_pago) {
                        $query->where('referencia', $referencia_pago);
                        $query->withCount('referenciasDuplicadas');
                        $query->having('referencias_duplicadas_count', '=', 1);
                        $query->where('pagado', 0);
                    })
                    ->first();

                    // Validamos que la referencia corresponda al codigo de venta del pedido
                    if( ! $pedido || ! Str::startsWith(
                        $webhook_request->data->merchantReferenceCode,
                        "PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-"
                        )
                    ) {
                        Log::error('NetPayController@webhook La referencia no corresponde al pedido: ', ['referencia' => $webhook_request->data->merchantReferenceCode, 'pedido' => "{$pedido->serie}{$pedido->folio} ({$pedido->id})"]);
                    }
                } catch (Exception $e) {
                    Log::error('Error al obtener la referencia: ' . $e->getMessage());
                    return response()->json(['success' => false, 'message' => 'Formato de informacion invalido, error al obtener la referencia, '.$e->getMessage()], 200);
                }

                if( ! $pedido) {
                    Log::error('NetPayController@webhook: No se encontró el pedido', ['referencia' => $webhook_request->data->reference]);
                    return response()->json(['success' => false, 'message' => 'No se encuentra el pedido'], 404);
                }

                $referencia_pedido = $pedido->referencias()->first();

                if( ! Str::contains($referencia_pedido->response, "PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}") ) {
                    Log::error('NetPayController@webhook: El registro del pago no corresponde al pedido', ['referencia' => $webhook_request->data->reference]);
                    return response()->json(['success' => false, 'message' => 'El registro del pago no corresponde al pedido'], 404);
                }

                if (!is_null($referencia_pedido)) {

                    // si el pedido ya esta pagado no se realiza ningúna acción
                    if ($referencia_pedido->pagado == 1)
                        return response()->json(['success' => true, 'message' => 'El pedido ya se había pagado'], 200);

                    if($pedido->pagos()->where('status', 'SUCCESS')->sharedLock()->count() == 0) {
                        if ($webhook_request->data->transactionStatus == 'DONE') {

                            $referencia_pedido->pagado = 1;
                            $referencia_pedido->fecha_pago = Carbon::now();
                            //$referencia_pedido->response = json_encode($request->all());

                            $referencia_pedido->save();

                            $pedido->status = 'PAGADO';
                            $pedido->metodo_pago = 'TIENDA';
                            $pedido->save();

                            // Guardamos la información del pago en el pedido
                            $pedido->pagos()->create([
                                'amount'                => $webhook_request->data->amount,
                                'source'                => $webhook_request->data->reference,
                                'transactionTokenId'    => $webhook_request->data->transactionId,
                                'status'                => 'SUCCESS',
                                //'raw'                   => $referencia_pedido->response,
                                'raw'                   => json_encode($request->all()),
                                'origen'                => 'WEBHOOK'
                            ]);

                            $pedido->sendPagoExitosoNotification();

                            return response()->json(['success' => true, 'message' => 'El pago ha sido registrado correctamente'], 200);
                        }else{
                            return response()->json(['success' => false, 'message' => 'El estado del pago no es valido: ' . $webhook_request->data->transactionStatus], 400);
                        }
                    }else{
                        Log::error("NetPayController@webhook", ['error' => 'El pedido ya tiene un pago con esa referencia']);
                        return response()->json(['success' => false, 'message' => 'La referencia ya se encuentra registrada'], 409);
                    }
                } else {
                    Log::info('No se encontro la referencia: ' . $webhook_request->data->reference);
                    return response()->json(['success' => false, 'message' => 'No se encontro la referencia'], 409);
                }
            break;
            case 'cep.created':
                // Respuesta de NetPay Checkout Hosted con Referencia Creada
                Log::info('return, cep.created');
                return response()->json(['success' => true, 'message' => 'cep.created'], 200);
            break;
            case 'sessionLink.paid':
                // Checkout Hosted
                Log::info('return, sessionLink.paid');
                return response()->json(['success' => true, 'message' => 'sessionLink.paid'], 200);
            break;
            case 'sessionLink.failed':
                // Checkout Hosted
                Log::info('return 200, sessionLink.failed');
                return response()->json(['success' => true, 'message' => 'sessionLink.failed'], 200);
            break;
            case 'subscription.paid':
                // Loop pago exitoso
                Log::info('return 200, subscription.paid');
                return response()->json(['success' => true, 'message' => 'subscription.paid'], 200);
            break;
            case 'subscription.payment_failed':
                // Loop pago fallido
                Log::info('return 200, subscription.payment_failed');
                return response()->json(['success' => true, 'message' => 'subscription.payment_failed'], 200);
            break;
            default:
                // Evento desconocido
                Log::info('return 409, evento desconocido');
                return response()->json(['success' => true, 'message' => 'evento desconocido'], 409);
            break;
        }
    }
}
