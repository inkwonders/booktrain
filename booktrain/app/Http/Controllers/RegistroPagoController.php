<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\Colegio;
use Illuminate\Http\Request;
use App\Models\DetalleResumenPedido;
// use App\Http\Controllers\Carbon;
use Carbon\Carbon;

use App\Models\RegistroPago;

class RegistroPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getReporteCierreCajaValues(Request $request) {
        $request->validate([
            'colegio' => 'required|exists:colegios,id',
            'caja' => 'required|exists:cajas,id',
            'fin' => 'required'
        ]);

        $colegio = Colegio::find($request->colegio);
        $fecha = Carbon::create($request->fin);
        $caja = $colegio->cajas()->where('id', $request->caja)->first();

        //TODO: Verificar que esto este bien, no solo debe salir información de esa caja especifica?
        $pedidos = $caja->pedidos()
            ->wherePivotBetween('created_at', [$request->fin . ' 00:00:00', $request->fin . ' 23:59:59'])
            ->get();

        $pedidos_separados = $pedidos->partition(function ($pedido) {
            return $pedido->status == 'ENTREGADO';
        });

        $pedidos_entregados = $pedidos_separados[0]->groupBy('metodo_pago_id');

        $pedidos_efectivo = isset($pedidos_entregados[6]) ? $pedidos_entregados[6] : collect();
        $pedidos_terminal = isset($pedidos_entregados[7]) ? $pedidos_entregados[7] : collect();
        $pedidos_transferencia = isset($pedidos_entregados[8]) ? $pedidos_entregados[8] : collect();

        $pedidos_cancelados = $pedidos_separados[1];

        $operacion_caja = $caja->operaciones->last();

        $devoluciones = $pedidos->sum('descuento_devolucion');

        return [
            'colegio' => $request->colegio,
            'fecha_raw' => $request->fin,
            'caja' => $request->caja,
            'nombre_colegio' => $colegio->nombre,
            'logo_colegio' => $colegio->logo,
            'fecha' => $fecha->format('d/m/Y'),

            'pago_efectivo' => $pedidos->sum('total'),

            'devoluciones' => $devoluciones,
            'total_efectivo' => $pedidos_efectivo->sum('total'),
            'pago_tarjeta' => $pedidos_terminal->sum('total'),
            'pago_tienda' => 0,
            'pago_practicaja' => 0,
            'pago_transferencia' => $pedidos_transferencia->sum('total'),

            'total' => $pedidos->sum('total') - $devoluciones,

            'total_libros' => $pedidos->pluck('libros')->collapse()->count() - $pedidos->sum('libros_devueltos'),

            'desglose_efectivo' => [
                '500' => $operacion_caja ? $operacion_caja->billete_500 : 0,
                '200' => $operacion_caja ? $operacion_caja->billete_200 : 0,
                '100' => $operacion_caja ? $operacion_caja->billete_100 : 0,
                '50' => $operacion_caja ? $operacion_caja->billete_50 : 0,
                '20' => $operacion_caja ? $operacion_caja->billete_20 : 0,
                'monedas' => $operacion_caja ? $operacion_caja->monto_monedas : 0,
                'cantidad_monedas' => $operacion_caja ? $operacion_caja->conteo_monedas : 0,
                'total_desglosado' => $operacion_caja ? $operacion_caja->total : 0
            ]
        ];
    }

    public function getReporteGananciasDiarias(Request $request)
    {
        $data = $this->getReporteCierreCajaValues($request);
        return view('private.reportes.cierre_caja')->with($data);
    }

    public function printReporteGananciasDiarias(Request $request)
    {
        $data = $this->getReporteCierreCajaValues($request);

        $pdf = \PDF::loadView('private.reportes.print_cierre_caja', $data);
        $pdf->getDomPDF()->set_options(['dpi' => 150, 'defaultFont' => 'arial', 'enable_php' => true]);

        return $pdf->stream('cierre_caja.pdf');
    }
}
