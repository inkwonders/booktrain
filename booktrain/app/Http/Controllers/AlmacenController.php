<?php

namespace App\Http\Controllers;

use App\Models\Colegio;
use App\Models\MovimientoAlmacen;
use App\Models\TipoMovimientoAlmacen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
class AlmacenController extends Controller
{
    public function index() {
        return view('private.admin.almacen.index');
    }

    public function movimientos() {
        return view('private.admin.almacen.movimientos.index');
    }

    public function vermovimientos() {
        return view('private.admin.almacen.movimientos.vermovimientos');
    }

    public function movimientoManual($colegio_id, $tipo_movimiento_id, $operaciones, $comentarios) {
        $tipo_movimiento_almacen = TipoMovimientoAlmacen::find($tipo_movimiento_id);
        $colegio = Colegio::where('id', $colegio_id)->first();

        DB::beginTransaction();

        try {
            $operaciones->each(function($operacion) use ($colegio, $tipo_movimiento_almacen, $comentarios) {
                // Obtenemos el estado actual
                $libro = $colegio->libros()->wherePivot('paquete_id', null)->where('libro_id', $operacion['id'])->first();

                $stock_actual = $libro->pivot->stock;

                if($tipo_movimiento_almacen->naturaleza == 'ENTRADA') {
                    $nuevo_stock = $stock_actual + $operacion['cantidad'];
                }

                if($tipo_movimiento_almacen->naturaleza == 'SALIDA') {
                    $nuevo_stock = $stock_actual - $operacion['cantidad'];
                }

                // Afectamos al almacén
                $colegio->libros()->wherePivot('paquete_id', null)->updateExistingPivot($libro->id, [
                    'stock' => $nuevo_stock
                ]);

                // Registramos el movimiento
                $colegio->movimientosAlmacen()->create([
                    'tipo_movimiento_almacen_id' => $tipo_movimiento_almacen->id,
                    'libro_id'                   => $libro->id,
                    'user_id'                    => Auth::user()->id,
                    'cantidad'                   => $operacion['cantidad'],
                    'cantidad_anterior'          => $stock_actual,
                    'descripcion'                => "{$tipo_movimiento_almacen->descripcion} en el colegio {$colegio->id} [{$colegio->codigo}] al libro {$libro->id} ISBN [{$libro->isbn}] por la cantidad de {$operacion['cantidad']} pz [{$tipo_movimiento_almacen->naturaleza}]",
                    'comentarios'                => $comentarios
                ]);
            });
        } catch (\Throwable $th) {
            Log::error("AlmacenController:movimientoManual", ['error' => $th]);

            DB::rollback();
            return false;
        }

        DB::commit();
        return true;
    }

    public function traspasoInventarios($colegio_origen_id, $colegio_destino_id, $operaciones, $comentarios) {
        $colegio_origen = Colegio::where('id', $colegio_origen_id)->first();
        $colegio_destino = Colegio::where('id', $colegio_destino_id)->first();

        // Entrada por transferencia entre almacenes
        $operacion_entrada = TipoMovimientoAlmacen::where('clave', 'ENT06')->first();
        // Salida por transferencia entre almacenes
        $operacion_salida = TipoMovimientoAlmacen::where('clave', 'SAL03')->first();

        DB::beginTransaction();

        try {
            $operaciones->each(function($operacion) use ($colegio_origen, $colegio_destino, $operacion_entrada, $operacion_salida, $comentarios) {
                // ================================================================================================
                // REALIZAMOS LA OPERACIÓN DE SALIDA

                // Obtenemos el estado actual
                $libro_origen = $colegio_origen->libros()->wherePivot('paquete_id', null)->where('libro_id', $operacion['id'])->first();
                $stock_actual = $libro_origen->pivot->stock;
                // Restamos la cantidad a extraer del almacén
                if($operacion['cantidad'] > $stock_actual) {
                    // Solo se pueden transferir como máximo el total de unidades en stock
                    $nuevo_stock = 0;
                    $operacion['cantidad'] = $stock_actual;
                }else{
                    $nuevo_stock = $stock_actual - $operacion['cantidad'];
                }

                // Afectamos al almacén
                $colegio_origen->libros()->wherePivot('paquete_id', null)->updateExistingPivot($libro_origen->id, [
                    'stock' => $nuevo_stock
                ]);
                // Registramos el movimiento
                $colegio_origen->movimientosAlmacen()->create([
                    'libro_id' => $libro_origen->id,
                    'tipo_movimiento_almacen_id' => $operacion_salida->id,
                    'user_id' => Auth::user()->id,
                    'cantidad' => $operacion['cantidad'],
                    'cantidad_anterior' => $stock_actual,
                    'descripcion' => "{$operacion_salida->descripcion} en el colegio {$colegio_origen->id} [{$colegio_origen->codigo}] al colegio {$colegio_destino->id} [{$colegio_destino->codigo}] libro {$libro_origen->id} por la cantidad de {$operacion['cantidad']} pz [{$operacion_salida->naturaleza}]",
                    'comentarios' => $comentarios
                ]);

                // ================================================================================================
                // REALIZAMOS LA OPERACIÓN DE ENTRADA
                // Obtenemos el estado actual
                $libro_destino = $colegio_destino->libros()->wherePivot('paquete_id', null)->where('libro_id', $operacion['id'])->first();

                if($libro_destino == null) {
                    // El Colegio no tiene el libro en su inventario, procedemos a crearlo
                    $colegio_destino->libros()->attach($operacion['id'], [
                        'paquete_id' => null,
                        'obligatorio' => null,
                        'activo' => 1,
                        'bajo_pedido' => $libro_origen->pivot->bajo_pedido,
                        'precio' => $libro_origen->pivot->precio,
                        'punto_reorden' => 0,
                        'stock' => 0
                    ]);

                    $libro_destino = $colegio_destino->libros()->wherePivot('paquete_id', null)->where('libro_id', $operacion['id'])->first();
                }

                $stock_actual = $libro_destino->pivot->stock;
                // Restamos la cantidad a extraer del almacén

                $nuevo_stock = $stock_actual + $operacion['cantidad'];
                // Afectamos al almacén
                $colegio_destino->libros()->wherePivot('paquete_id', null)->updateExistingPivot($libro_destino->id, [
                    'stock' => $nuevo_stock
                ]);

                // Registramos el movimiento
                $colegio_destino->movimientosAlmacen()->create([
                    'libro_id' => $libro_destino->id,
                    'tipo_movimiento_almacen_id' => $operacion_entrada->id,
                    'user_id' => Auth::user()->id,
                    'cantidad' => $operacion['cantidad'],
                    'cantidad_anterior' => $stock_actual,
                    'descripcion' => "{$operacion_entrada->descripcion} del colegio {$colegio_origen->id} [{$colegio_origen->codigo}] al colegio {$colegio_destino->id} [{$colegio_destino->codigo}] libro {$libro_destino->id} por la cantidad de {$operacion['cantidad']} pz [{$operacion_entrada->naturaleza}]",
                    'comentarios' => $comentarios
                ]);
            });
        } catch (\Throwable $th) {
            Log::error("AlmacenController:traspasoInventarios", ['error' => $th]);

            DB::rollback();
            return false;
        }

        DB::commit();
        return true;
    }
}
