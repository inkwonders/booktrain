<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MercadoPagoController extends Controller
{
    public function respuestaMercadoPago(Request $request){

        switch($request->res){
            case 'success':
                return view('private.resultado3ds')->with(['success' => true, 'message' => 'Pago aprobado por Mercado Pago']);
                break;
            case 'false':
                return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago ha sido rechazado']);
                break;
            case 'in-process':
                return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago aun no ha sido procesado, consulte más tarde']);
                break;
        }

    }
}
