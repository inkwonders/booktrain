<?php

namespace App\Http\Controllers\Bbva\Resources;

use Bbva\Data\BbvaApiDerivedResource;

class BbvaTokenList extends BbvaApiDerivedResource
{
    public function create($params)
    {
        return $this->add($params);
    }

}
