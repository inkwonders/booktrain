<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
use Mail;

class ForgotPasswordController extends Controller
{
    public function getEmail()
    {

       return view('public.recover');
    }

    public function postEmail(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'email' => 'required|email|exists:users',
        ]);

        if($validator->fails()) {
            return view('public.recover')->with('error_message', $validator->errors()->first());
        }

        $token = Str::random(60);
        //$token = Hash::make($request->email);

        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
        );
        $img_url = public_path()."/assets/img/asteriscos.png";
        $img_logo = public_path()."/assets/img/logo.png";
        Mail::send('emails.verify',['token' => $token, 'email' => $request->email, 'img_url' => $img_url, 'img_logo' => $img_logo], function($message) use ($request) {
            $message->from('noreply@booktrain.com.mx');
            $message->to($request->email);
            $message->subject('Restablecimiento de contraseña');
        });

        //return view('public.requestpass')->with('message', 'El link de restablecimiento se ha enviado.');
        return view('public.requestpass');
    }
}
