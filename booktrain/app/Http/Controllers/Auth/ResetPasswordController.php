<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordResets;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
class ResetPasswordController extends Controller
{
    public function getPassword($token) {
        $tiempo_limite = Carbon::now()->subMinutes(5);
        // Mover a boot $pr = PasswordResets::where('created_at', '<', $tiempo_limite)->delete();

        $password_reset = PasswordResets::where('token', $token)->first();

        if ($password_reset) {
            return view('public.newpass', ['token' => $token, 'email' => $password_reset['email']]);
        } else {
            return abort(419);
        }
    }

    public function getExpired(){
        return abort(419);
    }

    public function updatePassword(Request $request) {
        $tiempo_limite = Carbon::now()->subMinutes(10);
        // $pr = PasswordResets::where('created_at', '<', $tiempo_limite)->delete();

        $password_reset = PasswordResets::where('token', $request['before_token'])->first();

        $reglas = [
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
            'token' => 'exists:password_resets,token'
        ];

        $mensajes = [
            'email.required' => 'El email es obligatorio',
            'password.required' => 'El campo de nueva contraseña está vacio',
            'password.min' => 'La contraseña debe tener al menos 8 caracteres',
            'password_confirmation.required' => 'Es necesario repetir la contraseña',
            'password.confirmed' => 'Las contraseñas no son iguales',
            'token' => 'Se ha excedido el tiempo límite para cambiar la contraseña'
        ];

        $validador = Validator::make($request->all(), $reglas, $mensajes);

        // Valida que el token exista
        // Valida que el correo coincida con el registro en password reset
        // Valida las contraseñas
        // Si no falla debe regresar a la misma ruta con un mensaje

        if($validador->fails()) {
            return redirect()->route('reset_password', ['token' => $request->before_token])->with('error', $validador->errors()->first());
        } else {
            $user_change = User::where('email', $request->email)->first();
            $new_password = Hash::make($request->password);

            $user_change->password = $new_password;

            $user_change->save();

            $password_reset->delete();

            $img_url = public_path()."/assets/img/asteriscos.png";
            $img_logo = public_path()."/assets/img/logo.png";
            Mail::send('emails.confirm',['email' => $request->email, 'img_url' => $img_url, 'img_logo' => $img_logo], function($message) use ($request) {
                $message->from('noreply@booktrain.com.mx');
                $message->to($request->email);
                $message->subject('Restablecimiento de contraseña');
            });

            return view('public.requestsuccess');
        }
    }
}
