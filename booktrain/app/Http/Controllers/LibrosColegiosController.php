<?php

namespace App\Http\Controllers;

use App\DataTables\LibrosColegiosDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLibrosColegiosRequest;
use App\Http\Requests\UpdateLibrosColegiosRequest;
use App\Repositories\LibrosColegiosRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LibrosColegiosController extends AppBaseController
{
    /** @var  LibrosColegiosRepository */
    private $librosColegiosRepository;

    public function __construct(LibrosColegiosRepository $librosColegiosRepo)
    {
        $this->librosColegiosRepository = $librosColegiosRepo;
    }

    /**
     * Display a listing of the LibrosColegios.
     *
     * @param LibrosColegiosDataTable $librosColegiosDataTable
     * @return Response
     */
    public function index(LibrosColegiosDataTable $librosColegiosDataTable)
    {
        return $librosColegiosDataTable->render('libros_colegios.index');
    }

    /**
     * Show the form for creating a new LibrosColegios.
     *
     * @return Response
     */
    public function create()
    {
        return view('libros_colegios.create');
    }

    /**
     * Store a newly created LibrosColegios in storage.
     *
     * @param CreateLibrosColegiosRequest $request
     *
     * @return Response
     */
    public function store(CreateLibrosColegiosRequest $request)
    {
        $input = $request->all();

        $librosColegios = $this->librosColegiosRepository->create($input);

        Flash::success('Libros Colegios saved successfully.');

        return redirect(route('librosColegios.index'));
    }

    /**
     * Display the specified LibrosColegios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            Flash::error('Libros Colegios not found');

            return redirect(route('librosColegios.index'));
        }

        return view('libros_colegios.show')->with('librosColegios', $librosColegios);
    }

    /**
     * Show the form for editing the specified LibrosColegios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            Flash::error('Libros Colegios not found');

            return redirect(route('librosColegios.index'));
        }

        return view('libros_colegios.edit')->with('librosColegios', $librosColegios);
    }

    /**
     * Update the specified LibrosColegios in storage.
     *
     * @param  int              $id
     * @param UpdateLibrosColegiosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLibrosColegiosRequest $request)
    {
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            Flash::error('Libros Colegios not found');

            return redirect(route('librosColegios.index'));
        }

        $librosColegios = $this->librosColegiosRepository->update($request->all(), $id);

        Flash::success('Libros Colegios updated successfully.');

        return redirect(route('librosColegios.index'));
    }

    /**
     * Remove the specified LibrosColegios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            Flash::error('Libros Colegios not found');

            return redirect(route('librosColegios.index'));
        }

        $this->librosColegiosRepository->delete($id);

        Flash::success('Libros Colegios deleted successfully.');

        return redirect(route('librosColegios.index'));
    }
}
