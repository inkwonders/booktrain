<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Pedido;
use App\Models\Direccion;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Configuracion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Openpay\Data\Openpay;
use App\Http\Controllers\Openpay\Data\OpenpayApi;
use App\Http\Controllers\Openpay\Data\OpenpayApiConsole;

class OpenPayController extends Controller
{
    private function getInstance() {
        if(env('OPENPAY_ENABLED', 0) == 1) {
            // OpenpayApiConsole::setLevel(127); // Debug: CONSOLE_ALL

            Openpay::setCountry( env('OPENPAY_COUNTRY_CODE', 'MX') );
            Openpay::setProductionMode( ! env('OPENPAY_SANDBOX_MODE') );
            Openpay::setSandboxMode( env('OPENPAY_SANDBOX_MODE') );

            return Openpay::getInstance(
                env('OPENPAY_MERCHANT_ID'),
                env('OPENPAY_PRIVATE_KEY')
            );
        }

        throw new Exception("Openpay no está hábilitado", 1);
    }

    /**
     * Procesa un pago usando una referencia a tienda (PayNet) como metodo de pago
     */
    public function referenciaTienda(Request $request) {

        Log::debug('OpenPayController@referenciaTienda', ['request' => $request->all()]);
        $request->validate([
            'pedido_id'      => 'required|exists:pedidos,id',
            'metodo_pago'    => 'required',
            'forma_pago'     => 'required',
            'terminos_aceptados'     => 'required',

            'facturacion_requerida' => 'required',

            'contacto.nombre'    => 'required',
            'contacto.apellidos' => 'required',
            'contacto.celular'   => 'required',
            'contacto.correo'    => 'nullable|email',
            'facturacion.id_cfdi'      => 'exclude_if:facturacion_requerida,false|sometimes|exists:cfdis,id',
            'facturacion.cfdi'         => 'required_if:facturacion_requerida,1',
            'facturacion.correo'       => 'required_if:facturacion_requerida,1',
            'facturacion.cp'           => 'required_if:facturacion_requerida,1',
            'facturacion.razon_social' => 'required_if:facturacion_requerida,1',
            'facturacion.rfc'          => 'required_if:facturacion_requerida,1',

            'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_interior'      => 'sometimes|exclude_if:facturacion_requerida,false|nullable',
            'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',

            'entrega.calle'        => 'required_if:tipo_entrega,2',
            'entrega.colonia'      => 'required_if:tipo_entrega,2',
            'entrega.cp'           => 'required_if:tipo_entrega,2',
            'entrega.estado'       => 'required_if:tipo_entrega,2',
            'entrega.estado_id'    => 'required_if:tipo_entrega,2',
            'entrega.municipio'    => 'required_if:tipo_entrega,2',
            'entrega.municipio_id' => 'required_if:tipo_entrega,2',
            'entrega.no_exterior'  => 'required_if:tipo_entrega,2',
            'entrega.no_interior'  => 'required_if:tipo_entrega,2',
            'entrega.referencia'   => 'required_if:tipo_entrega,2',

            'description' => 'sometimes'
        ]);

        $user = Auth::user();
        $pedido = $user->pedidos()->where('id', $request->pedido_id)->first();

        if( ! $pedido) {
            Log::error('OpenPayController@referenciaTienda', ['error' => 'El pedido no existe o no pertenece al usuario']);

            return response()->json(['success' => false, 'message' => 'El pedido no existe'], 404);
        }

        DB::beginTransaction();

        try {
            $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->firstOrFail();
            $forma_pago = FormaPago::where('codigo', $request->forma_pago)->firstOrFail();

            // Es importante cambiar el estado del pedido antes de crear el pago para obtener un folio y una serie
            $pedido->update([
                'status' => 'PROCESADO',
                'forma_pago' => $request->forma_pago,
                'metodo_pago' => $request->metodo_pago,
                'forma_pago_id' => $forma_pago->id,
                'metodo_pago_id' => $metodo_pago->id,

                'nombre_contacto'       => $request->contacto['nombre'],
                'apellidos_contacto'    => $request->contacto['apellidos'],
                'celular_contacto'      => $request->contacto['celular'],
                'correo_contacto'       => $request->contacto['correo'],
                'tipo_entrega'          => $request->tipo_entrega,
                'terminos_condiciones'  => $request->terminos_aceptados,

                // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                'metodo_pago'           => $request->metodo_pago,
                'forma_pago'            => $request->forma_pago,
            ]);

            $pedido->procesar();

            $configuracion_compra = [
                'customer' => [
                    'external_id' => $pedido->id,
                    'name' => $request->contacto['nombre'],
                    'last_name' => $request->contacto['apellidos'],
                    'email' => $user->email,
                    'requires_account' => false,
                    'phone_number' => $request->contacto['celular']
                    // 'address' => []
                ],
                'order_id' => uniqid("PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-"),
                'method' => 'store',
                'amount' => "{$pedido->total}",
                'description' => 'Compra de libros'
            ];

            Log::debug('OpenPayController@referenciaTienda', ['configuracion_compra' => $configuracion_compra ]);
            $referencia_pago = $this->crearCargo($configuracion_compra);

            Log::debug('OpenPayController@referenciaTienda', ['pago' => $referencia_pago->serializableData ]);

            $pedido->referencias()->create([
                'referencia' => $referencia_pago->payment_method->reference,
                'tipo' => 'tienda',
                'dias_vigencia' => 3,
                'response' => json_encode($referencia_pago->serializableData)
            ]);

            if($request->facturacion_requerida) {
                // SEGUN EL SAT
                // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T, DEBITO, ID 3 = T. CREDITO

                $forma_pago_factura = 1;
                $metodo_pago_factura = 1;

                switch($request->forma_pago) {
                    case 'PUE': $metodo_pago_factura = 1; break;
                    default: $metodo_pago_factura = 2; break;
                }

                switch($request->metodo_pago) {
                    case 'BANCO': $forma_pago_factura = 1; break;
                    case 'BBVADEBITOVPOS': $forma_pago_factura = 2; break;
                    case 'BBVACREDITOVPOS': $forma_pago_factura = 3; break;
                }

                // Eliminamos los datos anteriores
                $datos_factura = $pedido->datosFactura;
                if($datos_factura != null) $datos_factura->delete();

                $descripcion_metodo_pago_factura = null;

                switch ($metodo_pago_factura) {
                    case 1: $descripcion_metodo_pago_factura = 'PUE';break;
                    case 2: $descripcion_metodo_pago_factura = 'PDD';break;
                }

                $descripcion_forma_pago_factura = null;

                switch ($forma_pago_factura) {
                    case 1: $descripcion_forma_pago_factura = '01-EFECTIVO';break;
                    case 2: $descripcion_forma_pago_factura = '04-TARJETA CREDITO';break;
                    case 3: $descripcion_forma_pago_factura = '28-TARJETA DEBITO';break;
                }

                $pedido->datosFactura()->create([
                    'user_id'        => $pedido->usuario_id,
                    'id_cfdi'        => $request->facturacion['id_cfdi'],
                    'metodo_pago_id' => $metodo_pago_factura,
                    'forma_pago_id'  => $forma_pago_factura,
                    'forma_pago'     => $descripcion_forma_pago_factura,
                    'metodo_pago'    => $descripcion_metodo_pago_factura,
                    'razon_social'   => $request->facturacion['razon_social'],
                    'rfc'            => $request->facturacion['rfc'],
                    'correo'         => $request->facturacion['correo'],
                    'cp'             => $request->facturacion['cp'],
                    'calle'          => $request->facturacion['calle'],
                    'num_exterior'   => $request->facturacion['num_exterior'],
                    'num_interior'   => $request->facturacion['num_interior'],
                    'colonia'        => $request->facturacion['colonia'],
                    'municipio'      => $request->facturacion['municipio'],
                    'estado'         => $request->facturacion['estado'],
                    'regimen_fiscal' => $request->facturacion['regimen_fiscal'],

                ]);

                $pedido->update(['factura' => 1]);
            }

            $direccion = $pedido->direccion()->dissociate();
            if ($request->tipo_entrega == 2) { // Entrega a domicilio
                $direccion = Direccion::create([
                    'calle'        => $request->input('entrega.calle', ''),
                    'no_exterior'  => $request->input('entrega.no_exterior', ''),
                    'no_interior'  => $request->input('entrega.no_interior', ''),
                    'colonia'      => $request->input('entrega.colonia', ''),
                    'cp'           => $request->input('entrega.cp', ''),
                    'estado_id'    => $request->input('entrega.estado_id', ''),
                    'municipio_id' => $request->input('entrega.municipio_id', ''),
                    'referencia'   => $request->input('entrega.referencia', '')
                ]);

                $pedido->direccion()->associate($direccion);
                $pedido->save();

                // Eliminamos la información del envio anterior
                $envio = $pedido->envio;
                if($envio != null) $envio->delete();

                // Creamos el registro en la tabla de envios
                $pedido->envio()->create([
                    'pedido_id' => $pedido->id,
                    'status' => 'PROCESANDO',
                    'costo' => $pedido->colegio->config('costo_envio')
                ]);
            }else {
                $pedido->direccion()
                    ->associate(
                        Direccion::find($request->id_direccion_envio)
                    );
            }

            $pedido->save();

            DB::commit();

            $referencia = [
                'nombre_servicio' => $metodo_pago->descripcion,
                'codigo' => $referencia_pago->payment_method->reference,
                'url_barcode' => $referencia_pago->payment_method->barcode_url,
                'monto' => $pedido->total,
                'monto_letra' => $pedido->total_letra,
                'fecha_limite' => now()->addDays(3)->format('d-m-Y'),
                'cliente' => [
                    'correo' => $pedido->user->email,
                    'grado' => $pedido->niveles->pluck('nombre')->join(', ', ' y '),
                    'nombre' => $pedido->nombre_contacto . ' ' . $pedido->apellidos_contacto
                ],
                'compra' => [
                    'folio' => $pedido->folio,
                    'descripcion' => $referencia_pago->description,
                    'fecha' => $pedido->updated_at->format('d-m-Y'),
                ]
            ];

            return response()->json($referencia);
        } catch (\Throwable $th) {
            Log::error('OpenPayController@referenciaTienda', [$th]);
            DB::rollback();
            return response()->json(['success' => false, 'error' => $th->getMessage(), 'trace' => $th->getTraceAsString()], 503);
        }

        return response()->json(['success' => false], 503);
    }

    /**
     * ==============
     * CREA UN CARGO
     * ==============
     * https://www.openpay.mx/en/docs/api/#charges
     *
     * Ejemplo de respuesta (Referencia a tienda):
     *
     * {
     *     "id": "trqrtyfndnrmfnomfozi",
     *     "authorization": null,
     *     "operation_type": "in",
     *     "transaction_type": "charge",
     *     "status": "in_progress",
     *     "conciliated": false,
     *     "creation_date": "2021-09-08T10:44:42-05:00",
     *     "operation_date": "2021-09-08T10:44:42-05:00",
     *     "description": "Compra de libros",
     *     "error_message": null,
     *     "order_id": "PED-24056---6138da68aa86e",
     *     "payment_method": {
     *         "type": "store",
     *         "reference": "9988783190194033",
     *         "barcode_url": "https://sandbox-api.openpay.mx/barcode/9988783190194033?width=1&height=45&text=false"
     *     },
     *     "amount": 5.00,
     *     "currency": "MXN",
     *     "customer": {
     *         "name": "eeee",
     *         "last_name": "eeee",
     *         "email": "josue@inkwonders.com",
     *         "phone_number": "1231231231",
     *         "address": null,
     *         "creation_date": "2021-09-08T10:44:40-05:00",
     *         "external_id": "24056",
     *         "clabe": null
     *     },
     *     "method": "store"
     * }
     *
     * Ejemplo de respuesta 2 (Tarjeta):
     *
     * {
     *    "id":"trzjaozcik8msyqshka4",
     *    "amount":100.00,
     *    "authorization":"801585",
     *    "method":"card",
     *    "operation_type":"in",
     *    "transaction_type":"charge",
     *    "card":{
     *       "id":"kqgykn96i7bcs1wwhvgw",
     *       "type":"debit",
     *       "brand":"visa",
     *       "address":null,
     *       "card_number":"411111XXXXXX1111",
     *       "holder_name":"Juan Perez Ramirez",
     *       "expiration_year":"20",
     *       "expiration_month":"12",
     *       "allows_charges":true,
     *       "allows_payouts":true,
     *       "creation_date":"2014-05-26T11:02:16-05:00",
     *       "bank_name":"Banamex",
     *       "bank_code":"002",
     *       "customer_id":"ag4nktpdzebjiye1tlze"
     *    },
     *    "status":"completed",
     *    "currency":"MXN",
     *    "creation_date":"2014-05-26T11:02:45-05:00",
     *    "operation_date":"2014-05-26T11:02:45-05:00",
     *    "description":"Cargo inicial a mi cuenta",
     *    "error_message":null,
     *    "order_id":"oid-00051"
     * }
     */
    public function crearCargo($configuracion_compra) {
        $this->getInstance();

        $response = $this->getInstance()
            ->charges
            ->create($configuracion_compra);

        return $response;
    }

    /**
     * Confirmamos un cargo vía tarjeta
     *
     * URL: https://www.openpay.mx/en/docs/api/#confirming-a-charge
     */
    public function confirmarCargo($transactionTokenId, $amount) {
        return $this->getInstance()->charges->get($transactionTokenId)->confirm($amount);
    }

    /**
     * Crea un webhook para recibir las llamadas de notificación de pago desde Openpay
     */
    public function registerWebhook() {

        $configuracion = Configuracion::firstOrNew([
            'etiqueta' => 'webhook_creado'
        ]);

        try {

            $configuracion_webhook = [
                'url' => route('webhook.openpay'), // 'https://booktrain.com.mx/webhook/openpay/gateway',
                // 'user' => env('OPENPAY_WEBHOOK_USERNAME', 'BOOKTRAIN_WH_DEFAULT_USER'),
                // 'password' => env('OPENPAY_WEBHOOK_PASSWORD', 'BOOKTRAIN_WH_DEFAULT_PASSWORD'),
                'user' => 'booktrain',
                'password' => 'booktrain',
                'event_types' => [
                    'verification',
                    'charge.created',
                    'charge.succeeded',
                    'charge.refunded'
                    //  'payout.created',
                    //  'payout.succeeded',
                    //  'payout.failed',
                    //  'transfer.succeeded',
                    //  'fee.succeeded',
                    //  'spei.received',
                    //  'chargeback.created',
                    //  'chargeback.rejected',
                    //  'chargeback.accepted',
                ]
                // "status" => "verified"
            ];

            $configuracion->valor = 0;
            $configuracion->save();

            $resultado_creacion_webhook = $this->getInstance()->webhooks->add($configuracion_webhook);
           /* $instancia = Openpay::getInstance(env('OPENPAY_MERCHANT_ID'),env('OPENPAY_PRIVATE_KEY'));
            $resultado_creacion_webhook = $instancia->webhooks->add($configuracion_webhook);*/


            Log::debug('createWebhook', ['resultado' => $resultado_creacion_webhook->serializableData]);

            activity()
            ->causedBy(1)
            ->log('Se creó un webhook para openpay con el id: ' . $resultado_creacion_webhook->id);

            return response()->json($resultado_creacion_webhook->serializableData);
        } catch (\Throwable $th) {
            Log::error('createWebhook', [$th]);
            return response()->json(['error' => $th->getMessage()], 503);
        }
    }

    /**
     * Ejemplo
     * {
     *    "type" : "charge.succeeded",
     *    "event_date" : "2013-11-22T15:09:38-06:00",
     *    "transaction" : {
     *        "amount" : 2000.0,
     *        "authorization" : "801585",
     *        "method" : "card",
     *        "operation_type" : "in",
     *        "transaction_type" : "charge",
     *        "card" : {
     *            "type" : "debit",
     *            "brand" : "mastercard",
     *            "address" : {
     *               "line1" : "Calle #1 Interior #2",
     *               "line2" : null,
     *               "line3" : null,
     *               "state" : "Queretaro",
     *               "city" : "Queretaro",
     *               "postal_code" : "76040",
     *               "country_code" : "MX"
     *            },
     *            "card_number" : "1881",
     *            "holder_name" : "Card Holder",
     *            "expiration_month" : "10",
     *            "expiration_year" : "14",
     *            "allows_charges" : true,
     *            "allows_payouts" : true,
     *            "creation_date" : "2013-11-22T15:09:32-06:00",
     *            "bank_name" : "BBVA BANCOMER",
     *            "bank_code" : "012"
     *        },
     *        "status" : "completed",
     *        "id" : "tlxcm4vprtz74qoenuay",
     *        "creation_date" : "2013-11-22T15:09:33-06:00",
     *        "description" : "Description",
     *        "error_message" : null,
     *        "order_id" : "oid_14235"
     *    }
     *}
     */
    public function webhook(Request $request) {

        // dd($request->all());

        $request->validate([
            'type'                                  => 'required',
            'transaction'                           => 'required',
            'transaction.id'                        => 'required',
            'transaction.amount'                    => 'required',
            'transaction.method'                    => 'required',
            'transaction.transaction_type'          => 'required',
            'transaction.status'                    => 'required',
            'transaction.order_id'                  => 'required',
            'transaction.description'               => 'required',
            'transaction.payment_method.reference'  => 'required|exists:referencias_pedidos,referencia',

        ]);

        Log::debug("OpenPayController@webhook: request", $request->all());


        $transactionTokenId = $request->transaction['id'];

        $referencia = $request->transaction['payment_method']['reference'];

        $pedido = Pedido::whereHas('referencias', function($query) use ($referencia) {
            $query->where('referencia', $referencia);
            $query->withCount('referenciasDuplicadas');
            $query->having('referencias_duplicadas_count', '=', 1); // Validamos que no tengan referencias duplicadas
        })->first();

        if( ! $pedido) {
            Log::error("OpenPayController@webhook: Pedido no encontrado", ['referencia' => $referencia, 'transactionTokenId' => $transactionTokenId]);
            return response()->json(['error' => 'Pedido no encontrado'], 404);
        }

        if( ! in_array($pedido->status, ['PROCESADO'] )) {
            Log::error("OpenPayController@webhook: El pedido ya fue procesado", ['referencia' => $referencia, 'transactionTokenId' => $transactionTokenId, 'pedido_id' => $pedido->id]);
            return response()->json(['error' => 'El pedido ya fue procesado'], 403);
        }

        Log::debug("OpenPayController@webhook: Pedido", ['pedido' => $pedido->id, 'referencia' => $referencia, 'transactionTokenId' => $transactionTokenId]);

        try {
            switch ($request->type) {
                case 'verification': //La notificación contiene el código de verificación del Webhook
                    $configuracion_webhook = Configuracion::where('etiqueta', 'webhook_creado')->first();

                    if($configuracion_webhook->valor == 0) {
                        $configuracion_webhook->update(['valor' => 1]);

                        return response()->json(['success' => 'true', 'message' => 'Webhook registrado correctamente']);
                    }
                break;

                case 'charge.created': // Se creó un cargo para ser pagado por transferencia bancaria.
                    $pago = [
                        'transactionTokenId' => $transactionTokenId,
                        'origen' => 'WEBHOOK',
                        'source' => $request->transaction['payment_method']['reference'],
                        'raw' => json_encode($request->all()),
                        'status' => 'IN_PROCESS',
                        'amount' => $request->transaction['amount']
                    ];

                    Log::debug('OpenPayController@webhook: Creando pago en pedido', ['pedido_id' => $pedido->id, 'pago' => $pago]);
                    $pedido->pagos()->create($pago);
                break;

                case 'charge.succeeded': // Indica que el cargo se ha completado (tarjeta, banco o tienda)
                    switch($request->transaction['method']) {
                        case 'store':
                            Log::info('OpenPayController@webhook: Notificación de cargo en tienda, se recibió un estado ' . $request->transaction['status']);
                            $pago = $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->first();

                            switch($request->transaction['status']) {
                                case 'completed':
                                    if($pedido->total != $request->transaction['amount']){
                                        Log::error('OpenPayController@webhook: El monto de la transacción no coincide con el pedido', ['pedido_id' => $pedido->id]);
                                        return response()->json(['error' => 'El monto de la transacción no coincide con el pedido'], 400);
                                    }

                                    if( ! Str::startsWith($request->transaction['order_id'], "PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-")){
                                        Log::error('OpenPayController@webhook: La orden no coincide con el pedido', ['pedido_id' => $pedido->id]);
                                        return response()->json(['error' => 'La orden no coincide con el pedido'], 400);
                                    }

                                    //dd($pago, $request->all());
                                    $pago->update([
                                        'status' => 'SUCCESS',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);

                                    $pedido->update([
                                        'status' => 'PAGADO'
                                    ]);

                                    $pedido->referencias()->where('referencia', $referencia)->first()->update([
                                        'pagado' => 1,
                                        'notificado' => $pedido->sendPagoExitosoNotification()
                                    ]);
                                break;

                                case 'in_progress':
                                    $pago->update([
                                        'status' => 'IN_PROCESS',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);
                                break;

                                case 'failed':
                                    $pago->update([
                                        'status' => 'FAILED',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);
                                break;
                            }
                        break;
                        case 'card':
                            Log::info('OpenPayController@webhook: Notificación de cargo por tarjeta, se recibió un estado ' . $request->transaction['status']);
                        break;
                        case 'bank':
                            Log::info('OpenPayController@webhook: Notificación de cargo en banco, se recibió un estado ' . $request->transaction['status']);
                        break;
                    }
                break;

                case 'charge.refunded': // Un cargo a tarjeta fue reembolsado.
                break;

                case 'payout.created': // Se ha programado un pago.
                break;

                case 'payout.succeeded': // Se ha enviado el pago.
                break;

                case 'payout.failed': // El pago fue rechazado.
                break;

                case 'transfer.succeeded': // Se ha realizado una transferencia entre dos clientes.
                break;

                case 'fee.succeeded': // Se ha cobrado una comisión a un cliente.
                break;

                case 'spei.received': // Se han agregado fondos a una cuenta mediante SPEI.
                break;

                case 'chargeback.created': // Se ha recibido un contracargo de un cargo a tarjeta
                break;

                case 'chargeback.rejected': // El contracargo se ha ganado a favor del comercio
                break;

                case 'chargeback.accepted': // El contracargo se ha perdido. Se ha creado una transacción tipo contracargo que descontará los fondos de tu cuenta.
                break;
            }

        } catch (\Throwable $th) {
            Log::error("OpenPayController@webhook", [$th]);
            return response()->json(['error' => 'Error al procesar el webhook', 'message' => $th->getMessage()]);
        }

        return response()->json(['success' => true]);
    }

    /**
     * Devuelve la información de un cargo
     *
     * Ejemplo de respuesta:
     *
     *
     */
    public function getCharge($transactionTokenId) {
        Log::info('OpenPayController@getCharge: Obteniendo información de cargo', ['transactionTokenId' => $transactionTokenId]);
        $charge = $this->getInstance()->charges->get($transactionTokenId);

        $cargo = $charge->serializableData;
        $cargo['creation_date']    = $charge->creation_date;
        $cargo['currency']         = $charge->currency;
        $cargo['customer_id']      = $charge->customer_id;
        $cargo['operation_type']   = $charge->operation_type;
        $cargo['status']           = $charge->status;
        $cargo['transaction_type'] = $charge->transaction_type;
        $cargo['card']             = $charge->card;

        Log::info('OpenPayController@getCharge: Información de cargo obtenida', ['transactionTokenId' => $transactionTokenId, 'cargo' => $cargo]);
        return $cargo;
    }

    /**
     * Obtiene información de una tarjeta a partir de su ID
     *
     * Ejemplo de respuesta:
     *
     * {
     *    "id":"ktrpvymgatocelsciak7",
     *    "type":"debit",
     *    "brand":"visa",
     *    "card_number":"411111XXXXXX1111",
     *    "holder_name":"Juan Perez Ramirez",
     *    "expiration_year":"20",
     *    "expiration_month":"12",
     *    "allows_charges":true,
     *    "allows_payouts":true,
     *    "creation_date":"2014-05-21T17:31:01-05:00",
     *    "bank_name":"Banamex",
     *    "customer_id":"ag4nktpdzebjiye1tlze",
     *    "bank_code":"002"
     * }
     */
    public function getCard($card_id) {
        return $this->getInstance()
            ->cards->get($card_id);
    }

    /**
     * Procesamos el pago de la pasarela
     */
    public function postPagar(Request $request, $pedido_id) {
        Log::debug('OpenPayController@postPagar: Datos de solicitud', $request->all());

        // dd('llego',$request->all());

        $request->validate([
            'token_tarjeta'                 => 'required',
            'device_session_id'             => 'required',
            'contacto.nombre'               => 'required',
            'contacto.apellidos'            => 'required',
            'contacto.celular'              => 'required',
            'contacto.correo'               => 'nullable|email',
            'tipo_entrega'                  => 'sometimes|required|between:1,3',
            'id_direccion_envio'            => 'sometimes|exists:direcciones,id',
            'facturacion_requerida'         => 'required|boolean',
            'facturacion.nombre'            => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.rfc'               => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.correo'            => 'exclude_if:facturacion_requerida,false|sometimes|email',
            'facturacion.cfdi'              => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.id_cfdi'           => 'exclude_if:facturacion_requerida,false|sometimes|exists:cfdis,id',

            'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_interior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',

            'terminos_aceptados'            => 'required|accepted',
            'forma_pago'                    => 'required|in:PUE,DIFERIDO3M,DIFERIDO6M,DIFERIDO9M',
            'metodo_pago'                   => 'required|in:CREDITO,DEBITO,AMEX,BANCO,TIENDA,EFECTIVO,TERMINAL,TRANSFERENCIA,BBVADEBITOVPOS,BBVACREDITOVPOS,BBVATARJETA,OPDEBITO,OPAMEX,OPMASTERCARD,OPEFECTIVO,OPSPEI,OPBANCO,OPTIENDA,OPQRALIPAY,OPQRCODI,OPIVR',
            'direccion.calle'               => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_exterior'         => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_interior'         => 'sometimes',
            'direccion.colonia'             => 'sometimes|required_if:tipo_entrega,2',
            'direccion.cp'                  => 'sometimes|required_if:tipo_entrega,2',
            'direccion.estado'              => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio'           => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio_id'        => 'sometimes|required_if:tipo_entrega,2|exists:municipios,id',
            'direccion.estado_id'           => 'sometimes|required_if:tipo_entrega,2|exists:estados,id',
            'direccion.referencia'          => 'sometimes',
            'tarjeta.​​address'               => 'sometimes',// null
            'tarjeta.​​brand'                 => 'sometimes|required_with:tarjeta',// "mastercard"
            'tarjeta.​​card_number'           => 'sometimes|required_with:tarjeta',// "545454XXXXXX5454"
            'tarjeta.​​creation_date'         => 'sometimes',// null
            'tarjeta.​​expiration_month'      => 'sometimes|required_with:tarjeta',// "10"
            'tarjeta.​​expiration_year'       => 'sometimes|required_with:tarjeta',// "22"
            'tarjeta.​​holder_name'           => 'sometimes|required_with:tarjeta',// "Prueba"

        ]);

        $user = Auth::user();

        $pedido = $user
            ->pedidos()
            ->whereId($pedido_id)
            ->whereStatus('CARRITO')
            ->first();

        if( ! $pedido){
            Log::debug('OpenPayController@postPagar: El pedido no existe o ya fue pagado', ['pedido_id' => $pedido_id]);

            return response()->json(['success' => false, 'message' => 'El pedido no existe o ya ha sido procesado'], 404);
        }

        DB::beginTransaction();

        try {
            $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->first();
            $forma_pago = FormaPago::where('codigo', $request->forma_pago)->first();

            $pedido->update([
                'nombre_contacto'       => $request->contacto['nombre'],
                'apellidos_contacto'    => $request->contacto['apellidos'],
                'celular_contacto'      => $request->contacto['celular'],
                'correo_contacto'       => $request->contacto['correo'],
                'tipo_entrega'          => $request->tipo_entrega,
                'terminos_condiciones'  => $request->terminos_aceptados,
                // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                'metodo_pago'           => $request->metodo_pago,
                'forma_pago'            => $request->forma_pago,
                'status'                => 'PROCESADO',
                'metodo_pago_id'        => $metodo_pago->id,
                'forma_pago_id'         => $forma_pago->id,
            ]);

            if($request->facturacion_requerida) {
                // SEGUN EL SAT
                // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T. DEBITO, ID 3 = T. CREDITO

                $forma_pago_factura = 1;
                $metodo_pago_factura = 1;

                switch($request->forma_pago) {
                    case 'PUE': $metodo_pago_factura = 1; break;
                    default: $metodo_pago_factura = 2; break;
                }

                switch($request->metodo_pago) {
                    case 'OPSPEI': $forma_pago_factura = 1; break; // Inalcanzable
                    case 'OPBANCO': $forma_pago_factura = 1; break; // Inalcanzable
                    case 'OPEFECTIVO': $forma_pago_factura = 1; break; // Inalcanzable

                    //case 'OPDEBITO': $forma_pago_factura = 2; break; // antes version jacobo
                    //case 'OPMASTERCARD': $forma_pago_factura = 3; break;// antes version jacobo
                    //case 'OPAMEX': $forma_pago_factura = 3; break;// antes version jacobo

                    case 'OPDEBITO': $forma_pago_factura = 3; break;
                    case 'OPMASTERCARD': $forma_pago_factura = 2; break;
                    case 'OPAMEX': $forma_pago_factura = 2; break;
                }

                // Eliminamos los datos anteriores
                $datos_factura = $pedido->datosFactura;
                if($datos_factura != null) $datos_factura->delete();

                $metodo_pago_descripcion = null;

                switch ($metodo_pago_factura) {
                    case 1: $metodo_pago_descripcion = 'PUE';break;
                    case 2: $metodo_pago_descripcion = 'PDD';break;
                }

                $forma_pago_descripcion = null;

                switch ($forma_pago_factura) {
                    case 1: $forma_pago_descripcion = '01-EFECTIVO';break;
                    case 2: $forma_pago_descripcion = '04-TARJETA CREDITO';break;
                    case 3: $forma_pago_descripcion = '28-TARJETA DEBITO';break;
                }

                $pedido->datosFactura()->create([
                    'user_id'        => $pedido->usuario_id,
                    'id_cfdi'        => $request->facturacion['id_cfdi'],
                    'metodo_pago_id' => $metodo_pago_factura,
                    'forma_pago_id'  => $forma_pago_factura,
                    'forma_pago'     => $forma_pago_descripcion,
                    'metodo_pago'    => $metodo_pago_descripcion,
                    'razon_social'   => $request->facturacion['razon_social'],
                    'rfc'            => $request->facturacion['rfc'],
                    'correo'         => $request->facturacion['correo'],
                    'cp'             => $request->facturacion['cp'],
                    'calle'          => $request->facturacion['calle'],
                    'num_exterior'   => $request->facturacion['num_exterior'],
                    'num_interior'   => $request->facturacion['num_interior'],
                    'colonia'        => $request->facturacion['colonia'],
                    'municipio'      => $request->facturacion['municipio'],
                    'estado'         => $request->facturacion['estado'],
                    'regimen_fiscal' => $request->facturacion['regimen_fiscal'],

                ]);

                $pedido->update(['factura' => 1]);
            }

            $direccion = $pedido->direccion()->dissociate(); // <<<< Verificar que eso sea correcto

            if ($request->tipo_entrega == 2) { // a domicilio
                $direccion = Direccion::create([
                        'calle'        => $request->input('direccion.calle', ''),
                        'no_exterior'  => $request->input('direccion.no_exterior', ''),
                        'no_interior'  => $request->input('direccion.no_interior', ''),
                        'colonia'      => $request->input('direccion.colonia', ''),
                        'cp'           => $request->input('direccion.cp', ''),
                        'estado_id'    => $request->input('direccion.estado_id', ''),
                        'municipio_id' => $request->input('direccion.municipio_id', ''),
                        'referencia'   => $request->input('direccion.referencia', '')
                    ]);

                    $pedido->direccion()->associate($direccion);
                    $pedido->save();

                    // Eliminamos la información del envio anterior
                    $envio = $pedido->envio;
                    if($envio != null) $envio->delete();

                    // Creamos el registro en la tabla de envios
                    $pedido->envio()->create([
                        'pedido_id' => $pedido->id,
                        'status' => 'PROCESANDO',
                        'costo' => $pedido->colegio->config('costo_envio')
                    ]);
            }else {
                $pedido->direccion()->associate(Direccion::find($request->id_direccion_envio));
            }
            $pedido->save();

        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->sendError($th->getMessage(), 503);
        }

        DB::commit();

        /** se vuelve a obtener el pedido para obtener los datos actualizados */
        $pedido->refresh();

         // PREPARAMOS EL OBJETO DE CONFIGURACIÓN DEL PAGO
        $configuracion_compra = [
           'source_id'         => $request->token_tarjeta,
           'method'            => 'card',
           'amount'            => number_format($pedido->total,2),
           'currency'          => 'MXN',
           'description'       => 'Venta de libros',
           'order_id'          => uniqid("PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-"),
           'device_session_id' => $request->device_session_id,
           'customer' => [
               'name'         => $request->contacto['nombre'],
               'last_name'    => $request->contacto['apellidos'],
               'phone_number' => $request->contacto['celular'],
               'email'        => $user->email,
           ],
           'redirect_url' => route('openpay.3ds'),
           'use_3d_secure' => env('OPENPAY_FORCE_3DS', false)
        ];

        if($forma_pago->codigo != 'PUE') {
            $forma_pago_colegio = $pedido->colegio->formasPago()->where('metodo_pago_id', $metodo_pago->id)->where('forma_pago_id', $forma_pago->id)->first();
            Log::debug('OpenPayController@postPagar: Forma de pago', ['forma_pago' => $forma_pago_colegio]);

            $configuracion_compra['payment_plan'] = [
                'payments' => $forma_pago_colegio->pivot->meses
                // 'payments_type' => 'without_interest'
            ];
        }

        Log::debug('CONFIGURACION COMPRA: ', ['DATOS' => $configuracion_compra]);

        $codigo_error = 0;
        $error = '';

        try {
            Log::debug('OpenPayController@postPagar: Creando cargo', ['configuracion' => $configuracion_compra]);
            $cargo = $this->crearCargo($configuracion_compra);

            Log::debug('OpenPayController@postPagar: Cargo creado', ['cargo' => $cargo]);

            $estado_cargo = 'IN_PROCESS';

            switch($cargo->status) {
                case 'completed': $estado_cargo = 'SUCCESS';break;
                case 'failed': $estado_cargo = 'FAILED';break;
                case 'in_progress': $estado_cargo = 'IN_PROCESS';break;
                case 'charge_pending': $estado_cargo = 'WAIT_THREEDS';break;
            }

            // Creamos el pago para el pedido
            $pedido->pagos()->create([
                'amount'             => $cargo->amount,
                'origen'             => 'API',
                'transactionTokenId' => $cargo->id,
                'source'             => $cargo->authorization, // Debe ir esto aquí ??
                'status'             => $estado_cargo,
                'raw'                => json_encode($cargo->serializableData)
            ]);

            // Si el pago fue exitoso, marcamos el pedido como pagado
            if($cargo->status == 'SUCCESS') {
                $pedido->update([
                    'status' => 'PAGADO'
                ]);

                // Descontamos las unidades del almacén y enviamos el correo al cliente confirmando su pago
                $pedido->procesar();

                return response()->json(['success' => true]);
            }

            if($cargo->status == 'charge_pending') {
                Log::debug('OpenPayController@postPagar: El cargo requiere 3DS, se redireccionará');

                return response()->json(['success' => true, 'returnUrl' => $cargo->serializableData['payment_method']->url]);
            }
        } catch (\Throwable $th) {
            /**
             * Ejemplo de error
             *
             * {
             *     "http_code": 402,
             *     "error_code": 3005,
             *     "category": "gateway",
             *     "description": "Fraud risk detected by anti-fraud system",
             *     "request_id": "92365b52-b489-45c8-8b48-e2c4afddff6d"
             * }
             */
            Log::error('OpenPayController@postPagar: Error al crear el cargo', [
                'pedido_id' => $pedido_id,
                'error' => $th->getMessage(),
                'codigo' => $th->getCode(),
                'configuracion_compra' => $configuracion_compra,
            ]);

            $codigo_error = $th->getCode();
            $error = $th->getMessage();

            if($codigo_error != 3005)
                DB::rollback();
        }

        // Fraud risk detected by anti-fraud system
        if($codigo_error == 3005) {
            // try {
                // Intentamos hacer un cargo con 3DS
                $configuracion_compra['use_3d_secure'] = true;

                $cargo_3ds = $this->crearCargo($configuracion_compra);
                Log::debug('OpenPayController@postPagar: Creación de cargo con 3DS', ['cargo' => json_encode($cargo_3ds) ]);

                // Creamos el pago para el pedido
                $pedido->pagos()->create([
                    'amount'             => $cargo_3ds->serializableData['amount'],
                    'origen'             => 'API',
                    'transactionTokenId' => $cargo_3ds->id,
                    'source'             => $cargo_3ds->order_id, // Debe ir esto aquí ??
                    'status'             => 'WAIT_THREEDS',
                    'raw'                => json_encode($cargo_3ds->serializableData)
                ]);

                DB::commit();

                // Redirigimos al usuario a la pasarela de 3DS
                return response()->json(['success' => true, 'returnUrl' => $cargo_3ds->serializableData['payment_method']->url]);
            // } catch (\Throwable $th) {
            //     Log::error('OpenPayController@postPagar: Error al crear el cargo 3DS', [
            //         'pedido_id' => $pedido_id,
            //         'error' => $th->getMessage(),
            //         'codigo' => $th->getCode(),
            //         'configuracion_compra' => $configuracion_compra,
            //     ]);

            //     return response()->json(['success' => false, 'message' => "Error al realizar la operación 3DS: {$th->getCode()}"]);
            // }
        }else{
            return response()->json(['success' => false, 'message' => "Error al realizar la operación: {$codigo_error} {$error}"]);
        }
    }

    /**
     * Ruta para recibir la redirección del 3DS
     */
    public function continue3Ds(Request $request) {
        $request->validate([
            'id' => 'required'
        ]);

        $transactionTokenId = $request->id;

        $user = Auth::user();
        $pedido = $user->pedidos()
            ->whereHas('pagos', function($query) use ($transactionTokenId) {
                $query->where('transactionTokenId', $transactionTokenId);
            })->first();

        if( ! $pedido) {
            Log::error('OpenPayController@continue3Ds: Error al obtener el pedido', ['transactionTokenId' => $request->id]);

            return view('errors.404');
        }

        $pago = $pedido->pagos()->where('transactionTokenId', $request->id)->first();

        DB::beginTransaction();

        // Comprobamos el estado del pago del pedido
        try {
            $cargo = $this->getCharge($transactionTokenId);

            // dd($cargo, $cargo['customer']->serializableData);
            Log::debug('OpenPayController@continue3Ds: Obtenemos el cargo', ['transactionTokenId' => $request->id, 'cargo' => $cargo['customer']->serializableData]);

            switch($cargo['status']) {
                case 'completed':
                    $pago->update(['status' => 'SUCCESS']);
                    $pedido->update([ 'status' => 'PAGADO' ]);

                    $pedido->procesar();

                    // Notificamos al usuario que el pago fue exitoso
                    $pedido->sendPagoExitosoNotification();

                    DB::commit();
                    return view('private.resultado3ds')->with(['success' => true, 'message' => 'Pago aprobado por la entidad bancaria']);
                case 'in_progress':
                    $pago->update(['status' => 'IN_PROCESS']);

                    DB::commit();
                    return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago aun no ha sido procesado, consulte más tarde']);
                case 'failed':
                    $pago->update(['status' => 'FAILED']);
                    $pedido->update([ 'status' => 'PAGO RECHAZADO' ]);

                    DB::commit();
                    return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago ha sido rechazado']);
            }
        } catch (\Throwable $th) {
            Log::error('OpenPayController@continue3Ds: Error al obtener el cargo', ['transactionTokenId' => $request->id, 'pedido' => $pedido]);
            DB::rollback();

            return view('private.resultado3ds')->with(['success' => false, 'message' => $th->getMessage()]);
        }

        return view('private.resultado3ds')->with(['success' => false, 'message' => 'Resultado inalcanzable']);
    }
}
