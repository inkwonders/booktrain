<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Requests;
use Laracasts\Flash\Flash;
// use GuzzleHttp\Psr7\Request;
use App\Models\ResumenPedido;
use App\Models\Colegio;
use App\Models\DetalleResumenPedido;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AppBaseController;
use App\DataTables\DetalleResumenPedidoDataTable;
use App\Repositories\DetalleResumenPedidoRepository;
use App\Http\Requests\CreateDetalleResumenPedidoRequest;
use App\Http\Requests\UpdateDetalleResumenPedidoRequest;
use App\Models\DatosFacturas;

// use Request;
use Illuminate\Http\Request;

class DetalleResumenPedidoController extends AppBaseController
{
    /** @var  DetalleResumenPedidoRepository */
    private $detalleResumenPedidoRepository;

    public function __construct(DetalleResumenPedidoRepository $detalleResumenPedidoRepo)
    {
        $this->detalleResumenPedidoRepository = $detalleResumenPedidoRepo;
    }

    /**
     * Display a listing of the DetalleResumenPedido.
     *
     * @param DetalleResumenPedidoDataTable $detalleResumenPedidoDataTable
     * @return Response
     */
    public function index(DetalleResumenPedidoDataTable $detalleResumenPedidoDataTable)
    {
        return $detalleResumenPedidoDataTable->render('detalle_resumen_pedidos.index');
    }

    /**
     * Show the form for creating a new DetalleResumenPedido.
     *
     * @return Response
     */
    public function create()
    {
        return view('detalle_resumen_pedidos.create');
    }

    /**
     * Store a newly created DetalleResumenPedido in storage.
     *
     * @param CreateDetalleResumenPedidoRequest $request
     *
     * @return Response
     */
    public function store(CreateDetalleResumenPedidoRequest $request)
    {
        $input = $request->all();

        $detalleResumenPedido = $this->detalleResumenPedidoRepository->create($input);

        Flash::success('Detalle Resumen Pedido saved successfully.');

        return redirect(route('detalleResumenPedidos.index'));
    }

    /**
     * Display the specified DetalleResumenPedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            Flash::error('Detalle Resumen Pedido not found');

            return redirect(route('detalleResumenPedidos.index'));
        }

        return view('detalle_resumen_pedidos.show')->with('detalleResumenPedido', $detalleResumenPedido);
    }

    /**
     * Show the form for editing the specified DetalleResumenPedido.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            Flash::error('Detalle Resumen Pedido not found');

            return redirect(route('detalleResumenPedidos.index'));
        }

        return view('detalle_resumen_pedidos.edit')->with('detalleResumenPedido', $detalleResumenPedido);
    }

    /**
     * Update the specified DetalleResumenPedido in storage.
     *
     * @param  int              $id
     * @param UpdateDetalleResumenPedidoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetalleResumenPedidoRequest $request)
    {
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            Flash::error('Detalle Resumen Pedido not found');

            return redirect(route('detalleResumenPedidos.index'));
        }

        $detalleResumenPedido = $this->detalleResumenPedidoRepository->update($request->all(), $id);

        Flash::success('Detalle Resumen Pedido updated successfully.');

        return redirect(route('detalleResumenPedidos.index'));
    }

    /**
     * Remove the specified DetalleResumenPedido from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            Flash::error('Detalle Resumen Pedido not found');

            return redirect(route('detalleResumenPedidos.index'));
        }

        $this->detalleResumenPedidoRepository->delete($id);

        Flash::success('Detalle Resumen Pedido deleted successfully.');

        return redirect(route('detalleResumenPedidos.index'));
    }

    // public function tablaexelcolegio(Request $request)
    // {
    //     $request->validate([
    //         'inicio' => 'required',
    //         'fin' => 'required',
    //         'grado_id' => 'required|exists:secciones_colegios,id',
    //         'isbn_req' => 'required'
    //     ]);

    //     $inicio = $request->inicio;
    //     $fin = $request->fin;
    //     $grado_id = $request->grado;
    //     $isbn = $request->isbn;

    //     $colegio = Colegio::whereIn('codigo',
    //             Auth::user()->permissions->pluck('name')
    //         )->first();

    //     // Colegio > Pedidos > Resumenes > Detalles > Paquetes > Libros

    //     if($colegio) {
    //         $pedidos = $colegio->pedidos()
    //         ->where('created_at', '>=', $inicio)
    //         ->where('created_at', '<', $fin)
    //         ->with(['resumenes' => function($query) use ($grado_id, $isbn) {

    //             $query->when($isbn, function($query, $isbn) {
    //                 $query->whereHas('detalles.libro', function($query)  use ($isbn) {
    //                     $query->where('isbn', $isbn);
    //                 });
    //             });

    //             $query->when($grado_id, function($query, $grado_id) {
    //                 $query->whereHas('paquete', function($query, $grado_id) {
    //                     $query->where('nivel_id'. $grado_id);
    //                 });
    //             });

    //         }])
    //         ->get();

    //         $libros = $pedidos->pluck('libros')->collapse();
    //     }else{
    //         $libros = collect();
    //     }

    //     return view('private.tablaexelcolegio')
    //         ->with('libros',  $libros)
    //         ->with('inicio', $inicio)
    //         ->with('fin', $fin)
    //         ->with('colegio', $colegio)
    //         ->with('grado_id', $grado_id)
    //         ->with('isbn', $isbn);
    // }

    public function tablaReporteColegio(Request $request)
    {

        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;


    $colegio = Colegio::where('id',  $colegio_id)->first();

    $pedidos = $colegio->pedidos()
        ->whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59'])
        // ->where('created_at', '<', $fin)
        // ->where('created_at', '>', $inicio)
        ->with(['resumenes' => function($query)  use ($grado_id, $isbn_req){

            $query->whereHas('paquete', function($query) use ($grado_id, $isbn_req){
                if($grado_id != 0 || $grado_id != '0')
                    $query->where('nivel_id', $grado_id);
            });

            $query->with(['detalles' => function ($query)  use ($grado_id, $isbn_req){
                $query->whereHas('libro', function($query)  use ($grado_id, $isbn_req){
                    if($isbn_req != 0 || $isbn_req != '0')
                        $query->where('isbn', $isbn_req);
                });
            }]);
        }]);
    $pedidos = $pedidos->get();


    $libros = $pedidos->pluck('libros')->collapse();

    $libros = $colegio->libros()->wherePivot('paquete_id', null)->with([
        'paquetes.nivel.seccion' => function($query) use ($colegio, $inicio, $fin) {
            $query->where('colegio_id', $colegio->id);
        },
        'paquetes.resumenes.pedido' => function($query) use ($colegio, $inicio, $fin) {
            $query->where('colegio_id', $colegio->id);
            $query->whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59']);
            // $query->where('created_at', '<', $fin);
            // $query->where('created_at', '>', $inicio);
        }
    ]);

    // ISBN especifico
    if($isbn_req != 0 || $isbn_req != '0')
        $libros = $libros->where('isbn', $isbn_req);

    if($grado_id != 0 || $grado_id != '0')
        $libros = $libros->whereHas('paquetes', function($query) use($grado_id, $isbn_req) {
            $query->where('nivel_id', $grado_id);
        });



        return view('private.tablareportecolegio')->with('libros', $libros->get())->with('inicio', $inicio)->with('fin', $fin)->with('colegio', $colegio)->with('grado_id', $grado_id)->with('isbn_req',$isbn_req);

    }



    public function tablaReporteGeneral(Request $request)
    {

        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;
        $estatus = $request->estatus;

        if($colegio_id != '' || $colegio_id != ''){
            $colegio = Colegio::where('id',  $colegio_id)->first();
         }
         else
         {
            $colegio = '';
         }


        $detalles = DetalleResumenPedido::whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59'])

        ->whereHas('resumenPedido.pedido', function($query) use ($colegio,$grado_id, $isbn_req,$estatus,$colegio_id){
            if($colegio_id != '' || $colegio_id != ''){
               $query->where('colegio_id', $colegio->id);
            }
            if($estatus != '' || $estatus != ''){
                if($estatus == 0 || $estatus == '0')
                $query->where('status','=', 'PAGADO');
                if($estatus == 1 || $estatus == '1')
                $query->where('status','=', 'ENVIADO');
                if($estatus == 2 || $estatus == '2')
                $query->where('status','=', 'ENTREGADO');
                if($estatus == 3 || $estatus == '3')
                $query->where('status','=', 'ENVIO PARCIAL');
                if($estatus == 4 || $estatus == '4')
                $query->where('status','=', 'ENVIO LIQUIDADO');
            }

        })
        ->whereHas('resumenPedido.detalles.libro', function ($query)  use ($grado_id, $isbn_req){
                if($isbn_req != 0 || $isbn_req != '0')
                    $query->where('isbn', $isbn_req);
        })
        ->whereHas('resumenPedido.detalles.libro.paquetes.nivel', function ($query)  use ($grado_id, $isbn_req){
                if($grado_id != 0 || $grado_id != '0')
                $query->where('nivel_id', $grado_id);
        });

         return view('private.tablareportegeneral')
                ->with('detalles', $detalles->get())
                ->with('inicio', $inicio)
                ->with('fin', $fin)
                ->with('colegio', $colegio)
                ->with('grado_id', $grado_id)
                ->with('isbn_req', $isbn_req)
                ->with('estatus', $estatus);

    }


    public function tablaReporteFacturacion(Request $request)
    {

        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;

        $colegio = Colegio::where('id',  $colegio_id)->first();

        $facturas = DatosFacturas::whereBetween('created_at',[$inicio.' 00:00:00',$fin.' 23:59:59'])
        ->whereHas('pedido.colegio', function($query) use ($colegio){
            if($colegio != null)
            $query->where('id', $colegio->id);

        })
        ->get();

         return view('private.tablareportefacturacion')
                ->with('facturas', $facturas)
                ->with('inicio', $inicio)
                ->with('fin', $fin)
                ->with('colegio', $colegio);

    }

    public function tablaReporteCierre(Request $request)
    {

        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;


    $colegio = Colegio::where('id',  $colegio_id)->first();

    $pedidos = $colegio->pedidos()
        ->whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59'])
        // ->where('created_at', '<', $fin)
        // ->where('created_at', '>', $inicio)
        ->with(['resumenes' => function($query)  use ($grado_id, $isbn_req){

            $query->whereHas('paquete', function($query) use ($grado_id, $isbn_req){
                if($grado_id != 0 || $grado_id != '0')
                    $query->where('nivel_id', $grado_id);
            });

            $query->with(['detalles' => function ($query)  use ($grado_id, $isbn_req){
                $query->whereHas('libro', function($query)  use ($grado_id, $isbn_req){
                    if($isbn_req != 0 || $isbn_req != '0')
                        $query->where('isbn', $isbn_req);
                });
            }]);
        }]);
    $pedidos = $pedidos->get();


    $libros = $pedidos->pluck('libros')->collapse();

    $libros = $colegio->libros()->wherePivot('paquete_id', null)->with([
        'paquetes.nivel.seccion' => function($query) use ($colegio, $inicio, $fin) {
            $query->where('colegio_id', $colegio->id);
        },
        'paquetes.resumenes.pedido' => function($query) use ($colegio, $inicio, $fin) {
            $query->where('colegio_id', $colegio->id);
            $query->whereBetween('created_at', [$inicio . ' 00:00:00', $fin . ' 23:59:59']);
        }
    ]);

    // ISBN especifico
    if($isbn_req != 0 || $isbn_req != '0')
        $libros = $libros->where('isbn', $isbn_req);

    if($grado_id != 0 || $grado_id != '0')
        $libros = $libros->whereHas('paquetes', function($query) use($grado_id, $isbn_req) {
            $query->where('nivel_id', $grado_id);
        });

        return view('private.tablareportecierre')
        ->with('libros', $libros->get())
        ->with('inicio', $inicio)
        ->with('fin', $fin)
        ->with('colegio', $colegio)
        ->with('grado_id', $grado_id)
        ->with('isbn_req',$isbn_req);
    }

    public function editDetalle(UpdateDetalleResumenPedidoRequest $request, $id)
    {

        $user = Auth::user();
        $usuario_id = $user->id;

        $detalleResumen = DetalleResumenPedido::find($id);

        if (empty($detalleResumen)) {
            $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
            return response()->json(['success' => false, 'msg' => 'No existe', 'data' => $pedidoInformacion], 404);
        } else {

            if ($detalleResumen->resumenPedido->pedido->usuario_id != $usuario_id) {
                $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                return response()->json(['success' => false, 'msg' => 'No pertenece al usuario', 'data' => $pedidoInformacion], 403);
            } else {

                $detalleResumen->cantidad = $request->cantidad;

                try {
                    $detalleResumen->save();
                } catch (\Throwable $th) {
                    $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                    return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                }

                $detalleResumen->resumenPedido->update(['subtotal' => $detalleResumen->resumenPedido->getSubtotalAttribute($detalleResumen->resumenPedido->subtotal)]);

                $detalleResumen->resumenPedido->pedido->update([
                    'subtotal' => $detalleResumen->resumenPedido->pedido->getSubtotalAttribute($detalleResumen->resumenPedido->pedido->subtotal),
                    'total' => $detalleResumen->resumenPedido->pedido->getTotalAttribute($detalleResumen->resumenPedido->pedido->total),
                ]);

                $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                return response()->json(['success' => true, 'data' => $pedidoInformacion], 200);
            }
        }
    }

    public function deleteDetalle($id)
    {
        $eliminaPedido = false;

        $user = Auth::user();
        $usuario_id = $user->id;

        $detalleResumen = DetalleResumenPedido::find($id);

        $pedido = $detalleResumen->pedido->first();

        if ($pedido->status != 'CARRITO') {
            return response()->json(['success' => false, 'msg' => 'No se puede eliminar este elemento por que el pedido ya esta procesado'], 400);
        }

        if (empty($detalleResumen)) {
            return response()->json(['success' => false, 'msg' => 'No existe el detalle del resumen'], 404);
        } else {

            if ($detalleResumen->resumenPedido->pedido->usuario_id != $usuario_id) {
                $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                return response()->json(['success' => false, 'msg' => 'No pertenece al usuario', 'data' => $pedidoInformacion], 403);
            } else {
                $noDetalles = DetalleResumenPedido::where('resumen_pedidos_id', '=', $detalleResumen->resumen_pedidos_id)->count();

                if ($noDetalles == 1) {

                    $noResumenes = ResumenPedido::where('pedido_id', '=', $detalleResumen->resumenPedido->pedido->id)->count();

                    if ($noResumenes == 1) {
                        $eliminaPedido = true;
                        // elimina pedido
                        try {
                            $detalleResumen->resumenPedido->pedido->delete();
                        } catch (\Throwable $th) {
                            $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                            return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                        }
                    } else {
                        // elimina solo ese resumen
                        try {
                            $detalleResumen->resumenPedido->delete();
                        } catch (\Throwable $th) {
                            $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                            return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                        }
                    }
                } else {
                    // elimina solo este detalle
                    try {
                        $detalleResumen->delete();
                    } catch (\Throwable $th) {
                        $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                        return response()->json(['success' => false, 'msg' => $th->getMessage(), 'data' => $pedidoInformacion], 400);
                    }
                }
            }

            if ($eliminaPedido) {
                $data = array('carrito_vacio' => true);
                return response()->json(['success' => true, 'data' => $data], 200);
            } else {

                $detalleResumen->resumenPedido->update(['subtotal' => $detalleResumen->resumenPedido->getSubtotalAttribute($detalleResumen->resumenPedido->subtotal)]);

                $detalleResumen->resumenPedido->pedido->update([
                    'subtotal' => $detalleResumen->resumenPedido->pedido->getSubtotalAttribute($detalleResumen->resumenPedido->pedido->subtotal),
                    'total' => $detalleResumen->resumenPedido->pedido->getTotalAttribute($detalleResumen->resumenPedido->pedido->total),
                ]);

                $pedidoInformacion = PedidoController::datosCarrito($detalleResumen->resumenPedido->pedido);
                return response()->json(['success' => true, 'data' => $pedidoInformacion], 200);
            }
        }
    }


    public function update_libros_entregados(Request $request)
    {
        // $eliminaPedido = false;

        // $user = Auth::user();
        // $usuario_id = $user->id;

        // $detalleResumen = DetalleResumenPedido::find($id);
        dd($request->all());


    }
}
