<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNotificacionAPIRequest;
use App\Http\Requests\API\UpdateNotificacionAPIRequest;
use App\Models\Notificacion;
use App\Repositories\NotificacionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Transformers\Serializer;
use App\Transformers\NotificacionesTransformer;
use League\Fractal\Manager;

/**
 * Class NotificacionController
 * @package App\Http\Controllers\API
 */

class NotificacionAPIController extends AppBaseController
{
    /** @var  NotificacionRepository */
    private $notificacionRepository;

    public function __construct(NotificacionRepository $notificacionRepo)
    {
        $this->notificacionRepository = $notificacionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/notificacions",
     *      summary="Get a listing of the Notificacions.",
     *      tags={"Notificacion"},
     *      description="Get all Notificacions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Notificacion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request, $id)
    {
        // $notificacions = $this->notificacionRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
         /** @var Notificacion $notificacion */
         $notificacion = Notificacion::where('colegio_id', $id)->where('activo', 1)->get();
         if (empty($notificacion)) {
             return $this->sendError('Notificacion not found');
         }
         // $notificacion = Notificacion::where('colegio_id', $id)->get();

        //  return $this->sendResponse($notificacion->toArray(), 'Notificacion retrieved successfully');
        //return $this->sendResponse($notificacion->toArray(), 'Notificacions retrieved successfully');

        $fractal = new Manager();

        $notificacion = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($notificacion, new NotificacionesTransformer))->toArray();

        return $this->sendResponse($notificacion, "Información obtenida");



    }

    /**
     * @param CreateNotificacionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/notificacions",
     *      summary="Store a newly created Notificacion in storage",
     *      tags={"Notificacion"},
     *      description="Store Notificacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Notificacion that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Notificacion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notificacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateNotificacionAPIRequest $request)
    {
        $input = $request->all();

        $notificacion = $this->notificacionRepository->create($input);

        return $this->sendResponse($notificacion->toArray(), 'Notificacion saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/notificacions/{id}",
     *      summary="Display the specified Notificacion",
     *      tags={"Notificacion"},
     *      description="Get Notificacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notificacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notificacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Notificacion $notificacion */
        $notificacion = $this->notificacionRepository->find($id);

        if (empty($notificacion)) {
            return $this->sendError('Notificacion not found');
        }

        return $this->sendResponse($notificacion->toArray(), 'Notificacion retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateNotificacionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/notificacions/{id}",
     *      summary="Update the specified Notificacion in storage",
     *      tags={"Notificacion"},
     *      description="Update Notificacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notificacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Notificacion that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Notificacion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Notificacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateNotificacionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Notificacion $notificacion */
        $notificacion = $this->notificacionRepository->find($id);

        if (empty($notificacion)) {
            return $this->sendError('Notificacion not found');
        }

        $notificacion = $this->notificacionRepository->update($input, $id);

        return $this->sendResponse($notificacion->toArray(), 'Notificacion updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/notificacions/{id}",
     *      summary="Remove the specified Notificacion from storage",
     *      tags={"Notificacion"},
     *      description="Delete Notificacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Notificacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Notificacion $notificacion */
        $notificacion = $this->notificacionRepository->find($id);

        if (empty($notificacion)) {
            return $this->sendError('Notificacion not found');
        }

        $notificacion->delete();

        return $this->sendSuccess('Notificacion deleted successfully');
    }
}
