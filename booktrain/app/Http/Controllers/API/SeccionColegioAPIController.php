<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeccionColegioAPIRequest;
use App\Http\Requests\API\UpdateSeccionColegioAPIRequest;
use App\Models\SeccionColegio;
use App\Repositories\SeccionColegioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SeccionColegioController
 * @package App\Http\Controllers\API
 */

class SeccionColegioAPIController extends AppBaseController
{
    /** @var  SeccionColegioRepository */
    private $seccionColegioRepository;

    public function __construct(SeccionColegioRepository $seccionColegioRepo)
    {
        $this->seccionColegioRepository = $seccionColegioRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/seccionColegios",
     *      summary="Get a listing of the SeccionColegios.",
     *      tags={"SeccionColegio"},
     *      description="Get all SeccionColegios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SeccionColegio")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $seccionColegios = $this->seccionColegioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seccionColegios->toArray(), 'Seccion Colegios retrieved successfully');
    }

    /**
     * @param CreateSeccionColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/seccionColegios",
     *      summary="Store a newly created SeccionColegio in storage",
     *      tags={"SeccionColegio"},
     *      description="Store SeccionColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SeccionColegio that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SeccionColegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SeccionColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSeccionColegioAPIRequest $request)
    {
        $input = $request->all();

        $seccionColegio = $this->seccionColegioRepository->create($input);

        return $this->sendResponse($seccionColegio->toArray(), 'Seccion Colegio saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/seccionColegios/{id}",
     *      summary="Display the specified SeccionColegio",
     *      tags={"SeccionColegio"},
     *      description="Get SeccionColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SeccionColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SeccionColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SeccionColegio $seccionColegio */
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            return $this->sendError('Seccion Colegio not found');
        }

        return $this->sendResponse($seccionColegio->toArray(), 'Seccion Colegio retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSeccionColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/seccionColegios/{id}",
     *      summary="Update the specified SeccionColegio in storage",
     *      tags={"SeccionColegio"},
     *      description="Update SeccionColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SeccionColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SeccionColegio that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SeccionColegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SeccionColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSeccionColegioAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeccionColegio $seccionColegio */
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            return $this->sendError('Seccion Colegio not found');
        }

        $seccionColegio = $this->seccionColegioRepository->update($input, $id);

        return $this->sendResponse($seccionColegio->toArray(), 'SeccionColegio updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/seccionColegios/{id}",
     *      summary="Remove the specified SeccionColegio from storage",
     *      tags={"SeccionColegio"},
     *      description="Delete SeccionColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SeccionColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SeccionColegio $seccionColegio */
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            return $this->sendError('Seccion Colegio not found');
        }

        $seccionColegio->delete();

        return $this->sendSuccess('Seccion Colegio deleted successfully');
    }
}
