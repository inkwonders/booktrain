<?php

namespace App\Http\Controllers\API;

use Response;
use Illuminate\Http\Request;
use App\Models\DatosFacturas;
use App\Exports\DatosFacturasExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DatosFacturasRepository;
use App\Http\Requests\API\CreateDatosFacturasAPIRequest;
use App\Http\Requests\API\UpdateDatosFacturasAPIRequest;

/**
 * Class DatosFacturasController
 * @package App\Http\Controllers\API
 */

class DatosFacturasAPIController extends AppBaseController
{
    /** @var  DatosFacturasRepository */
    private $datosFacturasRepository;

    public function __construct(DatosFacturasRepository $datosFacturasRepo)
    {
        $this->datosFacturasRepository = $datosFacturasRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/datosFacturas",
     *      summary="Get a listing of the DatosFacturas.",
     *      tags={"DatosFacturas"},
     *      description="Get all DatosFacturas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/DatosFacturas")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $datosFacturas = $this->datosFacturasRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($datosFacturas->toArray(), 'Datos Facturas retrieved successfully');
    }

    /**
     * @param CreateDatosFacturasAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/datosFacturas",
     *      summary="Store a newly created DatosFacturas in storage",
     *      tags={"DatosFacturas"},
     *      description="Store DatosFacturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DatosFacturas that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DatosFacturas")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DatosFacturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDatosFacturasAPIRequest $request)
    {
        $input = $request->all();

        $datosFacturas = $this->datosFacturasRepository->create($input);

        return $this->sendResponse($datosFacturas->toArray(), 'Datos Facturas saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/datosFacturas/{id}",
     *      summary="Display the specified DatosFacturas",
     *      tags={"DatosFacturas"},
     *      description="Get DatosFacturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DatosFacturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DatosFacturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var DatosFacturas $datosFacturas */
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            return $this->sendError('Datos Facturas not found');
        }

        return $this->sendResponse($datosFacturas->toArray(), 'Datos Facturas retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDatosFacturasAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/datosFacturas/{id}",
     *      summary="Update the specified DatosFacturas in storage",
     *      tags={"DatosFacturas"},
     *      description="Update DatosFacturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DatosFacturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DatosFacturas that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DatosFacturas")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DatosFacturas"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDatosFacturasAPIRequest $request)
    {
        $input = $request->all();

        /** @var DatosFacturas $datosFacturas */
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            return $this->sendError('Datos Facturas not found');
        }

        $datosFacturas = $this->datosFacturasRepository->update($input, $id);

        return $this->sendResponse($datosFacturas->toArray(), 'DatosFacturas updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/datosFacturas/{id}",
     *      summary="Remove the specified DatosFacturas from storage",
     *      tags={"DatosFacturas"},
     *      description="Delete DatosFacturas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DatosFacturas",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var DatosFacturas $datosFacturas */
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            return $this->sendError('Datos Facturas not found');
        }

        $datosFacturas->delete();

        return $this->sendSuccess('Datos Facturas deleted successfully');
    }




    public function facturasXLS(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;


          /** pruebas */
        //   return Excel::download(new DatosFacturasExport('2020-01-01','2022-01-01'), 'datosFacturas.xlsx');

          return Excel::download(new DatosFacturasExport($inicio,$fin,$colegio_id), 'datosFacturas.xlsx');

    }
}
