<?php

namespace App\Http\Controllers\API;

use Response;
use Illuminate\Http\Request;
use App\Models\DetalleResumenPedido;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DetalleResumenPedidoExport;
use App\Exports\DetalleResumenPedidoColegioExport;
use App\Exports\DetalleResumenPedidosFacturacion;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DetalleResumenPedidoRepository;
use App\Http\Requests\API\CreateDetalleResumenPedidoAPIRequest;
use App\Http\Requests\API\UpdateDetalleResumenPedidoAPIRequest;
use App\Exports\DetalleResumenPedidoColegioAdminExport;
use App\Exports\DetalleResumenCierreColegioAdminExport;
use App\Exports\DetalleResumenPedidosReferencias;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

/**
 * Class DetalleResumenPedidoController
 * @package App\Http\Controllers\API
 */

class DetalleResumenPedidoAPIController extends AppBaseController
{
    /** @var  DetalleResumenPedidoRepository */
    private $detalleResumenPedidoRepository;

    public function __construct(DetalleResumenPedidoRepository $detalleResumenPedidoRepo)
    {
        $this->detalleResumenPedidoRepository = $detalleResumenPedidoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/detalleResumenPedidos",
     *      summary="Get a listing of the DetalleResumenPedidos.",
     *      tags={"DetalleResumenPedido"},
     *      description="Get all DetalleResumenPedidos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/DetalleResumenPedido")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $detalleResumenPedidos = $this->detalleResumenPedidoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detalleResumenPedidos->toArray(), 'Detalle Resumen Pedidos retrieved successfully');
    }

    /**
     * @param CreateDetalleResumenPedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/detalleResumenPedidos",
     *      summary="Store a newly created DetalleResumenPedido in storage",
     *      tags={"DetalleResumenPedido"},
     *      description="Store DetalleResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DetalleResumenPedido that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DetalleResumenPedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DetalleResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDetalleResumenPedidoAPIRequest $request)
    {
        $input = $request->all();

        $detalleResumenPedido = $this->detalleResumenPedidoRepository->create($input);

        return $this->sendResponse($detalleResumenPedido->toArray(), 'Detalle Resumen Pedido saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/detalleResumenPedidos/{id}",
     *      summary="Display the specified DetalleResumenPedido",
     *      tags={"DetalleResumenPedido"},
     *      description="Get DetalleResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DetalleResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DetalleResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var DetalleResumenPedido $detalleResumenPedido */
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            return $this->sendError('Detalle Resumen Pedido not found');
        }

        return $this->sendResponse($detalleResumenPedido->toArray(), 'Detalle Resumen Pedido retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDetalleResumenPedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/detalleResumenPedidos/{id}",
     *      summary="Update the specified DetalleResumenPedido in storage",
     *      tags={"DetalleResumenPedido"},
     *      description="Update DetalleResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DetalleResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DetalleResumenPedido that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DetalleResumenPedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DetalleResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDetalleResumenPedidoAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetalleResumenPedido $detalleResumenPedido */
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            return $this->sendError('Detalle Resumen Pedido not found');
        }

        $detalleResumenPedido = $this->detalleResumenPedidoRepository->update($input, $id);

        return $this->sendResponse($detalleResumenPedido->toArray(), 'DetalleResumenPedido updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/detalleResumenPedidos/{id}",
     *      summary="Remove the specified DetalleResumenPedido from storage",
     *      tags={"DetalleResumenPedido"},
     *      description="Delete DetalleResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DetalleResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var DetalleResumenPedido $detalleResumenPedido */
        $detalleResumenPedido = $this->detalleResumenPedidoRepository->find($id);

        if (empty($detalleResumenPedido)) {
            return $this->sendError('Detalle Resumen Pedido not found');
        }

        $detalleResumenPedido->delete();

        return $this->sendSuccess('Detalle Resumen Pedido deleted successfully');
    }


    public function detallesXLS(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;
        $estatus = $request->estatus;

        /** pruebas */
        // return Excel::download(new DetalleResumenPedidoExport('2020-01-01','2022-01-01'), 'detallesPedidos.xlsx');

        /** el bueno */
        return Excel::download(new DetalleResumenPedidoExport($inicio,$fin,$colegio_id,$isbn_req,$grado_id,$estatus), 'PedidosGeneralesAdmin.xlsx');

    }

    public function detallesColegioXLS(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;


        /** pruebas */
        // return Excel::download(new DetalleResumenPedidoColegioExport('2020-01-01','2022-01-01'), 'detallesPedidosColegios.xlsx');

        /** el bueno */
        return Excel::download(new DetalleResumenPedidoColegioExport($inicio,$fin,$colegio_id,$isbn_req,$grado_id), 'detallesPedidosColegios.xlsx');

    }

    public function detallesAdminColegioXLS(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;


        /** pruebas */
        // return Excel::download(new DetalleResumenPedidoColegioExport('2020-01-01','2022-01-01'), 'detallesPedidosColegios.xlsx');

        /** el bueno */
        return Excel::download(new DetalleResumenPedidoColegioAdminExport($inicio,$fin,$colegio_id,$isbn_req,$grado_id), 'detallesPedidosColegiosAdmin.xlsx');

    }

    public function detallesAdminCierreColegioXLS(Request $request)
    {
        $inicio = $request->inicio;
        $fin = $request->fin;
        $colegio_id = $request->colegio;
        $grado_id = $request->grado;
        $isbn_req = $request->isbn;


        /** pruebas */
        // return Excel::download(new DetalleResumenPedidoColegioExport('2020-01-01','2022-01-01'), 'detallesPedidosColegios.xlsx');

        /** el bueno */
        return Excel::download(new DetalleResumenCierreColegioAdminExport($inicio,$fin,$colegio_id,$isbn_req,$grado_id), 'detallesCierreColegiosAdmin.xlsx');

    }

    public function pedidosReferenciasXLS(Request $request)
    {
        $status = $request->status;
        $factura = $request->factura;
        $metodo_pagos = $request->metodo_pagos;
        $forma_pagos = $request->forma_pagos;
        $forma_entrega = $request->forma_entrega;
        $inicio = $request->inicio;
        $fin = $request->fin;

        return Excel::download(new DetalleResumenPedidosReferencias($status, $factura, $metodo_pagos,  $forma_pagos, $forma_entrega,  $inicio , $fin), 'detallesPedidosReferenciasAdmin.xlsx');
    }

}
