<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\Pedido;
use App\Models\Colegio;
use App\Models\FormaPago;
use League\Fractal\Manager;
use App\Models\NivelColegio;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ColegioRepository;
use League\Fractal\Resource\Collection;
use App\Transformers\ColegioTransformer;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;
use App\Transformers\NivelesColegioTransformer;
use App\Http\Requests\API\CreateColegioAPIRequest;
use App\Http\Requests\API\UpdateColegioAPIRequest;
use App\Models\Cupon;
use App\Models\MetodoPago;
use App\Models\User;

/**
 * Class ColegioController
 * @package App\Http\Controllers\API
 */

class ColegioAPIController extends AppBaseController
{
    /** @var  ColegioRepository */
    private $colegioRepository;

    public function __construct(ColegioRepository $colegioRepo)
    {
        $this->colegioRepository = $colegioRepo;
    }

    public function getInformacion(Request $request)
    {
        $reglas = [
            'codigo' => 'required'
        ];

        $mensajes = [
            'codigo.required' => 'El código es obligatorio'
        ];

        $validador = Validator::make($request->all(), $reglas, $mensajes);

        if ($validador->fails()) {
            return redirect('inicio')->with('error', $validador->errors()->first());
        }

        $colegio = Colegio::where('codigo', $request->codigo)->first();

        if (empty($colegio)) {
            return redirect('inicio')->with('error', 'El código es incorrecto.');
        }

        if ($colegio->activo == 0) {
            return redirect('inicio')->with('error', 'Por el momento el colegio no está disponible.');
        }

        $fractal = new Manager();

        $colegioInformacion = $fractal->setSerializer(new Serializer())
            ->parseIncludes(['secciones'])
            ->createData(new Item($colegio, new ColegioTransformer))->toArray();

        return $this->sendResponse($colegioInformacion, "Información obtenida correctamente");
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/colegios",
     *      summary="Get a listing of the Colegios.",
     *      tags={"Colegio"},
     *      description="Get all Colegios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Colegio")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $colegios = $this->colegioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $fractal = new Manager();

        $colegios = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($colegios, new ColegioTransformer))->toArray();

        return $this->sendResponse($colegios, 'Colegios retrieved successfully');
    }

    /**
     * @param CreateColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/colegios",
     *      summary="Store a newly created Colegio in storage",
     *      tags={"Colegio"},
     *      description="Store Colegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Colegio that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Colegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Colegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateColegioAPIRequest $request)
    {
        $input = $request->all();

        $colegio = $this->colegioRepository->create($input);

        return $this->sendResponse($colegio->toArray(), 'Colegio saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/colegios/{id}",
     *      summary="Display the specified Colegio",
     *      tags={"Colegio"},
     *      description="Get Colegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Colegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Colegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Colegio $colegio */
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            return $this->sendError('Colegio not found');
        }

        $fractal = new Manager();

        $colegioInformacion = $fractal->setSerializer(new Serializer())
            ->createData(new Item($colegio, new ColegioTransformer));

        return $this->sendResponse($colegioInformacion->toArray(), 'Colegio retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/colegios/{id}",
     *      summary="Update the specified Colegio in storage",
     *      tags={"Colegio"},
     *      description="Update Colegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Colegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Colegio that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Colegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Colegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateColegioAPIRequest $request)
    {
        $input = $request->all();

        /** @var Colegio $colegio */
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            return $this->sendError('Colegio not found');
        }

        $colegio = $this->colegioRepository->update($input, $id);

        return $this->sendResponse($colegio->toArray(), 'Colegio updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/colegios/{id}",
     *      summary="Remove the specified Colegio from storage",
     *      tags={"Colegio"},
     *      description="Delete Colegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Colegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Colegio $colegio */
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            return $this->sendError('Colegio not found');
        }

        $colegio->delete();

        return $this->sendSuccess('Colegio deleted successfully');
    }

    /*
     *
     */
    public function getColegiosISBN(Request $request, $colegio_id)
    {
        $colegio = $this->colegioRepository->find($colegio_id);

        if (empty($colegio)) {
            return $this->sendError('Colegio not found');
        }

        $niveles = $colegio->niveles->transform(function ($item) {
            $item->makeHidden('activo');
            $item->makeHidden('created_at');
            $item->makeHidden('updated_at');
            $item->makeHidden('laravel_through_key');
            $item->makeHidden('seccionColegio');
            return $item;
        });

        $isbn = $colegio->catalogoLibros->toArray();

        $libros = $colegio->catalogoLibros->transform(function ($item) {
            return $item->libro;
        });

        $cajas = $colegio->cajas()->with('usuario')->get();

        return $this->sendResponse(['niveles' => $niveles, 'libros' => $libros, 'cajas' => $cajas], 'Information retrieved successfully');
    }

    public function colegioslibrosISBN(Request $request, $colegio_id)
    {
        $colegio = $this->colegioRepository->find($colegio_id);

        if (empty($colegio)) {
            return $this->sendError('Colegio not found');
        }

        $niveles = $colegio->niveles->transform(function ($item) {
            $item->makeHidden('activo');
            $item->makeHidden('created_at');
            $item->makeHidden('updated_at');
            $item->makeHidden('laravel_through_key');
            $item->makeHidden('seccionColegio');
            return $item;
        });

        $isbn = $colegio->catalogoLibros->toArray();

        $libros = $colegio->catalogoLibros->transform(function ($item) {
            return $item->libro;
        });

        return $this->sendResponse(['niveles' => $niveles, 'libros' => $libros], 'Information retrieved successfully');
    }

    /**
     * Devuelve la información del colegio asociado al usuario que está realizando la venta movil
     *
     * Grados, secciones y paquetes con sus libros
     * Métodos de pago para el colegio disponibles para la venta movil
     * Logotipo del colegio
     * Avisos para el tablero
     */
    public function configuracionVentaMovil(Colegio $colegio)
    {
        return $this->sendResponse($colegio->configuracion_venta_movil, "Configuración obtenida de {$colegio->codigo}");
    }

    public function pagarVentaMovil(Request $request)
    {
        Log::debug('ColegioAPIController::pagarVentaMovil', $request->all());

        /**
         * array:7 [
         *   "colegio_id" => 1
         *   "metodo_pago_id" => 6
         *   "forma_pago_id" => 1
         *   "pedido_id" => 4143
         *   "referencia" => "123456798"
         *   "privacidad" => true
         *   "cliente" => array:4 [
         *       "nombre" => "jacobo"
         *       "apellidos" => "GT"
         *       "celular" => "4611989064"
         *       "correo" => "girisnotadog@gmail.com"
         *   ]
         * ]
         */

        $request->validate([
            'colegio_id'        => 'required|exists:colegios,id',
            'metodo_pago_id'    => 'required|exists:metodos_pagos,id',
            'forma_pago_id'     => 'required|exists:formas_pagos,id',
            'pedido_id'         => 'required|exists:pedidos,id',
            'referencia'        => 'sometimes',
            'privacidad'        => 'required|accepted',
            'cliente.nombre'    => 'required',
            'cliente.apellidos' => 'required',
            'cliente.celular'   => 'required',
            'cliente.correo'    => 'required|email',
            // 'codigo_cupon'      => 'sometimes|required|exists:cupones,codigo',
        ]);

        DB::beginTransaction();

        $id_pago = 0;

        try {
            $colegio = Colegio::find($request->get('colegio_id'));

            $forma_pago = $colegio->formasPago()->wherePivot('metodo_pago_id', $request->metodo_pago_id)->where('forma_pago_id', $request->forma_pago_id)->first();

            $metodo_pago = MetodoPago::find($request->metodo_pago_id);

            $pedido = Pedido::find($request->pedido_id);

            if ($forma_pago->pivot->activo == 0) {
                return $this->sendError('La forma de pago no está disponible', 403);
            }

            $pedido->update([
                'status'                => 'ENTREGADO', // En este tipo de venta se considera entregado el pedido una vez pagado
                'metodo_pago'           => $metodo_pago->codigo,
                'forma_pago'            => $forma_pago->codigo,
                'usuario_venta_id'      => Auth::user()->id,
                'nombre_contacto'       => $request->cliente['nombre'],
                'apellidos_contacto'    => $request->cliente['apellidos'],
                'celular_contacto'      => $request->cliente['celular'],
                'email_pedido'          => $request->cliente['correo'],
                'tipo_entrega'          => 1, // Entrega en el colegio,
                'terminos_condiciones'  => $request->privacidad,
                'metodo_pago_id'        => $request->metodo_pago_id,
                'forma_pago_id'         => $request->forma_pago_id,
            ]);

            // Marcamos todos los detalles del pedido como entregados
            $pedido->resumenes->each(function ($resumen) {
                $resumen->detalles->each(function ($detalle) {
                    $detalle->update(['entregado' => 1]);
                });
            });

            $id_pago = $pedido->pagos()->create([
                'amount'             => $pedido->total,
                'referencia'         => $request->referencia,
                'source'             => 'venta_movil',
                'transactionTokenId' => uniqid(),
                'raw'                => json_encode($request->all()),
                'status'             => 'SUCCESS',
                'origen'             => 'VENTA MOVIL'
            ]);

            $pedido->save();

            // Buscamos el cupón introducido
            $cupon = Cupon::where('codigo', $request->codigo_cupon)->first();

            if ($cupon && $cupon->status == 'DISPONIBLE') {
                $cupon->pedido()->associate($pedido->id)->save();
                $cupon->afectarPedido();
            }

            $pedido->refresh();
            $pedido->procesar();

            // Asociamos el pedido a la caja abierta
            $caja_usuario = Auth::user()->cajas()->abierta();

            if ($caja_usuario)
                $caja_usuario->pedidos()->attach($pedido->id, []);

            $referencia = $request->has('referencia') ? $request->referencia : '';

            activity()
                ->performedOn($pedido)
                ->causedBy(Auth::user()->id)
                ->log("Registró venta movil en el pedido {$pedido->serie}{$pedido->folio} con la referencia {$referencia}");

            $pedido->sendPagoExitosoVentaMovilNotification();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::debug("Error al procesar el pago", [$th]);
            return $this->sendError('Error al procesar el pago, intente más tarde', 503);
        }

        DB::commit();

        return $this->sendSuccess('Pago registrado exitosamente: ' . $id_pago);
    }

    public function canjearCupon(Request $request)
    {
        $request->validate([
            'codigo' => 'required|min:13'
        ]);

        // Buscamos el cupón
        $cupones = Cupon::whereCodigo($request->codigo)
            ->whereNull('pedido_id')->get()
            ->filter(function ($cupon) {
                return $cupon->status == 'DISPONIBLE';
            });

        if ($cupones->isNotEmpty())
            return $this->sendResponse($cupones->map(function ($cupon) {
                return (object) [
                    'codigo'        => $cupon->codigo,
                    'valor'         => $cupon->valor,
                    'caducidad'     => $cupon->caducidad,
                ];
            })->first(), 'Cupón disponible');
        else
            return $this->sendError('El cupón no existe o ya ha sido utilizado');
    }

    public function cajasCerradas(Request $request, $colegio_id)
    {
        $colegio = $this->colegioRepository->find($colegio_id);

        $cajas = $colegio->cajas()->where('status', "CERRADA")->whereBetween('fecha_cierre', [$request->fecha_cierre . ' 00:00:00', $request->fecha_cierre . ' 23:59:59'])->get();

        foreach ($cajas as $caja) {
            $usuario_caja = User::find($caja->user_id);
            $caja->usuario_nombre =  $usuario_caja->name . " " . $usuario_caja->apellidos;
        }

        return $this->sendResponse(['cajas' => $cajas], 'Information retrieved successfully');
    }
}
