<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaqueteAPIRequest;
use App\Http\Requests\API\UpdatePaqueteAPIRequest;
use App\Models\Paquete;
use App\Repositories\PaqueteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PaqueteController
 * @package App\Http\Controllers\API
 */

class PaqueteAPIController extends AppBaseController
{
    /** @var  PaqueteRepository */
    private $paqueteRepository;

    public function __construct(PaqueteRepository $paqueteRepo)
    {
        $this->paqueteRepository = $paqueteRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/paquetes",
     *      summary="Get a listing of the Paquetes.",
     *      tags={"Paquete"},
     *      description="Get all Paquetes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Paquete")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $paquetes = $this->paqueteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($paquetes->toArray(), 'Paquetes retrieved successfully');
    }

    /**
     * @param CreatePaqueteAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/paquetes",
     *      summary="Store a newly created Paquete in storage",
     *      tags={"Paquete"},
     *      description="Store Paquete",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Paquete that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Paquete")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paquete"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePaqueteAPIRequest $request)
    {
        $input = $request->all();

        $paquete = $this->paqueteRepository->create($input);

        return $this->sendResponse($paquete->toArray(), 'Paquete saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/paquetes/{id}",
     *      summary="Display the specified Paquete",
     *      tags={"Paquete"},
     *      description="Get Paquete",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paquete",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paquete"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Paquete $paquete */
        $paquete = $this->paqueteRepository->find($id);

        if (empty($paquete)) {
            return $this->sendError('Paquete not found');
        }

        return $this->sendResponse($paquete->toArray(), 'Paquete retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePaqueteAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/paquetes/{id}",
     *      summary="Update the specified Paquete in storage",
     *      tags={"Paquete"},
     *      description="Update Paquete",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paquete",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Paquete that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Paquete")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Paquete"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePaqueteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Paquete $paquete */
        $paquete = $this->paqueteRepository->find($id);

        if (empty($paquete)) {
            return $this->sendError('Paquete not found');
        }

        $paquete = $this->paqueteRepository->update($input, $id);

        return $this->sendResponse($paquete->toArray(), 'Paquete updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/paquetes/{id}",
     *      summary="Remove the specified Paquete from storage",
     *      tags={"Paquete"},
     *      description="Delete Paquete",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Paquete",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Paquete $paquete */
        $paquete = $this->paqueteRepository->find($id);

        if (empty($paquete)) {
            return $this->sendError('Paquete not found');
        }

        $paquete->delete();

        return $this->sendSuccess('Paquete deleted successfully');
    }
}
