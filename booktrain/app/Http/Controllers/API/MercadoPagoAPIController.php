<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use MercadoPago;


use App\Models\Pedido;
use App\Models\Direccion;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use App\Models\RegistroPago;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\Configuracion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class MercadoPagoAPIController extends AppBaseController
{

    public function getPreference(Request $request){

        Log::debug('MercadoPagoAPIController@getPreference: Datos de solicitud', $request->all());


        $request->validate([

            'pedido.pedido_id'            => 'sometimes|exists:pedidos,id',
            'contacto.nombre'               => 'required',
            'contacto.apellidos'            => 'required',
            'contacto.celular'              => 'required',
            'contacto.correo'               => 'nullable|email',
            'tipo_entrega'                  => 'sometimes|required|between:1,3',
            'id_direccion_envio'            => 'sometimes|exists:direcciones,id',
            'facturacion_requerida'         => 'required|boolean',
            'facturacion.nombre'            => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.rfc'               => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.correo'            => 'exclude_if:facturacion_requerida,false|sometimes|email',
            'facturacion.cfdi'              => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.id_cfdi'           => 'exclude_if:facturacion_requerida,false|sometimes|exists:cfdis,id',

            'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_interior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',

            'terminos_aceptados'            => 'required|accepted',
            'forma_pago'                    => 'required|in:PUE,DIFERIDO3M,DIFERIDO6M,DIFERIDO9M',
            'metodo_pago'                   => 'required|in:CREDITO,DEBITO,AMEX,BANCO,TIENDA,EFECTIVO,TERMINAL,TRANSFERENCIA,BBVADEBITOVPOS,BBVACREDITOVPOS,BBVATARJETA,OPDEBITO,OPAMEX,OPMASTERCARD,OPEFECTIVO,OPSPEI,OPBANCO,OPTIENDA,OPQRALIPAY,OPQRCODI,OPIVR,MERCADO PAGO',
            'direccion.calle'               => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_exterior'         => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_interior'         => 'sometimes',
            'direccion.colonia'             => 'sometimes|required_if:tipo_entrega,2',
            'direccion.cp'                  => 'sometimes|required_if:tipo_entrega,2',
            'direccion.estado'              => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio'           => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio_id'        => 'sometimes|required_if:tipo_entrega,2|exists:municipios,id',
            'direccion.estado_id'           => 'sometimes|required_if:tipo_entrega,2|exists:estados,id',
            'direccion.referencia'          => 'sometimes',
            'tarjeta.​​address'               => 'sometimes',// null
            'tarjeta.​​brand'                 => 'sometimes|required_with:tarjeta',// "mastercard"
            'tarjeta.​​card_number'           => 'sometimes|required_with:tarjeta',// "545454XXXXXX5454"
            'tarjeta.​​creation_date'         => 'sometimes',// null
            'tarjeta.​​expiration_month'      => 'sometimes|required_with:tarjeta',// "10"
            'tarjeta.​​expiration_year'       => 'sometimes|required_with:tarjeta',// "22"
            'tarjeta.​​holder_name'           => 'sometimes|required_with:tarjeta',// "Prueba"

        ]);

        $pedido_id=$request->pedido['pedido_id'];

        try {

            $user_id=$request->contacto['user_id'];

            $user=User::find($user_id);

            $pedido = $user
                ->pedidos()
                ->whereId($pedido_id)
                ->whereStatus('CARRITO')
                ->first();

            // dd($pedido);


            // dd($request);

            DB::beginTransaction();

            if( ! $pedido){
                Log::debug('MercadoPagoAPIController@getPreference: El pedido no existe o ya fue pagado', ['pedido_id' => $pedido_id]);

                return response()->json(['success' => false, 'message' => 'El pedido no existe o ya ha sido procesado'], 404);
            }

                $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->first();
                $forma_pago = FormaPago::where('codigo', $request->forma_pago)->first();

                // dd($metodo_pago, $forma_pago);

                $pedido->update([
                    'nombre_contacto'       => $request->contacto['nombre'],
                    'apellidos_contacto'    => $request->contacto['apellidos'],
                    'celular_contacto'      => $request->contacto['celular'],
                    'correo_contacto'       => $request->contacto['correo'],
                    'tipo_entrega'          => $request->tipo_entrega,
                    'terminos_condiciones'  => $request->terminos_aceptados,
                    // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                    'metodo_pago'           => $request->metodo_pago,
                    'forma_pago'            => $request->forma_pago,
                    'status'                => 'PROCESADO',
                    'metodo_pago_id'        => $metodo_pago->id,
                    'forma_pago_id'         => $forma_pago->id,
                ]);

                if($request->facturacion_requerida) {
                    // SEGUN EL SAT
                    // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                    // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T. DEBITO, ID 3 = T. CREDITO

                    $forma_pago_factura = 1;
                    $metodo_pago_factura = 1;

                    switch($request->forma_pago) {
                        case 'PUE': $metodo_pago_factura = 1; break;
                        default: $metodo_pago_factura = 2; break;
                    }

                    switch($request->metodo_pago) {
                        case 'OPSPEI': $forma_pago_factura = 1; break; // Inalcanzable
                        case 'OPBANCO': $forma_pago_factura = 1; break; // Inalcanzable
                        case 'OPEFECTIVO': $forma_pago_factura = 1; break; // Inalcanzable

                        //case 'OPDEBITO': $forma_pago_factura = 2; break; // antes version jacobo
                        //case 'OPMASTERCARD': $forma_pago_factura = 3; break;// antes version jacobo
                        //case 'OPAMEX': $forma_pago_factura = 3; break;// antes version jacobo

                        case 'OPDEBITO': $forma_pago_factura = 3; break;
                        case 'OPMASTERCARD': $forma_pago_factura = 2; break;
                        case 'OPAMEX': $forma_pago_factura = 2; break;
                    }

                    // Eliminamos los datos anteriores
                    $datos_factura = $pedido->datosFactura;
                    if($datos_factura != null) $datos_factura->delete();

                    $metodo_pago_descripcion = null;

                    switch ($metodo_pago_factura) {
                        case 1: $metodo_pago_descripcion = 'PUE';break;
                        case 2: $metodo_pago_descripcion = 'PDD';break;
                    }

                    $forma_pago_descripcion = null;

                    switch ($forma_pago_factura) {
                        case 1: $forma_pago_descripcion = '01-EFECTIVO';break;
                        case 2: $forma_pago_descripcion = '04-TARJETA CREDITO';break;
                        case 3: $forma_pago_descripcion = '28-TARJETA DEBITO';break;
                    }

                    //SE DEJA X DEFAULT XQ NO SABEMOS Q TIPOD  ETARJETA USARA
                    $forma_pago_descripcion = '99-POR DEFINIR';

                    $pedido->datosFactura()->create([
                        'user_id'        => $pedido->usuario_id,
                        'id_cfdi'        => $request->facturacion['id_cfdi'],
                        'metodo_pago_id' => $metodo_pago_factura,
                        'forma_pago_id'  => $forma_pago_factura,
                        'forma_pago'     => $forma_pago_descripcion,
                        'metodo_pago'    => $metodo_pago_descripcion,
                        'razon_social'   => $request->facturacion['razon_social'],
                        'rfc'            => $request->facturacion['rfc'],
                        'correo'         => $request->facturacion['correo'],
                        'cp'             => $request->facturacion['cp'],
                        'calle'          => $request->facturacion['calle'],
                        'num_exterior'   => $request->facturacion['num_exterior'],
                        'num_interior'   => $request->facturacion['num_interior'],
                        'colonia'        => $request->facturacion['colonia'],
                        'municipio'      => $request->facturacion['municipio'],
                        'estado'         => $request->facturacion['estado'],
                        'regimen_fiscal' => $request->facturacion['regimen_fiscal'],
                    ]);

                    $pedido->update(['factura' => 1]);
                }

                $direccion = $pedido->direccion()->dissociate(); // <<<< Verificar que eso sea correcto

                if ($request->tipo_entrega == 2) { // a domicilio
                    $direccion = Direccion::create([
                            'calle'        => $request->input('direccion.calle', ''),
                            'no_exterior'  => $request->input('direccion.no_exterior', ''),
                            'no_interior'  => $request->input('direccion.no_interior', ''),
                            'colonia'      => $request->input('direccion.colonia', ''),
                            'cp'           => $request->input('direccion.cp', ''),
                            'estado_id'    => $request->input('direccion.estado_id', ''),
                            'municipio_id' => $request->input('direccion.municipio_id', ''),
                            'referencia'   => $request->input('direccion.referencia', '')
                        ]);

                        $pedido->direccion()->associate($direccion);
                        $pedido->save();

                        // Eliminamos la información del envio anterior
                        $envio = $pedido->envio;
                        if($envio != null) $envio->delete();

                        // Creamos el registro en la tabla de envios
                        $pedido->envio()->create([
                            'pedido_id' => $pedido->id,
                            'status' => 'PROCESANDO',
                            'costo' => $pedido->colegio->config('costo_envio')
                        ]);
                }else {
                    $pedido->direccion()->associate(Direccion::find($request->id_direccion_envio));
                }
                $pedido->save();

                // dd("llego");

                //preferencia Mercado Pago

                $array=[];

                Log::debug('MercadoPagoController@getPreference Inicializando Preferencia/Instancia de Mercado Pago');

                MercadoPago\SDK::setAccessToken(config('services.mercadopago.token')); // Either Production or SandBox AccessToken

                $key=config('services.mercadopago.key');
                $preference = new MercadoPago\Preference();

                    $item = new MercadoPago\Item();
                    $item->title = "Venta de libros";
                    $item->quantity = 1;
                    $item->currency_id = "MXN";
                    $item->unit_price = $request->pedido['total'];

                //    $payer = new MercadoPago\Payer();
                //    $payer->email = $request->pedido['correo'];
                // $payer->email = 'test_user_9791251@testuser.com';

                $preference->items = array($item);
                //    $preference->payer = $payer;

                $preference->back_urls = array(
                    "success" => url('/api/responseMp/'.$pedido->id),
                    "failure" => url('/api/responseMp/'.$pedido->id),
                    "pending" => url('/api/responseMp/'.$pedido->id)
                );

                $preference->auto_return="approved"; //movil


                $costo_envio=$pedido->colegio->config('costo_envio');


                if ($request->tipo_entrega == 2) {


                $shipments = new MercadoPago\Shipments();
                $shipments->mode="not_specified";
                $shipments->cost= floatval($costo_envio);

                // dd($shipments);

                $preference->shipments=$shipments;


                    // $preference->shipments=array(
                    //     "cost" => $costo_envio,
                    //     "mode" => "not_specified",
                    //   );


                }


                //   ejemplño repsuesta pago

                //   {
                //     "id": 2798247250,
                //     "date_created": "2017-06-16T21:10:06.000-04:00",
                //     "date_approved": "2017-06-16T21:10:06.000-04:00",
                //     "date_last_updated": "2017-06-28T19:39:41.000-04:00",
                //     "date_of_expiration": null,
                //     "money_release_date": "2017-06-21T21:10:06.000-04:00",
                //     "operation_type": "regular_payment",
                //     "payment_method_id": "visa",
                //     "payment_type_id": "credit_card",
                //     "status": "approved",
                //     "status_detail": "accredited",
                //     "currency_id": "MXN",
                //     "description": "Telefono Celular iPhone 7",
                //     ...,
                //   }



                $referencia=$pedido->serie."-".$pedido->folio;
                $preference->external_reference=$referencia;
                // $preference->external_reference=$pedido->id;

                //ruta noficiaciones webhook / SE PUED ECONFIGURAR AQUI O EN LA PAGINA PERO NO LAS DOS AL MISMO TIEMPO SI NO MP PRIORIZA NOTIFICATION_URL Y SOLAMENTE LO MANDA 1 VZ
                //  $preference->notification_url=url('/api/webhook');
                // $preference->notification_url="https://demo.booktrain.com.mx/api/webhook";

                $preference->save();

                // dd($preference);


                $array[0]=$preference->id;
                $array[1]=$key;

                DB::commit();

                return $array;

            } catch (Exception $e) {

                DB::rollBack();

                $array=[
                    'success' => false,
                    'ERROR on the transaction Exception: ' => $e->getMessage(),
                ];

                Log::error('MercadoPagoController@getPreference ERROR on the transaction Exception: ',$array);

                return $this->sendError($array, 503);

            }

    }


            public function responseMp(Request $request){

                    // dd($request->status);


                     Log::debug('MercadoPagoAPIController@responseMp: Datos de solicitud', $request->all());


                     DB::beginTransaction();

                     try {

                        $pedido_id=$request->route('id');

                        $pedido=Pedido::find($pedido_id);

                        if( ! $pedido) {
                            Log::error('MercadoPagoAPIController@responseMp: Error al obtener el pedido');

                            return view('errors.404');
                        }



                        Log::debug('MercadoPagoAPIController@responseMp: Resvisamos estatus y acctualizamos pedido '.$pedido->id );
                        $status=$request->status;



                           //buscamos el pago
                $pago=RegistroPago::where('pedido_id',$pedido->id)->first();


                if(empty($pago)) {
                    Log::error('MercadoPagoAPIController@responseMp: Error al obtener el pago, se genera uno');

                        //generamos el pago

                        $pago= new RegistroPago();
                        $pago->pedido_id=$pedido->id;
                        $pago->amount=$pedido->total;
                        $pago->origen="API";
                        $pago->source="MERCADO PAGO";
                        $pago->transactionTokenId=$request->payment_id;
                        $pago->raw=$request;
                        $pago->status="IN_PROCESS";
                        $pago->save();

                }


                        $pago->refresh();



                        switch($status) {
                            case 'approved':
                                $pago->update(['status' => 'SUCCESS']);
                                $pedido->update([ 'status' => 'PAGADO' ]);

                                $pedido->procesar();

                                // Notificamos al usuario que el pago fue exitoso
                                $pedido->sendPagoExitosoNotification();

                                DB::commit();

                                return redirect('/resultadomercadopago/success');

                                // return view('private.user.pedidos');
                                // return view('private.resultado3ds')->with(['success' => true, 'message' => 'Pago aprobado por la entidad bancaria']);

                            case 'in_process':
                            case 'pending':
                                $pago->update(['status' => 'IN_PROCESS']);

                                DB::commit();

                                return redirect('/resultadomercadopago/in-process');
                                // return redirect('/resultadomercadopago')->with(['success' => false, 'message' => 'El pago aun no ha sido procesado, consulte más tarde']);
                                // return view('private.user.pedidos');
                                // return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago aun no ha sido procesado, consulte más tarde']);


                            case 'rejected':
                                $pago->update(['status' => 'FAILED']);
                                $pedido->update([ 'status' => 'PAGO RECHAZADO' ]);

                                DB::commit();

                                return redirect('/resultadomercadopago/false');

                                // return redirect('/resultadomercadopago')->with(['success' => false, 'message' => 'El pago ha sido rechazado']);
                                // return view('private.user.pedidos');
                                // return view('private.resultado3ds')->with(['success' => false, 'message' => 'El pago ha sido rechazado']);
                        }


                     } catch (Exception $e) {

                         DB::rollBack();

                         $array=[
                             'success' => false,
                             'ERROR on the transaction Exception: ' => $e->getMessage(),
                         ];

                         Log::error('MercadoPagoController@responseMp ERROR on the transaction Exception: ',$array);

                         return $this->sendError($array, 503);

                     }


        }

        public function webhook(Request $request){

            Log::debug('MercadoPagoAPIController@webhook: Datos de solicitud', $request->all());


            // dd($request->all());

            //ejemplo Request webhook

            // {
            //     "id": 12345,
            //     "live_mode": true,
            //     "type": "payment",
            //     "date_created": "2015-03-25T10:04:58.396-04:00",
            //     "application_id": 123123123,
            //     "user_id": 44444,
            //     "version": 1,
            //     "api_version": "v1",
            //     "action": "payment.created",
            //     "data": {
            //         "id": "999999999"
            //     }
            //   }


            DB::beginTransaction();

            try {


                Log::debug('MercadoPagoAPIController@webhook: verificamos tipo de peticion live:'.$request->live_mode.' action: '.$request->action);

                // //solamente es para el test del webhook en la pagina
                // if(!$request->live_mode || $request->action=="payment.created"){      Log::debug('MercadoPagoAPIController@webhook: live mode false o action en payment.created');  return response()->json(['success' => true,], 200);  }


                Log::debug('MercadoPagoAPIController@webhook: asignamos valor del pago en la peticion');

                $payment_id=null;

                if(!empty($request->data)){  Log::debug('MercadoPagoAPIController@webhook:external id desde data/id');  $payment_id=$request->data['id']; }

                if(empty($payment_id)){   Log::debug('MercadoPagoAPIController@webhook:external id desde data_id'); $payment_id=$request->data_id;   }

                //  dd($payment_id);

                Log::debug('MercadoPagoAPIController@webhook: Enviamos solicitud para obtener datos dle pago '."https://api.mercadopago.com/v1/payments/$payment_id"."?access_token=".config('services.mercadopago.token'));

                $reponse= Http::get("https://api.mercadopago.com/v1/payments/$payment_id"."?access_token=".config('services.mercadopago.token'));

                // dd($reponse);

                $reponse=json_decode($reponse);

                $status=$reponse->status;
                // $pedido_id=$reponse->external_reference;

                $datos_pedido=explode("-",$reponse->external_reference);
                $cuantos=count($datos_pedido);

                if($cuantos>1){

                    $folio_pedido=$datos_pedido[1];
                    $pedido=Pedido::where('folio',$folio_pedido)->first();
                    $numero=$folio_pedido;

                }else{

                     $pedido_id=$reponse->external_reference;
                     $pedido=Pedido::find($pedido_id);
                     $numero=$pedido_id;

                }

                if($pedido->status=='PAGADO'){

                Log::debug('MercadoPagoAPIController@webhook: Pedido ya pagado, no se realizan acciones');


                    return response()->json(['success' => true,], 200);


                }//ya estando pagado el pedido ya no debe hacer nada el webhook


                Log::debug('MercadoPagoAPIController@webhook: external reference'.$reponse->external_reference);



                if( ! $pedido) {
                    Log::error('MercadoPagoAPIController@responseMp: Error al obtener el pedido');

                    return response()->json(['success' => false,], 404);
                }


                Log::debug('MercadoPagoAPIController@webhook: buscamos el pago');

                //buscamos el pago
                $pago=RegistroPago::where('pedido_id',$pedido->id)->first();


                if(empty($pago)) {
                    Log::error('MercadoPagoAPIController@webhook: Error al obtener el pago, se genera uno');

                    // dd('no hay pago');


                    $pago= new RegistroPago();
                    $pago->pedido_id=$pedido->id;
                    $pago->amount=$pedido->total;
                    $pago->origen="API";
                    $pago->source="MERCADO PAGO";
                    $pago->transactionTokenId=$payment_id;
                    $pago->raw=$request;
                    $pago->status="IN_PROCESS";
                    $pago->save();

                }




                Log::debug('MercadoPagoAPIController@webhook: actualizamos el estatus del pedido '.$numero. " con estatus ". $status);

                // dd($status);

                switch($status) {
                    case 'approved':
                        $pago->update(['status' => 'SUCCESS']);
                        $pedido->update([ 'status' => 'PAGADO' ]);

                        $pedido->procesar();

                        // Notificamos al usuario que el pago fue exitoso
                        $pedido->sendPagoExitosoNotification();

                        DB::commit();

                        return response()->json(['success' => true,], 200);

                        break;


                    case 'in_process':
                    case 'pending':
                        $pago->update(['status' => 'IN_PROCESS']);

                        DB::commit();

                        return response()->json(['success' => true,], 200);

                        break;

                    case 'rejected':
                    case 'cancelled':
                        $pago->update(['status' => 'FAILED']);
                        $pedido->update([ 'status' => 'PAGO RECHAZADO' ]);

                        DB::commit();

                        return response()->json(['success' => true,], 200);


                        break;

                }

                // return response()->json(['success' => true,], 200);



            } catch (Exception $e) {

                DB::rollBack();

                $array=[
                    'success' => false,
                    'ERROR on the transaction Exception: ' => $e->getMessage(),
                ];

                Log::error('MercadoPagoController@webhook ERROR on the transaction Exception: ',$array);

                return $this->sendError($array, 503);

            }

         }

}
