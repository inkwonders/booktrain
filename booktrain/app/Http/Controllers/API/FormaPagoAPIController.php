<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\FormaPago;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Collection;
use App\Repositories\FormaPagoRepository;
use App\Transformers\FormaPagoTransformer;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateFormaPagoAPIRequest;
use App\Http\Requests\API\UpdateFormaPagoAPIRequest;

/**
 * Class FormaPagoController
 * @package App\Http\Controllers\API
 */

class FormaPagoAPIController extends AppBaseController
{
    /** @var  FormaPagoRepository */
    private $formaPagoRepository;

    public function __construct(FormaPagoRepository $formaPagoRepo)
    {
        $this->formaPagoRepository = $formaPagoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/formaPagos",
     *      summary="Get a listing of the FormaPagos.",
     *      tags={"FormaPago"},
     *      description="Get all FormaPagos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/FormaPago")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $formaPagos = $this->formaPagoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($formaPagos->toArray(), 'Forma Pagos retrieved successfully');
    }

    /**
     * @param CreateFormaPagoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/formaPagos",
     *      summary="Store a newly created FormaPago in storage",
     *      tags={"FormaPago"},
     *      description="Store FormaPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FormaPago that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FormaPago")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FormaPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFormaPagoAPIRequest $request)
    {
        $input = $request->all();

        $formaPago = $this->formaPagoRepository->create($input);

        return $this->sendResponse($formaPago->toArray(), 'Forma Pago saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/formaPagos/{id}",
     *      summary="Display the specified FormaPago",
     *      tags={"FormaPago"},
     *      description="Get FormaPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FormaPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FormaPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var FormaPago $formaPago */
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            return $this->sendError('Forma Pago not found');
        }

        return $this->sendResponse($formaPago->toArray(), 'Forma Pago retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFormaPagoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/formaPagos/{id}",
     *      summary="Update the specified FormaPago in storage",
     *      tags={"FormaPago"},
     *      description="Update FormaPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FormaPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FormaPago that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FormaPago")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FormaPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFormaPagoAPIRequest $request)
    {
        $input = $request->all();

        /** @var FormaPago $formaPago */
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            return $this->sendError('Forma Pago not found');
        }

        $formaPago = $this->formaPagoRepository->update($input, $id);

        return $this->sendResponse($formaPago->toArray(), 'FormaPago updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/formaPagos/{id}",
     *      summary="Remove the specified FormaPago from storage",
     *      tags={"FormaPago"},
     *      description="Delete FormaPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FormaPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var FormaPago $formaPago */
        $formaPago = $this->formaPagoRepository->find($id);

        if (empty($formaPago)) {
            return $this->sendError('Forma Pago not found');
        }

        $formaPago->delete();

        return $this->sendSuccess('Forma Pago deleted successfully');
    }

    public function formas()
    {
        $formasPago = FormaPago::orderBy('orden')->get();

        $fractal = new Manager();

        $datosFormasPago = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($formasPago, new FormaPagoTransformer))->toArray();

        return $this->sendSuccess($datosFormasPago, "Información obtenida");
    }
}
