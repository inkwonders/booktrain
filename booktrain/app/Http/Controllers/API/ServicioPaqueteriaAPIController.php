<?php

namespace App\Http\Controllers\API;

use App\Models\ServicioPaqueteria;
use League\Fractal\Manager;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use App\Transformers\ServicioPaqueteriaTransformer;
use League\Fractal\Resource\Collection;
use App\Http\Controllers\AppBaseController;


/**
 * Class UsuarioAPIController
 * @package App\Http\Controllers\API
 */

class ServicioPaqueteriaAPIController extends AppBaseController
{
    function index()
    {
        $servicios_paqueteria = ServicioPaqueteria::where('activo', '1')->get();;

        $fractal = new Manager();

        $servicios_paqueteria = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($servicios_paqueteria, new ServicioPaqueteriaTransformer))->toArray();

        return $this->sendResponse($servicios_paqueteria, "Información obtenida");
    }

}
