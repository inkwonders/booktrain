<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\Cupon;
use App\Models\Libro;
use App\Models\Pedido;
use App\Models\Colegio;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DetalleEnviosPedidos;
use App\Models\TipoMovimientoAlmacen;
use App\Repositories\PedidoRepository;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Validation\Validator;
// use App\Http\Requests\CreatePedidoRequest;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreatePedidoAPIRequest;
use App\Http\Requests\API\UpdatePedidoAPIRequest;
use App\Transformers\ReImpresionBBVA10Transformer;
use App\Transformers\ReImpresionTiendaConvenienciaTransformer;

/**
 * Class PedidoController
 * @package App\Http\Controllers\API
 */

class PedidoAPIController extends AppBaseController
{
    /** @var  PedidoRepository  */
    private $pedidoRepository;

    public function __construct(PedidoRepository $pedidoRepo)
    {
        $this->pedidoRepository = $pedidoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pedidos",
     *      summary="Get a listing of the Pedidos.",
     *      tags={"Pedido"},
     *      description="Get all Pedidos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Pedido")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $pedidos = $this->pedidoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($pedidos->toArray(), 'Pedidos retrieved successfully');
    }

    /**
     * @param CreatePedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pedidos",
     *      summary="Store a newly created Pedido in storage",
     *      tags={"Pedido"},
     *      description="Store Pedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pedido that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePedidoAPIRequest $request)
    {
        $input = $request->all();

        $pedido = $this->pedidoRepository->create($input);

        return $this->sendResponse($pedido->toArray(), 'Pedido saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pedidos/{id}",
     *      summary="Display the specified Pedido",
     *      tags={"Pedido"},
     *      description="Get Pedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        return $this->sendResponse($pedido->toArray(), 'Pedido retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pedidos/{id}",
     *      summary="Update the specified Pedido in storage",
     *      tags={"Pedido"},
     *      description="Update Pedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pedido that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePedidoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        $pedido = $this->pedidoRepository->update($input, $id);

        return $this->sendResponse($pedido->toArray(), 'Pedido updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pedidos/{id}",
     *      summary="Remove the specified Pedido from storage",
     *      tags={"Pedido"},
     *      description="Delete Pedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Pedido $pedido */
        $pedido = $this->pedidoRepository->find($id);

        if (empty($pedido)) {
            return $this->sendError('Pedido not found');
        }

        $pedido->delete();

        return $this->sendSuccess('Pedido deleted successfully');
    }

    public function getInformacionFormato (Request $request, $pedido_id) {
        $usuario = Auth::user();

        $pedido = $usuario->pedidos->where('id', $pedido_id)->first();

        if (is_null($pedido)) {
            return $this->sendError('No se encontro el pedido');
        }

        $referencia = $pedido->referencia;

        if (is_null($referencia)) {
            return $this->sendError('No se encontro la referencia para el pedido ' . $pedido->id);
        }

        $fractal = new Manager();

        //referencia BBVA
        if ($referencia->tipo == 'BANCARIA') {
            $informacion_pedido = $fractal->setSerializer(new Serializer())->createData(new Item($pedido, new ReImpresionBBVA10Transformer))->toArray();
        //referencia tienda de conveniencia
        } else if ($referencia->tipo == 'TIENDA') {
            $informacion_pedido = $fractal->setSerializer(new Serializer())->createData(new Item($pedido, new ReImpresionTiendaConvenienciaTransformer))->toArray();
        } else {
            return $this->sendError('No se encontro un formato para el tipo de referencia ' . $referencia->tipo);
        }

        return $this->sendResponse($informacion_pedido, 'Información enviada correctamente');
    }

    public function exportarEnviosXLS (Request $request) {
        $inicio = $request->inicio;
        $fin = $request->fin;

        return Excel::download(new DetalleEnviosPedidos( $inicio , $fin), 'detallesEnviosAdmin.xlsx');
    }

    /**
     * @api{get} /api/venta_movil/carrito Obtener carrito
     * @apiGroup VentaMovil
     * @apiDescription Obtiene el contenido actual del carrito de compras
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "Carrito obtenido correctamente con xxxxx elementos",
     *     "data": {
     *       "id": 12345,
     *       "total": 1000,
     *       "resumenes": [
     *           {
     *              "id": 123,
     *              "nombre": "Alumno",
     *              "apellido_paterno": "ApellidoPaterno,
     *              "apellido_materno": "ApellidoMaterno",
     *              "detalles": [
     *                  {
     *                     "id": 456,
     *                     "libro_id": 15225,
     *                     "libro": "Enciclopedia de lo absurdo",
     *                     "isbn": "313115615153153",
     *                     "cantidad": 2,
     *                     "precio": 150.25,
     *                     "total": 300.5,
     *                  }
     *              ],
     *           }
     *        ]
     *     }
     * }
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     * @apiSuccess (200) {Object} data Contenido del carrito
     *
     * @apiErrorExample {json} Error-Response: 404
     * HTTP/1.1 404 Bad request
     * {
     *   "success": false,
     *   "message": "El pedido indicado no existe"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function obtenerCarrito() {
        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        DB::beginTransaction();

        try {
            $pedido = $usuario->ventas()->firstOrCreate([
                'usuario_venta_id' => $usuario->id,
                'colegio_id' => $colegio->id,
                'status' => 'CARRITO'
            ]);

            $caja->pedidos()->syncWithoutDetaching( $pedido->id );

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('PedidoAPIController@obtenerCarrito Error al crear el pedido', [$th]);

            return $this->sendError("Ocurrió un error {$th->getCode()} al obtener el carrito", 503);
        }

        return $this->sendResponse([
            'id'                   => $pedido->id,
            'usuario_venta_id'     => $pedido->usuario_venta_id,
            'nombre_usuario_venta' => $pedido->usuarioVenta->nombre_completo,
            'total'                => $pedido->total,
            'resumenes'            => $pedido->resumenes->map(function($resumen) {
                return (Object) [
                    'id'               => $resumen->id,
                    'nombre'           => $resumen->nombre_alumno,
                    'apellido_paterno' => $resumen->paterno_alumno,
                    'apellido_materno' => $resumen->materno_alumno,
                    'colegio'          => $resumen->paquete->nivel->seccion->colegio->nombre,
                    'seccionEducativa' => $resumen->paquete->nivel->seccion->nombre,
                    'grado'            => $resumen->paquete->nivel->nombre,
                    'detalles'         => $resumen->detalles->map(function($detalle) {
                        return (Object) [
                            'id'       => $detalle->id,
                            'libro_id' => $detalle->libro_id,
                            'libro'    => $detalle->libro->nombre,
                            'isbn'     => $detalle->libro->isbn,
                            'cantidad' => $detalle->cantidad,
                            'precio'   => $detalle->precio_libro,
                            'total'    => $detalle->total,
                        ];
                    })
                ];
            })
        ], "Carrito obtenido correctamente con {$pedido->detalles->count()} elementos");
    }

    // public function storeCarrito(CreatePedidoRequest $request)
    // {
    //     $usuario = Auth::user();
    //     $carrito = Pedido::where([
    //         ['status', '=', 'CARRITO'],
    //         ['usuario_id', '=', $usuario->id]
    //     ])->first();

    //     /** Se crea el pedido si no existe ninguno en carrito */
    //     if (empty($carrito)){
    //         $carrito = new Pedido;
    //     } else {
    //         if ($request->colegio_id != $carrito->colegio_id)
    //             return $this->sendError('Carrito existente de otro colegio', 403);
    //     }

    //     $carrito->colegio_id = $request->colegio_id;
    //     $carrito->status = 'CARRITO';
    //     $carrito->usuario_id = $usuario->id;
    //     if( $usuario->roles->pluck('name')->contains('Venta Movil') )
    //         $carrito->usuario_venta_id = $usuario->id;

    //     DB::beginTransaction();

    //     try {
    //         $carrito->touch();
    //         $carrito->save();

    //         /** Se agrega el resumen del pedido */
    //         $resumen = $carrito->resumenes()->create([
    //             'paquete_id' => $request->paquete_id
    //         ]);

    //         /** Se recorren los N libros que traiga para agregralos al detalle del resumen */
    //         foreach ($request->libros as $libro) {
    //             $resumen->detalles()->create([
    //                 'libro_id' => $libro['id'],
    //                 'precio_libro' => $libro['precio']
    //             ]);
    //         }
    //     } catch (\Throwable $th) {
    //         DB::rollBack();
    //         Log::error('PedidoController@storeCarrito', [
    //             'error' => $th->getMessage(),
    //             'linea' => $th->getLine(),
    //             'request' => $request->all()
    //         ]);
    //         // Log::debug($th);
    //         return $this->sendError($th->getMessage(), 400);
    //     }

    //     DB::commit();

    //     return response()->json([
    //         'success' => true,
    //         'data' => $carrito->id
    //     ], 200);
    // }

    /**
     * @api{post} /api/venta_movil/pagar Pagar pedido
     * @apiGroup VentaMovil
     * @apiDescription Añade un la configuración de pago y el contenido de la orden al pedido para procesarlo
     *
     * @apiBody {String} metodo_pago_id Método utilizado para recibir el dinero del pago, se asume la forma de pago PUE = 1 en todos los casos
     * @apiBody {String} [referencia] Valor para identificar la transferencia o el pago en terminal de tarjetas
     * @apiBody {String} [cupon_descuento] En caso de haber sido utilizado, se adjunta el código del cupón de descuento
     * @apiBody {String} correo_contacto Correo para informar sobre el pago exitoso
     * @apiBody {String} telefono_contacto Teléfono para contactar si hubiera algún problema con el pedido
     * @apiBody {String} nombre_contacto Nombre del contacto en caso de aclaración
     * @apiBody {String} apellidos_contacto Apellidos del contacto en caso de aclaración
     * @apiBody {Array} contenido Arreglo con los elementos (Resumenes) que van a ir en el paquete
     * @apiBody {Number} contenido.paquete_id Id del paquete de libros correspondiente a los libros
     * @apiBody {String} contenido.nombre_alumno Nombre del alumno para generar el pedido
     * @apiBody {String} contenido.apellido_paterno_alumno Apellido paterno del alumno para generar el pedido
     * @apiBody {String} contenido.apellido_materno_alumno Apellido materno del alumno para generar el pedido
     * @apiBody {Array} contenido.libros Arreglo con los IDs de los libros a agregar al carrito
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "metodo_pago_id": 8,
     *   "referencia": "AA11BB22CC33DD44",
     *   "nombre_contacto": "Juan Pablo",
     *   "apellidos_contacto": "Pérez",
     *   "correo_contacto": "papadefamilia@gmail.com",
     *   "telefono_contacto": "552361235",
     *   "cupon_descuento": "ABC123DEF45",
     *   "contenido": [
     *     {
     *       "nombre_alumno": "Juan Pablo",
     *       "apellido_paterno_alumno": "Pérez",
     *       "apellido_materno_alumno": "Jímenez",
     *       "paquete_id": 12345,
     *       "libros": [24685, 25486, 25568, 48456]
     *     }
     *   ]
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "Se agregaron 4 objetos al pedido con el id 25645"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 403
     * HTTP/1.1 403 Bad request
     * {
     *   "success": false,
     *   "message": "El paquete indicado no pertenece al colegio asignado al vendedor"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function pagarPedido(Request $request) {
        Log::debug('PedidoAPIController@pagarPedido', ['request' => $request->all()]);

        $validator = Validator::make(
            $request->all(),
            [
                'metodo_pago_id'                      => 'required|exists:metodos_pagos,id',
                'referencia'                          => 'sometimes',
                'cupon_descuento'                     => 'sometimes',
                'fecha_descuento'                     => 'sometimes',
                'telefono_contacto'                   => 'required',
                'correo_contacto'                     => 'required|email',
                'email_licencias'                     => 'nullable|email',
                'nombre_contacto'                     => 'required',
                'apellidos_contacto'                  => 'required',
                'contenido.*.nombre_alumno'           => 'required',
                'contenido.*.apellido_paterno_alumno' => 'required',
                'contenido.*.apellido_materno_alumno' => 'sometimes',
                'contenido.*.paquete_id'              => 'required|exists:paquetes,id',
                'contenido.*.libros'                  => 'required',
            ]
        );

        if($validator->fails())
            return $this->sendError($validator->errors()->first(), 400);

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        if( ! $metodo_pago = $colegio->metodosPago()->where('id', $request->metodo_pago_id)->wherePivot('contexto', 'PRESENCIAL')->first())
            return $this->sendError('El método de pago indicado no existe', 404);

        $forma_pago = $colegio->formasPago()->where('metodo_pago_id', $metodo_pago->id)->where('forma_pago_id', 1)->first();

        $conteo_libros = 0;

        $carrito = Pedido::where([
            ['status', '=', 'CARRITO'],
            ['usuario_id', '=', $usuario->id]
        ])->first();

        /** Se crea el pedido si no existe ninguno en carrito */
        if (empty($carrito)){
            $carrito = new Pedido;
        } else {
            return $this->sendError('Carrito abierto, para realizar esta operación de \'Booktrain Offline\' es necesario que el carrito en \'Online\' esté vacio', 409);
        }

        $carrito->colegio_id = $colegio->id;
        $carrito->status = 'CARRITO';
        $carrito->usuario_id = $usuario->id;
        if( $usuario->roles->pluck('name')->contains('Venta Movil') )
            $carrito->usuario_venta_id = $usuario->id;

        DB::beginTransaction();

        try {
            $carrito->touch();
            $carrito->save();
            // dd('ok');
            $pedido = $usuario->ventas()->firstOrCreate([
                'usuario_venta_id' => $usuario->id,
                'colegio_id' => $colegio->id,
                'status' => 'CARRITO'
            ]);

            // Asociamos el pedido a la caja sin duplicar entradas en la tabla 'pedidos_cajas'
            $caja->pedidos()->syncWithoutDetaching( $pedido->id );

            //--------------------------------------------------------------
            $cupon = null;
            $monto_cupon = 0;

            // dd('ok');
            // Canjeamos el cupon ( Asociarlo al pedido )
            if( $request->has('cupon_descuento') && !empty($request->cupon_descuento) ) {
                // dd(empty($request->cupon_descuento));
                // dd(strlen($request->cupon_descuento));
                $cupon = Cupon::where('codigo', $request->cupon_descuento)->first();
                $cupon->pedido()->associate($pedido);
                $cupon->save();

                if($cupon->tipo == 'MONTO')
                    $monto_cupon = $cupon->valor;
            }
            
            // Registramos los resumenes
            foreach($request->contenido as $resumen_request) {
                $resumen = $pedido->resumenes()->create([
                    'paquete_id'     => $resumen_request['paquete_id'],
                    'nombre_alumno'  => $resumen_request['nombre_alumno'],
                    'paterno_alumno' => $resumen_request['apellido_paterno_alumno'],
                    'materno_alumno' => $resumen_request['apellido_materno_alumno'],
                ]);

                $libros_request = Libro::whereIn('id', $resumen_request['libros'])->with(['paquetes' => function($query) use ($resumen_request) {
                    $query->where('paquete_id', $resumen_request['paquete_id']);
                }])->get();

                if($libros_request->count() == 0)
                    Log::error('PedidoAPIController@pagarPedido Se indicaron libros en el pedido que no corresponden al paquete',
                        [
                            'paquete_id' => $resumen_request['paquete_id'],
                            'libros'     => $resumen_request['libros']
                        ]
                    );

                // Registramos los detalles
                foreach($libros_request as $libro_request) {
                    // dd($libro_request->paquetes->first()->pivot->precio);
                    $precio_libro = $libro_request->paquetes->first()->pivot->precio;

                    $conteo_libros ++;

                    if($cupon != null) {
                        // Porcentaje sobre el precio del libro
                        if($cupon->tipo == 'PORCENTAJE')
                            $precio_libro = $precio_libro - ($precio_libro * ( $cupon->valor / 100 ));

                        // Parte proporcional a cada libro de cada entrada del pedido
                        if($cupon->tipo == 'MONTO')
                            $precio_libro = $precio_libro - ($monto_cupon / sizeof($request->contenido) / sizeof($resumen_request['libros']));
                    }

                    $resumen->detalles()->create([
                        'libro_id'      => $libro_request->id,
                        'precio_libro'  => $precio_libro,
                        'cantidad'      => 1,
                        'fecha_entrega' => now(),
                    ]);
                }
            }

            // Registramos el pago al pedido
            $pedido->pagos()->create([
                'amount'             => $pedido->total,
                'status'             => 'SUCCESS',
                'raw'                => json_encode( $request->all() ),
                'referencia'         => $request->referencia,
                'origen'             => 'VENTA MOVIL',
                'source'             => $metodo_pago->descripcion,
                'transactionTokenId' => $metodo_pago->codigo
            ]);

            // Cambiar estado del pedido a pagado
            $pedido->update([
                'status'               => 'PAGADO',
                'metodo_pago_id'       => $request->metodo_pago_id,
                'forma_pago_id'        => 1,
                'metodo_pago'          => $metodo_pago->codigo,
                'forma_pago'           => $forma_pago->codigo,
                'nombre_contacto'      => $request->nombre_contacto,
                'apellidos_contacto'   => $request->apellidos_contacto,
                'celular_contacto'     => $request->telefono_contacto,
                'email_pedido'         => $request->correo_contacto,
                'correo_contacto'      => $request->email_licencias,
                'terminos_condiciones' => 1, // Se asume 1 por que es venta en físico
                'tipo_entrega'         => 1, // Colegio por que la venta se hará en los colegios
            ]);

            // Procesamos el pedido ( Descontar unidades del stock )
            $pedido->procesar(false);

            DB::commit();

            // Enviamos el correo de notificación al correo de contacto
            $pedido->sendPagoExitosoVentaMovilNotification();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('PedidoAPIController@pagarPedido Error al crear el pedido', [$th]);

            return $this->sendError("Ocurrió un error {$th->getCode()} al crear el pedido", 503);
        }

        // Enviamos una respuesta exitosa
        return $this->sendResponse([
            'id'            => $pedido->id,
            'id_registro'   => $request->id,
            'serie'         => $pedido->serie,
            'folio'         => $pedido->folio,
            'total'         => $pedido->total,
        ],  "Se agregaron {$conteo_libros} al pedido con el id {$pedido->id} serie {$pedido->serie}{$pedido->folio}");
    }

    /**
     * @api{post} /api/venta_movil/carrito/quitar_elemento Quitar elemento
     * @apiGroup VentaMovil
     * @apiDescription Quita un producto del carrito, pudiendo ser un detalle o un resumen
     *
     * @apiBody {Number} tipo Tipo de elemento 1: DetalleResumenPedido, 2: ResumenPedido
     * @apiBody {Number} id Id del elemento a quitar
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "tipo" : 2,
     *   "id" : 13325
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "Objeto con el $id de tipo $tipo quitado correctamente"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function quitarElementoCarrito(Request $request) {
        Log::debug('PedidoAPIController@quitarElementoCarrito', ['request' => $request->all()]);

        $rules = [
            'tipo' => 'required',
            'id'   => 'required'
        ];

        $validator = Validator::create(
            $request->all(),
            $rules
        );

        if($validator->fails())
            return $this->sendError($validator->errors()->first(), 400);

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        DB::beginTransaction();
        try {
            $pedido = $usuario->ventas()
                ->where('usuario_venta_id', $usuario->id)
                ->where('colegio_id', $colegio->id)
                ->where('status', 'CARRITO')
                ->get();

            if($pedido->count() > 1)
                throw new \Exception("Existen inconsistencias en el carrito del vendedor, contacte con el administrador", 1);

            $pedido = $pedido->first();

            if($request->tipo == 1)
                $pedido->detalles()->where('id', $request->id)->delete();

            if($request->tipo == 2)
                $pedido->resumenes()->where('id', $request->id)->delete();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('PedidoAPIController@quitarElementoCarrito Error al quitar el elemento', [$th]);

            return $this->sendError("Ocurrió un error {$th->getCode()} al quitar el elemento", 503);
        }

        return $this->sendSuccess("Objeto con el {$request->id} de tipo {$request->tipo} quitado correctamente");
    }

    /**
     * @api{get} /api/venta_movil/carrito/vaciar Vaciar carrito
     * @apiGroup VentaMovil
     * @apiDescription Elimina el contenido del carrito pero no el elemento Pedido asociado a él
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "El carrito ha sido vaciado"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function vaciarCarrito() {
        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        DB::beginTransaction();
        try {
            $pedido = $usuario->ventas()
                ->where('usuario_venta_id', $usuario->id)
                ->where('colegio_id', $colegio->id)
                ->where('status', 'CARRITO')
                ->get();

            if($pedido->count() > 1)
                throw new \Exception("Existen inconsistencias en el carrito del vendedor, contacte con el administrador", 1);

            $pedido = $pedido->first();

            $pedido->detalles()->delete();
            $pedido->resumenes()->delete();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('PedidoAPIController@vaciarCarrito Error al vaciar el carrito', [$th]);

            return $this->sendError("Ocurrió un error {$th->getCode()} al vaciar el carrito", 503);
        }

        return $this->sendSuccess("El carrito ha sido vaciado");
    }


    /**
     * @api{post} /api/venta_movil/devolver Devolver mercancía
     * @apiGroup VentaMovil
     * @apiDescription Permite hacer una devolución de mecancía SOLO SI fue vendida EN LA CAJA ABIERTA asociada al usuario
     *
     * @apiBody {Number} pedido_id Id del pedido a pagar
     * @apiBody {Number} detalle_id Id del detalle_resumen_pedido que se desea devolver
     * @apiBody {String} comentarios Información sobre la razón de devolución
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "pedido_id" : 123,
     *   "detalle_id" : 1655,
     *   "comentarios" : "El libro fue devuelto por que un familiar en otro grado le facilitó el libro al alumno"
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "¡Operación realizada correctamente!"
     * }
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El campo pedido id es obligatorio."
     * }
     *
     * @apiErrorExample {json} Error-Response: 404
     * HTTP/1.1 404 Bad request
     * {
     *   "success": false,
     *   "message": "El pedido indicado no existe"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function devolverMercancia(Request $request) {
        Log::debug('CajaController@devolverMercancia', ['input' => $request->all()]);

        $request->validate([
            'pedido_id'   => 'required|exists:pedidos,id',
            'detalle_id'  => 'required|exists:detalles_resumen_pedidos,id',
            'comentarios' => 'required'
        ]);

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        if( ($pedido = $caja->pedidos()->find($request->pedido_id)) && $pedido->resumenes()->where('devuelto', 0)->count() == 0)
            $this->sendError('El pedido ya ha devuelto todos sus productos', 412);

        if( ! in_array($pedido->status, ['PAGADO', 'ENTREGADO', 'ENTREGA PARCIAL']) )
            $this->sendError('El pedido no puede ser devuelto', 409);

        DB::beginTransaction();

        try {
            $detalle = $pedido->detalles()->where('id', $request->detalle_id)->first();

            // Cambia estado del detalle
            $detalle->update([
                'devuelto' => 1,
                'fecha_devolucion' => now(),
                'razon_devolucion' => 'DEVOLUCION EN VENTA MOVIL',
                'porcentaje_descuento_devolucion' => 1, // 100%
                'descuento_devolucion' => $detalle->precio_libro
            ]);

            // Modifica si es necesario el estado del pedido
            if($pedido->detalles()->where('devuelto', 1)->count() == $pedido->detalles->count() ) {
                // Se ha devuelto todo el contenido del pedido
                $pedido->update([
                    'status' => 'CANCELADO', // Es correcto esto ?
                ]);

                // Marcamos el pedido como cancelado y devolvemos las existencias
                $pedido->cancelar();
            }else{
                // Entrada por devolución
                $tipo_movimiento_almacen = TipoMovimientoAlmacen::where('clave', 'ENT04')->first();

                // Obtenemos el stock actual
                $stock_actual = $colegio
                    ->libros()
                    ->sharedLock()
                    ->wherePivot('paquete_id', null)
                    ->wherePivot('libro_id', $detalle->libro_id)
                    ->first()
                    ->pivot
                    ->stock;

                // Guardamos el nuevo stock
                $colegio->libros()
                    ->wherePivot('paquete_id', null)
                    ->wherePivot('libro_id', $detalle->libro_id)
                    ->lockForUpdate()
                    ->updateExistingPivot($detalle->libro_id, [
                         'stock' => $stock_actual + $detalle->cantidad
                    ]);

                // Crea un registro de entrada de almacén por devolución solo para este elemento
                $colegio->movimientosAlmacen()->create([
                    'tipo_movimiento_almacen' => $tipo_movimiento_almacen->id,
                    'user_id' => $usuario->id,
                    'cantidad' => $detalle->cantidad,
                    'cantidad_anterior' => $stock_actual,
                    'descripcion' => 'Devolución en venta movil',
                    'comentarios' => $request->comentarios
                ]);
            }

            // Registra el movimiento en el log
            activity()
                ->performedOn($pedido)
                ->causedBy($usuario->id)
                ->withProperties(['detalle' => $detalle])
                ->log("Se devolvió mercancia en la venta movil: El libro {$detalle->libro->nombre}");

            DB::commit();
        } catch (\Throwable $th) {
            Log::error('CajaController@devolverMercancia', ['error' => $th->getMessage(), 'linea' => $th->getLine()]);
            DB::rollback();

            $this->sendError("Error al realizar la operación: {$th->getMessage()} @ {$th->getLine()}", 503);
        }

        $this->sendSuccess('¡Operación realizada correctamente!');
    }
}
