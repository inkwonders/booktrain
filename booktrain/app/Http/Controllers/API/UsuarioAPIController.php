<?php

namespace App\Http\Controllers\API;

use App\Models\user;
use App\Models\Pedido;
use League\Fractal\Manager;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;
use App\Http\Controllers\AppBaseController;
use App\Transformers\UltimoPedidoTransformer;

/**
 * Class UsuarioAPIController
 * @package App\Http\Controllers\API
 */

class UsuarioAPIController extends AppBaseController
{
    /**
     * @api{get} /api/user Información del usuario
     * @apiGroup Autenticación
     * @apiDescription Regresa información sobre el usuario
     *
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": {
     *         "id": 7,
     *         "nombre": "Luz",
     *         "apellido": "Aguirre",
     *         "correo": "luz@inkwonders.com",
     *         "alumnos_resumen_pedido": [
     *             {
     *                 "nombre": "Prueba Eddy",
     *                 "apellido_paterno": "Pulido",
     *                 "apellido_materno": null
     *             }
     *         ],
     *         "datos_facturacion_ultimo_pedido": {
     *             "id": 25,
     *             "razon_social": "JUAN MANUEL GARCÍA (PRUEBA)",
     *             "rfc": "GACJ841106CX7",
     *             "correo": "zonademanel@gmail.com",
     *             "cp": 38000,
     *             "usoCFDI": {
     *                 "id": 2,
     *                 "codigo": "G03",
     *                 "descripcion": "Gastos en general"
     *             },
     *             "metodoPago": "PUE",
     *             "formaPago": "01-EFECTIVO"
     *         }
     *     }
     * }
     *
     * @apiErrorExample {json} Error-Response:
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "Credenciales inválidas"
     * }
     */
    function getMiInformacion()
    {
        //return $request->user()->only(['id', 'name', 'nombre_completo', 'email']);
        // Reemplazar con la autenticacion User::auth() .....
        $usuario = Auth::User();

        $fractal = new Manager();

        $mi_informacion = $fractal->setSerializer(new Serializer())
            ->parseIncludes(['alumnos_resumen_pedido', 'direccion_ultimo_pedido', 'datos_facturacion_ultimo_pedido'])
            ->createData(new Item($usuario, new UserTransformer))->toArray();

        return $this->sendSuccess($mi_informacion, "Información obtenida");
    }

    /**
     *
    "ultimo_pedido" : {
        "datos_facturacion" : {
            // ya esta
        },
        "contacto" : {
            "nombre" : 'juan',
            "apellido" : 'perez',
            "telefono" : '1234567890'
        },
            direccion_entrega" : {
            "calle" : '',
            "no_interior" : '',
            "no_exterior" : '',
            "colonia" : '',
            "cp" : '',
            "estado_id" : '',
            "municipio_id" : ''
        }
    }
     */
    public function datosFactura($id)
    {
        $pedido = Pedido::where('usuario_id', $id)->get()->last();

        if (!empty($pedido)) {

            $fractal = new Manager();

            $datos = $fractal->setSerializer(new Serializer())
                ->parseIncludes(['datosFacturacion', 'direccionEntrega'])
                ->createData(new Item($pedido, new UltimoPedidoTransformer))->toArray();

            return $this->sendResponse($datos, "Información obtenida");
        } else {
            return $this->sendError("No se encontro información");;
        }
    }
}
