<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMunicipioAPIRequest;
use App\Http\Requests\API\UpdateMunicipioAPIRequest;
use App\Models\Municipio;
use App\Repositories\MunicipioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MunicipioController
 * @package App\Http\Controllers\API
 */

class MunicipioAPIController extends AppBaseController
{
    /** @var  MunicipioRepository */
    private $municipioRepository;

    public function __construct(MunicipioRepository $municipioRepo)
    {
        $this->municipioRepository = $municipioRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/municipios",
     *      summary="Get a listing of the Municipios.",
     *      tags={"Municipio"},
     *      description="Get all Municipios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Municipio")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $municipios = $this->municipioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($municipios->toArray(), 'Municipios retrieved successfully');
    }

    /**
     * @param CreateMunicipioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/municipios",
     *      summary="Store a newly created Municipio in storage",
     *      tags={"Municipio"},
     *      description="Store Municipio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Municipio that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Municipio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Municipio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMunicipioAPIRequest $request)
    {
        $input = $request->all();

        $municipio = $this->municipioRepository->create($input);

        return $this->sendResponse($municipio->toArray(), 'Municipio saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/municipios/{id}",
     *      summary="Display the specified Municipio",
     *      tags={"Municipio"},
     *      description="Get Municipio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Municipio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Municipio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError('Municipio not found');
        }

        return $this->sendResponse($municipio->toArray(), 'Municipio retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMunicipioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/municipios/{id}",
     *      summary="Update the specified Municipio in storage",
     *      tags={"Municipio"},
     *      description="Update Municipio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Municipio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Municipio that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Municipio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Municipio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMunicipioAPIRequest $request)
    {
        $input = $request->all();

        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError('Municipio not found');
        }

        $municipio = $this->municipioRepository->update($input, $id);

        return $this->sendResponse($municipio->toArray(), 'Municipio updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/municipios/{id}",
     *      summary="Remove the specified Municipio from storage",
     *      tags={"Municipio"},
     *      description="Delete Municipio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Municipio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Municipio $municipio */
        $municipio = $this->municipioRepository->find($id);

        if (empty($municipio)) {
            return $this->sendError('Municipio not found');
        }

        $municipio->delete();

        return $this->sendSuccess('Municipio deleted successfully');
    }
}
