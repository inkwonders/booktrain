<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnvioAPIRequest;
use App\Http\Requests\API\UpdateEnvioAPIRequest;
use App\Models\Envio;
use App\Repositories\EnvioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EnvioController
 * @package App\Http\Controllers\API
 */

class EnvioAPIController extends AppBaseController
{
    /** @var  EnvioRepository */
    private $envioRepository;

    public function __construct(EnvioRepository $envioRepo)
    {
        $this->envioRepository = $envioRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/envios",
     *      summary="Get a listing of the Envios.",
     *      tags={"Envio"},
     *      description="Get all Envios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Envio")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $envios = $this->envioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($envios->toArray(), 'Envios retrieved successfully');
    }

    /**
     * @param CreateEnvioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/envios",
     *      summary="Store a newly created Envio in storage",
     *      tags={"Envio"},
     *      description="Store Envio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Envio that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Envio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Envio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEnvioAPIRequest $request)
    {
        $input = $request->all();

        $envio = $this->envioRepository->create($input);

        return $this->sendResponse($envio->toArray(), 'Envio saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/envios/{id}",
     *      summary="Display the specified Envio",
     *      tags={"Envio"},
     *      description="Get Envio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Envio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Envio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Envio $envio */
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            return $this->sendError('Envio not found');
        }

        return $this->sendResponse($envio->toArray(), 'Envio retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEnvioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/envios/{id}",
     *      summary="Update the specified Envio in storage",
     *      tags={"Envio"},
     *      description="Update Envio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Envio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Envio that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Envio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Envio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEnvioAPIRequest $request)
    {
        $input = $request->all();

        /** @var Envio $envio */
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            return $this->sendError('Envio not found');
        }

        $envio = $this->envioRepository->update($input, $id);

        return $this->sendResponse($envio->toArray(), 'Envio updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/envios/{id}",
     *      summary="Remove the specified Envio from storage",
     *      tags={"Envio"},
     *      description="Delete Envio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Envio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Envio $envio */
        $envio = $this->envioRepository->find($id);

        if (empty($envio)) {
            return $this->sendError('Envio not found');
        }

        $envio->delete();

        return $this->sendSuccess('Envio deleted successfully');
    }
}
