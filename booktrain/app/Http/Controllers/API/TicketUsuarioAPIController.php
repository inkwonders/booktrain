<?php

namespace App\Http\Controllers\API;

use Response;
use Validator;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Models\TicketEntrada;
use App\Models\TicketUsuario;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;
use App\Http\Controllers\AppBaseController;
use App\Repositories\TicketUsuarioRepository;
use App\Transformers\TicketEntradaTransformer;
use App\Transformers\TicketUsuarioTransformer;
use App\Http\Requests\API\CreateTicketUsuarioAPIRequest;
use App\Http\Requests\API\UpdateTicketUsuarioAPIRequest;

/**
 * Class TicketUsuarioController
 * @package App\Http\Controllers\API
 */

class TicketUsuarioAPIController extends AppBaseController
{
    /** @var  TicketUsuarioRepository */
    private $ticketUsuarioRepository;

    public function __construct(TicketUsuarioRepository $ticketUsuarioRepo)
    {
        $this->ticketUsuarioRepository = $ticketUsuarioRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketUsuarios",
     *      summary="Get a listing of the TicketUsuarios.",
     *      tags={"TicketUsuario"},
     *      description="Get all TicketUsuarios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TicketUsuario")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketUsuarios = $this->ticketUsuarioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ticketUsuarios->toArray(), 'Ticket Usuarios retrieved successfully');
    }

    public function postCierraTicket(Request $request, $id)
    {
        $ticket = $this->ticketUsuarioRepository->find($id);

        if (empty($ticket)) {
            return $this->sendError('Ticket Usuario no encontrado');
        }

        DB::beginTransaction();
        try{
            $ticket->status = 0;
            $ticket->touch();
            $ticket->save();
        }catch(\Exception $e) {
            DB::rollback();
            return $this->sendError($e->getMessage());
        }
        DB::commit();

        return $this->sendSuccess('Ticket '. $id . ' cerrado correctamente');
    }

    public function postCalificaTicket(Request $request, $id)
    {
        $TicketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($TicketUsuario)) {
            return $this->sendError('Ticket Usuario no encontrado');
        }
        $reglas = [
            'comentarios'            => 'sometimes|required|string',
             'calificacion'          => 'sometimes|integer',


        ];

        $mensajes = [
            'comentarios.required'    => 'Debe incluir un comentario para la calificación',
            'calificacion.required'   => 'Debe seleccionar una cali',

        ];
        $validator = Validator::make(
            $request->all(),
            $reglas,
            $mensajes
          );

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->first(),'success' => false], 200);
        }
        DB::beginTransaction();
        try{
        $TicketUsuario->calificacion = $request->calificacion;
        $TicketUsuario->comentario_calificacion = $request->comentarios;
        $TicketUsuario->touch();
        $TicketUsuario->save();
            }catch(\Exception $e) {
                DB::rollback();
                return $this->sendError($e->getMessage());
            }
            DB::commit();


        return $this->sendSuccess('Ticket '. $TicketUsuario->id.' calificado correctamente');


    }

    /**
     * @param CreateTicketUsuarioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticketUsuarios",
     *      summary="Store a newly created TicketUsuario in storage",
     *      tags={"TicketUsuario"},
     *      description="Store TicketUsuario",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketUsuario that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketUsuario")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketUsuario"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTicketUsuarioAPIRequest $request)
    {
        $input = $request->all();

        $ticketUsuario = $this->ticketUsuarioRepository->create($input);

        return $this->sendResponse($ticketUsuario->toArray(), 'Ticket Usuario saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketUsuarios/{id}",
     *      summary="Display the specified TicketUsuario",
     *      tags={"TicketUsuario"},
     *      description="Get TicketUsuario",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketUsuario",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketUsuario"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TicketUsuario $ticketUsuario */
        $ticket = $this->ticketUsuarioRepository->find($id);

        if (empty($ticket)) {
            return $this->sendError('Ticket Usuario not found');
        }

        $fractal = new Manager();

        $ticketUsuario = $fractal->setSerializer(new Serializer())
            ->parseIncludes(['usuario'])
            ->createData(new Item($ticket, new TicketUsuarioTransformer))->toArray();


        return $this->sendResponse($ticketUsuario, 'Ticket Usuario retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTicketUsuarioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticketUsuarios/{id}",
     *      summary="Update the specified TicketUsuario in storage",
     *      tags={"TicketUsuario"},
     *      description="Update TicketUsuario",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketUsuario",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketUsuario that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketUsuario")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketUsuario"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTicketUsuarioAPIRequest $request)
    {
        $input = $request->all();

        /** @var TicketUsuario $ticketUsuario */
        $ticketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($ticketUsuario)) {
            return $this->sendError('Ticket Usuario not found');
        }

        $ticketUsuario = $this->ticketUsuarioRepository->update($input, $id);

        return $this->sendResponse($ticketUsuario->toArray(), 'TicketUsuario updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticketUsuarios/{id}",
     *      summary="Remove the specified TicketUsuario from storage",
     *      tags={"TicketUsuario"},
     *      description="Delete TicketUsuario",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketUsuario",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TicketUsuario $ticketUsuario */
        $ticketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($ticketUsuario)) {
            return $this->sendError('Ticket Usuario not found');
        }

        $ticketUsuario->delete();

        return $this->sendSuccess('Ticket Usuario deleted successfully');
    }

    public function entradasTicket($id)
    {
        $entradasTicket = TicketEntrada::where('ticket_usuario_id', $id)->get();

        $fractal = new Manager();

        $entradasInformacion = $fractal->setSerializer(new Serializer())
            ->parseIncludes('usuario')
            ->createData(new Collection($entradasTicket, new TicketEntradaTransformer))->toArray();

        return response()->json(['success' => true, 'data' => $entradasInformacion], 200);
    }

    public function valoresGrafica(){
        $datos = TicketUsuario::select(
            DB::raw('count(*) AS total'),
            DB::raw( 'count(case when calificacion = 10 then 1 else null end) AS positivos'),
            DB::raw( 'count(case when calificacion = 1 then 1 else null end) AS negativos'),
            DB::raw( 'count(case when status != 0 then 1 else null end) AS abiertos'),
            DB::raw( 'count(case when status = 0 then 1 else null end) AS cerrados')
                    )->get()->first();
        return response()->json(['success' => true, 'data' => $datos], 200);
    }
}
