<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLibroAPIRequest;
use App\Http\Requests\API\UpdateLibroAPIRequest;
use App\Models\Libro;
use App\Repositories\LibroRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LibroController
 * @package App\Http\Controllers\API
 */

class LibroAPIController extends AppBaseController
{
    /** @var  LibroRepository */
    private $libroRepository;

    public function __construct(LibroRepository $libroRepo)
    {
        $this->libroRepository = $libroRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/libros",
     *      summary="Get a listing of the Libros.",
     *      tags={"Libro"},
     *      description="Get all Libros",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Libro")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $libros = $this->libroRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($libros->toArray(), 'Libros retrieved successfully');
    }

    /**
     * @param CreateLibroAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/libros",
     *      summary="Store a newly created Libro in storage",
     *      tags={"Libro"},
     *      description="Store Libro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Libro that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Libro")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Libro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateLibroAPIRequest $request)
    {
        $input = $request->all();

        $libro = $this->libroRepository->create($input);

        return $this->sendResponse($libro->toArray(), 'Libro saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/libros/{id}",
     *      summary="Display the specified Libro",
     *      tags={"Libro"},
     *      description="Get Libro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Libro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Libro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Libro $libro */
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            return $this->sendError('Libro not found');
        }

        return $this->sendResponse($libro->toArray(), 'Libro retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateLibroAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/libros/{id}",
     *      summary="Update the specified Libro in storage",
     *      tags={"Libro"},
     *      description="Update Libro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Libro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Libro that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Libro")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Libro"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateLibroAPIRequest $request)
    {
        $input = $request->all();

        /** @var Libro $libro */
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            return $this->sendError('Libro not found');
        }

        $libro = $this->libroRepository->update($input, $id);

        return $this->sendResponse($libro->toArray(), 'Libro updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/libros/{id}",
     *      summary="Remove the specified Libro from storage",
     *      tags={"Libro"},
     *      description="Delete Libro",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Libro",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Libro $libro */
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            return $this->sendError('Libro not found');
        }

        $libro->delete();

        return $this->sendSuccess('Libro deleted successfully');
    }
}
