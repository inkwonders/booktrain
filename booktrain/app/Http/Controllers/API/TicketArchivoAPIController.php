<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTicketArchivoAPIRequest;
use App\Http\Requests\API\UpdateTicketArchivoAPIRequest;
use App\Models\TicketArchivo;
use App\Repositories\TicketArchivoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TicketArchivoController
 * @package App\Http\Controllers\API
 */

class TicketArchivoAPIController extends AppBaseController
{
    /** @var  TicketArchivoRepository */
    private $ticketArchivoRepository;

    public function __construct(TicketArchivoRepository $ticketArchivoRepo)
    {
        $this->ticketArchivoRepository = $ticketArchivoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketArchivos",
     *      summary="Get a listing of the TicketArchivos.",
     *      tags={"TicketArchivo"},
     *      description="Get all TicketArchivos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TicketArchivo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketArchivos = $this->ticketArchivoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ticketArchivos->toArray(), 'Ticket Archivos retrieved successfully');
    }

    /**
     * @param CreateTicketArchivoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticketArchivos",
     *      summary="Store a newly created TicketArchivo in storage",
     *      tags={"TicketArchivo"},
     *      description="Store TicketArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketArchivo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketArchivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTicketArchivoAPIRequest $request)
    {
        $input = $request->all();

        $ticketArchivo = $this->ticketArchivoRepository->create($input);

        return $this->sendResponse($ticketArchivo->toArray(), 'Ticket Archivo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketArchivos/{id}",
     *      summary="Display the specified TicketArchivo",
     *      tags={"TicketArchivo"},
     *      description="Get TicketArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TicketArchivo $ticketArchivo */
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            return $this->sendError('Ticket Archivo not found');
        }

        return $this->sendResponse($ticketArchivo->toArray(), 'Ticket Archivo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTicketArchivoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticketArchivos/{id}",
     *      summary="Update the specified TicketArchivo in storage",
     *      tags={"TicketArchivo"},
     *      description="Update TicketArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketArchivo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketArchivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTicketArchivoAPIRequest $request)
    {
        $input = $request->all();

        /** @var TicketArchivo $ticketArchivo */
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            return $this->sendError('Ticket Archivo not found');
        }

        $ticketArchivo = $this->ticketArchivoRepository->update($input, $id);

        return $this->sendResponse($ticketArchivo->toArray(), 'TicketArchivo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticketArchivos/{id}",
     *      summary="Remove the specified TicketArchivo from storage",
     *      tags={"TicketArchivo"},
     *      description="Delete TicketArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TicketArchivo $ticketArchivo */
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            return $this->sendError('Ticket Archivo not found');
        }

        $ticketArchivo->delete();

        return $this->sendSuccess('Ticket Archivo deleted successfully');
    }
}
