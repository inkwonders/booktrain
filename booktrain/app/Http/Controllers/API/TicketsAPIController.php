<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTicketEntradaAPIRequest;
use App\Http\Requests\API\UpdateTicketEntradaAPIRequest;
use App\Models\TicketEntrada;
use App\Models\TicketMotivo;
use App\Models\User;
use App\Models\TicketUsuario;
use App\Repositories\TicketEntradaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Transformers\Serializer;
use App\Transformers\OpcionesTicketTransformer;
use League\Fractal\Manager;
use Response;
use App\Models\TicketEntradaArchivo;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Notifications\Notification;
use Illuminate\Routing\Route;
use Illuminate\Notifications\Messages\MailMessage;


/**
 * Class TicketsAPIController
 * @package App\Http\Controllers\API
 */

class TicketsAPIController extends AppBaseController
{
    /** @var  TicketEntradaRepository */
    private $ticketEntradaRepository;

    public function __construct(TicketEntradaRepository $ticketEntradaRepo)
    {
        $this->ticketEntradaRepository = $ticketEntradaRepo;
    }





    public function getListaOpciones() {
        $motivos_ticket = TicketMotivo::all();

        $fractal = new Manager();

        $motivos = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($motivos_ticket, new OpcionesTicketTransformer))->toArray();

        return $this->sendResponse($motivos, 'Se encontraron ' . count($motivos) . ' motivos');
    }




    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketEntradas",
     *      summary="Get a listing of the TicketEntradas.",
     *      tags={"TicketEntrada"},
     *      description="Get all TicketEntradas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TicketEntrada")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketEntradas = $this->ticketEntradaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ticketEntradas->toArray(), 'Ticket Entradas retrieved successfully');
    }

    /**
     * @param CreateTicketEntradaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticketEntradas",
     *      summary="Store a newly created TicketEntrada in storage",
     *      tags={"TicketEntrada"},
     *      description="Store TicketEntrada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketEntrada that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketEntrada")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntrada"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function agregar_entrada(Request $request)
    {

        $usuario = Auth::user();

        $reglas = [
            'descripcion'   => 'sometimes|required|string',
            'archivos.*'      => 'sometimes|mimes:jpeg,png,jpg,gif,svg',
            'caso'          => 'required'
        ];

        $mensajes = [
            'descripcion.required'  => 'Debe incluir una descripción',
            'caso.required'         => 'El Ticket es obligatorio',
            'archivos.*.mimes' => 'Solo se permite subir imagenes como evidencia adjunta.',
        ];

        $validator = Validator::make(
            $request->all(),
            $reglas,
            $mensajes
        );

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->first(),'success' => false], 200);
        }

        $ticket = new TicketEntrada();
        if($usuario->hasRole('Admin')){
            $ticket->tipo_entrada = 2; // Entrada de usuario 1=cliente
        }
        else{
            $ticket->tipo_entrada = 1; // Entrada de usuario 1=cliente
        }

        // $ticket->folio = $usuario->id.'_'.Carbon::now()->format("YmdHms");
        $ticket->ticket_usuario_id = $request->caso;
        $ticket->comentario = request('descripcion');
        $ticket->usuario_id = $usuario->id;
        $ticket->save();

        // Guardamos los archivos
        if($request->has('archivos'))
        foreach ($request->archivos as $id => $archivo) {
            $nombre_imagen = $id . '_' . $ticket->id . '_' . $usuario->id . '_' . Carbon::now()->format("YmdHmsu") . '.' . $archivo->guessExtension();

            Storage::disk('tickets_entrada')->putFileAs('', $archivo, $nombre_imagen);
            $ticket->archivos()->create(
                [
                    'url' => "/storage/images/tickets_entrada_files/$nombre_imagen",
                    'archivo_nombre_original' => $archivo->getClientOriginalName(),
                    'tipo_archivo' => 'IMAGEN', // HARDCODEADO
                    'orden' => $ticket->archivos()->max('orden') + 1
                ]
            );
        }

        // 1.- Se verifica si el usuario logueado es admin
        //2.- Si es admin entra en el condicional
        if($usuario->hasRole('Admin'))
        {
            // 2.1 Si el usuario es admin se manda el correo Usuario Notificacion al soporte tecnico
            auth()->user()->sendTicketEntradaUsuarioNotification($ticket);
            // 2.1 si el usuario es admin se manda el correo de respuesta de soporte al usuario booktrain
            $ticket->ticket->user->sendTicketEntradaSoporteNotification($ticket);

        }
        // 3.- Si el usuario no es admin entra
        else
        {
        // usuario booktrain
            // 4.- se verifica que el usuario no tenga respuestas de soporte tecnico
            $ticket_entrada_notificacion = TicketEntrada::where('ticket_usuario_id',$request->caso)->where('tipo_entrada',2)->get()->last();
            // 5.- Si el usuario tiene respuestas de soporte tecnico entra
            if($ticket_entrada_notificacion != null){
                // 6.-Se busca al usuario asesor tecnico para mandar correo de notificacion
                $usuario_asesor = User::find($ticket_entrada_notificacion->usuario_id);
                // 7.- se manda correo al asesor tecnico de la respuesta del cliente
                    $usuario_asesor->sendTicketEntradaSoporteNotification($ticket);
            }
            else{
                //8 si el cliente no tiene respuestas de soporte aun y crea una nueva entrada debe llegar a pediosweb@booktrain
                // $usuario_pedidosweb = User::find(2080);
                $usuario_pedidosweb = User::find(2080);
                $usuario_pedidosweb->sendTicketEntradaSoporteNotification($ticket);
            }

            // 9 Sele envia el correo de entrada de usuario al usuario booktrain para notificar que se ha creado su entrada
         auth()->user()->sendTicketEntradaUsuarioNotification($ticket);
        }



        $ticket_usuario = TicketUsuario::find($ticket->ticket_usuario_id);
        if($usuario->hasRole('Admin'))
        {
        $ticket_usuario->status = 3;
        }
        else
        {
        $ticket_usuario->status = 2;
        }

        $ticket_usuario->save();

        return $this->sendSuccess('Ticket Entrada añadido correctamente con id:' . $ticket->id);
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketEntradas/{id}",
     *      summary="Display the specified TicketEntrada",
     *      tags={"TicketEntrada"},
     *      description="Get TicketEntrada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntrada",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntrada"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TicketEntrada $ticketEntrada */
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            return $this->sendError('Ticket Entrada not found');
        }

        return $this->sendResponse($ticketEntrada->toArray(), 'Ticket Entrada retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTicketEntradaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticketEntradas/{id}",
     *      summary="Update the specified TicketEntrada in storage",
     *      tags={"TicketEntrada"},
     *      description="Update TicketEntrada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntrada",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketEntrada that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketEntrada")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntrada"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTicketEntradaAPIRequest $request)
    {
        $input = $request->all();

        /** @var TicketEntrada $ticketEntrada */
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            return $this->sendError('Ticket Entrada not found');
        }

        $ticketEntrada = $this->ticketEntradaRepository->update($input, $id);

        return $this->sendResponse($ticketEntrada->toArray(), 'TicketEntrada updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticketEntradas/{id}",
     *      summary="Remove the specified TicketEntrada from storage",
     *      tags={"TicketEntrada"},
     *      description="Delete TicketEntrada",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntrada",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TicketEntrada $ticketEntrada */
        $ticketEntrada = $this->ticketEntradaRepository->find($id);

        if (empty($ticketEntrada)) {
            return $this->sendError('Ticket Entrada not found');
        }

        $ticketEntrada->delete();

        return $this->sendSuccess('Ticket Entrada deleted successfully');
    }
}
