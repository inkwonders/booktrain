<?php

namespace App\Http\Controllers\API;

use App\Models\Colegio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;

/**
 * Class AuthAPIController
 * @package App\Http\Controllers\API
 */

class AuthAPIController extends AppBaseController
{
    /**
     * @api{post} /api/login Iniciar sesión
     * @apiGroup Autenticación
     * @apiDescription Inicia sesión obteniendo un token que puede ser utilizado en subsecuentes llamadas
     *
     * @apiBody {String} email Correo del usuario
     * @apiBody {String} password Contraseña del usuario
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "email" : "admin@booktrain.com",
     *   "password" : "MySecret"
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": {
     *         "id": 7,
     *         "name": "Usuario Venta Movil",
     *         "email": "ventaMovil@inkwonders.com",
     *         "roles": "Venta Movil",
     *         "token": "13|gGG3HBSeLQOJIVUp66Q8WZY62nnOa3JYZzkDYnPF",
     *         "colegio": "COLEGIO BOOKTRAIN",
     *         "codigo_colegio": "CC1111"
     *     },
     *     "message": "Autenticado exitosamente como Usuario Venta Movil"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "Credenciales inválidas"
     * }
     */
    public function login(Request $request) {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validator = Validator::make(
            $request->all(),
            $rules
        );

        if( $validator->fails() )
            return $this->sendError($validator->errors()->first(), 400);

        if( Auth::attempt(['email' => $request->email, 'password' => $request->password, 'activo' => 1]) ) {
            $user = Auth::user();

            $colegio = Colegio::whereIn('codigo', $user->permissions->pluck('name') )->first();

            $token = $user->createToken( env('APP_NAME') );

            activity()
                ->causedBy($user->id)
                ->log("Inicio de sesión mediante el API generando un token");

            return $this->sendResponse([
                'id'             => $user->id,
                'name'           => $user->nombre_completo,
                'email'          => $user->email,
                'roles'          => $user->roles->pluck('name')->join(', ', ' y '),
                'token'          => $token->plainTextToken,
                'colegio'        => $colegio ? $colegio->nombre : '',
                'codigo_colegio' => $colegio ? $colegio->codigo : ''
            ], "Autenticado exitosamente como {$user->nombre_completo}");
        }

        return $this->sendError('Credenciales inválidas', 401);
    }

    /**
     * @api{get} /api/logout Cerrar sesión
     * @apiGroup Autenticación
     * @apiDescription Finaliza la sesión eliminando el token asociado al usuario
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "Se finalizó la sesión correctamente del usuario luz@inkwonders.com"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "Credenciales inválidas"
     * }
     */
    public function logout() {
        $user = Auth::user();

        if($token = $user->currentAccessToken())
            $token->delete();

        return $this->sendSuccess("Se finalizó la sesión correctamente del usuario {$user->email}");
    }
}
