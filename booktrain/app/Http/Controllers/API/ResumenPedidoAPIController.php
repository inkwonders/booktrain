<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResumenPedidoAPIRequest;
use App\Http\Requests\API\UpdateResumenPedidoAPIRequest;
use App\Models\ResumenPedido;
use App\Repositories\ResumenPedidoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ResumenPedidoController
 * @package App\Http\Controllers\API
 */

class ResumenPedidoAPIController extends AppBaseController
{
    /** @var  ResumenPedidoRepository */
    private $resumenPedidoRepository;

    public function __construct(ResumenPedidoRepository $resumenPedidoRepo)
    {
        $this->resumenPedidoRepository = $resumenPedidoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/resumenPedidos",
     *      summary="Get a listing of the ResumenPedidos.",
     *      tags={"ResumenPedido"},
     *      description="Get all ResumenPedidos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ResumenPedido")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $resumenPedidos = $this->resumenPedidoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($resumenPedidos->toArray(), 'Resumen Pedidos retrieved successfully');
    }

    /**
     * @param CreateResumenPedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/resumenPedidos",
     *      summary="Store a newly created ResumenPedido in storage",
     *      tags={"ResumenPedido"},
     *      description="Store ResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ResumenPedido that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ResumenPedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateResumenPedidoAPIRequest $request)
    {
        $input = $request->all();

        $resumenPedido = $this->resumenPedidoRepository->create($input);

        return $this->sendResponse($resumenPedido->toArray(), 'Resumen Pedido saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/resumenPedidos/{id}",
     *      summary="Display the specified ResumenPedido",
     *      tags={"ResumenPedido"},
     *      description="Get ResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ResumenPedido $resumenPedido */
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            return $this->sendError('Resumen Pedido not found');
        }

        return $this->sendResponse($resumenPedido->toArray(), 'Resumen Pedido retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateResumenPedidoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/resumenPedidos/{id}",
     *      summary="Update the specified ResumenPedido in storage",
     *      tags={"ResumenPedido"},
     *      description="Update ResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ResumenPedido that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ResumenPedido")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ResumenPedido"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateResumenPedidoAPIRequest $request)
    {
        $input = $request->all();

        /** @var ResumenPedido $resumenPedido */
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            return $this->sendError('Resumen Pedido not found');
        }

        $resumenPedido = $this->resumenPedidoRepository->update($input, $id);

        return $this->sendResponse($resumenPedido->toArray(), 'ResumenPedido updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/resumenPedidos/{id}",
     *      summary="Remove the specified ResumenPedido from storage",
     *      tags={"ResumenPedido"},
     *      description="Delete ResumenPedido",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ResumenPedido",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ResumenPedido $resumenPedido */
        $resumenPedido = $this->resumenPedidoRepository->find($id);

        if (empty($resumenPedido)) {
            return $this->sendError('Resumen Pedido not found');
        }

        $resumenPedido->delete();

        return $this->sendSuccess('Resumen Pedido deleted successfully');
    }
}
