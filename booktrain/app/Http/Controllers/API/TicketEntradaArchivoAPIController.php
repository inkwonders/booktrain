<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTicketEntradaArchivoAPIRequest;
use App\Http\Requests\API\UpdateTicketEntradaArchivoAPIRequest;
use App\Models\TicketEntradaArchivo;
use App\Repositories\TicketEntradaArchivoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TicketEntradaArchivoController
 * @package App\Http\Controllers\API
 */

class TicketEntradaArchivoAPIController extends AppBaseController
{
    /** @var  TicketEntradaArchivoRepository */
    private $ticketEntradaArchivoRepository;

    public function __construct(TicketEntradaArchivoRepository $ticketEntradaArchivoRepo)
    {
        $this->ticketEntradaArchivoRepository = $ticketEntradaArchivoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketEntradaArchivos",
     *      summary="Get a listing of the TicketEntradaArchivos.",
     *      tags={"TicketEntradaArchivo"},
     *      description="Get all TicketEntradaArchivos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TicketEntradaArchivo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketEntradaArchivos = $this->ticketEntradaArchivoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ticketEntradaArchivos->toArray(), 'Ticket Entrada Archivos retrieved successfully');
    }

    /**
     * @param CreateTicketEntradaArchivoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticketEntradaArchivos",
     *      summary="Store a newly created TicketEntradaArchivo in storage",
     *      tags={"TicketEntradaArchivo"},
     *      description="Store TicketEntradaArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketEntradaArchivo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketEntradaArchivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntradaArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTicketEntradaArchivoAPIRequest $request)
    {
        $input = $request->all();

        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->create($input);

        return $this->sendResponse($ticketEntradaArchivo->toArray(), 'Ticket Entrada Archivo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketEntradaArchivos/{id}",
     *      summary="Display the specified TicketEntradaArchivo",
     *      tags={"TicketEntradaArchivo"},
     *      description="Get TicketEntradaArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntradaArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntradaArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TicketEntradaArchivo $ticketEntradaArchivo */
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            return $this->sendError('Ticket Entrada Archivo not found');
        }

        return $this->sendResponse($ticketEntradaArchivo->toArray(), 'Ticket Entrada Archivo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTicketEntradaArchivoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticketEntradaArchivos/{id}",
     *      summary="Update the specified TicketEntradaArchivo in storage",
     *      tags={"TicketEntradaArchivo"},
     *      description="Update TicketEntradaArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntradaArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketEntradaArchivo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketEntradaArchivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketEntradaArchivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTicketEntradaArchivoAPIRequest $request)
    {
        $input = $request->all();

        /** @var TicketEntradaArchivo $ticketEntradaArchivo */
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            return $this->sendError('Ticket Entrada Archivo not found');
        }

        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->update($input, $id);

        return $this->sendResponse($ticketEntradaArchivo->toArray(), 'TicketEntradaArchivo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticketEntradaArchivos/{id}",
     *      summary="Remove the specified TicketEntradaArchivo from storage",
     *      tags={"TicketEntradaArchivo"},
     *      description="Delete TicketEntradaArchivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketEntradaArchivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TicketEntradaArchivo $ticketEntradaArchivo */
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            return $this->sendError('Ticket Entrada Archivo not found');
        }

        $ticketEntradaArchivo->delete();

        return $this->sendSuccess('Ticket Entrada Archivo deleted successfully');
    }
}
