<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLibrosColegiosAPIRequest;
use App\Http\Requests\API\UpdateLibrosColegiosAPIRequest;
use App\Models\LibrosColegios;
use App\Repositories\LibrosColegiosRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LibrosColegiosController
 * @package App\Http\Controllers\API
 */

class LibrosColegiosAPIController extends AppBaseController
{
    /** @var  LibrosColegiosRepository */
    private $librosColegiosRepository;

    public function __construct(LibrosColegiosRepository $librosColegiosRepo)
    {
        $this->librosColegiosRepository = $librosColegiosRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/librosColegios",
     *      summary="Get a listing of the LibrosColegios.",
     *      tags={"LibrosColegios"},
     *      description="Get all LibrosColegios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/LibrosColegios")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $librosColegios = $this->librosColegiosRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($librosColegios->toArray(), 'Libros Colegios retrieved successfully');
    }

    /**
     * @param CreateLibrosColegiosAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/librosColegios",
     *      summary="Store a newly created LibrosColegios in storage",
     *      tags={"LibrosColegios"},
     *      description="Store LibrosColegios",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="LibrosColegios that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/LibrosColegios")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/LibrosColegios"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateLibrosColegiosAPIRequest $request)
    {
        $input = $request->all();

        $librosColegios = $this->librosColegiosRepository->create($input);

        return $this->sendResponse($librosColegios->toArray(), 'Libros Colegios saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/librosColegios/{id}",
     *      summary="Display the specified LibrosColegios",
     *      tags={"LibrosColegios"},
     *      description="Get LibrosColegios",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of LibrosColegios",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/LibrosColegios"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var LibrosColegios $librosColegios */
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            return $this->sendError('Libros Colegios not found');
        }

        return $this->sendResponse($librosColegios->toArray(), 'Libros Colegios retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateLibrosColegiosAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/librosColegios/{id}",
     *      summary="Update the specified LibrosColegios in storage",
     *      tags={"LibrosColegios"},
     *      description="Update LibrosColegios",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of LibrosColegios",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="LibrosColegios that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/LibrosColegios")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/LibrosColegios"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateLibrosColegiosAPIRequest $request)
    {
        $input = $request->all();

        /** @var LibrosColegios $librosColegios */
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            return $this->sendError('Libros Colegios not found');
        }

        $librosColegios = $this->librosColegiosRepository->update($input, $id);

        return $this->sendResponse($librosColegios->toArray(), 'LibrosColegios updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/librosColegios/{id}",
     *      summary="Remove the specified LibrosColegios from storage",
     *      tags={"LibrosColegios"},
     *      description="Delete LibrosColegios",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of LibrosColegios",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var LibrosColegios $librosColegios */
        $librosColegios = $this->librosColegiosRepository->find($id);

        if (empty($librosColegios)) {
            return $this->sendError('Libros Colegios not found');
        }

        $librosColegios->delete();

        return $this->sendSuccess('Libros Colegios deleted successfully');
    }
}
