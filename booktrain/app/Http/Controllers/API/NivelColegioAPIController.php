<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNivelColegioAPIRequest;
use App\Http\Requests\API\UpdateNivelColegioAPIRequest;
use App\Models\NivelColegio;
use App\Repositories\NivelColegioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NivelColegioController
 * @package App\Http\Controllers\API
 */

class NivelColegioAPIController extends AppBaseController
{
    /** @var  NivelColegioRepository */
    private $nivelColegioRepository;

    public function __construct(NivelColegioRepository $nivelColegioRepo)
    {
        $this->nivelColegioRepository = $nivelColegioRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/nivelColegios",
     *      summary="Get a listing of the NivelColegios.",
     *      tags={"NivelColegio"},
     *      description="Get all NivelColegios",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/NivelColegio")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $nivelColegios = $this->nivelColegioRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($nivelColegios->toArray(), 'Nivel Colegios retrieved successfully');
    }

    /**
     * @param CreateNivelColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/nivelColegios",
     *      summary="Store a newly created NivelColegio in storage",
     *      tags={"NivelColegio"},
     *      description="Store NivelColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="NivelColegio that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/NivelColegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NivelColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateNivelColegioAPIRequest $request)
    {
        $input = $request->all();

        $nivelColegio = $this->nivelColegioRepository->create($input);

        return $this->sendResponse($nivelColegio->toArray(), 'Nivel Colegio saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/nivelColegios/{id}",
     *      summary="Display the specified NivelColegio",
     *      tags={"NivelColegio"},
     *      description="Get NivelColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NivelColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NivelColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var NivelColegio $nivelColegio */
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            return $this->sendError('Nivel Colegio not found');
        }

        return $this->sendResponse($nivelColegio->toArray(), 'Nivel Colegio retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateNivelColegioAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/nivelColegios/{id}",
     *      summary="Update the specified NivelColegio in storage",
     *      tags={"NivelColegio"},
     *      description="Update NivelColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NivelColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="NivelColegio that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/NivelColegio")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NivelColegio"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateNivelColegioAPIRequest $request)
    {
        $input = $request->all();

        /** @var NivelColegio $nivelColegio */
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            return $this->sendError('Nivel Colegio not found');
        }

        $nivelColegio = $this->nivelColegioRepository->update($input, $id);

        return $this->sendResponse($nivelColegio->toArray(), 'NivelColegio updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/nivelColegios/{id}",
     *      summary="Remove the specified NivelColegio from storage",
     *      tags={"NivelColegio"},
     *      description="Delete NivelColegio",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NivelColegio",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var NivelColegio $nivelColegio */
        $nivelColegio = $this->nivelColegioRepository->find($id);

        if (empty($nivelColegio)) {
            return $this->sendError('Nivel Colegio not found');
        }

        $nivelColegio->delete();

        return $this->sendSuccess('Nivel Colegio deleted successfully');
    }
}
