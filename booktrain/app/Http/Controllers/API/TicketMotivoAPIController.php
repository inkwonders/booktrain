<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTicketMotivoAPIRequest;
use App\Http\Requests\API\UpdateTicketMotivoAPIRequest;
use App\Models\TicketMotivo;
use App\Repositories\TicketMotivoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TicketMotivoController
 * @package App\Http\Controllers\API
 */

class TicketMotivoAPIController extends AppBaseController
{
    /** @var  TicketMotivoRepository */
    private $ticketMotivoRepository;

    public function __construct(TicketMotivoRepository $ticketMotivoRepo)
    {
        $this->ticketMotivoRepository = $ticketMotivoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketMotivos",
     *      summary="Get a listing of the TicketMotivos.",
     *      tags={"TicketMotivo"},
     *      description="Get all TicketMotivos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TicketMotivo")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ticketMotivos = $this->ticketMotivoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ticketMotivos->toArray(), 'Ticket Motivos retrieved successfully');
    }

    /**
     * @param CreateTicketMotivoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ticketMotivos",
     *      summary="Store a newly created TicketMotivo in storage",
     *      tags={"TicketMotivo"},
     *      description="Store TicketMotivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketMotivo that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketMotivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketMotivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTicketMotivoAPIRequest $request)
    {
        $input = $request->all();

        $ticketMotivo = $this->ticketMotivoRepository->create($input);

        return $this->sendResponse($ticketMotivo->toArray(), 'Ticket Motivo saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ticketMotivos/{id}",
     *      summary="Display the specified TicketMotivo",
     *      tags={"TicketMotivo"},
     *      description="Get TicketMotivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketMotivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketMotivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TicketMotivo $ticketMotivo */
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            return $this->sendError('Ticket Motivo not found');
        }

        return $this->sendResponse($ticketMotivo->toArray(), 'Ticket Motivo retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTicketMotivoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ticketMotivos/{id}",
     *      summary="Update the specified TicketMotivo in storage",
     *      tags={"TicketMotivo"},
     *      description="Update TicketMotivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketMotivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TicketMotivo that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TicketMotivo")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TicketMotivo"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTicketMotivoAPIRequest $request)
    {
        $input = $request->all();

        /** @var TicketMotivo $ticketMotivo */
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            return $this->sendError('Ticket Motivo not found');
        }

        $ticketMotivo = $this->ticketMotivoRepository->update($input, $id);

        return $this->sendResponse($ticketMotivo->toArray(), 'TicketMotivo updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ticketMotivos/{id}",
     *      summary="Remove the specified TicketMotivo from storage",
     *      tags={"TicketMotivo"},
     *      description="Delete TicketMotivo",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TicketMotivo",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TicketMotivo $ticketMotivo */
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            return $this->sendError('Ticket Motivo not found');
        }

        $ticketMotivo->delete();

        return $this->sendSuccess('Ticket Motivo deleted successfully');
    }
}
