<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\MetodoPago;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Collection;
use App\Repositories\MetodoPagoRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateMetodoPagoAPIRequest;
use App\Http\Requests\API\UpdateMetodoPagoAPIRequest;
use App\Transformers\MetodoPagoTransformer;

/**
 * Class MetodoPagoController
 * @package App\Http\Controllers\API
 */

class MetodoPagoAPIController extends AppBaseController
{
    /** @var  MetodoPagoRepository */
    private $metodoPagoRepository;

    public function __construct(MetodoPagoRepository $metodoPagoRepo)
    {
        $this->metodoPagoRepository = $metodoPagoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/metodoPagos",
     *      summary="Get a listing of the MetodoPagos.",
     *      tags={"MetodoPago"},
     *      description="Get all MetodoPagos",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MetodoPago")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $metodoPagos = $this->metodoPagoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($metodoPagos->toArray(), 'Metodo Pagos retrieved successfully');
    }

    /**
     * @param CreateMetodoPagoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/metodoPagos",
     *      summary="Store a newly created MetodoPago in storage",
     *      tags={"MetodoPago"},
     *      description="Store MetodoPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MetodoPago that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MetodoPago")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetodoPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMetodoPagoAPIRequest $request)
    {
        $input = $request->all();

        $metodoPago = $this->metodoPagoRepository->create($input);

        return $this->sendResponse($metodoPago->toArray(), 'Metodo Pago saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/metodoPagos/{id}",
     *      summary="Display the specified MetodoPago",
     *      tags={"MetodoPago"},
     *      description="Get MetodoPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetodoPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetodoPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MetodoPago $metodoPago */
        $metodoPago = $this->metodoPagoRepository->find($id);

        if (empty($metodoPago)) {
            return $this->sendError('Metodo Pago not found');
        }

        return $this->sendResponse($metodoPago->toArray(), 'Metodo Pago retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMetodoPagoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/metodoPagos/{id}",
     *      summary="Update the specified MetodoPago in storage",
     *      tags={"MetodoPago"},
     *      description="Update MetodoPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetodoPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MetodoPago that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MetodoPago")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetodoPago"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMetodoPagoAPIRequest $request)
    {
        $input = $request->all();

        /** @var MetodoPago $metodoPago */
        $metodoPago = $this->metodoPagoRepository->find($id);

        if (empty($metodoPago)) {
            return $this->sendError('Metodo Pago not found');
        }

        $metodoPago = $this->metodoPagoRepository->update($input, $id);

        return $this->sendResponse($metodoPago->toArray(), 'MetodoPago updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/metodoPagos/{id}",
     *      summary="Remove the specified MetodoPago from storage",
     *      tags={"MetodoPago"},
     *      description="Delete MetodoPago",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetodoPago",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MetodoPago $metodoPago */
        $metodoPago = $this->metodoPagoRepository->find($id);

        if (empty($metodoPago)) {
            return $this->sendError('Metodo Pago not found');
        }

        $metodoPago->delete();

        return $this->sendSuccess('Metodo Pago deleted successfully');
    }

    public function metodos()
    {
        $metodosPago = MetodoPago::orderBy('orden')->get();

        $fractal = new Manager();

        $datosMetodosPago = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($metodosPago, new MetodoPagoTransformer))->toArray();

        return $this->sendSuccess($datosMetodosPago, $metodosPago->count() . " metodos obtenidos");
    }
}
