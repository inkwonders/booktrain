<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\UsoCFDI;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use App\Transformers\UserTransformer;
use App\Repositories\UsoCFDIRepository;
use League\Fractal\Resource\Collection;
use App\Transformers\UsoCFDITransformer;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateUsoCFDIAPIRequest;
use App\Http\Requests\API\UpdateUsoCFDIAPIRequest;

/**
 * Class UsoCFDIController
 * @package App\Http\Controllers\API
 */

class UsoCFDIAPIController extends AppBaseController
{
    /** @var  UsoCFDIRepository */
    private $usoCFDIRepository;

    public function __construct(UsoCFDIRepository $usoCFDIRepo)
    {
        $this->usoCFDIRepository = $usoCFDIRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/usoCFDIs",
     *      summary="Get a listing of the UsoCFDIs.",
     *      tags={"UsoCFDI"},
     *      description="Get all UsoCFDIs",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UsoCFDI")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $usoCFDIs = $this->usoCFDIRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $fractal = new Manager();

        $datosUsosCFDI = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($usoCFDIs, new UsoCFDITransformer))->toArray();

        return $this->sendSuccess($datosUsosCFDI, "Información obtenida");
    }

    /**
     * @param CreateUsoCFDIAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/usoCFDIs",
     *      summary="Store a newly created UsoCFDI in storage",
     *      tags={"UsoCFDI"},
     *      description="Store UsoCFDI",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsoCFDI that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsoCFDI")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsoCFDI"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUsoCFDIAPIRequest $request)
    {
        $input = $request->all();

        $usoCFDI = $this->usoCFDIRepository->create($input);

        return $this->sendResponse($usoCFDI->toArray(), 'Uso C F D I saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/usoCFDIs/{id}",
     *      summary="Display the specified UsoCFDI",
     *      tags={"UsoCFDI"},
     *      description="Get UsoCFDI",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsoCFDI",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsoCFDI"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UsoCFDI $usoCFDI */
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            return $this->sendError('Uso C F D I not found');
        }

        return $this->sendResponse($usoCFDI->toArray(), 'Uso C F D I retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUsoCFDIAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/usoCFDIs/{id}",
     *      summary="Update the specified UsoCFDI in storage",
     *      tags={"UsoCFDI"},
     *      description="Update UsoCFDI",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsoCFDI",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UsoCFDI that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UsoCFDI")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UsoCFDI"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUsoCFDIAPIRequest $request)
    {
        $input = $request->all();

        /** @var UsoCFDI $usoCFDI */
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            return $this->sendError('Uso C F D I not found');
        }

        $usoCFDI = $this->usoCFDIRepository->update($input, $id);

        return $this->sendResponse($usoCFDI->toArray(), 'UsoCFDI updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/usoCFDIs/{id}",
     *      summary="Remove the specified UsoCFDI from storage",
     *      tags={"UsoCFDI"},
     *      description="Delete UsoCFDI",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UsoCFDI",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UsoCFDI $usoCFDI */
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            return $this->sendError('Uso C F D I not found');
        }

        $usoCFDI->delete();

        return $this->sendSuccess('Uso C F D I deleted successfully');
    }
}
