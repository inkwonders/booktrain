<?php

namespace App\Http\Controllers\API;

use Response;
use App\Models\Estado;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use App\Repositories\EstadoRepository;
use App\Transformers\EstadoTransformer;
use League\Fractal\Resource\Collection;
use App\Transformers\MunicipioTransformer;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateEstadoAPIRequest;
use App\Http\Requests\API\UpdateEstadoAPIRequest;

/**
 * Class EstadoController
 * @package App\Http\Controllers\API
 */

class EstadoAPIController extends AppBaseController
{
    /** @var  EstadoRepository */
    private $estadoRepository;

    public function __construct(EstadoRepository $estadoRepo)
    {
        $this->estadoRepository = $estadoRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/estados",
     *      summary="Get a listing of the Estados.",
     *      tags={"Estado"},
     *      description="Get all Estados",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Estado")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $estados = $this->estadoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($estados->toArray(), 'Estados retrieved successfully');
    }

    /**
     * @param CreateEstadoAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/estados",
     *      summary="Store a newly created Estado in storage",
     *      tags={"Estado"},
     *      description="Store Estado",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Estado that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Estado")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estado"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEstadoAPIRequest $request)
    {
        $input = $request->all();

        $estado = $this->estadoRepository->create($input);

        return $this->sendResponse($estado->toArray(), 'Estado saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/estados/{id}",
     *      summary="Display the specified Estado",
     *      tags={"Estado"},
     *      description="Get Estado",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estado",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estado"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Estado $estado */
        $estado = $this->estadoRepository->find($id);

        if (empty($estado)) {
            return $this->sendError('Estado not found');
        }

        return $this->sendResponse($estado->toArray(), 'Estado retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEstadoAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/estados/{id}",
     *      summary="Update the specified Estado in storage",
     *      tags={"Estado"},
     *      description="Update Estado",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estado",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Estado that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Estado")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Estado"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEstadoAPIRequest $request)
    {
        $input = $request->all();

        /** @var Estado $estado */
        $estado = $this->estadoRepository->find($id);

        if (empty($estado)) {
            return $this->sendError('Estado not found');
        }

        $estado = $this->estadoRepository->update($input, $id);

        return $this->sendResponse($estado->toArray(), 'Estado updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/estados/{id}",
     *      summary="Remove the specified Estado from storage",
     *      tags={"Estado"},
     *      description="Delete Estado",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Estado",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Estado $estado */
        $estado = $this->estadoRepository->find($id);

        if (empty($estado)) {
            return $this->sendError('Estado not found');
        }

        $estado->delete();

        return $this->sendSuccess('Estado deleted successfully');
    }

    public function estados()
    {

        $fractal = new Manager();

        $estadoInformacion = $fractal->setSerializer(new Serializer())
            ->createData(new Collection(Estado::all(), new EstadoTransformer))->toArray();

        return response()->json(['success' => true, 'data' => $estadoInformacion], 200);

    }

    public function municipiosEstado($id)
    {
        $estado = Estado::find($id);

        $fractal = new Manager();

        $municipiosEstado = $fractal->setSerializer(new Serializer())
            ->createData(new Collection($estado->municipios, new MunicipioTransformer))->toArray();

        return response()->json(['success' => true, 'data' => $municipiosEstado], 200);

    }
}
