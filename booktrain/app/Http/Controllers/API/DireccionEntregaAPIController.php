<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDireccionEntregaAPIRequest;
use App\Http\Requests\API\UpdateDireccionEntregaAPIRequest;
use App\Models\DireccionEntrega;
use App\Repositories\DireccionEntregaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DireccionEntregaController
 * @package App\Http\Controllers\API
 */

class DireccionEntregaAPIController extends AppBaseController
{
    /** @var  DireccionEntregaRepository */
    private $direccionEntregaRepository;

    public function __construct(DireccionEntregaRepository $direccionEntregaRepo)
    {
        $this->direccionEntregaRepository = $direccionEntregaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/direccionEntregas",
     *      summary="Get a listing of the DireccionEntregas.",
     *      tags={"DireccionEntrega"},
     *      description="Get all DireccionEntregas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/DireccionEntrega")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $direccionEntregas = $this->direccionEntregaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($direccionEntregas->toArray(), 'Direccion Entregas retrieved successfully');
    }

    /**
     * @param CreateDireccionEntregaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/direccionEntregas",
     *      summary="Store a newly created DireccionEntrega in storage",
     *      tags={"DireccionEntrega"},
     *      description="Store DireccionEntrega",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DireccionEntrega that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DireccionEntrega")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DireccionEntrega"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDireccionEntregaAPIRequest $request)
    {
        $input = $request->all();

        $direccionEntrega = $this->direccionEntregaRepository->create($input);

        return $this->sendResponse($direccionEntrega->toArray(), 'Direccion Entrega saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/direccionEntregas/{id}",
     *      summary="Display the specified DireccionEntrega",
     *      tags={"DireccionEntrega"},
     *      description="Get DireccionEntrega",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DireccionEntrega",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DireccionEntrega"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var DireccionEntrega $direccionEntrega */
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            return $this->sendError('Direccion Entrega not found');
        }

        return $this->sendResponse($direccionEntrega->toArray(), 'Direccion Entrega retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDireccionEntregaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/direccionEntregas/{id}",
     *      summary="Update the specified DireccionEntrega in storage",
     *      tags={"DireccionEntrega"},
     *      description="Update DireccionEntrega",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DireccionEntrega",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DireccionEntrega that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DireccionEntrega")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DireccionEntrega"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDireccionEntregaAPIRequest $request)
    {
        $input = $request->all();

        /** @var DireccionEntrega $direccionEntrega */
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            return $this->sendError('Direccion Entrega not found');
        }

        $direccionEntrega = $this->direccionEntregaRepository->update($input, $id);

        return $this->sendResponse($direccionEntrega->toArray(), 'DireccionEntrega updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/direccionEntregas/{id}",
     *      summary="Remove the specified DireccionEntrega from storage",
     *      tags={"DireccionEntrega"},
     *      description="Delete DireccionEntrega",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DireccionEntrega",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var DireccionEntrega $direccionEntrega */
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            return $this->sendError('Direccion Entrega not found');
        }

        $direccionEntrega->delete();

        return $this->sendSuccess('Direccion Entrega deleted successfully');
    }
}
