<?php

namespace App\Http\Controllers;

use League\Fractal;
use App\Models\User;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Log;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Transformers\UserTransformer;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Validator;
use Response;

class UsuariosController extends Controller
{

    public function index(Request $request)
    {

        $usuarios = User::all();
        return view('private.usuarios.index')->with('usuarios', $usuarios);
    }

    public function new_superadmin(Request $request, $id)
    {
        $usuario = User::find($id);
        $usuario = $usuario->hasRole('Super Admin');
    }

    public function show(Request $request, $id)
    {

        $usuario = User::find($id);

        return view('private.usuarios.show')->with('usuario', $usuario);
    }

    public function edit(Request $request, $id)
    {
    }

    public function update(Request $request, $id)
    {

        Log::info("UsuariosController@update", $request->all());

        $reglas = [
            'name' => 'sometimes|required|string|max:60',
            'apellidos' => 'sometimes|required|string|max:60',
            'email' => 'sometimes|string|email|max:100|unique:users|confirmed',
            'password' => 'sometimes',
            'password_confirmation' => 'sometimes',
        ];

        $mensajes = [
            'name.required' => 'Su nombre es requerido',
            'apellidos.required' => 'Sus apellidos son requeridos',
            'email.unique' => 'Éste correo ya se encuentra en uso',
            'email.required' => 'Por favor ingrese un correo electrónico',
            // 'password.required' => 'Por favor ingrese una contraseña de 8 caractéres como mínimo',
            'required' => 'Se esperaba :attribute',
            // 'password.min' => 'Se esperaba un password de tamaño minimo :min caractéres',
        ];

        $validator = Validator::make(
            $request->all(),
            $reglas,
            $mensajes
        );

        if ($validator->fails()) {
            // TO DO VERIFICAR ESTO
            return response()->json(['success' => false, 'msg' => $validator->errors()->first()], 400);
            // return redirect('mi-informacion');
        }



        $usuario = User::find($id);


        // $usuario->name = $request->name;
        // $usuario->apellidos = $request->apellidos;
        // $usuario->email = $request->email;

        // if(!empty($request->password)){
        //     $usuario->password = Hash::make($request->password);
        // }

        // $usuario->save();

        DB::beginTransaction();

        $data = $request->all();

        if (!empty($request->password)) {
            $data['password'] = bcrypt($request->password);
            $request->password = bcrypt($request->password);
        }
        // if ($request->has('password'))
        // {
        //     $data['password'] = bcrypt($request->password);
        //     // $request->password = bcrypt($request->password);
        // }

        try {
            // Obtenemos los campos del request y filtramos los que no están vaciós para actualizar el perfil del usuario
            $usuario->update(
                collect($data)->filter(function ($item) {
                    // collect($request->all())->filter(function($item) {
                    return $item != '';
                })->all()

            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'success' => false,
                'message' => 'Error al actualizar al usuario: ' . $e->getMessage()
            ], 503);
        }


        return redirect('mi-informacion');
    }

    public function destroy(Request $request, $id)
    {
    }



    public function transformerUser()
    {





        $fractal = new Manager();


        /** Ejemplo crear transformer en una coleccion de los usuarios */

        // $usuarios = User::all();
        // $usuarios = $fractal->setSerializer(new Serializer())
        //     ->createData( new Collection( $usuarios, new UserTransformer ) )->toArray();

        /** Fin del ejemplo de coleccion */

        /** Ejemplo crear transformer de un solo usuario */

        // $usuario = User::find(1);
        // $usuario = $fractal->setSerializer(new Serializer())
        //     ->createData( new Item( $usuario, new UserTransformer ) )->toArray();

        // dd($usuario);

        /** Fin del ejemplo unitario */
    }

    public function miInformacion()
    {
        return view('private.mi-informacion');
    }

    public function  notificacionEmail()
    {
        $usuarios = User::where('email_verified_at', null)->get();

        foreach ($usuarios as $usuarios) {
            $usuarios->email_verified_at = '2021-05-14 12:15:52';
            $usuarios->save();
        }
    }

    public function getUsuarios()
    {
        return view('private.admin.usuarios');
    }

    public function getInformacionLog(Request $request)
    {
        if ($request->inicio == null) {
            $request->inicio = date("Y-m-d");
        }

        if ($request->fin == null) {
            $request->fin = date("Y-m-d");
        }

        return view('private.admin.log-reporte')
            ->with('inicio', $request->inicio)
            ->with('fin', $request->fin)
            ->with('usuario', $request->usuario);
    }
}
