<?php

namespace App\Http\Controllers;

use App\DataTables\UsoCFDIDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUsoCFDIRequest;
use App\Http\Requests\UpdateUsoCFDIRequest;
use App\Repositories\UsoCFDIRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class UsoCFDIController extends AppBaseController
{
    /** @var  UsoCFDIRepository */
    private $usoCFDIRepository;

    public function __construct(UsoCFDIRepository $usoCFDIRepo)
    {
        $this->usoCFDIRepository = $usoCFDIRepo;
    }

    /**
     * Display a listing of the UsoCFDI.
     *
     * @param UsoCFDIDataTable $usoCFDIDataTable
     * @return Response
     */
    public function index(UsoCFDIDataTable $usoCFDIDataTable)
    {
        return $usoCFDIDataTable->render('uso_c_f_d_is.index');
    }

    /**
     * Show the form for creating a new UsoCFDI.
     *
     * @return Response
     */
    public function create()
    {
        return view('uso_c_f_d_is.create');
    }

    /**
     * Store a newly created UsoCFDI in storage.
     *
     * @param CreateUsoCFDIRequest $request
     *
     * @return Response
     */
    public function store(CreateUsoCFDIRequest $request)
    {
        $input = $request->all();

        $usoCFDI = $this->usoCFDIRepository->create($input);

        Flash::success('Uso C F D I saved successfully.');

        return redirect(route('usoCFDIs.index'));
    }

    /**
     * Display the specified UsoCFDI.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            Flash::error('Uso C F D I not found');

            return redirect(route('usoCFDIs.index'));
        }

        return view('uso_c_f_d_is.show')->with('usoCFDI', $usoCFDI);
    }

    /**
     * Show the form for editing the specified UsoCFDI.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            Flash::error('Uso C F D I not found');

            return redirect(route('usoCFDIs.index'));
        }

        return view('uso_c_f_d_is.edit')->with('usoCFDI', $usoCFDI);
    }

    /**
     * Update the specified UsoCFDI in storage.
     *
     * @param  int              $id
     * @param UpdateUsoCFDIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsoCFDIRequest $request)
    {
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            Flash::error('Uso C F D I not found');

            return redirect(route('usoCFDIs.index'));
        }

        $usoCFDI = $this->usoCFDIRepository->update($request->all(), $id);

        Flash::success('Uso C F D I updated successfully.');

        return redirect(route('usoCFDIs.index'));
    }

    /**
     * Remove the specified UsoCFDI from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $usoCFDI = $this->usoCFDIRepository->find($id);

        if (empty($usoCFDI)) {
            Flash::error('Uso C F D I not found');

            return redirect(route('usoCFDIs.index'));
        }

        $this->usoCFDIRepository->delete($id);

        Flash::success('Uso C F D I deleted successfully.');

        return redirect(route('usoCFDIs.index'));
    }
}
