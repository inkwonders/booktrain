<?php

namespace App\Http\Controllers;

use App\Models\Libro;
use App\Http\Requests;
use App\Models\Pedido;
use App\Models\Colegio;
use App\Models\Paquete;
use Laracasts\Flash\Flash;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use App\Models\NivelColegio;
use Illuminate\Http\Request;
// use Response;
use App\Models\SeccionColegio;
use App\Transformers\Serializer;
use App\Models\RegistroActividad;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Log;
use App\DataTables\ColegioDataTable;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteColegioExport;
use App\Exports\ReporteGeneralExport;
use App\Exports\AnalisisPedidosExport;
use App\Repositories\ColegioRepository;
use League\Fractal\Resource\Collection;
use App\Transformers\ColegioTransformer;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel as ExcelExcel;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateColegioRequest;
use App\Http\Requests\UpdateColegioRequest;
use App\Imports\UploadPedidosEntregadosImport;
use App\Exports\Colegios\RolColegioReporteColegioExport;
use App\Exports\Colegios\RolColegioReporteGeneralExport;

class ColegioController extends AppBaseController
{
    /** @var  ColegioRepository */
    private $colegioRepository;

    public function __construct(ColegioRepository $colegioRepo)
    {
        $this->colegioRepository = $colegioRepo;
    }

    /**
     * Display a listing of the Colegio.
     *
     * @return Response
     */
    public function index()
    {
        return view('private.admin.colegios.index');
    }

    /**
     * Show the form for creating a new Colegio.
     *
     * @return Response
     */
    public function create()
    {
        return view('private.admin.colegios.edit')->with('colegio', null);
    }

    /**
     * Store a newly created Colegio in storage.
     *
     * @param CreateColegioRequest $request
     *
     * @return Response
     */
    public function store(CreateColegioRequest $request)
    {
        $input = $request->all();

        $colegio = $this->colegioRepository->create($input);

        Flash::success('Colegio saved successfully.');

        return redirect(route('colegios.index'));
    }

    /**
     * Display the specified Colegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($codigo)
    {
        $colegio = $this->colegioRepository->allQuery(['codigo' => $codigo])->first();

        if (empty($colegio)) {
            Flash::error('Colegio not found');

            return redirect(route('inicio'));
        }

        return view('private.colegio')->with('colegio', $colegio);
    }

    /**
     * Show the form for editing the specified Colegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            Flash::error('Colegio not found');

            return redirect(route('colegios.index'));
        }

        return view('private.admin.colegios.edit')->with('colegio', $colegio);
    }

    /**
     * Update the specified Colegio in storage.
     *
     * @param  int              $id
     * @param UpdateColegioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateColegioRequest $request)
    {
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            Flash::error('Colegio not found');

            return redirect(route('colegios.index'));
        }

        $colegio = $this->colegioRepository->update($request->all(), $id);

        Flash::success('Colegio updated successfully.');

        return redirect(route('colegios.index'));
    }

    /**
     * Remove the specified Colegio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $colegio = $this->colegioRepository->find($id);

        if (empty($colegio)) {
            Flash::error('Colegio not found');

            return redirect(route('colegios.index'));
        }

        $this->colegioRepository->delete($id);

        Flash::success('Colegio deleted successfully.');

        return redirect(route('colegios.index'));
    }

    public function libros(Colegio $colegio) {
        return view('private.admin.colegios.lista_libros_colegio')->with('colegio', $colegio);
    }

    public function asignarLibros(Colegio $colegio) {
        return view('private.admin.colegios.listado_libros')->with('colegio', $colegio);
    }

    public function metodosPago(Colegio $colegio) {
        return view('private.admin.colegios.metodos_pago')->with('colegio', $colegio);
    }

    public function editarLibro(Colegio $colegio, $libro, $paquete) {
        return view('private.admin.colegios.editar_libro_colegio')
            ->with([
                'colegio' => $colegio,
                'libro_id'   => $libro,
                'paquete_id' => $paquete
            ]);
    }

    /**
     * Devuelve la información del colegio según los roles o permisos del usuario que la solicita
     */
    public function informacion() {
        $usuario = Auth::user();

        $permisos = $usuario->permissions->pluck('name');

        $colegio = Colegio::whereIn('codigo', $permisos)
            ->activo()
            ->with('libros', 'secciones.niveles')
            ->first();

        if( ! $colegio)
            return $this->sendError('No existe un colegio asociado a este usuario');

        $informacion = [
            'id'            => (int) $colegio->id,
            'codigo'        => $colegio->codigo,
	        'nombre'        => $colegio->nombre,
            'logo'          => $colegio->logo,
            'libros'        => $colegio->libros()
                    ->wherePivot('paquete_id', null)
                    ->get()
                    ->map(function($libro) {
                return [
                    'id'        => $libro->id,
                    'nombre'    => $libro->nombre,
                    'isbn'      => $libro->isbn
                ];
            }),
            'niveles' => $colegio->secciones
                    ->pluck('niveles')
                    ->collapse()
                    ->unique()
                    ->map(function($nivel) {
                return [
                    'id' => $nivel->id,
                    'seccion' => $nivel->seccion->nombre,
                    'nombre' => $nivel->nombre
                ];
            })
        ];

        return $this->sendResponse($informacion, "Colegio { $colegio->id } obtenido exitosamente");
    }

    public function informacionColegio(Request $request)
    {
        $reglas = [
            'codigo' => 'required'
        ];
        $mensajes = [
            'codigo.required' => 'El código es obligatorio'
        ];

        $validador = Validator::make($request->all(), $reglas, $mensajes);

        if ($validador->fails()) {
            return redirect('inicio')->with('error', $validador->errors()->first());
        }

        $colegio = Colegio::where('codigo', $request->codigo)->first();

        if (empty($colegio)) {
            return redirect('inicio')->with('error', 'El código es incorrecto.');
        }

        if ($colegio->activo == 0) {
            return redirect('inicio')->with('error', 'Por el momento el colegio no está disponible.');
        }

        $fractal = new Manager();

        $colegioInformacion = $fractal->setSerializer(new Serializer())
            ->parseIncludes(['secciones'])
            ->createData(new Item($colegio, new ColegioTransformer))->toArray();

        return redirect()->route('colegio.show', ['codigo' => $colegio->codigo])->with('colegioInformacion', $colegioInformacion);
    }

    public function secciones(Request $request)
    {

        $colegio = Colegio::find($request->id);

        $secciones_colegio = $colegio->secciones;

        return response()->json(['success' => true, 'data' => $secciones_colegio], 200);
    }
    // Funcion para añadir activo a métodos de pago
    public function addActivo()
    {
        $colegios = Colegio::all();
        foreach ($colegios as $colegio) {
            $configuracion = $colegio->configuracion;
            foreach ($configuracion->metodos_pago as $metodo_pago_objeto) {
                foreach ($metodo_pago_objeto->formas_pago as $forma_de_pago) {
                    $forma_de_pago->activo = 1;
                }
            }
            $colegio->update([
                'configuracion' => $configuracion
            ]);
        }
    }

    /**
     * Devuelve la vista para generar un reporte para el rol de colegio
     */
    public function reporteGeneral() {
        $colegio = Colegio::whereIn('codigo',
            Auth::user()
                ->permissions
                ->pluck('name')
        )->first();

        return view('private.colegio.reporte-general')->with('colegio', $colegio);
    }


    /**
     * Esta vista es el resultado del formuario de selección de reporte del colegio al acceder como un rol de colegio
     * Devuelve el reporte de colegio según los parámetros enviados en el Request
     *
     * Es un reporte de las ventas de un colegio en un rango de fechas
     * Si un libro está disponible en más de un grado se debe repetir
     * Cada venta tiene N libros, estos se deberán agrupar para obtener la sumatoria de ventas, devoluciones, cantidad de ventas, etc
     */
    public function postReporteGeneral(Request $request) {
        $request->validate([
            'inicio'    => 'required',
            'fin'       => 'required',
            'isbn'      => 'sometimes',
            'grado'     => 'sometimes',
        ]);

        $usuario = Auth::user();

        $inicio = $request->inicio;
        $fin = $request->fin;
        $grado = $request->grado;

        $isbn = $request->get('isbn');

        $codigos = $usuario->permissions->pluck('name');

        $colegio = Colegio::whereIn('codigo', $codigos)->first();

        if( ! $colegio )
            return $this->sendError('No existe un colegio asociado a este usuario');

        $libro_id = null;

        if( $isbn )
            $libro_id = Libro::where('isbn', $isbn)->select('id')->first()->id;

        $libros = app('App\Http\Controllers\ReporteController')->generaInformacionReporteGeneralColegio($codigos, $inicio, $fin, $grado, $libro_id);

        if( ! $isbn )
            $isbn = 'TODOS LOS ISBN';

        $grado = 'TODOS LOS GRADOS';

        if( $request->grado )
            $grado = NivelColegio::where('id', $request->grado)->nombre;

        return view('private.reportes.colegio')
            ->with(compact('colegio', 'inicio', 'fin', 'grado', 'isbn', 'libros'));
    }

    public function postReporteGeneralDescargar(Request $request) {
        $request->validate([
            'inicio'    => 'required',
            'fin'       => 'required',
            'isbn'      => 'sometimes',
            'grado'     => 'sometimes',
        ]);

        $usuario = Auth::user();

        $inicio = $request->inicio;
        $fin = $request->fin;
        $grado = $request->grado;

        $isbn = $request->get('isbn');

        $codigos = $usuario->permissions->pluck('name');

        $colegio = Colegio::whereIn('codigo', $codigos)->first();

        if( ! $colegio )
            return $this->sendError('No existe un colegio asociado a este usuario');

        $libro_id = null;

        if( $isbn )
            $libro_id = Libro::where('isbn', $isbn)->select('id')->first()->id;

        $libros = app('App\Http\Controllers\ReporteController')->generaInformacionReporteGeneralColegio($codigos, $inicio, $fin, $grado, $libro_id);

        if( ! $isbn )
            $isbn = 'TODOS LOS ISBN';

        $grado = 'TODOS LOS GRADOS';

        if( $request->grado )
            $grado = NivelColegio::where('id', $request->grado)->nombre;

        return Excel::download(
            new RolColegioReporteColegioExport(
                    $libros, $inicio, $fin, $colegio, $grado, $isbn
                ), "Reporte general colegio {$colegio->nombre}.xls"
            );
    }

    public function reporteAlumno() {
        $colegio = Colegio::whereIn('codigo',
            Auth::user()
                ->permissions
                ->pluck('name')
        )->first();

        return view('private.colegio.reporte-alumno')->with('colegio', $colegio);
    }

    /**
    * Este código fue copiado de ReporteController con la finalidad de poder adaptarlo y modificarlo si el cliente lo requiere
    * Si no fuera necesario eso, puede llamarse a la función original utilizando:
    * app('App\Http\Controllers\ReporteController')->getDataReporteGeneral($fecha_inicial, $fecha_final, $colegio_id, $nivel_id, $libro_id, $estado_pedido);
    */
    // private function generaInformacionReporteAlumnoColegio($fecha_inicial, $fecha_final, $colegio_id, $nivel_id, $libro_id, $estado_pedido) {
        // return Pedido::with([
        //     'user',
        //     'colegio',
        //     'direccion',
        //     'envio.paqueteria',
        //     'datosFactura',
        //     'resumenes' => function($query) use ($nivel_id, $libro_id) {
        //         $query->whereHas('paquete', function($query) use ($nivel_id) {
        //             $query->whereHas('nivel', function($query) use ($nivel_id) {
        //                 $query->when($nivel_id, function($query, $nivel_id) {
        //                     $query->where('niveles_colegios.id', $nivel_id);
        //                 });

        //                 $query->whereHas('seccion');
        //             });
        //         });

        //         $query->with('paquete', function($query) use ($nivel_id) {
        //             $query->with('nivel', function($query) use ($nivel_id) {
        //                 $query->when($nivel_id, function($query, $nivel_id) {
        //                     $query->where('niveles_colegios.id', $nivel_id);
        //                 });

        //                 $query->with('seccion');
        //             });
        //         });
        //         $query->withCount('detalles');
        //         $query->with('detalles', function($query) use ($libro_id) {
        //             $query->with('libro', function($query) use ($libro_id) {
        //                 $query->when($libro_id, function($query, $libro_id) {
        //                     $query->where('id', $libro_id);
        //                 });
        //             });
        //             $query->whereHas('libro', function($query) use ($libro_id) {
        //                 $query->when($libro_id, function($query, $libro_id) {
        //                     $query->where('id', $libro_id);
        //                 });
        //             });
        //         });
        //     },
        //     ])
        //     ->whereHas(
        //         'resumenes', function($query) use ($nivel_id, $libro_id) {
        //             $query->whereHas('paquete', function($query) use ($nivel_id) {
        //                 $query->whereHas('nivel', function($query) use ($nivel_id) {
        //                     $query->when($nivel_id, function($query, $nivel_id) {
        //                         $query->where('niveles_colegios.id', $nivel_id);
        //                     });

        //                     $query->whereHas('seccion');
        //                 });
        //             });
        //             $query->whereHas('detalles', function($query) use ($libro_id) {
        //                 $query->whereHas('libro', function($query) use ($libro_id) {
        //                     $query->when($libro_id, function($query, $libro_id) {
        //                         $query->where('id', $libro_id);
        //                     });
        //                 });
        //             });
        //         }
        //     )
        //     ->when($colegio_id, function($query, $colegio_id) {
        //         $query->where('colegio_id', $colegio_id);
        //     })
        //     ->when($estado_pedido, function ($query, $estado_pedido) {
        //         $query->where('status', $estado_pedido);
        //     })
        //     ->whereNotIn('status', ['CARRITO'])
        //     ->where('created_at', '>', "{$fecha_inicial} 00:00:00")
        //     ->where('created_at', '<', "{$fecha_final} 23:59:59")
        //     ->get();
    // }

    /**
     * Devuelve una vista con la tabla llena de información sobre el reporte general del colegio
     */
    public function postReporteAlumno(Request $request) {
        $request->validate([
            'inicio'  => 'required',
            'fin'     => 'required',
            'grado'   => 'sometimes',
            'libro'   => 'sometimes',
            'estado'  => 'sometimes'
        ]);

        $colegio = Colegio::whereIn('codigo',
            Auth::user()
                ->permissions
                ->pluck('name')
        )->first();

        $nivel = NivelColegio::find($request->grado);
        $libro = Libro::find($request->libro);

        $nombre_colegio = 'TODOS LOS COLEGIOS';
        $nombre_grado = 'TODOS LOS GRADOS';
        $nombre_isbn = 'TODOS LOS ISBN';
        $fecha_inicial = $request->inicio;
        $fecha_final = $request->fin;

        if($colegio)
            $nombre_colegio = $colegio->nombre;

        if($nivel)
            $nombre_grado = "{$nivel->nombre} {$nivel->seccion->nombre}";

        if($libro)
            $nombre_isbn = $libro->isbn;

        $estado_pedido = $request->get('estado', '');
        $pedidos = app('App\Http\Controllers\ReporteController')
            ->getDataReporteGeneral(
                $fecha_inicial,
                $fecha_final,
                $colegio->id,
                $request->grado,
                $request->libro,
                $estado_pedido
            );

        return view('private.reportes.colegio-alumno')
            ->with('inicio',     $fecha_inicial)
            ->with('colegio',    $colegio)
            ->with('fin',        $fecha_final)
            ->with('grado',      $nombre_grado)
            ->with('isbn',       $nombre_isbn)
            ->with('pedidos',    $pedidos)
            ->with('resultados', $pedidos->count());
    }

    public function postReporteAlumnoDescargar(Request $request) {
        $request->validate([
            'inicio'  => 'required',
            'fin'     => 'required',
            'grado'   => 'sometimes',
            'libro'   => 'sometimes',
            'estado'  => 'sometimes'
        ]);

        $colegio = Colegio::whereIn('codigo',
            Auth::user()
                ->permissions
                ->pluck('name')
        )->first();

        $nivel = NivelColegio::find($request->grado);
        $libro = Libro::find($request->libro);

        $nombre_colegio = 'TODOS LOS COLEGIOS';
        $nombre_grado = 'TODOS LOS GRADOS';
        $nombre_isbn = 'TODOS LOS ISBN';
        $fecha_inicial = $request->inicio;
        $fecha_final = $request->fin;

        if($colegio)
            $nombre_colegio = $colegio->nombre;

        if($nivel)
            $nombre_grado = "{$nivel->nombre} {$nivel->seccion->nombre}";

        if($libro)
            $nombre_isbn = $libro->isbn;

        $estado_pedido = $request->get('estado', '');
        $pedidos = app('App\Http\Controllers\ReporteController')
            ->getDataReporteGeneral(
                $fecha_inicial,
                $fecha_final,
                $colegio->id,
                $request->grado,
                $request->libro,
                $estado_pedido
            );

        return Excel::download(new RolColegioReporteGeneralExport(
            $pedidos, $fecha_inicial, $fecha_final, $nombre_colegio, $nombre_grado, $nombre_isbn, $estado_pedido
        ), 'Reporte por alumno.xlsx');
    }

    public function dashboard() {
        $colegio = Colegio::whereIn('codigo',
            Auth::user()
                ->permissions
                ->pluck('name')
        )->first();

        return view('private.colegio.dashboard')->with('colegio', $colegio);
    }

    public function conciliacion() {
        return view('private.admin.pedidos.conciliacion');
    }

    public function postConciliacion(Request $request) {
        $request->validate([
            'archivo' => 'required|file|mimes:xlsx,csv'
        ]);

        $folios = Excel::toCollection(new UploadPedidosEntregadosImport, $request->file('archivo'))
        ->first() // Primera hoja
        ->map(function($row) {
            if($folio = $row->first())
                return str_replace('PR', '', $folio);
        })
        ->whereNotNull();

        $pedidos_collect = Pedido::whereIn('folio', $folios)
        ->get()
        ->map(function($pedido) {
            $referencia = $pedido->referencia;
            $pago_exitoso = $pedido->pagos()->where('status', 'SUCCESS')->first();

            $pago_netpay = null;

            $errores = $pedido->tieneErrores();

            if($referencia)
                $pago_netpay = json_decode($referencia->raw);

            if($pedido->metodo_pago == 'TIENDA' && $pago_exitoso != null && $pago_exitoso->status == 'SUCCESS') {
                // Consulta estado del pago
                /**
                 * {
                 *  +"transactionTokenId": "e50b850c-06ee-413f-a05b-6657cfbcd24a"
                 *  +"status": "DONE"
                 *  +"orderId": "C4CAD4538D73951DE053250115AC8255"
                 *  +"merchantReferenceCode": "PED-1148-PR-2791-60c23ff5dbd51"
                 *  +"currency": null
                 *  +"amount": 1446.0
                 *  +"timeIn": "2021-06-10T16:38:14.000+0000"
                 *  +"timeOut": "2021-06-13T06:00:00.000+0000"
                 *  +"responseCode": null
                 *  +"responseMsg": null
                 *  +"authCode": null
                 *  +"spanRouteNumber": null
                 *  +"cardHolderName": "ANGELES MELISSA"
                 *  +"billToEmail": "melymtz09@hotmail.com"
                 *  +"bankName": null
                 *  +"paymentMethod": "BANORTE_PAY"
                 *  +"externalReference": "1591222215131427260"
                 *  +"expireDate": "2021-06-12T16:38:14.000+0000"
                 *  +"dayToExpirePayment": 2
                 *}
                 */
            }

            $registro_log = RegistroActividad::orWhere('description', 'like', "%{$pedido->serie}{$pedido->folio}%")->orWhere('description', 'like', "%{$pedido->serie}-{$pedido->folio}%")->get()
            ->map(function($registro) {
                $usuario = $registro->usuario;
                if($usuario)
                    return "({$registro->usuario->email}) {$registro->description}";

                return "(Usuario no identificado - id{$registro->id}) {$registro->description}";
            })->join(', ', ' y ');

            $transactionTokenId = '';
            if($pago_exitoso != null && in_array($pago_exitoso->origen, ['API', 'WEBHOOK', 'CRON'])) {
                $transactionTokenId = json_decode($pago_exitoso->raw)->transactionTokenId;
            }

            return [
                'folio'                  => "{$pedido->serie}{$pedido->folio}",
                'colegio'                => $pedido->colegio->nombre,
                'estado_pedido'          => $pedido->status,
                'total'                  => $pedido->total,
                'metodo_pago'            => $pedido->metodo_pago,
                'forma_pago'             => $pedido->forma_pago,
                'fecha_orden'            => $pedido->created_at->format('Y / m / d H:i:s'),

                'referencia'             => $referencia ? "'{$referencia->referencia}" : 'SIN REFERENCIA',
                'tipo'                   => $referencia ? $referencia->tipo : '',
                'coincide_1'             => $referencia ? (($pedido->metodo_pago == 'TIENDA' && $referencia->tipo == 'TIENDA') ? 'Si' : 'No') : 'SIN REFERENCIA', // El tipo de referencia coincide con el tipo de pedido

                'origen'                 => ($pago_exitoso ? $pago_exitoso->origen : ''),
                'amount'                 => ($pago_exitoso ? $pago_exitoso->amount : ''),
                'estado_pago'            => ($pago_exitoso ? $pago_exitoso->status : ''),
                'concide_2'              => $pago_exitoso ? (($pago_exitoso->origen == 'MANUAL' || $pago_exitoso->amount == $pedido->total) ? 'Si' : 'No') : '', // El monto coincide con el total del pedido
                'fecha de pago'          => $pago_exitoso ? $pago_exitoso->created_at->format('Y / m / d H:i:s') : '',
                'raw'                    => $pago_exitoso ? $transactionTokenId : '',

                'estado_pago_netpay'     => $pago_netpay ? $pago_netpay->status : '', // Status en netpay
                'merchan_reference_code' => $pago_netpay ? $pago_netpay->merchantReferenceCode : '',
                'pertenece_al_pedido'    => $pago_netpay ? (Str::contains($pago_netpay->merchantReferenceCode, "PED-{$pedido->id}") ? 'Si' : 'No') : '', // El folio del pedido coincide con el merchantreferencecode

                'cantidad_errores'       => $errores->count(),
                'descripcion'            => $errores->pluck('descripcion')->join(', ', ' y '),
                'log'                    => $registro_log, // Todas las coincidencias de ese pedido en el log
            ];
        });

        return Excel::download(
            new AnalisisPedidosExport($pedidos_collect),
            'Analisis de pedidos.xlsx');
    }
}
