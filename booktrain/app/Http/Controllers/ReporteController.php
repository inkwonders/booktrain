<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Libro;
use App\Models\Pedido;
use App\Models\Colegio;
use App\Models\NivelColegio;
use Illuminate\Http\Request;
use App\Models\SeccionColegio;
use App\Exports\ReporteCierreExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteColegioExport;
use App\Exports\ReporteGeneralExport;
use App\Models\Cupon;

class ReporteController extends Controller
{
    public function general() {
        return view('private.admin.reportes.general');
    }

    public function colegio() {
        return view('private.admin.reportes.colegio');
    }

    /**
     * Devuelve la vista con el selector del reporte de cierre
     */
    public function cierre() {
        return view('private.admin.reportes.cierre');
    }

    public function tableGeneral($fecha_inicial, $fecha_final, $colegio_id, $seccion_id, $libro_id, $estado_pedido) {
        $colegio = Colegio::find($colegio_id);
        $seccion = SeccionColegio::find($seccion_id);
        $libro = Libro::find($libro_id);

        $nombre_colegio = 'TODOS LOS COLEGIOS';
        $nombre_grado = 'TODOS LOS GRADOS';
        $nombre_isbn = 'TODOS LOS ISBN';

        if($colegio)
            $nombre_colegio = $colegio->nombre;

        if($seccion)
            $nombre_grado = $seccion->nombre;

        if($libro)
            $nombre_isbn = $libro->isbn;

        $pedidos = $this->getDataReporteGeneral($fecha_inicial, $fecha_final, $colegio_id, $seccion_id, $libro_id, $estado_pedido);

        $resultados = $pedidos->map(function($pedido) {
            return $pedido->resumenes->sum('detalles_count');
        })->sum();

        return view('private.reportes.admin_general')
            ->with(
                compact('pedidos', 'fecha_inicial', 'fecha_final', 'nombre_colegio', 'nombre_grado', 'nombre_isbn', 'resultados')
            );
    }

    public function excelGeneral($fecha_inicial, $fecha_final, $colegio_id, $seccion_id, $libro_id, $estado_pedido) {
        $colegio = Colegio::find($colegio_id);
        $seccion = SeccionColegio::find($seccion_id);
        $libro = Libro::find($libro_id);

        $nombre_colegio = 'TODOS LOS COLEGIOS';
        $nombre_grado = 'TODOS LOS GRADOS';
        $nombre_isbn = 'TODOS LOS ISBN';

        if($colegio)
            $nombre_colegio = $colegio->nombre;

        if($seccion)
            $nombre_grado = $seccion->nombre;

        if($libro)
            $nombre_isbn = $libro->isbn;

        $pedidos = $this->getDataReporteGeneral($fecha_inicial, $fecha_final, $colegio_id, $seccion_id, $libro_id, $estado_pedido);

        return Excel::download(new ReporteGeneralExport(
            $pedidos, $fecha_inicial, $fecha_final, $nombre_colegio, $nombre_grado, $nombre_isbn, $estado_pedido
        ), 'Reporte general.xlsx');
    }

    public function getDataReporteGeneral($fecha_inicial, $fecha_final, $colegio_id, $nivel_id, $libro_id, $estado_pedido) {
        return Pedido::with([
            'user',
            'colegio',
            'direccion',
            'envio.paqueteria',
            'datosFactura',
            'resumenes' => function($query) use ($nivel_id, $libro_id) {
                $query->whereHas('paquete', function($query) use ($nivel_id) {
                    $query->whereHas('nivel', function($query) use ($nivel_id) {
                        $query->when($nivel_id, function($query, $nivel_id) {
                            $query->where('niveles_colegios.id', $nivel_id);
                        });

                        $query->whereHas('seccion');
                    });
                });

                $query->with('paquete', function($query) use ($nivel_id) {
                    $query->with('nivel', function($query) use ($nivel_id) {
                        $query->when($nivel_id, function($query, $nivel_id) {
                            $query->where('niveles_colegios.id', $nivel_id);
                        });

                        $query->with('seccion');
                    });
                });
                $query->withCount('detalles');
                $query->with('detalles', function($query) use ($libro_id) {
                    $query->with('libro', function($query) use ($libro_id) {
                        $query->when($libro_id, function($query, $libro_id) {
                            $query->where('id', $libro_id);
                        });
                    });
                    $query->whereHas('libro', function($query) use ($libro_id) {
                        $query->when($libro_id, function($query, $libro_id) {
                            $query->where('id', $libro_id);
                        });
                    });
                });
            },
            ])
            ->whereHas(
                'resumenes', function($query) use ($nivel_id, $libro_id) {
                    $query->whereHas('paquete', function($query) use ($nivel_id) {
                        $query->whereHas('nivel', function($query) use ($nivel_id) {
                            $query->when($nivel_id, function($query, $nivel_id) {
                                $query->where('niveles_colegios.id', $nivel_id);
                            });

                            $query->whereHas('seccion');
                        });
                    });
                    $query->whereHas('detalles', function($query) use ($libro_id) {
                        $query->whereHas('libro', function($query) use ($libro_id) {
                            $query->when($libro_id, function($query, $libro_id) {
                                $query->where('id', $libro_id);
                            });
                        });
                    });
                }
            )
            ->when($colegio_id, function($query, $colegio_id) {
                $query->where('colegio_id', $colegio_id);
            })
            ->when($estado_pedido, function ($query, $estado_pedido) {
                $query->where('status', $estado_pedido);
            })
            ->whereNotIn('status', ['CARRITO'])
            ->where('created_at', '>', "{$fecha_inicial} 00:00:00")
            ->where('created_at', '<', "{$fecha_final} 23:59:59")
            ->get()
            ->sortByDesc('folio');
    }

    /**
     * Descarga de reporte Para colegio en la vista del administrador
     */
    public function excelColegio( $codigo_colegio, $fecha_inicial, $fecha_final, $grado_id, $libro_id ) {
        $colegio = Colegio::where('codigo', $codigo_colegio)->first();

        if( ! $libro_id )
            $isbn = 'TODOS LOS ISBN';
        else
            $isbn = Libro::find($libro_id)->isbn;

        $grado = 'TODOS LOS GRADOS';

        if( $grado_id )
            $grado = NivelColegio::where('id', $grado_id)->first()->nombre;

        $libros = $this->generaInformacionReporteGeneralColegio($codigo_colegio, $fecha_inicial, $fecha_final, $grado_id, $libro_id);

        return Excel::download(
            new ReporteColegioExport(
                    $libros, $fecha_inicial, $fecha_final, $colegio, $grado, $isbn
                ), "Reporte general colegio {$colegio->nombre}.xls"
            );
    }

    /**
     * Formato de tabla del reporte Para colegio en la vista del administrador
     */
    public function tableColegio($codigo_colegio, $inicio, $fin, $grado_id, $libro_id ) {
        $colegio = Colegio::where('codigo', $codigo_colegio)->first();

        if( ! $libro_id )
            $isbn = 'TODOS LOS ISBN';
        else
            $isbn = Libro::find($libro_id)->isbn;

        $grado = 'TODOS LOS GRADOS';

        if( $grado_id )
            $grado = NivelColegio::where('id', $grado_id)->first()->nombre;

        $libros = $this->generaInformacionReporteGeneralColegio($codigo_colegio, $inicio, $fin, $grado_id, $libro_id);

        return view('private.reportes.admin_colegio')
            ->with(compact('colegio', 'inicio', 'fin', 'grado', 'isbn', 'libros'));
    }

    public function generaInformacionReporteGeneralColegio($codigo_colegio, $inicio, $fin, $grado_id, $libro_id) {
        if(is_string($codigo_colegio) )
            $codigo_colegio = [$codigo_colegio];

        $colegio = Colegio::whereIn('codigo', $codigo_colegio)
        ->with([
            'paquetes' => function($query) use ($libro_id, $grado_id, $inicio, $fin) {
                $query->where('paquete_id', '!=', null);
                $query->with([
                    'libros' => function($query) use ($libro_id, $inicio, $fin) {
                        $query->where('libros.activo', 1);

                        $query->when($libro_id, function($query, $libro_id) {
                            $query->where('libros.id', $libro_id);
                        });

                        $query->whereHas('pedidos', function($query) use ($inicio, $fin) {
                            $query->when($inicio, function($query, $inicio) {
                                $query->where('pedidos.created_at', '>', "{$inicio} 00:00:00");
                            });
                            $query->when($fin, function($query, $fin) {
                                $query->where('pedidos.created_at', '<=', "{$fin} 23:59:59");
                            });

                        });
                    },
                    'nivel' => function($query) {}
                ]);
                $query->whereHas('nivel', function($query) use ($grado_id) {
                    $query->when($grado_id, function($query, $grado_id) {
                        $query->where('id', $grado_id);
                    });
                });

                $query->whereHas('libros.pedidos', function($query) use ($inicio, $fin) {
                    $query->where('pedidos.created_at', '>', "{$inicio} 00:00:00");
                    $query->where('pedidos.created_at', '<=', "{$fin} 23:59:59");
                });
            }
        ])
        ->first();

        /**
         * Sin paquete:
         * - stock
         * - punto_reorden
         * - bajo_pedido
         *
         * Con paquete:
         * - precio
         * - obligatorio
         * - activo
         */

        // Obtenemos todos los paquetes del colegio
        $libros = $colegio->paquetes
            ->map(function($paquete) use ($colegio, $grado_id, $inicio, $fin) {
                // Obtenemos todos los libros de los paquetes
                return $paquete->libros->map(function($libro) use ($colegio, $grado_id, $paquete, $inicio, $fin) {

                    $libro_colegio_base = $colegio->libros()->wherePivot('paquete_id', null)->where('libros.id', $libro->id)->first();

                    $detalles = $libro->detalle()
                        ->with('resumen', function($query) use ($colegio, $paquete, $grado_id, $inicio, $fin) {
                            $query->where('paquete_id', $paquete->id);

                            $query->with('pedido', function($query) use ($colegio, $inicio, $fin) {
                                $query->where('colegio_id', $colegio->id);
                                $query->whereNotIn('status', ['CARRITO', 'CANCELADO', 'PROCESADO', 'PAGO EN REVISION', 'PAGO RECHAZADO']);
                                $query->where('pedidos.created_at', '>', "{$inicio} 00:00:00");
                                $query->where('pedidos.created_at', '<=', "{$fin} 23:59:59");

                            });
                        })
                        ->whereHas('resumen', function($query) use ($colegio, $paquete, $inicio, $fin) {
                            $query->where('paquete_id', $paquete->id);

                            $query->whereHas('pedido', function($query) use ($colegio, $inicio, $fin) {
                                $query->where('colegio_id', $colegio->id);
                                $query->whereNotIn('status', ['CARRITO', 'CANCELADO', 'PROCESADO', 'PAGO EN REVISION', 'PAGO RECHAZADO']);
                                $query->where('pedidos.created_at', '>', "{$inicio} 00:00:00");
                                $query->where('pedidos.created_at', '<=', "{$fin} 23:59:59");
                            });
                        })
                        ->get();

                    $devoluciones = $detalles->where('devuelto', 1);

                    $libros_vendidos = $detalles->sum('cantidad');

                    $libros_entregados = $detalles->filter(function($detalle) {
                            return in_array($detalle->resumen->pedido->status, ['ENVIADO','ENTREGADO','ENVIO LIQUIDADO','ENTREGA LIQUIDADA']);
                        })
                        ->where('devuelto', 0);

                    $libros_venta_mostrador = $detalles
                        ->filter(function($detalle) {
                            return in_array($detalle->resumen->pedido->metodo_pago_id, [6, 7, 8]);
                        })
                        ->where('devuelto', 0);

                    $valor_pedido = $detalles->reduce(function($carry, $item) {
                        return $carry + ($item->precio_libro_original * $item->cantidad);
                    }, 0);

                    $valor_devuelto = $devoluciones
                        ->map(function($item) {
                            return $item->precio_libro_original * $item->cantidad;
                        })->sum();

                    $valor_total_venta = $detalles->filter(function($detalle) {
                        return  in_array($detalle->resumen->pedido->status, ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']) ||
                                in_array($detalle->resumen->pedido->metodo_pago_id, [6, 7, 8]);
                    })
                    ->reduce(function($carry, $item) {
                        return $carry + ($item->cantidad * $item->precio_libro_original);
                    }, 0);

                    $valor_real_venta = $valor_total_venta - $valor_devuelto;

                    return (Object) [
                        'id'               => $libro->id,
                        'paquete_id'       => $paquete->id,
                        'isbn'             => $libro->isbn,
                        'titulo'           => $libro->nombre,
                        'obligatorio'      => $libro->pivot->obligatorio,
                        'grado'            => $paquete->nivel->nombre,
                        'seccion'          => $paquete->nivel->seccion->nombre,
                        'precio'           => $libro->pivot->precio,
                        'pedidos'          => $libros_vendidos,
                        'reorden'          => $libro_colegio_base->pivot->punto_reorden,

                        /**
                         * Pedidos: Se refiere al numero de libros que fueron vendidos
                         * Reorden: Punto de reorden
                         * Entrega cantidad: # de libros que se han entregado
                         * Venta mostrador: Libros que se vendieron con venta movil
                         * Devoluciones cantidad: # de libros que se han devuelto
                         * Venta real (# libros): Entrega cantidad + Venta mostrador - devoluciones cantidad
                         *
                         * Pedidos cantidad + Venta mostrador - devolucion
                         *
                         * Valor pedido ($): Entrega cantidad + Venta mostrador (expresado en $, hay que sacar el valor de los libros del detalle del resumen del pedido)
                         * Valor de devolución ($): Devoluciones cantidad (expresado en $, hay que sacar el valor de los libros del detalle del resumen del pedido)
                         * Valor real de venta ($): Son solo los libros vendidos (no devueltos) en venta de mostrador (venta movil) mas los libros que ya fueron enviados
                         *   Entrega cantidad + Venta mostrador - devoluciones cantidad (hay que sacar el valor de los libros del detalle del resumen del pedido)
                         */

                        'entrega'          => $libros_entregados->sum('cantidad'),
                        'mostrador'        => $libros_venta_mostrador->sum('cantidad'),
                        'devoluciones'     => $devoluciones->sum('cantidad'),// # de libros que se han devuelto
                        // 'venta_real'       => $libros_venta_mostrador->sum('cantidad') + $libros_entregados->sum('cantidad') - $devoluciones->sum('cantidad'),
                        'venta_real'       => $libros_vendidos - $devoluciones->sum('cantidad'),
                        'valor_pedido'     => $valor_pedido,
                        'valor_devolucion' => $valor_devuelto,
                        'valor_venta_real' => $valor_real_venta,
                    ];
                });
        })->collapse()->unique();

        $libros_sin_pedidos = $colegio->paquetes()
            ->with('libros', function($query) use ($libros, $libro_id) {
                $query->whereNotIn('libros.id', $libros->pluck('id'));

                $query->when($libro_id, function($query, $libro_id) {
                    $query->where('libros.id', $libro_id);
                });
            })
            ->get()
            ->map(function($paquete) use ($colegio) {
                return $paquete->libros->map(function($libro) use ($paquete, $colegio) {

                    $libro_colegio_base = $colegio->libros()->wherePivot('paquete_id', null)->where('libros.id', $libro->id)->first();

                    return (Object) [
                        'id'               => $libro->id,
                        'isbn'             => $libro->isbn,
                        'titulo'           => $libro->nombre,
                        'obligatorio'      => $libro->pivot->obligatorio,
                        'grado'            => $paquete->nivel->nombre,
                        'seccion'          => $paquete->nivel->seccion->nombre,
                        'precio'           => $libro->pivot->precio,
                        'pedidos'          => 0,
                        'reorden'          => $libro_colegio_base->pivot->punto_reorden,
                        'entrega'          => 0,
                        'mostrador'        => 0,
                        'devoluciones'     => 0,
                        'venta_real'       => 0,
                        'valor_pedido'     => 0,
                        'valor_devolucion' => 0,
                        'valor_venta_real' => 0,
                    ];
                });

        })->collapse()->unique();

        $libros = $libros->concat($libros_sin_pedidos);

        return $libros;
    }

    /*
     * Reporte del cierre del día
     */
    public function getReporteCierreDiaValues(Request $request) {
        $request->validate([
            //'colegio' => 'required|exists:colegios,id',
            'colegio_id' => 'required',
            'fin' => 'required'
        ]);

        $colegio = Colegio::find($request->colegio_id);

        //Obtener los pedidos que ya están pagados de un día específico y opcionalmente especificar un colegio, los pedidos pagados tienen un registro de pago con un estado success
        $inicio = $request->fin;
        $fin = $request->fin;

        $pedidos = Pedido::whereHas('pagos', function($query) use ($inicio, $fin) {
            $query->where('status', 'SUCCESS');
            $query->whereBetween('created_at', [$inicio . ' 00:00:00' , $fin . ' 23:59:59']);
        })
        ->when($request->colegio_id, function($query, $colegio_id) {
            $query->where('colegio_id', $colegio_id);
        })
        ->with('cupon')
        ->get();

        $cupones = [];

        foreach($pedidos as $pedido){
            if(!is_null($pedido->cupon)){
                array_push($cupones, $pedido->cupon->valor);
            }
        }

        $cajas = Colegio::when($request->colegio_id, function($query, $colegio_id) {
            $query->where('id', $colegio_id);
        })
        ->with('cajas', function($query) use ($inicio, $fin) {
            $query->where('status', 'CERRADA');
            $query->whereHas('pedidos', function($query) use ($inicio, $fin) {
                $query->whereIn('status', ['PAGADO','ENVIADO','ENTREGADO','ENVIO PARCIAL','ENVIO LIQUIDADO','ENTREGA PARCIAL','ENTREGA LIQUIDADA']);
                $query->whereBetween('pedidos.created_at', [$inicio . ' 00:00:00' , $fin . ' 23:59:59']);
            });
        })
        ->with('pedidos')
        ->get()
        ->pluck('cajas')
        ->collapse()
        ->sortBy('updated_at');



        $pedidos_separados = $pedidos->partition(function ($pedido) {
            return $pedido->status == 'CANCELADO';
        });

        $pedidos_cancelados = $pedidos_separados[0];

        $pedidos = $pedidos_separados[1];

        $pedidos_metodos_pago = $pedidos->groupBy('metodo_pago');


        //TODO: devoluciones

        //metodo_pago: 'CREDITO','DEBITO','AMEX','BANCO','TIENDA','EFECTIVO','TERMINAL','TRANSFERENCIA'

        //tarjeta
        $pedidos_tarjeta = collect();
        $pedidos_tarjeta_credito = collect();
        $pedidos_tarjeta_debito = collect();
        $pedidos_tarjeta_amex = collect();

        if ($pedidos_metodos_pago->has('CREDITO')) {
            $pedidos_tarjeta_credito = $pedidos_metodos_pago["CREDITO"];
            $pedidos_tarjeta = $pedidos_tarjeta->concat($pedidos_tarjeta_credito);
        }

        if ($pedidos_metodos_pago->has('DEBITO')) {
            $pedidos_tarjeta_debito = $pedidos_metodos_pago["DEBITO"];
            $pedidos_tarjeta = $pedidos_tarjeta->concat($pedidos_tarjeta_debito);
        }

        if ($pedidos_metodos_pago->has('AMEX')) {
            $pedidos_tarjeta_amex = $pedidos_metodos_pago["AMEX"];
            $pedidos_tarjeta = $pedidos_tarjeta->concat($pedidos_tarjeta_amex);
        }

        //pago con terminal bancaria
        $pedidos_terminal = collect();
        if ($pedidos_metodos_pago->has('TERMINAL')) {
            $pedidos_terminal = $pedidos_metodos_pago["TERMINAL"];
        }

        //pago en practicaja
        $pedidos_practicaja = collect();
        if ($pedidos_metodos_pago->has('BANCO')) {
            $pedidos_practicaja = $pedidos_metodos_pago["BANCO"];
        }

        //pago con tienda de conveniencia
        $pedidos_tienda = collect();
        if ($pedidos_metodos_pago->has('TIENDA')) {
            $pedidos_tienda = $pedidos_metodos_pago["TIENDA"];
        }

        //total efectivo
        $pedidos_efectivo = collect();
        if ($pedidos_metodos_pago->has('EFECTIVO')) {
            $pedidos_efectivo = $pedidos_metodos_pago["EFECTIVO"];
        }

        // pago con transferencia
        $pedidos_transferencia = collect();
        if ($pedidos_metodos_pago->has('TRANSFERENCIA')) {
            $pedidos_transferencia = $pedidos_metodos_pago["TRANSFERENCIA"];
        }

        $devoluciones = $pedidos->sum('descuento_devolucion');
        $total_efectivo = $pedidos_efectivo->count() > 0 ? $pedidos_efectivo->sum('total') : 0;
        $pago_tarjeta = $pedidos_tarjeta->count() > 0 ? $pedidos_tarjeta->sum('total') : 0;
        $pago_terminal = $pedidos_terminal->count() > 0 ? $pedidos_terminal->sum('total') : 0;
        $pago_tienda = $pedidos_tienda->count() > 0 ? $pedidos_tienda->sum('total') : 0;
        $pago_practicaja = $pedidos_practicaja->count() > 0 ? $pedidos_practicaja->sum('total') : 0;
        $pago_transferencia = $pedidos_transferencia->count() > 0 ? $pedidos_transferencia->sum('total') : 0;

        $pago_efectivo = $total_efectivo + $pago_tarjeta + $pago_terminal + $pago_tienda + $pago_practicaja + $pago_transferencia;

        //$cajas = $colegio? $colegio->cajas()->where('status', 'CERRADA')->get(): collect();
        $total_cajas = 0;
        $b1000 = 0;
        $b500 = 0;
        $b200 = 0;
        $b100 = 0;
        $b50 = 0;
        $b20 = 0;
        $monto_monedas = 0;
        $conteo_monedas = 0;
        $suma_cupon = 0;

        foreach ($cajas as $caja) {
            $total_cajas += ($caja->operaciones->last()? $caja->operaciones->last()->total : 0);

            // dd($caja->operaciones);
            $b1000 += $caja->operaciones->last()->billete_1000;
            $b500 += $caja->operaciones->last()->billete_500;
            $b200 += $caja->operaciones->last()->billete_200;
            $b100 += $caja->operaciones->last()->billete_100;
            $b50 += $caja->operaciones->last()->billete_50;
            $b20 += $caja->operaciones->last()->billete_20;
            $monto_monedas += $caja->operaciones->last()->monto_monedas;
            $conteo_monedas += $caja->operaciones->last()->conteo_monedas;
            $cupon = $caja->pedidos;
        }
        if($cupones == null || $cupones == ""){
            // dd("sin cupones");
                $descuento = 0;
        }
        else{
            foreach($cupones as $cupon){

                $descuento = ((float)$cupon * 100) / $total_cajas; // Regla de tres
            }
        }


        // $descuento = round($descuento, 0);  // Quitar los decimales

        return [
            'colegio_id' => $request->colegio_id,
            'colegio' => $request->colegio,
            'fecha_raw' => $request->fin,
            'cajas' => $cajas,
            'nombre_colegio' => $colegio ? $colegio->nombre : "TODOS LOS COLEGIOS",
            'logo_colegio' => $colegio ? $colegio->logo : "http://booktrain.com.mx/assets/img/svg/logo_principal.svg",
            'fecha' => Carbon::create($request->fin)->format('d/m/Y'),

            'pago_efectivo' => $pago_efectivo,

            'devoluciones' => $devoluciones,
            'total_efectivo' => $total_efectivo,
            'pago_tarjeta' => $pago_tarjeta,
            'pago_terminal' => $pago_terminal,
            'pago_tienda' => $pago_tienda,
            'pago_practicaja' => $pago_practicaja,
            'pago_transferencia' => $pago_transferencia,

            'total' => $pedidos->sum('total') - $devoluciones,

            'total_libros' => $pedidos->pluck('libros')->collapse()->count() - $pedidos->sum('libros_devueltos'),

            'total_cajas' => $total_cajas,

            'b1000' => $b1000,
            'b500' => $b500,
            'b200' => $b200,
            'b100' => $b100,
            'b50' => $b50,
            'b20' => $b20,
            'monto_monedas' => $monto_monedas,
            'conteo_monedas' => $conteo_monedas,
            'descuento' => $descuento,

        ];
    }

    /*
     * Reporte de cierre del día
     */
    public function getReporteCierreDia(Request $request)
    {
        $data = $this->getReporteCierreDiaValues($request);
        return view('private.reportes.cierre_del_dia')->with($data);
    }

    /*
     * Impresión del reporte de cierre del día
     */
    public function printReporteCierreDelDia(Request $request)
    {
        $data = $this->getReporteCierreDiaValues($request);

        $pdf = \PDF::loadView('private.reportes.print_cierre_del_dia', $data);

        $pdf->getDomPDF()->set_options(['dpi' => 150, 'defaultFont' => 'arial', 'enable_php' => true]);

        return $pdf->stream('cierre_del_dia.pdf');
    }

    /**
     * Devuelve una vista con una tabla que muestra el reporte de cierre
     */
    public function tableCierre( $codigo_colegio, $inicio, $fin, $grado_id, $libro_id ) {
        $colegio = Colegio::where('codigo', $codigo_colegio)->first();

        if( ! $libro_id )
            $isbn = 'TODOS LOS ISBN';
        else
            $isbn = Libro::find($libro_id)->isbn;

        $grado = 'TODOS LOS GRADOS';

        if( $grado_id )
            $grado = NivelColegio::where('id', $grado_id)->first()->nombre;

        $libros = $this->generaInformacionReporteGeneralColegio($codigo_colegio, $inicio, $fin, $grado_id, $libro_id);

        return view('private.reportes.cierre')
            ->with(compact('colegio', 'inicio', 'fin', 'grado', 'isbn', 'libros'));
    }

    /**
     * Devuelve un archivo descargable en formato .xlsx con la información del cierre de un colegio
     */
    public function excelCierre( $codigo_colegio, $inicio, $fin, $grado_id, $libro_id ) {
        $colegio = Colegio::where('codigo', $codigo_colegio)->first();

        if( ! $libro_id )
            $isbn = 'TODOS LOS ISBN';
        else
            $isbn = Libro::find($libro_id)->isbn;

        $grado = 'TODOS LOS GRADOS';

        if( $grado_id )
            $grado = NivelColegio::where('id', $grado_id)->first()->nombre;

        $libros = $this->generaInformacionReporteGeneralColegio($codigo_colegio, $inicio, $fin, $grado_id, $libro_id);

        return Excel::download(new ReporteCierreExport(
                $libros, $inicio, $fin, $colegio, $grado, $isbn
            ), 'Reporte de cierre.xlsx');
    }

}
