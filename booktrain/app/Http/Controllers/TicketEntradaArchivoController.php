<?php

namespace App\Http\Controllers;

use App\DataTables\TicketEntradaArchivoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTicketEntradaArchivoRequest;
use App\Http\Requests\UpdateTicketEntradaArchivoRequest;
use App\Repositories\TicketEntradaArchivoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TicketEntradaArchivoController extends AppBaseController
{
    /** @var  TicketEntradaArchivoRepository */
    private $ticketEntradaArchivoRepository;

    public function __construct(TicketEntradaArchivoRepository $ticketEntradaArchivoRepo)
    {
        $this->ticketEntradaArchivoRepository = $ticketEntradaArchivoRepo;
    }

    /**
     * Display a listing of the TicketEntradaArchivo.
     *
     * @param TicketEntradaArchivoDataTable $ticketEntradaArchivoDataTable
     * @return Response
     */
    public function index(TicketEntradaArchivoDataTable $ticketEntradaArchivoDataTable)
    {
        return $ticketEntradaArchivoDataTable->render('ticket_entrada_archivos.index');
    }

    /**
     * Show the form for creating a new TicketEntradaArchivo.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_entrada_archivos.create');
    }

    /**
     * Store a newly created TicketEntradaArchivo in storage.
     *
     * @param CreateTicketEntradaArchivoRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketEntradaArchivoRequest $request)
    {
        $input = $request->all();

        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->create($input);

        Flash::success('Ticket Entrada Archivo saved successfully.');

        return redirect(route('ticketEntradaArchivos.index'));
    }

    /**
     * Display the specified TicketEntradaArchivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            Flash::error('Ticket Entrada Archivo not found');

            return redirect(route('ticketEntradaArchivos.index'));
        }

        return view('ticket_entrada_archivos.show')->with('ticketEntradaArchivo', $ticketEntradaArchivo);
    }

    /**
     * Show the form for editing the specified TicketEntradaArchivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            Flash::error('Ticket Entrada Archivo not found');

            return redirect(route('ticketEntradaArchivos.index'));
        }

        return view('ticket_entrada_archivos.edit')->with('ticketEntradaArchivo', $ticketEntradaArchivo);
    }

    /**
     * Update the specified TicketEntradaArchivo in storage.
     *
     * @param  int              $id
     * @param UpdateTicketEntradaArchivoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketEntradaArchivoRequest $request)
    {
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            Flash::error('Ticket Entrada Archivo not found');

            return redirect(route('ticketEntradaArchivos.index'));
        }

        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->update($request->all(), $id);

        Flash::success('Ticket Entrada Archivo updated successfully.');

        return redirect(route('ticketEntradaArchivos.index'));
    }

    /**
     * Remove the specified TicketEntradaArchivo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketEntradaArchivo = $this->ticketEntradaArchivoRepository->find($id);

        if (empty($ticketEntradaArchivo)) {
            Flash::error('Ticket Entrada Archivo not found');

            return redirect(route('ticketEntradaArchivos.index'));
        }

        $this->ticketEntradaArchivoRepository->delete($id);

        Flash::success('Ticket Entrada Archivo deleted successfully.');

        return redirect(route('ticketEntradaArchivos.index'));
    }
}
