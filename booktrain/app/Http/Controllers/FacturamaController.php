<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Mail;

use Facturama\Exception\RequestException;

use App\Models\Pedido;
use App\Models\UsoCFDI;

class FacturamaController extends Controller
{


     private function getInstance()
    {

        try {


            Log::debug('FacturamaController@getInstance', ['Conexion con Facturama']);

            $production=boolval(env('FACTURAMA_DEVELOPMENT'));

            $facturama = new \Facturama\Client(env('FACTURAMA_USERNAME'), env('FACTURAMA_PASSWORD'));

            if($production){

                $url_prod=env('FACTURAMA_INVOICE_URL');

                 $facturama->setApiUrl($url_prod);

            }

                //   dd($facturama);

            return $facturama;

        } catch (RequestException $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);

        } catch (\Exception $e) {

            Log::debug('FacturamaController@getInstance', ['Facturama no está hábilitado']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInstance ERROR on the transaction Exception: ', $array);

             return response()->json($array);

            // throw new Exception("Facturama no está hábilitado", 1);
        }
    }



    public function generateCfdi($pedido_id){

        Log::debug('FacturamaController@generateCfdi', ['Creacion de factura:Obtenemos datos del pedido '=> $pedido_id]);


        // $request->validate([
        //     'pedido_id'            => 'required|exists:pedidos,id',
        // ]);
        // $pedido_id=$request->pedido_id;


                if(empty($pedido_id)){

                    Log::error('FacturamaController@generateCfdi No se encontro pedido ',$pedido_id);

                    return response()->json(
                        [
                            'success' => false,
                            'msg:'    => 'No se encontro pedido '.$pedido_id,
                        ]
                    );



                }

        try {


            DB::beginTransaction();

            //datos del pedido

            $pedido = Pedido::find($pedido_id);
            $libros = $pedido->libros;


            // dd($libros);

            $items=[];

            foreach ($libros as $libro) {


                  //se descomenta cuando si lleve impuesto
                // $tasa_impuesto=0.16;
                // $precio=$libro->precio;
                // $precio_base=round($precio/1.16,2,PHP_ROUND_HALF_EVEN);
                // $iva=round($precio_base* $tasa_impuesto,2,PHP_ROUND_HALF_EVEN);


                $precio=$libro->precio;
                $precio_base=$precio;


               $array_libro=[
                "ProductCode"=> $libro->clave_producto,
                "Description"=> $libro->nombre,
                "UnitCode"=> "H87",
                "Quantity"=> $libro->cantidad,
                "UnitPrice"=>$precio_base,
                "Subtotal"=> $precio_base,
                "TaxObject" =>  $libro->objeto_impuesto,

                //se descomenta cuando si lleve impuesto
            //     "Taxes"=> [
            //         [
            //         "Total"=> $iva,
            //         "Name"=> "IVA",
            //         "Base"=> $precio_base,
            //         "Rate"=>  $tasa_impuesto,
            //         "IsRetention"=> false
            //     ]
            // ],

            "Total"=> $precio

               ];



                array_push($items, $array_libro);
            }


            $facturama = $this->getInstance();
            $url_logo = "https://booktrain.com.mx/assets/img/svg/logo_principal.png";
            $hoy = date("Y-m-d H:i:s");

            //  dd($facturama);

            $payment_form = explode("-", $pedido->datosFactura->forma_pago);
            $payment_method = $pedido->datosFactura->metodo_pago;
            $folio = $pedido->folio;
            $expedition_place = env('FACTURAMA_EXPEDITIONPLACE');
            $fiscal_regime = env('FACTURAMA_FISCAL_REGIME');
            $rfc = env('FACTURAMA_RFC');
            $name = str_replace("-", " ", env('FACTURAMA_NAME'));

            //Receiver
            //   dd($pedido->datosFactura);

            $rfc_receiver = $pedido->datosFactura->rfc;
            $cfdi_use = UsoCFDI::find($pedido->datosFactura->id_cfdi);
            $name_receiver = strtoupper($pedido->datosFactura->razon_social);
            $fiscal_regime_receiver = $pedido->datosFactura->regimen_fiscal;
            $tax_zip_code = $pedido->datosFactura->cp;
            $street = $pedido->datosFactura->calle;
            $exterior_number = $pedido->datosFactura->num_exterior;
            $interior_number = $pedido->datosFactura->num_interior;
            $neighborhood = $pedido->datosFactura->colonia;
            $municipality = $pedido->datosFactura->municipio;
            $state = $pedido->datosFactura->estado;
            $email=$pedido->datosFactura->correo;


            $params = [
                "CfdiType" => "I",
                "PaymentForm" => $payment_form[0],
                "PaymentMethod" => $payment_method,
                "ExpeditionPlace" => $expedition_place,
                "Date" => $hoy,
                "Folio" => $folio,
                "LogoUrl" => "$url_logo",
                "Issuer" => [
                    "FiscalRegime" => $fiscal_regime,
                    "Rfc" => $rfc,
                    "Name" => $name
                ],
                "Receiver" => [
                    "Rfc" => $rfc_receiver,
                    "CfdiUse" => $cfdi_use->codigo,
                    "Name" => $name_receiver,
                    "FiscalRegime" => $fiscal_regime_receiver,
                    "TaxZipCode" => $tax_zip_code,
                    "Address" => [
                        "Street" => $street,
                        "ExteriorNumber" => $exterior_number,
                        "InteriorNumber" => $interior_number,
                        "Neighborhood" => $neighborhood,
                        "ZipCode" => $tax_zip_code,
                        "Municipality" => $municipality,
                        "State" => $state,
                        "Country" => "México"
                    ]
                ],

                "Items" => $items

            ];

            $ruta = env('FACTURAMA_INVOICE_VERSION');

            // dd($ruta);

            try {

                $result = $facturama->post($ruta, $params);


                $invoice_data = json_encode($result);
                $invoice_data = json_decode($invoice_data, true);

                $invoice_id = $invoice_data['Id'];

                //  dd($invoice_id);
                //  printf('<pre>%s<pre>', var_export($result, true));

            //actualizamos en el pedido el id de la factura
            $pedido->facturama_invoice_id=$invoice_id;
            $pedido->facturama_status="active";
            $pedido->save();

            //enviamos x correo la factura
            $this->sendInvoiceEmail($invoice_id,$email);

            DB::commit();

                Log::debug('FacturamaController@generateCfdi', ['Factura creada con id ' . $invoice_id . " asignada al pedido " . $pedido->id]);

                return response()->json([
                    'success' => true,
                    'msg' => 'Factura creada con id ' . $invoice_id . " asignada al pedido " . $pedido->id,
                    'invoice_id' => $invoice_id
                ], 201);
            } catch (RequestException $e) {

                // dd($e);
                // dd($e->getPrevious()->getMessage());

                if(!empty($e->getPrevious())){

                $error=$e->getPrevious()->getMessage();
                }else{

                    $error= $e->getMessage();

                }


                $pedido->datosFactura->facturama_error = $error;
                $pedido->datosFactura->save();

                DB::commit();

                Log::error('FacturamaController@generateCfdi', ['Error al generar factura para el pedido '.$pedido->id]);

                //enviamos correo d error

                 $this->errorEmail($pedido->id);

                $array = [
                    'success' => false,
                    'ERROR on the transaction Exception: ' => $e->getMessage(),
                    'invoice_error' => $error,
                ];

                Log::error('FacturamaController@generateCfdi ERROR on the transaction Exception: ', $array);


                return response()->json($array);
            } //catch envio peticion


        } catch (\Exception $e) {


            Log::debug('FacturamaController@generateCfdi', ['Error al generar la fatura']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@generateCfdi ERROR on the transaction Exception: ', $array);

            return response()->json($array);
        }
    } //generateCfdi




    public function addCsd()
    {


        $cer = storage_path("app\public\csd\OAVF5710046J6.cer");
        $key = storage_path("app\public\csd\OAVF5710046J6.key");


        $obten_cer = file_get_contents($cer);


        $obten_key = file_get_contents($key);

        $base64_cer = base64_encode($obten_cer);
        $base64_key = base64_encode($obten_key);


        return response()->json([
            'success' => true,
            'base64_cer' => $base64_cer,
            'base64_key' => $base64_key
        ], 201);
    }



    public function getInvoiceFiles(Request $request)
    {



        Log::debug('FacturamaController@generateCfdi', ['Obtenemos datos' => $request->all()]);


                        $request->validate([
                            'facturama_id'  => 'required|exists:pedidos,facturama_invoice_id',
                             'format'       =>  'in:xml,pdf,acuse'
                        ]);


        try {

            $id = $request->facturama_id;
            $format = $request->format;


            if($format=='acuse'){
                $dir="acuse/";
                $format='pdf';
                $nombre_archivo=$id.'_acuse.'.$format;

             }else{  $dir="cfdi/";  $nombre_archivo=$id.'.'.$format; }


            $facturama= $this->getInstance();

            $type = env('FACTURAMA_INVOICE_TYPE');
            $params = [];
            $result = $facturama->get($dir.$format.'/'.$type.'/'.$id, $params);

            //guarda el archivo
            Storage::disk('public')->put('xml_pdf/'.$nombre_archivo, base64_decode(end($result)));




           } catch (RequestException $e) {


            Log::debug('FacturamaController@getInvoiceFiles', ['Error al descargar el archivo']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@getInvoiceFiles ERROR on the transaction Exception: ', $array);

            return response()->json($array);



         }




     }



     public function deleteInvoice(Request $request){


        Log::debug('FacturamaController@deleteInvoice', ['Obtenemos datos'=> $request->all()]);


                $request->validate([
                    'pedido_id'     => 'required|exists:pedidos,id',
                    // 'facturama_id'  => 'required|exists:pedidos,facturama_invoice_id',
                    'motive'        => 'required|numeric'
                ]);


         try{

            DB::beginTransaction();

            $pedido_id=$request->pedido_id;
            $pedido=Pedido::find($pedido_id);
            $id=$pedido->facturama_invoice_id;
            $facturama= $this->getInstance();
            $motive=$request->motive;

            $params =
            [
                'motive'=>$motive,
                'uuidReplacement'=>'null'
            ];

            $ruta=env('FACTURAMA_INVOICE_CANCELLATION');
            $result = $facturama->delete($ruta.$id, $params);

            //actualizamos el pedido

            $pedido->facturama_status=$result->Status;

            $invoice_data=json_encode($result);


            $pedido->facturama_cancellation_response=$invoice_data;
            $pedido->save();

            DB::commit();

            return response()->json(['success' => true, 'msg' => 'Factura cancelada'], 200);


            } catch (RequestException $e) {


            Log::debug('FacturamaController@deleteInvoice', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@deleteInvoice ERROR on the transaction Exception: ',$array);

            return response()->json($array);


         }


     }


     public function sendInvoiceEmail($id,$correo){


        Log::debug('FacturamaController@sendInvoiceEmail', ['Enviamos por correo la factura'=> $id]);


         try{

            $body = [];
            $params = [
            'cfdiType' => env('FACTURAMA_INVOICE_TYPE'),
            'cfdiId' => $id,
            'email' => $correo,
            ];

            $facturama= $this->getInstance();

          $result = $facturama->post('cfdi', $body, $params);


        return response()->json(['success' => true, 'msg' => 'Factura enviada'], 200);


            } catch (RequestException $e) {


            Log::debug('FacturamaController@deleteInvoice', ['Error al descargar el archivo']);

            $array=[
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@deleteInvoice ERROR on the transaction Exception: ',$array);

            return response()->json($array);


         }


     }


     public function errorEmail($pedido_id){

        Log::debug('FacturamaControl
        ler@errorEmail', ['Enviamos por correo los errrores de la factura, del pedido'=> $pedido_id]);


        try {

            $pedido=Pedido::find($pedido_id);
            $email=trim($pedido->datosFactura->correo);
            $error=$pedido->datosFactura->facturama_error;
            $folio=$pedido->serie.$pedido->folio;
            $img_logo = public_path()."/assets/img/logo.png";

            Mail::send('emails.facturama_error',['email' => $email, 'folio' => $folio,'msg_error'=>$error,'img_logo' => $img_logo], function($message)  use ($email,$folio){
                $message->from('noreply@booktrain.com.mx');
                $message->to($email);
                $message->subject('Error al generar Factura del pedido '.$folio);
            });


        } catch (\Exception $e) {



            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@errorEmail ERROR on the transaction Exception: ', $array);

             return response()->json($array);

        }




     }


    //para mandarlas a timbrar desde postman cuando falle algun pedido

     public function generateCfdiPost(Request $request){

        Log::debug('FacturamaController@generateCfdi', ['Creacion de factura:Obtenemos datos del pedido '=> $request->pedido_id]);


        $request->validate([
            'pedido_id'            => 'required|exists:pedidos,id',
        ]);


        $pedido_id=$request->pedido_id;


                if(empty($pedido_id)){

                    Log::error('FacturamaController@generateCfdi No se encontro pedido ',$pedido_id);

                    return response()->json(
                        [
                            'success' => false,
                            'msg:'    => 'No se encontro pedido '.$pedido_id,
                        ]
                    );



                }

        try {


            DB::beginTransaction();

            //datos del pedido

            $pedido = Pedido::find($pedido_id);
            $libros = $pedido->libros;


            // dd($libros);

            $items=[];

            foreach ($libros as $libro) {


                  //se descomenta cuando si lleve impuesto
                // $tasa_impuesto=0.16;
                // $precio=$libro->precio;
                // $precio_base=round($precio/1.16,2,PHP_ROUND_HALF_EVEN);
                // $iva=round($precio_base* $tasa_impuesto,2,PHP_ROUND_HALF_EVEN);


                $precio=$libro->precio;
                $precio_base=$precio;


               $array_libro=[
                "ProductCode"=> $libro->clave_producto,
                "Description"=> $libro->nombre,
                "UnitCode"=> "H87",
                "Quantity"=> $libro->cantidad,
                "UnitPrice"=>$precio_base,
                "Subtotal"=> $precio_base,
                "TaxObject" =>  $libro->objeto_impuesto,

                //se descomenta cuando si lleve impuesto
            //     "Taxes"=> [
            //         [
            //         "Total"=> $iva,
            //         "Name"=> "IVA",
            //         "Base"=> $precio_base,
            //         "Rate"=>  $tasa_impuesto,
            //         "IsRetention"=> false
            //     ]
            // ],

            "Total"=> $precio

               ];



                array_push($items, $array_libro);
            }


            $facturama = $this->getInstance();
            $url_logo = "https://booktrain.com.mx/assets/img/svg/logo_principal.png";
            $hoy = date("Y-m-d H:i:s");

            //  dd($facturama);

            $payment_form = explode("-", $pedido->datosFactura->forma_pago);
            $payment_method = $pedido->datosFactura->metodo_pago;
            $folio = $pedido->folio;
            $expedition_place = env('FACTURAMA_EXPEDITIONPLACE');
            $fiscal_regime = env('FACTURAMA_FISCAL_REGIME');
            $rfc = env('FACTURAMA_RFC');
            $name = str_replace("-", " ", env('FACTURAMA_NAME'));

            //Receiver
            //   dd($pedido->datosFactura);

            $rfc_receiver = $pedido->datosFactura->rfc;
            $cfdi_use = UsoCFDI::find($pedido->datosFactura->id_cfdi);
            $name_receiver = strtoupper($pedido->datosFactura->razon_social);
            $fiscal_regime_receiver = $pedido->datosFactura->regimen_fiscal;
            $tax_zip_code = $pedido->datosFactura->cp;
            $street = $pedido->datosFactura->calle;
            $exterior_number = $pedido->datosFactura->num_exterior;
            $interior_number = $pedido->datosFactura->num_interior;
            $neighborhood = $pedido->datosFactura->colonia;
            $municipality = $pedido->datosFactura->municipio;
            $state = $pedido->datosFactura->estado;
            $email=$pedido->datosFactura->correo;


            $params = [
                "CfdiType" => "I",
                "PaymentForm" => $payment_form[0],
                "PaymentMethod" => $payment_method,
                "ExpeditionPlace" => $expedition_place,
                "Date" => $hoy,
                "Folio" => $folio,
                "LogoUrl" => "$url_logo",
                "Issuer" => [
                    "FiscalRegime" => $fiscal_regime,
                    "Rfc" => $rfc,
                    "Name" => $name
                ],
                "Receiver" => [
                    "Rfc" => $rfc_receiver,
                    "CfdiUse" => $cfdi_use->codigo,
                    "Name" => $name_receiver,
                    "FiscalRegime" => $fiscal_regime_receiver,
                    "TaxZipCode" => $tax_zip_code,
                    "Address" => [
                        "Street" => $street,
                        "ExteriorNumber" => $exterior_number,
                        "InteriorNumber" => $interior_number,
                        "Neighborhood" => $neighborhood,
                        "ZipCode" => $tax_zip_code,
                        "Municipality" => $municipality,
                        "State" => $state,
                        "Country" => "México"
                    ]
                ],

                "Items" => $items

            ];

            $ruta = env('FACTURAMA_INVOICE_VERSION');

            // dd($ruta);

            try {

                $result = $facturama->post($ruta, $params);


                $invoice_data = json_encode($result);
                $invoice_data = json_decode($invoice_data, true);

                $invoice_id = $invoice_data['Id'];

                //  dd($invoice_id);
                //  printf('<pre>%s<pre>', var_export($result, true));

            //actualizamos en el pedido el id de la factura
            $pedido->facturama_invoice_id=$invoice_id;
            $pedido->facturama_status="active";
            $pedido->save();

            //enviamos x correo la factura
            $this->sendInvoiceEmail($invoice_id,$email);

            DB::commit();

                Log::debug('FacturamaController@generateCfdi', ['Factura creada con id ' . $invoice_id . " asignada al pedido " . $pedido->id]);

                return response()->json([
                    'success' => true,
                    'msg' => 'Factura creada con id ' . $invoice_id . " asignada al pedido " . $pedido->id,
                    'invoice_id' => $invoice_id
                ], 201);
            } catch (RequestException $e) {

                // dd($e);
                // dd($e->getPrevious()->getMessage());

                if(!empty($e->getPrevious())){

                $error=$e->getPrevious()->getMessage();
                }else{

                    $error= $e->getMessage();

                }


                $pedido->datosFactura->facturama_error = $error;
                $pedido->datosFactura->save();

                DB::commit();

                Log::error('FacturamaController@generateCfdi', ['Error al generar factura para el pedido '.$pedido->id]);

                //enviamos correo d error

                 $this->errorEmail($pedido->id);

                $array = [
                    'success' => false,
                    'ERROR on the transaction Exception: ' => $e->getMessage(),
                    'invoice_error' => $error,
                ];

                Log::error('FacturamaController@generateCfdi ERROR on the transaction Exception: ', $array);


                return response()->json($array);
            } //catch envio peticion


        } catch (\Exception $e) {


            Log::debug('FacturamaController@generateCfdi', ['Error al generar la fatura']);

            $array = [
                'success' => false,
                'ERROR on the transaction Exception: ' => $e->getMessage(),
            ];

            Log::error('FacturamaController@generateCfdi ERROR on the transaction Exception: ', $array);

            return response()->json($array);
        }
    } //generateCfdi



}
