<?php

namespace App\Http\Controllers;

use App\DataTables\LibroPaqueteDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateLibroPaqueteRequest;
use App\Http\Requests\UpdateLibroPaqueteRequest;
use App\Repositories\LibroPaqueteRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class LibroPaqueteController extends AppBaseController
{
    /** @var  LibroPaqueteRepository */
    private $libroPaqueteRepository;

    public function __construct(LibroPaqueteRepository $libroPaqueteRepo)
    {
        $this->libroPaqueteRepository = $libroPaqueteRepo;
    }

    /**
     * Display a listing of the LibroPaquete.
     *
     * @param LibroPaqueteDataTable $libroPaqueteDataTable
     * @return Response
     */
    public function index(LibroPaqueteDataTable $libroPaqueteDataTable)
    {
        return $libroPaqueteDataTable->render('libro_paquetes.index');
    }

    /**
     * Show the form for creating a new LibroPaquete.
     *
     * @return Response
     */
    public function create()
    {
        return view('libro_paquetes.create');
    }

    /**
     * Store a newly created LibroPaquete in storage.
     *
     * @param CreateLibroPaqueteRequest $request
     *
     * @return Response
     */
    public function store(CreateLibroPaqueteRequest $request)
    {
        $input = $request->all();

        $libroPaquete = $this->libroPaqueteRepository->create($input);

        Flash::success('Libro Paquete saved successfully.');

        return redirect(route('libroPaquetes.index'));
    }

    /**
     * Display the specified LibroPaquete.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $libroPaquete = $this->libroPaqueteRepository->find($id);

        if (empty($libroPaquete)) {
            Flash::error('Libro Paquete not found');

            return redirect(route('libroPaquetes.index'));
        }

        return view('libro_paquetes.show')->with('libroPaquete', $libroPaquete);
    }

    /**
     * Show the form for editing the specified LibroPaquete.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $libroPaquete = $this->libroPaqueteRepository->find($id);

        if (empty($libroPaquete)) {
            Flash::error('Libro Paquete not found');

            return redirect(route('libroPaquetes.index'));
        }

        return view('libro_paquetes.edit')->with('libroPaquete', $libroPaquete);
    }

    /**
     * Update the specified LibroPaquete in storage.
     *
     * @param  int              $id
     * @param UpdateLibroPaqueteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLibroPaqueteRequest $request)
    {
        $libroPaquete = $this->libroPaqueteRepository->find($id);

        if (empty($libroPaquete)) {
            Flash::error('Libro Paquete not found');

            return redirect(route('libroPaquetes.index'));
        }

        $libroPaquete = $this->libroPaqueteRepository->update($request->all(), $id);

        Flash::success('Libro Paquete updated successfully.');

        return redirect(route('libroPaquetes.index'));
    }

    /**
     * Remove the specified LibroPaquete from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $libroPaquete = $this->libroPaqueteRepository->find($id);

        if (empty($libroPaquete)) {
            Flash::error('Libro Paquete not found');

            return redirect(route('libroPaquetes.index'));
        }

        $this->libroPaqueteRepository->delete($id);

        Flash::success('Libro Paquete deleted successfully.');

        return redirect(route('libroPaquetes.index'));
    }
}
