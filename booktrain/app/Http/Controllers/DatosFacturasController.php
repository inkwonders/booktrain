<?php

namespace App\Http\Controllers;

use App\DataTables\DatosFacturasDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDatosFacturasRequest;
use App\Http\Requests\UpdateDatosFacturasRequest;
use App\Repositories\DatosFacturasRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DatosFacturasController extends AppBaseController
{
    /** @var  DatosFacturasRepository */
    private $datosFacturasRepository;

    public function __construct(DatosFacturasRepository $datosFacturasRepo)
    {
        $this->datosFacturasRepository = $datosFacturasRepo;
    }

    /**
     * Display a listing of the DatosFacturas.
     *
     * @param DatosFacturasDataTable $datosFacturasDataTable
     * @return Response
     */
    public function index(DatosFacturasDataTable $datosFacturasDataTable)
    {
        return $datosFacturasDataTable->render('datos_facturas.index');
    }

    /**
     * Show the form for creating a new DatosFacturas.
     *
     * @return Response
     */
    public function create()
    {
        return view('datos_facturas.create');
    }

    /**
     * Store a newly created DatosFacturas in storage.
     *
     * @param CreateDatosFacturasRequest $request
     *
     * @return Response
     */
    public function store(CreateDatosFacturasRequest $request)
    {
        $input = $request->all();

        $datosFacturas = $this->datosFacturasRepository->create($input);

        Flash::success('Datos Facturas saved successfully.');

        return redirect(route('datosFacturas.index'));
    }

    /**
     * Display the specified DatosFacturas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            Flash::error('Datos Facturas not found');

            return redirect(route('datosFacturas.index'));
        }

        return view('datos_facturas.show')->with('datosFacturas', $datosFacturas);
    }

    /**
     * Show the form for editing the specified DatosFacturas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            Flash::error('Datos Facturas not found');

            return redirect(route('datosFacturas.index'));
        }

        return view('datos_facturas.edit')->with('datosFacturas', $datosFacturas);
    }

    /**
     * Update the specified DatosFacturas in storage.
     *
     * @param  int              $id
     * @param UpdateDatosFacturasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDatosFacturasRequest $request)
    {
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            Flash::error('Datos Facturas not found');

            return redirect(route('datosFacturas.index'));
        }

        $datosFacturas = $this->datosFacturasRepository->update($request->all(), $id);

        Flash::success('Datos Facturas updated successfully.');

        return redirect(route('datosFacturas.index'));
    }

    /**
     * Remove the specified DatosFacturas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $datosFacturas = $this->datosFacturasRepository->find($id);

        if (empty($datosFacturas)) {
            Flash::error('Datos Facturas not found');

            return redirect(route('datosFacturas.index'));
        }

        $this->datosFacturasRepository->delete($id);

        Flash::success('Datos Facturas deleted successfully.');

        return redirect(route('datosFacturas.index'));
    }
}
