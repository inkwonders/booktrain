<?php

namespace App\Http\Controllers;

use Bbva\Data\Bbva;
use Bbva\Data\BbvaApiAuthError;
use Bbva\Data\BbvaApiConnectionError;
use Bbva\Data\BbvaApiError;
use Bbva\Data\BbvaApiRequestError;
use Bbva\Data\BbvaApiTransactionError;

use App\Models\Direccion;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Transformers\ReferenciaPedidosTransformer;
use App\Models\Configuracion;

class BbvaController extends AppBaseController
{
    public function getInstance() {
        if(env('BBVA_ENABLED', 0) == 1) {
            Bbva::setProductionMode( true );
            Bbva::setSandboxMode( env('BBVA_SANDBOX_MODE', 1) );

            return Bbva::getInstance(
                env('BBVA_MERCHANT_ID', ''),
                env('BBVA_PRIVATE_KEY', '')
            );
        }
        return null;

    }

    /**
     * $tokenData = array(
	 * 'holder_name' => 'Luis Pérez',
	 * 'card_number' => '4111111111111111',
	 * 'cvv2' => '123',
	 * 'expiration_month' => '12',
	 * 'expiration_year' => '15',
	 * 'address' => array(
	 * 	    'line1' => 'Av. 5 de Febrero No. 1',
	 * 	    'line2' => 'Col. Felipe Carrillo Puerto',
	 * 	    'line3' => 'Zona industrial Carrillo Puerto',
	 * 	    'postal_code' => '76920',
	 * 	    'state' => 'Querétaro',
	 * 	    'city' => 'Querétaro',
	 * 	    'country_code' => 'MX'));
     */
    public function addToken($tokenData) {
        return $this->getInstance()->tokens->add($tokenData);

        try {
            return $this->getInstance()->tokens->add($tokenData);
        } catch (BbvaApiTransactionError $e) {
            Log::error("BBVAController::addToken", [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::addToken", ['error' => 'ERROR on the request: ' . $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::addToken", ['error' => 'ERROR while connecting to the API: ' . $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::addToken", ['error' => 'ERROR on the authentication: ' . $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::addToken", ['error' => 'ERROR on the API: ' . $e->getMessage()]);
        }
    }

    public function getToken($token) {
        return $this->getInstance()->tokens->get($token);

        try {
            return $this->getInstance()->tokens->get($token);
        } catch (BbvaApiTransactionError $e) {
            Log::error("BBVAController::getToken", [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::getToken", ['error' => 'ERROR on the request: ' . $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::getToken", ['error' => 'ERROR while connecting to the API: ' . $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::getToken", ['error' => 'ERROR on the authentication: ' . $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::getToken", ['error' => 'ERROR on the API: ' . $e->getMessage()]);
        }
    }

    /**
     * $chargeData = array(
	 * 'affiliation_bbva' => '781500',
	 * 'amount' => 100,
	 * 'currency' => 'MXN',
	 * 'order_id' => 'ORDEN-00071',
	 * 'customer' => array(
	 * 	  'name' => 'Teofilo',
	 * 	  'last_name' => 'Velazco',
	 * 	  'email' => 'teofilo@payments.com',
	 * 	  'phone_number' => '4421112233',
	 * 	  'address' => array(
	 * 	    'line1' => 'Privada Rio No. 12',
	 * 	    'line2' => 'Co. El Tintero',
	 * 	    'line3' => '',
	 * 	    'postal_code' => '76920',
	 * 	    'state' => 'Querétaro',
	 * 	    'city' => 'Querétaro.',
	 * 	    'country_code' => 'MX')),
	 * 'description' => 'Cargo inicial a mi merchant',
	 * 'redirect_url' => 'https://sand-portal.ecommercebbva.com/'
	 * );
     *
     *
     * Ejemplo de respuesta
     *
     * {
     *     "id": "trz8v1n3g992xtylohts",
     *     "authorization": null,
     *     "operation_type": "in",
     *     "method": "card",
     *     "transaction_type": "charge",
     *     "status": "charge_pending",
     *     "conciliated": false,
     *     "creation_date": "2019-04-03T03:57:58-06:00",
     *     "operation_date": "2019-04-03T03:57:58-06:00",
     *     "description": "Pago",
     *     "error_message": null,
     *     "order_id": "oid-00051",
     *     "payment_method": {
     *         "type": "redirect",
     *         "url": "https://sand-api.ecommercebbva.com/v1/mptdggroasfcmqs8plpy/charges/trywj1kyx7vczirifkyw/card_capture"
     *     },
     *     "currency": "MXN",
     *     "amount": 100.00,
     *     "customer": {
     *         "name": "Juan",
     *         "last_name": "Vazquez Juarez",
     *         "email": "juan.vazquez@empresa.com.mx",
     *         "phone_number": "555-444-3322",
     *         "address": null,
     *         "creation_date": "2019-04-03T03:57:58-06:00",
     *         "external_id": null,
     *         "clabe": null
     *     }
     * }
     */
    public function createCharge($chargeData) {
        try {
            $cargo = $this->getInstance()->charges->create($chargeData);

            // dd($this->getInstance()->charges, $chargeData,  $this->getInstance()->charges->create($chargeData));

            Log::debug('BbvaController@createCharge', ['cargo' => $cargo]);

            return $cargo;
        } catch (BbvaApiTransactionError $e) {
            Log::error("BBVAController::createCharge", [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::createCharge", ['error' => 'ERROR on the request: ' . $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::createCharge", ['error' => 'ERROR while connecting to the API: ' . $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::createCharge", ['error' => 'ERROR on the authentication: ' . $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::createCharge", ['error' => 'ERROR on the API: ' . $e->getMessage()]);
        }

        return null;
    }

    public function getCharge($charge) {
        try {
            $cargo = $this->getInstance()->charges->get($charge);

            Log::debug('BbvaController@getCharge', ['cargo' => $cargo]);

            return $cargo;
        } catch (BbvaApiTransactionError $e) {
            Log::error("BBVAController::getCharge", [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::getCharge", ['error' => 'ERROR on the request: ' . $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::getCharge", ['error' => 'ERROR while connecting to the API: ' . $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::getCharge", ['error' => 'ERROR on the authentication: ' . $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::getCharge", ['error' => 'ERROR on the API: ' . $e->getMessage()]);
        }

        return null;
    }

    public function makeCapture($charge, $amount) {
        try {
            $captureData = [
                'amount' => $amount
            ];

            $charge = $this->getInstance()->charges->get($charge);

            return $charge->capture($captureData);
        } catch (BbvaApiTransactionError $e) {
            Log::error('BbvaController::makeCapture', [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::makeCapture", ['error' => 'ERROR on the request: ' . $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::makeCapture", ['error' => 'ERROR while connecting to the API: ' . $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::makeCapture", ['error' => 'ERROR on the authentication: ' . $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::makeCapture", ['error' => 'ERROR on the API: ' . $e->getMessage()]);
        }

        return null;
    }

    public function makeRefund($charge_id, $description, $amount) {


    // Ejemplo de respuesta

    // {
    //    "id":"tr6cxbcefzatd10guvvw",
    //    "amount":100.00,
    //    "authorization":"801585",
    //    "method":"card",
    //    "operation_type":"in",
    //    "transaction_type":"charge",
    //    "card":{
    //       "type":"debit",
    //       "brand":"visa",
    //       "address":null,
    //       "card_number":"411111XXXXXX1111",
    //       "holder_name":"Juan Perez Ramirez",
    //       "expiration_year":"20",
    //       "expiration_month":"12",
    //       "allows_charges":true,
    //       "allows_payouts":true,
    //       "bank_name":"Banamex",
    //       "bank_code":"002"
    //    },
    //    "status":"completed",
    //    "refund":{
    //       "id":"trcbsmjkroqmjobxqhpb",
    //       "amount":100.00,
    //       "authorization":"801585",
    //       "method":"card",
    //       "operation_type":"out",
    //       "transaction_type":"refund",
    //       "status":"completed",
    //       "currency":"MXN",
    //       "creation_date":"2014-05-26T13:56:21-05:00",
    //       "operation_date":"2014-05-26T13:56:21-05:00",
    //       "description":"devolucion",
    //       "error_message":null,
    //       "order_id":null,
    //       "customer_id":"ag4nktpdzebjiye1tlze"
    //    },
    //    "currency":"MXN",
    //    "creation_date":"2014-05-26T11:56:25-05:00",
    //    "operation_date":"2014-05-26T11:56:25-05:00",
    //    "description":"Cargo inicial a mi cuenta",
    //    "error_message":null,
    //    "order_id":"oid-00052",
    //    "customer_id":"ag4nktpdzebjiye1tlze"
    // }
        try {
            $refundData = [
                'description' => $description,
            ];

            if(isset($amount)) {
                $refundData['amount'] = $amount;
            }

            $charge = $this->getInstance()->charges->get($charge_id);

            if($charge == null) {
                Log::error('BbvaController::makeRefund No se encontró el cargo', ['charge_id' => $charge_id]);

                return null;
            }

            $refund = $charge->refund($refundData);

            Log::debug('BbvaController@makeRefund', ['charge' => $charge->serializableData, 'refund' => $refund->serializableData, 'refundData' => $refundData]);

            return $refund;
        }catch (BbvaApiTransactionError $e) {
            Log::error('BbvaController::makeRefund', [
                'code' => $e->getErrorCode(),
                'category' => $e->getCategory(),
                'httpCode' => $e->getHttpCode(),
                'requestId' => $e->getRequestId(),
                'description' => $e->getDescription()
            ]);
        } catch (BbvaApiRequestError $e) {
            Log::error("BBVAController::makeRefund ERROR on the request: ", ['error' => $e->getMessage()]);
        } catch (BbvaApiConnectionError $e) {
            Log::error("BBVAController::makeRefund ERROR while connecting to the API: ", ['error' => $e->getMessage()]);
        } catch (BbvaApiAuthError $e) {
            Log::error("BBVAController::makeRefund ERROR on the authentication: ", ['error' => $e->getMessage()]);
        } catch (BbvaApiError $e) {
            Log::error("BBVAController::makeRefund ERROR on the API: ", ['error' => $e->getMessage()]);
        } finally {
            return null;
        }

        return null;
    }

    /**
     * Ruta para generar el pago de BBVA
     * https://docs.ecommercebbva.com/images/charge-vpos.png
     * 1 El cliente envía datos de la compra a Booktrain
     * 2 Booktrain crea un cargo en BBVA
     * 3 BBVA devuelve una url para acceder al formulario de pago con tarjeta
     * 4 El cliente es redireccionado al formulario de pago de BBVA
     * 5 El cliente envía los datos de tarjeta directamente a BBVA
     * 5.1 Si hay una validación 3DS esta pasa por manos de BBVA
     * 6 Después de hacer el pago en el formulario de BBVA el cliente es redirigido a Booktrain
     */
    public function createPayment(Request $request) {
        $request->validate([
            'pedido_id'      => 'required',
            'metodo_pago'    => 'required',
            'forma_pago'     => 'required',
            'terminos_aceptados'     => 'required',

            'facturacion_requerida' => 'required',

            'contacto.nombre'    => 'required',
            'contacto.apellidos' => 'required',
            'contacto.celular'   => 'required',

            'direccion.calle'      => 'required',
            'direccion.condominio' => 'required',
            'direccion.colonia'    => 'required',
            'direccion.cp'         => 'required',
            'direccion.ciudad'     => 'required',
            'direccion.estado'     => 'required',

            'facturacion.cfdi'         => 'required_if:facturacion_requerida,1',
            'facturacion.correo'       => 'required_if:facturacion_requerida,1',
            'facturacion.cp'           => 'required_if:facturacion_requerida,1',
            'facturacion.razon_social' => 'required_if:facturacion_requerida,1',
            'facturacion.rfc'          => 'required_if:facturacion_requerida,1',

            'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_interior'      => 'sometimes|exclude_if:facturacion_requerida,false|nullable',
            'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',

            'entrega.calle'        => 'required_if:tipo_entrega,2',
            'entrega.colonia'      => 'required_if:tipo_entrega,2',
            'entrega.cp'           => 'required_if:tipo_entrega,2',
            'entrega.estado'       => 'required_if:tipo_entrega,2',
            'entrega.estado_id'    => 'required_if:tipo_entrega,2',
            'entrega.municipio'    => 'required_if:tipo_entrega,2',
            'entrega.municipio_id' => 'required_if:tipo_entrega,2',
            'entrega.no_exterior'  => 'required_if:tipo_entrega,2',
            'entrega.no_interior'  => 'required_if:tipo_entrega,2',
            'entrega.referencia'   => 'required_if:tipo_entrega,2',

            'description' => 'sometimes'
        ]);

        $description = $request->get('description', 'Venta de libros');

        // $description = 'Pago'; // Test 1
        // $description = 'Puntos VPOS'; // Test 2
        // $description = '3D Secure VPOS'; // Test 3
        // Para hacer el reembolso usar php artisan tinker
        // 12345 Es el id del pedido
        // App\Models\Pedido::find(12345)->devolverPago('Reembolso', 105.32);
        // $description = 'MSI'; // Test 4
        // $description = 'SKIP Payment MSI'; // Test 5

        // Creamos la información del cargo
        $user = Auth::user();

        $pedido = $user->pedidos()
            ->with('colegio')
            ->where('status', 'CARRITO')
            ->find($request->pedido_id); // Solo se procesan los pedidos que estén en el carrito

        if (empty($pedido))
            return response()->json(['success' => false, 'message' => 'El pedido no existe o es inválido.'], 404);

        $cargo = null;

        DB::beginTransaction();
            try {
                $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->firstOrFail();
                $forma_pago = FormaPago::where('codigo', $request->forma_pago)->firstOrFail();

                $pedido->update([
                    'status' => 'PROCESADO',
                    'forma_pago' => $request->forma_pago,
                    'metodo_pago' => $request->metodo_pago,
                    'forma_pago_id' => $forma_pago->id,
                    'metodo_pago_id' => $metodo_pago->id,

                    'nombre_contacto'       => $request->contacto['nombre'],
                    'apellidos_contacto'    => $request->contacto['apellidos'],
                    'celular_contacto'      => $request->contacto['celular'],
                    'correo_contacto'       => $request->contacto['correo'],
                    'tipo_entrega'          => $request->tipo_entrega,
                    'terminos_condiciones'  => $request->terminos_aceptados,

                    // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                    'metodo_pago'           => $request->metodo_pago,
                    'forma_pago'            => $request->forma_pago,
                ]);

                $pedido->procesar();

                if($request->facturacion_requerida) {
                    // SEGUN EL SAT
                    // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                    // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T, DEBITO, ID 3 = T. CREDITO

                    $forma_pago_factura = 1;
                    $metodo_pago_factura = 1;

                    switch($request->forma_pago) {
                        case 'PUE': $metodo_pago_factura = 1; break;
                        default: $metodo_pago_factura = 2; break;
                    }

                    switch($request->metodo_pago) {
                        case 'BANCO': $forma_pago_factura = 1; break;
                        case 'BBVADEBITOVPOS': $forma_pago_factura = 2; break;
                        case 'BBVACREDITOVPOS': $forma_pago_factura = 3; break;
                    }

                    // Eliminamos los datos anteriores
                    $datos_factura = $pedido->datosFactura;
                    if($datos_factura != null) $datos_factura->delete();

                    $descripcion_metodo_pago_factura = null;

                    switch ($metodo_pago_factura) {
                        case 1: $descripcion_metodo_pago_factura = 'PUE';break;
                        case 2: $descripcion_metodo_pago_factura = 'PDD';break;
                    }

                    $descripcion_forma_pago_factura = null;

                    switch ($forma_pago_factura) {
                        case 1: $descripcion_forma_pago_factura = '01-EFECTIVO';break;
                        case 2: $descripcion_forma_pago_factura = '04-TARJETA CREDITO';break;
                        case 3: $descripcion_forma_pago_factura = '28-TARJETA DEBITO';break;
                    }

                    // dd($request);

                    $pedido->datosFactura()->create([
                        'user_id'        => $pedido->usuario_id,
                        'id_cfdi'        => $request->facturacion['id_cfdi'],
                        'metodo_pago_id' => $metodo_pago_factura,
                        'forma_pago_id'  => $forma_pago_factura,
                        'forma_pago'     => $descripcion_forma_pago_factura,
                        'metodo_pago'    => $descripcion_metodo_pago_factura,
                        'razon_social'   => $request->facturacion['razon_social'],
                        'rfc'            => $request->facturacion['rfc'],
                        'correo'         => $request->facturacion['correo'],
                        'cp'             => $request->facturacion['cp'],
                        'calle'          => $request->facturacion['calle'],
                        'num_exterior'   => $request->facturacion['num_exterior'],
                        'num_interior'   => $request->facturacion['num_interior'],
                        'colonia'        => $request->facturacion['colonia'],
                        'municipio'      => $request->facturacion['municipio'],
                        'estado'         => $request->facturacion['estado'],
                        'regimen_fiscal' => $request->facturacion['regimen_fiscal'],

                    ]);

                    $pedido->update(['factura' => 1]);
                }

                $direccion = $pedido->direccion()->dissociate();
                if ($request->tipo_entrega == 2) { // Entrega a domicilio
                    $direccion = Direccion::create([
                        'calle'        => $request->input('entrega.calle', ''),
                        'no_exterior'  => $request->input('entrega.no_exterior', ''),
                        'no_interior'  => $request->input('entrega.no_interior', ''),
                        'colonia'      => $request->input('entrega.colonia', ''),
                        'cp'           => $request->input('entrega.cp', ''),
                        'estado_id'    => $request->input('entrega.estado_id', ''),
                        'municipio_id' => $request->input('entrega.municipio_id', ''),
                        'referencia'   => $request->input('entrega.referencia', '')
                    ]);

                    $pedido->direccion()->associate($direccion);
                    $pedido->save();

                    // Eliminamos la información del envio anterior
                    $envio = $pedido->envio;
                    if($envio != null) $envio->delete();

                    // Creamos el registro en la tabla de envios
                    $pedido->envio()->create([
                        'pedido_id' => $pedido->id,
                        'status' => 'PROCESANDO',
                        'costo' => $pedido->colegio->config('costo_envio')
                    ]);
                }else {
                    $pedido->direccion()
                        ->associate(
                            Direccion::find($request->id_direccion_envio)
                        );
                }

                $pedido->save();

                $chargeData = [
                    'affiliation_bbva' => env('BBVA_AFFILIATION_ID', '4302277'),
                    'amount' => $pedido->total,
                    'currency' => 'MXN',
                    'order_id' => uniqid("PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-"),
                    'customer' => [
                        'name' => $request->contacto['nombre'],
                        'last_name' => $request->contacto['apellidos'],
                        'email' => $pedido->user->email,
                        'phone_number' => $request->contacto['celular'],
                        'address' => [
                            'line1' => $request->direccion['calle'],
                            'line2' => $request->direccion['condominio'],
                            'line3' => $request->direccion['colonia'],
                            'postal_code' => $request->direccion['cp'],
                            'state' => $request->direccion['ciudad'],
                            'city' => $request->direccion['estado'],
                            'country_code' => 'MX'
                        ]
                    ],
                    'description' => $description,
                    'redirect_url' => route('bbva.vpos.redirect'),
                    'use_3d_secure' => env('BBVA_3DS_ENABLED', 'true')
                ];

                if( in_array( $request->forma_pago, ['DIFERIDO3M', 'DIFERIDO6M', 'DIFERIDO9M'])) {
                    $configuracion_forma_pago = $pedido
                        ->colegio
                        ->formasPago()
                        ->wherePivot('forma_pago_id', $forma_pago->id)
                        ->wherePivot('metodo_pago_id', $metodo_pago->id)
                        ->first();

                    $chargeData['payment_plan'] = [ // Asumimos la cantidad de pagos igual a la cantidad de meses
                        'payments' => $configuracion_forma_pago->pivot->meses,
                        'payments_type' => 'WITHOUT_INTEREST', // WITHOUT_INTEREST ó WITH_INTEREST
                        'deferred_months' => $configuracion_forma_pago->pivot->meses
                    ];
                }

                Log::debug('createCharge', ['chargeData' => $chargeData]);

                $cargo = $this->createCharge($chargeData);
                /**
                 *       Ejemplo de respuesta
                 * {
                 *     "id": "trz8v1n3g992xtylohts",
                 *     "authorization": null,
                 *     "operation_type": "in",
                 *     "method": "card",
                 *     "transaction_type": "charge",
                 *     "status": "charge_pending",
                 *     "conciliated": false,
                 *     "creation_date": "2019-04-03T03:57:58-06:00",
                 *     "operation_date": "2019-04-03T03:57:58-06:00",
                 *     "description": "Pago",
                 *     "error_message": null,
                 *     "order_id": "oid-00051",
                 *     "payment_method": {
                 *         "type": "redirect",
                 *         "url": "https://sand-api.ecommercebbva.com/v1/mptdggroasfcmqs8plpy/charges/trywj1kyx7vczirifkyw/card_capture"
                 *     },
                 *     "currency": "MXN",
                 *     "amount": 100.00,
                 *     "customer": {
                 *         "name": "Juan",
                 *         "last_name": "Vazquez Juarez",
                 *         "email": "juan.vazquez@empresa.com.mx",
                 *         "phone_number": "555-444-3322",
                 *         "address": null,
                 *         "creation_date": "2019-04-03T03:57:58-06:00",
                 *         "external_id": null,
                 *         "clabe": null
                 *     }
                 * }
                 */

                $pedido->pagos()->create([
                    'amount' => $cargo->amount,
                    'origen' => 'API',
                    'source' => $cargo->order_id,
                    'transactionTokenId' => $cargo->id,
                    'raw' => json_encode($cargo->serializableData),
                    'status' => 'IN_PROCESS'
                ]);

                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();

                Log::error('BbvaController@createPayment', ['error' => $th]);
                return response()->json(['success' => false, 'message' => "Error {$th->getCode()}: {$th->getMessage()}"], 503);
                // return response()->json(['success' => false, 'message' => "{$th->getMessage()} {$th->getTraceAsString()}"], 503);
            }

        return response()->json([
            'success' => true,
            'redirect_url' => $cargo->payment_method->url
        ]);
    }

    /**
     * Ruta para capturar la redirección del proceso de pago de BBVA
     * https://docs.ecommercebbva.com/images/charge-vpos.png
     * 7 El cliente accede a la url de redirección en Booktrain para consultar el estado del pago
     * 8 Booktrain consulta el estado del pago en el servidor de BBVA
     * 9 El cliente recibe una respuesta de parte de Booktrain según el resultado de la transacción
     */
    public function redirectVpos(Request $request) {
        Log::debug('BbvaController@redirectVpos', ['request' => $request->all()]);

        $transactionId = $request->get('id', '');
        $cargo = $this->getCharge($transactionId);

        $user = Auth::user();

        $pedido = $user->pedidos()
            ->whereHas('pagos', function($query) use ($transactionId) {
                $query->where('transactionTokenId', $transactionId);
                // El pedido no debe tener pagos exitosos, obvio
                $query->whereNotIn('status', ['FAILED', 'SUCCESS']);
            })
            ->whereDoesntHave('pagos', function ($query) use ($transactionId) {
                $query->where('status', 'SUCCESS');
            })
            ->with('pagos')
            ->first();

        if( ! $pedido) {
            Log::error('BbvaController@redirectVpos', ['error' => 'El pedido no se encuentra', 'id' => $request->id ]);

            return 'Error';
        }

        if( ! $cargo) {
            Log::error('BbvaController@redirectVpos', ['error' => 'El cargo no existe', 'id' => $request->id ]);

            return 'Error';
        }

        // Comprobamos el estado del pago
        $pago = $pedido->pagos()->where('transactionTokenId', $transactionId)->first();

        // Buscamos la referencia de pago del pedido, cambiamos el estado del pedido a pagado y creamos el registro de pago
        DB::beginTransaction();
        try {
            $estado_pago = 'IN_PROCESS';

            $mensaje = 'Sin mensajes';
            switch (strtolower( $cargo->serializableData['status']) ){
                case 'completed': // 	Transacción ejecutadá correctamente
                    $estado_pago = 'SUCCESS';
                    $mensaje = 'Transacción exitosa';
                break;

                case 'in_progress': // 	Transacción en proceso
                    $estado_pago = 'IN_PROCESS';

                    $mensaje = 'La transacción aun no ha sido aprobada, reintente más tarde';
                break;

                case 'failed': // 	Transacción que se intentó pagar pero ocurrió algún error
                    $estado_pago = 'FAILED';

                    $mensaje = 'La transacción ha fallado';
                break;

                case 'refunded': // 	Transacción reembolsada
                    $estado_pago = 'FAILED';
                    $mensaje = 'Transacción reembolsada';
                break;

                case 'charge_pending': // 	Transacción de cargo que no ha sido pagada
                    $estado_pago = 'FAILED';
                    $mensaje = 'Transacción de cargo que no ha sido pagada';
                break;

                case 'cancelled': // 	Transacción de cargo que no fue pagada y se ha cancelado
                    $estado_pago = 'FAILED';
                    $mensaje = 'Transacción de cargo que no fue pagada y se ha cancelado';
                break;
            }

            $pago->update([
                'status' => $estado_pago,
                'raw' => json_encode($cargo->serializableData)
            ]);

            if($estado_pago == 'SUCCESS') {
                $pedido->update([
                    'status' => 'PAGADO'
                ]);

                $pedido->sendPagoExitosoNotification();
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('BbvaController@redirectVpos', ['error' => 'Error al procesar la redirección', 'cargo' => $cargo, 'message' => $th->getMessage(), 'mensaje_estado_pago' => $mensaje]);

            $mensaje = 'Error al procesar la redirección';
        }

        if($estado_pago == 'SUCCESS') {
            return view('private.resultado3ds')->with(['success' => true, 'error' => '', 'message' => 'El pago se ha procesado correctamente']);
        }else{
            return view('private.resultado3ds')->with(['success' => false, 'titulo' => 'La transacción no pudo ser procesada', 'message' => $mensaje]);
        }

        // Ejemplo de respuesta

        // {
        //    "id":"tr6cxbcefzatd10guvvw",
        //    "amount":100.00,
        //    "authorization":"801585",
        //    "method":"card",
        //    "operation_type":"in",
        //    "transaction_type":"charge",
        //    "card":{
        //       "type":"debit",
        //       "brand":"visa",
        //       "address":null,
        //       "card_number":"411111XXXXXX1111",
        //       "holder_name":"Juan Perez Ramirez",
        //       "expiration_year":"20",
        //       "expiration_month":"12",
        //       "allows_charges":true,
        //       "allows_payouts":true,
        //       "bank_name":"Banamex",
        //       "bank_code":"002"
        //    },
        //    "status":"completed",
        //    "refund":{
        //       "id":"trcbsmjkroqmjobxqhpb",
        //       "amount":100.00,
        //       "authorization":"801585",
        //       "method":"card",
        //       "operation_type":"out",
        //       "transaction_type":"refund",
        //       "status":"completed",
        //       "currency":"MXN",
        //       "creation_date":"2014-05-26T13:56:21-05:00",
        //       "operation_date":"2014-05-26T13:56:21-05:00",
        //       "description":"devolucion",
        //       "error_message":null,
        //       "order_id":null,
        //       "customer_id":"ag4nktpdzebjiye1tlze"
        //    },
        //    "currency":"MXN",
        //    "creation_date":"2014-05-26T11:56:25-05:00",
        //    "operation_date":"2014-05-26T11:56:25-05:00",
        //    "description":"Cargo inicial a mi cuenta",
        //    "error_message":null,
        //    "order_id":"oid-00052",
        //    "customer_id":"ag4nktpdzebjiye1tlze"
        // }
    }

    /**
     * Crea una referencia de pago en ventanilla de BBVA y procesa el pedido, descontando existencias
     */
    public function createPaymentReference(Request $request, $pedido_id) {
        $request->validate([
            'contacto.nombre'               => 'required',
            'contacto.apellidos'            => 'required',
            'contacto.celular'              => 'required',
            'tipo_entrega'                  => 'sometimes|required|between:1,3',
            'id_direccion_envio'            => 'sometimes|exists:direcciones,id',
            'facturacion_requerida'         => 'required|boolean',
            'facturacion.nombre'            => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.rfc'               => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.correo'            => 'exclude_if:facturacion_requerida,false|sometimes|email',
            'facturacion.cfdi'              => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.id_cfdi'           => 'exclude_if:facturacion_requerida,false|sometimes|exists:cfdis,id',
            'facturacion.calle'             => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_exterior'      => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.num_interior'      => 'sometimes|exclude_if:facturacion_requerida,false|nullable',
            'facturacion.colonia'           => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.municipio'         => 'exclude_if:facturacion_requerida,false|sometimes',
            'facturacion.estado'            => 'exclude_if:facturacion_requerida,false|sometimes',
            'terminos_aceptados'            => 'required|accepted',
            'forma_pago'                    => 'required|in:PUE,DIFERIDO3M,DIFERIDO6M,DIFERIDO9M',
            'metodo_pago'                   => 'required|in:CREDITO,DEBITO,AMEX,BANCO,TIENDA,EFECTIVO,TERMINAL,TRANSFERENCIA,BBVADEBITOVPOS,BBVACREDITOVPOS,BBVATARJETA,OPDEBITO,OPAMEX,OPMASTERCARD,OPEFECTIVO,OPSPEI,OPBANCO,OPTIENDA,OPQRALIPAY,OPQRCODI,OPIVR',
            'direccion.calle'               => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_exterior'         => 'sometimes|required_if:tipo_entrega,2',
            'direccion.no_interior'         => 'sometimes',
            'direccion.colonia'             => 'sometimes|required_if:tipo_entrega,2',
            'direccion.cp'                  => 'sometimes|required_if:tipo_entrega,2',
            'direccion.estado'              => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio'           => 'sometimes|required_if:tipo_entrega,2',
            'direccion.municipio_id'        => 'sometimes|required_if:tipo_entrega,2|exists:municipios,id',
            'direccion.estado_id'           => 'sometimes|required_if:tipo_entrega,2|exists:estados,id',
            'direccion.referencia'          => 'sometimes',
        ]);

        $user = Auth::user();

        $pedido = $user->pedidos()->where('status', 'CARRITO')->find($pedido_id); // Solo se procesan los pedidos que estén en el carrito
        $pago = $request->all();

        if (empty($pedido))
            return response()->json(['success' => false, 'message' => 'El pedido no existe o es inválido.'], 404);

        if( $request->metodo_pago != 'BANCO' )
            return response()->json(['success' => false, 'message' => 'No se admite esta forma de pago en esta ruta'], 400);

        // Almacenamos la información del pedido
        // Si el pago procede continuamos a editar el registro del pedido para agregar su información del pago
        DB::beginTransaction();

        try {
            $metodo_pago = MetodoPago::where('codigo', $request->metodo_pago)->first();
            $forma_pago = FormaPago::where('codigo', $request->forma_pago)->first();

            $pedido->update([
                'nombre_contacto'       => $request->contacto['nombre'],
                'apellidos_contacto'    => $request->contacto['apellidos'],
                'celular_contacto'      => $request->contacto['celular'],
                'tipo_entrega'          => $request->tipo_entrega,
                'terminos_condiciones'  => $request->terminos_aceptados,
                // 'comision'              => $request->comision, // Se debe calcular según el tipo de pedido
                'metodo_pago'           => $request->metodo_pago,
                'forma_pago'            => $request->forma_pago,
                'status'                => 'PROCESADO',
                'metodo_pago_id'        => $metodo_pago->id,
                'forma_pago_id'         => $forma_pago->id,
            ]);

            if($request->facturacion_requerida) {
                // SEGUN EL SAT
                // METODO DE PAGO ID 1 = PUE, ID 2 = PPD
                // FORMA DE PAGO ID 1 = EFECTIVO, ID 2 = T, DEBITO, ID 3 = T. CREDITO

                $forma_pago_factura = 1;
                $metodo_pago_factura = 1;

                switch($request->forma_pago) {
                    case 'PUE': $metodo_pago_factura = 1; break;
                    default: $metodo_pago_factura = 2; break;
                }

                switch($request->metodo_pago) {
                    case 'BANCO': $forma_pago_factura = 1; break;
                    case 'TIENDA': $forma_pago_factura = 1; break;
                    case 'DEBITO': $forma_pago_factura = 2; break;
                    case 'CREDITO': $forma_pago_factura = 3; break;
                    case 'AMEX': $forma_pago_factura = 3; break;
                }

                // Eliminamos los datos anteriores
                $datos_factura = $pedido->datosFactura;
                if($datos_factura != null) $datos_factura->delete();

                $metodo_pago = null;

                switch ($metodo_pago_factura) {
                    case 1: $metodo_pago = 'PUE';break;
                    case 2: $metodo_pago = 'PDD';break;
                }

                $forma_pago = null;

                switch ($forma_pago_factura) {
                    case 1: $forma_pago = '01-EFECTIVO';break;
                    case 2: $forma_pago = '04-TARJETA CREDITO';break;
                    case 3: $forma_pago = '28-TARJETA DEBITO';break;
                }

                $pedido->datosFactura()->create([
                    'user_id'        => $pedido->usuario_id,
                    'id_cfdi'        => $request->facturacion['id_cfdi'],
                    'metodo_pago_id' => $metodo_pago_factura,
                    'forma_pago_id'  => $forma_pago_factura,
                    'forma_pago'     => $forma_pago,
                    'metodo_pago'    => $metodo_pago,
                    'razon_social'   => $request->facturacion['razon_social'],
                    'rfc'            => $request->facturacion['rfc'],
                    'correo'         => $request->facturacion['correo'],
                    'cp'             => $request->facturacion['cp'],
                    'calle'          => $request->facturacion['calle'],
                    'num_exterior'   => $request->facturacion['num_exterior'],
                    'num_interior'   => $request->facturacion['num_interior'],
                    'colonia'        => $request->facturacion['colonia'],
                    'municipio'      => $request->facturacion['municipio'],
                    'estado'         => $request->facturacion['estado'],
                    'regimen_fiscal' => $request->facturacion['regimen_fiscal'],

                ]);

                $pedido->update(['factura' => 1]);
            }

            $direccion = $pedido->direccion()->dissociate(); // <<<< Verificar que eso sea correcto

            if ($request->tipo_entrega == 2) { // a domicilio
                $direccion = Direccion::create([
                        'calle'        => $request->input('direccion.calle', ''),
                        'no_exterior'  => $request->input('direccion.no_exterior', ''),
                        'no_interior'  => $request->input('direccion.no_interior', ''),
                        'colonia'      => $request->input('direccion.colonia', ''),
                        'cp'           => $request->input('direccion.cp', ''),
                        'estado_id'    => $request->input('direccion.estado_id', ''),
                        'municipio_id' => $request->input('direccion.municipio_id', ''),
                        'referencia'   => $request->input('direccion.referencia', '')
                    ]);

                    $pedido->direccion()->associate($direccion);
                    $pedido->save();

                    // Eliminamos la información del envio anterior
                    $envio = $pedido->envio;
                    if($envio != null) $envio->delete();

                    // Creamos el registro en la tabla de envios
                    $pedido->envio()->create([
                        'pedido_id' => $pedido->id,
                        'status' => 'PROCESANDO',
                        'costo' => $pedido->colegio->config('costo_envio')
                    ]);
            }else {
                $pedido->direccion()->associate(Direccion::find($request->id_direccion_envio));
            }
            $pedido->save();

        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->sendError($th->getMessage(), 503);
        }

        // DB::commit();

        /** se vuelve a obtener el pedido para obtener los datos actualizados */
        $pedido->refresh();

        // CASO 1: Referencia bancaria
        if($pedido->metodo_pago == 'BANCO') {
            try {
                $pedido->referencia()->create([
                    'referencia'    => $pedido->generarReferenciaAlgoritmo10BBVA(),
                    'tipo'          => 'BANCARIA',
                    'dias_vigencia' => 2,
                    'pagado'        => 0,
                    'fecha_pago'    => NULL,
                    'response'      => NULL,
                ]);

                $pedido->update([ 'status' => 'PROCESADO' ]);

            } catch (\Throwable $th) {
                DB::rollback();
                $pedido->update([ 'status' => 'CARRITO' ]);
                return $this->sendError($th->getMessage(), 503);
            }
        }

        // DB::commit();

        try {
            $pedido->save();
            $pedido->refresh();
            $pedido->procesar();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::debug("procesarPedido", ['message' => $th->getMessage()]);
            return $this->sendError('Error al procesar el pedido: ' . $th->getMessage(), 503);
        }

        DB::commit();

        $comision = $pedido->comision;
        $total = $pedido->total;

        $pedido->update([
            'comision' => $comision,
            'total' => $total
        ]);

        // DETERMINAMOS EL TIPO DE RESPUESTA A REGRESAR
        $fractal = new Manager();
        $informacion = $fractal->setSerializer(new Serializer());

        // CASO 1: Referencia bancaria
        if($pedido->metodo_pago == 'BANCO')
            $datosPedido = $informacion
                ->parseIncludes(['referencia_interbancaria'])
                ->createData(new Item($pedido, new ReferenciaPedidosTransformer))
                ->toArray();


        return $this->sendResponse($datosPedido, 'Pedido ' . $pedido->id . ' actualizado correctamente');
    }




    /**
     * Crea un webhook para recibir las llamadas de notificación de pago desde Openpay
     */
    public function registerWebhook() {

        $configuracion = Configuracion::firstOrNew([
            'etiqueta' => 'webhook_creado'
        ]);

        try {

            $configuracion_webhook = [
                'url' => route('webhook.bbva'), // 'https://booktrain.com.mx/webhook/openpay/gateway',
                // 'user' => env('OPENPAY_WEBHOOK_USERNAME', 'BOOKTRAIN_WH_DEFAULT_USER'),
                // 'password' => env('OPENPAY_WEBHOOK_PASSWORD', 'BOOKTRAIN_WH_DEFAULT_PASSWORD'),
                'user' => 'booktrain',
                'password' => 'booktrain',
                'event_types' => [
                    'verification',
                    'charge.created',
                    'charge.succeeded',
                    'charge.refunded'
                    //  'payout.created',
                    //  'payout.succeeded',
                    //  'payout.failed',
                    //  'transfer.succeeded',
                    //  'fee.succeeded',
                    //  'spei.received',
                    //  'chargeback.created',
                    //  'chargeback.rejected',
                    //  'chargeback.accepted',
                ]
                // "status" => "verified"
            ];

            $configuracion->valor = 0;
            $configuracion->save();

            $resultado_creacion_webhook = $this->getInstance()->webhooks->add($configuracion_webhook);
           /* $instancia = Openpay::getInstance(env('OPENPAY_MERCHANT_ID'),env('OPENPAY_PRIVATE_KEY'));
            $resultado_creacion_webhook = $instancia->webhooks->add($configuracion_webhook);*/


            Log::debug('createWebhook', ['resultado' => $resultado_creacion_webhook->serializableData]);

            activity()
            ->causedBy(1)
            ->log('Se creó un webhook para bbva con el id: ' . $resultado_creacion_webhook->id);

            return response()->json($resultado_creacion_webhook->serializableData);
        } catch (\Throwable $th) {
            Log::error('createWebhook', [$th]);
            return response()->json(['error' => $th->getMessage()], 503);
        }
    }

    // webhook bancomer
    public function webhook(Request $request) {

        // dd($request->all());

        $request->validate([
            'type'                                  => 'required',
            'transaction'                           => 'required',
            'transaction.id'                        => 'required',
            'transaction.amount'                    => 'required',
            'transaction.method'                    => 'required',
            'transaction.transaction_type'          => 'required',
            'transaction.status'                    => 'required',
            'transaction.order_id'                  => 'required',
            'transaction.description'               => 'required',
            'transaction.payment_method.reference'  => 'required|exists:referencias_pedidos,referencia',

        ]);

        Log::debug("BbvaController@webhook: request", $request->all());


        $transactionTokenId = $request->transaction['id'];

        $referencia = $request->transaction['payment_method']['reference'];

        $pedido = Pedido::whereHas('referencias', function($query) use ($referencia) {
            $query->where('referencia', $referencia);
            $query->withCount('referenciasDuplicadas');
            $query->having('referencias_duplicadas_count', '=', 1); // Validamos que no tengan referencias duplicadas
        })->first();

        if( ! $pedido) {
            Log::error("BbvaController@webhook: Pedido no encontrado", ['referencia' => $referencia, 'transactionTokenId' => $transactionTokenId]);
            return response()->json(['error' => 'Pedido no encontrado'], 404);
        }

        if( ! in_array($pedido->status, ['PROCESADO'] )) {
            Log::error("BbvaController@webhook: El pedido ya fue procesado", ['referencia' => $referencia, 'transactionTokenId' => $transactionTokenId, 'pedido_id' => $pedido->id]);
            return response()->json(['error' => 'El pedido ya fue procesado'], 403);
        }

        Log::debug("BbvaController@webhook: Pedido", ['pedido' => $pedido->id, 'referencia' => $referencia, 'transactionTokenId' => $transactionTokenId]);

        try {
            switch ($request->type) {
                case 'verification': //La notificación contiene el código de verificación del Webhook
                    $configuracion_webhook = Configuracion::where('etiqueta', 'webhook_creado')->first();

                    if($configuracion_webhook->valor == 0) {
                        $configuracion_webhook->update(['valor' => 1]);

                        return response()->json(['success' => 'true', 'message' => 'Webhook registrado correctamente']);
                    }
                break;

                case 'charge.created': // Se creó un cargo para ser pagado por transferencia bancaria.
                    $pago = [
                        'transactionTokenId' => $transactionTokenId,
                        'origen' => 'WEBHOOK',
                        'source' => $request->transaction['payment_method']['reference'],
                        'raw' => json_encode($request->all()),
                        'status' => 'IN_PROCESS',
                        'amount' => $request->transaction['amount']
                    ];

                    Log::debug('BbvaController@webhook: Creando pago en pedido', ['pedido_id' => $pedido->id, 'pago' => $pago]);
                    $pedido->pagos()->create($pago);
                break;

                case 'charge.succeeded': // Indica que el cargo se ha completado (tarjeta, banco o tienda)
                    switch($request->transaction['method']) {
                        case 'store':
                            Log::info('BbvaController@webhook: Notificación de cargo en tienda, se recibió un estado ' . $request->transaction['status']);
                            $pago = $pedido->pagos()->where('transactionTokenId', $transactionTokenId)->first();

                            switch($request->transaction['status']) {
                                case 'completed':
                                    if($pedido->total != $request->transaction['amount']){
                                        Log::error('BbvaController@webhook: El monto de la transacción no coincide con el pedido', ['pedido_id' => $pedido->id]);
                                        return response()->json(['error' => 'El monto de la transacción no coincide con el pedido'], 400);
                                    }

                                    if( ! Str::startsWith($request->transaction['order_id'], "PED-{$pedido->id}-{$pedido->serie}-{$pedido->folio}-")){
                                        Log::error('BbvaController@webhook: La orden no coincide con el pedido', ['pedido_id' => $pedido->id]);
                                        return response()->json(['error' => 'La orden no coincide con el pedido'], 400);
                                    }

                                    //dd($pago, $request->all());
                                    $pago->update([
                                        'status' => 'SUCCESS',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);

                                    $pedido->update([
                                        'status' => 'PAGADO'
                                    ]);

                                    $pedido->referencias()->where('referencia', $referencia)->first()->update([
                                        'pagado' => 1,
                                        'notificado' => $pedido->sendPagoExitosoNotification()
                                    ]);
                                break;

                                case 'in_progress':
                                    $pago->update([
                                        'status' => 'IN_PROCESS',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);
                                break;

                                case 'failed':
                                    $pago->update([
                                        'status' => 'FAILED',
                                        'raw_comprobacion' => json_encode($request->all())
                                    ]);
                                break;
                            }
                        break;
                        case 'card':
                            Log::info('BbvaController@webhook: Notificación de cargo por tarjeta, se recibió un estado ' . $request->transaction['status']);
                        break;
                        case 'bank':
                            Log::info('BbvaController@webhook: Notificación de cargo en banco, se recibió un estado ' . $request->transaction['status']);
                        break;
                    }
                break;

                case 'charge.refunded': // Un cargo a tarjeta fue reembolsado.
                break;

                case 'payout.created': // Se ha programado un pago.
                break;

                case 'payout.succeeded': // Se ha enviado el pago.
                break;

                case 'payout.failed': // El pago fue rechazado.
                break;

                case 'transfer.succeeded': // Se ha realizado una transferencia entre dos clientes.
                break;

                case 'fee.succeeded': // Se ha cobrado una comisión a un cliente.
                break;

                case 'spei.received': // Se han agregado fondos a una cuenta mediante SPEI.
                break;

                case 'chargeback.created': // Se ha recibido un contracargo de un cargo a tarjeta
                break;

                case 'chargeback.rejected': // El contracargo se ha ganado a favor del comercio
                break;

                case 'chargeback.accepted': // El contracargo se ha perdido. Se ha creado una transacción tipo contracargo que descontará los fondos de tu cuenta.
                break;
            }

        } catch (\Throwable $th) {
            Log::error("BbvaController@webhook", [$th]);
            return response()->json(['error' => 'Error al procesar el webhook', 'message' => $th->getMessage()]);
        }

        return response()->json(['success' => true]);
    }

}

