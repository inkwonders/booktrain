<?php

namespace App\Http\Controllers;

use App\DataTables\TicketMotivoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTicketMotivoRequest;
use App\Http\Requests\UpdateTicketMotivoRequest;
use App\Repositories\TicketMotivoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TicketMotivoController extends AppBaseController
{
    /** @var  TicketMotivoRepository */
    private $ticketMotivoRepository;

    public function __construct(TicketMotivoRepository $ticketMotivoRepo)
    {
        $this->ticketMotivoRepository = $ticketMotivoRepo;
    }

    /**
     * Display a listing of the TicketMotivo.
     *
     * @param TicketMotivoDataTable $ticketMotivoDataTable
     * @return Response
     */
    public function index(TicketMotivoDataTable $ticketMotivoDataTable)
    {
        return $ticketMotivoDataTable->render('ticket_motivos.index');
    }

    /**
     * Show the form for creating a new TicketMotivo.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_motivos.create');
    }

    /**
     * Store a newly created TicketMotivo in storage.
     *
     * @param CreateTicketMotivoRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketMotivoRequest $request)
    {
        $input = $request->all();

        $ticketMotivo = $this->ticketMotivoRepository->create($input);

        Flash::success('Ticket Motivo saved successfully.');

        return redirect(route('ticketMotivos.index'));
    }

    /**
     * Display the specified TicketMotivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            Flash::error('Ticket Motivo not found');

            return redirect(route('ticketMotivos.index'));
        }

        return view('ticket_motivos.show')->with('ticketMotivo', $ticketMotivo);
    }

    /**
     * Show the form for editing the specified TicketMotivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            Flash::error('Ticket Motivo not found');

            return redirect(route('ticketMotivos.index'));
        }

        return view('ticket_motivos.edit')->with('ticketMotivo', $ticketMotivo);
    }

    /**
     * Update the specified TicketMotivo in storage.
     *
     * @param  int              $id
     * @param UpdateTicketMotivoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketMotivoRequest $request)
    {
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            Flash::error('Ticket Motivo not found');

            return redirect(route('ticketMotivos.index'));
        }

        $ticketMotivo = $this->ticketMotivoRepository->update($request->all(), $id);

        Flash::success('Ticket Motivo updated successfully.');

        return redirect(route('ticketMotivos.index'));
    }

    /**
     * Remove the specified TicketMotivo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketMotivo = $this->ticketMotivoRepository->find($id);

        if (empty($ticketMotivo)) {
            Flash::error('Ticket Motivo not found');

            return redirect(route('ticketMotivos.index'));
        }

        $this->ticketMotivoRepository->delete($id);

        Flash::success('Ticket Motivo deleted successfully.');

        return redirect(route('ticketMotivos.index'));
    }
}
