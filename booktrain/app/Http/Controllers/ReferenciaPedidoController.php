<?php

namespace App\Http\Controllers;

use App\Models\ReferenciaPedido;
use Illuminate\Http\Request;

use App\Imports\BancomerImport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Excel;

class ReferenciaPedidoController extends Controller
{

    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReferenciaPedido  $referenciaPedido
     * @return \Illuminate\Http\Response
     */
    public function show(ReferenciaPedido $referenciaPedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReferenciaPedido  $referenciaPedido
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferenciaPedido $referenciaPedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReferenciaPedido  $referenciaPedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferenciaPedido $referenciaPedido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReferenciaPedido  $referenciaPedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferenciaPedido $referenciaPedido)
    {
        //
    }

    /*
     * Recibe un excel de BBVA
     *
     */
    public function importarReporteBancomerExcel(Request $request)
    {
        $nombre_archivo = "";
        foreach ($request->files as $nombre => $contenido) {
            if (!$contenido->isValid()) {
                Log::error("Error al subir el archivo {$nombre} es invalido");
                return response()->json(['success' => false, 'msg' => 'El archivo es invalido'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'El archivo es invalido');
            }

            if (!($contenido->guessExtension() == "xlsx" || $contenido->guessExtension() == "xls" || $contenido->guessExtension() == "csv")) {
                Log::error("Error al subir el archivo, solo se aceptan hojas con formato xslx, xsl o csv");
                return response()->json(['success' => false, 'msg' => 'Solo se aceptan hojas con formato xslx, xsl o csv'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'Solo se aceptan hojas con formato xslx, xsl o csv');
            }

            //guardamos la imagen en el disco
            $nombre_archivo = sha1(time() . "-" . $nombre) . '.' . $contenido->guessExtension();
            \Storage::disk('local')->put($nombre_archivo, \File::get($contenido));
        }

        $nombre_archivo = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $nombre_archivo;

        $response = $this->excel->import(new BancomerImport, $nombre_archivo);

        //TODO: Eliminar el archivo antes de terminar la funcion

        return response()->json(['success' => true], 200);
        //return view('private.catalogo.update')->with('mensaje', 'Catalogo Actualizado Correctamente!!');
    }

    /*
     * Recibe un archivod e texto con extensión .exp
     *
     */
    public function importarReporteBancomerExp(Request $request)
    {
        $nombre_archivo = "";
        foreach ($request->files as $nombre => $contenido) {
            if (!$contenido->isValid()) {
                Log::error("Error al subir el archivo {$nombre} es invalido");
                return response()->json(['success' => false, 'msg' => 'El archivo es invalido'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'El archivo es invalido');
            }

            if (!($contenido->guessExtension() == "txt")) {
                Log::error("Error al subir el archivo, solo se aceptan hojas con formato txt");
                return response()->json(['success' => false, 'msg' => 'Solo se aceptan hojas con formato xslx, xsl o csv'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'Solo se aceptan hojas con formato xslx, xsl o csv');
            }

            //guardamos la imagen en el disco
            $nombre_archivo = sha1(time() . "-" . $nombre) . '.' . $contenido->guessExtension();
            \Storage::disk('local')->put($nombre_archivo, \File::get($contenido));
        }

        $nombre_archivo = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $nombre_archivo;

        //$response = $this->excel->import(new BancomerImport, $nombre_archivo);
        $file = fopen($nombre_archivo, "r") or exit("Unable to open file!");

        $pedidos = [];

        $num_referencias_encontradas = 0;
        $num_referencias_pedidos_encontrados = 0;
        $num_referencias_afectadas = 0;
        $num_pedidos_no_encontrados = 0;

        DB::beginTransaction();

        while (!feof($file)) {
            // echo fgets($file). "<br>";
            $linea = fgets($file);

            $referencia = (int) substr($linea, 7, 20);

            if ($referencia == 0) {
                continue;
            }

            $num_referencias_encontradas += 1;

            $importe_total = (float) substr($linea, 89, 16);
            $fecha = substr($linea, 114, 10);

            //echo "ref = " . $referencia . " importe = " . $importe_total . " fecha = " . $fecha . "<br>";

            //TODO: Pendiente verificar si esta es la referencia correcta o es la Guia CIE
            $referencia_pedido = ReferenciaPedido::where("referencia", $referencia)->first();

            if (is_null($referencia_pedido)) {
                continue;
            }

            $num_referencias_pedidos_encontrados += 1;

            //echo "ref = " . $referencia . " importe = " . $importe_total . " fecha = " . $fecha . "<br>";

            try {
                if (!$request->has('preview')) {
                    //actualizamos la referencia del pedido
                    $referencia_pedido->response = $linea;
                    $referencia_pedido->pagado = 1;
                    $referencia_pedido->fecha_pago = $fecha;
                    $referencia_pedido->save();
                }
            } catch (\Exception $e) {
                Log::error('Error al actualizar la referencia del pedido: ' . $e->getMessage);
                DB::rollback();
            }

            $pedido = $referencia_pedido->pedido;

            //actualizamos el estado del pedido

            if (is_null($pedido)) {
                $num_pedidos_no_encontrados += 1;

                continue;
            }

            try {
                if (!$request->has('preview')) {
                    $pedido->status = 'PAGADO';
                    $pedido->save();
                }
            } catch (\Exception $e) {
                Log::error('Error al actualizar el estatus del pedido: ' . $e->getMessage);
                DB::rollback();
            }

            //validamos si ya hay pagos registrados
            $registro_pago = $pedido->pagos()->where('status', 'SUCCESS')->first();

            if (!$request->has('preview') && is_null($registro_pago)) {
                // Guardamos la información del pago en el pedido
                $nuevo_pago = $pedido->pagos()->firstOrCreate(
                    [ // Parámetros para la búsqueda
                        'amount'                => $importe_total,
                        'source'                => $referencia,
                        'transactionTokenId'    => '',
                        'status'                => 'SUCCESS',
                        'raw'                   => $linea,
                        'origen'                => 'CARGA MASIVA'
                    ],
                    [ // Parámetros para su creación
                        'amount'                => $importe_total,
                        'source'                => $referencia,
                        'transactionTokenId'    => '',
                        'status'                => 'SUCCESS',
                        'raw'                   => $linea,
                        'origen'                => 'CARGA MASIVA'
                    ]);

                $num_referencias_afectadas += 1;

                if (!$request->has('preview')) {
                    activity()
                        ->performedOn($referencia_pedido)
                        ->causedBy(Auth::user()->id)
                        ->log("Realizó el pago para el pedido {$pedido->serie}{$pedido->folio} desde la carga masiva");
                }

                //enviamos correo
                $pedido->sendPagoExitosoNotification();
            }

            $pedido->referencia_bbva = $referencia;

            if (!is_null($registro_pago)) {
                $pedido->fecha_pago = $registro_pago->created_at->format('Y-m-d');
                if ($registro_pago->origen == 'MANUAL') {
                    $pedido->fecha_pago .= ' (' . $registro_pago->raw . ')';
                }
            } else {
                $pedido->fecha_pago = $fecha;
                // $pedido->fecha_pago = NULL;
            }

            $pedido->importe_total = $importe_total;
            $c = collect([$pedido]);

            $c = $c->transform(function ($item) {
                $item->makeHidden('resumenes');
                $item->makeHidden('colegio');
                $item->makeHidden('envio');
                return $item;
            });

            array_push($pedidos, $c->first());
        }

        DB::commit();

        fclose($file);

        //TODO: Eliminar el archivo antes de terminar la funcion

        $referencia_pedido = new ReferenciaPedido;

        $message = 'referencias leidas del .exp: ' . $num_referencias_encontradas . ', referencias de pedidos: ' . $num_referencias_pedidos_encontrados . ', referencias afectadas: ' . $num_referencias_afectadas . ', pedidos no encontrados: ' . $num_pedidos_no_encontrados;

        if (!$request->has('preview')) {
            activity()
                ->performedOn($referencia_pedido)
                ->causedBy(Auth::user()->id)
                ->log("Realizó carga masiva de referencias BBVA. Message: {$message}");
        }

        return response()->json(['success' => true, 'message' => $message, 'pedidos_no_identificados' => $num_pedidos_no_encontrados , 'pedidos' => $pedidos], 200);
    }

    public function tablaReferencias(Request $request)
    {

        if ($request->inicio != null && $request->fin != null) {
            $referencias = ReferenciaPedido::where('tipo', 'BANCARIA')->whereBetween('created_at', [$request->inicio, $request->fin])->get();

            return view('private.tabla_referencias')
                ->with('referencias', $referencias);
        } else {
            return view('private.tabla_referencias')
                ->with('referencias', null);
        }
    }
}
