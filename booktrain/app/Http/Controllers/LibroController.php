<?php

namespace App\Http\Controllers;

use App\DataTables\LibroDataTable;
//use App\Http\Requests;
use App\Http\Requests\CreateLibroRequest;
use App\Http\Requests\UpdateLibroRequest;
use App\Repositories\LibroRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\Libro;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Imports\UploadLibrosImport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class LibroController extends AppBaseController
{
    /** @var  LibroRepository */
    private $libroRepository;

    public function __construct(LibroRepository $libroRepo)
    {
        $this->libroRepository = $libroRepo;
    }

    /**
     * Display a listing of the Libro.
     *
     * @param LibroDataTable $libroDataTable
     * @return Response
     */
    public function index(LibroDataTable $libroDataTable)
    {
        return $libroDataTable->render('libros.index');
    }

    public function getLibros()
    {
        return view('private.admin.libros');
    }

    /**
     * Show the form for creating a new Libro.
     *
     * @return Response
     */
    public function create()
    {
        return view('libros.create');
    }

    /**
     * Store a newly created Libro in storage.
     *
     * @param CreateLibroRequest $request
     *
     * @return Response
     */
    public function store(CreateLibroRequest $request)
    {
        $input = $request->all();

        $libro = $this->libroRepository->create($input);

        Flash::success('Libro saved successfully.');

        return redirect(route('libros.index'));
    }

    /**
     * Display the specified Libro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            Flash::error('Libro not found');

            return redirect(route('libros.index'));
        }

        return view('libros.show')->with('libro', $libro);
    }

    /**
     * Show the form for editing the specified Libro.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            Flash::error('Libro not found');

            return redirect(route('libros.index'));
        }

        return view('libros.edit')->with('libro', $libro);
    }

    /**
     * Update the specified Libro in storage.
     *
     * @param  int              $id
     * @param UpdateLibroRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLibroRequest $request)
    {
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            Flash::error('Libro not found');

            return redirect(route('libros.index'));
        }

        $libro = $this->libroRepository->update($request->all(), $id);

        Flash::success('Libro updated successfully.');

        return redirect(route('libros.index'));
    }

    /**
     * Remove the specified Libro from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $libro = $this->libroRepository->find($id);

        if (empty($libro)) {
            Flash::error('Libro not found');

            return redirect(route('libros.index'));
        }

        $this->libroRepository->delete($id);

        Flash::success('Libro deleted successfully.');

        return redirect(route('libros.index'));
    }

    /*
    * Recibe un Excel con la lista de libros que hay que modificar/crear
    */
    public function uploadLibros(Request $request)
    {
        $nombre_archivo = "";
        foreach ($request->files as $nombre => $contenido) {
            if (!$contenido->isValid()) {
                Log::error("Error al subir el archivo {$nombre} es invalido");
                return response()->json(['success' => false, 'msg' => 'El archivo es invalido'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'El archivo es invalido');
            }

            if (!($contenido->guessExtension() == "xlsx" || $contenido->guessExtension() == "xls" || $contenido->guessExtension() == "csv")) {
                Log::error("Error al subir el archivo, solo se aceptan hojas con formato xslx, xsl o csv");
                return response()->json(['success' => false, 'msg' => 'Solo se aceptan hojas con formato xslx, xsl o csv'], 503);
                //return view('private.catalogo.update')->with('error', true)->with('mensaje', 'Solo se aceptan hojas con formato xslx, xsl o csv');
            }

            //guardamos la imagen en el disco
            $nombre_archivo = sha1(time() . "-" . $nombre) . '.' . $contenido->guessExtension();
            \Storage::disk('local')->put($nombre_archivo, \File::get($contenido));
        }

        $nombre_archivo = \Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $nombre_archivo;

        //$response = $this->excel->import(new UploadPedidosEntregadosImport, $nombre_archivo);
        //App\Imports\UploadLibrosImport

        $array = Excel::toCollection(new UploadLibrosImport, $nombre_archivo);
        $libros = [];
        $libros_no_identificados = 0;
        $libros_actualizados = 0;
        $libros_creados = 0;

        foreach ($array->first() as $element) {
            //$elementx = collect((object) $element);
            //validamos el formato: $col_concepto = CE00000000000100011279
            $isbn = $element[0];
            $nombre = $element[1];

            $libro = Libro::where("isbn", $isbn)->first();

            if (is_null($libro)) {
                //libro no identificado, hay que crearlo

                //validamos el tamaño del ISBN
                $len = strlen($isbn);

                if (!($len == 13 || $len == 17)) {
                    // creamos un libro con error
                    $libros_no_identificados += 1;

                    $libro = new \stdClass();
                    $libro->id = 0;
                    $libro->nombre = $nombre;
                    $libro->editorial = '';
                    $libro->edicion = '';
                    $libro->isbn = $isbn;
                    $libro->activo = 0;
                    $libro->error = 1;
                    $libro->message = 'El ISBN no tiene el formato correcto';


                    array_push($libros, $libro);
                    continue;
                }

                //creamos el nuevo libro
                $libro = new Libro;
                $libro->nombre = $nombre;
                $libro->editorial = '';
                $libro->edicion = '';
                $libro->isbn = $isbn;
                $libro->activo = 1;

                $libro->save();
                $libro->error = 0;
                $libro->message = 'Libro creado';
                $libros_creados += 1;
            } else {
                //libro encontrado, hay que actualizar el nombre
                $libro->nombre = $nombre;
                $libro->save();
                $libros_actualizados += 1;
                $libro->error = 0;
                $libro->message = 'Nombre actualizado';
            }



            array_push($libros, $libro);
        }

        //TODO: Eliminar el archivo antes de terminar la funcion

        $libros = new Libro;
        activity()
            ->performedOn($libros)
            ->causedBy(Auth::user()->id)
            ->log('Realizó una carga de libros masiva.');
        return response()->json(['success' => true, 'message' => 'libros actualizados: ' . $libros_actualizados . ' libros no identificados: ' . $libros_no_identificados . ' libros creados: ' . $libros_creados, 'total_count' => count($libros), 'libros' => $libros], 200);
        //return view('private.catalogo.update')->with('mensaje', 'Catalogo Actualizado Correctamente!!');
    }
}
