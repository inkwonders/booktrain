<?php

namespace App\Http\Controllers;

use App\Models\TicketUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    public function index(){
        $usuario = Auth::user();

        $tickets = TicketUsuario::all()->sortByDesc('id');

        return view('private.tickets.index')->with('usuario', $usuario)->with('tickets', $tickets);
    }

    public function show($id){
        return view('private.tickets.show');
    }

}
