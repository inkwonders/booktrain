<?php

namespace App\Http\Controllers;

use App\DataTables\TicketArchivoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTicketArchivoRequest;
use App\Http\Requests\UpdateTicketArchivoRequest;
use App\Repositories\TicketArchivoRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TicketArchivoController extends AppBaseController
{
    /** @var  TicketArchivoRepository */
    private $ticketArchivoRepository;

    public function __construct(TicketArchivoRepository $ticketArchivoRepo)
    {
        $this->ticketArchivoRepository = $ticketArchivoRepo;
    }

    /**
     * Display a listing of the TicketArchivo.
     *
     * @param TicketArchivoDataTable $ticketArchivoDataTable
     * @return Response
     */
    public function index(TicketArchivoDataTable $ticketArchivoDataTable)
    {
        return $ticketArchivoDataTable->render('ticket_archivos.index');
    }

    /**
     * Show the form for creating a new TicketArchivo.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_archivos.create');
    }

    /**
     * Store a newly created TicketArchivo in storage.
     *
     * @param CreateTicketArchivoRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketArchivoRequest $request)
    {
        $input = $request->all();

        $ticketArchivo = $this->ticketArchivoRepository->create($input);

        Flash::success('Ticket Archivo saved successfully.');

        return redirect(route('ticketArchivos.index'));
    }

    /**
     * Display the specified TicketArchivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            Flash::error('Ticket Archivo not found');

            return redirect(route('ticketArchivos.index'));
        }

        return view('ticket_archivos.show')->with('ticketArchivo', $ticketArchivo);
    }

    /**
     * Show the form for editing the specified TicketArchivo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            Flash::error('Ticket Archivo not found');

            return redirect(route('ticketArchivos.index'));
        }

        return view('ticket_archivos.edit')->with('ticketArchivo', $ticketArchivo);
    }

    /**
     * Update the specified TicketArchivo in storage.
     *
     * @param  int              $id
     * @param UpdateTicketArchivoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketArchivoRequest $request)
    {
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            Flash::error('Ticket Archivo not found');

            return redirect(route('ticketArchivos.index'));
        }

        $ticketArchivo = $this->ticketArchivoRepository->update($request->all(), $id);

        Flash::success('Ticket Archivo updated successfully.');

        return redirect(route('ticketArchivos.index'));
    }

    /**
     * Remove the specified TicketArchivo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketArchivo = $this->ticketArchivoRepository->find($id);

        if (empty($ticketArchivo)) {
            Flash::error('Ticket Archivo not found');

            return redirect(route('ticketArchivos.index'));
        }

        $this->ticketArchivoRepository->delete($id);

        Flash::success('Ticket Archivo deleted successfully.');

        return redirect(route('ticketArchivos.index'));
    }
}
