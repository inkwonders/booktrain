<?php

namespace App\Http\Controllers;

use App\DataTables\DireccionEntregaDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateDireccionEntregaRequest;
use App\Http\Requests\UpdateDireccionEntregaRequest;
use App\Repositories\DireccionEntregaRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DireccionController extends AppBaseController
{
    /** @var  DireccionEntregaRepository */
    private $direccionEntregaRepository;

    public function __construct(DireccionEntregaRepository $direccionEntregaRepo)
    {
        $this->direccionEntregaRepository = $direccionEntregaRepo;
    }

    /**
     * Display a listing of the DireccionEntrega.
     *
     * @param DireccionEntregaDataTable $direccionEntregaDataTable
     * @return Response
     */
    public function index(DireccionEntregaDataTable $direccionEntregaDataTable)
    {
        return $direccionEntregaDataTable->render('direccion_entregas.index');
    }

    /**
     * Show the form for creating a new DireccionEntrega.
     *
     * @return Response
     */
    public function create()
    {
        return view('direccion_entregas.create');
    }

    /**
     * Store a newly created DireccionEntrega in storage.
     *
     * @param CreateDireccionEntregaRequest $request
     *
     * @return Response
     */
    public function store(CreateDireccionEntregaRequest $request)
    {
        $input = $request->all();

        $direccionEntrega = $this->direccionEntregaRepository->create($input);

        Flash::success('Direccion Entrega saved successfully.');

        return redirect(route('direccionEntregas.index'));
    }

    /**
     * Display the specified DireccionEntrega.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            Flash::error('Direccion Entrega not found');

            return redirect(route('direccionEntregas.index'));
        }

        return view('direccion_entregas.show')->with('direccionEntrega', $direccionEntrega);
    }

    /**
     * Show the form for editing the specified DireccionEntrega.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            Flash::error('Direccion Entrega not found');

            return redirect(route('direccionEntregas.index'));
        }

        return view('direccion_entregas.edit')->with('direccionEntrega', $direccionEntrega);
    }

    /**
     * Update the specified DireccionEntrega in storage.
     *
     * @param  int              $id
     * @param UpdateDireccionEntregaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDireccionEntregaRequest $request)
    {
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            Flash::error('Direccion Entrega not found');

            return redirect(route('direccionEntregas.index'));
        }

        $direccionEntrega = $this->direccionEntregaRepository->update($request->all(), $id);

        Flash::success('Direccion Entrega updated successfully.');

        return redirect(route('direccionEntregas.index'));
    }

    /**
     * Remove the specified DireccionEntrega from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $direccionEntrega = $this->direccionEntregaRepository->find($id);

        if (empty($direccionEntrega)) {
            Flash::error('Direccion Entrega not found');

            return redirect(route('direccionEntregas.index'));
        }

        $this->direccionEntregaRepository->delete($id);

        Flash::success('Direccion Entrega deleted successfully.');

        return redirect(route('direccionEntregas.index'));
    }
}
