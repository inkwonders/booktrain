<?php

namespace App\Http\Controllers;

use Response;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use League\Fractal\Manager;
use App\Models\SeccionColegio;
use App\Transformers\Serializer;
use League\Fractal\Resource\Collection;
use App\DataTables\SeccionColegioDataTable;
use App\Http\Controllers\AppBaseController;
use App\Repositories\SeccionColegioRepository;
use App\Transformers\SeccionesColegioTransformer;
use App\Http\Requests\CreateSeccionColegioRequest;
use App\Http\Requests\UpdateSeccionColegioRequest;

class SeccionColegioController extends AppBaseController
{
    /** @var  SeccionColegioRepository */
    private $seccionColegioRepository;

    public function __construct(SeccionColegioRepository $seccionColegioRepo)
    {
        $this->seccionColegioRepository = $seccionColegioRepo;
    }

    /**
     * Display a listing of the SeccionColegio.
     *
     * @param SeccionColegioDataTable $seccionColegioDataTable
     * @return Response
     */
    public function index(SeccionColegioDataTable $seccionColegioDataTable)
    {
        return $seccionColegioDataTable->render('seccion_colegios.index');
    }

    /**
     * Show the form for creating a new SeccionColegio.
     *
     * @return Response
     */
    public function create()
    {
        return view('seccion_colegios.create');
    }

    /**
     * Store a newly created SeccionColegio in storage.
     *
     * @param CreateSeccionColegioRequest $request
     *
     * @return Response
     */
    public function store(CreateSeccionColegioRequest $request)
    {
        $input = $request->all();

        $seccionColegio = $this->seccionColegioRepository->create($input);

        Flash::success('Seccion Colegio saved successfully.');

        return redirect(route('seccionColegios.index'));
    }

    /**
     * Display the specified SeccionColegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            Flash::error('Seccion Colegio not found');

            return redirect(route('seccionColegios.index'));
        }

        return view('seccion_colegios.show')->with('seccionColegio', $seccionColegio);
    }

    /**
     * Show the form for editing the specified SeccionColegio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            Flash::error('Seccion Colegio not found');

            return redirect(route('seccionColegios.index'));
        }

        return view('seccion_colegios.edit')->with('seccionColegio', $seccionColegio);
    }

    /**
     * Update the specified SeccionColegio in storage.
     *
     * @param  int              $id
     * @param UpdateSeccionColegioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeccionColegioRequest $request)
    {
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            Flash::error('Seccion Colegio not found');

            return redirect(route('seccionColegios.index'));
        }

        $seccionColegio = $this->seccionColegioRepository->update($request->all(), $id);

        Flash::success('Seccion Colegio updated successfully.');

        return redirect(route('seccionColegios.index'));
    }

    /**
     * Remove the specified SeccionColegio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $seccionColegio = $this->seccionColegioRepository->find($id);

        if (empty($seccionColegio)) {
            Flash::error('Seccion Colegio not found');

            return redirect(route('seccionColegios.index'));
        }

        $this->seccionColegioRepository->delete($id);

        Flash::success('Seccion Colegio deleted successfully.');

        return redirect(route('seccionColegios.index'));
    }


    public function transformSeccionesColegio()
    {

        $fractal = new Manager();


        $seccionesColegios = SeccionColegio::all();
        $seccionesColegios = $fractal->setSerializer(new Serializer())
            ->createData( new Collection( $seccionesColegios, new SeccionesColegioTransformer ) )->toArray();

        dd($seccionesColegios);

    }

}
