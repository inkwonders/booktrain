<?php

namespace App\Http\Controllers;

use Image;
use Response;
use Exception;
use Carbon\Carbon;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use League\Fractal\Manager;
use App\Models\TicketMotivo;
use Illuminate\Http\Request;
use App\Models\TicketArchivo;
use App\Models\TicketUsuario;
use App\Transformers\Serializer;
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Auth;
use League\Fractal\Resource\Collection;
use Illuminate\Support\Facades\Validator;
use App\DataTables\TicketUsuarioDataTable;
use App\Http\Controllers\AppBaseController;
use App\Repositories\TicketUsuarioRepository;
use App\Transformers\TicketEntradaTransformer;
use App\Transformers\TicketUsuarioTransformer;
use App\Http\Requests\CreateTicketUsuarioRequest;
use App\Http\Requests\UpdateTicketUsuarioRequest;
use Illuminate\Support\Facades\Log;

class TicketUsuarioController extends AppBaseController
{
    /** @var  TicketUsuarioRepository */
    private $ticketUsuarioRepository;

    public function __construct(TicketUsuarioRepository $ticketUsuarioRepo)
    {
        $this->ticketUsuarioRepository = $ticketUsuarioRepo;
    }

    /**
     * Display a listing of the TicketUsuario.
     *
     * @param TicketUsuarioDataTable $ticketUsuarioDataTable
     * @return Response
     */
    public function index()
    {
        return view('private.soporte', ['motivos' => TicketMotivo::all()]);
    }

    /**
     * Show the form for creating a new TicketUsuario.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_usuarios.create');
    }

    /**
     * Store a newly created TicketUsuario in storage.
     *
     * @param CreateTicketUsuarioRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {



        $usuario = Auth::user();

        $reglas = [
            'titulo'   => 'sometimes|required|string',
            'motivo'      => 'required',
            'descripcion'  => 'sometimes|required|string',
            'telefono'  => 'required|required|integer',
            'filenames.*' =>  'mimes:jpeg,png,jpg,gif,svg'
        ];

        $mensajes = [
            'titulo.required'       => 'Debe incluir un título',
            'descripcion.required'  => 'La descripción de su caso es necesario',
            'motivo.required'  => 'Por favor seleccione un motivo',
            'telefono.required'  => 'Por favor ingrese un teléfono',
            'telefono.integer'  => 'El teléfono debe ser solo numérico',
            'filenames.mimes'  => 'Solo se permite subir imagenes como evidencia adjunta, por favor selecciona una imagen.',
            'filenames.*.mimes' => 'Solo se permite subir imagenes como evidencia adjunta.',
            // 'filenames' => 'Solo se permite subir imagenes como evidencia adjunta, por favor selecciona una imagen.',
        ];

        $validator = Validator::make(
            $request->all(),
            $reglas,
            $mensajes
        );

        if($validator->fails()){
            return redirect()->to('/tickets/')->withErrors(['error' => $validator->errors()->first(),'success' => false]);
        }


        $ticket = new TicketUsuario();
        $ticket->titulo = $request->titulo;
        $ticket->folio = $usuario->id.'_'.Carbon::now()->format("YmdHmsu");
        $ticket->ticket_motivos_id = $request->motivo;
        $ticket->descripcion = $request->descripcion;
        $ticket->usuario_id = $usuario->id;
        $ticket->telefono = $request->telefono;
        $ticket->save();

        if($request->filenames != "" || $request->filenames!=null){
        foreach ($request->filenames as $nombre => $contenido) {
            //guardamos la imagen en el disco
            $nombre_imagen = $ticket->id.'_'.$usuario->id.'_'.Carbon::now()->format("YmdHmsu"). '.' . $contenido->guessExtension();
            $originalImage= \File::get($contenido);
            $imagen = Image::make($originalImage);
            $image_path = \Storage::disk('tickets')->path('') . '/' .$nombre_imagen;
            $imagen->save($image_path);
            // $imageNombre = $contenido->getClientOriginalName();
            try {
                $file= new TicketArchivo();
                $file->archivo_nombre_original =  $contenido->getClientOriginalName();
                $file->ticket_usuario_id =   $ticket->id;
                $file->url =  "/storage/images/tickets_files/$nombre_imagen";
                $file->archivo = $nombre_imagen;
                $file->tipo_archivo = $contenido->guessExtension();
                $file->save();

                } catch (Exception $e) {
                    return redirect()->to('/tickets/')->withErrors(['Actualización Correcta!!']);
                }
        }
      }
        try {
            auth()->user()->sendTicketCreatedSuccessNotification($ticket);
        } catch(\Exception $e) {
            Log::info("TicketsUsuarioController:store", ["error:" => $e]);
        }
        // $ticket->user->sendTicketCreatedSuccessNotification();
         return redirect()->to('/tickets/')->withErrors(['Actualización Correcta!!']);
    }

    /**
     * Display the specified TicketUsuario.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $usuario = Auth::user();
        if($usuario->hasRole(['Admin']))
        {
            $ticketUsuario = TicketUsuario::where('id', $id)->first();
        }
        else
        {
            $ticketUsuario = TicketUsuario::where([['id', '=', $id],['usuario_id', '=', $usuario->id]])->first();
        }



        $fractal = new Manager();

        $ticketInformacion = (Object) $fractal->setSerializer(new Serializer())
            ->parseIncludes('usuario')
            ->createData(new Item($ticketUsuario, new TicketUsuarioTransformer))->toArray();

       // return response()->json(['success' => true, 'data' => $ticketInformacion], 200);

        // if (empty($ticketUsuario)) {
        //     Flash::error('Ticket Usuario not found');

        //     return redirect(route('ticketUsuarios.index'));
        // }
        //  return view('ticket_usuarios.show')->with('ticketUsuario', $ticketUsuario);
        return view('private.tickets.show')->with('ticketInformacion', $ticketInformacion);
    }

    /**
     * Show the form for editing the specified TicketUsuario.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($ticketUsuario)) {
            Flash::error('Ticket Usuario not found');

            return redirect(route('ticketUsuarios.index'));
        }

        return view('ticket_usuarios.edit')->with('ticketUsuario', $ticketUsuario);
    }

    /**
     * Update the specified TicketUsuario in storage.
     *
     * @param  int              $id
     * @param UpdateTicketUsuarioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketUsuarioRequest $request)
    {
        $ticketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($ticketUsuario)) {
            Flash::error('Ticket Usuario not found');

            return redirect(route('ticketUsuarios.index'));
        }

        $ticketUsuario = $this->ticketUsuarioRepository->update($request->all(), $id);

        Flash::success('Ticket Usuario updated successfully.');

        return redirect(route('ticketUsuarios.index'));
    }

    /**
     * Remove the specified TicketUsuario from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketUsuario = $this->ticketUsuarioRepository->find($id);

        if (empty($ticketUsuario)) {
            Flash::error('Ticket Usuario not found');

            return redirect(route('ticketUsuarios.index'));
        }

        $this->ticketUsuarioRepository->delete($id);

        Flash::success('Ticket Usuario deleted successfully.');

        return redirect(route('ticketUsuarios.index'));
    }

    public function cerrar($id)
    {
        $status_cerrado = 0;

        $ticket = TicketUsuario::find($id);
        $ticket->status = $status_cerrado;
        $ticket->save();

        return redirect()->route('ticket.show', ['id' => $id]);

        $fractal = new Manager();

        $entradasInformacion = $fractal->setSerializer(new Serializer())
            ->parseIncludes('usuario')
            ->createData(new Collection($entradasTicket, new TicketEntradaTransformer))->toArray();

        $datos = $request->all();

        $reglas = [
            'calificacion' => 'required|integer',
            'comentario_calificacion' => 'required|string',
        ];

        $validador = Validator::make($datos, $reglas);

        if ($validador->fails()) {
            return $this->sendError($validador->errors()->first(),400);
        }

        $ticket = TicketUsuario::find($id);
        $ticket->calificacion = $request->calificacion;
        $ticket->comentario_calificacion = $request->comentario_calificacion;
        $ticket->fecha_calificacion = date('Y-m-d H:i:s');
        $ticket->save();

        return redirect()->route('ticket.show', ['id' => $id]);

    }

}
