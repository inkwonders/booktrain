<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use App\Models\Colegio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\TipoMovimientoAlmacen;
use Illuminate\Support\Facades\Validator;
use App\Models\Cupon;

class CajaController extends AppBaseController
{
    /**
     * @api{post} /api/venta_movil/caja/abrir Abrir caja
     * @apiGroup VentaMovil
     * @apiDescription Abre una caja para iniciar la venta movil
     *
     * @apiBody {Number {0..}} billete_1000 Cantidad de billetes de 1000 pesos MXN
     * @apiBody {Number {0..}} billete_500 Cantidad de billetes de 500 pesos MXN
     * @apiBody {Number {0..}} billete_200 Cantidad de billetes de 200 pesos MXN
     * @apiBody {Number {0..}} billete_100 Cantidad de billetes de 100 pesos MXN
     * @apiBody {Number {0..}} billete_50 Cantidad de billetes de 50 pesos MXN
     * @apiBody {Number {0..}} billete_20 Cantidad de billetes de 20 pesos MXN
     * @apiBody {Number {0..}} conteo_monedas Cantidad de monedas físicas
     * @apiBody {Number {0..}} monto_monedas Sumatoria del valor total de las monedas físicas
     * @apiBody {String} [observaciones] Información extra relevante al abrir la caja
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "billete_1000" : 0,
     *   "billete_500" : 0,
     *   "billete_200" : 1,
     *   "billete_100" : 1,
     *   "billete_50" : 2,
     *   "billete_20" : 5,
     *   "conteo_monedas": 5,
     *   "monto_monedas": 13.5
     *   "observaciones": "Un billete de 100 está roto",
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": {
     *         "id": 11,
     *         "monto_inicial": 513.5,
     *         "usuario": "Luz Aguirre",
     *         "colegio": "COLEGIO BOOKTRAIN",
     *         "fecha_apertura": "28-10-2021 16:43:15"
     *     },
     *     "message": "La caja se abrió correctamente con el id 11 y el monto inicial de 513.5"
     * }
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     * @apiSuccess (200) {Object} data Objeto con información sobre la operación
     * @apiSuccess (200) {Number} data.id Id de la caja abierta
     * @apiSuccess (200) {Number} data.monto_inicial Cantidad en efectivo disponible en la caja abierta
     * @apiSuccess (200) {String} data.usuario Nombre del usuario que abrió la caja
     * @apiSuccess (200) {String} data.colegio Nombre del colegio que abrió la caja
     * @apiSuccess (200) {String} data.fecha_apertura Fecha en que fue abierta la caja
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El campo billete 200 es obligatorio."
     * }
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario ya tiene una caja abierta: 11"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     */
    public function abrir(Request $request) {
        $rules = [
            'billete_1000'   => 'required|numeric',
            'billete_500'    => 'required|numeric',
            'billete_200'    => 'required|numeric',
            'billete_100'    => 'required|numeric',
            'billete_50'     => 'required|numeric',
            'billete_20'     => 'required|numeric',
            'conteo_monedas' => 'required|numeric',
            'monto_monedas'  => 'required|numeric',
            'observaciones'  => 'sometimes',
        ];

        $validator = Validator::make(
            $request->all(),
            $rules
        );

        Log::debug('CajaController@abrir', ['request' => $request->all(), 'errores' => $validator->errors()]);

        if( $validator->fails() )
            return $this->sendError($validator->errors()->first(), 400);

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        DB::beginTransaction();
        try {
            // Si no hay una caja, la creamos
            if ( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last())
                $caja = $usuario->cajas()->create([
                    'colegio_id'     => $colegio->id,
                    'status'         => 'ABIERTA',
                    'fecha_apertura' => now()
                ]);
            else
                // La caja ya existe
                if( $caja->status == 'ABIERTA' )
                    return $this->sendError("El usuario ya tiene una caja abierta: {$caja->id}", 400);
                else
                    $caja->update([
                        'status'         => 'ABIERTA',
                        'fecha_apertura' => now()
                    ]);

            // Creamos la operación de apertura
            $apertura = $caja->operaciones()->create([
                'tipo'           => 'APERTURA',
                'billete_1000'   => $request->billete_1000,
                'billete_500'    => $request->billete_500,
                'billete_200'    => $request->billete_200,
                'billete_100'    => $request->billete_100,
                'billete_50'     => $request->billete_50,
                'billete_20'     => $request->billete_20,
                'conteo_monedas' => $request->conteo_monedas,
                'monto_monedas'  => $request->monto_monedas,
                'observaciones'  => $request->input('observaciones', ''),
            ]);

            DB::commit();

            return $this->sendResponse([
                'id'             => $caja->id,
                'monto_inicial'  => $apertura->total,
                'usuario'        => $usuario->nombre_completo,
                'colegio'        => $caja->colegio->nombre,
                'fecha_apertura' => $caja->fecha_apertura->format('d-m-Y H:i:s'),
            ], "La caja se abrió correctamente con el id {$caja->id} y el monto inicial de {$apertura->total}");
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('CajaController@abrir Error al registrar la operación', ['message' => $th->getMessage(), 'line' => $th->getLine()]);

            return $this->sendError("Error en la operación, intente más tarde: {$th->getMessage()} @ {$th->getLine()}", 503);
        }
    }

    /**
     * @api{post} /api/venta_movil/caja/cerrar Cerrar caja
     * @apiGroup VentaMovil
     * @apiDescription Cierra una caja asociada a un usuario de venta movil
     *
     * @apiBody {Number {0..}} billete_1000 Cantidad de billetes de 1000 pesos MXN
     * @apiBody {Number {0..}} billete_500 Cantidad de billetes de 500 pesos MXN
     * @apiBody {Number {0..}} billete_200 Cantidad de billetes de 200 pesos MXN
     * @apiBody {Number {0..}} billete_100 Cantidad de billetes de 100 pesos MXN
     * @apiBody {Number {0..}} billete_50 Cantidad de billetes de 50 pesos MXN
     * @apiBody {Number {0..}} billete_20 Cantidad de billetes de 20 pesos MXN
     * @apiBody {Number {0..}} conteo_monedas Cantidad de monedas físicas
     * @apiBody {Number {0..}} monto_monedas Sumatoria del valor total de las monedas físicas
     * @apiBody {String} [observaciones] Información extra relevante al abrir la caja
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     * @apiSuccess (200) {Object} data Objeto con información sobre la operación
     * @apiSuccess (200) {Number} data.total Cantidad total en efectivo
     * @apiSuccess (200) {Number} data.delta Diferencia en efectivo de la apertura y el cierre de la caja
     * @apiSuccess (200) {Number} data.fecha_cierre Fecha en que se realizó la operación de cierre de la caja
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "billete_1000" : 2,
     *   "billete_500" : 3,
     *   "billete_200" : 4,
     *   "billete_100" : 3,
     *   "billete_50" : 10,
     *   "billete_20" : 25,
     *   "conteo_monedas": 55,
     *   "monto_monedas": 853.5,
     *   "observaciones": "Las monedas están en un bote de kitty"
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": {
     *         "monto": 6453.5,
     *         "delta": 5940,
     *         "fecha": "28-10-2021 16:48:07"
     *     },
     *     "message": "La caja 11 se ha cerrado exitosamente"
     * }
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El campo billete 200 es obligatorio."
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene cajas abiertas"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     */
    public function cerrar(Request $request) {
        $rules = [
            'billete_1000'   => 'required|numeric',
            'billete_500'    => 'required|numeric',
            'billete_200'    => 'required|numeric',
            'billete_100'    => 'required|numeric',
            'billete_50'     => 'required|numeric',
            'billete_20'     => 'required|numeric',
            'conteo_monedas' => 'required|numeric',
            'monto_monedas'  => 'required|numeric',
            'observaciones'  => 'sometimes',
        ];

        $validator = Validator::make(
            $request->all(),
            $rules
        );

        Log::debug('CajaController@cerrar', ['request' => $request->all(), 'errores' => $validator->errors()]);

        if( $validator->fails() )
            return $this->sendError($validator->errors()->first());

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene cajas asignadas', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('El usuario no tiene cajas abiertas', 412);

        DB::beginTransaction();
        try {
            $caja->update([
                'status' => 'CERRADA',
                'fecha_cierre' => now()
            ]);

            $apertura = $caja->operaciones->last();

            $cierre = $caja->operaciones()->create([
                'tipo'           => 'CIERRE',
                'billete_1000'   => $request->billete_1000,
                'billete_500'    => $request->billete_500,
                'billete_200'    => $request->billete_200,
                'billete_100'    => $request->billete_100,
                'billete_50'     => $request->billete_50,
                'billete_20'     => $request->billete_20,
                'conteo_monedas' => $request->conteo_monedas,
                'monto_monedas'  => $request->monto_monedas,
                'observaciones'  => $request->input('observaciones', ''),
            ]);

            $delta = $cierre->total - $apertura->total;

            DB::commit();

            return $this->sendResponse([
                "monto" => $cierre->total,
                "delta" => $delta,
                "fecha" => $caja->fecha_cierre->format('d-m-Y H:i:s')
            ], "La caja {$caja->id} se ha cerrado exitosamente");
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('CajaController@cerrar Error al registrar la operación', ['message' => $th->getMessage(), 'line' => $th->getLine()]);

            return $this->sendError("Error en la operación, intente más tarde: {$th->getMessage()} @ {$th->getLine()}", 503);
        }
    }

    /**
     * @api{get} /api/venta_movil/configuracion Configuración de la caja
     * @apiGroup VentaMovil
     * @apiDescription Devuelve la configuración del colegio para la caja asociada al usuario: Libros, Grados, secciones y paquetes, Métodos de pago offline, Logotipo y Avisos
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": {
     *         "id": 1,
     *         "nombre": "COLEGIO BOOKTRAIN",
     *         "codigo": "CC1111",
     *         "logo": "http://booktrain.com.mx/storage/logocolegiobooktrain.jpg",
     *         "avisos": [
     *             "Aquí podrás publicar la información que los padres de familia deban conocer",
     *             "Podrás publicar lugares y fechas de entrega.",
     *             "Contamos con 3 pizarrones para publicar información.",
     *             "Aviso 4"
     *         ],
     *         "grados": [
     *             {
     *                 "id": 1,
     *                 "nombre": "Kinder",
     *                 "niveles": [
     *                     {
     *                         "id": 1,
     *                         "nombre": "KPrimero A",
     *                         "paquetes": [
     *                             {
     *                               "id": 1,
     *                               "libros": [
     *                                     { "id": 1, "nombre": "K1 Diccionario 1 Básico Escolar", "isbn": "AAAA1111", "precio": 22, "obligatorio": 1 },
     *                                     { "id": 2, "nombre": "K1 Léeme SB + WB Pk 1", "isbn": "BBBB2222", "precio": 110, "obligatorio": 0 },
     *                                     { "id": 3, "nombre": "K1 Español 1 ME 2011", "isbn": "CCCC3333", "precio": 120, "obligatorio": 0 }
     *                                 ]
     *                             },
     *                             {
     *                               "id": 394,
     *                               "libros": [
     *                                     { "id": 23, "nombre": "Pre DIBUJO, TRAZO Y APRENDO 1", "isbn": "KKKK1111", "precio": 500, "obligatorio": 1 }
     *                                 ]
     *                             }
     *                         ]
     *                     },
     *                     {
     *                         "id": 2,
     *                         "nombre": "KSegundo A",
     *                         "paquetes": [
     *                             {
     *                               "id": 2,
     *                               "libros": [
     *                                     { "id": 4, "nombre": "K2 Diccionario 2 Básico Escolar", "isbn": "AAAA1111", "precio": 200, "obligatorio": 0 },
     *                                     { "id": 5, "nombre": "K2 Léeme SB + WB Pk 2", "isbn": "BBBB2222", "precio": 210, "obligatorio": 1 },
     *                                     { "id": 6, "nombre": "K2 Español 2 ME 2011", "isbn": "CCCC3333", "precio": 220, "obligatorio": 0 }
     *                                 ]
     *                             }
     *                         ]
     *                     },
     *                     {
     *                         "id": 3,
     *                         "nombre": "KTercero AAA",
     *                         "paquetes": [
     *                             {
     *                               "id": 3,
     *                               "libros": [
     *                                     { "id": 7, "nombre": "K3 Diccionario 3 Básico Escolar :)", "isbn": "AAAA1111", "precio": 300, "obligatorio": 0 },
     *                                     { "id": 8, "nombre": "K3 Léeme SB + WB Pk 3", "isbn": "BBBB2222", "precio": 310, "obligatorio": 0 },
     *                                     { "id": 9, "nombre": "K3 Español 3 ME 2011", "isbn": "CCCC3333", "precio": 320, "obligatorio": 1 }
     *                                 ]
     *                             },
     *                             {
     *                               "id": 395,
     *                               "libros": [
     *                                     { "id": 23, "nombre": "Pre DIBUJO, TRAZO Y APRENDO 1", "isbn": "KKKK1111", "precio": 150, "obligatorio": 1 }
     *                                 ]
     *                             }
     *                         ]
     *                     }
     *                 ]
     *             },
     *             {
     *                 "id": 193,
     *                 "nombre": "Prueba",
     *                 "niveles": [
     *                     {
     *                         "id": 671,
     *                         "nombre": "1",
     *                         "paquetes": [
     *                             {
     *                               "id": 568,
     *                               "libros": [
     *                                     { "id": 917, "nombre": "Libro de prueba Eddy", "isbn": "29292929292929292", "precio": 29, "obligatorio": 0 }
     *                                 ]
     *                             }
     *                         ]
     *                     }
     *                 ]
     *             }
     *         ],
     *         "metodos_pago": [
     *             { "id": 6, "codigo": "EFECTIVO", "descripcion": "Pago en efectivo", "formas_pago": [{ "id": 1, "codigo": "PUE", "descripcion": "Pago en una exhibición", "comision": 0, "minimo": 0 }] },
     *             { "id": 7, "codigo": "TERMINAL", "descripcion": "Terminal bancaria", "formas_pago": [{ "id": 1, "codigo": "PUE", "descripcion": "Pago en una exhibición", "comision": 0, "minimo": 0 }] },
     *             { "id": 8, "codigo": "TRANSFERENCIA", "descripcion": "Transferencia electrónica", "formas_pago": [{ "id": 1, "codigo": "PUE", "descripcion": "Pago en una exhibición", "comision": 0, "minimo": 0 }] }
     *         ]
     *     },
     *     "message": "Configuración obtenida para el colegio CC1111"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function configuracionVentaOffline() {
        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        return $this->sendResponse([
            'id' => $colegio->id,
            'nombre' => $colegio->nombre,
            'codigo' => $colegio->codigo,
            'logo'   => url( $colegio->logo ),
            'avisos' => $colegio->notificaciones()->where('activo', 1)->get()->pluck('aviso'),
            'grados' => $colegio->secciones->map(function($seccion) use ($colegio) {
                return (Object) [
                    'id'      => $seccion->id,
                    'nombre'  => $seccion->nombre,
                    'niveles' => $seccion->niveles->map(function($nivel) use ($colegio) {
                        return (Object) [
                            'id'       => $nivel->id,
                            'nombre'   => $nivel->nombre,
                            'paquetes' => $nivel->paquetes->map(function($paquete) use ($colegio) {
                                return (Object) [
                                    'id'     => $paquete->id,
                                    'libros' => $paquete
                                        ->libros()
                                        ->wherePivot('colegio_id', $colegio->id)
                                        ->where('libros.activo', 1)
                                        ->get()
                                        ->map(function($libro) {
                                        return (Object) [
                                            'id' => $libro->id,
                                            'nombre' => $libro->nombre,
                                            'isbn' => $libro->isbn,
                                            'precio' => $libro->pivot->precio,
                                            'obligatorio' => $libro->pivot->obligatorio,
                                        ];
                                    }),
                                ];
                            })
                        ];
                    })
                ];
            }),
            'metodos_pago' => $colegio->metodosPago()
                ->wherePivot('activo', 1)
                ->wherePivot('contexto', 'PRESENCIAL')
                ->where('metodos_pagos.disponible', 1)
                ->get()
                ->map(function($metodo_pago) use ($colegio) {
                return (Object) [
                    'id'          => $metodo_pago->id,
                    'codigo'      => $metodo_pago->codigo,
                    'descripcion' => $metodo_pago->descripcion,
                    'formas_pago' => $metodo_pago->formasPago()
                        ->wherePivot('colegio_id', $colegio->id)
                        ->wherePivot('activo', 1)
                        ->get()
                        ->map(function($forma_pago) {
                        return (Object) [
                            'id'          => $forma_pago->id,
                            'codigo'      => $forma_pago->codigo,
                            'descripcion' => $forma_pago->descripcion,
                            'comision'    => $forma_pago->pivot->comision,
                            'minimo'      => $forma_pago->pivot->minimo,
                        ];
                    })
                ];
            }),
            'nombre' => $colegio->nombre,
        ], "Configuración obtenida para el colegio {$colegio->codigo}");
    }

    /**
     * @api{get} /api/venta_movil/caja/abrir Abrir caja
     * @apiGroup VentaMovil
     * @apiDescription Obtiene los cupones validos
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": [
     *         "1": {
     *             "cupon": "f828dd281234f73d650b4d8eb722bab41fedbe3743b4fb12b7bfb4c30c9e68f4bd3a4ee2b0bcbba1df69d64f93c9d7f8f4645f9d22e6026b85a32dd2e119ab25",
     *             "inicio": "2021-07-13T16:27:56.000000Z",
     *             "caducidad": null,
     *             "aplica_caducidad": false,
     *             "descuento": 100
     *         }
     *     ],
     *     "message": "Cupones encontrados: 1"
     * }
     *
     * @apiSuccess (200) {String} data.cupon Código del cupón
     * @apiSuccess (200) {String} data.inicio Fecha desde que es válido el cupón
     * @apiSuccess (200) {String} data.caducidad Fecha límite para usar el cupón
     * @apiSuccess (200) {Boolean} data.aplica_caducidad Indica si el cupón tiene fecha de caducidad
     * @apiSuccess (200) {Number} data.descuento Porcentaje de descuento que aplica el cupón
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El campo billete 200 es obligatorio."
     * }
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario ya tiene una caja abierta: 11"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     */
    public function cupones() {
        if(!$usuario = Auth::user())
            return $this->sendError('Usuario no autenticado', 401);

        if(!$usuario->roles->pluck('name')->contains('Venta Movil'))
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if(!$colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first())
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if(!$caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last())
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if($caja->status == 'CERRADA')
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        if(!$cupones = Cupon::get()->where('status', '=', 'DISPONIBLE'))
            return $this->sendError('No hay cupones disponibles', 412);

        $data = $cupones->map(function($cupon) {
            return (Object) [
                'codigo'            => hash("sha512", $cupon->codigo),  // Por seguridad se cifran los cupones
                'inicio'            => $cupon->created_at->format('U'),
                'caducidad'         => $cupon->caducidad != null? $cupon->caducidad->format('U') : '--',
                'aplica_caducidad'  => $cupon->caducidad != null,
                'descuento'         => $cupon->valor
            ];
        });

        return $this->sendResponse($data, "Cupones disponibles: {$cupones->count()}");
    }

    /**
     * @api{get} /api/venta_movil/caja Información de la caja
     * @apiGroup VentaMovil
     * @apiDescription Devuelve la información sobre la caja para la venta movil: Efectivo inicial, Pedidos realizados en la caja, Usuario asociado a la caja
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     * @apiSuccess (200) {Object} data Objeto con información sobre la caja
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": {
     *         "id": 11,
     *         "efectivo_inicial": 18343.5,
     *         "colegio": {
     *             "id": 1,
     *             "codigo": "CC1111"
     *         },
     *         "fecha_apertura": "2021-10-27 13:08:09",
     *         "usuario": {
     *             "id": 7,
     *             "email": "luz@inkwonders.com",
     *             "nombre": "Luz Aguirre",
     *             "rol": "Venta Movil"
     *         }
     *     },
     *     "message": "Información de la caja 11 obtenida correctamente"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "Usuario no autenticado"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene el rol de venta movil"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene un colegio válido asociado"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene asociada una caja para venta"
     * }
     *
     * @apiErrorExample {json} Error-Response: 403
     * HTTP/1.1 403 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function informacionCajaVentaMovil() {
        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 403);

        return $this->sendResponse([
            'id'               => $caja->id, // ID de la caja
            'efectivo_inicial' => $caja->operaciones()->where('tipo', 'APERTURA')->get()->last()->total,
            'colegio' => (Object) [
                'id'     => $colegio->id,
                'codigo' => $colegio->codigo
            ],
            'fecha_apertura' => $caja->fecha_apertura,
            'usuario' => (Object) [
                'id'     => $usuario->id,
                'email'  => $usuario->email,
                'nombre' => $usuario->nombre_completo,
                'rol'    => $usuario->roles->pluck('name')->join(', ', ' y '),
            ]
        ], "Información de la caja {$caja->id} obtenida correctamente");
    }

    /**
     * @api{get} /api/venta_movil/inventario Inventario del colegio
     * @apiGroup VentaMovil
     * @apiDescription Devuelve los libros asociados al colegio del vendedor con sus existencias, esta llamada permite sincronizar la interfaz de usuario con el servidor
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     * @apiSuccess (200) {Object[]} data Arreglo con los libros asignados al colegio
     *
     * @apiSuccessExample {json} Success-Response: Información obtenida 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "data": [
     *         { "id": 1, "nombre": "K1 Diccionario 1 Básico Escolar", "isbn": "AAAA1111", "existencias": -2, "bajo_pedido": 1 },
     *         { "id": 2, "nombre": "K1 Léeme SB + WB Pk 1", "isbn": "BBBB2222", "existencias": 9, "bajo_pedido": 0 },
     *         { "id": 3, "nombre": "K1 Español 1 ME 2011", "isbn": "CCCC3333", "existencias": 12, "bajo_pedido": 0 },
     *         { "id": 4, "nombre": "K2 Diccionario 2 Básico Escolar", "isbn": "AAAA1111", "existencias": 16, "bajo_pedido": 1 },
     *         { "id": 5, "nombre": "K2 Léeme SB + WB Pk 2", "isbn": "BBBB2222", "existencias": 16, "bajo_pedido": 0 },
     *         { "id": 6, "nombre": "K2 Español 2 ME 2011", "isbn": "CCCC3333", "existencias": 20, "bajo_pedido": 0 },
     *         { "id": 7, "nombre": "K3 Diccionario 3 Básico Escolar :)", "isbn": "AAAA1111", "existencias": 19, "bajo_pedido": 1 },
     *         { "id": 8, "nombre": "K3 Léeme SB + WB Pk 3", "isbn": "BBBB2222", "existencias": 9, "bajo_pedido": 0 },
     *         { "id": 9, "nombre": "K3 Español 3 ME 2011", "isbn": "CCCC3333", "existencias": 11, "bajo_pedido": 0 },
     *         { "id": 23, "nombre": "Pre DIBUJO, TRAZO Y APRENDO 1", "isbn": "KKKK1111", "existencias": 0, "bajo_pedido": 0 },
     *         { "id": 917, "nombre": "Libro de prueba Eddy", "isbn": "29292929292929292", "existencias": -1, "bajo_pedido": 1 }
     *     ],
     *     "message": "Se encontraron 11 libros"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "message": "Unauthenticated"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene el rol de venta movil"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene un colegio válido asociado"
     * }
     *
     * @apiErrorExample {json} Error-Response: 412
     * HTTP/1.1 412 Bad request
     * {
     *   "success": false,
     *   "message": "El usuario no tiene asociada una caja para venta"
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function obtenerInventario() {
        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        $inventario = $colegio
            ->libros()
            ->wherePivot('paquete_id', null)
            ->get()
            ->map(function($libro) {
                return (Object) [
                    'id'          => $libro->id,
                    'nombre'      => $libro->nombre,
                    'isbn'        => $libro->isbn,
                    'existencias' => $libro->pivot->stock,
                    'bajo_pedido' => $libro->pivot->bajo_pedido,
                ];
        });

        return $this->sendResponse($inventario, "Se encontraron {$inventario->count()} libros");
    }

    /**
     * @api{post} /api/venta_movil/pagar Registrar Pago
     * @apiGroup VentaMovil
     * @apiDescription Registra un pago y salida de mercancia a partir de una venta movil, guarda también si la venta fue realizada en modo offline y luego sincronizada con el servidor
     *
     * @apiBody {Number} pedido_id Id del pedido a pagar
     * @apiBody {Number} monto Cantidad recibida como pago
     * @apiBody {String} referencia Información sobre el pago, puede ser una referencia de depósito o transferencia
     * @apiBody {Boolean} modo_offline Indica si la operación se realizó mientras el punto de venta se encontraba sin conexión
     *
     * @apiParamExample {json} Request-Example:
     * {
     *   "pedido_id" : 123,
     *   "monto" : 655.5,
     *   "Referencia" : "Transferencia SPEI con referencia 12345678901234567890",
     *   "modo_offline" : 1,
     * }
     *
     * @apiSuccessExample {json} Success-Response: 200
     * HTTP/1.1 200 OK
     * {
     *     "success": true,
     *     "message": "¡Operación realizada correctamente!"
     * }
     *
     * @apiSuccess (200) {Boolean} success Indicador de éxito en la operación
     * @apiSuccess (200) {String} message Mensaje con información sobre la operación
     *
     * @apiErrorExample {json} Error-Response: 400
     * HTTP/1.1 400 Bad request
     * {
     *   "success": false,
     *   "message": "El campo pedido id es obligatorio."
     * }
     *
     * @apiErrorExample {json} Error-Response: 404
     * HTTP/1.1 404 Bad request
     * {
     *   "success": false,
     *   "message": "El pedido indicado no existe"
     * }
     *
     * @apiErrorExample {json} Error-Response: 503
     * HTTP/1.1 503 Service Unavailable
     * {
     *   "success": false,
     *   "message": "Error en la operación, intente más tarde: "
     * }
     *
     * @apiErrorExample {json} Error-Response: 401
     * HTTP/1.1 401 Bad request
     * {
     *   "success": false,
     *   "message": "La caja asociada al usuario ya ha sido cerrada"
     * }
     */
    public function registrarPago(Request $request) {
        Log::debug('CajaController@registrarPago', ['input' => $request->all()]);

        $request->validate([
            'pedido_id'    => 'required|exists:pedidos,id',
            'monto'        => 'required|numeric',
            'referencia'   => 'required',
            'modo_offline' => 'required|boolean',
        ]);

        if( ! $usuario = Auth::user() )
            return $this->sendError('Usuario no autenticado', 401);

        if( ! $usuario->roles->pluck('name')->contains('Venta Movil') )
            return $this->sendError('El usuario no tiene el rol de venta movil', 412);

        if( ! $colegio = Colegio::whereIn('codigo', $usuario->permissions->pluck('name') )->first() )
            return $this->sendError('El usuario no tiene un colegio válido asociado', 412);

        if( ! $caja = $usuario->cajas()->where('colegio_id', $colegio->id)->get()->last() )
            return $this->sendError('El usuario no tiene asociada una caja para venta', 412);

        if( $caja->status == 'CERRADA' )
            return $this->sendError('La caja asociada al usuario ya ha sido cerrada', 401);

        $pedido = Pedido::find($request->pedido_id);

        if( $pedido->status != 'CARRITO' )
            return $this->sendError('El pedido no puede ser procesado por que no se encuentra en el carrito', 412);

        DB::beginTransaction();

        try {
            // Asociamos el pedido a la caja
            $caja->pedidos()->syncWithoutDetaching([$request->pedido_id]);

            // Cambiamos el estado del pedido
            $pedido->update([
                'status' => 'PAGADO'
            ]);

            // Registramos el pago
            $pedido->pagos()->create([
                'amount'             => $request->monto,
                'referencia'         => $request->referencia,
                'origen'             => 'VENTA MOVIL',
                'source'             => $caja->usuario_id,
                'transactionTokenId' => $caja->id,
                'raw'                => json_encode( $request->all() ),
                'status'             => 'SUCCESS'
            ]);

            // Procesamos el pedido y afectamos inventarios sin enviar el correo de notificación de pago
            $pedido->procesar(false);

            DB::commit();

            // Notificamos al usuario sobre su pago
            $pedido->sendPagoExitosoVentaMovilNotification();
        } catch (\Throwable $th) {
            Log::error('CajaController@registrarPago', ['error' => $th->getMessage(), 'linea' => $th->getLine()]);
            DB::rollback();

            $this->sendError("Error al realizar la operación: {$th->getMessage()} @ {$th->getLine()}", 503);
        }

        $this->sendSuccess('¡Operación realizada correctamente!');
    }

}
