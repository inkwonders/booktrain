<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Colegio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Access\AuthorizationException;

class AuthController extends Controller
{
    //LOGIN DE USUARIO
    public function getLogin()
    {
        return view('public.login');
    }

    // funcion de envio de confirmacion de correo, verificar usuario
    public function verify(Request $request, $user_id)
    {
        $user = User::find($request->route('id'));

        if (!hash_equals((string) $request->route('hash'), sha1($user->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($user->markEmailAsVerified())
            // event(new Verified($user));
            $user->sendUserCreatedSuccessNotification();
        // return response()->json(['success' => true, 'message' => "Se ha verificado la cuenta correctamente"], 200);
        return redirect()->to('/inicio');
    }

    // funcion de reenvio de confirmacion de correo
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(['success' => false, 'message' => 'El email ya se encuentra verificado'], 422);
        }

        auth()->user()->sendEmailVerificationNotification();

        return response()->json(['success' => true, 'message' => "Se ha enviado el correo de verificación nuevamente"], 200);
    }

    //PASAMOS EMAIL Y PASSWORD PARA INICIAR SESION Y REDIRIJIMOS A LA VISTA
    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $autenticacion = Auth::attempt(['email' => $request->email, 'password' => $request->password, 'activo' => 1]);

        // Si la autenticación es por AJAX
        if($request->expectsJson()) {
            if($autenticacion)
                return response()->json([
                    'message' => 'Autenticación exitosa',
                    'success' => true,
                    'token' => $request->user()->createToken("HOLA")->plainTextToken
                ]);
            else
                return response()->json([
                    'message' => 'Credenciales inválidas',
                    'success' => false
                ], 401);
        }

        if ( ! $autenticacion ) {
            return view('public.login')
                ->with('error', 'La contraseña no coincide o la cuenta no existe.');
        }

        $request->session()->regenerate();

        $usuario = Auth::user();
        // TODO: Toda la lógica de redirección del usuario según su rol debe hacerse en el Middleware RedirectIfAuthenticated personalizando sus rutas

        $redirect_url = session()->get('redirect_url');

        if($redirect_url) {
            session()->forget('redirect_url');

            return redirect()->to($redirect_url);
        }

        if ($usuario->hasRole('Admin|Super Admin')) {
            return redirect()->route('admin.pedidos');
        }

        if ($usuario->hasRole('Venta Movil')) {
            if($usuario->permissions->count() == 0 || Colegio::whereIn('codigo', $usuario->permissions->pluck('name'))->count() != 1)
                return view('public.login')->with('error', "El usuario {$request->email} no tiene asignado un colegio");

            if ($usuario->cajas->count() == 0 || Auth()->user()->cajas->last()->status == "CERRADA") {
                return redirect()->to('/venta_movil/movimiento_caja');
            } else {
                return redirect()->to('/venta_movil');
            }
        }

        if ($usuario->hasRole('Cliente')) {
            return redirect()->route('inicio');
        }

        if ($usuario->hasRole('Colegio')) {
            if($usuario->permissions->count() == 0) {
                return view('public.login')->with('error', "El usuario {$request->email} no tiene asignado un colegio");
            }

            return redirect()->route('colegio.dashboard');
        }

        //redirección por default
        return redirect()->route('inicio');

        // if ($usuario->activo == 1) {

        // } else {
        //     Auth::logout();
        //     return view('public.login')->with('error', 'Su cuenta se encuentra inactiva.');
        // }
    }

    //CERRAMOS SESION
    public function logOut(Request $request)
    {
        Auth::logout();
        return redirect()->route('root');
    }

    //REGISTRO DE NUEVO USUARIO
    public function postRegister(Request $request)
    {

        $reglas = [
            'name' => 'required|string|max:60',
            'apellidos' => 'required|string|max:60',
            'email' => 'required|string|email|max:100|unique:users|confirmed',
            'email_confirmation' => 'required|string|email|',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
        ];

        $mensajes = [
            'name.required' => 'Su nombre es requerido',
            'apellidos.required' => 'Sus apellidos son requeridos',
            'email.unique' => 'Éste correo ya se encuentra en uso',
            'email.required' => 'Por favor ingrese un correo electrónico',
            'email_confirmation.required' => 'Por favor ingrese la confirmación del correo electrónico',
            'password.required' => 'Por favor ingrese una contraseña de 8 caractéres como mínimo',
            'required' => 'Se esperaba :attribute',
            'password.min' => 'Se esperaba un password de tamaño minimo :min caractéres',
        ];

        $validator = Validator::make(
            $request->all(),
            $reglas,
            $mensajes
        );

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()], 422);
        }

        $usuario = new User($request->all());
        $usuario->assignRole('Cliente');

        $usuario->password = Hash::make($usuario->password);

        $usuario->save();

        return response()->json(['success' => true, 'message' => "Se ha registrado correctamente"], 200);
    }
}
