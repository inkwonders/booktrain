<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            // Guardamos la url para una redirección después de hacer login
            $path = $request->path();

            if( ! $request->is('salir'))
                session()->put('redirect_url', $path);

            return route('login');
        }
    }
}
