<?php

use App\Http\Controllers\API\AuthAPIController;
use App\Http\Controllers\API\ColegioAPIController;
use App\Http\Controllers\API\PedidoAPIController;
use App\Http\Controllers\API\UsuarioAPIController;
use App\Http\Controllers\API\MercadoPagoAPIController;
use App\Http\Controllers\CajaController;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::get('/user', [UsuarioAPIController::class, 'getMiInformacion'])->name('api.user');

    Route::group(['prefix' => 'venta_movil'], function() {

        Route::get('configuracion', [CajaController::class, 'configuracionVentaOffline'])->name('api.caja.configuracion');

        Route::get('inventario', [CajaController::class, 'obtenerInventario'])->name('api.caja.inventario');

        Route::get('cupones', [CajaController::class, 'cupones'])->name('api.cupones');

        Route::get('caja', [CajaController::class, 'informacionCajaVentaMovil'])->name('api.caja');

        // Route::post('registrar', [PedidoAPIController::class, 'storeCarrito'])->name('api.pedido.registrar');

        Route::post('pagar', [PedidoAPIController::class, 'pagarPedido'])->name('api.pedido.pagar');

        Route::post('devolver', [PedidoAPIController::class, 'devolverMercancia'])->name('api.pedido.devolver');

        Route::post('caja/abrir', [CajaController::class, 'abrir'])->name('api.caja.abrir');

        Route::post('caja/cerrar', [CajaController::class, 'cerrar'])->name('api.caja.cerrar');

        Route::get('carrito/', [PedidoAPIController::class, 'obtenerCarrito'])->name('api.caja.carrito');
    });

    Route::get('logout', [AuthAPIController::class, 'logout'])->name('api.logout');
});

Route::post('login', [AuthAPIController::class, 'login'])->name('api.login');

Route::post('/mercado_pago', [MercadoPagoAPIController::class, 'getPreference']);

Route::get('/responseMp/{id}', [MercadoPagoAPIController::class, 'responseMp']);

Route::post('/webhook', [MercadoPagoAPIController::class, 'webhook']);

// Route::resource('colegios', App\Http\Controllers\API\ColegioAPIController::class)->only([
//     'index', 'show'
// ]);

/*
Route::resource('seccion_colegios', App\Http\Controllers\API\SeccionColegioAPIController::class);

Route::resource('nivel_colegios', App\Http\Controllers\API\NivelColegioAPIController::class);

Route::resource('libros', App\Http\Controllers\API\LibroAPIController::class);

Route::resource('paquetes', App\Http\Controllers\API\PaqueteAPIController::class);

Route::resource('pedidos', App\Http\Controllers\API\PedidoAPIController::class);

Route::resource('envios', App\Http\Controllers\API\EnvioAPIController::class);

Route::resource('resumen_pedidos', App\Http\Controllers\API\ResumenPedidoAPIController::class);

Route::resource('detalle_resumen_pedidos', App\Http\Controllers\API\DetalleResumenPedidoAPIController::class);

Route::resource('direccion_entregas', App\Http\Controllers\API\DireccionEntregaAPIController::class);

Route::resource('datos_facturas', App\Http\Controllers\API\DatosFacturasAPIController::class);

Route::resource('uso_c_f_d_is', App\Http\Controllers\API\UsoCFDIAPIController::class);

Route::resource('metodo_pagos', App\Http\Controllers\API\MetodoPagoAPIController::class);

Route::resource('forma_pagos', App\Http\Controllers\API\FormaPagoAPIController::class);

Route::resource('estados', App\Http\Controllers\API\EstadoAPIController::class);

Route::resource('municipios', App\Http\Controllers\API\MunicipioAPIController::class);

Route::resource('libros_colegios', App\Http\Controllers\API\LibrosColegiosAPIController::class);

Route::resource('configuracions', App\Http\Controllers\API\ConfiguracionAPIController::class);

*/

// Route::resource('ticket_motivos', App\Http\Controllers\API\TicketMotivoAPIController::class);

// Route::resource('ticket_usuarios', App\Http\Controllers\API\TicketUsuarioAPIController::class);

// Route::resource('ticket_archivos', App\Http\Controllers\API\TicketArchivoAPIController::class);

// Route::resource('ticket_entradas', App\Http\Controllers\API\TicketsAPIController::class);

// Route::resource('ticket_entrada_archivos', App\Http\Controllers\API\TicketEntradaArchivoAPIController::class);


Route::resource('notificacions', App\Http\Controllers\API\NotificacionAPIController::class);

