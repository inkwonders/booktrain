<?php
use App\Models\User;
use App\Models\Libro;
use App\Models\Pedido;
use App\Models\Colegio;
use App\Models\Notificacion;
use App\Models\LibrosColegios;
use App\Models\ReferenciaPedido;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BbvaController;
use App\Http\Controllers\ColegioController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MercadoPagoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/actualiza_fecha_envio', 'EnvioController@funcionActualizaFechaEnvioConUpdatedAt');

Route::get('/tabla_ejemplo', function () {
    return view('public.tabla_ejemplo');
});

Route::get('/tickets_ejemplo', function () {
    return view('public.tikets_ejemplo');
});

Route::get('/new_superadmin/{id}', 'UsuariosController@new_superadmin');

// Ruta para obtener csrf para hacer pruebas en postman
Route::get('/csrf', function () {
    return csrf_token();
});
Route::get('/phpinfo', function () {
    return date("Y-m-d H:i:s");
    // phpinfo();
});

// prueba de notificacion de pedido
Route::get('/notificacion_pedido/{pedido}', 'PedidoController@notificacionPedido');
//temrina prueba de notificacion de pedido

// prueba de notificacion de pedido
Route::get('/notificacion_veryfy', 'UsuariosController@notificacionEmail');
//temrina prueba de notificacion de pedido

Route::get('email/verify/{id}/{hash}', [AuthController::class, 'verify'])->name('verification.verify');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');

// Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

// Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');
// Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');
// Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');
// Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');
// Route::post('generator_builder/generate-from-file', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile')->name('io_generator_builder_generate_from_file');
/*
*/
/** Ruta index */

Route::get('/', function () {
    return view('public.login');
})->middleware('guest')->name('root');

/** Fin ruta index */

/** Inicio rutas registro */
Route::get('/registro', function () {
    return view('public.register');
})->name('registro');

Route::post('/registro', 'AuthController@postRegister');
/** Fin rutas registro */

/** Incio rutas recuperar contraseñas */

Route::get('/recuperar', 'Auth\ForgotPasswordController@getEmail')->name('recuperar');
Route::post('/recuperar', 'Auth\ForgotPasswordController@postEmail');


Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@getPassword')->name('reset_password');
Route::get('/reset-password', 'Auth\ResetPasswordController@getExpired');
Route::post('/reset-password', 'Auth\ResetPasswordController@updatePassword');

Route::get('/descargar_factura/{accion}/{id}', 'PedidoController@imprimirFactura')->name('imprimir_factura');

/** Incio rutas login */

Route::get('/login', [AuthController::class, 'getLogin'])->name('login');
Route::post('/login', [AuthController::class, 'postLogin']);

/** Fin rutas login */


/** Inicio rutas protegidas */
Route::group(['middleware' => 'auth'], function () {
    // Ruta inicio
    Route::get('/email1', function () {
        return view('/emails.entregado');
    });

    Route::get('/inicio', function () {
        return view('private.home');
    })->name('inicio');

    // Ruta información usuario
    Route::get('/mi-informacion', 'UsuariosController@miInformacion')->name('mi-informacion');

    // Ruta para el carrito de compras
    Route::get('/carrito', 'PedidoController@viewCarrito')->name('carrito');

    // Ruta para recibir la uri de redirección de la pasarela de pago de bbva y proceder a consultar el estado del pago
    Route::get('/bbva/redirect', [BbvaController::class, 'redirectVpos'])->name('bbva.vpos.redirect');

    // Crea un pago y obtiene una url de redirección para que el cliente pague usando la pasarela de BBVA
    Route::post('/bbva/pagar', [BbvaController::class, 'createPayment'])->name('bbva.vpos.create');

    Route::group(['prefix' => 'openpay'], function() {
        // Crea una referencia de pago para tienda con la pasarela de pago de OpenPay
        Route::post('crearReferencia', 'OpenPayController@referenciaTienda')->name('openpay.referencia.tienda.create');
    });

    Route::get('/datos-compra/{id}', 'PedidoController@datosPedido');

    Route::get('/datos-compra/{id}/recibo', 'PedidoController@reciboReferencia');

    // Ruta mis pedidos
    Route::get('/mis-pedidos', 'PedidoController@pedidosUsuario')->name('private.user.pedidos');

    // ruta para crear nuevo caso de soporte técnico
    Route::get('/soporte', 'TicketUsuarioController@index')->name('soporte');
    Route::post('/caso', 'TicketUsuarioController@store')->name('caso');

    // Ruta logout
    Route::get('/salir', "AuthController@logOut")->name('logout');

    // Ruta detalles de compra
    Route::prefix('pedidos')->group(function () {

        Route::prefix('xls')->group(function () {
            //cargamos un excel con los pedidos entregados y regresa un json con la información de los pedidos modificados
            Route::post('uploadPedidosEntregados', 'PedidoController@uploadPedidosEntregados');

            //cargamos un excel con los pedidos que ya fueron enviados y regresa un json con la información de los pedidos modificados
            Route::post('uploadPedidosEnviados', 'PedidoController@uploadPedidosEnviados');
        });

        Route::get('{id}/pagar', 'PedidoController@getPagar')->name('pedidos.pagar');
    });

    // Rutas para el rol de Colegio
    Route::group(['prefix' => 'colegio'], function () {
        // Vista principal para el usuario con el rol Colegio
        Route::get('dashboard', 'ColegioController@dashboard')->name('colegio.dashboard');

        Route::group(['prefix' => 'reportes'], function() {

            Route::group(['prefix' => 'general'], function() {
                // Vista para generar el reporte general
                Route::get('/', 'ColegioController@reporteGeneral')->name('colegio.reportes.general');

                // Ruta para ver el reporte
                Route::post('/', 'ColegioController@postReporteGeneral');

                // Ruta para descargar el reporte
                Route::get('/descargar', 'ColegioController@postReporteGeneralDescargar');
            });

            Route::group(['prefix' => 'alumno'], function() {
                // Vista para generar el reporte por alumno
                Route::get('/', 'ColegioController@reporteAlumno')->name('colegio.reportes.alumno');

                // Ruta para ver el reporte
                Route::post('/', 'ColegioController@postReporteAlumno');

                // Ruta para descargar el reporte
                Route::get('/descargar', 'ColegioController@postReporteAlumnoDescargar');
            });
        });

        // Ruta colegio
        Route::get('{codigo}', 'ColegioController@show')->name('colegio.show');

        // Ruta para regresar la informacion del colegio
        Route::post('/informacion', 'ColegioController@informacionColegio')->name('colegio-informacion');

        // Ruta para regresar las secciones del colegio
        Route::post('/secciones', 'ColegioController@secciones');
    });

    Route::group(['prefix' => 'usuario'], function () {

        // Ruta para editar la información del usuario
        Route::post('/informacion/{id}', 'UsuariosController@update');
    });

    Route::group(['prefix' => 'tickets'], function () {

        Route::get('/', 'TicketController@index')->name('ticket.index');

        // Ruta para retornar vista con json de informacion
        Route::get('/{id}', 'TicketUsuarioController@show')->name('ticket.show');

        // Ruta para calificar ticket
        Route::put('/{id}/calificar', 'TicketUsuarioController@calificar')->name('ticket.calificar');

        // Ruta para cerrar caso
        Route::put('/{id}/cerrar', 'TicketUsuarioController@cerrar')->name('ticket.cerrar');
    });

    Route::group(['prefix' => 'cierre_caja'], function () {
        Route::get('/', 'RegistroPagoController@getReporteGananciasDiarias');
        Route::post('/print', 'RegistroPagoController@printReporteGananciasDiarias');
    });

    Route::group(['prefix' => 'cierre_del_dia'], function () {
        Route::get('/', 'ReporteController@getReporteCierreDia');
        Route::post('/print', 'ReporteController@printReporteCierreDelDia');
    });

    Route::prefix('print')->group(function () {
        Route::get('diario_pedidos', 'PedidoController@printDiarioPedios')->name('admin.pedidos.impresion');
    });

    Route::prefix('admin')->group(function () {
        Route::get('metodosPago', 'ConfiguracionController@metodosPago')->name('admin.metodos_pago');

        Route::group(['prefix' => 'devoluciones'], function () {
            Route::get('/', 'DevolucionesController@reporteDevoluciones')->name('admin.devoluciones');
        });

        Route::get('/pedidos/conciliacion', 'ColegioController@conciliacion')->name('admin.pedidos.conciliacion');
        Route::post('/pedidos/conciliacion', 'ColegioController@postConciliacion');

        Route::get('/cupones', 'CuponController@index');

        Route::get('/pagos_fallidos', function () {
            return view('private.reporte.pagos_fallidos');
        })->name("pagos_fallidos");

        Route::prefix('colegios')->group(function () {
            Route::get('/', 'ColegioController@index')->name('admin.colegios');

            Route::get('/{colegio}/editar', 'ColegioController@edit')->name('admin.colegios.edit');

            Route::get('/{colegio}/libros', 'ColegioController@libros')->name('admin.colegios.libros');

            Route::get('/{colegio}/libros/{libro}/paquetes/{paquete}/editar', 'ColegioController@editarLibro')->name('admin.colegios.libros.editar');

            Route::get('/{colegio}/libros/asignar', 'ColegioController@asignarLibros')->name('admin.colegios.libros.asignar');

            Route::get('/{colegio}/metodos_pago', 'ColegioController@metodosPago')->name('admin.colegios.metodos_pago');

            Route::get('/nuevo', 'ColegioController@create')->name('admin.colegios.create');
        });

        Route::get('colegios_editando/secciones/{id}', function ($id) { // esto es provisional
            return view('private.admin.colegios.secciones_colegios')->with('colegio', Colegio::find($id));
        });

        Route::get('colegios/{colegio_id}/secciones/{seccion_id}', function ($colegio_id, $seccion_id) {
            return view('private.admin.colegios.secciones')->with(['colegio_id' => $colegio_id, 'seccion_id' => $seccion_id]);
        })->name('admin.colegios.secciones');

        Route::get('colegios_envio/{colegio_id}', function ($id) {
            return view('private.admin.colegios.envios')->with('colegio', Colegio::find($id));
        })->name('admin.colegios.envios');

        Route::get('colegios_envio/{colegio_id}/{metodo_envio_id}', function ($id_colegio, $id_metodo_envio) {
            return view('private.admin.colegios.envios_editar')->with(['colegio' => Colegio::find($id_colegio), 'metodo_envio' => $id_metodo_envio]);
        })->name('admin.colegios.envios_editar');

        Route::get('envio_domicilio/{colegio_id}/', function ($id_colegio) {
            return view('private.admin.colegios.envio_domicilio_editar')->with(['colegio' => Colegio::find($id_colegio)]);
        })->name('admin.colegios.envio_domicilio_editar');

        Route::get('agregar_metodo_envio/{colegio_id}/{metodo_envio}', function ($id_colegio, $metodo_envio) {
            return view('private.admin.colegios.envios_agregar')->with(['colegio' => Colegio::find($id_colegio), 'metodo_envio' => $metodo_envio]);
        })->name('admin.colegios.envios_agregar');

        Route::get('libros', 'LibroController@getLibros');

        Route::get('usuarios', 'UsuariosController@getUsuarios');

        Route::get('pedidos', 'PedidoController@index')->name('admin.pedidos');

        Route::get('facturas', 'PedidoController@facturas')->name('admin.facturas');

        Route::get('colegios_editando/nueva/{id}', function ($id) {
            return view('private.admin.colegios.nueva_seccion')->with('colegio', Colegio::find($id));
        });

        Route::get('nuevo_libro', function () {
            return view('private.admin.nuevo_libro');
        });

        Route::get('editar_configuracion_pago/{id_colegio}/{metodo_pago}/{forma_pago}', function ($id_colegio, $metodo_pago, $forma_pago) {
            return view('private.admin.colegios.editar_configuracion_pagos')->with(['colegio' => Colegio::find($id_colegio), 'metodo_pago' => $metodo_pago, 'forma_pago' => $forma_pago]);
        })->name('admin.colegios.configuracion.pagos.forma');

        Route::get('avisos_colegio/{id}', function ($id) {
            return view('private.admin.colegios.avisos')->with('colegio', Colegio::find($id));
        });

        Route::get('avisos_colegio/nuevo/{id}', function ($id) {
            return view('private.admin.colegios.nuevo_aviso')->with('colegio', Colegio::find($id));
        });

        Route::get('/impresion_pedidos', function () {
            return view('private.admin.pedidos.impresion');
            // TODO: Eliminar esta vista: view('private.impresion_pedidos');
        })->name('private.admin.pedidos.imprimir');

        Route::get('/impresion_pedidos/{fecha}', 'PedidoController@imprimirPedidos')->name('admin.pedidos.imprimir');
        Route::get('/imprimir_pedidos/{fecha}/{colegio_id}', 'PedidoController@imprimirPedidoscolegio')->name('admin.pedidos.imprimir.colegio');

        Route::get('avisos_colegio/editar/{id_colegio}/{id_notificacion}', function ($id_colegio, $id_notificacion) {
            return view('private.admin.colegios.editar_aviso')->with(['notificacion' => Notificacion::find($id_notificacion), 'colegio' => Colegio::find($id_colegio)]);
        });

        Route::get('editar_libro/{id}', function ($id) {
            return view('private.admin.editar_libro')->with('libro', Libro::find($id));;
        });

        Route::get('editar_usuario/{id}', function ($id) {
            return view('private.admin.editar_usuario')->with('user', User::find($id));;
        })->name('admin.usuarios.editar');

        Route::get('nuevo_usuario', function () {
            return view('private.admin.nuevo_usuario');
        });


        Route::get('editar_seccion/{id}/{seccion_id}/', function ($id, $seccion_id) {
            return view('private.admin.colegios.editar_seccion')->with(['colegio' => Colegio::find($id), 'seccion_id' => $seccion_id]);
        });

        Route::get('paqueteria', function () {
            return view('private.admin.paqueteria.index');
        })->name('private.admin.paqueteria');

        Route::get('log', function () {
            return view('private.admin.log');
        })->name('private.admin.log');
    });

    Route::group(['prefix' => 'almacen'], function () {
        Route::get('/', 'AlmacenController@index')->name('admin.almacen');
        Route::get('/movimientos', 'AlmacenController@movimientos')->name('admin.almacen.movimientos');
        Route::get('/vermovimientos', 'AlmacenController@vermovimientos')->name('admin.almacen.vermovimientos');
    });

    Route::get('tabla_entregas_parciales', 'PedidoController@getListaEntregaPedidos');
    Route::post('tabla_entregas_parciales/cambio_status', 'PedidoController@cambioDeStatusPedido');

    Route::group(['prefix' => 'envios_parciales'], function () {
        Route::get('/', 'EnvioController@enviosParciales')->name('envios_parciales');
        Route::post('/cambio_status', 'EnvioController@cambioDeStatusPedido');
    });

    Route::group(['prefix' => 'reportes'], function () {
        Route::get('/', function () {
            return view('private.reporte.index');
        })->name('reportes');

        Route::get('/pagos_fallidos', 'PedidoController@reportePedidosFallidos');

        Route::get('/general', 'ReporteController@general')->name('admin.reportes.general');
        Route::get('/general/{fecha_inicial}/{fecha_final}/{colegio_id}/{seccion_id}/{libro_id}/{estado_pedido}', 'ReporteController@tableGeneral')->name('admin.reportes.general.table');

        Route::get('/colegio', 'ReporteController@colegio')->name('admin.reportes.colegio');
        Route::get('/colegio/{codigo_colegio}/{fecha_inicial}/{fecha_final}/{seccion_id}/{libro_id}', 'ReporteController@tableColegio')->name('admin.reportes.colegio.table');

        Route::get('cierre', 'ReporteController@cierre')->name('admin.reportes.cierre');
        Route::get('/cierre/{codigo_colegio}/{fecha_inicial}/{fecha_final}/{seccion_id}/{libro_id}', 'ReporteController@tableCierre')->name('admin.reportes.cierre.table');
    });

    Route::group(['prefix' => 'referencias'], function () {
        Route::get('/reporte_referencias', 'PedidoController@reporteReferencias');
        Route::get('/reporte_referencias_status', 'PedidoController@reporteReferenciasStatus');
        Route::get('/envio_confirmacion_pago', 'PedidoController@envioCorreoConfirmacionPago');
    });

    Route::group(['prefix' => 'venta_movil'], function () {
        Route::get('/', 'PedidoController@getVentaMovil')->name('venta_movil');
        Route::get('movimiento_caja/', function () {
            return view('venta_movil.movimiento_caja');
        })->name('venta_movil.movimiento_caja');
        Route::get('/pedidos', 'PedidoController@pedidosVentaMovil');
    });

    Route::group(['prefix' => 'referencia_bbva'], function () {

        Route::get('/', function () {
            return view('private.referencia_bbva');
        })->name('referencia_bbva');
    });
    Route::group(['prefix' => 'carga_libros'], function () {

        Route::get('/', function () {
            return view('private.carga_libros');
        })->name('carga_libros');
    });

    Route::group(['prefix' => 'pedidos_entregados'], function () {
        Route::get('/', function () {
            return view('private.pedidos_entregados');
        })->name('pedidos_entregados');
    });

    Route::group(['prefix' => 'pedidos_enviados'], function () {
        Route::get('/', function () {
            return view('private.pedidos_enviados');
        })->name('pedidos_enviados');
    });

    Route::group(['prefix' => 'tabla_referencias'], function () {
        Route::get('/', 'ReferenciaPedidoController@tablaReferencias');
    });

    Route::group(['prefix' => 'envios_general'], function () {
        Route::post('/exportar-envios', 'API\PedidoAPIController@exportarEnviosXLS');
        Route::get('/tablareporteenvios', 'PedidoController@reporteEnviosGenerales');
    });

    Route::group(['prefix' => 'reporte_pedidos'], function () {
        Route::get('/pedidos-referencias', 'API\DetalleResumenPedidoAPIController@pedidosReferenciasXLS')->name('pedidos-referencias');

        Route::get('/pedidos_general', function () {
            return view('private.admin.reporte_pedidos_general');
        })->name('reporte_pedido');
    });
    // Ruta para añadir activo a métodos de pagos de colegio
    Route::group(['prefix' => 'metodos_pago_modificar'], function () {
        Route::get('/', 'ColegioController@addActivo');
        Route::get('/general', 'ConfiguracionController@AddActivo');
    });

    Route::group(['prefix' => 'import'], function () {
        // Ruta para cargar los pagos de la referencia bancaria
        Route::post('reporte_bancomer', 'ReferenciaPedidoController@importarReporteBancomerExp');
        //ruta para cargar libros de forma masiva
        Route::post('catalogo_libros', 'LibroController@uploadLibros');
    });

    // Ruta para procesar las llamadas del webhook de 3DS de Netpay
    Route::get('/netpay/3ds/continuar', 'NetPayController@getWebhook3DS')->name('netpay.3ds.continuar');

    // Ruta para procesar la redirección del 3DS del openpay
    Route::get('/openpay/3ds/continuar', 'OpenPayController@continue3Ds')->name('openpay.3ds');

    /** Rutas API Web */
    Route::prefix('api_web')->group(function () {

        Route::post('/cupones/canjear', 'API\ColegioAPIController@canjearCupon');

        Route::prefix('colegios')->group(function () {
            // regresa la información de todos los colegios
            Route::get('/', 'API\ColegioAPIController@index');
            //regresa la lista de grados del colegio
            Route::get('/{id}/colegiosISBN', 'API\ColegioAPIController@getColegiosISBN');

            Route::get('/{id}/colegioslibrosISBN', 'API\ColegioAPIController@colegioslibrosISBN');

            Route::post('/{id}/cajas_cerradas', 'API\ColegioAPIController@cajasCerradas');

            Route::get('/{colegio}/configuracion_venta_movil', 'API\ColegioAPIController@configuracionVentaMovil');

            Route::post('/venta_movil/pagar', 'API\ColegioAPIController@pagarVentaMovil');
        });

        Route::prefix('colegio')->group(function () {
            Route::get('/', [ColegioController::class, 'informacion']);
            Route::post('informacion', 'API\ColegioAPIController@getInformacion');
            Route::get('{id}', 'API\ColegioAPIController@show');
        });

        Route::get("mi-informacion-2", "API\UsuarioAPIController@getMiInformacion");

        Route::group(['prefix' => 'estados'], function () {

            // regresa en un json los estados
            Route::get('/', 'API\EstadoAPIController@estados');

            // regresa en un json los municipios del estado solicitado
            Route::get('/{estado}/municipios', 'API\EstadoAPIController@municipiosEstado');
        });

        Route::group(['prefix' => 'tickets'], function () {
            Route::get('/opciones', 'API\TicketsAPIController@getListaOpciones');

            // Ruta para obtener los valores de la grafica
            //Route::get('/grafica', 'API\TicketUsuarioAPIController@valoresGrafica')->name('tickets.grafica');
            Route::get('/grafica', 'API\TicketUsuarioAPIController@valoresGrafica');

            // Ruta para regresar las entradas del ticket
            Route::get('{id}', 'API\TicketUsuarioAPIController@show');
            Route::get('/{id}/entradas', 'API\TicketUsuarioAPIController@entradasTicket');
            Route::post('/agregar_entrada', 'API\TicketsAPIController@agregar_entrada');

            //ruta api bajo el prefijo /tickets, recibe ajax para cerrar un caso, pone en estatus 0 el ticket_usuario, es necesario mandar id del ticket
            Route::post('/cierra_ticket/{id}', 'API\TicketUsuarioAPIController@postCierraTicket');

            //ruta api bajo el prefijo /tickets, recibe ajax para calificar e ingresar comentarios finales de cada ticket, es necesario mandar id del ticket
            Route::post('/califica_ticket/{id}', 'API\TicketUsuarioAPIController@postCalificaTicket');
        });

        Route::group(['prefix' => 'usuario'], function () {

            // Ruta para mostrar la información del ticket
            Route::get('/{id}/datos-ultimo-pedido', 'API\UsuarioAPIController@datosFactura');
        });

        Route::get('/usos_cfdi', 'API\UsoCFDIAPIController@index');

        // Ruta para notificaciones API-WEB
        Route::prefix('/notificaciones')->group(function () {
            // Ruta de notificaciones, se manda como parametro el id del colegio para hacer el filtro
            Route::get('/notificaciones/{id}', 'API\NotificacionAPIController@index');
        });

        /** Ruta para agregar pedidos y sus detalles */
        Route::group(['prefix' => 'pedido'], function () {

            /** Ruta para agregar resumen de pedido y/o crear pedido si no existe */
            Route::post('/carrito', 'PedidoController@storeCarrito');

            /** Ruta para regresar json del carrito */
            Route::get('/carrito/{id}', 'PedidoController@showCarrito');

            /** Editar y eliminar resumen del pedido */
            Route::put('/carrito/resumen/{id}', 'ResumenPedidoController@editResumen');
            Route::delete('/carrito/resumen/{id}', 'ResumenPedidoController@deleteResumen');

            /** Editar y eliminar detalle del resumen del pedido */
            Route::put('/carrito/detalle/{id}', 'DetalleResumenPedidoController@editDetalle');
            Route::delete('/carrito/detalle/{id}', 'DetalleResumenPedidoController@deleteDetalle');

            // Ruta para pagar un pedido usando NETPAY
            Route::post('{id}/pagarNetpay', 'NetPayController@postPagar');

            // Ruta para pagar un pedido usando OpenPay
            Route::post('{pedido_id}/pagarOpenpay', 'OpenPayController@postPagar');

            // Ruta para pagar un pedido usando OpenPay
            Route::post('{pedido_id}/referenciaBbva', 'BbvaController@createPaymentReference');
        });

        Route::group(['prefix' => 'excel'], function () {

            /** Ruta para descargar excel de datos factura */
            Route::post('/datos-factura', 'API\DatosFacturasAPIController@facturasXLS')->name('excel.facturas');

            /** Ruta para descargar excel de datos factura */
            /** debe ser por post y modificar controller */
            Route::post('/detalles-colegio', 'API\DetalleResumenPedidoAPIController@detallesColegioXLS')->name('excel.colegio');

            Route::post('/reporte-admin-colegio', 'API\DetalleResumenPedidoAPIController@detallesAdminColegioXLS');

            Route::post('/reporte-admin-general', 'API\DetalleResumenPedidoAPIController@detallesXLS');

            Route::post('/detalles-cierre-colegio', 'API\DetalleResumenPedidoAPIController@detallesAdminCierreColegioXLS');

            Route::get('/reportePagosRechazados/', 'PedidoController@reportePagosrechazadosTest');
        });

        Route::group(['prefix' => 'formatos'], function () {
            /** Ruta para reimprimir la referencia bancaria de bancomer */
            Route::post('/ref-bbva10/{pedido_id}', 'API\PedidoAPIController@getPedidoBBVA10');
            Route::get('/reimprimir/{pedido_id}', 'API\PedidoAPIController@getInformacionFormato');
        });

        Route::group(['prefix' => 'servicio_paqueterias'], function () {
            //obtiene la lista de servicios de paqueterias activos
            Route::get('/', 'API\ServicioPaqueteriaAPIController@index');
        });

        Route::group(['prefix' => 'log'], function () {
            Route::get('/', 'API\UsuarioAPIController@getUsuariosDiferentesDeClientes');
            Route::post('/informacion-log', 'UsuariosController@getInformacionLog');
        });
    });
    /** Fin rutas API Web */

    Route::group(['prefix' => 'referencias/api_web/excel'], function () {
        Route::post('/pedidos-referencias', 'API\DetalleResumenPedidoAPIController@pedidosReferenciasXLS');
    });

    // prueba de rutas admin para roles y permisos
    Route::get('/user_tickets', 'Admin\UserAdminController@index');

    // Rutas de prueba
    // Route::post('/tablaexel', function () {
    //     return view('private.tablaexel');
    // });

    // Route::post('/tablaexelcolegio', 'DetalleResumenPedidoController@tablaexelcolegio');

    // Route::post('/tablareportecolegio', 'DetalleResumenPedidoController@tablaReporteColegio');

    // Route::post('/tablareportegeneral', 'DetalleResumenPedidoController@tablaReporteGeneral');

    Route::post('/tablareportefacturacion', 'DetalleResumenPedidoController@tablaReporteFacturacion');

    Route::post('/tablareportecierre', 'DetalleResumenPedidoController@tablaReporteCierre');

    Route::get('/resultado3ds', function () {
        return view('private.resultado3ds');
    });

    Route::get('/resultadomercadopago/{res}', 'MercadoPagoController@respuestaMercadoPago');

    // End Rutas de prueba
});

/** Fin rutas protegidas */

/** Rutas prueba transformes */
Route::post('/transformerUsuario', 'UsuariosController@transformerUser');

/** RUTAS PARA PROBAR FUERA DEL MIDDLEWARE */

/** Ruta para probar refrencias de banco */
Route::get('/testReferenciaBBVA/{id}', 'PedidoController@testReferenciaBBVA');

/** Ruta  para pruebas de reporte de emergencia*/
Route::get('/reportePagosrechazadosTest', 'PedidoController@reportePagosrechazadosTest');

/** Ruta para revisar folios faltantes */
Route::get('/reporteListaFoliosTest', 'PedidoController@reporteListaFoliosTest');

// Ruta para calificar ticket
Route::put('/ticketsTest/{id}/calificar', 'TicketUsuarioController@calificar')->name('ticket2.calificar');

// Ruta para cerrar caso
Route::put('/ticketsTest/{id}/cerrar', 'TicketUsuarioController@cerrar')->name('ticket2.cerrar');

Route::get('/carrito/{id}', 'PedidoController@showCarrito');

Route::get('/referencia', function () {
    return view('private.referencia');
});

Route::get('/aviso-de-privacidad', function () {
    return view('public.avisoprivacidad');
})->name('aviso_privacidad');

Route::get('/politicas', function () {
    return view('public.politicas');
});

/** RUTAS DE WEBHOOKS */
Route::group(['prefix' => 'webhook'], function () {
    Route::post('/netpay/co-cash', 'NetPayController@webhook')->name('webhook.netpay');

    Route::post('/openpay/gateway', 'OpenPayController@webhook')->name('webhook.openpay');
    Route::get('/openpay/register', 'OpenPayController@registerWebhook');
    Route::post('/bbva/gateway', 'BbvaController@webhook')->name('webhook.bbva');
    Route::get('/bbva/register', 'BbvaController@registerWebhook');
});

/*RUTA WEBHOOK OPENPAY */
/*Route::group(['prefix' => 'response'], function () {
    Route::any('/openpay/gateway', 'OpenPayController@webhook')->name('webhook.openpay');
    Route::get('/openpay/register', 'OpenPayController@registerWebhook')->name('webhook.register');
});*/

Route::get('/pago_existoso_venta_movil', 'PedidoController@pruebaEnvioMailVentaMovil');

Route::get('/prueba_punto_reorden', 'PedidoController@pruebaEnvioPuntoReorden');

Route::post('/facturama', 'FacturamaController@getInstance');


Route::post('/generateCfdi', 'FacturamaController@generateCfdiPost');

Route::post('/addCsd', 'FacturamaController@addCsd');


Route::post('/getInvoiceFiles', 'FacturamaController@getInvoiceFiles');

Route::post('/deleteInvoice', 'FacturamaController@deleteInvoice');


Route::post('/errorEmail', 'FacturamaController@errorEmail');


