<?php

namespace Database\Factories;

use App\Models\SeccionColegio;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeccionColegioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SeccionColegio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'colegio_id' => $this->faker->randomDigitNotNull,
        'nombre' => $this->faker->word,
        'activo' => $this->faker->numberBetween(0, 1),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
