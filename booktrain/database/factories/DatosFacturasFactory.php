<?php

namespace Database\Factories;

use App\Models\DatosFacturas;
use Illuminate\Database\Eloquent\Factories\Factory;

class DatosFacturasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DatosFacturas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pedido_id' => $this->faker->randomDigitNotNull,
        'user_id' => $this->faker->randomDigitNotNull,
        'razon_social' => $this->faker->word,
        'rfc' => $this->faker->regexify('/^([A-Z,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/'),
        'correo' => $this->faker->safeEmail,
        'id_cfdi' => $this->faker->randomDigitNotNull,
        'cp' => $this->faker->numberBetween(10000, 99000),
        'metodo_pago_id' => $this->faker->randomDigitNotNull,
        'forma_pago_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
