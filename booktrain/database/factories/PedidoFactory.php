<?php

namespace Database\Factories;

use App\Models\Configuracion;
use App\Models\Pedido;
use Illuminate\Database\Eloquent\Factories\Factory;

class PedidoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pedido::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'serie' => $this->faker->word,
            'folio' => 0,
            'status' => ['CARRITO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO'][0],
            'usuario_id' => $this->faker->randomDigitNotNull,
            'colegio_id' => $this->faker->randomDigitNotNull,
            'nombre_contacto' => $this->faker->name,
            'apellidos_contacto' => $this->faker->lastName,
            'celular_contacto' => $this->faker->phoneNumber,
            'tipo_entrega' => $this->faker->numberBetween(1, 3),
            'factura' => $this->faker->numberBetween(0, 1),
            'terminos_condiciones' => 1,
            'subtotal' => 0, // Se va a incrementar según se agreguen sus detalles y resumenes
            'comision' => array_values([100, 150, 200, 250, 300])[rand(0, 4)],
            'total' => 0, // Calcular al terminar de agregar los detalles y resumenes
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
