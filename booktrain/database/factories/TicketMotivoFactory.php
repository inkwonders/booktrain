<?php

namespace Database\Factories;

use App\Models\TicketMotivo;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketMotivoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketMotivo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'orden' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
