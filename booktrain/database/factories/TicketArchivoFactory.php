<?php

namespace Database\Factories;

use App\Models\TicketArchivo;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketArchivoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketArchivo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ticket_usuario_id' => $this->faker->randomDigitNotNull,
        'archivo' => $this->faker->cityPrefix . '_' . $this->faker->word . '.' . $this->faker->fileExtension,
        'archivo_nombre_original' => $this->faker->word . '.' . $this->faker->fileExtension,
        'orden' => $this->faker->numberBetween(1, 5),
        'tipo_archivo' => $this->faker->word,
        'url' => $this->faker->imageUrl,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
