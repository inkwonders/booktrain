<?php

namespace Database\Factories;

use App\Models\Envio;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnvioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Envio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'fecha_recepcion' => $this->faker->dateTimeThisMonth,
        'fecha_estimada' => $this->faker->dateTimeThisMonth,
        'horarios_entrega' => $this->faker->word,
        'costo' => $this->faker->numberBetween(0, 350),
        'guia' => $this->faker->word . $this->faker->numberBetween(500, 100000),
        'paqueteria' => $this->faker->sentence,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
