<?php

namespace Database\Factories;

use App\Models\TicketUsuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketUsuarioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketUsuario::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'usuario_id' => $this->faker->randomDigitNotNull,
        'folio' => $this->faker->randomDigitNotNull,
        'status' => $this->faker->numberBetween(0, 3),
        'titulo' => $this->faker->sentence,
        'ticket_motivos_id' => $this->faker->numberBetween(1, 5),
        'descripcion' => $this->faker->text(rand(100, 1000)),
        'calificacion' => $this->faker->numberBetween(1, 2),
        'comentario_calificacion' => $this->faker->text(rand(100, 1000)),
        'fecha_calificacion' => $this->faker->dateTimeThisYear,
        'telefono' => $this->faker->numberBetween(1000000000, 9999999999),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
