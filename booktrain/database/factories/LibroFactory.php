<?php

namespace Database\Factories;

use App\Models\Libro;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Libro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->word,
        'editorial' => $this->faker->sentence,
        'edicion' => $this->faker->word,
        'isbn' => $this->faker->ean13(),
        // 'stock' => $this->faker->numberBetween(0, 100),
        // 'precio' => $this->faker->word,
        'activo' => $this->faker->numberBetween(0, 1),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
