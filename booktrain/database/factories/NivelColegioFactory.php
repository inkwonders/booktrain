<?php

namespace Database\Factories;

use App\Models\NivelColegio;
use Illuminate\Database\Eloquent\Factories\Factory;

class NivelColegioFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NivelColegio::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seccion_id' => $this->faker->randomDigitNotNull,
        'nombre' => $this->faker->word,
        'activo' => $this->faker->numberBetween(0, 1),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
