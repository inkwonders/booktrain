<?php

namespace Database\Factories;

use App\Models\MetodoPago;
use Illuminate\Database\Eloquent\Factories\Factory;

class MetodoPagoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MetodoPago::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'codigo' => $this->faker->word,
        'descripcion' => $this->faker->word,
        'orden'=> $this->faker->word,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
