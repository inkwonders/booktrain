<?php

namespace Database\Factories;

use App\Models\DetalleResumenPedido;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetalleResumenPedidoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetalleResumenPedido::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'resumen_pedidos_id' => 1,
        'libro_id'           => 1,
        'cantidad'           => $this->faker->numberBetween(1, 3),
        'precio_libro'       => $this->faker->numberBetween(25, 5000),
        'created_at'         => $this->faker->dateTimeThisMonth,
        'updated_at'         => $this->faker->dateTimeThisMonth
        ];
    }
}
