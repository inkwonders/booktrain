<?php

namespace Database\Factories;

use App\Models\ResumenPedido;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResumenPedidoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ResumenPedido::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pedido_id' => 1,
            'paquete_id' => 1,
        'nombre_alumno' => $this->faker->name,
        'paterno_alumno' => $this->faker->lastName,
        'materno_alumno' => $this->faker->lastName,
        //'subtotal' => $this->faker->numberBetween(25, 6000), // Calcular con el detalle del resumen
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
