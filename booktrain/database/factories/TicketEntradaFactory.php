<?php

namespace Database\Factories;

use App\Models\TicketEntrada;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketEntradaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketEntrada::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ticket_usuario_id' => $this->faker->randomDigitNotNull,
        'usuario_id' => $this->faker->randomDigitNotNull,
        'tipo_entrada' => $this->faker->numberBetween(1, 2),
        'comentario' => $this->faker->text(rand(100, 1000)),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
