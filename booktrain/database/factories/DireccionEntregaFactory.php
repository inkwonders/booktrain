<?php

namespace Database\Factories;

use App\Models\DireccionEntrega;
use Illuminate\Database\Eloquent\Factories\Factory;

class DireccionEntregaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DireccionEntrega::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'colegio_id' => $this->faker->randomDigitNotNull,
            'direccion_id' => $this->faker->randomDigitNotNull,
            'tipo' => ['COLEGIO', 'PICK UP'][rand(0, 1)],
        // 'calle' => $this->faker->streetName,
        // 'no_exterior' => $this->faker->buildingNumber,
        // 'no_interior' => $this->faker->buildingNumber,
        // 'colonia' => $this->faker->cityPrefix,
        // 'cp' => $this->faker->randomDigitNotNull,
        // 'estado_id' => $this->faker->randomDigitNotNull,
        // 'municipio_id' => $this->faker->randomDigitNotNull,
        // 'referencia' => $this->faker->sentence(),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
