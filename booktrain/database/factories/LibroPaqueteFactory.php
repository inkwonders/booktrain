<?php

namespace Database\Factories;

use App\Models\LibroPaquete;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibroPaqueteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LibroPaquete::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'libro_id' => $this->faker->randomDigitNotNull,
        'paquete_id' => $this->faker->randomDigitNotNull,
        'obligatorio' => $this->faker->word,
        'activo' => $this->faker->word,
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
