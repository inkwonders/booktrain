<?php

namespace Database\Factories;

use App\Models\LibrosColegios;
use Illuminate\Database\Eloquent\Factories\Factory;

class LibrosColegiosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LibrosColegios::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'libro_id' => $this->faker->randomDigitNotNull,
        'colegio_id' => $this->faker->randomDigitNotNull,
        'precio' => $this->faker->numberBetween(25, 5000),
        //'paquete_id' => "",
        'obligatorio' => $this->faker->numberBetween(0, 1),
        'activo' => $this->faker->numberBetween(0, 1),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
