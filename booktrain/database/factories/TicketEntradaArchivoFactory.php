<?php

namespace Database\Factories;

use App\Models\TicketEntradaArchivo;
use Illuminate\Database\Eloquent\Factories\Factory;

class TicketEntradaArchivoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TicketEntradaArchivo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'entrada_id' => $this->faker->randomDigitNotNull,
        'archivo_nombre_original' => $this->faker->word,
        'url' => $this->faker->imageUrl,
        'tipo_archivo' => array_values(['IMAGEN', 'TEXTO', 'AUDIO', 'VIDEO'])[rand(0, 3)],
        'orden' => $this->faker->numberBetween(1, 5),
        'created_at' => $this->faker->dateTimeThisMonth,
        'updated_at' => $this->faker->dateTimeThisMonth
        ];
    }
}
