<?php

namespace Database\Factories;

use App\Models\Configuracion;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConfiguracionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Configuracion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'etiqueta' => $this->faker->word,
        'valor' => $this->faker->word,
        'created_at' => $this->faker->now,
        'updated_at' => $this->faker->now
        ];
    }
}
