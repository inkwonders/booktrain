<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosFacturasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_facturas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pedido_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('razon_social');
            $table->string('rfc');
            $table->string('correo');
            $table->integer('cp');
            $table->integer('id_cfdi')->unsigned();
            $table->integer('metodo_pago_id')->unsigned();
            $table->integer('forma_pago_id')->unsigned();
            $table->timestamps();
            $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_cfdi')->references('id')->on('cfdis')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('metodo_pago_id')->references('id')->on('metodos_pagos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('forma_pago_id')->references('id')->on('formas_pagos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('datos_facturas');
    }
}
