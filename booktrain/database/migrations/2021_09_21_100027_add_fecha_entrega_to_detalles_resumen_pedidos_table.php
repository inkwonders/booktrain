<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFechaEntregaToDetallesResumenPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->timestamp('fecha_entrega')->nullable()->after('entregado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->dropColumn('fecha_entrega');
        });
    }
}
