<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketArchivosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_archivos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('archivo')->nullable();
            $table->string('archivo_nombre_original')->nullable();
            $table->integer('orden')->default(1);
            $table->string('tipo_archivo')->nullable();
            $table->longText('url');
            $table->timestamps();
            $table->integer('ticket_usuario_id')->unsigned();
            $table->foreign('ticket_usuario_id')->references('id')->on('ticket_usuarios')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_archivos');
    }
}
