<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrigenToRegistrosPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            $table->enum('origen', ['MANUAL', 'API', 'WEBHOOK', 'CARGA MASIVA', 'VENTA MOVIL', 'CRON'])->after('referencia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            $table->dropColumn('origen');
        });
    }
}
