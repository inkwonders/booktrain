<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColegioIdToConfiguracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuraciones', function (Blueprint $table) {
            $table->integer('configuracion_padre_id')->unsigned()->index()->nullable()->after('id');
            $table->foreign('configuracion_padre_id')->references('id')->on('configuraciones')->onDelete('CASCADE');
            $table->integer('colegio_id')->unsigned()->index()->nullable()->after('configuracion_padre_id');
            $table->foreign('colegio_id')->references('id')->on('colegios');
            $table->enum('categoria', ['CUENTAS', 'PAGOS', 'ENVIOS', 'OTRO'])->default('OTRO')->after('valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuraciones', function (Blueprint $table) {
            $table->dropForeign('configuraciones_colegio_id_foreign');
            $table->dropForeign('configuraciones_configuracion_padre_id_foreign');
            $table->dropColumn('configuracion_padre_id');
            $table->dropColumn('colegio_id');
            $table->dropColumn('categoria');
        });
    }
}