<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldPuntoReordenLibrosColegios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('libros_colegios', function (Blueprint $table) {
            $table->integer('punto_reorden')->default(0)->after('stock')->comment('Stock mínimo para volver a surtir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('libros_colegios', function (Blueprint $table) {
            $table->dropColumn('punto_reorden');
        });
    }
}
