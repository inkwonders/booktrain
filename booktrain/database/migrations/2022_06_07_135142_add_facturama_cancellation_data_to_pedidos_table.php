<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFacturamaCancellationDataToPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {

                $table->string('facturama_status')->after('facturama_invoice_id')->nullable();
                $table->text('facturama_cancellation_response')->after('referencia_factura')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {

            $table->dropColumn('facturama_status');
            $table->dropColumn('facturama_cancellation_response');


        });
    }
}
