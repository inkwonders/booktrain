<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuponesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedido_id')->unsigned()->index()->nullable()->comment('El ID del pedido que lo utilizó');
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->string('codigo', 15);
            $table->double('valor');
            $table->enum('tipo', ['PORCENTAJE', 'MONTO'])->default('PORCENTAJE');
            $table->enum('status', ['DISPONIBLE', 'UTILIZADO', 'VENCIDO'])->default('DISPONIBLE');
            $table->timestamp('caducidad')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupones');
    }
}
