<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketEntradasArchivosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_entradas_archivos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('url');
            $table->string('archivo_nombre_original');
            $table->string('tipo_archivo');
            $table->integer('orden');
            $table->timestamps();
            $table->integer('entrada_id')->unsigned();
            // $table->integer('archivo_id')->unsigned();
            $table->foreign('entrada_id')->references('id')->on('ticket_entradas')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('archivo_id')->references('id')->on('ticket_archivos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_entradas_archivos');
    }
}
