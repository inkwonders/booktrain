<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketEntradasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_entradas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('tipo_entrada')->comment("1-cliente, 2-soporte");
            $table->longText('comentario');
            // $table->string('carpeta');
            $table->timestamps();
            $table->integer('ticket_usuario_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->foreign('ticket_usuario_id')->references('id')->on('ticket_usuarios')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_entradas');
    }
}
