<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetallesResumenPedidosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('resumen_pedidos_id')->unsigned();
            $table->integer('libro_id')->unsigned();
            $table->double('precio_libro');
            $table->smallInteger('cantidad')->default('1');
            $table->timestamps();
            $table->foreign('resumen_pedidos_id')->references('id')->on('resumen_pedidos')->onDelete('cascade');
            $table->foreign('libro_id')->references('id')->on('libros')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalles_resumen_pedidos');
    }
}
