<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketUsuariosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_usuarios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('folio');
            $table->smallInteger('status')->comment("1-nuevo, 2-respuesta cliente, 3-respuesta soporte, 0-caso cerrado")->default(1);
            $table->string('titulo');
            $table->longText('descripcion');
            $table->integer('calificacion')->nullable();
            $table->longText('comentario_calificacion')->nullable();
            $table->dateTime('fecha_calificacion')->nullable();
            $table->string('telefono');
            $table->integer('usuario_id')->unsigned();
            $table->integer('ticket_motivos_id')->unsigned();
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('ticket_motivos_id')->references('id')->on('ticket_motivos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_usuarios');
    }
}
