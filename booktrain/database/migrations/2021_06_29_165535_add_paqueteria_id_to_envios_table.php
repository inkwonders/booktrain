<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaqueteriaIdToEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('envios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('paqueteria_id')->unsigned()->after('guia')->nullable()->index();
            $table->foreign('paqueteria_id')->references('id')->on('servicios_paqueteria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('envios', function (Blueprint $table) {
            $table->dropForeign('envios_paqueteria_id_foreign');
            $table->dropColumn('paqueteria_id');
        });
    }
}