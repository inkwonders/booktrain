<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrosPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registros_pagos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pedido_id')->unsigned()->index();
            $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade')->onUpdate('cascade');
            $table->double('amount');
            $table->string('source');
            $table->string('transactionTokenId');
            $table->text('raw')->nullable();
            $table->enum('status', ['SUCCESS', 'FAILED', 'REVIEW', 'CHARGEABLE', 'WAIT_THREEDS', 'REJECT', 'DONE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros_pagos');
    }
}

/**
 {"source":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","amount":51.0,"description":"Compra de libros","status":"success","transactionTokenId":"7716e730-61e3-459c-bfc9-56de57befe72","redirect3dsUri":"http://booktrain.test/mis-pedidos/","returnUrl":null,"paymentMethod":"card","currency":"MXN","createdAt":"2021-05-07T16:26:22.949+0000","error":null,"installments":null,"ship":{"city":"Aguascalientes","country":"MX","firstName":"sfsfsd","lastName":"sdfsfsd","phoneNumber":"1651561544","postalCode":"","state":"Aguascalientes","street1":"Bolivar","street2":"algarin","shippingMethod":"CUSTOM"},"client":null,"saveCard":false,"instegrationsdk":null,"integrationSdkVersion":null,"cvv":null,"merchantRefCode":null,"seamless":false,"paymentSource":{"cardDefault":false,"card":{"token":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","expYear":"22","expMonth":"11","lastFourDigits":"0002","cardHolderName":"dfgdfgdfg dfgdgd","brand":"visa","deviceFingerPrint":"1620404762040","ipAddress":"2806:103e:21:20a9:7839:8cf8:1dd3:af47","bank":"Test bank","type":"debit","country":"unknown","scheme":"unknown","cardPrefix":"400000","preAuth":false,"vault":false,"simpleUse":false,"integrationSdkVersion":"1.0.0"},"source":"token_ZaADgBXKiLzjGMkrDmNzFoiLKsDJejhinnCWEjdG","type":"card"},"billing":{"firstName":"sfsfsd","lastName":"sdfsfsd","email":"accept@netpay.com.mx","phone":"1651561544","ipAddress":"2806:103e:21:20a9:7839:8cf8:1dd3:af47","merchantReferenceCode":"compra-","address":{"city":"Singuilucan","country":"MX","postalCode":"dfgdfgd","state":"Hidalgo","street1":"gdgdfgdf","street2":"dfgdfgdf"}}}
 */
