<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibrosColegiosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros_colegios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('precio');
            $table->integer('libro_id')->unsigned();
            $table->integer('colegio_id')->unsigned();
            $table->integer('paquete_id')->unsigned()->nullable();
            $table->smallInteger('obligatorio')->nullable();
            $table->smallInteger('activo')->default(1);
            $table->smallInteger('bajo_pedido')->default(0);
            $table->integer('stock')->default(0);
            $table->timestamps();
            $table->foreign('libro_id')->references('id')->on('libros')->onDelete('cascade')->unsigned()->index();
            $table->foreign('colegio_id')->references('id')->on('colegios')->onDelete('cascade');
            $table->foreign('paquete_id')->references('id')->on('paquetes')->onDelete('cascade')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('libros_colegios');
    }
}
