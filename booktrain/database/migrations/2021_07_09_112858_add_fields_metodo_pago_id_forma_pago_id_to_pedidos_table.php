<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsMetodoPagoIdFormaPagoIdToPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->integer('forma_pago_id')->unsigned()->index()->after('id')->nullable();
            $table->foreign('forma_pago_id')->references('id')->on('formas_pagos');
            $table->integer('metodo_pago_id')->unsigned()->index()->after('id')->nullable();
            $table->foreign('metodo_pago_id')->references('id')->on('metodos_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign('pedidos_forma_pago_id_foreign');
            $table->dropForeign('pedidos_metodo_pago_id_foreign');

            $table->dropColumn('forma_pago_id');
            $table->dropColumn('metodo_pago_id');
        });
    }
}
