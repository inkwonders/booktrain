<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetodosPagosColegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metodos_pagos_colegios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('colegio_id')->unsigned()->index();
            $table->foreign('colegio_id')->references('id')->on('colegios');
            $table->integer('metodo_pago_id')->unsigned()->index();
            $table->foreign('metodo_pago_id')->references('id')->on('metodos_pagos');
            $table->boolean('activo')->default(0);
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metodos_pagos_colegios', function (Blueprint $table) {
            $table->dropForeign('metodos_pagos_formas_pagos_colegios_colegio_id_foreign');
            $table->dropForeign('metodos_pagos_formas_pagos_colegios_metodo_pago_id_foreign');
        });
        Schema::dropIfExists('metodos_pagos_colegios');
    }
}