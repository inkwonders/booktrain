<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferenciaToRegistrosPagos24052021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            //
            $table->string('referencia')->comment("En caso de venta movil, el vendedor pueda ingresar este campo libre para escribir algún folio o referencia de una operación en terminal")->nullable()->after("amount");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            //
            $table->dropColumn('referencia');
        });
    }
}
