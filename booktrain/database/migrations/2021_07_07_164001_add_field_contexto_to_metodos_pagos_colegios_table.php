<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldContextoToMetodosPagosColegiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metodos_pagos_colegios', function (Blueprint $table) {
            $table->enum('contexto', ['ONLINE', 'PRESENCIAL'])->default('ONLINE')->after('metodo_pago_id')->comment('Contexto en el cual aplica este método de pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metodos_pagos_colegios', function (Blueprint $table) {
            $table->dropColumn('contexto');
        });
    }
}
