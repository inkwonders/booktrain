<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFiscalregimeToDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

                $table->string('regimen_fiscal')->after('rfc')->nullable();
                $table->string('facturama_error',500)->after('forma_pago_id')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

                $table->dropColumn('regimen_fiscal');
                $table->dropColumn('facturama_error');

        });
    }
}
