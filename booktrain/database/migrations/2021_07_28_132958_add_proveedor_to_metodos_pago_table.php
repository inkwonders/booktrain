<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProveedorToMetodosPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metodos_pagos', function (Blueprint $table) {
            $table->enum('proveedor', ['NETPAY', 'OPENPAY', 'BBVA'])->after('descripcion')->nullable();
            $table->tinyInteger('disponible')->after('proveedor')->default(1)->comment('Inhabilita el metodo de pago, esta tiene máxima prioridad sobre el sistema, no está disponible desde una GUI');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metodos_pagos', function (Blueprint $table) {
            $table->dropColumn('proveedor');
            $table->dropColumn('disponible');
        });
    }
}
