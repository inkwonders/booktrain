<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacionesCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operaciones_cajas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('caja_id')->unsigned()->index();
            $table->foreign('caja_id')->references('id')->on('cajas');
            $table->enum('tipo', ['APERTURA', 'CORTE', 'CIERRE', 'REANUDACIÓN']);
            $table->integer('billete_1000')->default(0);
            $table->integer('billete_500')->default(0);
            $table->integer('billete_200')->default(0);
            $table->integer('billete_100')->default(0);
            $table->integer('billete_50')->default(0);
            $table->integer('billete_20')->default(0);
            $table->integer('conteo_monedas')->default(0)->comment('La cantidad de monedas físicas sin importar denominación');
            $table->float('monto_monedas')->default(0)->comment('La sumatoria del dinero en monedas');
            $table->string('observaciones', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operaciones_cajas');
    }
}
