<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('serie')->nullable();
            $table->string('folio')->unique()->nullable();
            // $table->smallInteger('status')->comment('0: Carrito, 1: Procesado, 2: Pagado, 3: Enviado, 4: Entregado, 5: Cancelado');
            $table->enum('status',['CARRITO','PAGO EN REVISION','PAGO RECHAZADO','PROCESADO','PAGADO','ENVIADO','ENTREGADO','CANCELADO']);
            $table->string('nombre_contacto')->nullable();
            $table->string('apellidos_contacto')->nullable();
            $table->string('celular_contacto')->nullable();
            $table->smallInteger('tipo_entrega')->nullable()->comment('1: Colegio, 2: Domicilio, 3: Pick-up');
            $table->smallInteger('factura')->nullable();
            $table->smallInteger('terminos_condiciones')->nullable();
            $table->double('subtotal')->nullable();
            $table->double('comision')->nullable();
            $table->double('total')->nullable();
            $table->integer('usuario_id')->unsigned();
            $table->integer('colegio_id')->unsigned();
            $table->integer('direccion_id')->unsigned()->nullable();
            // $table->enum('metodo_pago',['PUE','Diferido 3 meses','Diferido 6 meses','Diferido 9 meses'])->nullable();
            // $table->enum('forma_pago',['Tarjeta de crédito','Tarjeta de débito','American Express','Referencia interbancaria','Tienda de conveniencia'])->nullable();
            $table->enum('metodo_pago',['CREDITO','DEBITO','AMEX','BANCO','TIENDA'])->nullable();
            $table->enum('forma_pago',['PUE','DIFERIDO3M','DIFERIDO6M','DIFERIDO9M'])->nullable();
            $table->timestamps();
            $table->foreign('usuario_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('colegio_id')->references('id')->on('colegios')->onDelete('cascade');
            $table->foreign('direccion_id')->references('id')->on('direcciones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos');
    }
}
