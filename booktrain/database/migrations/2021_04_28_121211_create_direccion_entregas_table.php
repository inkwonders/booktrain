<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion_entregas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('colegio_id')->unsigned();
            $table->integer('direccion_id')->unsigned();
            $table->enum('tipo',['COLEGIO','PICK UP']);
            $table->timestamps();
            $table->foreign('colegio_id')->references('id')->on('colegios')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('direccion_id')->references('id')->on('direcciones')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccion_entregas');
    }
}
