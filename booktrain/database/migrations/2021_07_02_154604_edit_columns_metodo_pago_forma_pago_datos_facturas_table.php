<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditColumnsMetodoPagoFormaPagoDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {
            $table->enum('metodo_pago', ['PUE', 'PDD'])->after('cp')->nullable();
            $table->enum('forma_pago', ['01-EFECTIVO', '04-TARJETA CREDITO', '28-TARJETA DEBITO'])->after('cp')->nullable();
            $table->dropForeign('datos_facturas_metodo_pago_id_foreign');
            $table->dropForeign('datos_facturas_forma_pago_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {
            $table->dropColumn('metodo_pago');
            $table->dropColumn('forma_pago');
            $table->foreign('metodo_pago_id')->references('id')->on('metodos_pagos');
            $table->foreign('forma_pago_id')->references('id')->on('formas_pagos');
        });
    }
}
