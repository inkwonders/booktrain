<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClaveObjetoSatToLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('libros', function (Blueprint $table) {

            $table->string('objeto_impuesto')->after('isbn')->nullable();
            $table->string('clave_producto')->after('isbn')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('libros', function (Blueprint $table) {

                $table->dropColumn('objeto_impuesto');
                $table->dropColumn('clave_producto');

        });
    }
}
