<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEntregadoToDetallesResumenPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            //
            $table->engine = 'InnoDB';
            $table->integer('entregado')->after('cantidad')->nullable()->default('0')->comment('Este campo tiene valor de 0 por de fault, sirve para saber que libro de que paquete ha sido entregado en una entrega parcial en el reporte entregas admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            //
            $table->dropColumn('entregado');
        });
    }
}
