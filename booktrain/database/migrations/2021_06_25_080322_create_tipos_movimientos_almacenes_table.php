<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTiposMovimientosAlmacenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_movimientos_almacenes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('clave')->default('')->comment('Código interno para identificar el tipo de movimiento');
            $table->string('descripcion');
            $table->boolean('privado')->default(false)->comment('Necesario para determinar si se muestra o no en el selector de nuevo movimiento a inventario');
            $table->enum('naturaleza', ['ENTRADA', 'SALIDA']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_movimientos_almacenes');
    }
}