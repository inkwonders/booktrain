<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotificadoToReferenciasPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referencias_pedidos', function (Blueprint $table) {
            //
            $table->boolean('notificado')->after('response')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referencias_pedidos', function (Blueprint $table) {
            //
            $table->dropColumn('notificado');
        });
    }
}
