<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftdeleteToSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // Schema::table('api_calls_count', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('failed_jobs', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('migrations', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('model_has_permissions', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('model_has_roles', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('estados', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('municipios', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('password_resets', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('permissions', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('roles', function (Blueprint $table) { $table->softDeletes(); });
        // Schema::table('role_has_permissions', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('cfdis', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('colegios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('configuraciones', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('datos_facturas', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('direcciones', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('direccion_entregas', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('envios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('formas_pagos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('libros', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('libros_colegios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('metodos_pagos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('niveles_colegios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('notificaciones', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('paquetes', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('pedidos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('referencias_pedidos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('registros_pagos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('resumen_pedidos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('secciones_colegios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('sent_emails', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('sent_emails_url_clicked', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('ticket_archivos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('ticket_entradas', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('ticket_entradas_archivos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('ticket_motivos', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('ticket_usuarios', function (Blueprint $table) { $table->softDeletes(); });
        Schema::table('users', function (Blueprint $table) { $table->softDeletes(); });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('api_calls_count', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('failed_jobs', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('migrations', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('model_has_permissions', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('model_has_roles', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('estados', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('municipios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('password_resets', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('permissions', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('roles', function (Blueprint $table) { $table->dropSoftDeletes(); });
        // Schema::table('role_has_permissions', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('cfdis', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('colegios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('configuraciones', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('datos_facturas', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('direcciones', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('direccion_entregas', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('envios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('formas_pagos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('libros', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('libros_colegios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('metodos_pagos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('niveles_colegios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('notificaciones', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('paquetes', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('pedidos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('referencias_pedidos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('registros_pagos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('resumen_pedidos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('secciones_colegios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('sent_emails', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('sent_emails_url_clicked', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('ticket_archivos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('ticket_entradas', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('ticket_entradas_archivos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('ticket_motivos', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('ticket_usuarios', function (Blueprint $table) { $table->dropSoftDeletes(); });
        Schema::table('users', function (Blueprint $table) { $table->dropSoftDeletes(); });
    }
}
