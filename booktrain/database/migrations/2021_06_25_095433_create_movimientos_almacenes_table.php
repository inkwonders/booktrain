<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosAlmacenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos_almacenes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // $table->foreignId('colegio_id')->constrained('colegios')->comment('Almacén');
            // $table->foreignId('libro_id')->constrained('libros')->comment('Articulo en movimiento');
            // $table->foreignId('tipo_movimiento_almacen_id')->constrained('tipos_movimientos_almacenes');
            // $table->foreignId('user_id')->constrained('users')->comment('Responsable del movimiento');

            $table->integer('colegio_id')->unsigned()->comment('Almacén');
            $table->foreign('colegio_id')->references('id')->on('colegios');
            $table->integer('libro_id')->unsigned()->comment('Articulo en movimiento');
            $table->foreign('libro_id')->references('id')->on('libros');
            $table->integer('tipo_movimiento_almacen_id')->unsigned();
            $table->foreign('tipo_movimiento_almacen_id')->references('id')->on('tipos_movimientos_almacenes');
            $table->integer('user_id')->unsigned()->comment('Responsable del movimiento');
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('cantidad');
            $table->integer('cantidad_anterior')->default(0)->comment('Registra el valor anterior del inventario');
            $table->string('descripcion', 255)->default('');
            // $table->boolean('resultado')->default(false)->comment('Registra si fue exitosa o fallida la operación del almacén, en caso de inventarios negativos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos_almacenes');
    }
}