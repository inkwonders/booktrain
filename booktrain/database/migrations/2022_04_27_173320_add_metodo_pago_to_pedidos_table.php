<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetodoPagoToPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {


            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `pedidos` CHANGE `metodo_pago` `metodo_pago` ENUM('CREDITO','DEBITO','AMEX','BANCO','TIENDA','EFECTIVO','TERMINAL','TRANSFERENCIA','BBVADEBITOVPOS','BBVACREDITOVPOS','BBVATARJETA','OPDEBITO','OPAMEX','OPMASTERCARD','OPEFECTIVO','OPSPEI','OPBANCO','OPTIENDA','OPQRALIPAY','OPQRCODI','OPIVR','MERCADO PAGO')");
            Schema::enableForeignKeyConstraints();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {


            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `pedidos` CHANGE `metodo_pago` `metodo_pago` ENUM('CREDITO','DEBITO','AMEX','BANCO','TIENDA','EFECTIVO','TERMINAL','TRANSFERENCIA','BBVADEBITOVPOS','BBVACREDITOVPOS','BBVATARJETA','OPDEBITO','OPAMEX','OPMASTERCARD','OPEFECTIVO','OPSPEI','OPBANCO','OPTIENDA','OPQRALIPAY','OPQRCODI','OPIVR')");
            Schema::enableForeignKeyConstraints();


        });
    }
}
