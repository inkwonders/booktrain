<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldComprobacionToRegistrosPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            $table->timestamp('fecha_comprobacion')->after('confirm')->nullable()->comment('Fecha en la que se realizó la comprobación del status de pago');
            $table->text('raw_comprobacion')->after('confirm')->nullable()->comment('JSON con la respuesta de la comprobación del status de pago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registros_pagos', function (Blueprint $table) {
            $table->dropColumn('fecha_comprobacion');
            $table->dropColumn('raw_comprobacion');
        });
    }
}
