<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRazonRevolucionToDetallesResumenPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->double('porcentaje_descuento_devolucion')
                ->after('descuento_devolucion')
                ->default(0)
                ->comment('Indica el % del precio_libro que se descontó en descuento_devolucion');

            $table->enum('razon_devolucion', [
                    'FALTA DE STOCK',
                    'DEVOLUCION EN VENTA MOVIL',
                    'DEVOLUCION EN VENTA ONLINE'
                ])
                ->after('devuelto')
                ->nullable()
                ->comment('Indica la razón por la que fue devuelto el item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->dropColumn('razon_devolucion');
            $table->dropColumn('porcentaje_descuento_devolucion');
        });
    }
}
