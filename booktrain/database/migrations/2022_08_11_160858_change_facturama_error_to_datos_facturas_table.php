<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFacturamaErrorToDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

            $table->text('facturama_error')->after('forma_pago_id')->nullable()->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

            $table->string('facturama_error',500)->after('forma_pago_id')->nullable();


        });
    }
}
