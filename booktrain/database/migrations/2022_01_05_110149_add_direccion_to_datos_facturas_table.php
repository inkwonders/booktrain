<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDireccionToDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {
            $table->string('calle')->after('cp');
            $table->string('num_exterior')->after('calle');
            $table->string('num_interior')->after('num_exterior')->nullable();
            $table->string('colonia')->after('num_interior');
            $table->string('municipio')->after('colonia');
            $table->string('estado')->after('municipio');
            // $table->string('forma_pago')->after('estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {
            $table->dropColumn('calle');
            $table->dropColumn('num_exterior');
            $table->dropColumn('num_interior');
            $table->dropColumn('colonia');
            $table->dropColumn('municipio');
            $table->dropColumn('estado');
            // $table->dropColumn('forma_pago');
        });
    }
}
