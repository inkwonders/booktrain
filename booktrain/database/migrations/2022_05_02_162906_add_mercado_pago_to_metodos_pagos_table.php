<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMercadoPagoToMetodosPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metodos_pagos', function (Blueprint $table) {

            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `metodos_pagos` CHANGE `proveedor` `proveedor` ENUM('NETPAY','OPENPAY','BBVA','MERCADO PAGO')");
            Schema::enableForeignKeyConstraints();



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metodos_pagos', function (Blueprint $table) {

            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `metodos_pagos` CHANGE `proveedor` `proveedor` ENUM('NETPAY','OPENPAY','BBVA')");
            Schema::enableForeignKeyConstraints();

        });
    }
}
