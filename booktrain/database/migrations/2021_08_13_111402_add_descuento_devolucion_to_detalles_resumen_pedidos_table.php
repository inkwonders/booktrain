<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescuentoDevolucionToDetallesResumenPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->double('descuento_devolucion')->after('devuelto')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->dropColumn('descuento_devolucion');
        });
    }
}
