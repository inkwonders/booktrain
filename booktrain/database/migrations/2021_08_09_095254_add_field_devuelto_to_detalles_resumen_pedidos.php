<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldDevueltoToDetallesResumenPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->tinyInteger('devuelto')->default(0)->after('enviado')->comment('Determina si el producto fue devuelto, lo que puede hacer que tenga un precio_libro menor a otros detalles');
            $table->timestamp('fecha_devolucion')->nullable()->after('updated_at')->comment('Momento en que fue devuelto el producto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalles_resumen_pedidos', function (Blueprint $table) {
            $table->dropColumn('devuelto');
            $table->dropColumn('fecha_devolucion');
        });
    }
}
