<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferenciaPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referencias_pedidos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pedido_id')->unsigned();
            $table->string('referencia');
            $table->enum('tipo',['BANCARIA','TIENDA']);
            $table->smallInteger('dias_vigencia')->default(2);
            $table->boolean('pagado')->default(0);
            $table->timestamp('fecha_pago')->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
            $table->foreign('pedido_id')->references('id')->on('pedidos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referencias_pedidos');
    }
}
