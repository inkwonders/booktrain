<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetodoPorDefinirToDatosFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `datos_facturas` CHANGE `forma_pago` `forma_pago` ENUM('01-EFECTIVO','04-TARJETA CREDITO','28-TARJETA DEBITO','99-POR DEFINIR')");
            Schema::enableForeignKeyConstraints();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_facturas', function (Blueprint $table) {

            Schema::disableForeignKeyConstraints();
            DB::statement("ALTER TABLE `datos_facturas` CHANGE `forma_pago` `forma_pago` ENUM('01-EFECTIVO','04-TARJETA CREDITO','28-TARJETA DEBITO')");
            Schema::enableForeignKeyConstraints();
        });
    }
}
