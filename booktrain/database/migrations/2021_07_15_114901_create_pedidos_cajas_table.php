<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosCajasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos_cajas', function (Blueprint $table) {
            $table->integer('pedido_id')->unsigned()->index();
            $table->foreign('pedido_id')->references('id')->on('pedidos');
            $table->integer('caja_id')->unsigned()->index();
            $table->foreign('caja_id')->references('id')->on('cajas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos_cajas', function (Blueprint $table) {
            $table->dropForeign('pedidos_cajas_pedido_id_foreign');
            $table->dropForeign('pedidos_cajas_caja_id_foreign');
        });
        Schema::dropIfExists('pedidos_cajas');
    }
}
