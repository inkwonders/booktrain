<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsuarioVentaIdToPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->integer('usuario_venta_id')->unsigned()->index()->after('usuario_id')->nullable()->comment('ID del usuario que realizó la venta (Vendedor)');
            $table->foreign('usuario_venta_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropForeign('pedidos_usuario_venta_id_foreign');
            $table->dropColumn('usuario_venta_id');
        });
    }
}
