<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // if(env('APP_ENV') == 'local'){
        //     $this->call(CargaConfiguracionesColegiosSeeder::class);
        // }

        //$this->call(ConfiguracionesSeeder::class);
        //$this->call(RoleSeeder::class);
        //$this->call(UsuariosSeeder::class);
        //$this->call(ColegiosSeeder::class);
        //$this->call(EstadosMunicipiosSeeder::class);
        //$this->call(DireccionesSeeder::class);
        //$this->call(SeccionesColegiosSeeder::class);
        //$this->call(NivelesColegiosSeeder::class);
        //$this->call(LibrosSeeder::class);
        //$this->call(FormasPagosSeeder::class);
        //$this->call(MetodosPagosSeeder::class);
        //$this->call(UsosCfdiSeeder::class);
        //$this->call(PaquetesSeeder::class);
        //$this->call(PedidosSeeder::class);
        //$this->call(DatosFacturasSeeder::class);
        //$this->call(DireccionEntregaSeeder::class);
        //$this->call(EnviosSeeder::class);
        //$this->call(ResumenSeeder::class);
        //$this->call(DetallesResumenPedidosSedder::class);
        //$this->call(LibrosColegioSeeder::class);
        //$this->call(TicketMotivosSeeder::class);
        //$this->call(TicketUsuariosSeeder::class);
        //$this->call(TicketEntradasSeeder::class);
        //$this->call(TicketArchivosSeeder::class);
        //$this->call(TicketEntradasArchivosSeeder::class);
        //$this->call(NotificacionesSeeder::class);
        //$this->call(ReferenciaPedidoSeeder::class);
        //$this->call(TiposMovimientosAlmacenesSeeder::class);
        //$this->call(ServiciosPaqueteriaSeeder::class);
        //$this->call(ReestructuraDatosFacturasSeeder::class);

        // Comentar esta línea
        // $this->call(FakeDataSeeder::class);
    }
}
