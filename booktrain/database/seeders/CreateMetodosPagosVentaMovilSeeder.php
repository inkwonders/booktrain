<?php

namespace Database\Seeders;

use App\Models\Colegio;
use App\Models\MetodoPago;
use Illuminate\Database\Seeder;

class CreateMetodosPagosVentaMovilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MetodoPago::create(['id' => 6, 'codigo' => 'EFECTIVO', 'descripcion' => 'Pago en efectivo']);
        MetodoPago::create(['id' => 7, 'codigo' => 'TERMINAL', 'descripcion' => 'Terminal bancaria']);
        MetodoPago::create(['id' => 8, 'codigo' => 'TRANSFERENCIA', 'descripcion' => 'Transferencia electrónica']);

        Colegio::get()->each(function($colegio) {
            $colegio->metodosPago()->attach(6, ['contexto' => 'PRESENCIAL', 'activo' => 1]);
            $colegio->metodosPago()->attach(7, ['contexto' => 'PRESENCIAL', 'activo' => 1]);
            $colegio->metodosPago()->attach(8, ['contexto' => 'PRESENCIAL', 'activo' => 1]);

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => 6, 'comision' => 0, 'meses' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => 7, 'comision' => 0, 'meses' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => 8, 'comision' => 0, 'meses' => 0, 'minimo' => 0, 'activo' => 0]);
        });
    }
}
