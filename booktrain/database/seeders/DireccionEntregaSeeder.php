<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DireccionEntregaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('direccion_entregas')->insert([
            [
                'colegio_id' => '1',
                'direccion_id' => '2',
                'tipo' => 'COLEGIO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'colegio_id' => '1',
                'direccion_id' => '1',
                'tipo' => 'PICK UP',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], /*[
                'colegio_id' => '1',
                'direccion_id' => '3',
                'tipo' => 'PICK UP',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],*/
            [
                'colegio_id' => '2',
                'direccion_id' => '4',
                'tipo' => 'COLEGIO',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'colegio_id' => '2',
                'direccion_id' => '1',
                'tipo' => 'PICK UP',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]/*, [
                'colegio_id' => '2',
                'direccion_id' => '5',
                'tipo' => 'PICK UP',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
