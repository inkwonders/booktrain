<?php

namespace Database\Seeders;

use App\Models\ServicioPaqueteria;
use Illuminate\Database\Seeder;

class ServiciosPaqueteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServicioPaqueteria::create(['nombre' => 'Fedex', 'url' => 'https://www.fedex.com/es-mx/tracking.html', 'activo' => 1]);
    }
}