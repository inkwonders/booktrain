<?php

namespace Database\Seeders;
use App\Models\TicketMotivo;
use Illuminate\Database\Seeder;

class TicketMotivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TicketMotivo::insert([



            ['orden' => '1','nombre'=>'Servicio a Cliente','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '2','nombre'=>'Error en pedido','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '3','nombre'=>'Pago','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '4','nombre'=>'Facturación','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '5','nombre'=>'Entregas','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '5','nombre'=>'Cancelación','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '5','nombre'=>'Comprobante de compra','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '5','nombre'=>'Registro y mi Perfil','created_at'=>date('Y-m-d H:i:s')],
            ['orden' => '5','nombre'=>'Sugerencias','created_at'=>date('Y-m-d H:i:s')],



             ]);
    }
}
// Servicio a Cliente, Error en pedido, Pago, Facturación, Entregas, Cancelación, Comprobante de compra, Registro y mi Perfil, Sugerencias