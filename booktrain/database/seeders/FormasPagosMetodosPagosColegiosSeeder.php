<?php

namespace Database\Seeders;

use App\Models\FormaPago;
use App\Models\MetodoPago;
use App\Models\Colegio;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormasPagosMetodosPagosColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks = 0');
        MetodoPago::truncate();

        FormaPago::truncate();

        FormaPago::create(['id' => 1, 'codigo' => 'PUE', 'descripcion' => 'Pago en una exhibición']);
        FormaPago::create(['id' => 2, 'codigo' => 'DIFERIDO3M', 'descripcion' => 'Pago diferido a 3 meses']);
        FormaPago::create(['id' => 3, 'codigo' => 'DIFERIDO6M', 'descripcion' => 'Pago diferido a 6 meses']);
        FormaPago::create(['id' => 4, 'codigo' => 'DIFERIDO9M', 'descripcion' => 'Pago diferido a 9 meses']);

        MetodoPago::create(['id' => 1, 'codigo' => 'DEBITO', 'descripcion' => 'Tarjeta de débito']);
        MetodoPago::create(['id' => 2, 'codigo' => 'CREDITO', 'descripcion' => 'Tarjeta de crédito']);
        MetodoPago::create(['id' => 3, 'codigo' => 'AMEX', 'descripcion' => 'American Express']);
        MetodoPago::create(['id' => 4, 'codigo' => 'BANCO', 'descripcion' => 'Referencia interbancaria']);
        MetodoPago::create(['id' => 5, 'codigo' => 'TIENDA', 'descripcion' => 'Tienda de conveniencia']);

        DB::statement('SET foreign_key_checks = 1');

        // -------------------------------------------------------------------------------------------------------------------
        $metodos_pago_base = MetodoPago::all();
        $formas_pago_base = FormaPago::all();

        // A partir de aquí creamos la configuración para los colegios
        Colegio::get()->each(function($colegio) use ($metodos_pago_base, $formas_pago_base) {
            // PROVISIONAL: Eliminamos las asociaciones presentes
            $colegio->metodosPago()->detach();

            // Este comando se debe correr antes de:
            // php artisan db:seed --class=CargaConfiguracionesColegiosSeeder
            // La configuración devuelta es un json (String)
            $metodos_pago_colegio = collect(json_decode($colegio->configuracion)->metodos_pago)->keyBy('nombre');

            foreach ($metodos_pago_base as $metodo_pago_base) {
                // Agregamos los metodos de pago al colegio
                $colegio->metodosPago()->attach($metodo_pago_base->id, ['activo' => isset($metodos_pago_colegio [$metodo_pago_base->descripcion]) ]);

                // Provisional, eliminamos las asociaciones actuales
                $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->wherePivot('colegio_id', $colegio->id)->detach();

                if(isset($metodos_pago_colegio [$metodo_pago_base->descripcion])) {
                    // Agregamos la formas de pago
                    $formas_pago_colegio = collect($metodos_pago_colegio[$metodo_pago_base->descripcion]->formas_pago)->keyBy('valor');

                    foreach ($formas_pago_base as $forma_pago_base) {
                        if(isset($formas_pago_colegio[$forma_pago_base->codigo] )) {

                            $colegio->metodosPago()
                                ->wherePivot('metodo_pago_id', $metodo_pago_base->id)
                                ->first()
                                ->formasPago()
                                ->attach($forma_pago_base->id, [
                                    'colegio_id' => $colegio->id,
                                    'meses' => isset($formas_pago_colegio[$forma_pago_base->codigo]->meses) ? $formas_pago_colegio[$forma_pago_base->codigo]->meses : 0,
                                    'comision' => isset($formas_pago_colegio[$forma_pago_base->codigo]->comision) ? $formas_pago_colegio[$forma_pago_base->codigo]->comision : 0,
                                    'minimo' => isset($formas_pago_colegio[$forma_pago_base->codigo]->minimo) ? $formas_pago_colegio[$forma_pago_base->codigo]->minimo : 0,
                                    'activo' => 1
                                ]);
                        }else{

                            switch ($forma_pago_base->codigo) {
                                case 'PUE':
                                    $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => false]);
                                break;
                                case 'DIFERIDO3M':
                                    if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                        $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 3, 'comision' => 0, 'minimo' => 300, 'activo' => false]);
                                break;
                                case 'DIFERIDO6M':
                                    if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                        $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 6, 'comision' => 0, 'minimo' => 600, 'activo' => false]);
                                break;
                                case 'DIFERIDO9M':
                                    if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                        $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 9, 'comision' => 0, 'minimo' => 900, 'activo' => false]);
                                break;
                            }
                        }
                    }
                }else{
                    // Si el colegio no tiene registrado el metodo de pago, entonces lo cargamos con todas las formas de pago base desactivadas
                    foreach ($formas_pago_base as $forma_pago_base) {
                        switch ($forma_pago_base->codigo) {
                            case 'PUE':
                                $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => false]);
                            break;
                            case 'DIFERIDO3M':
                                if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                    $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 3, 'comision' => 0, 'minimo' => 300, 'activo' => false]);
                            break;
                            case 'DIFERIDO6M':
                                if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                    $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 6, 'comision' => 0, 'minimo' => 600, 'activo' => false]);
                            break;
                            case 'DIFERIDO9M':
                                if(in_array($metodo_pago_base->codigo, ['CREDITO', 'AMEX']))
                                    $colegio->metodosPago()->wherePivot('metodo_pago_id', $metodo_pago_base->id)->first()->formasPago()->attach($forma_pago_base->id, ['colegio_id' => $colegio->id, 'meses' => 9, 'comision' => 0, 'minimo' => 900, 'activo' => false]);
                            break;
                        }
                    }
                }
            }
        });
    }
}
