<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsosCfdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cfdis')->insert([
            [
                'codigo'=>'G01',
                'descripcion'=>'Adquisición de mercancias',
                //'orden'=>'3',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'codigo'=>'G03',
                'descripcion'=>'Gastos en general',
                //'orden'=>'4',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'codigo'=>'P01',
                'descripcion'=>'Por definir',
                //'orden'=>'2',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
