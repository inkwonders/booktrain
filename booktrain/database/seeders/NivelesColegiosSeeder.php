<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NivelesColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('niveles_colegios')->insert([
            [
                'id'=>'1',
                'seccion_id'=>'1',
                'nombre'=>'KPrimero A',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'2',
                'seccion_id'=>'1',
                'nombre'=>'KSegundo A',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'3',
                'seccion_id'=>'1',
                'nombre'=>'KTercero A',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], /*[
                'id'=>'4',
                'seccion_id'=>'1',
                'nombre'=>'KPrimero B',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'5',
                'seccion_id'=>'1',
                'nombre'=>'KSegundo B',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'6',
                'seccion_id'=>'1',
                'nombre'=>'KTercero B',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'7',
                'seccion_id'=>'2',
                'nombre'=>'Pri Primero',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'8',
                'seccion_id'=>'2',
                'nombre'=>'Pri Segundo',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'9',
                'seccion_id'=>'2',
                'nombre'=>'Pri Tercero',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'10',
                'seccion_id'=>'2',
                'nombre'=>'Pri Cuarto',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'11',
                'seccion_id'=>'2',
                'nombre'=>'Pri Quinto',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'12',
                'seccion_id'=>'2',
                'nombre'=>'Pri Sexto',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], */[
                'id'=>'13',
                'seccion_id'=>'3',
                'nombre'=>'Sec Primero',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'14',
                'seccion_id'=>'3',
                'nombre'=>'Sec Segundo',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'15',
                'seccion_id'=>'3',
                'nombre'=>'Sec Tercero',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]/*, [
                'id'=>'16',
                'seccion_id'=>'4',
                'nombre'=>'Pre Primero',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'17',
                'seccion_id'=>'4',
                'nombre'=>'Pre Segundo',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'18',
                'seccion_id'=>'4',
                'nombre'=>'Pre Segundo',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
