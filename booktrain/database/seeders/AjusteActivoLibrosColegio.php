<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AjusteActivoLibrosColegio extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        Log::debug("Ajustando libros");
        $contador = 0;
        $libros = DB::table('libros_colegios')->where('paquete_id', null)->get();

        try {
            $libros->each(function($libro) use (&$contador) {
                DB::table('libros_colegios')
                    ->where('colegio_id', $libro->colegio_id)
                    ->where('libro_id', $libro->libro_id)
                    ->update([
                        'activo' => $libro->activo
                    ]);
                $contador ++;
            });
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            Log::error("Error al ajustar libros", [$th]);
        }

        Log::debug("Ajuste terminado: se ajustaron {$contador} de {$libros->count()}");
    }
}
