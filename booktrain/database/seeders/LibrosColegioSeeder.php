<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LibrosColegioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('libros_colegios')->insert([
            [
                'id' => 1,
                'precio'=>'-100',
                'libro_id'=>1,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 2,
                'precio'=>'-110',
                'libro_id'=>2,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 3,
                'precio'=>'-120',
                'libro_id'=>3,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 4,
                'precio'=>'-200',
                'libro_id'=>4,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 5,
                'precio'=>'-210',
                'libro_id'=>5,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 6,
                'precio'=>'-220',
                'libro_id'=>6,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 7,
                'precio'=>'-300',
                'libro_id'=>7,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 8,
                'precio'=>'-310',
                'libro_id'=>8,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 9,
                'precio'=>'-320',
                'libro_id'=>9,
                'colegio_id'=>1,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 10,
                'precio'=>'-140',
                'libro_id'=>10,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 11,
                'precio'=>'-150',
                'libro_id'=>11,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 12,
                'precio'=>'-160',
                'libro_id'=>12,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 13,
                'precio'=>'-240',
                'libro_id'=>13,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 14,
                'precio'=>'-250',
                'libro_id'=>14,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 15,
                'precio'=>'-260',
                'libro_id'=>15,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 16,
                'precio'=>'-340',
                'libro_id'=>16,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 1,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 17,
                'precio'=>'26',
                'libro_id'=>17,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 18,
                'precio'=>'-360',
                'libro_id'=>18,
                'colegio_id'=>2,
                'obligatorio' => null,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 20,
                'paquete_id'=>null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 19,
                'precio'=>'100',
                'libro_id'=>1,
                'colegio_id'=>1,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 20,
                'precio'=>'110',
                'libro_id'=>2,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 21,
                'precio'=>'120',
                'libro_id'=>3,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 22,
                'precio'=>'200',
                'libro_id'=>4,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 23,
                'precio'=>'210',
                'libro_id'=>5,
                'colegio_id'=>1,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 24,
                'precio'=>'220',
                'libro_id'=>6,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 25,
                'precio'=>'300',
                'libro_id'=>7,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 26,
                'precio'=>'310',
                'libro_id'=>8,
                'colegio_id'=>1,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 27,
                'precio'=>'320',
                'libro_id'=>9,
                'colegio_id'=>1,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 28,
                'precio'=>'140',
                'libro_id'=>10,
                'colegio_id'=>2,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 29,
                'precio'=>'150',
                'libro_id'=>11,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 30,
                'precio'=>'160',
                'libro_id'=>12,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>4,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 31,
                'precio'=>'240',
                'libro_id'=>13,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 32,
                'precio'=>'250',
                'libro_id'=>14,
                'colegio_id'=>2,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 33,
                'precio'=>'260',
                'libro_id'=>15,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>5,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 34,
                'precio'=>'340',
                'libro_id'=>16,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 35,
                'precio'=>'350',
                'libro_id'=>17,
                'colegio_id'=>2,
                'obligatorio' => 0,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id' => 36,
                'precio'=>'360',
                'libro_id'=>18,
                'colegio_id'=>2,
                'obligatorio' => 1,
                'activo' => 1,
                'bajo_pedido' => 0,
                'stock' => 0,
                'paquete_id'=>6,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
