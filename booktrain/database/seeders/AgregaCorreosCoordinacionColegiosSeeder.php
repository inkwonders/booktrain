<?php

namespace Database\Seeders;

use App\Models\Colegio;
use App\Models\Configuracion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class AgregaCorreosCoordinacionColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuraciones')->where('etiqueta', 'correo_comercial_provesa')->delete();
        DB::table('configuraciones')->where('etiqueta', 'correo_coordinador_provesa')->delete();
        DB::table('configuraciones')->where('etiqueta', 'correo_coordinador_colegio')->delete();

        Configuracion::create(['etiqueta' => 'correo_comercial_provesa', 'valor' => 'pedidosweb@booktrain.com.mx']);
        Configuracion::create(['etiqueta' => 'correo_coordinador_provesa', 'valor' => '']);

        Colegio::get()->each(function($colegio) {
            $colegio->configuraciones()->create(['etiqueta' => 'correo_comercial_provesa', 'valor' => ""]);
            $colegio->configuraciones()->create(['etiqueta' => 'correo_coordinador_provesa', 'valor' => ""]);
            $colegio->configuraciones()->create(['etiqueta' => 'correo_coordinador_colegio', 'valor' => ""]);
        });
    }
}
