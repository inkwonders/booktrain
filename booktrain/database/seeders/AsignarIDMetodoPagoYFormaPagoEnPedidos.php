<?php

namespace Database\Seeders;

use App\Models\Pedido;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AsignarIDMetodoPagoYFormaPagoEnPedidos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metodos_pago = MetodoPago::get()->keyBy('codigo');
        $formas_pago = FormaPago::get()->keyBy('codigo');

        $this->command->info('Afectando metodo_pago_id y forma_pago_id en pedidos');

        $contador = 0;

        Pedido::where('forma_pago_id', null)
            ->where('forma_pago_id', null)
            ->where('status', '!=', 'CARRITO')
            ->get()
            ->each(function($pedido) use ($metodos_pago, $formas_pago, &$contador) {

            if(
                isset($metodos_pago[ $pedido->metodo_pago ]) &&
                isset($formas_pago[ $pedido->forma_pago ])
            ) {
                $contador ++;
                DB::table('pedidos')
                    ->where('id', $pedido->id)->update([
                    'metodo_pago_id' => $metodos_pago[ $pedido->metodo_pago ]->id,
                    'forma_pago_id'  => $formas_pago[ $pedido->forma_pago ]->id
                ]);
            }
        });

        $this->command->info("Se afectaron {$contador} registros de pedidos");
    }
}
