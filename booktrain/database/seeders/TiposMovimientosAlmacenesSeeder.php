<?php

namespace Database\Seeders;

use App\Models\TipoMovimientoAlmacen;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposMovimientosAlmacenesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET foreign_key_checks = 0;');
        TipoMovimientoAlmacen::truncate();
        TipoMovimientoAlmacen::insert([
            ['clave' => 'ENT01', 'naturaleza' => 'ENTRADA','privado' => true, 'descripcion' => 'Entrada inicial de almacén'],
            ['clave' => 'ENT02', 'naturaleza' => 'ENTRADA','privado' => false, 'descripcion' => 'Entrada por compra'],
            ['clave' => 'ENT03', 'naturaleza' => 'ENTRADA','privado' => false, 'descripcion' => 'Entrada por re surtido'],
            ['clave' => 'ENT04', 'naturaleza' => 'ENTRADA','privado' => false, 'descripcion' => 'Entrada por devolución'],
            ['clave' => 'ENT05', 'naturaleza' => 'ENTRADA','privado' => false, 'descripcion' => 'Entrada por ajuste'],
            ['clave' => 'ENT06', 'naturaleza' => 'ENTRADA','privado' => true, 'descripcion' => 'Entrada por transferencia entre almacenes'],
            ['clave' => 'SAL01', 'naturaleza' => 'SALIDA','privado' => true, 'descripcion' => 'Salida por venta'],
            ['clave' => 'SAL02', 'naturaleza' => 'SALIDA','privado' => false, 'descripcion' => 'Salida por ajuste'],
            ['clave' => 'SAL03', 'naturaleza' => 'SALIDA','privado' => true, 'descripcion' => 'Salida por transferencia entre almacenes']
        ]);
        DB::statement('SET foreign_key_checks = 1;');
    }
}