<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaquetesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paquetes')->insert([
            [
                'id'=>'1',
                'nivel_id'=>'1',
                'nombre'=>'Libros Kinder 1',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'2',
                'nivel_id'=>'2',
                'nombre'=>'Cuadernillos Kinder 2',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'3',
                'nivel_id'=>'3',
                'nombre'=>'Materiales Kinder 3',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'4',
                'nivel_id'=>'13',
                'nombre'=>'Lib. Sec 1ro',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'5',
                'nivel_id'=>'14',
                'nombre'=>'Material Sec Segundo',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'6',
                'nivel_id'=>'15',
                'nombre'=>'Libros Sec Tercero',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]/*, [
                'id'=>'7',
                'nivel_id'=>'7',
                'nombre'=>'Libros Preparatoria 1',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'8',
                'nivel_id'=>'8',
                'nombre'=>'Libros Preparatoria 2',
                'activo'=>'1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
