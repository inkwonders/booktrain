<?php

namespace Database\Seeders;
use App\Models\TicketArchivo;
use Illuminate\Database\Seeder;

class TicketArchivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TicketArchivo::insert([



            ['archivo' => 'archivo.jpg','archivo_nombre_original'=>'archivo-nombre-completo.jpg','orden'=>'1','tipo_archivo'=>'IMAGEN','url'=>'assets/images/images_archivos/archivo.jpg','ticket_usuario_id'=>'1' ,'created_at'=>date('Y-m-d H:i:s')],
            ['archivo' => 'archivo.jpg','archivo_nombre_original'=>'archivo-nombre-completo.jpg','orden'=>'2','tipo_archivo'=>'IMAGEN','url'=>'assets/images/images_archivos/archivo.jpg','ticket_usuario_id'=>'2' ,'created_at'=>date('Y-m-d H:i:s')],
            ['archivo' => 'archivo.jpg','archivo_nombre_original'=>'archivo-nombre-completo.jpg','orden'=>'3','tipo_archivo'=>'IMAGEN','url'=>'assets/images/images_archivos/archivo.jpg','ticket_usuario_id'=>'3' ,'created_at'=>date('Y-m-d H:i:s')],
            ['archivo' => 'archivo.jpg','archivo_nombre_original'=>'archivo-nombre-completo.jpg','orden'=>'4','tipo_archivo'=>'IMAGEN','url'=>'assets/images/images_archivos/archivo.jpg','ticket_usuario_id'=>'4' ,'created_at'=>date('Y-m-d H:i:s')],
            ['archivo' => 'archivo.jpg','archivo_nombre_original'=>'archivo-nombre-completo.jpg','orden'=>'5','tipo_archivo'=>'IMAGEN','url'=>'assets/images/images_archivos/archivo.jpg','ticket_usuario_id'=>'5' ,'created_at'=>date('Y-m-d H:i:s')],



             ]);
    }
}
