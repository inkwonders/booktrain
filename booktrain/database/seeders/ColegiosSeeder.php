<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Colegio;
use Illuminate\Support\Facades\DB;

class ColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colegios = [
            [
                'id'=>'1',
                'codigo'=>'CC-1111',
                'nombre'=>'Colegio Demo',
                'logo'=>'logo1.jpg',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'2',
                'codigo'=>'DD-2222',
                'nombre'=>'Colegio Demo 2',
                'logo'=>'logo2.jpg',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        foreach($colegios as $colegio) {
            Colegio::create($colegio);
        }
    }
}
