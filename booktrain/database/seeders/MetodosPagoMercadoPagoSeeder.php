<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MetodoPago;
use App\Models\FormaPago;
use App\Models\Colegio;

class MetodosPagoMercadoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metodo_1 = MetodoPago::create(['codigo' => 'MERCADO PAGO',  'descripcion' => 'Mercado Pago', 'proveedor' => 'MERCADO PAGO', 'disponible' => 1]);                // ID $metodo_1->id

        Colegio::get()->each(function($colegio) use ($metodo_1) {

            $colegio->metodosPago()->attach($metodo_1->id, ['contexto' => 'ONLINE', 'activo' => 1]);


            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_1->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUE

        });
    }
}
