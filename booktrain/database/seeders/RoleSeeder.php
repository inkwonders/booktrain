<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role1 = Role::create(['name' => 'Admin']);
        $role2 = Role::create(['name' => 'Cliente']);
        $role3 = Role::create(['name' => 'Colegio']);

        // Permission::create(['name' => 'admin.home'])->assignRole($role1);
        //se le da el nombre de la ruta que se desea proteger

        //sirve para asignarle a esta ruta varios roles y permisos, no solo 1 (todos los roles con el cual queremos relacionaer este permiso)
        // Permission::create(['name' => 'admin.home'])->syncRoles([$role1,$role2]);


        Permission::create(['name' => 'CC-1111'])->assignRole($role3);
        Permission::create(['name' => 'DD-2222'])->assignRole($role3);
        Permission::create(['name' => 'TORREON21'])->assignRole($role3);
        Permission::create(['name' => 'SALTILLO21'])->assignRole($role3);
        Permission::create(['name' => 'CEDROS2021'])->assignRole($role3);
        Permission::create(['name' => 'UTEQ2021'])->assignRole($role3);

        Permission::create(['name' => 'menu_soporte'])->assignRole($role1);
        Permission::create(['name' => 'menu_reportes'])->assignRole($role1);
        Permission::create(['name' => 'menu_pedidos'])->assignRole($role1);
        Permission::create(['name' => 'menu_colegios'])->assignRole($role1);
        Permission::create(['name' => 'menu_colegios_editando'])->assignRole($role1);
        Permission::create(['name' => 'menu_libros'])->assignRole($role1);
        Permission::create(['name' => 'menu_pedidos_usuarios'])->assignRole($role1);
        Permission::create(['name' => 'menu_referencia_bbva'])->assignRole($role1);
        Permission::create(['name' => 'menu_pedidos_entregados'])->assignRole($role1);
        Permission::create(['name' => 'menu_pedidos_enviados'])->assignRole($role1);

        Permission::create(["name" => "menu_usuarios"])->assignRole($role1);
        Permission::create(["name" => "menu_envios"])->assignRole($role1);
        Permission::create(["name" => "menu_referencias_bbva"])->assignRole($role1);
        Permission::create(["name" => "menu_impresion_pedidos"])->assignRole($role1);


        Permission::create(["name" => "menu_entregas_parciales"])->assignRole($role1);
        Permission::create(["name" => "ver almacen"])->assignRole($role1);
    }
}
