<?php

namespace Database\Seeders;

use App\Models\Colegio;
use App\Models\FormaPago;
use App\Models\MetodoPago;
use Illuminate\Database\Seeder;

class CreaMetodosPagoOpenPaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ALTER TABLE `pedidos` CHANGE `metodo_pago` `metodo_pago` ENUM('CREDITO','DEBITO','AMEX','BANCO','TIENDA','EFECTIVO','TERMINAL','TRANSFERENCIA','BBVADEBITOVPOS','BBVACREDITOVPOS','BBVATARJETA','OPDEBITO','OPAMEX','OPMASTERCARD','OPEFECTIVO','OPSPEI','OPBANCO','OPTIENDA','OPQRALIPAY','OPQRCODI','OPIVR') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
        // Creamos los metodos de pago de openpay
        $metodo_pago_1 = MetodoPago::create(['codigo' => 'OPDEBITO',    'descripcion' => 'Tarjeta de Débito',       'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_2 = MetodoPago::create(['codigo' => 'OPAMEX',      'descripcion' => 'American Express',        'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_3 = MetodoPago::create(['codigo' => 'OPMASTERCARD','descripcion' => 'Visa/Mastercard',         'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_4 = MetodoPago::create(['codigo' => 'OPEFECTIVO',  'descripcion' => 'Efectivo',                'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_5 = MetodoPago::create(['codigo' => 'OPSPEI',      'descripcion' => 'Transferencia SPEI',      'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_6 = MetodoPago::create(['codigo' => 'OPBANCO',     'descripcion' => 'CIE Bancomer/SPEI',       'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_7 = MetodoPago::create(['codigo' => 'OPTIENDA',    'descripcion' => 'Pago en Extra',            'proveedor' => 'OPENPAY', 'disponible' => 1]);
        $metodo_pago_8 = MetodoPago::create(['codigo' => 'OPQRALIPAY',  'descripcion' => 'Pago con QR AliPay',      'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_9 = MetodoPago::create(['codigo' => 'OPQRCODI',    'descripcion' => 'Pago con QR CoDi',        'proveedor' => 'OPENPAY', 'disponible' => 0]);
        $metodo_pago_10 = MetodoPago::create(['codigo' => 'OPIVR',      'descripcion' => 'Autorizador remoto IVR',  'proveedor' => 'OPENPAY', 'disponible' => 0]);

        Colegio::get()->each(function($colegio) use (
            $metodo_pago_1, $metodo_pago_2, $metodo_pago_3, $metodo_pago_4, $metodo_pago_5, $metodo_pago_6, $metodo_pago_7, $metodo_pago_8, $metodo_pago_9, $metodo_pago_10 ) {

            // Asignamos los metodos de pago de openpay a los colegios
            $colegio->metodosPago()->attach($metodo_pago_1->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_2->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_3->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_4->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_5->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_6->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_7->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_8->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_9->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_pago_10->id, ['contexto' => 'ONLINE', 'activo' => 1]);

            // Asignamos las formas de pago para los metodos de pago de openpay en los colegios
            $colegio->formasPago()->attach(1, ['metodo_pago_id' =>  $metodo_pago_1->id, 'meses' => 0, 'comision' => 2.9, 'minimo' => 0, 'activo' => 0]); // 2.9% + $2.50 MXN

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_2->id, 'meses' => 0, 'comision' => 2.9, 'minimo' => 0, 'activo' => 0]); // 2.9% + $2.50 MXN
            $colegio->formasPago()->attach(2, ['metodo_pago_id' => $metodo_pago_2->id, 'meses' => 3, 'comision' => 3.8, 'minimo' => 300, 'activo' => 0]);
            $colegio->formasPago()->attach(3, ['metodo_pago_id' => $metodo_pago_2->id, 'meses' => 6, 'comision' => 5.8, 'minimo' => 600, 'activo' => 0]);
            $colegio->formasPago()->attach(4, ['metodo_pago_id' => $metodo_pago_2->id, 'meses' => 9, 'comision' => 7.8, 'minimo' => 900, 'activo' => 0]);

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_3->id, 'meses' => 0, 'comision' => 2.9, 'minimo' => 0, 'activo' => 0]); // 2.9% + $2.50 MXN
            $colegio->formasPago()->attach(2, ['metodo_pago_id' => $metodo_pago_3->id, 'meses' => 3, 'comision' => 4.8, 'minimo' => 300, 'activo' => 0]);
            $colegio->formasPago()->attach(3, ['metodo_pago_id' => $metodo_pago_3->id, 'meses' => 6, 'comision' => 7.8, 'minimo' => 600, 'activo' => 0]);
            $colegio->formasPago()->attach(4, ['metodo_pago_id' => $metodo_pago_3->id, 'meses' => 9, 'comision' => 10.8, 'minimo' => 900, 'activo' => 0]);

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_4->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_5->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // $8.00 MXN
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_6->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_7->id, 'meses' => 0, 'comision' => 2.9, 'minimo' => 0, 'activo' => 0]); // 2.9% + $2.50 MXN
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_8->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_9->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]);
            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_pago_10->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]);
        });
    }
}
