<?php

namespace Database\Seeders;

use App\Models\Colegio;
use App\Models\TipoMovimientoAlmacen;
use Illuminate\Database\Seeder;

class CargaAlmacenInicialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Entrada inicial de almacén
        $tipo_movimiento_inicial = TipoMovimientoAlmacen::where('clave', 'ENT01')->first();

        Colegio::all()->each(function($colegio) use ($tipo_movimiento_inicial) {

            // Consultamos existencias en libros del colegio y los damos de alta como movimientos de almacén
            $colegio->libros()->activo()->wherePivot('paquete_id', null)->each(function($libro) use ($colegio, $tipo_movimiento_inicial) {

                $colegio->movimientosAlmacen()->create([
                    'libro_id' => $libro->id,
                    'tipo_movimiento_almacen_id' => $tipo_movimiento_inicial->id,
                    'user_id' => 1, // Usuario administrador?
                    'cantidad' => $libro->pivot->stock,
                    'cantidad_anterior' => 0,
                    'descripcion' => $tipo_movimiento_inicial->descripcion
                ]);
            });
        });
    }
}