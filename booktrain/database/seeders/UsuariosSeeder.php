<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;


class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('usuarios')->truncate();
        User::create(['name' => 'alain', 'apellidos' => 'apellido', 'email' => 'alain@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Admin')
        ->givePermissionTo(
        'menu_soporte',
        'menu_reportes',
        'menu_pedidos',
        'menu_colegios',
        'menu_colegios_editando',
        'menu_libros',
        'menu_pedidos_usuarios',
        'menu_referencia_bbva',
        'menu_pedidos_entregados',
        'menu_pedidos_enviados');

        User::create(['name' => 'manuel', 'apellidos' => 'apellido', 'email' => 'manuel@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Admin')->givePermissionTo(
            'menu_soporte',
            'menu_reportes',
            'menu_pedidos',
            'menu_colegios',
            'menu_colegios_editando',
            'menu_libros',
            'menu_pedidos_usuarios',
            'menu_referencia_bbva',
            'menu_pedidos_entregados',
            'menu_pedidos_enviados');

        User::create(['name' => 'mario', 'apellidos' => 'apellido', 'email' => 'mario@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente')->givePermissionTo(
            'menu_soporte',
            'menu_reportes',
            'menu_pedidos',
            'menu_colegios',
            'menu_colegios_editando',
            'menu_libros',
            'menu_pedidos_usuarios',
            'menu_referencia_bbva',
            'menu_pedidos_entregados',
            'menu_pedidos_enviados');
        User::create(['name' => 'german', 'apellidos' => 'apellido', 'email' => 'yetzin@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Admin');

        User::create(['name' => 'josue', 'apellidos' => 'apellido', 'email' => 'josue@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente');
        User::create(['name' => 'oswaldo', 'apellidos' => 'apellido', 'email' => 'oswaldo@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente')->givePermissionTo('DD-2222');
        User::create(['name' => 'Luz María', 'apellidos' => 'Pérez', 'email' => 'luz@inkwonders.com','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente')->givePermissionTo('CC-1111');

        User::create(['name' => 'Accept', 'apellidos' => 'Netpay', 'email' => 'accept@netpay.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente');
        User::create(['name' => 'Review', 'apellidos' => 'Netpay', 'email' => 'review@netpay.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente');
        User::create(['name' => 'Reject', 'apellidos' => 'Netpay', 'email' => 'reject@netpay.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Cliente');

        User::create(['name' => 'UTEQ2021', 'apellidos' => 'colegio', 'email' => 'uteq@colegio.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Colegio')->givePermissionTo('UTEQ2021');
        User::create(['name' => 'CEDROS2021', 'apellidos' => 'colegio', 'email' => 'cedros@colegio.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Colegio')->givePermissionTo('CEDROS2021');
        User::create(['name' => 'SALTILLO21', 'apellidos' => 'colegio', 'email' => 'saltillo@colegio.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Colegio')->givePermissionTo('SALTILLO21');
        User::create(['name' => 'TORREON21', 'apellidos' => 'colegio', 'email' => 'torreon@colegio.com.mx','email_verified_at'=>'2021-05-04 15:42:52', 'password' => Hash::make('inkwonders')])->assignRole('Colegio')->givePermissionTo('TORREON21');
        // DB::table('users')->insert([
        //     // ['name' => 'alain', 'apellidos' => 'apellido', 'email' => 'alain@inkwonders.com', 'password' => Hash::make('inkwonders')],
        //     // ['name' => 'manuel', 'apellidos' => 'apellido', 'email' => 'manuel@inkwonders.com', 'password' => Hash::make('inkwonders')],
        //     ['name' => 'josue', 'apellidos' => 'apellido', 'email' => 'josue@inkwonders.com', 'password' => Hash::make('inkwonders')],
        //     ['name' => 'oswaldo', 'apellidos' => 'apellido', 'email' => 'oswaldo@inkwonders.com', 'password' => Hash::make('inkwonders')],
        //     // ['name' => 'german', 'apellidos' => 'apellido', 'email' => 'yetzin@inkwonders.com', 'password' => Hash::make('inkwonders')],
        //     ['name' => 'luz', 'apellidos' => 'apellido', 'email' => 'luz@inkwonders.com', 'password' => Hash::make('inkwonders')],
        // ]);
    }
}
