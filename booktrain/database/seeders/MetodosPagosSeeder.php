<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetodosPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metodos_pagos')->insert([
            [
                'id'=>1,
                'codigo'=>'PUE',
                'descripcion'=>'Pago en una sola exhibición',
                //'orden'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>2,
                'codigo'=>'PPD',
                'descripcion'=>'Pago en parcialidades o diferido',
                //'orden'=>'2',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]

        ]);
    }
}
