<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatosFacturasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('datos_facturas')->insert([
            /*[
                'pedido_id'=>'1',
                'user_id'=>'1',
                'razon_social'=>'ALAIN LEMUS MUÑOZ',
                'rfc'=>'LEMA901006M31',
                'correo'=>'alain@inkwonders.com',
                'id_cfdi'=>'4',
                'cp' => '06880',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'pedido_id'=>'2',
                'user_id'=>'3',
                'razon_social'=>'JOSUE VARGAS MARTINEZ',
                'rfc'=>'VAMJ960824SQ8',
                'correo'=>'josue@inkwonder.com',
                'id_cfdi'=>'4',
                'cp' => '06870',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'pedido_id'=>'3',
                'user_id'=>'2',
                'razon_social'=>'Manuel apellido',
                'rfc'=>'VAMJ960824SQ8',
                'correo'=>'manuel@inkwonder.com',
                'id_cfdi'=>'4',
                'cp' => '06870',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'pedido_id'=>'4',
                'user_id'=>'4',
                'razon_social'=>'Oswaldo Ferral Mejia',
                'rfc'=>'VAMJ960824SQ8',
                'correo'=>'josue@inkwonder.com',
                'id_cfdi'=>'4',
                'cp' => '06870',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'pedido_id'=>'5',
                'user_id'=>'5',
                'razon_social'=>'German alvarez robles',
                'rfc'=>'VAMJ960824SQ8',
                'correo'=>'german@inkwonder.com',
                'id_cfdi'=>'4',
                'cp' => '06870',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'pedido_id'=>'7',
                'user_id'=>'7',
                'razon_social'=>'Luz maria anaya',
                'rfc'=>'VAMJ960824SQ8',
                'correo'=>'luz@inkwonder.com',
                'id_cfdi'=>'4',
                'cp' => '06870',
                'metodo_pago_id' => '1',
                'forma_pago_id'=>'1',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
