<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EnviosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('envios')->insert([
            [
                'pedido_id' => 1,
                'status' => 'ENVIADO',
                'fecha_recepcion'=> date('Y-m-d'),
                'fecha_estimada'=> date('Y-m-d'),
                'horarios_entrega'=>'11:00:00',
                'costo'=>'150',
                'guia'=>'A529D002',
                'paqueteria' => 'DHL',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]
            //, [
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'958',
            //     'guia'=>'Kr67D106',
            //     'paqueteria' => 'Fedex',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ], [
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'100',
            //     'guia'=>'YK37A1B0',
            //     'paqueteria' => 'Fedex',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ], [
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'150',
            //     'guia'=>'XM323A434',
            //     'paqueteria' => 'DHL',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ], [
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'200',
            //     'guia'=>'TS123A40B3',
            //     'paqueteria' => 'Estafeta',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ], [
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'550',
            //     'guia'=>'GSR23A30K0',
            //     'paqueteria' => 'DHL',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ],[
            //     'status' => 'ENVIADO',
            //     'fecha_recepcion'=> date('Y-m-d'),
            //     'fecha_estimada'=> date('Y-m-d'),
            //     'horarios_entrega'=>'19:00:00',
            //     'costo'=>'550',
            //     'guia'=>'GSR22A30K0',
            //     'paqueteria' => 'DHL',
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at'=> date('Y-m-d H:i:s')
            // ]
        ]);
    }
}
