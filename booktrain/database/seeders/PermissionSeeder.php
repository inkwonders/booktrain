<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("permissions")
            ->insert([
                ["name" => "Usuarios", "guard_name" => "web"],
                ["name" => "Pedidos", "guard_name" => "web"],
                ["name" => "Libros", "guard_name" => "web"],
                ["name" => "Colegios", "guard_name" => "web"],
                ["name" => "Envios", "guard_name" => "web"],
                ["name" => "Entregas", "guard_name" => "web"],
                ["name" => "Referencias BBVA", "guard_name" => "web"],
                ["name" => "Almacen", "guard_name" => "web"],
                ["name" => "Paquetería", "guard_name" => "web"],
                ["name" => "Reportes", "guard_name" => "web"],
                ["name" => "Impresión de pedidos", "guard_name" => "web"],
                ["name" => "Soporte", "guard_name" => "web"],
            ]);
    }
}