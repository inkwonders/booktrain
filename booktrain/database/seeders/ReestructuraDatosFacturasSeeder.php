<?php

namespace Database\Seeders;

use App\Models\DatosFacturas;
use Illuminate\Database\Seeder;

class ReestructuraDatosFacturasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DatosFacturas::get()->each(function($datos_factura) {
            $metodo_pago = null;

            switch ($datos_factura->metodo_pago_id) {
                case 1: $metodo_pago = 'PUE';break;
                case 2: $metodo_pago = 'PDD';break;
            }

            $forma_pago = null;

            switch ($datos_factura->forma_pago_id) {
                case 1: $forma_pago = '01-EFECTIVO';break;
                case 2: $forma_pago = '04-TARJETA CREDITO';break;
                case 3: $forma_pago = '28-TARJETA DEBITO';break;
            }

            $datos_factura->update([
                'metodo_pago' => $metodo_pago,
                'forma_pago' => $forma_pago
            ]);
        });
    }
}