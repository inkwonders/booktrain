<?php

namespace Database\Seeders;

use App\Models\Colegio;
use App\Models\Configuracion;
use Illuminate\Database\Seeder;
use DB;

class CargaConfiguracionesColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/**
 * {"nombre_cuenta":"PROVESA EDICIONES S.A. DE C.V",
 * "nombre_banco":"BBVA BANCOMER",
 * "numero_cuenta":"123 456 7890",
 * "clabe":"1234 5678 1234 567890",
 * "ventanilla_otros_bancos":"CONTRATO CIE 001643290",
 * "portal_bbva":"CONTRATO CIE 001643290",
 * "portal_otros_bancos":"CLABE CIE 012914002016432906",
 * "numero_contrato":"001813587",
 * "numero_contrato_clabe":"012914002018135874",
 * "costo_envio":150,"metodos_pago":[{"nombre":"Tarjeta de d\u00e9bito",
 * "valor":"DEBITO",
 * "comision":0,"formas_pago":[{"nombre":"Pago en una sola exhibicion",
 * "valor":"PUE",
 * "activo":1}]},{"nombre":"Tarjeta de cr\u00e9dito",
 * "valor":"CREDITO",
 * "comision":3.5,"formas_pago":[{"nombre":"Pago en una sola exhibici\u00f3n",
 * "valor":"PUE",
 * "activo":1},{"nombre":"Pago diferido a 3 meses",
 * "valor":"DIFERIDO3M",
 * "meses":3,"comision":4.66,"minimo":300,"activo":1},{"nombre":"Pago diferido a 6 meses",
 * "valor":"DIFERIDO6M",
 * "meses":6,"comision":7.51,"minimo":600,"activo":1},{"nombre":"Pago diferido a 9 meses",
 * "valor":"DIFERIDO9M",
 * "meses":9,"comision":10.86,"minimo":900,"activo":1}]},{"nombre":"American Express",
 * "valor":"AMEX",
 * "comision":6,"formas_pago":[{"nombre":"Pago en una sola exhibici\u00f3n",
 * "valor":"PUE",
 * "activo":1},{"nombre":"Pago diferido a 3 meses",
 * "valor":"DIFERIDO3M",
 * "meses":3,"comision":6.05,"minimo":300,"activo":1},{"nombre":"Pago diferido a 6 meses",
 * "valor":"DIFERIDO6M",
 * "meses":6,"comision":8.5,"minimo":600,"activo":1},{"nombre":"Pago diferido a 9 meses",
 * "valor":"DIFERIDO9M",
 * "meses":9,"comision":10.8,"minimo":900,"activo":1}]},{"nombre":"Referencia interbancaria",
 * "valor":"BANCO",
 * "comision":0,"formas_pago":[{"nombre":"Pago en una sola exhibici\u00f3n",
 * "valor":"PUE",
 * "activo":1}]},{"nombre":"Tienda de conveniencia",
 * "valor":"TIENDA",
 * "comision":0,"formas_pago":[{"nombre":"Pago en una sola exhibici\u00f3n",
 * "valor":"PUE",
 * "activo":1}]}]}
 */
        // Configuracion::where('id', '>', 3)->delete();
        DB::statement('DELETE FROM configuraciones WHERE ID > 3');
        DB::statement('ALTER TABLE configuraciones AUTO_INCREMENT = 4');

        // Creamos las configuraciones padre
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'nombre_cuenta', 'valor' => 'PROVESA EDICIONES S.A. DE C.V']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'nombre_banco', 'valor' => 'BBVA BANCOMER']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'numero_cuenta', 'valor' => '123 456 7890']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'clabe', 'valor' => '1234 5678 1234 567890']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'ventanilla_otros_bancos', 'valor' => 'CONTRATO CIE 001643290']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'portal_bbva', 'valor' => 'CONTRATO CIE 001643290']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'portal_otros_bancos', 'valor' => 'CLABE CIE 012914002016432906']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'numero_contrato', 'valor' => '001813587']);
        Configuracion::insert(['categoria' => 'CUENTAS', 'etiqueta' => 'numero_contrato_clabe', 'valor' => '012914002018135874']);
        Configuracion::insert(['categoria' => 'ENVIOS', 'etiqueta' => 'costo_envio', 'valor' => '150']);
        Configuracion::insert(['categoria' => 'ENVIOS', 'etiqueta' => 'envio_domicilio_disponible', 'valor' => '1']);


        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_debito', 'valor' => 'Tarjeta de débito']);
        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_debito_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_nombre', 'valor' => 'Pago en una sola exhibicion']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_valor', 'valor' => 'PUE']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_meses', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_comision', 'valor' => '3']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_minimo', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_nombre', 'valor' => 'Pago diferido a 3 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_nombre', 'valor' => 'Pago diferido a 6 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_nombre', 'valor' => 'Pago diferido a 9 meses']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_valor', 'valor' => 'DIFERIDO3M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_valor', 'valor' => 'DIFERIDO6M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_valor', 'valor' => 'DIFERIDO9M']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_minimo', 'valor' => '4.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_minimo', 'valor' => '5.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_minimo', 'valor' => '6.5']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_comision', 'valor' => '300']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_comision', 'valor' => '600']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_comision', 'valor' => '900']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_meses', 'valor' => 3]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_meses', 'valor' => 6]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_meses', 'valor' => 9]);


        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_credito', 'valor' => 'Tarjeta de crédito']);
        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_credito_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_nombre', 'valor' => 'Pago en una sola exhibicion']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_valor', 'valor' => 'PUE']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_meses', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_comision', 'valor' => '3']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_minimo', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_nombre', 'valor' => 'Pago diferido a 3 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_nombre', 'valor' => 'Pago diferido a 6 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_nombre', 'valor' => 'Pago diferido a 9 meses']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_valor', 'valor' => 'DIFERIDO3M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_valor', 'valor' => 'DIFERIDO6M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_valor', 'valor' => 'DIFERIDO9M']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_minimo', 'valor' => '300']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_minimo', 'valor' => '600']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_minimo', 'valor' => '900']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_comision', 'valor' => '4.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_comision', 'valor' => '5.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_comision', 'valor' => '6.5']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_meses', 'valor' => 3]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_meses', 'valor' => 6]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_meses', 'valor' => 9]);

        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_amex', 'valor' => 'American Express']);
        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_amex_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_nombre', 'valor' => 'Pago en una sola exhibicion']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_valor', 'valor' => 'PUE']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_meses', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_comision', 'valor' => '3']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_minimo', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_nombre', 'valor' => 'Pago diferido a 3 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_nombre', 'valor' => 'Pago diferido a 6 meses']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_nombre', 'valor' => 'Pago diferido a 9 meses']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_valor', 'valor' => 'DIFERIDO3M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_valor', 'valor' => 'DIFERIDO6M']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_valor', 'valor' => 'DIFERIDO9M']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_activo', 'valor' => true]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_minimo', 'valor' => '300']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_minimo', 'valor' => '600']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_minimo', 'valor' => '900']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_comision', 'valor' => '4.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_comision', 'valor' => '5.5']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_comision', 'valor' => '6.5']);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_3m_meses', 'valor' => 3]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_6m_meses', 'valor' => 6]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_diferido_9m_meses', 'valor' => 9]);


        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_banco', 'valor' => 'Referencia interbancaria']);
        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_banco_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_nombre', 'valor' => 'Pago en una sola exhibicion']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_valor', 'valor' => 'PUE']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_meses', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_comision', 'valor' => '3']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_minimo', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_activo', 'valor' => true]);


        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_tienda', 'valor' => 'Tienda de conveniencia']);
        // $config = Configuracion::create(['categoria' => 'PAGOS', 'etiqueta' => 'metodo_pago_tienda_activo', 'valor' => true]);

        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_nombre', 'valor' => 'Pago en una sola exhibicion']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_valor', 'valor' => 'PUE']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_meses', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_comision', 'valor' => '3']);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_minimo', 'valor' => 0]);
        // $config->configuraciones()->create(['categoria' => 'PAGOS', 'etiqueta' => 'forma_pago_una_exhibicion_activo', 'valor' => true]);

        Colegio::all()->each(function($colegio) {
            $json_configuracion_colegio = json_decode($colegio->configuracion);

            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'nombre_cuenta','valor' => isset($json_configuracion_colegio->nombre_cuenta) ? $json_configuracion_colegio->nombre_cuenta : Configuracion::where('etiqueta', 'nombre_cuenta')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'nombre_banco','valor' => isset($json_configuracion_colegio->nombre_banco) ? $json_configuracion_colegio->nombre_banco : Configuracion::where('etiqueta', 'nombre_banco')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_cuenta','valor' => isset($json_configuracion_colegio->numero_cuenta) ? $json_configuracion_colegio->numero_cuenta : Configuracion::where('etiqueta', 'numero_cuenta')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'clabe','valor' => isset($json_configuracion_colegio->clabe) ? $json_configuracion_colegio->clabe : Configuracion::where('etiqueta', 'clabe')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'ventanilla_otros_bancos','valor' => isset($json_configuracion_colegio->ventanilla_otros_bancos) ? $json_configuracion_colegio->ventanilla_otros_bancos : Configuracion::where('etiqueta', 'ventanilla_otros_bancos')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'portal_bbva','valor' => isset($json_configuracion_colegio->portal_bbva) ? $json_configuracion_colegio->portal_bbva : Configuracion::where('etiqueta', 'portal_bbva')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'portal_otros_bancos','valor' => isset($json_configuracion_colegio->portal_otros_bancos) ? $json_configuracion_colegio->portal_otros_bancos : Configuracion::where('etiqueta', 'portal_otros_bancos')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_contrato','valor' => isset($json_configuracion_colegio->numero_contrato) ? $json_configuracion_colegio->numero_contrato : Configuracion::where('etiqueta', 'numero_contrato')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'CUENTAS','etiqueta' => 'numero_contrato_clabe','valor' => isset($json_configuracion_colegio->numero_contrato_clabe) ? $json_configuracion_colegio->numero_contrato_clabe : Configuracion::where('etiqueta', 'numero_contrato_clabe')->base()->first()->valor ]);
            $colegio->configuraciones()->create(['categoria' => 'ENVIOS','etiqueta' => 'costo_envio','valor' => isset($json_configuracion_colegio->costo_envio) ? $json_configuracion_colegio->costo_envio : Configuracion::where('etiqueta', 'costo_envio')->base()->first()->valor ]);

            $envio_disponible = isset($json_configuracion_colegio->costo_envio) && $json_configuracion_colegio->costo_envio > 0;

            $colegio->configuraciones()->create(['categoria' => 'ENVIOS','etiqueta' => 'envio_domicilio_disponible','valor' => $envio_disponible]);

            // if($colegio->configuracion == '') {
            //     $metodos_pago = json_decode(Configuracion::where('etiqueta', 'json_base_pago_colegio')->first()->valor)->metodos_pago;
            // }else{
            //     $metodos_pago = collect($json_configuracion_colegio->metodos_pago)->keyBy('nombre');
            // }

            // // Si no existen las configuraciones en el json del colegio, las insertamos desactivadas

            // // ---------------------------------------------- METODO PAGO DEBITO --------------------------------------------------
            // $config_metodo_pago_debito = $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_debito', 'valor' => 'Tarjeta de débito']);

            // $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_debito_activo', 'valor' => false]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_activo','valor' => false ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_nombre')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_valor')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_meses')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_comision')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_minimo')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_debito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_activo','valor' => false ]);

            // if( isset($metodos_pago['Tarjeta de débito'])){
            //     // La configuración existe, obtenemos la correspondiente al colegio
            //     $colegio->configuraciones()->where('etiqueta', 'metodo_pago_debito_activo')->update(['valor' => 1]);

            //     $formas_pago = collect( $metodos_pago['Tarjeta de débito']->formas_pago )->keyBy('valor');

            //     if($formas_pago->has('PUE')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_una_exhibicion_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_una_exhibicion_comision')->update(['valor' => isset($formas_pago['PUE']->comision) ? $formas_pago['PUE']->comision : 0 ]);
            //     }

            //     if($formas_pago->has('DIFERIDO3M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_3m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_3m_comision')->update(['valor' => $formas_pago['DIFERIDO3M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_3m_minimo')->update(['valor' => $formas_pago['DIFERIDO3M']->minimo ]);
            //     }

            //     if($formas_pago->has('DIFERIDO6M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_6m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_6m_comision')->update(['valor' => $formas_pago['DIFERIDO6M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_6m_minimo')->update(['valor' => $formas_pago['DIFERIDO6M']->minimo ]);
            //     }

            //     if($formas_pago->has('DIFERIDO9M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_9m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_9m_comision')->update(['valor' => $formas_pago['DIFERIDO9M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_debito->id)->where('etiqueta', 'forma_pago_diferido_9m_minimo')->update(['valor' => $formas_pago['DIFERIDO9M']->minimo ]);
            //     }
            // }

            // // ---------------------------------------------- METODO PAGO CRÉDITO --------------------------------------------------
            // $config_metodo_pago_credito = $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_credito', 'valor' => 'Tarjeta de crédito']);
            // $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_credito_activo', 'valor' => false]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_activo','valor' => false ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_nombre')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_valor')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_meses')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_minimo')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_comision')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_credito->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_activo','valor' => false ]);

            // if(isset($metodos_pago['Tarjeta de crédito'])){
            //     $colegio->configuraciones()->where('etiqueta', 'metodo_pago_credito_activo')->update(['valor' => 1]);

            //     $formas_pago = collect( $metodos_pago['Tarjeta de crédito']->formas_pago )->keyBy('valor');

            //     if($formas_pago->has('PUE')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_una_exhibicion_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_una_exhibicion_comision')->update(['valor' => isset($formas_pago['PUE']->comision) ? $formas_pago['PUE']->comision : 0 ]);
            //     }

            //     if($formas_pago->has('DIFERIDO3M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_3m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_3m_comision')->update(['valor' => isset($formas_pago['DIFERIDO3M']->comision) ? $formas_pago['DIFERIDO3M']->comision : 0 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_3m_minimo')->update(['valor' => isset($formas_pago['DIFERIDO3M']->minimo) ? $formas_pago['DIFERIDO3M']->minimo : 0 ]);
            //     }

            //     if($formas_pago->has('DIFERIDO6M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_6m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_6m_comision')->update(['valor' => isset($formas_pago['DIFERIDO6M']->comision) ? $formas_pago['DIFERIDO6M']->comision : 0 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_6m_minimo')->update(['valor' => isset($formas_pago['DIFERIDO6M']->minimo) ? $formas_pago['DIFERIDO6M']->minimo : 0 ]);
            //     }

            //     if($formas_pago->has('DIFERIDO9M')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_9m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_9m_comision')->update(['valor' => isset($formas_pago['DIFERIDO9M']->comision) ? $formas_pago['DIFERIDO9M']->comision : 0 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_credito->id)->where('etiqueta', 'forma_pago_diferido_9m_minimo')->update(['valor' => isset($formas_pago['DIFERIDO9M']->minimo) ? $formas_pago['DIFERIDO9M']->minimo : 0 ]);
            //     }
            // }

            // // ---------------------------------------------- METODO PAGO AMEX --------------------------------------------------
            // $config_metodo_pago_amex = $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_amex', 'valor' => 'American Express']);
            // $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_amex_activo', 'valor' => false]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_activo','valor' => false ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_nombre')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_valor')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_meses')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_minimo')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_3m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_6m_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_diferido_9m_comision')->base()->first()->valor ]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_3m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_6m_activo','valor' => false ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_amex->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_diferido_9m_activo','valor' => false ]);


            // if( isset($metodos_pago['American Express'])){
            //     $colegio->configuraciones()->where('etiqueta', 'metodo_pago_amex_activo')->update(['valor' => 1]);

            //     $formas_pago = collect( $metodos_pago['American Express']->formas_pago )->keyBy('valor');

            //     if($formas_pago->has('PUE')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_una_exhibicion_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_una_exhibicion_comision')->update(['valor' => isset($formas_pago['PUE']->comision) ? $formas_pago['PUE']->comision : 0 ]);
            //     }

            //     if($formas_pago->has('DIFERIDO3M')){
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_3m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_3m_comision')->update(['valor' => $formas_pago['DIFERIDO3M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_3m_minimo')->update(['valor' => $formas_pago['DIFERIDO3M']->minimo ]);
            //     }

            //     if($formas_pago->has('DIFERIDO6M')){
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_6m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_6m_comision')->update(['valor' => $formas_pago['DIFERIDO6M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_6m_minimo')->update(['valor' => $formas_pago['DIFERIDO6M']->minimo ]);
            //     }

            //     if($formas_pago->has('DIFERIDO9M')){
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_9m_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_9m_comision')->update(['valor' => $formas_pago['DIFERIDO9M']->comision ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_amex->id)->where('etiqueta', 'forma_pago_diferido_9m_minimo')->update(['valor' => $formas_pago['DIFERIDO9M']->minimo ]);
            //     }
            // }

            // // ---------------------------------------------- METODO PAGO REFERENCIA BANCARIA --------------------------------------------------
            // $config_metodo_pago_banco = $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_banco', 'valor' => 'Referencia interbancaria']);
            // $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_banco_activo', 'valor' => false]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_banco->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_activo','valor' => false ]);

            // if( isset($metodos_pago['Referencia interbancaria'])){
            //     $colegio->configuraciones()->where('etiqueta', 'metodo_pago_banco_activo')->update(['valor' => 1]);

            //     $formas_pago = collect( $metodos_pago['Referencia interbancaria']->formas_pago )->keyBy('valor');

            //     if($formas_pago->has('PUE')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_banco->id)->where('etiqueta', 'forma_pago_una_exhibicion_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_banco->id)->where('etiqueta', 'forma_pago_una_exhibicion_comision')->update(['valor' => isset($formas_pago['PUE']->comision) ? $formas_pago['PUE']->comision : 0 ]);
            //     }
            // }

            // // ---------------------------------------------- METODO PAGO REFERENCIA TIENDA --------------------------------------------------
            // $config_metodo_pago_tienda = $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_tienda', 'valor' => 'Tienda de conveniencia']);
            // $colegio->configuraciones()->create([ 'categoria' => 'CUENTAS', 'etiqueta' => 'metodo_pago_tienda_activo', 'valor' => false]);

            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_nombre','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_nombre')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_valor','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_valor')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_meses','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_meses')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_comision','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_comision')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_minimo','valor' => Configuracion::where('etiqueta', 'forma_pago_una_exhibicion_minimo')->base()->first()->valor ]);
            // $colegio->configuraciones()->create(['configuracion_padre_id' => $config_metodo_pago_tienda->id, 'categoria' => 'CUENTAS','etiqueta' => 'forma_pago_una_exhibicion_activo','valor' => false ]);

            // if( isset($metodos_pago['Tienda de conveniencia'])){
            //     $colegio->configuraciones()->where('etiqueta', 'metodo_pago_tienda_activo')->update(['valor' => 1]);

            //     $formas_pago = collect( $metodos_pago['Tienda de conveniencia']->formas_pago )->keyBy('valor');

            //     if($formas_pago->has('PUE')) {
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_tienda->id)->where('etiqueta', 'forma_pago_una_exhibicion_activo')->update(['valor' => 1 ]);
            //         $colegio->configuraciones()->where('configuracion_padre_id', $config_metodo_pago_tienda->id)->where('etiqueta', 'forma_pago_una_exhibicion_comision')->update(['valor' => isset($formas_pago['PUE']->comision) ? $formas_pago['PUE']->comision : 0 ]);
            //     }
            // }
        });
    }
}
