<?php

namespace Database\Seeders;

use App\Models\Colegio;
use Illuminate\Database\Seeder;
use App\Models\MetodoPago;
use App\Models\FormaPago;

class CreaMetodosPagoBbvaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ALTER TABLE `pedidos` CHANGE `metodo_pago` `metodo_pago` ENUM('CREDITO','DEBITO','AMEX','BANCO','TIENDA','EFECTIVO','TERMINAL','TRANSFERENCIA','BBVADEBITOVPOS','BBVACREDITOVPOS','BBVATARJETA') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
        $metodo_1 = MetodoPago::create(['codigo' => 'BBVADEBITOVPOS',  'descripcion' => 'Pago con tarjeta de débito', 'proveedor' => 'BBVA', 'disponible' => 1]);                // ID $metodo_1->id
        $metodo_2 = MetodoPago::create(['codigo' => 'BBVACREDITOVPOS',    'descripcion' => 'Pago con tarjeta de crédito', 'proveedor' => 'BBVA', 'disponible' => 1]);            // ID $metodo_2->id
        $metodo_3 = MetodoPago::create(['codigo' => 'BBVATARJETA',       'descripcion' => 'Pago con tarjeta autogestionado BBVA', 'proveedor' => 'BBVA', 'disponible' => 0]);    // ID $metodo_3->id

        $forma_pago_puntos = FormaPago::firstOrCreate(['codigo' => 'PUNTOS', 'descripcion' => 'Pago con puntos']); // ID $forma_pago_puntos->id

        Colegio::get()->each(function($colegio) use ($metodo_1, $metodo_2, $metodo_3, $forma_pago_puntos) {
            $colegio->metodosPago()->attach($metodo_1->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_2->id, ['contexto' => 'ONLINE', 'activo' => 1]);
            $colegio->metodosPago()->attach($metodo_3->id, ['contexto' => 'ONLINE', 'activo' => 1]);

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_1->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUE
            $colegio->formasPago()->attach($forma_pago_puntos->id, ['metodo_pago_id' => $metodo_1->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUNTOS

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_2->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUE
            $colegio->formasPago()->attach(2, ['metodo_pago_id' => $metodo_2->id, 'meses' => 3, 'comision' => 0, 'minimo' => 300, 'activo' => 0]); // DIFERIDO3M
            $colegio->formasPago()->attach(3, ['metodo_pago_id' => $metodo_2->id, 'meses' => 6, 'comision' => 0, 'minimo' => 600, 'activo' => 0]); // DIFERIDO6M
            $colegio->formasPago()->attach(4, ['metodo_pago_id' => $metodo_2->id, 'meses' => 9, 'comision' => 0, 'minimo' => 900, 'activo' => 0]); // DIFERIDO9M
            $colegio->formasPago()->attach($forma_pago_puntos->id, ['metodo_pago_id' => $metodo_2->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUNTOS

            $colegio->formasPago()->attach(1, ['metodo_pago_id' => $metodo_3->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUE
            $colegio->formasPago()->attach(2, ['metodo_pago_id' => $metodo_3->id, 'meses' => 3, 'comision' => 0, 'minimo' => 300, 'activo' => 0]); // DIFERIDO3M
            $colegio->formasPago()->attach(3, ['metodo_pago_id' => $metodo_3->id, 'meses' => 6, 'comision' => 0, 'minimo' => 600, 'activo' => 0]); // DIFERIDO6M
            $colegio->formasPago()->attach(4, ['metodo_pago_id' => $metodo_3->id, 'meses' => 9, 'comision' => 0, 'minimo' => 900, 'activo' => 0]); // DIFERIDO9M
            $colegio->formasPago()->attach($forma_pago_puntos->id, ['metodo_pago_id' => $metodo_3->id, 'meses' => 0, 'comision' => 0, 'minimo' => 0, 'activo' => 0]); // PUNTOS
        });
    }
}
