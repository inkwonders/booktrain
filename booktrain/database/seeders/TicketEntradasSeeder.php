<?php

namespace Database\Seeders;
use App\Models\TicketEntrada;
use Illuminate\Database\Seeder;

class TicketEntradasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TicketEntrada::insert([



            ['tipo_entrada' => '1','comentario'=>'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum','ticket_usuario_id'=>'1','usuario_id'=>'1' ,'created_at'=>date('Y-m-d H:i:s')],
            ['tipo_entrada' => '2','comentario'=>'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum','ticket_usuario_id'=>'2','usuario_id'=>'2' ,'created_at'=>date('Y-m-d H:i:s')],
            ['tipo_entrada' => '3','comentario'=>'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum','ticket_usuario_id'=>'3','usuario_id'=>'3' ,'created_at'=>date('Y-m-d H:i:s')],
            ['tipo_entrada' => '4','comentario'=>'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum','ticket_usuario_id'=>'4','usuario_id'=>'4' ,'created_at'=>date('Y-m-d H:i:s')],
            ['tipo_entrada' => '5','comentario'=>'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum','ticket_usuario_id'=>'5','usuario_id'=>'5' ,'created_at'=>date('Y-m-d H:i:s')],



             ]);
    }
}
