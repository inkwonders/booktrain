<?php

namespace Database\Seeders;

use App\Models\TicketUsuario;
use Illuminate\Database\Seeder;

class TicketUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        TicketUsuario::insert([



            ['folio' => '1', 'status' => '1', 'titulo' => 'Servicio a Cliente', 'descripcion' => 'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum', 'calificacion' => '10', 'comentario_calificacion' => 'Comentario calificacion Lorem Ipsun Lorem parse cupri a unt mirachel', 'fecha_calificacion' => '2021-04-12', 'telefono' => '4421234567', 'usuario_id' => '1', 'ticket_motivos_id' => '1', 'created_at' => date('Y-m-d H:i:s')],
            ['folio' => '2', 'status' => '1', 'titulo' => 'Error en pedido', 'descripcion' => 'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum', 'calificacion' => '10', 'comentario_calificacion' => 'Comentario calificacion Lorem Ipsun Lorem parse cupri a unt mirachel', 'fecha_calificacion' => '2021-04-12', 'telefono' => '4421234567', 'usuario_id' => '2', 'ticket_motivos_id' => '2', 'created_at' => date('Y-m-d H:i:s')],
            ['folio' => '3', 'status' => '1', 'titulo' => 'Pago', 'descripcion' => 'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum', 'calificacion' => '10', 'comentario_calificacion' => 'Comentario calificacion Lorem Ipsun Lorem parse cupri a unt mirachel', 'fecha_calificacion' => '2021-04-12', 'telefono' => '4421234567', 'usuario_id' => '3', 'ticket_motivos_id' => '3', 'created_at' => date('Y-m-d H:i:s')],
            ['folio' => '4', 'status' => '1', 'titulo' => 'Facturación', 'descripcion' => 'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum', 'calificacion' => '10', 'comentario_calificacion' => 'Comentario calificacion Lorem Ipsun Lorem parse cupri a unt mirachel', 'fecha_calificacion' => '2021-04-12', 'telefono' => '4421234567', 'usuario_id' => '4', 'ticket_motivos_id' => '4', 'created_at' => date('Y-m-d H:i:s')],
            ['folio' => '5', 'status' => '1', 'titulo' => 'Entregas', 'descripcion' => 'Descripcion de prueba LoremIpsun aunt mirage loret marche cas to upsideum', 'calificacion' => '10', 'comentario_calificacion' => 'Comentario calificacion Lorem Ipsun Lorem parse cupri a unt mirachel', 'fecha_calificacion' => '2021-04-12', 'telefono' => '4421234567', 'usuario_id' => '5', 'ticket_motivos_id' => '5', 'created_at' => date('Y-m-d H:i:s')],



        ]);
    }
}
// Servicio a Cliente, Error en pedido, Pago, Facturación, Entregas, Cancelación, Comprobante de compra, Registro y mi Perfil, Sugerencias