<?php

namespace Database\Seeders;

use App\Models\Envio;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargaRelacionesMorphEnvios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        DB::table('envios')->get()->each(function($envio)  {

            try {
                DB::table('envios')->where('id', $envio->id)->update([
                    'enviable_id' => $envio->pedido_id,
                    'enviable_type' => 'App\Models\Pedido'
                ]);
            } catch (\Throwable $th) {
                DB::rollback();
                dd($th);
            }
        });

        DB::commit();

    }
}
