<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PedidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pedidos')->insert([
            [
                'id'=>'1',
                'serie'=>'PR',
                'folio'=>'1001',
                'status'=>'PROCESADO',
                'nombre_contacto'=>'Luz Maria',
                'apellidos_contacto'=>'Perez Lopez',
                'celular_contacto'=>' 4422244420',
                'tipo_entrega'=>'1',
                'factura' => '0',
                'terminos_condiciones' => '1',
                'subtotal'=>'200',
                'comision'=>'0',
                'total'=>'200.00',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s'),
                'usuario_id'=>'7',
                'colegio_id'=>'1',
                'direccion_id' => '1',
                'metodo_pago'=>'BANCO',
                'forma_pago'=>'PUE'
            ]

        ]);
    }
}
