<?php

namespace Database\Seeders;

use App\Models\Libro;
use Illuminate\Database\Seeder;
use DB;

class ResetEditorialLibrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('libros')->update(['editorial' => '']);



    }
}
