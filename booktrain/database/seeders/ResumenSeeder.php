<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResumenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resumen_pedidos')->insert([
            [
                'id'=>'1',
                'pedido_id'=>'1',
                'nombre_alumno'=>'Luzma JR',
                'paterno_alumno'=>'Perez',
                'materno_alumno'=>'Lopez',
                'paquete_id'=>'1',
                'subtotal'=>'200',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]/*, [
                'pedido_id'=>'2',
                'nombre_alumno'=>'Alain',
                'paterno_alumno'=>'Lemus',
                'materno_alumno'=>'Muñoz',
                'paquete_id'=>'2',
                'subtotal'=>'350',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'pedido_id'=>'3',
                'nombre_alumno'=>'Luz',
                'paterno_alumno'=>'Maria',
                'materno_alumno'=>'Hernandez',
                'paquete_id'=>'3',
                'subtotal'=>'250',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'pedido_id'=>'5',
                'nombre_alumno'=>'Oswaldo',
                'paterno_alumno'=>'Ferral',
                'materno_alumno'=>'Martinez',
                'paquete_id'=>'5',
                'subtotal'=>'800',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'pedido_id'=>'6',
                'nombre_alumno'=>'Manuel',
                'paterno_alumno'=>'Mejia',
                'materno_alumno'=>'Ferral',
                'paquete_id'=>'8',
                'subtotal'=>'800',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'pedido_id'=>'4',
                'nombre_alumno'=>'Pedro',
                'paterno_alumno'=>'Morales',
                'materno_alumno'=>'Hernandez',
                'paquete_id'=>'2',
                'subtotal'=>'500',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'pedido_id'=>'7',
                'nombre_alumno'=>'Pedro',
                'paterno_alumno'=>'Morales',
                'materno_alumno'=>'Hernandez',
                'paquete_id'=>'2',
                'subtotal'=>'500',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
