<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Configuracion;

class ConfiguracionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 'PUE','DIFERIDO3M','DIFERIDO6M','DIFERIDO9M'
     * 'CREDITO','DEBITO','AMEX','BANCO','TIENDA'
     *
     * @return void
     */
    public function run()
    {
        Configuracion::insert([
            [
                'id' => 1,
                'etiqueta'=>'serie',
                'valor'=>'PR',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'etiqueta'=>'folio',
                'valor'=>'2000',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'etiqueta'  => 'json_base_pago_colegio',
                'valor'     => json_encode((Object) [
                    "nombre_cuenta" => "PROVESA EDICIONES S.A. DE C.V",
                    "nombre_banco" => "BBVA BANCOMER",
                    "numero_cuenta" => "123 456 7890",
                    "clabe" => "1234 5678 1234 567890",
                    "ventanilla_otros_bancos" => "CONTRATO CIE 001643290",
                    "portal_bbva" => "CONTRATO CIE 001643290",
                    "portal_otros_bancos" => "CLABE CIE 012914002016432906",
                    "numero_contrato"=>"001813587",
                    "numero_contrato_clabe"=>"012914002018135874",
                    "costo_envio" => 150,
                    'metodos_pago' => [
                        [
                            'nombre' => 'Tarjeta de débito',
                            'valor' => 'DEBITO',
                            'formas_pago' => [
                                ['nombre' => 'Pago en una sola exhibición', 'valor' => 'PUE', 'meses' => 0, 'comision' => 1, 'minimo' => 0],
                            ]],
                        [
                            'nombre' => 'Tarjeta de crédito',
                            'valor' => 'CREDITO',
                            'formas_pago' => [
                                ['nombre' => 'Pago en una sola exhibición', 'valor' => 'PUE', 'meses' => 0, 'comision' => 2, 'minimo' => 0],
                                ['nombre' => 'Pago diferido a 3 meses', 'valor' => 'DIFERIDO3M', 'meses' => 3, 'comision' => 3, 'minimo' => 300],
                                ['nombre' => 'Pago diferido a 6 meses', 'valor' => 'DIFERIDO6M', 'meses' => 6, 'comision' => 4, 'minimo' => 600],
                                ['nombre' => 'Pago diferido a 9 meses', 'valor' => 'DIFERIDO9M', 'meses' => 9, 'comision' => 5, 'minimo' => 900],
                            ]],
                        [
                            'nombre' => 'American Express',
                            'valor' => 'AMEX',
                            'formas_pago' => [
                                ['nombre' => 'Pago en una sola exhibición', 'valor' => 'PUE', 'meses' => 0, 'comision' => 6, 'minimo' => 0],
                                ['nombre' => 'Pago diferido a 3 meses', 'valor' => 'DIFERIDO3M', 'meses' => 3, 'comision' => 7, 'minimo' => 300],
                                ['nombre' => 'Pago diferido a 6 meses', 'valor' => 'DIFERIDO6M', 'meses' => 6, 'comision' => 8, 'minimo' => 600],
                                ['nombre' => 'Pago diferido a 9 meses', 'valor' => 'DIFERIDO9M', 'meses' => 9, 'comision' => 9, 'minimo' => 900],
                            ]],
                        [
                            'nombre' => 'Referencia interbancaria',
                            'valor' => 'BANCO',
                            'comision' => 0,
                            'formas_pago' => [
                                ['nombre' => 'Pago en una sola exhibición', 'valor' => 'PUE' ]
                            ]],
                        [
                            'nombre' => 'Tienda de conveniencia',
                            'valor' => 'TIENDA',
                            'comision' => 0,
                            'formas_pago' => [
                                ['nombre' => 'Pago en una sola exhibición', 'valor' => 'PUE' ]
                            ]]
                    ]
                ]),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}