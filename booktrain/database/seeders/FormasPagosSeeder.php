<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormasPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formas_pagos')->insert([
                [
                    'id'=>1,
                    'codigo'=>'01',
                    'descripcion'=>'Efectivo',
                    //'orden'=>'1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],[
                    'id'=>2,
                    'codigo'=>'28',
                    'descripcion'=>'Tarjeta de débito',
                    //'orden'=>'2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ], [
                    'id'=>3,
                    'codigo'=>'04',
                    'descripcion'=>'Tarjeta de crédito',
                    //'orden'=>'3',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
        ]);
    }
}
