<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LibrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('libros')->insert([
            [
                'id'=>'1',
                'nombre'=>'K1 Diccionario 1 Básico Escolar',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'AAAA1111',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'2',
                'nombre'=>'K1 Léeme SB + WB Pk 1',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'BBBB2222',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'3',
                'nombre'=>'K1 Español 1 ME 2011',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'CCCC3333',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'4',
                'nombre'=>'K2 Diccionario 2 Básico Escolar',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'AAAA1111',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'5',
                'nombre'=>'K2 Léeme SB + WB Pk 2',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'BBBB2222',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'6',
                'nombre'=>'K2 Español 2 ME 2011',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'CCCC3333',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'7',
                'nombre'=>'K3 Diccionario 3 Básico Escolar',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'AAAA1111',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'8',
                'nombre'=>'K3 Léeme SB + WB Pk 3',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'BBBB2222',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'9',
                'nombre'=>'K3 Español 3 ME 2011',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'CCCC3333',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'10',
                'nombre'=>'Sec1 Totem Livre de l´élève + DVD-ROM + Man',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'GGGG7777',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'11',
                'nombre'=>'Sec1 Infinita Química Sec SB/WB/RD PACK',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'HHHH8888',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'12',
                'nombre'=>'Sec1 Los relámpagos de agosto',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'IIII9999',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'13',
                'nombre'=>'Sec2 Totem Livre de l´élève + DVD-ROM + Man',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'GGGG7777',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'14',
                'nombre'=>'Sec2 Infinita Química Sec SB/WB/RD PACK',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'HHHH8888',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'15',
                'nombre'=>'Sec2 Los relámpagos de agosto',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'IIII9999',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],[
                'id'=>'16',
                'nombre'=>'Sec3 Totem Livre de l´élève + DVD-ROM + Man',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'GGGG7777',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'17',
                'nombre'=>'Sec3 Infinita Química Sec SB/WB/RD PACK',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'HHHH8888',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'18',
                'nombre'=>'Sec3 Los relámpagos de agosto',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'IIII9999',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'19',
                'nombre'=>'Pri1 Matemáticas 1 (Conecta Secundaria) ME 20',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'DDDD4444',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'20',
                'nombre'=>' Pri1 CAMBRIDGE 1 PRIMARY PATH AMERICAN ENGLISH',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'EEEE5555',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'21',
                'nombre'=>'Pri1 Cambridge 1 Primary Path American English',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'FFFF6666',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'22',
                'nombre'=>'Pre MEPRO MATEMÁTICAS 2',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'JJJJ0000',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'23',
                'nombre'=>'Pre DIBUJO, TRAZO Y APRENDO 1',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'KKKK1111',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'24',
                'nombre'=>'Pre SOLUCIÓN MEPRO INTERACCIONES',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'LLLL2222',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'25',
                'nombre'=>'Lenguaje y comunicación Grado 4',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'MMMM3333',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'26',
                'nombre'=>'Informática y Tecnología Beta 2016',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'NNNN4444',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'27',
                'nombre'=>'MYP Chemistry LIBRO CONSULTA',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'OOOO5555',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'28',
                'nombre'=>'DOMINIOS DE ESPAÑOL 3',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'PPPP6666',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'29',
                'nombre'=>'DOMINIOS DE MATEMATICA 5',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'QQQQ7777',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'30',
                'nombre'=>'M Décibel 2 A2.1 Cahier+CD',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'RRRR8888',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'31',
                'nombre'=>'OBWL 3E 4 PERSUASION',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'SSSS9999',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'id'=>'32',
                'nombre'=>'MyON Digital Phonics Program',
                'editorial'=>'Editorial de prueba',
                'edicion'=>'1era',
                'isbn'=>'TTTT0000',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
