<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DireccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('direcciones')->insert([
            [
                'id'=> '1',
                'calle'=>'Av. Universidad',
                'no_exterior'=>'1000',
                'no_interior'=>'',
                'colonia'=>'Centro. Plaza Ubica',
                'cp' => '06880',
                'estado_id' => '22',
                'municipio_id'=>'1834',
                'referencia'=> 'casa 513',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'id'=> '2',
                'calle'=>'Circuito Puerta del Sol',
                'no_exterior'=>'701',
                'no_interior'=>'',
                'colonia'=>'Angel Aniel',
                'cp' => '76116',
                'estado_id' => '22',
                'municipio_id'=>'1834',
                'referencia'=> 'casa 514',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'id'=> '3',
                'calle'=>'Bolivar',
                'no_exterior'=>'515',
                'no_interior'=>'',
                'colonia'=>'algarin',
                'cp' => '06880',
                'estado_id' => '1',
                'municipio_id'=>'1',
                'referencia'=> 'casa 515',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'id'=> '4',
                'calle'=>'Bolivar',
                'no_exterior'=>'516',
                'no_interior'=>'',
                'colonia'=>'algarin',
                'cp' => '06880',
                'estado_id' => '1',
                'municipio_id'=>'1',
                'referencia'=> 'casa 516',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'id'=> '5',
                'calle'=>'Bolivar',
                'no_exterior'=>'517',
                'no_interior'=>'',
                'colonia'=>'algarin',
                'cp' => '06880',
                'estado_id' => '1',
                'municipio_id'=>'1',
                'referencia'=> 'casa 517',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ], [
                'id'=> '6',
                'calle'=>'Bolivar',
                'no_exterior'=>'518',
                'no_interior'=>'',
                'colonia'=>'algarin',
                'cp' => '06880',
                'estado_id' => '1',
                'municipio_id'=>'1',
                'referencia'=> 'casa 518',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]
        ]);
    }
}
