<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ReferenciaPedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('referencias_pedidos')->insert([
            [
                'id'=>'1',
                'pedido_id'=>'1',
                'referencia'=>'1161630012130519211',
                'tipo'=>'TIENDA',
                'dias_vigencia'=>'2',
                'pagado'=>'0'
            ]
        ]);
    }
}
