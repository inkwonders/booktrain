<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeccionesColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('secciones_colegios')->insert([
            [
                'id'=>'1',
                'colegio_id'=>'1',
                'nombre'=>'Kinder',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ], /*[
                'id'=>'2',
                'colegio_id'=>'1',
                'nombre'=>'Primaria',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],*/  [
                'id'=>'3',
                'colegio_id'=>'2',
                'nombre'=>'Secundaria',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]/*,[
                'id'=>'4',
                'colegio_id'=>'2',
                'nombre'=>'Preparatoria',
                'activo'=>'1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
