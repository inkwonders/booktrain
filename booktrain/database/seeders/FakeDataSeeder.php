<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Colegio;
use App\Models\Configuracion;
use App\Models\Libro;
use App\Models\Envio;
use App\Models\LibrosColegios;
use App\Models\LibroPaquete;
use App\Models\User;
use App\Models\SeccionColegio;
use App\Models\NivelColegio;
use App\Models\TicketArchivo;
use App\Models\TicketEntrada;
use App\Models\TicketEntradaArchivo;
use App\Models\TicketUsuario;
use App\Models\Pedido;
use App\Models\ResumenPedido;
use App\Models\DetalleResumenPedido;
use App\Models\DireccionEntrega;
use App\Models\DatosFacturas;
use App\Models\Estado;
use App\Models\Paquete;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Facades\Hash;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Vamos a crear 1000 libros
         * Vamos a crear 50 colegios
         * cada colegio puede tener entre 1 y 200 libros
         * cada colegio puede tener entre 1 y 4 secciones
         * cada seccion puede tener entre 1 y 6 niveles
         * cada nivel puede tener entre 1 y 2 paquetes de libros
         * cada paquete de libro puede tener entre 1 y 10 libros siempre que sean los libros del colegio
        */
        $this->command->info('Creando libros...');
        $this->command->getOutput()->progressStart(10);

        Libro::factory()->count(20)->create()->each(function($libro) {
            $this->command->getOutput()->progressAdvance();
        });

        $this->command->getOutput()->progressFinish();

        $this->command->info('Creando colegios...');
        $this->command->getOutput()->progressStart(10);

        Colegio::factory()->count(10)->create()->each(function($colegio) {

            Libro::factory()->count(rand(1, 200))->create()->each(function($libro) use ($colegio) {
                $colegio->libros()->attach($libro->id, [
                    'precio' => rand(25, 5000),
                    'obligatorio' => rand(0, 1),
                    'activo' => rand(0, 1),
                    'stock' => rand(0, 100)
                ]);
            });

            $colegio->secciones()->saveMany(
                SeccionColegio::factory()->count(rand(1, 4))->create(['colegio_id' => $colegio->id])->each(function($seccion) use ($colegio) {

                    $seccion->niveles()->saveMany(
                        NivelColegio::factory()->count(rand(1, 6))->create(['seccion_id' => $seccion->id])->each(function($nivel)  use ($colegio){

                            $nivel->paquetes()->saveMany(
                                Paquete::factory()->count(rand(1, 2))->create(['nivel_id' => $nivel->id])->each(function($paquete)  use ($colegio){

                                    $colegio->libros()->take(rand(1, 10))->get()->each(function($libro) use ($paquete, $colegio){
                                        $paquete->libros()->attach($libro->id, [
                                            'precio'        => $libro->pivot->precio,
                                            'obligatorio'   => rand(0, 1),
                                            'activo'        => $libro->pivot->activo,
                                            'colegio_id'    => $colegio->id
                                        ]);
                                    });

                                })
                            );
                        })
                    );
                })
            );
            $this->command->getOutput()->progressAdvance();
        });

        $this->command->getOutput()->progressFinish();
        /*
        * Vamos a crear 1000 usuario
        * cada usuario va a tener entre 0 y 5 pedidos
        * cada usuario tiene entre 0 y 3 datos de facturación
        * cada usuario tiene entre 0 y 3 direcciones de entrega
        * cada pedido tiene entre 1 y 3 resumenes de pedido
        * cada resumen de pedido tiene entre 1 y 6 detalles de pedido
        */

        $configuracion = Configuracion::whereEtiqueta('serie')->first();

        $this->command->info('Creando usuarios y tickets...');
        $this->command->getOutput()->progressStart(10);

        User::factory()->count(10)
            ->create(['password' => Hash::make('12345')])
            ->each(function($user) use ($configuracion) {

                $user->pedidos()->saveMany(
                    Pedido::factory()
                        ->count(1)//->count(rand(0, 5))
                        ->create([
                        'usuario_id' => $user->id,
                        'metodo_pago' => ['PUE','Diferido 3 meses','Diferido 6 meses','Diferido 9 meses'][rand(0, 3)],
                        'forma_pago' => ['Tarjeta de crédito','Tarjeta de débito','American Express','Referencia interbancaria','Tienda de conveniencia'][rand(0, 4)],
                        'serie' => $configuracion->etiqueta,
                        ])->each(function($pedido) use ($user){
                            $colegio = Colegio::inRandomOrder()->first();

                            $pedido->update([
                                'colegio_id' => $colegio->id,
                                ]);

                            $pedido->envio()->create(
                                Envio::factory()->create()->toArray()
                            );

                            $pedido->datosFactura()->saveMany(
                                DatosFacturas::factory()->count(rand(0, 1))->create([
                                    'metodo_pago_id' => rand(1, 4),
                                    'forma_pago_id' => rand(1, 2),
                                    'id_cfdi' => rand(1, 4)
                                ])// Verificar que se agregue usuario_id automáticamente
                            );
                            $pedido->direccion()->saveMany(
                                DireccionEntrega::factory()->count(rand(0, 3))
                                ->create(['colegio_id' => $colegio->id])
                                ->each(function($direccion) {
                                    $estado = Estado::inRandomOrder()->first();
                                    $direccion->update([
                                        'estado_id'     => $estado->id,
                                        'municipio_id'  => $estado->municipios()->inRandomOrder()->first()->id
                                    ]);
                                })
                            );
                            $pedido->resumenes()->saveMany(
                                ResumenPedido::factory()->count(rand(1, 3))->create([
                                    'pedido_id'  => $pedido->id,
                                    'paquete_id' => 1, // Provisional
                                    'subtotal'   => 0
                                    ])->each(function($resumen) use ($colegio) {

                                    $resumen->detalles()->saveMany(
                                        DetalleResumenPedido::factory()->count(rand(1, 6))->create([])->each(function($detalle_resumen) use ($colegio, $resumen) {
                                            $libro_aleatorio = $colegio->libros()->has('paquetes')->inRandomOrder()->first();

                                            $resumen->update([
                                                'paquete_id' => $libro_aleatorio->paquetes()
                                                                                ->where('colegio_id', $colegio->id)
                                                                                ->inRandomOrder()
                                                                                ->first()
                                                                                ->id
                                                ]);
                                            $detalle_resumen->update([
                                                'resumen_pedidos_id' => $resumen->id,
                                                'libro_id' => $libro_aleatorio->id
                                                ]);
                                        })
                                    );
                                    // $resumen->update([
                                    //     'subtotal' => $resumen->detalles->map(function($detalle) {
                                    //         return $detalle->precio_libro * $detalle->cantidad;
                                    //     })->sum()
                                    // ]);
                                })
                            );
                    })
                );
                $this->command->getOutput()->progressAdvance();
        /**
         * cada usuario puede tener entre 0 y 5 tikets
         * cada ticket puede tener entre 0 y 5 archivos
         * cada ticket puede tener entre 0 y 35 entradas
         * cada entrada de ticket puede tener entre 0 y 5 archivos
         */
                $user->tickets()->saveMany(
                    TicketUsuario::factory()->count(rand(0, 5))->create()->each(function($ticket) use ($user) {
                        $ticket->archivos()->saveMany(
                            TicketArchivo::factory()->count(rand(0, 5))->create(['ticket_usuario_id' => $ticket->id])
                        );
                        $ticket->entradas()->saveMany(
                            TicketEntrada::factory()->count(rand(0, 35))->create(['ticket_usuario_id' => $ticket->id, 'usuario_id' => $user->id])->each(function($entrada_ticket) use ($user) {
                                if($entrada_ticket->tipo_entrada == 2)
                                    $entrada_ticket->update(['usuario_id' => User::inRandomOrder()->first()->id]);

                                $entrada_ticket->archivos()->saveMany(
                                    TicketEntradaArchivo::factory()->count(rand(0, 5))->create(['entrada_id' => $entrada_ticket->id])
                                );
                            })
                        );
                    })
                );
                $this->command->getOutput()->progressAdvance();
        });

        $this->command->getOutput()->progressFinish();
    }
}
