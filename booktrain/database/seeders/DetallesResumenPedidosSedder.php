<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetallesResumenPedidosSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detalles_resumen_pedidos')->insert([
            [
                'id'=>'1',
                'resumen_pedidos_id'=>'1',
                'libro_id'=>'1',
                'precio_libro'=>'50',
                'cantidad' => '1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],[
                'id'=>'2',
                'resumen_pedidos_id'=>'1',
                'libro_id'=>'2',
                'precio_libro'=>'150',
                'cantidad' => '1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]/*,[
                'resumen_pedidos_id'=>'1',
                'libro_id'=>'5',
                'precio_libro'=>'150',
                'cantidad' => '3',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],[
                'resumen_pedidos_id'=>'3',
                'libro_id'=>'8',
                'precio_libro'=>'250',
                'cantidad' => '3',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],[
                'resumen_pedidos_id'=>'4',
                'libro_id'=>'2',
                'precio_libro'=>'800',
                'cantidad' => '2',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],[
                'resumen_pedidos_id'=>'5',
                'libro_id'=>'4',
                'precio_libro'=>'800',
                'cantidad' => '1',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ],[
                'resumen_pedidos_id'=>'6',
                'libro_id'=>'1',
                'precio_libro'=>'500',
                'cantidad' => '2',
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]*/
        ]);
    }
}
