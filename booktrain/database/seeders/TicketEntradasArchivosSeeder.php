<?php

namespace Database\Seeders;
use App\Models\TicketEntradaArchivo;
use Illuminate\Database\Seeder;

class TicketEntradasArchivosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TicketEntradaArchivo::insert([



            ['archivo_nombre_original' => 'archivo.jpg','url'=>'assets/images/images_entradas_archivos/imagen_archivo.jpg','tipo_archivo'=>'IMAGEN','orden'=>'1','entrada_id'=>'1','created_at'=>date('Y-m-d H:i:s')],
            ['archivo_nombre_original' => 'archivo.jpg','url'=>'assets/images/images_entradas_archivos/imagen_archivo.jpg','tipo_archivo'=>'IMAGEN','orden'=>'2','entrada_id'=>'2','created_at'=>date('Y-m-d H:i:s')],
            ['archivo_nombre_original' => 'archivo.jpg','url'=>'assets/images/images_entradas_archivos/imagen_archivo.jpg','tipo_archivo'=>'IMAGEN','orden'=>'3','entrada_id'=>'3','created_at'=>date('Y-m-d H:i:s')],
            ['archivo_nombre_original' => 'archivo.jpg','url'=>'assets/images/images_entradas_archivos/imagen_archivo.jpg','tipo_archivo'=>'IMAGEN','orden'=>'4','entrada_id'=>'4','created_at'=>date('Y-m-d H:i:s')],
            ['archivo_nombre_original' => 'archivo.jpg','url'=>'assets/images/images_entradas_archivos/imagen_archivo.jpg','tipo_archivo'=>'IMAGEN','orden'=>'5','entrada_id'=>'5','created_at'=>date('Y-m-d H:i:s')],



             ]);
    }
}


