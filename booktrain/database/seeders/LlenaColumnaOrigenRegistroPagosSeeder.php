<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LlenaColumnaOrigenRegistroPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $registros_afectados = 0;

        $registros = DB::table('registros_pagos')
            ->where('origen', null)
            ->get()->each(function($pago) use (&$registros_afectados) {
            $origen = '';

            if($pago->source == 'Registro de pago de forma manual')
                $origen = 'MANUAL';

            if(Str::length($pago->raw) < 145 && Str::length($pago->raw) > 140)
                $origen = 'CARGA MASIVA';

            if(Str::contains($pago->raw, '"type":"debit"'))
                $origen = 'API';

            if(Str::contains($pago->raw, '"httpStatus":"BAD_REQUEST"'))
                $origen = 'API';

            if(Str::contains($pago->raw, 'CardToken not found with token'))
                $origen = 'API';

            if($pago->source == 'venta_movil')
                $origen = 'VENTA MOVIL';

            if($pago->status_3ds != null)
                $origen = 'WEBHOOK';

            if(Str::startsWith($pago->source, 'token_'))
                $origen = 'API';

            if(Str::contains($pago->raw, '"transactionStatus":"IN_PROCESS"'))
                $origen = 'WEBHOOK';

            if(Str::contains($pago->raw, '"event":"cep.paid"'))
                $origen = 'WEBHOOK';

            // if(Str::contains($pago->raw, '"event":"cep.created"'))
            //     $origen = 'WEBHOOK';

            // Este tipo de origen cambia cuando el status sea recibido desde el webhook
            if($pago->status == 'REVIEW')
                $origen = 'API';

            if(Str::contains($pago->raw, '"paymentMethod":"BANORTE_PAY"'))
                $origen = 'WEBHOOK';

            if($origen != '') {
                $registros_afectados ++;
                DB::table('registros_pagos')->where('id', $pago->id)->update([
                    'origen' => $origen
                ]);
            }
        });

        $this->command->line("Se afectaron {$registros_afectados} de {$registros->count()} registros");
    }
}
