/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap');
window.Vue = require('vue').default;

// import { Ability } from '@casl/ability'
// export const ability = new Ability()


// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('boton-secciones', require('./components/BotonSecciones.vue').default);
Vue.component('colegio-seccion', require('./components/ColegioSeccion.vue').default);
Vue.component('seccion-izquierda', require('./components/seccionIzquierda.vue').default);
Vue.component('seccion-derecha', require('./components/SeccionDerecha.vue').default);
Vue.component('pizarron-avisos', require('./components/PizarronAvisos.vue').default);
Vue.component('input-texto', require('./components/inputTexto.vue').default);
Vue.component('input-select', require('./components/inputSelect.vue').default);
Vue.component('form-pagos', require('./components/formularioPago.vue').default);
Vue.component('form-venta-movil', require('./components/formularioVentaMovil.vue').default);
Vue.component('header-bt', require('./components/header.vue').default);
Vue.component('input-check', require('./components/inputCheck.vue').default);
Vue.component('span-etiqueta-mi-informacion', require('./components/spanEtiquetaMiInformacion.vue').default);
Vue.component('span-titulo', require('./components/spanTitulo.vue').default);
Vue.component('span-datos-mi-informacion', require('./components/spanMiInformacion.vue').default);
Vue.component('boton-editar-mi-informacion', require('./components/botonEditarMiInformacion.vue').default);
Vue.component('mi-informacion', require('./components/MiInformacion.vue').default);
Vue.component('input-editar-mi-informacion', require('./components/inputEditarMiInformacion.vue').default);
Vue.component('form-casos', require('./components/formCasos.vue').default);
Vue.component('select-estados', require('./components/selectEstados.vue').default);
Vue.component('select-municipio', require('./components/selectMunicipio.vue').default);
Vue.component('entradas', require('./components/entradas.vue').default);
Vue.component('boton-agregar-caso', require('./components/botonAgregarCaso.vue').default);
Vue.component('modal-agregar-caso', require('./components/modalAgregarCaso.vue').default);
Vue.component('carrito', require('./components/carrito.vue').default);
Vue.component('select-cfdi', require('./components/selectCfdi.vue').default);
Vue.component('ticket', require('./components/ticket.vue').default);
Vue.component('boton-cerrar-caso', require('./components/botonCerrarCaso.vue').default);
Vue.component('modal-cerrar-caso', require('./components/modalCerrarCaso.vue').default);
Vue.component('modal-calificar-ticket', require('./components/modalCalificarTicket.vue').default);
Vue.component('carrito-vacio', require('./components/sinArticulos.vue').default);
Vue.component('boton-calificar-ticket', require('./components/botonCalificarTicket.vue').default);
Vue.component('graficas', require('./components/graficas.vue').default);
Vue.component('registro', require('./components/seccionRegistro.vue').default);
Vue.component('reimprimir-comprobante', require('./components/reimprimirComprobante.vue').default);

Vue.component('fp-contacto_venta_movil', require('./components/venta_movil/contacto.vue').default);
Vue.component('fp-metodo-pago_venta_movil', require('./components/venta_movil/metodoPago.vue').default);
Vue.component('fp-pago-exitoso_venta_movil', require('./components/venta_movil/pagoExitoso.vue').default);

// Componenetes de reportes
Vue.component('cuadro-tipo-reporte', require('./components/reportes/cuadroTipoReporte.vue').default);
Vue.component('elegir-tipo-reporte', require('./components/reportes/elegirTipoReporte.vue').default);
Vue.component('reportes-colegio-general', require('./components/reportes/reporteColegioGeneral.vue').default);
Vue.component('reportes-colegio-alumno', require('./components/reportes/reporteColegioAlumno.vue').default);
Vue.component('elegir-reporte-envios', require('./components/reportes/elegirReporteEnvios.vue').default);

// Componentes Super Admin
Vue.component('referencia-bbva', require('./components/admin/referenciaBBVA.vue').default);
Vue.component('pedidos-entregados', require('./components/admin/pedidosEntregados.vue').default);
Vue.component('pedidos-enviados', require('./components/admin/pedidosEnviados.vue').default);
Vue.component('impresion-pedidos', require('./components/admin/impresionPedidos.vue').default);
Vue.component('carga-libros', require('./components/admin/cargaLibros.vue').default);
Vue.component('log', require('./components/admin/log.vue').default);

window.Swal = require('sweetalert2')

// Vue.use(VueClickOutsideElement)

// import VueClickOutsideElement from 'VueClickOutsideElement'

Vue.directive('click-outside', {
    bind: function(el, binding, vnode) {
        this.event = function(event) {
            if (!(el == event.target || el.contains(event.target))) {
                vnode.context[binding.expression](event);
            }
        };
        document.body.addEventListener('click', this.event)
    },
    unbind: function(el) {
        document.body.removeEventListener('click', this.event)
    }
});

const app = new Vue({
    el: '#app'
});
