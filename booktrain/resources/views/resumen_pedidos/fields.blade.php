<!-- Pedido Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    {!! Form::number('pedido_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Alumno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_alumno', 'Nombre Alumno:') !!}
    {!! Form::text('nombre_alumno', null, ['class' => 'form-control']) !!}
</div>

<!-- Paterno Alumno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paterno_alumno', 'Paterno Alumno:') !!}
    {!! Form::text('paterno_alumno', null, ['class' => 'form-control']) !!}
</div>

<!-- Materno Alumno Field -->
<div class="form-group col-sm-6">
    {!! Form::label('materno_alumno', 'Materno Alumno:') !!}
    {!! Form::text('materno_alumno', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush