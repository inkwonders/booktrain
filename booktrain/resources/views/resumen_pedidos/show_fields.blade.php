<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $resumenPedido->id }}</p>
</div>

<!-- Pedido Id Field -->
<div class="col-sm-12">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    <p>{{ $resumenPedido->pedido_id }}</p>
</div>

<!-- Nombre Alumno Field -->
<div class="col-sm-12">
    {!! Form::label('nombre_alumno', 'Nombre Alumno:') !!}
    <p>{{ $resumenPedido->nombre_alumno }}</p>
</div>

<!-- Paterno Alumno Field -->
<div class="col-sm-12">
    {!! Form::label('paterno_alumno', 'Paterno Alumno:') !!}
    <p>{{ $resumenPedido->paterno_alumno }}</p>
</div>

<!-- Materno Alumno Field -->
<div class="col-sm-12">
    {!! Form::label('materno_alumno', 'Materno Alumno:') !!}
    <p>{{ $resumenPedido->materno_alumno }}</p>
</div>

<!-- Subtotal Field -->
<div class="col-sm-12">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{{ $resumenPedido->subtotal }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $resumenPedido->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $resumenPedido->updated_at }}</p>
</div>

