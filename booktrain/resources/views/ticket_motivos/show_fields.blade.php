<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $ticketMotivo->id }}</p>
</div>

<!-- Nombre Field -->
<div class="col-sm-12">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $ticketMotivo->nombre }}</p>
</div>

<!-- Orden Field -->
<div class="col-sm-12">
    {!! Form::label('orden', 'Orden:') !!}
    <p>{{ $ticketMotivo->orden }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ticketMotivo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ticketMotivo->updated_at }}</p>
</div>

