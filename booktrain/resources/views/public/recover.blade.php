@extends('layouts.base')

@section('title')
    Booktrain | Recuperar contraseña
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/css/login.css') }}">
@endsection

@section('container')
<section class="flex flex-col w-screen h-screen overflow-x-hidden overflow-y-auto md:table">
    <div class="container box-border w-full h-auto px-10 mx-auto md:px-20 md:w-1/3 md:h-full md:table-cell">
        <section class="box-border flex flex-col justify-between w-full h-full min-h-screen pt-10 pb-8 md:pt-20">
            <div class="flex justify-center">
                <img class="w-64 logo-bk h-30" src="assets/img/svg/logo_principal.svg" alt="Logo principal">
            </div>

            <div class="flex items-center w-full h-auto pt-20 md:pt-0">
                <form method="POST" action="/recuperar" class="w-full">
                    @csrf
                    <div class="w-full px-4 text-center">
                        <h1 class="text-3xl font-rob-bold text-grayDarkBT">RESTABLECER CONTRASEÑA</h1>
                    </div>

                    <div class="w-full px-4 pt-12 text-center md:pt-20 font-rob-light">
                        <span class="text-lg leading-5 text-grayDarkBT">Ingresa tu e-mail para <span class="font-rob-medium">enviarte las instrucciones</span> para restablecer tu contraseña.</span>
                    </div>

                    <div class="mt-10 md:mt-16">
                        <input type="email" name="email" class="appearance-none rounded-none relative block w-full px-3 h-14 py-2 border {{ isset($error_message)? 'border-color-red--BK' : 'border-gray-300' }} placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:z-10 lg:text-lg sm:text-sm md:text-base" placeholder="Correo electrónico">
                    </div>

                    <button class="relative block w-full px-3 py-3 my-5 text-center border border-solid rounded-none rounded-md appearance-none h-14 background-blue--bk color-white--bk hover:bg-white hover:border-color-blue--BK hover:color-blue--bk lg:text-lg md:text-base sm:text-sm font-rob-bold">
                        SOLICITAR
                    </button>

                    <p class="px-4 text-center color-red--bk rob-light">
                        @if (isset($error_message)) {{ $error_message }} @else &nbsp; @endif
                    </p>

                    <div class="box-border flex flex-col w-full px-6 text-center md:hidden">
                        <div class="w-full mt-2 border-b border-gray-400"></div>
                        <a href="/" class="cursor-pointer color-blue--bk">Regresar</a>
                    </div>
                </form>
            </div>
            <div class="w-full border-gray-400 md:border-b"></div>
        </section>
    </div>

    <div class="container box-border relative flex items-end w-full h-full px-4 md:w-2/3 md:img-bk md:overflow-hidden md:table-cell">
        <div class="relative bottom-0 flex flex-col w-full md:w-11/12 flex-column md:absolute">
            <div class="hidden md:block md:w-2/4">
                <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS</h1>
                <span class="color-white--bk rob-light sm:text-sm md:text-base">Ingresa o regístrate para adquirir tu paquete de libros</span>
            </div>

            <div class="md:mt-5">
              <div class="w-full border-b border-gray-400"></div>
              <div class="flex flex-col justify-around w-full py-1 text-center md:flex-row h-28 md:h-8 md:py-0 opacity-80">
                <span class="text-xs color-gray--bk md:color-white--bk"><a class="cursor-pointer" href="{{ route('aviso_privacidad') }}" target="_blank">Aviso de privacidad</a> / <a class="cursor-pointer" href="politicas/" target="_blank">Políticas de compra, entrega y devolución</a></span>
                <span class="text-xs color-gray--bk md:color-white--bk">© 2021 PROVESA EDICIONES S.A. DE C.V. Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro</span>
                <span class="text-xs color-gray--bk md:color-white--bk">Desarrollado por <b><a class="cursor-pointer" href="http://inkwonders.com" target="_blank">INK WONDERS</a></b></span>
              </div>
            </div>
        </div>
    </div>

</section>
@endsection

@section('js')
@endsection
