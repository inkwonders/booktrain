@extends('layouts.base')

@section('title')
    Booktrain | Aviso de privacidad
@endsection

@section('css')
@endsection

@section('container')
    <div class="w-full h-auto flex justify-center p-6 px-8 md:p-20 box-border text-gray-800">
        <div class="w-full max-w-3xl text-justify text-sm md:text-lg">
            <br>
            <p class="font-rob-bold text-2xl text-left">AVISO DE PRIVACIDAD</p>
            <br>
            <div class="w-full border-b border-gray-800"></div>
            <ol class="list-decimal font-rob-bold">
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">RESPONSABLE</p>
                    <br>
                    <p class="font-rob-light">
                        Conforme a lo dispuesto en la Ley Federal de Protección a los Datos Personales en Posesión de los
                        Particulares y su reglamento, Mytang SA de CV con domicilio fiscal en Av Campo Militar #25-A2, Col
                        La Sierrita, CP 76137, Querétaro, QRO. (en lo sucesivo MYTANG) emite el presente Aviso de
                        Privacidad para informar que es responsable de la confidencialidad, uso y protección de la
                        información personal que nos llegare a proporcionar por los distintos medios que utilizamos para la
                        difusión de bienes y servicios en <a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a>

                    </p>
                    <br>
                    <p class="font-rob-light">
                        Su información personal será utilizada para proveer los servicios y productos que ha solicitado,
informarle sobre cambios en los mismos, evaluar la calidad del servicio que le brindamos, realizar
estudios internos sobre hábitos de consumo y en general para cualquier tipo de relación jurídica o
de negocios que lleve a cabo con nosotros.

                    </p>
                    <br>
                    <p class="font-rob-light">
                        Para las finalidades antes mencionadas, podríamos requerirle su nombre, dirección de entrega,
dirección fiscal, teléfono particular, teléfono celular, correo electrónico, RFC, fecha y lugar de
nacimiento, sexo, nacionalidad, edad e información crediticia y patrimonial en caso de requerir
algún crédito.

                    </p>
                    <br>
                    <p class="font-rob-light">
                        La entrega a MYTANG de cualquier solicitud o documento que contenga datos personales para
convertirse en cliente, proveedor o empleado de MYTANG, ya sea por el Titular o su representante
legal, será considerada como una constancia del consentimiento del Titular para el tratamiento de
sus datos personales conforme a las finalidades del presente aviso.
                    </p>
                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">TRATAMIENTO DE DATOS PERSONALES</p>
                    <br>
                    <p class="font-rob-light">
                        Usted tiene derecho a rectificar y cancelar sus datos personales, así como de oponerse al
tratamiento de los mismos, o revocar el consentimiento que para tal fin nos haya otorgado, a través
de los procedimientos que hemos implementado. Para conocer los procedimientos, requisitos y
plazos, favor de contactar a nuestro departamento de servicio al cliente en nuestro correo a los
teléfonos <a href="tel:8008908340" class="font-rob-regular hover:underline">(01 800) 890 8340</a>.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        Le informamos que sus datos personales no serán transferidos a terceros para fines distintos a los
necesarios para brindarle oportunamente los servicios y/o productos adquiridos en
<a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a> salvaguardando así su protección y confidencialidad, sin que para ello sea
necesario obtener su autorización en términos del artículo 37 de la Ley Federal de Protección de
Datos Personales en Posesión de los Particulares.
                    </p>
                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">CERTIFICADO DE SEGURIDAD SSL y AUTORIDAD</p>
                    <br>
                    <p class="font-rob-light">
                        Si Ud. considera que su derecho de protección de datos personales ha sido afectado por
                        alguna conducta de nuestros empleados o de nuestras actuaciones o respuestas, o presume
                        que en el tratamiento de sus datos personales existe alguna violación a las disposiciones
                        previstas en la Ley Federal de Protección de Datos Personales en Posesión de Particulares,
                        puede interponer la queja o denuncia correspondiente ante el IFAI. Para mayor información
                        visite <a href="http://www.ifai.org.mx" target="_blank" class="font-rob-regular hover:underline">www.ifai.org.mx</a>
                    </p>
                </li>
            </ol>
            <br>
        </div>
    </div>
@endsection

@section('js')
@endsection
