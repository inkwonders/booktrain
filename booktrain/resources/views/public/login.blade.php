@extends('layouts.base')

@section('title', 'Booktrain | Login')

@section('css')
<style>
    .boton_guia_compra{
        background-color:#DF5617;
        color: white;
    }
    .boton_guia_compra:hover{
        background-color: white;
        border-color: #DF5617;
        color: #DF5617;
    }

    .animated {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }

    .animated.faster {
        -webkit-animation-duration: 500ms;
        animation-duration: 500ms;
    }

    .fadeIn {
        -webkit-animation-name: fadeIn;
        animation-name: fadeIn;
    }

    .fadeOut {
        -webkit-animation-name: fadeOut;
        animation-name: fadeOut;
    }

    @keyframes fadeIn {
        from {
            opacity: 0;
        }

        to {
            opacity: 1;
        }
    }

    @keyframes fadeOut {
        from {
            opacity: 1;
        }

        to {
            opacity: 0;
        }
    }
</style>
@endsection

@section('container')
<section class="flex flex-col justify-between w-screen min-h-screen overflow-x-hidden overflow-y-auto md:table">

    <!-- FORMULARIOS LOGIN -->
    <div class="container box-border w-full h-auto px-10 mx-auto md:px-20 md:w-1/3 md:h-full md:table-cell">

        <section class="box-border flex flex-col justify-between w-full h-full pt-10 pb-8 md:h-screen 2xl:pt-20">

            <div class="flex justify-center">
                <img class="w-64 h-20 logo-bk" src="{{ asset('img/svg/logo_principal.svg') }}" alt="Logo principal">
            </div>

            <div class="w-full h-auto">
                <form action="{{ route('login') }}" class="w-full" method="POST">

                    <div class="mt-7">
                        <input value="{{ old('email') }}" type="email" class="appearance-none relative block w-full h-14 px-3 py-2 border {{ isset($error)? 'border-color-red--BK' : 'border-gray-300' }} placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:z-10 lg:text-lg sm:text-sm md:text-base" placeholder="Correo electrónico" name="email">
                    </div>

                    <div class="mt-7">
                        <input type="password" class="appearance-none relative block w-full h-14 px-3 py-2 border {{ isset($error)? 'border-color-red--BK' : 'border-gray-300' }} placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:z-10 lg:text-lg sm:text-sm md:text-base" placeholder="Contraseña" name="password">
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                    @if(isset($error))
                    <!-- ERROR SPAN -->
                    <div class="w-full h-5 px-4 text-center">
                        <span class="py-2 text-sm color-red--bk rob-light">{{ $error }}</span>
                    </div>
                    @endif

                    <button type="submit" class="relative flex items-center justify-center w-full px-3 py-3 my-5 text-center uppercase border border-solid rounded-md appearance-none mt-7 bg-blueBT h-14 text-whiteBT hover:bg-whiteBT hover:border-blueBT hover:text-blueBT lg:text-lg md:text-base sm:text-sm rob-bold">
                        Entrar
                    </button>

                </form>

                <div class="box-border flex flex-col w-full px-6 py-4 text-center">
                    <a href="{{ route('recuperar') }}" class="cursor-pointer text-blueBT rob-light">¿Has olvidado contraseña?</a>
                    <div class="w-full mt-2 border-b border-gray-400"></div>
                </div>

                <a href="{{ route('registro') }}">
                    <div class="relative flex items-center justify-center w-full px-3 py-3 my-1 text-center uppercase border border-solid rounded-md appearance-none bg-greenBT h-14 text-whiteBT hover:bg-whiteBT hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm rob-bold">
                        <span>Registrarse</span>
                    </div>
                </a>

            </div>
            <div>
                <a href="https://www.youtube.com/watch?v=OqZEtwu30Cw" target=”_blank”>

                    <div class="relative flex items-center justify-center w-full px-3 py-3 my-1 text-center uppercase border border-solid rounded-md appearance-none boton_guia_compra h-14 text-whiteBT lg:text-lg md:text-base sm:text-sm rob-bold">
                        <span>Guía de compra</span>
                    </div>

                </a>
            </div>
            {{-- <div>
                <button onclick="openModal()" class="w-full">

                    <div class="relative flex items-center justify-center w-full px-3 py-3 my-1 text-center uppercase border border-solid rounded-md appearance-none boton_guia_compra h-14 text-whiteBT lg:text-lg md:text-base sm:text-sm rob-bold">
                        <span>Facturación</span>
                    </div>

                </button>
            </div> --}}


            <div class="w-full border-gray-400 md:border-b"></div>
        </section>
    </div>

    <div class="container box-border relative inline-flex items-end w-full h-full px-4 md:w-2/3 md:img-bk md:overflow-hidden md:table-cell">
        <div class="relative bottom-0 flex flex-col w-full md:w-11/12 flex-column md:absolute">
            <div class="hidden md:block md:w-2/4">
                <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS</h1>
                <span class="color-white--bk rob-light sm:text-sm md:text-base">Ingresa o regístrate para adquirir tu paquete de libros</span>
            </div>

            <div class="md:mt-5">
                <div class="w-full border-b border-gray-400"></div>
                <div class="flex flex-col justify-around w-full py-1 text-center md:flex-row h-28 md:h-8 md:py-0 opacity-80">
                    <span class="text-xs color-gray--bk md:color-white--bk"><a class="cursor-pointer" href="aviso-de-privacidad/" target="_blank">Aviso de privacidad</a> / <a class="cursor-pointer" href="politicas/" target="_blank">Políticas de compra, entrega y devolución</a></span>
                    <span class="text-xs color-gray--bk md:color-white--bk">© 2021 PROVESA EDICIONES S.A. DE C.V. Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro</span>
                    <span class="text-xs color-gray--bk md:color-white--bk">Desarrollado por <b><a class="cursor-pointer" href="http://inkwonders.com" target="_blank">INK WONDERS</a></b></span>
                </div>
            </div>
        </div>
    </div>

    <!-- modal -->
    <div class="fixed inset-0 z-50 flex items-center justify-center w-full overflow-hidden main-modal h-100 animated fadeIn faster"
		style="background: rgba(0,0,0,.7);">
		<div
			class="z-50 w-11/12 h-full mx-auto overflow-y-auto bg-white border border-teal-500 rounded shadow-lg lg:h-auto modal-container md:max-w-md">
			<div class="px-6 py-4 text-left modal-content">
				<!--Title-->
				<div class="flex items-center justify-between pb-3">
					<p class="text-2xl font-bold">Facturación</p>
					<div class="z-50 cursor-pointer modal-close" onclick="modalClose()">
						<svg class="text-black fill-current" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
							viewBox="0 0 18 18">
							<path
								d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z">
							</path>
						</svg>
					</div>
				</div>
				<!--Body-->
				<div class="my-5">
					<p>Podras solícitar tu factura hasta los siguientes <b>5 días hábiles</b> a partir de la fecha de tu compra.</p><br>
                    <p>Tu factura se te enviará por correo electrónico en las siguientes <b>48 horas</b> de la solicitud.</p><br>
                    <p>Por favor solicitar tu factura enviando los siguientes datos al correo: <b>malvarado@bodeli.com.mx</b></p><br>
                    <p>
                        <ul style="list-style:decimal" class="pl-4">
                            <li class="">Nombre o razón social *</li>
                            <li class="">RFC *</li>
                            <li class="">Nombre del colegio *</li>
                            <li class="">Dirección fiscal con Código postal y Estado *</li>
                            <li class="">Nombre de la persona que solicita la factura *</li>
                            <li class="">Fecha de su compra *</li>
                            <li class="">Monto exacto de su compra *</li>
                            <li class="">CFDI: Gastos en general ó por definir *</li>
                            <li class="">Teléfono para contactarlo</li>
                        </ul>
                    </p><br>
                    <p>* Datos obligatorios.</p>
				</div>
				<!--Footer-->
				<div class="flex justify-end pt-2">
					<button
						class="p-3 px-4 ml-3 text-white bg-blue-500 rounded-lg focus:outline-none hover:bg-teal-400" onclick="modalClose()">Aceptar</button>
				</div>
			</div>
		</div>
	</div>

</section>
@endsection

@push('script_stack')
<script>

    const modal = document.querySelector('.main-modal');
    const closeButton = document.querySelectorAll('.modal-close');

    const modalClose = () => {
        document.querySelector('.main-modal').classList.remove('fadeIn');
        document.querySelector('.main-modal').classList.add('fadeOut');
        setTimeout(() => {
            document.querySelector('.main-modal').style.display = 'none';
        }, 500);
    }

    const openModal = () => {
        document.querySelector('.main-modal').classList.remove('fadeOut');
        document.querySelector('.main-modal').classList.add('fadeIn');
        document.querySelector('.main-modal').style.display = 'flex';
        document.querySelector('.main-modal').style.display = 'flex'
    }

    for (let i = 0; i < closeButton.length; i++) {

        const elements = closeButton[i];

        elements.onclick = (e) => modalClose();

        document.querySelector('.main-modal').style.display = 'none';

        window.onclick = function (event) {
            if (event.target == document.querySelector('.main-modal')) modalClose();
        }
    }

</script>
@endpush
