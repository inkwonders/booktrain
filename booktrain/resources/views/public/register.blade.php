@extends('layouts.base')

@section('title')
    Booktrain | Registro
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/css/login.css') }}">
@endsection



@section('container')

        <section class="md:table w-screen h-screen flex flex-col overflow-x-hidden overflow-y-auto">
                <!-- FORMULARIOS LOGIN -->
            <div class="container mx-auto box-border px-10 md:px-20 w-full md:w-1/3 h-auto md:h-full md:table-cell">

                <section class="flex flex-col justify-between w-full h-full box-border pt-10 2xl:pt-20 pb-8">

                    <div class="flex justify-center">

                        <img class="logo-bk w-64 xl:h-28 2xl:h-20 2xl:w-80" src="assets/img/svg/logo_principal.svg" alt="Logo principal">

                    </div>

                    <registro
                        error="{{ isset($error)? $error : '' }}"
                        csrf="{{ csrf_token() }}"
                        apellido="{{ old('apellidos') }}"
                    ></registro>

                    <div class="md:border-b border-gray-400 w-full"></div>

                </section>

            </div>



            <div class="container w-full md:w-2/3 h-full md:img-bk px-4 flex items-end box-border relative md:overflow-hidden md:table-cell">

                <div class="w-full md:w-11/12 flex flex-column relative md:absolute bottom-0 flex-col">

                    <div class="hidden md:block md:w-2/4">

                        {{-- <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS DE MYTANG</h1> --}}
                        <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS</h1>

                        <span class="color-white--bk rob-light sm:text-sm md:text-base">Ingresa o regístrate para adquirir tu paquete de libros</span>

                    </div>

                    <div class="md:mt-5">

                      <div class="border-b border-gray-400 w-full"></div>

                      <div class="flex flex-col md:flex-row h-28 md:h-8 py-1 md:py-0 justify-around w-full opacity-80 text-center">

                        <span class="color-gray--bk md:color-white--bk text-xs"><a class="cursor-pointer" href="aviso-de-privacidad/" target="_blank">Aviso de privacidad</a> / <a class="cursor-pointer" href="politicas/" target="_blank">Políticas de compra, entrega y devolución</a></span>

                        <span class="color-gray--bk md:color-white--bk text-xs">© 2021 PROVESA EDICIONES S.A. DE C.V. Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro</span>

                        <span class="color-gray--bk md:color-white--bk text-xs">Desarrollado por <b><a class="cursor-pointer" href="http://inkwonders.com" target="_blank">INK WONDERS</a></b></span>

                      </div>

                    </div>


                </div>

            </div>

        </section>
@endsection

@section('js')

@endsection
