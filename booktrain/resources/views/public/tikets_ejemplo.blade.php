@extends('layouts.logged')
@section('title')
    Tickets
@endsection
@section('soporteSelected')
    border-b-green--BK
@endsection
@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>
                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }
        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .nuevo {
            color: var(--main-green--bk);
        }
        .cerrado {
            color: var(--main-red--bk);
        }
        .cliente, .soporte {
            color: #f7931e;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }
        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }
        .c_positivos::after {
            background-color: #109618;
        }
        .c_negativo::after {
            background-color: #ff5733;
        }
        .c_abiertos::after {
            background-color: #ff9900;
        }
        .c_cerrados::after {
            background-color: #3366cc;
        }

    </style>
@endsection

@section('contenido')
    <section class="w-full min-h-custom flex flex-col items-center pl-10">
        <div class="w-full flex justify-between py-10">
            <h2 class="color-red--bk font-bold text-xl">QUEJAS Y SUGERENCIAS</h2>

            @if($errors->any())
            <div id="modal-cerrar" class="modal flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
                <div class="bg-white rounded-lg w-1/3">
                    <div class="flex flex-col items-start p-4">
                        <div class="flex items-center w-full justify-center">
                            <div class="text-gray-900 font-medium text-lg pb-3">
                                <center> <p  class="mt-8 color-gray--bk">{{ implode('', $errors->all(':message')) }}</p> </center>
                            </div>
                        </div>
                        <div class="w-full">
                            <div class="flex justify-around w-full pt-5">
                                <div id="btn_cerrar" data-dismiss="modal" class=" close w-3/2 background-red--bk text-white rounded-md uppercase font-rob-bold cursor-pointer flex justify-center items-center hover:bg-white hover:border-red-500 hover:text-redBT border border-solid">
                                    Cerrar
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @hasrole("Cliente")
                <form-casos csrf="{{ csrf_token() }}"/>
            @endhasrole
        </div>
        @hasrole("Admin")

        <div class="w-full flex justify-between">
            <graficas />
        </div>
        @endhasrole
        <div class="w-full flex lg:pt-16 mt-6 lg:mt-0 rounded  bg-white justify-center items-center">

            <livewire:tikets-ejemplo searchable="name, email, status" exportable />
            {{-- <table id="example" class="stripe tabla_quejas responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">#</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">ESTATUS</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">TÍTULO</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">MOTIVO</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">FECHA ÚLTIMA ENTRADA</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">USUARIO ÚLTIMA ENTRADA</th>
                        <th class="px-6 py-3 border-b-2 border-gray-300 text-center leading-4 color-gray--bk tracking-wider">ACCIONES</th>
                    </tr>
                </thead>
                @hasrole('Admin')
                <tbody>
                    @foreach ($tickets as $ticket)
                        @php
                            $datos_status = $ticket->textoEstatus($ticket->status);
                        @endphp
                        <tr class="text-center">
                            <td>{{ $ticket->id }}</td>
                            <td class="{{ $datos_status['clase'] }} font-rob-bold text-sm">{{ $datos_status['texto'] }}</td>
                            <td>{{ $ticket->titulo }}</td>
                            <td>{{ $ticket->ticketMotivo->nombre }}</td>
                            @if ($ticket->entradas->count() == 0)
                                <td>{{ date_format($ticket->created_at,"d / m / Y") }}</td>
                                <td>{{ $ticket->user->name }} {{ $ticket->user->apellidos }}</td>
                            @else
                                <td>{{ date_format($ticket->entradas->last()->created_at,"d / m / Y") }}</td>
                                <td>{{ $ticket->entradas->last()->user->name }} {{ $ticket->entradas->last()->user->apellidos }} </td>
                            @endif
                            @if ($datos_status['texto'] == "Cerrado" && $ticket->calificacion == 0)
                                <td class="flex justify-around items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                </td>
                            @elseif ($datos_status['texto'] == "Cerrado" && $ticket->calificacion != 0)
                                <td class="flex justify-center items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                </td>
                            @else
                                <td class="flex justify-center items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                @else
                <tbody>
                    @foreach ($usuario->tickets as $ticket)
                        @php
                            $datos_status = $ticket->textoEstatus($ticket->status);
                        @endphp
                        <tr class="text-center">
                            <td>{{ $ticket->id }}</td>
                            <td class="{{ $datos_status['clase'] }} font-rob-bold text-sm">{{ $datos_status['texto'] }}</td>
                            <td>{{ $ticket->titulo }}</td>
                            <td>{{ $ticket->ticketMotivo->nombre }}</td>
                            @if ($ticket->entradas->count() == 0)
                                <td>{{ date_format($ticket->created_at,"d / m / Y") }}</td>
                                <td>{{ $ticket->user->name }} {{ $ticket->user->apellidos }}</td>
                            @else
                                <td>{{ date_format($ticket->entradas->last()->created_at,"d / m / Y") }}</td>
                                <td>{{ $ticket->entradas->last()->user->name }} {{ $ticket->entradas->last()->user->apellidos }} </td>
                            @endif
                            @if ($datos_status['texto'] == "Cerrado" && $ticket->calificacion == 0)
                                <td class="flex justify-around items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                    <boton-calificar-ticket caso="{{ $ticket->id }}" class="icono_calificar"/>
                                </td>
                            @elseif ($datos_status['texto'] == "Cerrado" && $ticket->calificacion != 0)
                                <td class="flex justify-center items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                </td>
                            @else
                                <td class="flex justify-center items-center p-4">
                                    <a href="/tickets/{{ $ticket->id }}" class="flex justify-center items-center">
                                        <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                @endhasrole
            </table> --}}
        </div>

    </section>
@endsection
@section('js-interno')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>

    <script>

        $(document).ready(function() {
            var table = $('#example').DataTable();
            table.order( [ 0, 'desc' ] ).draw();

            $("#btn_cerrar").on("click", function(){
                $('#modal-cerrar').addClass("hidden");
            })
        });

        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            responsive: {
                details: {
                    type: 'column'
                }
            },

             columnDefs: [
                { responsivePriority: 1, targets: -1 },
                { className: 'dtr-control', orderable: false }
            ],
            colReorder: true,
            scrollY: false,
            scrollX: false
        } );


    </script>
@endsection
