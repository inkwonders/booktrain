@extends('layouts.logged')
@section('title')
    Pedidos
@endsection
@section('usuariosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
<style>
    /* Estilos switch */
         /* The switch - the box around the slider */
         .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 25px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        border-radius: 5px;
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: red;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 12px;
        left: 4px;
        bottom: 4px;
        border-radius: 3px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #81b84e;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #81b84e;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(36px);
        -ms-transform: translateX(36px);
        transform: translateX(36px);
        }
</style>
@livewireStyles


@endsection

@hasrole('Admin')
@section('contenido')
<section class="w-full flex flex-col items-center justify-center min-h-custom px-2 py-4">
    <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK justify-center p-10">


        {{-- @livewire('form-reporte-pedidos') --}}
        {{-- <livewire:form-reporte-pedidos/>
        <livewire:example-datatable-pedidos searchable="folio, nombre_contacto, apellidos_contacto, correo" exportable /> --}}
        <livewire:contenedor-padre-reporte-pedidos/>
        {{-- @livewire('example-datatable-pedidos') --}}
    </div>
    <a href="/tickets"><button class="h-12 w-56 bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</button></a>
            <br><br><br>
</section>

<!--Modal-->
<div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="interestModal">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
            <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <form  action="/referencias/reporte_referencias_status/" method="GET" class="relative w-full items-center form-login" id="form-login" name="form-login" >
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="sm:flex sm:items-start pb-4">
                        {{-- <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"> --}}
                            {{-- <svg @click="toggleModal" class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                            </svg> --}}
                        {{-- </div> --}}
                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                            <h2 class="swal2-title pb-4" id="modal-title" >
                            Selecciona el estatus al que quieres cambiar este pedido
                            </h2>

                            <div class="relative w-full border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2" style="width: 94.5%;">
                                <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                                <select name="estatus" required class="relative w-1/2 mr-2 p-2 border color-gray--bk appearance-none" style="width: 110%">
                                    <option value="">Selecciona</option>|
                                    <option value="PAGADO">PAGADO</option>
                                    <option value="CANCELADO">CANCELADO</option>
                                </select>
                            {{-- <input type="text" name="pedido_id_cambio" id="pedido_id" value=""> --}}
                            </div>

                        </div>

                    </div>
                    <div class="grid grid-cols-2 gap-2 pt-6">
                        <div class="col-start-1 col-end-2 " style="padding-left: 53%;">
                        <button type="submit"  style="display: inline-block; " id="btnSend" name="btnSend" class="swal2-confirm swal2-styled">Modificar</button>
                        </div>
                        <div class="col-end-3  flex justify-end" style="padding-right: 53%;">
                        <button type="button" style="display: inline-block; background-color: rgb(144, 136, 136);" class="closeModal swal2-confirm swal2-styled">Cancelar</button>
                        </div>
                    </div>
            </div>

        </form>
        </div>
    </div>
</div>
{{-- endmodal --}}
@if($errors->any())
<div id="modal-cerrar" class="modal flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
    <div class="bg-white rounded-lg w-1/3">
        <div class="flex flex-col items-start p-4">
            <div class="flex items-center w-full justify-center">
                <div class="text-gray-900 font-medium text-lg pb-3">
                <center> <h2 class="rob-bold color-black--bk text-2xl">{{ implode('', $errors->all(':message')) }}</h2> </center>
                </div>
            </div>
            <div class="w-full">
                <div class="flex justify-around w-full pt-5">
                    <div id="btn_cerrar" data-dismiss="modal" class=" close w-1/4 background-red--bk text-white rounded-md uppercase font-rob-bold cursor-pointer flex justify-center items-center hover:bg-white hover:border-red-500 hover:text-redBT border border-solid">
                        Cerrar
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endif
{{-- endmodal de errores --}}


@endsection
@endhasrole


@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
          $(document).on('submit', '.formulario-cambio', function(e) {
           e.preventDefault();
           Swal.fire({
                    title: '¿Deseas cambiar el estatus de este pedido?',
                    showDenyButton: true,
                    // showCancelButton: true,
                    confirmButtonText: `Cambiar`,
                    denyButtonText: `Cancelar`,
                    denyButtonColor: '#908888',
                    }).then((result) => {
                    // if (result.isConfirmed) {
                    //     Swal.fire('Se ha cambiado!', '', 'success')
                    //     $('#btnSend').click();
                    // } else if (result.isDenied) {
                    //     Swal.fire('Los cambios no se efectuaron', '', 'info')
                    // }
                    if(result.value){
                        Swal.fire('Cambiando..!', '', 'success')
                        this.submit();
                    }
                })
          });

          $(document).on('submit', '.formulario-envio-mail', function(e) {
           e.preventDefault();
           Swal.fire({
                    title: '¿Deseas reenviar el correo de confirmación de pago al cliente?',
                    showDenyButton: true,
                    // showCancelButton: true,
                    confirmButtonText: `Enviar`,
                    denyButtonText: `Cancelar`,
                    denyButtonColor: '#908888',
                    }).then((result) => {
                    // if (result.isConfirmed) {
                    //     Swal.fire('Se ha cambiado!', '', 'success')
                    //     $('#btnSend').click();
                    // } else if (result.isDenied) {
                    //     Swal.fire('Los cambios no se efectuaron', '', 'info')
                    // }
                    if(result.value){
                        Swal.fire('Enviando correo..!', '', 'success')
                        this.submit();
                    }
                })
          });

    </script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.openModal').on('click', function(e){
            $('#interestModal').removeClass('invisible');
            var userid = $(this).data('userid');
        var form = $('form.form-login');
        var input = $('<input type="hidden" name="pedido_id" value="'+userid+'" />');
        if (form.find('input[name="pedido_id"]').length) {
            form.find('input[name="pedido_id"]').val(userid);
        } else {
            form.prepend(input);
        }
        });

        $('.closeModal').on('click', function(e){
            $('#interestModal').addClass('invisible');
        });
    });
</script>

    <script>

        $(document).ready(function() {

            $("#btn_cerrar").on("click", function(){
                $('#modal-cerrar').addClass("hidden");
            })
            // Datepicker
            $("#inicio").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#fin").datepicker({ dateFormat: 'yy-mm-dd' });
            // Datatable
            var table = $('#example2').DataTable();
            table.order( [ 0, 'desc' ] ).draw();
            // ingresar datos de hoy
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;
            let url_referencia = window.location.href;
            if(url_referencia.includes('?')){
                var url_string = window.location.href
                var url = new URL(url_string);
                var inicio = url.searchParams.get("inicio");
                var fin = url.searchParams.get("fin");
                // console.log(inicio);
                $("#inicio").val(inicio);
                $("#fin").val(fin);

            }else{
               $("#inicio").val(today);
               $("#fin").val(today);
            //    $("#btn_buscar").click();
            }

        });

        // $(document).ready(function () {
        // $("#inicio").keyup(function () {
        //     var value = $(this).val();
        //     $("#inicio2").val(value);
        // });
        // });

        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            // responsive: {
            //     details: {
            //         type: 'column'
            //     }
            // },
            // columnDefs: [
            //     { responsivePriority: 1, targets: -1 },
            //     { className: 'dtr-control', orderable: false }
            // ],
            colReorder: false,
            scrollY: false,
            scrollX: false,
        } );



    </script>



@endsection



{{-- <!DOCTYPE html>
<html>
<head>
    <title>Laravel 8 Livewire DataTable Example Tutorial - XpertPhp</title>
    @livewireStyles
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/1.9.2/tailwind.min.css" integrity="sha512-l7qZAq1JcXdHei6h2z8h8sMe3NbMrmowhOl+QkP3UhifPpCW2MC4M0i26Y8wYpbz1xD9t61MLT9L1N773dzlOA==" crossorigin="anonymous" />
</head>
<body>

<div class="container">

    <div class="card">
      <div class="card-header">
        Usuarios
      </div>
      <div class="card-body">
        <livewire:example-datatable searchable="name, email, status" exportable />
      </div>
    </div>
</div>
</body>

</html> --}}
