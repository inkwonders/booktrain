@extends('layouts.logged')
@section('title')
    Usuarios
@endsection
@section('usuariosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
<style>
    /* Estilos switch */
         /* The switch - the box around the slider */
         .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 25px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        border-radius: 5px;
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: red;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 12px;
        left: 4px;
        bottom: 4px;
        border-radius: 3px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #81b84e;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #81b84e;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(36px);
        -ms-transform: translateX(36px);
        transform: translateX(36px);
        }

/* Estilos tabla */
    table>tbody>tr:nth-child(odd)>td,
    table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #F2F2F2;
        font-family: 'roboto-light';
        color: #808080;
        font-size: 1rem;
        line-height: 1.5rem;
    }
    table>tbody>tr:nth-child(even)>td,
    table>tbody>tr:nth-child(even)>th {
        background-color: white;
        font-family: 'roboto-light';
        color: #808080;
        font-size: 1rem;
        line-height: 1.5rem;
    }
    table>thead>tr>th>button>span{
        font-size: 1rem;
        line-height: 1.5rem;
    }
    input{
        border: #808080 solid 1px;
        padding-left: 10px !important;
    }

    select{
        border: #808080 solid 1px;
    }
</style>
@livewireStyles
@endsection

@hasrole('Admin')
@section('contenido')
<section class="w-full flex flex-col items-center justify-center min-h-custom px-2 py-4">
    <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK justify-center p-10">
        <div class="relative flex justify-between w-full mb-4">
            <h2 class="color-red--bk font-bold text-xl">Usuarios</h2>
            <a href="/admin/nuevo_usuario" class="background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">+ NUEVO USUARIO</a>
            {{-- <input type="text" name="" wire-model="filters.search" id=""> --}}
        </div>
        {{-- <livewire:example-datatable/> --}}
    </div>
</section>
@endsection
@endhasrole
