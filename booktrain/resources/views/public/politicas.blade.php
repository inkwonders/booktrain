@extends('layouts.base')

@section('title')
    Booktrain | Políticas de compra, entrega y devolución
@endsection

@section('css')
@endsection

@section('container')
    <div class="w-full h-auto flex justify-center p-6 px-8 md:p-20 box-border text-gray-800">
        <div class="w-full max-w-3xl text-justify text-sm md:text-lg">
            <br>
           <center> <p class="font-rob-bold text-2xl ">Políticas de compra, devolución y de entrega</p></center>
            <br>
            <div class="w-full border-b border-gray-800"></div>
            <ol class="list-decimal font-rob-bold">
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">PRECIOS Y DISPONIBILIDAD</p>
                    <br>
                    <p class="font-rob-light">
                        Los precios y promociones sólo aplican para la tienda online.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        La disponibilidad y los precios de los productos están sujetos a cambio sin previo aviso por parte de las editoriales y proveedores, en caso de materiales agotados se informará al cliente la fecha de disponibilidad.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        Todos los precios están expresados en pesos mexicanos.
                    </p>
                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">FORMAS DE PAGO</p>
                    <br>
                    <p class="font-rob-light">
                        Tarjeta de débito y crédito VISA, MASTERCARD y AMEX.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        Transferencia o Deposito en Practicaja
                    </p>
                    <br>

                    <p class="font-rob-light">
                        El cargo en el estado de cuenta de su tarjeta se verá reflejado como Mercado Pago Ciudad de Mex, favor de tomarlo en cuenta para evitar confusiones.
                    </p>
                    <br>


                    <p class="font-rob-light">
                        Al momento de adquirir sus productos en la tienda online, usted seleccionará la alternativa de entrega que más se adapte a sus necesidades, los tipos de entrega disponibles son acordados con el colegio, puede consultar las opciones en el Flyer (tríptico) proporcionado por su colegio.
                    </p>
                    <br>

                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">ENVÍOS Y ENTREGAS</p>
                    <br>
                    <p class="font-rob-light">
                        En caso de haber seleccionado envío a domicilio por paquetería, el costo de envió es de $200 a cualquier parte de la República Mexicana, <a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a> se reserva el derecho de utilizar la empresa de paquetería con las que tiene convenio, para cumplir los días de entrega esperados.
                    </p>
                    <br>

                    <p class="font-rob-light">
                        Los tiempos de entrega están sujetos a la mensajería seleccionada para su estado con tiempos promedio de entrega de 5 días hábiles a partir de la recepción de la confirmación del pago.
                    </p>
                    <br>


                    <p class="font-rob-light">
                        La confirmación de su pedido se realizará por medio del correo electrónico registrado en su compra. Deberá revisar en su spam (bandeja de correo no deseado) en caso de no haberlo recibido en su bandeja de entrada, el correo es <a href="mailto:noreply@booktrain.com.mx" class="font-rob-regular hover:underline">noreply@booktrain.com.mx</a>
                    </p>
                    <br>


                    <p class="font-rob-light">
                        Los paquetes se enviarán una vez que se tenga el pedido completo en el Centro de Distribución y en la fecha confirmada con el colegio. Para colegios con preventa, favor de consultar en el flyer (tríptico) la fecha de inicio de entrega de materiales. Si en su compra seleccionó entrega PickUp, será con los horarios y días que están asignados por el colegio y se consulta en el flyer (tríptico) de la venta.
                    </p>
                    <br>


                    <p class="font-rob-light">
                        Todas las entregas cubren los protocolos de sanitización, el personal está capacitado para el seguimiento de los protocolos vigentes.
                    </p>
                    <br>

                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">FACTURACIÓN</p>
                    <br>
                    <p class="font-rob-light">
                        Al momento de realizar su compra, el comprador indica si requiere factura y deberá proporcionar sus datos fiscales.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        La factura se emitirá y enviará al correo electrónico del usuario registrado en el momento en que el envío sale del centro de distribución.
                    </p>
                    {{-- <br>
                    <p class="font-rob-light">
                        La factura va impresa en su pedido y se envía al correo electrónico que describió en sus
                        datos de perfil cuando solicitó factura.
                    </p> --}}
                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">DEVOLUCIONES</p>
                    <br>
                    <p class="font-rob-light">
                        Para material digital o licencias de plataformas no hay devoluciones debido a las políticas vigentes de las editoriales y desarrolladoras de contenido digital; le sugerimos confirmar la información antes de realizar su pedido.
                    </p>
                    <br>
                        <p class="font-rob-light">
                            Se tiene estipulado como máximo 2 días hábiles a partir de la recepción del material para proceder con algún reclamo o aclaración del envío.

                        </p>
                    <br>
                        <p class="font-rob-light">
                            Esta aclaración se hace vía WhatsApp <a href=" https://wa.me/+524426803196" target="_blank" class="font-rob-regular hover:underline">4426803196.</a>
                        </p>
                    <br>
                        <p class="font-rob-light">
                            Si la responsabilidad no es imputable a Booktrain, el cliente correrá con los gastos de envío, para hacer llegar el material a nuestro almacén de Booktrain ubicada en Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro. Dentro de los 5 siguientes días de haber generado el reporte vía WhatsApp.
                        </p>
                    <br>
                        <p class="font-rob-light">
                            Una vez que el material llegue a nuestro almacén se inspecciona para que este en condiciones vendibles sin estar maltratado, rayado, forrado o con algún deterioro que no pueda utilizarse nuevamente.
                        </p>
                        <br>
                        <p class="font-rob-light">
                            Una vez finalizando la inspección, se hace el reembolso a la cuenta proporcionada en los siguientes 7 días hábiles o se procede al envío del material correcto.
                        </p>

                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">ACCESOS AL SITIO</p>
                    <br>
                    <p class="font-rob-light">
                        <a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a> se reserva el derecho de interrumpir el servicio de compra online en caso de requerir dar mantenimiento, modificaciones o actualizaciones al sitio web.
                    </p>
                    <br>
                    <p class="font-rob-light">
                            El uso de <a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a> , así como el adquirir productos y/o servicios ofrecidos dentro del mismo, significa que ha leído, entendido y se encuentra de acuerdo con los puntos y términos aquí descritos.
                    </p>
                    <br>
                    <p class="font-rob-light">
                        La información solicitada al comprador es exclusiva para la compra, seguimiento del pedido, registro del perfil en el histórico de compras.
                    </p>
                </li>
                <br>
                <br>
                <li>
                    <p class="font-rob-bold">HELPDESK DE SERVICIO AL CLIENTE</p>
                    <br>
                    <p class="font-rob-light">
                        Para cualquier aclaración o información adicional, favor de contactarnos al WhatsApp: <a href=" https://wa.me/+524426803196" target="_blank" class="font-rob-regular hover:underline">4426803196.</a> o al HelpDesk desde <a href="http://www.booktrain.com.mx" target="_blank" class="font-rob-regular hover:underline">www.booktrain.com.mx</a> , ingresando a la sección de SOPORTE para levantar un ticket de servicio.

                    </p>
                </li>
            </ol>
            <br>
        </div>
    </div>
@endsection

@section('js')
@endsection
