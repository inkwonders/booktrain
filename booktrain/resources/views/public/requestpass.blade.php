@extends('layouts.base')

@section('title')
    Booktrain | Solicitud de restablecimiento
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('/css/login.css') }}">
@endsection

@section('container')

<section class="md:table w-screen h-screen flex flex-col overflow-x-hidden overflow-y-auto">

    <div class="container mx-auto box-border px-10 md:px-20 w-full md:w-1/3 h-auto md:h-full md:table-cell">

        <section class="flex flex-col justify-between w-full h-full min-h-screen box-border pt-10 2xl:pt-20 pb-8">

            <div class="flex justify-center">

                <img class="logo-bk w-64 h-30" src="assets/img/svg/logo_principal.svg" alt="Logo principal">

            </div>

            <div class="w-full h-auto flex items-center pt-20 md:pt-0">

                <div class="w-full">

                    <div class="w-full px-4 text-center">

                        <h1 class="text-2xl font-rob-bold text-grayDarkBT">HEMOS RECIBIDO TU SOLICITUD DE RESTABLECIMIENTO DE TU CONTRASEÑA</h1>

                    </div>

                    <div class="w-full px-4 text-center my-9">

                        <span class="text-lg font-rob-light text-grayDarkBT leading-3">Favor de consultar tu <b>e-mail</b> para <b>comenzar con el restablecimiento</b> de tu contraseña.</span>

                    </div>


                    <div class="w-full px-4 text-center my-9">

                        <span class="text-lg font-rob-light text-grayDarkBT leading-3"> <b>¿No recibiste el correo?</b>  Haz clic en el siguiente enlace para <b>solicitarlo nuevamente.</b></span>

                    </div>


                    <a href="/recuperar" class="appearance-none rounded-none relative flex justify-center items-center text-center py-3 h-14 px-3 background-green--bk w-full my-5 rounded-md color-white--bk border border-solid hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm font-rob-bold">
                    SOLICITAR NUEVAMENTE
                    </a>

                </div>

            </div>

            <div class="md:border-b border-gray-400 w-full"></div>

        </section>

    </div>



    <div class="container w-full md:w-2/3 h-full md:img-bk px-4 flex items-end box-border relative md:overflow-hidden md:table-cell">

        <div class="w-full md:w-11/12 flex flex-column relative md:absolute bottom-0 flex flex-col">

            <div class="hidden md:block md:w-2/4">

                {{-- <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS DE MYTANG</h1> --}}
                <h1 class="color-white--bk font-rob-bold lg:text-lg md:text-base">BIENVENIDO A LA TIENDA ONLINE PARA COLEGIOS</h1>

                <span class="color-white--bk rob-light sm:text-sm md:text-base">Ingresa o regístrate para adquirir tu paquete de libros</span>

            </div>

            <div class="md:mt-5">

              <div class="border-b border-gray-400 w-full"></div>

              <div class="flex flex-col md:flex-row h-28 md:h-8 py-1 md:py-0 justify-around w-full opacity-80 text-center">

                <span class="color-gray--bk md:color-white--bk text-xs"><a class="cursor-pointer" href="aviso-de-privacidad/" target="_blank">Aviso de privacidad</a> / <a class="cursor-pointer" href="politicas/" target="_blank">Políticas de compra, entrega y devolución</a></span>

                <span class="color-gray--bk md:color-white--bk text-xs">© 2021 PROVESA EDICIONES S.A. DE C.V. Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro</span>

                <span class="color-gray--bk md:color-white--bk text-xs">Desarrollado por <b><a class="cursor-pointer" href="http://inkwonders.com" target="_blank">INK WONDERS</a></b></span>

              </div>

            </div>


        </div>

    </div>

</section>

@endsection

@section('js')

@endsection
