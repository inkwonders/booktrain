<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $ticketUsuario->id }}</p>
</div>

<!-- Usuario Id Field -->
<div class="col-sm-12">
    {!! Form::label('usuario_id', 'Usuario Id:') !!}
    <p>{{ $ticketUsuario->usuario_id }}</p>
</div>

<!-- Folio Field -->
<div class="col-sm-12">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{{ $ticketUsuario->folio }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $ticketUsuario->status }}</p>
</div>

<!-- Titulo Field -->
<div class="col-sm-12">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{{ $ticketUsuario->titulo }}</p>
</div>

<!-- Ticket Motivo Id Field -->
<div class="col-sm-12">
    {!! Form::label('ticket_motivo_id', 'Ticket Motivo Id:') !!}
    <p>{{ $ticketUsuario->ticket_motivo_id }}</p>
</div>

<!-- Descripcion Field -->
<div class="col-sm-12">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{{ $ticketUsuario->descripcion }}</p>
</div>

<!-- Calificacion Field -->
<div class="col-sm-12">
    {!! Form::label('calificacion', 'Calificacion:') !!}
    <p>{{ $ticketUsuario->calificacion }}</p>
</div>

<!-- Comentario Calificacion Field -->
<div class="col-sm-12">
    {!! Form::label('comentario_calificacion', 'Comentario Calificacion:') !!}
    <p>{{ $ticketUsuario->comentario_calificacion }}</p>
</div>

<!-- Fecha Calificacion Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_calificacion', 'Fecha Calificacion:') !!}
    <p>{{ $ticketUsuario->fecha_calificacion }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ticketUsuario->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ticketUsuario->updated_at }}</p>
</div>

