<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $detalleResumenPedido->id }}</p>
</div>

<!-- Resumen Pedidos Id Field -->
<div class="col-sm-12">
    {!! Form::label('resumen_pedidos_id', 'Resumen Pedidos Id:') !!}
    <p>{{ $detalleResumenPedido->resumen_pedidos_id }}</p>
</div>

<!-- Libro Id Field -->
<div class="col-sm-12">
    {!! Form::label('libro_id', 'Libro Id:') !!}
    <p>{{ $detalleResumenPedido->libro_id }}</p>
</div>

<!-- Precio Libro Field -->
<div class="col-sm-12">
    {!! Form::label('precio_libro', 'Precio Libro:') !!}
    <p>{{ $detalleResumenPedido->precio_libro }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $detalleResumenPedido->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $detalleResumenPedido->updated_at }}</p>
</div>

