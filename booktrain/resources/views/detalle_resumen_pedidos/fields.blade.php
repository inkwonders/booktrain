<!-- Resumen Pedidos Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resumen_pedidos_id', 'Resumen Pedidos Id:') !!}
    {!! Form::number('resumen_pedidos_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Libro Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libro_id', 'Libro Id:') !!}
    {!! Form::text('libro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Libro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('precio_libro', 'Precio Libro:') !!}
    {!! Form::number('precio_libro', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush