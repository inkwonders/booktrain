<style>
    * {
        box-sizing: border-box;
    }
    body {
        position: relative;
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
    }
    .padre {
        width: 100%;
        font-family: arial;
        display: block;
        text-align: center;
        align-items: center;
        justify-content: center;
        margin: 0;
    }
    .contenedor {
        width: 700px;
        background-color: #efefef;
        margin: 50px 0;
        padding: 17px;
        display: inline-block;
    }
    .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
        border: solid 1px black;
    }
    .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
    }
    .mensaje_sub_1 {
        width: 100%;
        font-size: 18px;
        width: 100%;
        text-align: center;
        color: #808080;
        font-weight: bold
    }
    .mensaje_sub_2 {
        width: 100%;
        font-size: 14px;
        color: #999999;
        text-align: center;
    }
    .mensaje_izquierda {
        width: 100%;
        font-size: 14px;
        color: #999999;
        text-align: left;
    }
    .mensaje_izquierda p {
        margin: 0;
    }
    .mensaje_derecha {
        width: 100%;
        font-size: 14px;
        color: #999999;
        text-align: right;
    }
    .mensaje_derecha p {
        margin: 2px;
    }
    .mensaje_sub_3{
        width: 80%;
        font-size: 14px;
        color: #999999;
        text-align: center;
        display: inline-block;
    }
    .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
    }
    .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
    }
    .imagen {
        width: auto;
        max-height: 200px;
    }
    .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
    }
    .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
    }
    .logo {
        max-height: 80px;
    }
    .titulo {
        font-size: 32px;
        text-transform: uppercase;
        color: #808080;
        font-weight: bold;
    }
    .titulo_verde {
        font-size: 30px;
        text-transform: uppercase;
        color: #80b74e;
        font-weight: bold;
    }
    .btn_success {
        position: relative;
        width: 100%;
        padding: 15px 90px;
        text-decoration: none;
        color: white;
        background-color: #81b84e;
        font-weight: bold;
        font-size: 20px;
        border: none;
        border-radius: 10px;
        cursor: pointer;
    }
    .texto_verde {
        color: #80b74e;
    }
    .texto_negro {
        color: #000;
    }
    .fecha_pedido {
        font-size: 14px;
    }
    .sub_t {
        font-size: 16px;
    }
    .medio_porciento {
        width: 50%;
    }
    .text-sm {
        font-size: 12px;
    }
    .text-md {
        font-size: 15px;
    }
    .w-full {
        width: 100%;
    }
</style>
