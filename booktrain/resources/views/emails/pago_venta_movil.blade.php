<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>PAGO REGISTRADO</title>
        @include('emails.includes.estilos')
    </head>
    <body>
        <div class="padre">
            <div class="contenedor">
                <div class="mensaje">
                    <div class="cont_image_logo">
                        <img class="logo" src="{{ $message->embed($img_logo) }}">
                    </div>
                    <div class="cont_txt">
                        <span class="titulo">PAGO EXITOSO</span>
                    </div>
                    <div class="cont_txt texto_verde ">
                        <span>PEDIDO</span>
                        <br>
                        <span class="titulo_verde">{{ $pedido->serie }}-{{ $pedido->folio }}</span>
                    </div>
                    <div class="cont_txt fecha_pedido">
                        <span><b>FECHA DE PEDIDO:</b> {{ date('d / m / Y', strtotime($pedido->created_at)) }}</span>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <table>
                        <tr>
                        <td class="medio_porciento" style="padding: %;padding-right: 70px;">
                        <b class="sub_t">INFORMACIÓN DEL PEDIDO</b>
                        <br>
                        <br>
                        <p><b>Estado de pago:</b> {{ $pedido->status }}</p>
                        <p><b>Método de pago:</b> {{ $pedido->metodo_pago }}</p>
                        <p><b>Forma de pago:</b> {{ $pedido->forma_pago }}</p>
                        <p><b>Correo para receppciónde licencias digitales:</b> {{ !empty($pedido->correo_contacto)? $pedido->correo_contacto : "--" }}</p>
                    </td>
                    <td class="medio_porciento">
                            <b class="sub_t">INFORMACIÓN DE CONTACTO</b>
                            <br>
                            <br>
                            <p><b>Nombre de contacto:</b> {{ $pedido->nombre_contacto }} {{ $pedido->apellidos_contacto }}</p>
                            <p><b>Teléfono:</b> {{ $pedido->celular_contacto }}</p>
                            <p><b>Correo:</b> {{ $pedido->email_pedido }}</p>
                        </td>

                    </tr>
                </table>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    @foreach ($pedido->resumenes as $resumenes)
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">DETALLES DEL PEDIDO / {{ $resumenes->nombre_alumno}} {{ $resumenes->paterno_alumno}} {{ $resumenes->materno_alumno}}</b>
                        <br>
                        <b class="text-sm">ESCUELA: {{ $resumenes->pedido->colegio->nombre}} GRADO:  {{ $resumenes->paquete->nivelColegio->nombre }} - {{ $resumenes->paquete->nivelColegio->seccion->nombre }}</b>
                        <br>
                        <br>
                        <table class="w-full text-sm" style="font-size: 11px">
                            @foreach ($resumenes->detallesResumenPedidos as $resumenes_libros)
                            <tr>
                                <td>
                                    <b>{{$resumenes_libros->cantidad}} PZ</b>
                                </td>
                                <td>
                                    <p>Libro: {{ $resumenes_libros->libro->nombre}})</p>
                                </td>
                                <td>
                                    <b>ISBN: {{ $resumenes_libros->libro->isbn}}</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md mensaje_derecha">${{number_format($resumenes_libros->precio_libro, 2)}} MXN</p>
                                </td>
                            </tr>
                            @endforeach


                        </table>
                    </div>

                    <br>
                    <div class="linea_h" style="background-color: #00000066;height: 0.1px;"></div>
                    <br>

                    @if($pedido->resumenes->count() == 1)

                    @else
                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">

                                <tr>
                                    <td>
                                        <b>SUBTOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{  number_format($resumenes->subtotal, 2)}} MXN</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="linea_h" style="background-color: #00000066;height: 0.1px;"></div>
                    @endif

                    <br>
                    @endforeach
                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">

                                <tr>
                                    <td>
                                        <b>SUBTOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{  number_format($pedido->subtotal, 2)}} MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>COMISIÓN DE PAGO</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{  number_format($pedido->comision, 2)}} MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>TOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_negro">${{  number_format($pedido->total, 2)}} MXN</p>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
