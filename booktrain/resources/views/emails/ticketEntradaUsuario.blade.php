<html>
    <head>
      <meta charset='utf-8'>
      <title>Solicitud de restablecimiento de contraseña</title>
      <style>
      * {
        box-sizing: border-box;
      }
      body {
        position: relative;
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
      }
      .padre {
        width: 100%;
        font-family: arial;
        display: block;
        text-align: center;
        align-items: center;
        justify-content: center;
        margin: 0;
      }
      .contenedor {
        width: 600px;
        background-color: #efefef;
        margin-top: 50px;
        padding: 17px;
        display: inline-block;
      }
      .mensaje {
        width: 100%;
        background-color: #fff;
        display: inline-block;
        margin: 0;
        padding: 14px;
        border: solid 1px black;
      }
      .linea_h {
        width: 100%;
        height: 2px;
        background-color: #ebebeb;
      }
      .mensaje_sub_1 {
        width: 100%;
        font-size: 18px;
        width: 100%;
        text-align: center;
        color: #808080;
        font-weight: bold
      }
      .mensaje_sub_2 {
        width: 100%;
        font-size: 14px;
        color: #999999;
        text-align: center;
      }
      .mensaje_sub_3{
        width: 80%;
        font-size: 14px;
        color: #999999;
        text-align: center;
        display: inline-block;
      }
      .cont_boton {
        width: 100%;
        display: inline-block;
        text-align: center;
        align-items: center;
        justify-content: center;
        padding: 25px 0 14px 0;
      }
      .cont_image {
        width: 100%;
        display: inline-block;
        padding-top: 25px;
        text-align: center;
      }
      .imagen {
        width: auto;
        max-height: 200px;
      }
      .cont_txt {
        padding: 30px 0 16px 0;
        color: #808080;
      }
      .cont_image_logo {
        width: 100%;
        display: inline-block;
        padding: 20px 0 34px 0;
        text-align: center;
      }
      .logo {
        max-height: 80px;
      }
      .titulo{
        font-size: 22px;
        text-transform: uppercase;
        color: #808080;
        font-weight: bold;
      }
      .btn_success{
        position: relative;
        width: 100%;
        padding: 15px 90px;
        text-decoration: none;
        color: white;
        background-color: #81b84e;
        font-weight: bold;
        font-size: 20px;
        border: none;
        border-radius: 10px;
        cursor: pointer;
      }
      .texto{
        font-size: 16px;
        color: #808080;
        font-weight: bold;
      }
      </style>
    </head>
    <body>
      <div class='padre'>
        <div class='contenedor'>
          <div class='mensaje'>
            <div class='cont_image_logo'>
              {{-- <img class='logo' src='http://booktrain.local/assets/img/logo.png'> --}}
              <img class="logo" src="<?php echo $message->embed($img_logo)?>">
            </div>
            <br>
            <br>
            <div class='cont_txt'>
                <span class='titulo'>SE HA CREADO CON ÉXITO TU <br>RESPUESTA EN EL TICKET <br>{{ $ticket->ticket->id }}</span>
            </div>
            <div class='cont_txt'>
                <span class='titulo'>" {{ $ticket->ticket->titulo }} "</span>
            </div>
            <br>
            <div class='cont_image'>
              {{-- <img class='imagen' src='http://booktrain.local/assets/img/mensaje.png'> --}}
              <img class="logo" src="<?php echo $message->embed($img_url)?>">
            </div>
            <br>
            <br>
            <br>
            <div class='cont_txt'>
                <span class='texto'>{{$ticket->comentario}}</span>
            </div>
            <div class="linea_h"></div>
            <br>
            <br>
            <div class='mensaje_sub_2'>
                Su ticket será atendido dentro de las próximas 24 hrs, porfavor de estar <br> atento a su correo.
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </body>
    </html>
