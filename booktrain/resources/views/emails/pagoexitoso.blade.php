<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Solicitud de restablecimiento de contraseña</title>
        <style>
            * {
                box-sizing: border-box;
            }
            body {
                position: relative;
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
            }
            .padre {
                width: 100%;
                font-family: arial;
                display: block;
                text-align: center;
                align-items: center;
                justify-content: center;
                margin: 0;
            }
            .contenedor {
                width: 700px;
                background-color: #efefef;
                margin: 50px 0;
                padding: 17px;
                display: inline-block;
            }
            .mensaje {
                width: 100%;
                background-color: #fff;
                display: inline-block;
                margin: 0;
                padding: 14px;
                border: solid 1px black;
            }
            .linea_h {
                width: 100%;
                height: 2px;
                background-color: #ebebeb;
            }
            .mensaje_sub_1 {
                width: 100%;
                font-size: 18px;
                width: 100%;
                text-align: center;
                color: #808080;
                font-weight: bold
            }
            .mensaje_sub_2 {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: center;
            }
            .mensaje_izquierda {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: left;
            }
            .mensaje_izquierda p {
                margin: 0;
            }
            .mensaje_derecha {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: right;
            }
            .mensaje_derecha p {
                margin: 2px;
            }
            .mensaje_sub_3{
                width: 80%;
                font-size: 14px;
                color: #999999;
                text-align: center;
                display: inline-block;
            }
            .cont_boton {
                width: 100%;
                display: inline-block;
                text-align: center;
                align-items: center;
                justify-content: center;
                padding: 25px 0 14px 0;
            }
            .cont_image {
                width: 100%;
                display: inline-block;
                padding-top: 25px;
                text-align: center;
            }
            .imagen {
                width: auto;
                max-height: 200px;
            }
            .cont_txt {
                padding: 30px 0 16px 0;
                color: #808080;
            }
            .cont_image_logo {
                width: 100%;
                display: inline-block;
                padding: 20px 0 34px 0;
                text-align: center;
            }
            .logo {
                max-height: 80px;
            }
            .titulo {
                font-size: 22px;
                text-transform: uppercase;
                color: #808080;
                font-weight: bold;
            }
            .titulo_verde {
                font-size: 22px;
                text-transform: uppercase;
                color: #80b74e;
                font-weight: bold;
            }
            .btn_success {
                position: relative;
                width: 100%;
                padding: 15px 90px;
                text-decoration: none;
                color: white;
                background-color: #81b84e;
                font-weight: bold;
                font-size: 20px;
                border: none;
                border-radius: 10px;
                cursor: pointer;
            }
            .texto_verde {
                color: #80b74e;
            }
            .texto_negro {
                color: #000;
            }
            .fecha_pedido {
                font-size: 14px;
            }
            .sub_t {
                font-size: 15px;
            }
            .medio_porciento {
                width: 50%;
            }
            .text-sm {
                font-size: 11px;
            }
            .text-md {
                font-size: 14px;
            }
            .w-full {
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="padre">
            <div class="contenedor">
                <div class="mensaje">
                    <div class="cont_image_logo">
                        <img class="logo" src="http://booktrain.test/assets/img/logo.png">
                    </div>
                    <div class="cont_txt">
                        <span class="titulo">PAGO EXITOSO</span>
                    </div>
                    <div class="cont_txt texto_verde">
                        <span>PEDIDO</span>
                        <br>
                        <span class="titulo_verde">PD-2929</span>
                    </div>
                    <div class="cont_txt fecha_pedido">
                        <span><b>FECHA DE PEDIDO:</b> 18 / 01 / 2021</span>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">INFORMACIÓN DEL PEDIDO</b>
                        <br>
                        <br>
                        <p><b>Estado de pago:</b> En proceso</p>
                        <p><b>Método de pago:</b> 06 meses diferidos</p>
                        <p><b>Forma de pago:</b> Tarjeta de crédito</p>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <table>
                            <tr>

                                <td class="medio_porciento">
                                    <b class="sub_t">DATOS DE FACTURACIÓN</b>
                                    <br>
                                    <br>
                                    <p><b>Nombre o razón social:</b> Ramon Salmón</p>
                                    <p><b>RFC:</b> ADA0020210118QZ00y</p>
                                    <p><b>Correo:</b> rsalmon@provesa.mx</p>
                                    <p><b>Uso de CFDI:</b> 02051981651</p>
                                </td>

                                <td class="medio_porciento">
                                    <b class="sub_t">DATOS DE ENVÍO</b>
                                    <br>
                                    <br>
                                    <p>Calle vicente guerrero #13 Col. Centro C.P. 76000, Quéretaro, qro., (Entre la av. Zaragoza y la calle Arteaga)</p>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">DETALLES DEL PEDIDO / RAMON SALMÓN</b>
                        <br>
                        <b class="text-sm">ESCUELA: COLEGIO DEMO GRADO: SECUNDARIA</b>
                        <br>
                        <br>
                        <table class="text-sm w-full">

                            <tr>
                                <td>
                                    <b>1 PZ</b>
                                </td>
                                <td>
                                    <p>NUEVO ATLAS UNIVERSAL Y DE MÉXICO(COMPLEMENTO ADICIONAL)</p>
                                </td>
                                <td>
                                    <b>ISBN: 9992292929292</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md">$280.00 MXN</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b>2 PZ</b>
                                </td>
                                <td>
                                    <p>NUEVO ATLAS UNIVERSAL Y DE MÉXICO(COMPLEMENTO ADICIONAL)</p>
                                </td>
                                <td>
                                    <b>ISBN: 9992292929292</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md">$280.00 MXN</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b>4 PZ</b>
                                </td>
                                <td>
                                    <p>NUEVO ATLAS UNIVERSAL Y DE MÉXICO(COMPLEMENTO ADICIONAL)</p>
                                </td>
                                <td>
                                    <b>ISBN: 9992292929292</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md">$280.00 MXN</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <b>1 PZ</b>
                                </td>
                                <td>
                                    <p>NUEVO ATLAS UNIVERSAL Y DE MÉXICO(COMPLEMENTO ADICIONAL)</p>
                                </td>
                                <td>
                                    <b>ISBN: 9992292929292</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md">$280.00 MXN</p>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">

                                <tr>
                                    <td>
                                        <b>SUBTOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">$1,120.00 MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>ENVÍO</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">$160.00 MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>COMISIÓN DE PAGO</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">$51.20 MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>TOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_negro">$1,331.20 MXN</p>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>