<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Gracias por su compra</title>
        <style>
            * {
                box-sizing: border-box;
            }
            body {
                position: relative;
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
            }
            .padre {
                width: 100%;
                font-family: arial;
                display: block;
                text-align: center;
                align-items: center;
                justify-content: center;
                margin: 0;
            }
            .contenedor {
                width: 700px;
                background-color: #efefef;
                margin: 50px 0;
                padding: 17px;
                display: inline-block;
            }
            .mensaje {
                width: 100%;
                background-color: #fff;
                display: inline-block;
                margin: 0;
                padding: 14px;
                border: solid 1px black;
            }
            .linea_h {
                width: 100%;
                height: 2px;
                background-color: #ebebeb;
            }
            .mensaje_sub_1 {
                width: 100%;
                font-size: 18px;
                width: 100%;
                text-align: center;
                color: #808080;
                font-weight: bold
            }
            .mensaje_sub_2 {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: center;
            }
            .mensaje_izquierda {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: left;
            }
            .mensaje_izquierda p {
                margin: 0;
            }
            .mensaje_derecha {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: right;
            }
            .mensaje_derecha p {
                margin: 2px;
            }
            .mensaje_sub_3{
                width: 80%;
                font-size: 14px;
                color: #999999;
                text-align: center;
                display: inline-block;
            }
            .cont_boton {
                width: 100%;
                display: inline-block;
                text-align: center;
                align-items: center;
                justify-content: center;
                padding: 25px 0 14px 0;
            }
            .cont_image {
                width: 100%;
                display: inline-block;
                padding-top: 25px;
                text-align: center;
            }
            .imagen {
                width: auto;
                max-height: 200px;
            }
            .cont_txt {
                padding: 30px 0 16px 0;
                color: #808080;
            }
            .cont_image_logo {
                width: 100%;
                display: inline-block;
                padding: 20px 0 34px 0;
                text-align: center;
            }
            .logo {
                max-height: 80px;
            }
            .titulo {
                font-size: 22px;
                text-transform: uppercase;
                color: #808080;
                font-weight: bold;
            }
            .titulo_verde {
                font-size: 22px;
                text-transform: uppercase;
                color: #80b74e;
                font-weight: bold;
            }
            .btn_success {
                position: relative;
                width: 100%;
                padding: 15px 90px;
                text-decoration: none;
                color: white;
                background-color: #81b84e;
                font-weight: bold;
                font-size: 20px;
                border: none;
                border-radius: 10px;
                cursor: pointer;
            }
            .texto_verde {
                color: #80b74e;
            }
            .texto_negro {
                color: #000;
            }
            .fecha_pedido {
                font-size: 14px;
            }
            .sub_t {
                font-size: 15px;
            }
            .medio_porciento {
                width: 50%;
            }
            .text-sm {
                font-size: 11px;
            }
            .text-md {
                font-size: 14px;
            }
            .w-full {
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="padre">
            <div class="contenedor">
                <div class="mensaje">
                    <div class="cont_image_logo">
                        {{-- <img class="logo" src="<?php echo $message->embed($img_logo)?>"> --}}
                    </div>
                    <div class="cont_txt">
                        <span class="titulo">¡GRACIAS POR SU COMPRA!</span>
                    </div>
                    <div class="cont_txt texto_verde">
                        <span>PEDIDO</span>
                        <br>
                        <span class="titulo_verde">{{$carrito->serie}}-{{ $carrito->folio}}</span>
                    </div>
                    <div class="cont_txt fecha_pedido">
                        <span><b>FECHA DE PEDIDO:</b> {{ date('d / m / Y', strtotime($carrito->created_at))}}</span>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">INFORMACIÓN DEL PEDIDO</b>
                        <br>
                        <br>
                        <p><b>Estado de pago:</b> {{ $carrito->status}}</p>
                        <p><b>Método de pago:</b> {{ $carrito->metodo_pago}}</p>
                        <p><b>Forma de pago:</b> {{ $carrito->forma_pago}}</p>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <table>
                            <tr>


                                <td class="medio_porciento">
                                    {{-- @if($pedido->envio_id == null || $pedido->envio_id == "")
                                    <b class="sub_t">RECOGER EN:</b>
                                    @elseif($pedido->envio_id != "" )
                                    <b class="sub_t">DATOS DE ENVÍO</b>
                                     @endif --}}
                                     @if($pedido->tipo_entrega == 1)
                                    <b class="sub_t">ENTREGA EN COLEGIO:</b>
                                    @elseif($pedido->tipo_entrega == 2 )
                                    <b class="sub_t">ENTREGA EN DOMICILIO:</b>
                                    @elseif($pedido->tipo_entrega == 3 )
                                    <b class="sub_t">ENTREGA PICK-UP:</b>
                                     @endif

                                 <br>
                                 <br>
                                  Calle {{ $pedido->direccion->calle}}, No. {{ $pedido->direccion->no_exterior}}, No. Ext {{ $pedido->direccion->no_interior}},
                                 Col. {{ $pedido->direccion->colonia}}, CP. {{ $pedido->direccion->cp}},  {{ $pedido->direccion->municipio->descripcion}}, {{ $pedido->direccion->estado->descripcion}}
                                 (Referencias: {{ $pedido->direccion->referencia}})
                             </td>

                                <td class="medio_porciento">
                                    @if($carrito->datosFactura == "" || $carrito->datosFactura == null)

                                    @else
                                    <b class="sub_t">DATOS DE FACTURACIÓN</b>
                                    <br>
                                    <br>
                                    <p><b>Nombre o razón social: </b>{{ $carrito->datosFactura->razon_social}}</p>
                                    <p><b>RFC:</b> {{ $carrito->datosFactura->rfc}}</p>
                                    <p><b>Correo:</b>{{ $carrito->datosFactura->correo}}</p>
                                    <p><b>Uso de CFDI:</b> {{ $carrito->datosFactura->cfdis->descripcion}}</p>
                                    <p><b>Calle:</b> {{ $pedido->datosFactura->calle}}</p>
                                    <p><b>Num. exterior:</b> {{ $pedido->datosFactura->num_exterior}}</p>
                                    <p><b>Num. interior:</b> {{ $pedido->datosFactura->num_interior}}</p>
                                    <p><b>Colonia:</b> {{ $pedido->datosFactura->colonia}}</p>
                                    <p><b>Municipio:</b> {{ $pedido->datosFactura->municipio}}</p>
                                    <p><b>Estado:</b> {{ $pedido->datosFactura->estado}}</p>
                                    <p><b>Codigo postal:</b> {{ $pedido->datosFactura->cp}}</p>
                                    @endif

                                </td>

                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    @foreach ($carrito->resumenes as $resumenes)
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">DETALLES DEL PEDIDO /  </b>{{ $resumenes->nombre_alumno}} {{ $resumenes->paterno_alumno}} {{ $resumenes->materno_alumno}}</b>
                        <br>
                        <b class="text-sm">ESCUELA: {{ $resumenes->pedido->colegio->nombre}} GRADO: </b>{{ $resumenes->paquete->nombre}}</b>
                        <br>
                        <br>
                        <table class="w-full text-sm">
                            @foreach ($resumenes->detallesResumenPedidos as $resumenes_libros)
                            <tr>
                                <td>
                                    <b>{{$resumenes_libros->cantidad}} PZ</b>
                                </td>
                                <td>
                                    <p>Libro: {{ $resumenes_libros->libro->nombre}})</p>
                                </td>
                                <td>
                                    <b>ISBN: {{ $resumenes_libros->libro->isbn}}</b>
                                </td>
                                <td>
                                    <p class="texto_verde text-md">${{$resumenes_libros->precio_libro}} MXN</p>
                                </td>
                            </tr>
                            @endforeach


                        </table>
                    </div>

                    <br>
                    <div class="linea_h"></div>
                    <br>

                    @if($carrito->resumenes->count() == 1)

                    @else
                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">

                                <tr>
                                    <td>
                                        <b>SUBTOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{  $resumenes->subtotal}} MXN</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    @endif




                    <br>
                    @endforeach
                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">

                                <tr>
                                    <td>
                                        <b>SUBTOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{ $carrito->subtotal}} MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>ENVÍO</b>
                                    </td>
                                    <td>
                                        @if($carrito->envio == "" || $carrito->envio == null )
                                        <p class="texto_verde">$0.00 MXN</p>
                                        @elseif ($carrito->tipo_entrega == 2)
                                        <p class="texto_verde">${{  number_format($carrito->envio->costo, 2)}} MXN</p>
                                        @else
                                        <p class="texto_verde">$0.00 MXN</p>
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>COMISIÓN DE PAGO</b>
                                    </td>
                                    <td>
                                        <p class="texto_verde">${{ $carrito->comision}} MXN</p>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>TOTAL</b>
                                    </td>
                                    <td>
                                        <p class="texto_negro">${{ $carrito->total}} MXN</p>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
