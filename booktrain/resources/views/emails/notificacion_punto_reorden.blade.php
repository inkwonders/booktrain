<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>PAGO REGISTRADO</title>
        <style>
            * {
                box-sizing: border-box;
            }
            body {
                position: relative;
                padding: 0;
                margin: 0;
                width: 100%;
                height: 100%;
            }
            .padre {
                width: 100%;
                font-family: arial;
                display: block;
                text-align: center;
                align-items: center;
                justify-content: center;
                margin: 0;
            }
            .contenedor {
                width: 840px;
                background-color: #efefef;
                margin: 50px 0;
                padding: 17px;
                display: inline-block;
            }
            .mensaje {
                width: 100%;
                background-color: #fff;
                display: inline-block;
                margin: 0;
                padding: 14px;
                border: solid 1px black;
            }
            .linea_h {
                width: 100%;
                height: 2px;
                background-color: #ebebeb;
            }
            .mensaje_sub_1 {
                width: 100%;
                font-size: 18px;
                width: 100%;
                text-align: center;
                color: #808080;
                font-weight: bold
            }
            .mensaje_sub_2 {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: center;
            }
            .mensaje_izquierda {
                width: 100%;
                font-size: 14px;
                color: #999999;
                /* text-align: left; */
            }
            .mensaje_izquierda p {
                margin: 0;
            }
            .mensaje_derecha {
                width: 100%;
                font-size: 14px;
                color: #999999;
                text-align: right;
            }
            .mensaje_derecha p {
                margin: 2px;
            }
            .mensaje_sub_3{
                width: 80%;
                font-size: 14px;
                color: #999999;
                text-align: center;
                display: inline-block;
            }
            .cont_boton {
                width: 100%;
                display: inline-block;
                text-align: center;
                align-items: center;
                justify-content: center;
                padding: 25px 0 14px 0;
            }
            .cont_image {
                width: 100%;
                display: inline-block;
                padding-top: 25px;
                text-align: center;
            }
            .imagen {
                width: auto;
                max-height: 200px;
            }
            .cont_txt {
                padding: 30px 0 16px 0;
                color: #808080;
            }
            .cont_image_logo {
                width: 100%;
                display: inline-block;
                padding: 20px 0 34px 0;
                text-align: center;
            }
            .logo {
                max-height: 80px;
            }
            .titulo {
                font-size: 32px;
                text-transform: uppercase;
                color: #808080;
                font-weight: bold;
            }
            .titulo_verde {
                font-size: 25px;
                text-transform: uppercase;
                color: #80b74e;
                font-weight: bold;
            }
            .btn_success {
                position: relative;
                width: 100%;
                padding: 15px 90px;
                text-decoration: none;
                color: white;
                background-color: #81b84e;
                font-weight: bold;
                font-size: 20px;
                border: none;
                border-radius: 10px;
                cursor: pointer;
            }
            .texto_verde {
                color: #80b74e;
            }
            .texto_negro {
                color: #000;
            }
            .fecha_pedido {
                font-size: 14px;
            }
            .sub_t {
                font-size: 16px;
            }
            .medio_porciento {
                width: 50%;
            }
            .text-sm {
                font-size: 12px;
            }
            .text-md {
                font-size: 15px;
            }
            .w-full {
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="padre">
            <div class="contenedor">
                <div class="mensaje">
                    <div class="cont_image_logo">
                    </div>
                    <div class="cont_txt">
                        <span class="titulo_verde">Libros que han alcanzado su punto de reorden ({{ $libros->count() }} libro(s) )</span>
                    </div>

                    <div class="cont_txt fecha_pedido">
                        <span><b  class="texto_verde">FECHA DE REPORTE:</b><br><br><b>{{ $fecha }}</b></span>
                    </div>

                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">

                        <table class="w-full text-sm" style="font-size: 13px">
                            <thead>
                                <tr>
                                    <td class="texto_verde"><b>#</b></td>
                                    <td class="texto_verde"><b>Colegio</b></td>
                                    <td class="texto_verde"><b>ISBN</b></td>
                                    <td class="texto_verde"><b>Título</b></td>
                                    <td class="texto_verde"><b>Stock</b></td>
                                    <td class="texto_verde"><b>Punto de reorden</b></td>
                                    <td class="texto_verde"><b>Disponible bajo pedido</b></td>
                                    <td class="texto_verde"><b>Última venta</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($libros as $libro)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $libro->colegio }}</td>
                                    <td>{{ $libro->isbn }}</td>
                                    <td>{{ $libro->nombre }}</td>
                                    <td>{{ $libro->stock }}</td>
                                    <td>{{ $libro->punto_reorden }}</td>
                                    <td>{{ $libro->bajo_pedido ? 'Si' : 'No' }}</td>
                                    <td>{{ $libro->ultima_venta }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <p class="texto_verde"><b>No hay libros que hayan alcanzado su punto de reorden</b></p>
                                </tr>
                                @endforelse
                            </tbody>


                        </table>
                    </div>

                    <br>
                    <div class="linea_h" style="background-color: #00000066;height: 0.1px;"></div>
                    <br>

                </div>
            </div>
        </div>
    </body>
