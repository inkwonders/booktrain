<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>PEDIDO REALIZADO</title>
        @include('emails.includes.estilos')
    </head>
    <body>
        <div class="padre">
            <div class="contenedor">
                <div class="mensaje">
                    <div class="cont_image_logo"><img class="logo" src="{{ $message->embed($logo) }}"></div>
                    <div class="cont_txt"><span class="titulo">SU PEDIDO VA EN CAMINO</span></div><br>
                    <br>
                    @if($entrega_paqueteria)
                    <div class="cont_txt">
                        <span style="font-size: 22px;">NÚM. GUÍA</span><br>
                        <span class="titulo">{{ $no_guia }}</span><br>
                    </div>
                    <br>
                    <div style="display: inline-block;">
                        <a target="_blank" href='{{ $url_paqueteria }}' style="text-decoration: none;"><div class="btn_success">RASTREAR ENVÍO</div></a>
                    </div>
                    @else
                    <div class="cont_txt"><span style="font-size: 22px;">Para obtener su número de guía por favor contacte al administrador levantando un ticket de soporte, gracias.</span></div><br>
                    @endif
                    <br>
                    <br>
                    <div class="linea_h"></div>
                    <div class="cont_txt"><span class="titulo">INFORMACIÓN DEL PEDIDO</span></div>
                    <div class="cont_txt texto_verde ">
                        <span>PEDIDO</span><br>
                        <span class="titulo_verde">{{ $serie }}-{{ $folio }}</span>
                    </div>
                    <div class="cont_txt fecha_pedido">
                        <span><b>FECHA DE PEDIDO:</b> {{ $fecha }}</span>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <table>
                            <tr>
                                <td class="medio_porciento" style="padding: %;padding-right: 70px;">
                                    <b class="sub_t">INFORMACIÓN DEL PEDIDO</b><br>
                                    <br>
                                    <p><b>Estado de pago:</b> {{ $estado_pago }}</p>
                                    <p><b>Método de pago:</b> {{ $metodo_pago }}</p>
                                    <p><b>Forma de pago:</b> {{ $forma_pago }}</p>
                                </td>
                                <td class="medio_porciento">
                                    <b class="sub_t">INFORMACIÓN DE CONTACTO</b><br>
                                    <br>
                                    <p><b>Nombre de contacto:</b> {{ $nombre_contacto }}</p>
                                    <p><b>Teléfono:</b> {{ $telefono_contacto }}</p>
                                    <p><b>Correo:</b> {{ $correo }}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <div class="linea_h"></div>
                    <br>
                    <div class="mensaje_izquierda">
                        <table>
                            <tr>
                                <td class="medio_porciento">
                                    <b class="sub_t">{{ $descripcion_tipo_entrega }}</b><br>
                                    <br>
                                    Calle {{ $direccion->calle }}, No. {{ $direccion->no_exterior }}, No. Ext {{ $direccion->no_interior }},
                                    Col. {{ $direccion->colonia }}, CP. {{ $direccion->cp }},  {{ $direccion->municipio->descripcion }}, {{ $direccion->estado->descripcion }}
                                    (Referencias: {{ $direccion->referencia }})
                                </td>

                                <td class="medio_porciento">
                                    @if($solicito_facturacion)
                                    <b class="sub_t">DATOS DE FACTURACIÓN</b><br>
                                    <br>
                                    <p><b>Nombre o razón social: </b> {{ $datos_facturacion->razon_social }}</b></p>
                                    <p><b>RFC:</b> {{ $datos_facturacion->rfc }}</b></p>
                                    <p><b>Correo:</b> {{ $datos_facturacion->correo }}</b></p>
                                    <p><b>Uso de CFDI:</b> {{ $datos_facturacion->uso_cfdi }}</b></p>
                                    <p><b>Calle:</b> {{ $pedido->datosFactura->calle}}</p>
                                    <p><b>Num. exterior:</b> {{ $pedido->datosFactura->num_exterior}}</p>
                                    <p><b>Num. interior:</b> {{ $pedido->datosFactura->num_interior}}</p>
                                    <p><b>Colonia:</b> {{ $pedido->datosFactura->colonia}}</p>
                                    <p><b>Municipio:</b> {{ $pedido->datosFactura->municipio}}</p>
                                    <p><b>Estado:</b> {{ $pedido->datosFactura->estado}}</p>
                                    <p><b>Codigo postal:</b> {{ $pedido->datosFactura->cp}}</p>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>

                    <div class="linea_h"></div>
                    @foreach ($resumenes as $resumen)
                    <br>
                    <div class="mensaje_izquierda">
                        <b class="sub_t">DETALLES DEL PEDIDO / {{ $resumen->nombre }} {{ $resumen->apellido_paterno }} {{ $resumen->apellido_materno }}</b><br>
                        <b class="text-sm">ESCUELA: {{ $resumen->escuela }} GRADO:{{ $resumen->grado }}</b><br>
                        <br>
                        <table class="w-full text-sm" style="font-size: 11px">
                            @foreach ($resumen->detalles as $detalle)
                            <tr>
                                <td><b>{{ $detalle->cantidad }} PZ</b></td>
                                <td><p>Libro: {{ $detalle->libro }})</p></td>
                                <td><b>ISBN: {{ $detalle->isbn }}</b></td>
                                <td><p class="texto_verde text-md mensaje_derecha">${{ $detalle->precio }} MXN</p></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @endforeach

                    <br>
                    <div class="linea_h" style="background-color: #00000066;height: 0.1px;"></div><br>

                    <div class="mensaje_derecha">
                        <div class="medio_porciento" style="display: inline-block;">
                            <table class="w-full">
                                <tr>
                                    <td><b>SUBTOTAL</b></td>
                                    <td><p class="texto_verde">${{ $subtotal }} MXN</p></td>
                                </tr>
                                <tr>
                                    <td><b>ENVÍO</b></td>
                                    <td><p class="texto_verde">${{ $costo_envio }} MXN</p></td>
                                </tr>
                                <tr>
                                    <td><b>COMISIÓN DE PAGO</b></td>
                                    <td><p class="texto_verde">${{ $comision }} MXN</p></td>
                                </tr>
                                <tr>
                                    <td><b>TOTAL</b></td>
                                    <td><p class="texto_negro">${{ $total }} MXN</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
