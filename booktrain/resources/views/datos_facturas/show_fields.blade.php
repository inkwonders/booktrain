<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $datosFacturas->id }}</p>
</div>

<!-- Pedido Id Field -->
<div class="col-sm-12">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    <p>{{ $datosFacturas->pedido_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $datosFacturas->user_id }}</p>
</div>

<!-- Razon Social Field -->
<div class="col-sm-12">
    {!! Form::label('razon_social', 'Razon Social:') !!}
    <p>{{ $datosFacturas->razon_social }}</p>
</div>

<!-- Rfc Field -->
<div class="col-sm-12">
    {!! Form::label('rfc', 'Rfc:') !!}
    <p>{{ $datosFacturas->rfc }}</p>
</div>

<!-- Correo Field -->
<div class="col-sm-12">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{{ $datosFacturas->correo }}</p>
</div>

<!-- Id Cfdi Field -->
<div class="col-sm-12">
    {!! Form::label('id_cfdi', 'Id Cfdi:') !!}
    <p>{{ $datosFacturas->id_cfdi }}</p>
</div>

<!-- Cp Field -->
<div class="col-sm-12">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{{ $datosFacturas->cp }}</p>
</div>

<!-- Metodo Pago Id Field -->
<div class="col-sm-12">
    {!! Form::label('metodo_pago_id', 'Metodo Pago Id:') !!}
    <p>{{ $datosFacturas->metodo_pago_id }}</p>
</div>

<!-- Forma Pago Id Field -->
<div class="col-sm-12">
    {!! Form::label('forma_pago_id', 'Forma Pago Id:') !!}
    <p>{{ $datosFacturas->forma_pago_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $datosFacturas->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $datosFacturas->updated_at }}</p>
</div>

