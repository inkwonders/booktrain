<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $pedido->id }}</p>
</div>

<!-- Folio Field -->
<div class="col-sm-12">
    {!! Form::label('folio', 'Folio:') !!}
    <p>{{ $pedido->folio }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $pedido->status }}</p>
</div>

<!-- Usuario Id Field -->
<div class="col-sm-12">
    {!! Form::label('usuario_id', 'Usuario Id:') !!}
    <p>{{ $pedido->usuario_id }}</p>
</div>

<!-- Colegio Id Field -->
<div class="col-sm-12">
    {!! Form::label('colegio_id', 'Colegio Id:') !!}
    <p>{{ $pedido->colegio_id }}</p>
</div>

<!-- Nombre Contacto Field -->
<div class="col-sm-12">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    <p>{{ $pedido->nombre_contacto }}</p>
</div>

<!-- Apellidos Contacto Field -->
<div class="col-sm-12">
    {!! Form::label('apellidos_contacto', 'Apellidos Contacto:') !!}
    <p>{{ $pedido->apellidos_contacto }}</p>
</div>

<!-- Celular Contacto Field -->
<div class="col-sm-12">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    <p>{{ $pedido->celular_contacto }}</p>
</div>

<!-- Tipo Entrega Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_entrega', 'Tipo Entrega:') !!}
    <p>{{ $pedido->tipo_entrega }}</p>
</div>

<!-- Factura Field -->
<div class="col-sm-12">
    {!! Form::label('factura', 'Factura:') !!}
    <p>{{ $pedido->factura }}</p>
</div>

<!-- Metodo Pago Id Field -->
<div class="col-sm-12">
    {!! Form::label('metodo_pago_id', 'Metodo Pago Id:') !!}
    <p>{{ $pedido->metodo_pago_id }}</p>
</div>

<!-- Forma Pago Id Field -->
<div class="col-sm-12">
    {!! Form::label('forma_pago_id', 'Forma Pago Id:') !!}
    <p>{{ $pedido->forma_pago_id }}</p>
</div>

<!-- Terminos Condiciones Field -->
<div class="col-sm-12">
    {!! Form::label('terminos_condiciones', 'Terminos Condiciones:') !!}
    <p>{{ $pedido->terminos_condiciones }}</p>
</div>

<!-- Subtotal Field -->
<div class="col-sm-12">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{{ $pedido->subtotal }}</p>
</div>

<!-- Comision Field -->
<div class="col-sm-12">
    {!! Form::label('comision', 'Comision:') !!}
    <p>{{ $pedido->comision }}</p>
</div>

<!-- Total Field -->
<div class="col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $pedido->total }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $pedido->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $pedido->updated_at }}</p>
</div>

