<!-- Folio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folio', 'Folio:') !!}
    {!! Form::text('folio', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::number('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Usuario Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usuario_id', 'Usuario Id:') !!}
    {!! Form::number('usuario_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Colegio Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colegio_id', 'Colegio Id:') !!}
    {!! Form::number('colegio_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre Contacto:') !!}
    {!! Form::text('nombre_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Apellidos Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('apellidos_contacto', 'Apellidos Contacto:') !!}
    {!! Form::text('apellidos_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Celular Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('celular_contacto', 'Celular Contacto:') !!}
    {!! Form::text('celular_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Entrega Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_entrega', 'Tipo Entrega:') !!}
    {!! Form::number('tipo_entrega', null, ['class' => 'form-control']) !!}
</div>

<!-- Factura Field -->
<div class="form-group col-sm-6">
    {!! Form::label('factura', 'Factura:') !!}
    {!! Form::number('factura', null, ['class' => 'form-control']) !!}
</div>

<!-- Metodo Pago Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metodo_pago_id', 'Metodo Pago Id:') !!}
    {!! Form::number('metodo_pago_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Forma Pago Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forma_pago_id', 'Forma Pago Id:') !!}
    {!! Form::number('forma_pago_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Terminos Condiciones Field -->
<div class="form-group col-sm-6">
    {!! Form::label('terminos_condiciones', 'Terminos Condiciones:') !!}
    {!! Form::number('terminos_condiciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Comision Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comision', 'Comision:') !!}
    {!! Form::number('comision', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush