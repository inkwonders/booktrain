<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $configuracion->id }}</p>
</div>

<!-- Etiqueta Field -->
<div class="col-sm-12">
    {!! Form::label('etiqueta', 'Etiqueta:') !!}
    <p>{{ $configuracion->etiqueta }}</p>
</div>

<!-- Valor Field -->
<div class="col-sm-12">
    {!! Form::label('valor', 'Valor:') !!}
    <p>{{ $configuracion->valor }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $configuracion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $configuracion->updated_at }}</p>
</div>

