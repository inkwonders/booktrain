@extends('layouts.logged')
@section('title')
    Pedidos
@endsection
@section('referenciasSelected')
    border-b-green--BK
@endsection


@section('css')
@if(Session::has('download.in.the.next.request'))
         <meta http-equiv="refresh" content="5;url={{ Session::get('download.in.the.next.request') }}">
      @endif
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>
        p{
            font-family: roboto-medium;
            color: var(--main-gray--bk);
            /* font-weight: bold; */
         }
         .wrapper {
        overflow: hidden;
        position: relative;
        width: 10rem; /* real length of dropdown */
        }

        .wrapper:after {
        content: "▼";
        font-size: 0.8rem;
        pointer-events: none; /* ▼ click triggers dropdown */
        position: absolute;
        right: 0.2rem;
        top: 0.3rem;
        z-index: 1;
        }
        select {
        /* remove default caret */
        -webkit-appearance: none; /* webkit browsers */
        -moz-appearance: none; /* firefox */
        appearance: none; /* modern browsers */
        width: 100%;
        }

        select::-ms-expand {
        display: none; /* remove default caret for ie */
        }

                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }

        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }

        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }
        .text-blue-1000 {
            --tw-text-opacity: 1;
            color: rgba(80, 185, 231, var(--tw-text-opacity));
        }
        .text-green-1000 {
            --tw-text-opacity: 1;
            color: rgba(129, 184, 78, var(--tw-text-opacity));
        }
        .border-blue-1000
        {
            --tw-text-opacity: 1;
            color: rgba(80, 185, 231, var(--tw-text-opacity));
        }


    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

@endsection

@section('contenido')
<div class="w-full flex flex-col relative h-autos border rounded-md border-color-gray--BK min-h-interno items-center">
    <div class="relative w-full flex flex-col p-10">
        <span class="color-red--bk font-rob-bold uppercase text-xl">Pedidos</span>
        <br>
        <form action="/reporte_pedidos/pedidos-referencias" method="GET" >

        <div class="w-full flex justify-center items-center">
            <div class="relative w-4/5 flex flex-col items-center mt-12 ">

                <div class="relative w-full border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2" style=" width: 98.5%;">
                    <select name="status" id="status" class=" p-2 text-grayBT appearance-none ">
                        <option selected value="0">Cualquier estatus</option>
                        <option value="ENTREGADO">ENTREGADO</option>
                        <option value="PAGADO">PAGADO</option>
                        <option value="PROCESADO">PROCESADO</option>
                        <option value="ENVIADO">ENVIADO</option>
                        <option value="CANCELADO">CANCELADO</option>
                        <option value="PAGO RECHAZADO">PAGO RECHAZADO</option>
                        <option value="PAGO EN REVISION">PAGO EN REVISION</option>
                        <option value="ENVIO PARCIAL">ENVIO PARCIAL</option>
                        <option value="ENVIO LIQUIDADO">ENVIO LIQUIDADO</option>
                        <option value="ENTREGA PARCIAL">ENTREGA PARCIAL</option>
                        <option value="ENTREGA LIQUIDADA">ENTREGA LIQUIDADA</option>
                    </select>
                </div>
                <div class="w-full relative flex flex-row">
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <select name="factura" id="factura" class="p-2 text-grayBT appearance-none" >
                            <option selected value="0">Facturas Todo</option>
                            <option value="1">SI solicita</option>
                            <option value="2">NO solicita</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <select name="metodo_pagos" id="metodo_pagos" class="p-2 text-grayBT appearance-none" style="right: 2.5px;">
                            <option selected value="0">Cualquier Método de pago</option>
                            <option value="CREDITO">CREDITO</option>
                            <option value="DEBITO">DEBITO</option>
                            <option value="AMEX">AMEX</option>
                            <option value="BANCO">BANCO</option>
                            <option value="TIENDA">TIENDA</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <select name="forma_pagos" id="forma_pagos" class="p-2 text-grayBT appearance-none" style="left: 2.5px;">
                            <option selected value="0">Cualquier Forma de pago</option>
                            <option value="PUE">PUE</option>
                            <option value="DIFERIDO3M">Diferido 3m</option>
                            <option value="DIFERIDO6M">Diferido 6m</option>
                            <option value="DIFERIDO9">Diferido 9m</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <select name="forma_entrega" id="forma_entrega" class="p-2 text-grayBT appearance-none">
                            <option selected value="0">Cualquier Forma de entrega</option>
                            <option value="1">Colegio</option>
                            <option value="2">Domicilio</option>
                            <option value="3">Pick-Up</option>
                        </select>
                    </div>
                </div>
                <div class="w-full relative flex flex-row my-2 px-2">
                    <input name="inicio" id="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 mr-4 px-2 h-11 rounded-md border text-grayBT" type="text"  value="{{ old('inicio') }}">
                    <input name="fin" id="fin" required placeholder="FECHA FINAL" class="relative w-1/3 px-2 h-11 rounded-md border text-grayBT" type="text"  value="{{ old('fin') }}">

                <button type="button" id="buscar" class="relative ml-4 w-1/3 h-11 background-green--bk text-white font-bold rounded hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">BUSCAR</button>
                </div>

                <div class="w-full  flex flex-col justify-evenly items-center mt-4">
                    <button type="submit" id="exportar" class="h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">EXPORTAR EXCEL</button>
                </div>

            </div>

        </div>
        </form>

        <div class="data-table-diseño mt-8">
            <livewire:reporte-pedidos-general class="text-grayBT" searchable="folio, nombre_contacto, apellidos_contacto, email_pedido,celular_contacto,factura,metodo_pago,forma_pago,total,colegio_id" exportable/>
        </div>
    </div>

</div>


                {{-- <livewire:reporte-pedidos-general /> --}}
                <livewire:contenedor-reporte-pedidos-general />


@endsection

@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    {{-- <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


     <script type="text/javascript">

        // document.addEventListener("DOMContentLoaded", () => {
        //     Livewire.hook('element.updated', (el, component) => {
        //         element_input = document.querySelector('[placeholder="Buscar"]');
        //         element_input.focus();
        //     })
        // });


        $(document).on('submit', '.formulario-cambio', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas cambiar el estatus de este pedido?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Cambiar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Cambiando..!', '', 'success')
                    this.submit();
                }
            })
        });

        $(document).on('submit', '.formulario-envio-mail', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas reenviar el correo de confirmación de pago al cliente?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Enviar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Enviando correo..!', '', 'success')
                    this.submit();
                }
            })
        });

        $(document).ready(function () {
            $('.openModal').on('click', function(e){
                $('#interestModal').removeClass('invisible');
                var userid = $(this).data('userid');
                var form = $('form.form-login');
                var input = $('<input type="hidden" name="pedido_id" value="'+userid+'" />');
                if (form.find('input[name="pedido_id"]').length) {
                    form.find('input[name="pedido_id"]').val(userid);
                } else {
                    form.prepend(input);
                }
            });

            $('.closeModal').on('click', function(e){
                $('#interestModal').addClass('invisible');
            });
        });

        // $(document).ready(function () {
        //     let pedido = '';
        //     $('.boton_detalles').on('click', function(e){
        //         pedido = $(this).attr("name");
        //         // alert(pedido);
        //         $('#'+pedido).removeClass('invisible');
        //         var userid = $(this).data('userid');
        //         var form = $('form.form-login');
        //         var input = $('<input type="hidden" name="pedido_id" value="'+userid+'" />');
        //         if (form.find('input[name="pedido_id"]').length) {
        //             form.find('input[name="pedido_id"]').val(userid);
        //         } else {
        //             form.prepend(input);
        //         }
        //     });

        //     $('.closeModal').on('click', function(e){
        //         $('#'+pedido).addClass('invisible');
        //     });
        // });

        $(document).ready(function() {
            $("#btn_cerrar").on("click", function(){
                $('#modal-cerrar').addClass("hidden");
            })


        });

        $(document).ready(function() {
        // Datepicker
        $("#inicio").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#fin").datepicker({ dateFormat: 'yy-mm-dd' });

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;

        $("#inicio").val(today);
        $("#fin").val(today);


        $("#buscar").on("click", function(){

            Livewire.emit('fechas', $("#inicio").val(), $("#fin").val());
            Livewire.emit('status', $("#status").val());
            Livewire.emit('factura', $("#factura").val());
            Livewire.emit('metodo_pagos', $("#metodo_pagos").val());
            Livewire.emit('forma_pagos', $("#forma_pagos").val());
            Livewire.emit('forma_entrega', $("#forma_entrega").val());
        });



        });

        // $(document).ready(function() {

        //   $("#exportar").on("click", function(){
        //     Livewire.emit('exportar',
        //     $("#inicio").val(),
        //     $("#fin").val(),
        //     $("#status").val(),
        //     $("#factura").val(),
        //     $("#metodo_pagos").val(),
        //     $("#forma_pagos").val(),
        //     $("#forma_entrega").val())
        //     });
        // });



    </script>




@endsection


