@extends('layouts.logged')
@section('title')
    Pedidos
@endsection
@section('referenciasSelected')
    border-b-green--BK
@endsection


@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

    <style>
        p{
            font-family: roboto-medium;
            color: var(--main-gray--bk);
            /* font-weight: bold; */
         }
         .wrapper {
        overflow: hidden;
        position: relative;
        width: 10rem; /* real length of dropdown */
        }

        .wrapper:after {
        content: "▼";
        font-size: 0.8rem;
        pointer-events: none; /* ▼ click triggers dropdown */
        position: absolute;
        right: 0.2rem;
        top: 0.3rem;
        z-index: 1;
        }
        select {
        /* remove default caret */
        -webkit-appearance: none; /* webkit browsers */
        -moz-appearance: none; /* firefox */
        appearance: none; /* modern browsers */
        width: 100%;
        }


        select::-ms-expand {
        display: none; /* remove default caret for ie */
        }

                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }

        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }

        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }
        .modal {
        transition: opacity 0.25s ease;
        }
        body.modal-active {
        overflow-x: hidden;
        overflow-y: visible !important;
        }
        .text-blue-1000 {
            --tw-text-opacity: 1;
            color: rgba(80, 185, 231, var(--tw-text-opacity));
        }
        .text-green-1000 {
            --tw-text-opacity: 1;
            color: rgba(129, 184, 78, var(--tw-text-opacity));
        }
        .border-blue-1000
        {
            --tw-text-opacity: 1;
            color: rgba(80, 185, 231, var(--tw-text-opacity));
        }


    </style>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">


@endsection

@section('contenido')
    <section class="flex flex-col items-center justify-center w-full px-4 py-4 min-h-custom">
        @hasrole("Admin|Super Admin")
        @if (auth()->user()->hasDirectPermission('Pedidos'))
        <div class="relative flex flex-col items-center w-full h-full border rounded-md border-color-gray--BK">

            <div class="relative flex flex-col w-full p-5">
                <span class="mt-3 text-2xl uppercase color-red--bk font-rob-bold">Pedidos</span>

                {{-- {{ $pedido_generales = \App\Models\Pedido::whereNotIn('status','CARRITO') }} --}}
            <center>
                <form action="/referencias/reporte_referencias/" method="GET" class="relative items-center w-full ">
                    <div class="relative flex flex-col items-center w-4/5 mt-12 ">
                        <p>Busquedas sobre los pedidos</p>
                        <div class="relative w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer" style=" width: 98.5%;">
                            <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                            <select name="status" class="p-2 border appearance-none color-gray--bk">
                                <label for="status">Selecciona un estatus</label>
                                <option value="0">Cualquier estatus</option>
                                <option value="ENTREGADO">ENTREGADO</option>
                                <option value="PAGADO">PAGADO</option>
                                <option value="PROCESADO">PROCESADO</option>
                                <option value="ENVIADO">ENVIADO</option>
                                <option value="CANCELADO">CANCELADO</option>
                                <option value="PAGO RECHAZADO">PAGO RECHAZADO</option>
                                <option value="PAGO EN REVISION">PAGO EN REVISION</option>
                                <option value="ENVIO PARCIAL">ENVIO PARCIAL</option>
                                <option value="ENVIO LIQUIDADO">ENVIO LIQUIDADO</option>
                                <option value="ENTREGA PARCIAL">ENTREGA PARCIAL</option>
                                <option value="ENTREGA LIQUIDADA">ENTREGA LIQUIDADA</option>
                            </select>
                        </div>

                        <div class="relative flex flex-row w-full">
                            <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select name="factura" class="p-2 border appearance-none color-gray--bk" >
                                    <label for="factura">Facturas Si y No</label>
                                    <option value="0">Facturas Todo</option>
                                    <option value="1">SI solicita</option>
                                    <option value="2">NO solicita</option>
                                </select>
                            </div>
                            <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select name="metodo_pagos" class="p-2 border appearance-none color-gray--bk" style="right: 2.5px;">
                                    <label for="Métodos pagos">Métodos pagos</label>
                                    <option value="0">Cualquier Método de pago</option>
                                    <option value="CREDITO">CREDITO</option>
                                    <option value="DEBITO">DEBITO</option>
                                    <option value="AMEX">AMEX</option>
                                    <option value="BANCO">BANCO</option>
                                    <option value="TIENDA">TIENDA</option>
                                </select>
                            </div>
                            <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select name="forma_pagos" class="p-2 border appearance-none color-gray--bk" style="left: 2.5px;">
                                    <label for="forma_pagos">Forma pagos</label>
                                    <option value="0">Cualquier Forma de pago</option>
                                    <option value="PUE">PUE</option>
                                    <option value="DIFERIDO3M">Diferido 3m</option>
                                    <option value="DIFERIDO6M">Diferido 6m</option>
                                    <option value="DIFERIDO9">Diferido 9m</option>
                                </select>
                            </div>
                            <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select name="forma_entrega" class="p-2 border appearance-none color-gray--bk">
                                    <label for="forma_entrega">Forma de entrega</label>
                                    <option value="0">Cualquier Forma de entrega</option>
                                    <option value="1">Colegio</option>
                                    <option value="2">Domicilio</option>
                                    <option value="3">Pick-Up</option>
                                </select>
                            </div>
                        </div>
                        <p style="padding-top: 9px;">Rango de fechas</p>
                        <div class="relative flex flex-row w-full my-2">
                            <input name="inicio" id="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 p-2 mr-2 border color-gray--bk" type="text">
                            <input name="fin" id="fin" required placeholder="FECHA FINAL" class="relative w-1/3 p-2 border color-gray--bk" type="text">

                        <button style="left: 8px;width: 31%;" type="submit" id="btn_buscar" class="relative w-56 w-1/3 h-12 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">BUSCAR</button>
                        </div>
                    </div>
                </form>
                <div class="flex flex-col items-center w-full mt-4 justify-evenly">
                    <form action="api_web/excel/pedidos-referencias" method="POST" >

                        @if ($status != null || $status != '')
                        <input value="{{ $status }}" name="status" id="status" type="hidden">
                        @else
                        <input value="0" name="status" id="status" type="hidden">
                        @endif
                        <input value="{{ $factura}}" name="factura" id="factura" type="hidden">
                        <input value="{{ $metodo_pagos}}" name="metodo_pagos" id="metodo_pagos" type="hidden">
                        <input value="{{ $forma_pagos}}" name="forma_pagos" id="forma_pagos" type="hidden">
                        <input value="{{ $forma_entrega}}" name="forma_entrega" id="forma_entrega" type="hidden">
                        <input value="{{ $inicio}}" name="inicio" id="inicio" type="hidden">
                        <input value="{{ $fin}}" name="fin" id="fin" type="hidden">

                        <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}"/>
                        <button type="submit" class="w-56 h-12 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">EXPORTAR EXCEL</button>
                    </form>
                </div>

            </center>
                @if($errors->any())
                <div id="modal-cerrar" class="fixed bottom-0 left-0 z-30 flex items-center justify-center w-full h-full bg-gray-600 bg-opacity-50 modal">
                    <div class="w-1/3 bg-white rounded-lg">
                        <div class="flex flex-col items-start p-4">
                            <div class="flex items-center justify-center w-full">
                                <div class="pb-3 text-lg font-medium text-gray-900">
                                <center> <h2 class="text-2xl rob-bold color-black--bk">{{ implode('', $errors->all(':message')) }}</h2> </center>
                                </div>
                            </div>
                            <div class="w-full">
                                <div class="flex justify-around w-full pt-5">
                                    <div id="btn_cerrar" data-dismiss="modal" class="flex items-center justify-center w-1/4 text-white uppercase border border-solid rounded-md cursor-pointer close background-red--bk font-rob-bold hover:bg-white hover:border-red-500 hover:text-redBT">
                                        Cerrar
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!-- tabla -->
                <div class="block w-full h-auto mt-4 overflow-x-auto scrool_azul">
                    <div class="flex flex-row w-auto h-auto min-w-full">
                        <div class="table-responsive">
                        <table id="example2" class="stripe tabla_quejas" style="width:100%">
                            <thead>
                                <tr>
                                    <th><span>Folio</span></th>
                                    <th><span>Fecha</span></th>
                                    <th><span>Nombre</span></th>
                                    <th><span>Correo</span></th>
                                    <th><span>Teléfono</span></th>
                                    <th><span>Factura</span></th>
                                    <th><span>Referencia</span></th>
                                    <th><span>Método de pago</span></th>
                                    <th><span>Forma de pago</span></th>
                                    <th><span>Monto cobrado</span></th>
                                    <th><span>Monto recibido</span></th>
                                    <th><span>Colegio</span></th>
                                    <th><span>Fecha pago</span></th>
                                    <th><span>Entrega</span></th>
                                    <th><span>Estatus</span></th>
                                    <th><span>Cambio de Estatus</span></th>
                                    <th><span>Correo de confirmación</span></th>
                                    <th><span>Pedido</span></th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ( $pedidos as $pedido )

                                <tr class="h-24 text-center font-rob-light">
                                    <td><span>{{$pedido->serie }}{{$pedido->folio}}</span></td>
                                    <td><span>{{ $pedido->created_at != null ? (date_format($pedido->created_at,"d / m / Y")) : ''}}</span></td>
                                    <td><span>{{ $pedido->nombre_contacto }} {{ $pedido->apellidos_contacto }}</span></td>
                                    <td><span>{{ $pedido->user->email }}</span></td>
                                    <td><span>{{ $pedido->celular_contacto }} </span></td>
                                    @if ( $pedido->factura == 1)
                                    <td><span>Si</span></td>
                                    @else
                                    <td><span>No</span></td>
                                    @endif
                                    <td><span>{{ $pedido->referencia != null ? $pedido->referencia->referencia : '' }}</span></td>
                                    <td><span>{{ $pedido->metodo_pago }}</span></td>
                                    <td><span>{{ $pedido->forma_pago }}</span></td>
                                    <td><span>${{ number_format($pedido->total,2) }}</span></td>
                                    <td><span>${{ number_format($pedido->pagos->where('status','SUCCESS')->pluck('amount')->first(), 2) }}</span></td>
                                    <td><span>{{ $pedido->colegio->nombre }}</span></td>
                                    <td>
                                        <span>{{ $pedido->pagos->where('status','SUCCESS')->pluck('created_at')->first() }}</span>

                                    </td>
                                    <td>@if ($pedido->tipo_entrega == 1)
                                        <span>Colegio</span>
                                        @elseif ($pedido->tipo_entrega == 2)
                                        <span>Domicilio</span>
                                        @else
                                        <span>PickUp</span>
                                        @endif
                                    </td>

                                    <td>@if ($pedido->status == 'PAGO RECHAZADO' || $pedido->status == 'CANCELADO')
                                        <span class="color-red--bk">{{$pedido->status }}</span>
                                        @elseif ($pedido->status == 'PROCESADO')
                                        <span class="color-gray--bk">{{$pedido->status }}</span>
                                        @elseif ( $pedido->status == 'PAGO EN REVISION')
                                        <span class="color-blue--bk">{{$pedido->status }}</span>
                                        @else
                                        <span class="color-green--bk">{{$pedido->status }}</span>
                                        @endif
                                    </td>

                                    <td>
                                        <span>
                                            <form action="/referencias/reporte_referencias_status/" method="GET" class="relative items-center w-full formulario-cambio" id="formulario-cambio" name="formulario-cambio">
                                                @if($pedido->status  == 'PROCESADO')
                                                    {{-- <select name="estatus" required class="relative w-1/2 p-2 mr-2 border appearance-none color-gray--bk" style="width: 110%">
                                                        <option value="">Selecciona</option>
                                                        <option value="PAGADO">PAGADO</option>
                                                        <option value="CANCELADO">CANCELADO</option>
                                                    </select>
                                                    <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $pedido->id }}"> --}}
                                                    <a style="height: 47.65px;line-height: 43.65px;border-color: #81b84e;"type="submit" data-userid="{{ $pedido->id }}" class="block w-32 h-12 font-bold border border-green-500 border-solid rounded cursor-pointer focus:outline-none openModal background-white--bk text-green-1000 hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Modificar</a>
                                                    {{-- <a type="submit" data-userid="{{ $pedido->id }}" ><button type="submit" class="w-56 h-12 font-bold text-white border border-gray-400 border-solid rounded focus:outline-none openModal background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">Cambiar</button></a> --}}
                                                @endif
                                                @if($pedido->status  == 'PAGADO')
                                                    <input type="hidden" name="estatus" id="estatus" value="CANCELADO">
                                                    <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $pedido->id }}">
                                                    <button type="submit"  id="btnSend" name="btnSend"  class="w-32 h-12 font-bold text-red-500 border border-red-400 border-solid rounded background-white--bk hover:bg-red-500 hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Cancelar</button>
                                                @endif
                                                @if($pedido->status  == 'CANCELADO')
                                                    <input type="hidden" name="estatus" id="estatus" value="PROCESADO">
                                                    <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $pedido->id }}">
                                                    <button type="submit"  id="btnSend" name="btnSend"  class="w-32 h-12 font-bold border border-green-500 border-solid rounded background-white--bk text-green-1000 hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm" style="border-color: #81b84e;">Activar</button>
                                                @endif
                                            </form>
                                        </span>
                                    </td>
                                    <td>
                                        <span>
                                            <form action="/referencias/envio_confirmacion_pago/" method="GET" class="relative items-center w-full formulario-envio-mail" id="formulario-envio-mail" name="formulario-envio-mail" >
                                                @if($pedido->status  == 'PAGADO')
                                                <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $pedido->id }}">
                                                <button style="border-color: #50b9e7;" type="submit"  id="btnSend" name="btnSend" class="w-32 h-12 font-bold border border-solid rounded background-white--bk text-blue-1000 hover:background-blue--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border-blue-1000">Enviar</button>

                                                @else
                                                @endif
                                            </form>
                                        </span>
                                    </td>
                                    <td>
                                        <div class="px-4 w-max">
                                            <button class="boton_detalles w-max hover:underline" name="{{ $pedido->id }}">Ver detalles</button>
                                            <div class="fixed inset-0 z-10 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="{{ $pedido->id }}">
                                                <div class="items-end justify-center block min-h-screen px-4 pb-20 text-center sm:p-0 pt-28 sm:pt-28">
                                                    <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75 -z-1" aria-hidden="true"></div>
                                                    <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
                                                    <div class="inline-block w-10/12 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle max-w-7xl">
                                                        <form action="/referencias/reporte_referencias_status/" method="GET" class="relative items-center w-full form-login" id="form-login" name="form-login" >
                                                            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                                                                <div class="pb-4 sm:flex sm:items-start">
                                                                    <div class="w-full mt-3 text-center sm:mt-0 sm:px-4 sm:text-left">
                                                                        <h2 class="pb-4 swal2-title" id="modal-title" >
                                                                            {{$pedido->serie }}{{$pedido->folio}}
                                                                        </h2>
                                                                        <div class="w-full">
                                                                            {{-- <p>id: {{ ($pedido->direccion->calle) }}</p> --}}

                                                                            <div class="w-full border-b-2"></div>
                                                                            <div class="w-full border-b-2">
                                                                                <div class="flex mt-6 flex-column">
                                                                                    <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>
                                                                                </div>
                                                                                <div class="flex justify-between">
                                                                                    <div class="mt-4 w-max">
                                                                                        <span class="text-base uppercase color-gray--bk font-rob-bold">nombre: </span>
                                                                                        <span class="text-base uppercase color-gray--bk"> {{ $pedido->nombre_contacto }} </span>
                                                                                    </div>
                                                                                    <div class="mt-4 w-max">
                                                                                        <span class="text-base uppercase color-gray--bk font-rob-bold">apellido(s): </span>
                                                                                        <span class="text-base uppercase color-gray--bk"> {{ $pedido->apellidos_contacto }} </span>
                                                                                    </div>
                                                                                    <div class="mt-4 w-max">
                                                                                        <span class="text-base uppercase color-gray--bk font-rob-bold">Celular: </span>
                                                                                        <span class="text-base uppercase color-gray--bk"> {{ $pedido->celular_contacto }} </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="w-full border-b-2">
                                                                                <div class="flex mt-6 flex-column">
                                                                                    <p class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">ENTREGA</p>
                                                                                </div>
                                                                                <div class="flex mt-4 flex-column">
                                                                                    <span class="text-base uppercase color-gray--bk">
                                                                                        @if ($pedido->tipo_entrega == 1)
                                                                                            <span class="font-rob-bold">COLEGIO:</span>
                                                                                        @elseif (($pedido->tipo_entrega == 2))
                                                                                            <span class="font-rob-bold">DOMICILIO:</span>
                                                                                        @elseif (($pedido->tipo_entrega == 3))
                                                                                            <span class="font-rob-bold">PICKUP:</span>
                                                                                        @else
                                                                                            <span class="font-rob-bold">DOMICILIO:</span>
                                                                                        @endif
                                                                                        {{ is_null($pedido->direccion)? '' : $pedido->direccion->direccion_completa }}
                                                                                    </span>
                                                                                    <span class="text-base uppercase color-gray--bk"></span>
                                                                                </div>
                                                                                <div class="flex mt-4 flex-column">
                                                                                    <span class="text-base color-gray--bk ">
                                                                                        @if ($pedido->tipo_entrega == 2)
                                                                                            @if ($pedido->envio->paqueteria()->first() != null)
                                                                                                <span class="font-rob-bold">Guia:</span> {{ $pedido->envio->guia }}</br>
                                                                                                <span class="font-rob-bold">Paquetería:</span> {{ $pedido->envio->paqueteria()->first()->nombre }}</br>
                                                                                                <span class="font-rob-bold">Url: </span><a target="_blank" href='{{ $pedido->envio->paqueteria()->first() != null ? $pedido->envio->paqueteria()->first()->url :'' }}'>{{ $pedido->envio->paqueteria()->first() != null ? $pedido->envio->paqueteria()->first()->url :'' }}</a></br>
                                                                                            @else

                                                                                            @endif
                                                                                        @else

                                                                                        @endif
                                                                                    </span>
                                                                                    <span class="text-base uppercase color-gray--bk"></span>
                                                                                </div>
                                                                                <div class="mt-4 mb-4 text-left">
                                                                                </div>
                                                                            </div>
                                                                            @if (!is_null($pedido->datosFactura ))
                                                                                <div class="w-full border-b-2">
                                                                                    <div class="flex mt-6 flex-column">
                                                                                        <p class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">datos de facturación del último pedido</p>
                                                                                    </div>
                                                                                    <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                                                                                        <thead>
                                                                                            <tr class="font-bold uppercase align-top">
                                                                                                <th class="pb-4 pr-4">Nombre o razón social</th>
                                                                                                <th class="pb-4 pr-4">RFC</th>
                                                                                                <th class="pb-4 pr-4">Correo</th>
                                                                                                <th class="pb-4 pr-4">Uso de CFDI</th>
                                                                                                <th class="pb-4 pr-4">método de pago</th>
                                                                                                <th class="pb-4 pr-4">Forma de pago</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <tr class="align-top">
                                                                                                <td class="pr-4">{{ $pedido->datosFactura->razon_social }}</td>
                                                                                                <td class="pr-4">{{ $pedido->datosFactura->rfc }}</td>
                                                                                                <td class="pr-4 uppercase">{{ $pedido->datosFactura->correo }}</td>
                                                                                                <td class="pr-4">{{ $pedido->datosFactura->cfdis->descripcion }}</td>
                                                                                                <td class="pr-4">{{ $pedido->datosFactura->metodo_pago }}</td>
                                                                                                <td class="pr-4">{{ $pedido->datosFactura->forma_pago }}</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                    <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                                                                                        <thead>
                                                                                            <tr class="font-bold uppercase align-top">
                                                                                                <th class="pb-4 pr-4">Calle</th>
                                                                                                <th class="pb-4 pr-4">Num. exterior</th>
                                                                                                <th class="pb-4 pr-4">Num. interior</th>
                                                                                                <th class="pb-4 pr-4">Colonia</th>
                                                                                                <th class="pb-4 pr-4">Municipio</th>
                                                                                                <th class="pb-4 pr-4">Estado</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <tr class="align-top">
                                                                                                <td class="pr-4">{{ $datos_facturacion->calle }}</td>
                                                                                                <td class="pr-4">{{ $datos_facturacion->num_exterior }}</td>
                                                                                                <td class="pr-4">{{ $datos_facturacion->num_interior }}</td>
                                                                                                <td class="pr-4">{{ $datos_facturacion->colonia }}</td>
                                                                                                <td class="pr-4">{{ $datos_facturacion->municipio }}</td>
                                                                                                <td class="pr-4">{{ $datos_facturacion->estado }}</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                    <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                                                                                        <thead>
                                                                                            <tr class="font-bold uppercase align-top">
                                                                                                <th class="pb-4 pr-4">Codigo postal</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <tr class="align-top">
                                                                                                <td class="pr-4">{{ $datos_facturacion->cp }}</td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    {{-- <div class="justify-between hidden w-full h-auto mt-4 mb-4 lg:flex">
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
                                                                                            <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->razon_social }}</div>
                                                                                        </div>
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
                                                                                            <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->rfc }}</div>
                                                                                        </div>
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
                                                                                            <div class="text-base color-gray--bk break-word">{{ $pedido->datosFactura->correo }}</div>
                                                                                        </div>
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
                                                                                            <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->cfdis->descripcion }}</div>
                                                                                        </div>
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
                                                                                            <div class="text-base color-gray--bk break-word">{{ $pedido->datosFactura->metodo_pago }}</div>
                                                                                        </div>
                                                                                        <div class="flex flex-col justify-between min-h-full mr-4">
                                                                                            <div class="mb-4 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
                                                                                            <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido->datosFactura->forma_pago }}</div>
                                                                                        </div>
                                                                                    </div> --}}
                                                                                    <div class="pb-4 mt-4 lg:hidden">
                                                                                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
                                                                                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->razon_social }}</div>
                                                                                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
                                                                                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->rfc }}</div>
                                                                                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
                                                                                        <div class="text-base color-gray--bk break-word">{{ $pedido->datosFactura->correo }}</div>
                                                                                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
                                                                                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido->datosFactura->cfdis->descripcion }}</div>
                                                                                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
                                                                                        <div class="text-base color-gray--bk break-word">{{ $pedido->datosFactura->metodo_pago }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido->datosFactura->forma_pago }}</div>

                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Calle</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->calle }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. exterior</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_exterior }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. interior</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_interior }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colonia</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->colonia }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Municipio</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->municipio }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Estado</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->estado }}</div>
                                                                                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Codigo postal</div>
                                                                                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->cp }}</div>

                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            @foreach($pedido->resumenes as $resumen_pedido_alumno)
                                                                                <div class="flex flex-col w-full lg:flex-row">
                                                                                    <div class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">
                                                                                        <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">
                                                                                            <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                                                                                            <span class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido->colegio->nombre }} </span>
                                                                                            <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado: </span>
                                                                                            <span class="col-span-4 text-base uppercase color-gray--bk"> {{ $resumen_pedido_alumno->paquete->nivelColegio->nombre }} - {{ $resumen_pedido_alumno->paquete->nivelColegio->seccion->nombre }} </span>
                                                                                        </div>
                                                                                        <div class="w-full mt-4 text-left">
                                                                                            <span class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE DEL ALUMNO:* </span>
                                                                                        </div>
                                                                                        <div class="w-full mt-2 mb-5 text-left">
                                                                                            <span class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_alumno." ".$resumen_pedido_alumno->paterno_alumno." ".$resumen_pedido_alumno->materno_alumno }} </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="lg:w-2/3 lg:border-b-2">
                                                                                        <div class="flex flex-col mt-10 mb-6 lg:grid lg:grid-cols-11 lg:gap-4">
                                                                                            @foreach ($resumen_pedido_alumno->detallesResumenPedidos as $detalles_resumen)
                                                                                                <span class="col-span-7 text-base text-left color-gray--bk lg:text-right">
                                                                                                    {{ $detalles_resumen->libro->nombre }}
                                                                                                    {{ ($detalles_resumen->cantidad > 1? " (".$detalles_resumen->cantidad.")" : "" ) }}
                                                                                                </span>
                                                                                                <span class="col-span-2 text-base text-left color-gray--bk"><label class="text-base text-gray-500 lg:hidden">ISBN </label>{{ $detalles_resumen->libro->isbn }}</span>
                                                                                                <span class="flex justify-between w-full col-span-2 text-base text-right color-green--bk lg:inline-block rob-light font-extralight"> <label class="text-base text-gray-500 lg:hidden">PRECIO</label> ${{ number_format($detalles_resumen->precio_libro, 2) }} MXN </span>
                                                                                                <div class="w-full my-4 border-b-2 lg:hidden"></div>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                            <div class="flex w-full">
                                                                                <div class="hidden lg:w-1/3 lg:block">
                                                                                </div>
                                                                                <div class="w-full lg:w-2/3">
                                                                                    <div class="grid grid-cols-11 mt-10 mb-6 lg:gap-4 font-rob-regular">
                                                                                        @php
                                                                                            $subtotal = $pedido->resumenes->sum('subtotal');
                                                                                            $guia_pedido = $pedido->envio;
                                                                                            $costo_envio = 0;
                                                                                            if ($guia_pedido)
                                                                                                $costo_envio = $guia_pedido->costo;
                                                                                            $total = ($subtotal + $costo_envio) + $pedido->comision; //* 1.16;
                                                                                        @endphp
                                                                                        <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">SUBTOTAL</span>
                                                                                        <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3"> ${{ number_format($subtotal, 2) }} MXN </span>
                                                                                        <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">ENVIO</span>
                                                                                        @if (!is_null($guia_pedido))
                                                                                            <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3"> ${{ number_format($guia_pedido->costo, 2) }} MXN </span>
                                                                                        @else
                                                                                            <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3">${{ number_format(0, 2) }} MXN</span>
                                                                                        @endif
                                                                                        <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">COMISIÓN DE PAGO</span>
                                                                                        <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3 rob-regular"> ${{ number_format($pedido->comision, 2) }} MXN </span>
                                                                                        <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">TOTAL</span>
                                                                                        <span class="col-span-5 font-bold text-right lg:text-md text-md lg:col-span-3 rob-bold"> ${{ number_format($total, 2) }} MXN </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="w-full pt-6">
                                                                    <div class="flex items-center justify-center w-full">
                                                                        <button type="button" style="display: inline-block;" class="closeModal swal2-confirm swal2-styled">Aceptar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
            </div>
            <a href="/tickets"><button class="w-56 h-12 mb-5 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</button></a>
        </div>

 <!--Modal-->
 <div class="fixed inset-0 z-10 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="interestModal">
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <form action="/referencias/reporte_referencias_status/" method="GET" class="relative items-center w-full form-login" id="form-login" name="form-login" >
                <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                    <div class="pb-4 sm:flex sm:items-start">

                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                            <h2 class="pb-4 swal2-title" id="modal-title" >
                            Selecciona el estatus al que quieres cambiar este pedido
                            </h2>

                            <div class="relative w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer" style="width: 94.5%;">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select name="estatus" required class="relative w-1/2 p-2 mr-2 border appearance-none color-gray--bk" style="width: 110%">
                                    <option value="">Selecciona</option>|
                                    <option value="PAGADO">PAGADO</option>
                                    <option value="CANCELADO">CANCELADO</option>
                                </select>
                            {{-- <input type="text" name="pedido_id_cambio" id="pedido_id" value=""> --}}
                            </div>

                        </div>

                    </div>
                    <div class="grid grid-cols-2 gap-2 pt-6">
                        <div class="col-start-1 col-end-2 " style="padding-left: 53%;">
                        <button type="submit"  style="display: inline-block; " id="btnSend" name="btnSend" class="swal2-confirm swal2-styled">Modificar</button>
                        </div>
                        <div class="flex justify-end col-end-3" style="padding-right: 53%;">
                        <button type="button" style="display: inline-block; background-color: rgb(144, 136, 136);" class="closeModal swal2-confirm swal2-styled">Cancelar</button>
                        </div>
                    </div>
            </div>

        </form>
        </div>
    </div>
</div>






        @endif
        @endhasrole
    </section>
@endsection

@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).on('submit', '.formulario-cambio', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas cambiar el estatus de este pedido?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Cambiar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Cambiando..!', '', 'success')
                    this.submit();
                }
            })
        });

        $(document).on('submit', '.formulario-envio-mail', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas reenviar el correo de confirmación de pago al cliente?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Enviar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Enviando correo..!', '', 'success')
                    this.submit();
                }
            })
        });

        $(document).ready(function () {
            $('.openModal').on('click', function(e){
                $('#interestModal').removeClass('invisible');
                var userid = $(this).data('userid');
                var form = $('form.form-login');
                var input = $('<input type="hidden" name="pedido_id" value="'+userid+'" />');
                if (form.find('input[name="pedido_id"]').length) {
                    form.find('input[name="pedido_id"]').val(userid);
                } else {
                    form.prepend(input);
                }
            });

            $('.closeModal').on('click', function(e){
                $('#interestModal').addClass('invisible');
            });
        });

        $(document).ready(function () {
            let pedido = '';
            $('.boton_detalles').on('click', function(e){
                pedido = $(this).attr("name");
                // alert(pedido);
                $('#'+pedido).removeClass('invisible');
                var userid = $(this).data('userid');
                var form = $('form.form-login');
                var input = $('<input type="hidden" name="pedido_id" value="'+userid+'" />');
                if (form.find('input[name="pedido_id"]').length) {
                    form.find('input[name="pedido_id"]').val(userid);
                } else {
                    form.prepend(input);
                }
            });

            $('.closeModal').on('click', function(e){
                $('#'+pedido).addClass('invisible');
            });
        });


        $(document).ready(function() {

            $("#btn_cerrar").on("click", function(){
                $('#modal-cerrar').addClass("hidden");
            })
            // Datepicker
            $("#inicio").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#fin").datepicker({ dateFormat: 'yy-mm-dd' });
            // Datatable
            var table = $('#example2').DataTable();
            table.order( [ 0, 'desc' ] ).draw();
            // ingresar datos de hoy
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;
            let url_referencia = window.location.href;
            if(url_referencia.includes('?')){
                var url_string = window.location.href
                var url = new URL(url_string);
                var inicio = url.searchParams.get("inicio");
                var fin = url.searchParams.get("fin");
                // console.log(inicio);
                $("#inicio").val(inicio);
                $("#fin").val(fin);

            }else{
               $("#inicio").val(today);
               $("#fin").val(today);
            //    $("#btn_buscar").click();
            }

        });


        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },

            colReorder: false,
            scrollY: false,
            scrollX: false,
        } );



    </script>

@endsection
