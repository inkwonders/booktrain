@extends('layouts.logged')
@section('title')
    Pedidos fallidos
@endsection
@section('reportesSelected')
    border-b-green--BK
@endsection


@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">

@endsection

@section('contenido')
    <section class="w-full flex flex-col h-auto min-h-custom px-4 py-4">
        @hasrole("Admin|Super Admin")
        <div class="w-full flex flex-col relative h-interno border rounded-md border-color-gray--BK items-center">
            <div class="w-full flex flex-col relative h-full justify-center">
                <div class="w-full flex flex-row relative h-full justify-center items-center">
                    <!-- Parte izq -->
                    <div class="w-1/2 flex flex-col relative h-full p-12">
                        <span class="uppercase color-red--bk font-bold text-xl">Reporte pagos fallidos con tarjeta</span>
                        <div class="w-full h-full relative justify-center flex items-end">
                            <img src="/assets/img/svg/documento.svg" alt="" class="w-1/3 h-5/6">
                        </div>
                    </div>
                    <!-- Div separación -->
                    <div class="border-r border-color-gray--BK separacion mt-14 h-5/6">
                    </div>
                    <!-- Parte derecha -->
                    <!-- Reporte para colegio -->
                    <form method="GET" action="/reportes/pagos_fallidos/" class="w-1/2 flex flex-col relative h-full justify-center p-12 items-center">
                        <div class="w-full relative flex justify-center mb-12">
                            <input name="inicio" id="inicio" placeholder="FECHA INICIAL" class="relative w-1/3 mr-2 p-2 border color-gray--bk" type="text">
                            <input name="fin" id="fin" placeholder="FECHA FINAL" class="relative w-1/3 p-2 border color-gray--bk" type="text">
                        </div>
                        <button id="btn_buscar" class="relative w-1/3 h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">BUSCAR</button>
                    </form>
                </div>
            </div>
            <a style="width: 15%;" href="/reportes" class="relative w-1/3 h-12 w-40 flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk p-5 cursor-pointer"><div>Regresar</div></a>
            <br><br>
        </div>
        @endhasrole
    </section>
@endsection

@section('js-interno')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
                // Datepicker
                $("#inicio").datepicker({ dateFormat: 'yy-mm-dd' });
                $("#fin").datepicker({ dateFormat: 'yy-mm-dd' });
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();

                today = yyyy + '-' + mm + '-' + dd;
                $("#inicio").val(today);
                $("#fin").val(today);

        });
    </script>


@endsection


