@extends('layouts.logged')

@section('title')
    Exportar Exel
@endsection

@section('reportesSelected')
    border-b-green--BK
@endsection

@section('css')
@endsection

@section('contenido')

    @if(session('error') != null)
        <span>{{ session('error') }}</span>
    @endif

    <section class="w-full h-auto py-4">
        <div class="w-full h-auto py-6 px-8 border border-color-gray--BK rounded-lg flex flex-col justify-between">
            <div class="w-full h-auto">
                 {{-- @if($isbn_req == 0 || $isbn_req == '0')
                {{$isbn = 'TODOS'}}
                @else
                {{ $isbn = \App\Models\Libro::where('isbn', $this->isbn_req)->first()->isbn}}
                @endif --}}
                <span class="rob-bold color-red--bk font-18">REPORTE DE {{$inicio}} A {{$fin}}, {{$colegio->nombre}}, GRADOS: @if($grado_id == 0 || $grado_id == '0'){{$grado = 'TODOS'}}@else{{$grado = \App\Models\Paquete::where('id', $grado_id)->first()->nombre}}@endif, ISBN: {{$isbn_req}}.</span>

                <div class="w-full h-auto overflow-x-auto scrool_azul block mt-4">
                    <div class="w-auto min-w-full h-auto flex flex-row">
                        <table class="min-w-full tabla-p-0">
                            <tr>
                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>ISBN</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>EDITORIAL</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Título</span>
                                                    </th>
                                                    <th>
                                                        <span>Obligatorio / Opcional</span>
                                                    </th>
                                                    <th>
                                                        <span>Grado</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>Precio de lista</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Precio alumno</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>% Descuento Colegio / Alumno</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Pedidos Cantidad</span>
                                                    </th>
                                                    <th>
                                                        <span>Punto Reorden</span>
                                                    </th>
                                                    <th>
                                                        <span>Entrega Cantidad</span>
                                                    </th>
                                                    <th>
                                                        <span>Venta Mostrador</span>
                                                    </th>
                                                    <th>
                                                        <span>Devoluciones Cantidad</span>
                                                    </th>
                                                    <th>
                                                        <span>VENTA REAL</span>
                                                    </th>
                                                    <th>
                                                        <span>Valor Pedido</span>
                                                    </th>
                                                    <th>
                                                        <span>VALOR DE DEVOLUCIÓN</span>
                                                    </th>
                                                    <th>
                                                        <span>VALOR REAL DE VENTA</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>DESCUENTO OTORGADO (Bonificación)</span>
                                                    </th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($libros as $libro)

                                                <tr>
                                                    <td>
                                                        <span>{{ $libro->isbn }}</span>
                                                    </td>
                                                    {{-- <td>
                                                        <span>{{ $colegio->nombre }}</span>
                                                    </td> --}}
                                                    {{-- <td class="fondo_amarillo">
                                                        <span>{{ $libro->editorial }}</span>
                                                    </td> --}}
                                                    <td>
                                                        <span>{{ $libro->nombre }}</span>
                                                    </td>
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                            @if($libro->paquetes->first()->pivot->obligatorio == 1)
                                                            <span>Obligatorio</span>
                                                            @else
                                                            <span>Opcional</span>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{$libro->paquetes->first()->nivel->nombre}}</span>
                                                        @endif

                                                    </td>
                                                    {{-- <td class="fondo_amarillo">
                                                        <span></span>
                                                    </td> --}}
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>${{ $libro->paquetes()->first()->pivot->precio}}.00</span>
                                                        @endif
                                                    </td>
                                                    {{-- <td class="fondo_amarillo">
                                                        <span></span>
                                                    </td> --}}
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{$libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->count()}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{$libro->paquetes()->first()->pivot->punto_reorden}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- entrega cantidad es la suma de los libros entregados --}}
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        {{-- <span>{{ $libro->detalleResumen->where('entregado',1)->count() }}</span> --}}
                                                        <span>{{ $libro->paquetes->pluck('resumenes')->collapse()
                                                        ->pluck('pedido')->whereNotNull()->where('usuario_venta_id','=',null)
                                                        ->pluck('resumenes')->collapse()->pluck('detallesResumenPedidos')->collapse()
                                                        ->where('entregado',1)->where('libro_id',$libro->id)->count() }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- venta mostrador es todo venta movil --}}
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{ $libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('usuario_venta_id','<>',null)->count() }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{$libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('status', 'CANCELADO')->count()}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- venta real es entrega cantidad + venta de mostrador - de voluciones  --}}
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>{{ $libro->detalleResumen->where('entregado',1)->count() }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                    {{-- el valor pedido es entrega cantidad + venta mostrador en $ --}}
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>${{number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2)}}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- valor devolucion aun no es visible, en desarrollo --}}
                                                        <span>$0.00</span>
                                                    </td>
                                                    <td>
                                                        {{-- el valor real de venta es la canditd de libros entregaods + venta de mostrador - devoluciones cantidad --}}
                                                        @if($libro->paquetes->isEmpty())
                                                        <span>0</span>
                                                        @else
                                                        <span>${{ number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2) }}</span>
                                                        @endif
                                                    </td>
                                                    {{-- <td class="fondo_amarillo">
                                                        <span></span>
                                                    </td> --}}

                                                </tr>

                                                @endforeach




                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="w-full h-40 flex flex-col justify-evenly items-center mt-4">
                <form action="api_web/excel/detalles-colegio" method="POST">
                    <input type="hidden" name="inicio"  id="inicio"  value="{{$inicio}}">
                    <input type="hidden" name="fin" id="fin" value="{{$fin}}">
                    <input type="hidden" name="grado" id="grado" value="{{$grado_id}}">
                    <input type="hidden" name="isbn" id="isbn" value="{{$isbn_req}}">
                    <input type="hidden" name="colegio" id="colegio" value="{{$colegio->id}}">
                    <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}"/>
                    <button type="submit" class="h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">EXPORTAR EXCEL</button>
                </form>
                <form action="/reportes_colegio" method="GET">
                <button class="h-12 w-56 bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</button>
                </form>

            </div>
        </div>
    </section>

@endsection

@section('js-interno')
@endsection
