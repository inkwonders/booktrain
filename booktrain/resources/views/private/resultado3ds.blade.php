@extends('layouts.logged')

@section('title')
    Resultado del pago
@endsection

@section('contenido')
<div class="flex flex-col items-center w-full min-h-full lg:px-16">
    <div class="box-border flex flex-row items-start justify-center w-full min-h-full px-4 py-1 md:py-10 md:px-6">
        <div class="box-border flex flex-row items-start justify-center w-full h-full py-1">
            <div class="flex flex-col items-center justify-around w-4/12 h-full">
                <div><img src="{{ asset('/img/svg/emvco_logo.png') }}" alt="pago1" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/sello_logo.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/pci-standard.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/openpay.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
            </div>
            <div class="flex flex-col items-center justify-around w-1/2 h-full py-6 mx-4 border border-gray-500 shadow rounded-xl">
                <p class="mb-4 text-2xl text-center uppercase font-rob-bold text-grayBT">
                    @isset($titulo) {{ $titulo }} @endisset
                </p>
                {{ $message }}

                @if ($success == true)
                    <img src="{{ asset('/img/check.png') }}" alt="">
                    <a href="{{ route('private.user.pedidos') }}" class="relative flex items-center justify-center w-1/2 px-3 py-3 my-5 text-center uppercase border border-solid rounded-md appearance-none background-green--bk h-14 color-white--bk hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm rob-bold"><div class="relative"><span>Ver mis pedidos</span></div></a>
                @else
                    <img src="{{ asset('/img/svg/eliminar.svg') }}" alt="" class="w-44">
                    <a href="{{ route('inicio') }}" class="relative flex items-center justify-center w-1/2 px-3 py-3 my-5 text-center uppercase border border-solid rounded-md appearance-none background-green--bk h-14 color-white--bk hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm rob-bold"><div class=""><span>Regresar a comprar</span></div></a>
                @endif
            </div>
            <div class="flex flex-col items-center justify-around w-4/12 h-full">
                <div><img src="{{ asset('/img/svg/Logobm.svg.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/Mastercard-logo.png') }}" alt="pago1" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/Visa_Logo.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
                <div><img src="{{ asset('/img/svg/bbva.png') }}" class="object-contain w-52 h-28 opacity-70"></div>
            </div>
        </div>
    </div>
</div>
@endsection
