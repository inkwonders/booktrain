@extends('layouts.logged')

@section('title')
    Mi información
@endsection

@section('css')
@endsection

@section('contenido')

@php
    $rs = isset(Auth::user()->datosFacturacion->last()->razon_social) ? ucwords(Auth::user()->datosFacturacion->last()->razon_social): "No tienes una razon social registrada.";
    $rfc = isset(Auth::user()->datosFacturacion->last()->rfc) ? ucwords(Auth::user()->datosFacturacion->last()->rfc) : "No tienes un RFC registrado.";
    $email_factura = isset(Auth::user()->datosFacturacion->last()->correo) ? ucwords(Auth::user()->datosFacturacion->last()->rfc) : "No tienes un correo de facturación registrado.";;
    $descripcion = isset(Auth::user()->datosFacturacion->last()->cfdis->descripcion) ? Auth::user()->datosFacturacion->last()->cfdis->descripcion : "No tienes un uso de CFDI dado de alta." ;
@endphp

<mi-informacion
nombre_usuario     = "{{ ucwords(Auth::user()->name) }}"
apellido_usuario   = "{{ ucwords(Auth::user()->apellidos) }} "
correo_usuario     = "{{ Auth::user()->email }}"
id_usuario         = "{{ Auth::user()->id }}"
token              = "{{ csrf_token() }}"
razon_social       = "{{ $rs }}"
rfc                = "{{ $rfc }}"
email_factura      = "{{ $email_factura }}"
uso_cfdi           = "{{ $descripcion }}">
</mi-informacion>

@endsection

@section('js-interno')
@endsection
