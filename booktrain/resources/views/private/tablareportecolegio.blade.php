@extends('layouts.logged')

@section('title')
    Reporte Colegio
@endsection
@section('reportesSelected')
    border-b-green--BK
@endsection

@section('reportesSelected')
    border-b-green--BK
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>

        #example2_info{
            padding: 1.3em;
        }
                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }
        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }
        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }

    </style>
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">
     <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('contenido')

    @if(session('error') != null)
        <span>{{ session('error') }}</span>
    @endif

    <section class="w-full h-auto py-4">
        <div class="flex flex-col justify-between w-full h-auto px-8 py-6 border rounded-lg border-color-gray--BK">
            <div class="w-full h-auto">
                 {{-- @if($isbn_req == 0 || $isbn_req == '0')
                {{$isbn = 'TODOS'}}
                @else
                {{ $isbn = \App\Models\Libro::where('isbn', $this->isbn_req)->first()->isbn}}
                @endif --}}
                <h2 class="text-xl font-bold color-red--bk">REPORTE DE {{$inicio}} A {{$fin}}, {{$colegio->nombre}}, GRADOS: @if($grado_id == 0 || $grado_id == '0'){{$grado = 'TODOS'}}@else{{$grado = \App\Models\Paquete::where('nivel_id', $grado_id)->first()->nombre}}@endif, ISBN: {{ ($isbn_req != 0 ? $isbn_req : 'TODOS')}}.</h2>
                <div class="block w-full h-auto mt-4 overflow-x-auto scrool_azul">
                    <div class="flex flex-row w-auto h-auto min-w-full">
                        <table class="min-w-full tabla-p-0">
                            <tr>
                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="table-responsive">
                                        <table id="example2" class="text-center color-gray--bk espacio_tablas">
                                            <thead class="h-12 border-t border-b border-color-gray--BK">
                                                <tr>
                                                    <th>
                                                        <span>ISBN</span>
                                                    </th>

                                                    {{-- <th class="fondo_amarillo">
                                                        <span>EDITORIAL</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Título</span>
                                                    </th>
                                                    <th>
                                                        <span>Obligatorio / Opcional</span>
                                                    </th>
                                                    <th>
                                                        <span>Grado</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>Precio de lista</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Precio alumno</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>% Descuento Colegio / Alumno</span>
                                                    </th> --}}
                                                    <th>
                                                        <span>Pedidos Cantidad</span>
                                                    </th>
                                                    <th>
                                                        <span>Punto Reorden</span>
                                                    </th>
                                                    <th>
                                                        <span>Entrega Cantidad</span>
                                                        {{-- cantidad de libros que se han entregado --}}
                                                    </th>
                                                    <th>
                                                        <span>Venta Mostrador</span>
                                                    </th>
                                                    <th>
                                                        <span>Devoluciones Cantidad</span>
                                                    </th>
                                                    <th>
                                                        <span>VENTA REAL</span>
                                                    </th>
                                                    <th>
                                                        <span>Valor Pedido</span>
                                                    </th>
                                                    <th>
                                                        <span>VALOR DE DEVOLUCIÓN</span>
                                                    </th>
                                                    <th>
                                                        <span>VALOR REAL DE VENTA</span>
                                                    </th>
                                                    {{-- <th class="fondo_amarillo">
                                                        <span>DESCUENTO OTORGADO (Bonificación)</span>
                                                    </th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>

                                                    @foreach ($libros as $libro)

                                                        <tr>
                                                            <td>
                                                                <span>{{ $libro->isbn }}</span>
                                                            </td>
                                                            {{-- <td>
                                                                <span>{{ $colegio->nombre }}</span>
                                                            </td> --}}
                                                            {{-- <td class="fondo_amarillo">
                                                                <span>{{ $libro->editorial }}</span>
                                                            </td> --}}
                                                            <td>
                                                                <span>{{ $libro->nombre }}</span>
                                                            </td>
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                    @if($libro->paquetes->first()->pivot->obligatorio == 1)
                                                                    <span>Obligatorio</span>
                                                                    @else
                                                                    <span>Opcional</span>
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{$libro->paquetes->first()->nivel->nombre}}</span>
                                                                @endif

                                                            </td>
                                                            {{-- <td class="fondo_amarillo">
                                                                <span></span>
                                                            </td> --}}
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>${{ $libro->paquetes()->first()->pivot->precio}}.00</span>
                                                                @endif
                                                            </td>
                                                            {{-- <td class="fondo_amarillo">
                                                                <span></span>
                                                            </td> --}}
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{$libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->count()}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{$libro->paquetes()->first()->pivot->punto_reorden}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{-- entrega cantidad es la suma de los libros entregados --}}
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                {{-- <span>{{ $libro->detalleResumen->where('entregado',1)->count() }}</span> --}}
                                                                <span>{{ $libro->paquetes->pluck('resumenes')->collapse()
                                                                ->pluck('pedido')->whereNotNull()->where('usuario_venta_id','=',null)
                                                                ->pluck('resumenes')->collapse()->pluck('detallesResumenPedidos')->collapse()
                                                                ->where('entregado',1)->where('libro_id',$libro->id)->count() }}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{-- venta mostrador es todo venta movil --}}
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{ $libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('usuario_venta_id','<>',null)->count() }}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{$libro->paquetes->pluck('resumenes')->collapse()->pluck('pedido')->whereNotNull()->where('status', 'CANCELADO')->count()}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{-- venta real es entrega cantidad + venta de mostrador - de voluciones  --}}
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>{{ $libro->detalleResumen->where('entregado',1)->count() }}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                            {{-- el valor pedido es entrega cantidad + venta mostrador en $ --}}
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>${{number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2)}}</span>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{-- valor devolucion aun no es visible, en desarrollo --}}
                                                                <span>$0.00</span>
                                                            </td>
                                                            <td>
                                                                {{-- el valor real de venta es la canditd de libros entregaods + venta de mostrador - devoluciones cantidad --}}
                                                                @if($libro->paquetes->isEmpty())
                                                                <span>0</span>
                                                                @else
                                                                <span>${{ number_format($libro->detalleResumen->where('entregado',1)->sum('precio_libro'),2) }}</span>
                                                                @endif
                                                            </td>
                                                            {{-- <td class="fondo_amarillo">
                                                                <span></span>
                                                            </td> --}}

                                                        </tr>

                                                        @endforeach

                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="flex flex-col items-center w-full h-40 mt-4 justify-evenly">
                <form action="api_web/excel/reporte-admin-colegio" method="POST">
                    <input type="hidden" name="inicio"  id="inicio"  value="{{$inicio}}">
                    <input type="hidden" name="fin" id="fin" value="{{$fin}}">
                    <input type="hidden" name="grado" id="grado" value="{{$grado_id}}">
                    <input type="hidden" name="isbn" id="isbn" value="{{$isbn_req}}">
                    <input type="hidden" name="colegio" id="colegio" value="{{$colegio->id}}">
                    <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}"/>
                    <button type="submit" class="w-56 h-12 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">EXPORTAR EXCEL</button>
                </form>
                <form action="/reportes" method="GET">
                <button class="w-56 h-12 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</button>
                </form>
            </div>
        </div>
    </section>

@endsection

@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {

            var table = $('#example2').DataTable();
            table.order( [ 0, 'desc' ] ).draw();


        });
        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            // responsive: {
            //     details: {
            //         type: 'column'
            //     }
            // },
            // columnDefs: [
            //     { responsivePriority: 1, targets: -1 },
            //     { className: 'dtr-control', orderable: false }
            // ],
            colReorder: false,
            scrollY: false,
            scrollX: false,
        } );


    </script>

@endsection
