<!DOCTYPE html>
 <html lang="en">
 <head>
     <title>Reporte de Cierre del Día</title>
     <meta charset="UTF-8">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <style>
         @font-face {
            font-family: 'Roboto', sans-serif;
            font-style: normal;
            font-weight: normal;
            src: url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');
        }
        .rob-font {
            font-family: 'Roboto', sans-serif;
        }
        body{
            width: 100%;
        }
        #padre{
            width: 80%;
            margin:auto;
        }
        table{
            width: 100%;
            position: relative;
            font-size: 20px;
        }
        td{
            height: 55px;
            font-weight: lighter;
            color: #808080;
        }
        .center{
            position: relative;          /* where the next element will be automatically positioned */
            display: inline-block;       /* causes element width to shrink to fit content */
            left: 50%;                   /* moves left side of image/element to center of parent element */
            transform: translate(50%)
        }
        img{
            max-width: 400px;
        }

     </style>

 </head>

 <body>
    <div id="padre">
        <br>
        <br>
        <br>
        <img src="{{$logo_colegio}}" alt="" class="center" height="100px">
        <p class="rob-font" style="font-size: 24px; font-weight: bold; color: #e2492d; text-transform:capitalize;">REPORTE {{ $nombre_colegio }} {{ $fecha }}</p>
        <br>
        <table class="rob-font">
            <tr>
                <td colspan="2">
                    <span>Pago total</span>
                    <span style="float: right; transform: translate(0,50%);"> ${{ number_format($pago_efectivo, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2" style="color: #e2492d;"">
                    <span>Devoluciones</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($devoluciones, 2) }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span>Total efectivo</span>
                    <span  style="float: right; transform: translate(0,50%);">${{ number_format($total_efectivo, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Pago con tarjeta</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_tarjeta, 2) }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span>Pago con terminal</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_terminal, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Tienda de conveniencia</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_tienda, 2) }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span>Pago en practicaja</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_practicaja, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Pago con transferencia</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_transferencia, 2) }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-weight: bold">
                    <span>Total</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($total, 2) }}</span>
                </td>
            </tr>
        </table>
        <br>

        <table class="rob-font">
            <tr>
                <td style="background-color:#f2f2f2; font-weight: bold">
                    <span>Total de libros</span>
                    <span style="float: right; transform: translate(0,50%);">{{ $total_libros }}</span>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <br>

        <table class="rob-font">
            <tr>
                <td style="font-weight: bold;">Cajero</td>
                <td style="font-weight: bold;">Colegio</td>
                <td style="font-weight: bold;">Apertura</td>
                <td style="font-weight: bold;">Cierre</td>
                <td style="font-weight: bold; text-right">Efectivo</td>
            </tr>

            @foreach ($cajas as $key => $caja)
                <tr style="border-width: 0; {{ $key % 2 == 0? 'background-color:#f2f2f2;':'' }}">
                    <td class="px-2">{{ $caja->usuario->name }}</td>
                    <td class="px-2">{{ $caja->colegio->codigo }}</td>
                    <td class="px-2">{{ $caja->fecha_apertura}}</td>
                    <td class="px-2">{{ $caja->fecha_cierre }}</td>
                    <td class="px-2 text-right"><span style="float: right; transform: translate(0,50%);">${{ $caja->operaciones->last()? number_format($caja->operaciones->last()->total, 2) : 0.00 }}</span></td>
                </tr>

            @endforeach

        </table>
        <br>

        <table class="w-full text-lg font-light text-grayBT">
            <tr>
                <td class="px-2 font-bold">Desglose efectivo:</td>
                <td class="px-2 font-bold text-right">Cant.</td>
                <td class="px-2"></td>
            </tr>
            <tr class="h-14">
                <td class="px-2">$ 1000.00</td>
                <td class="px-2 text-right">{{ $b1000 }}</td>
                <td class="px-2 text-right">${{ number_format($b1000 * 1000, 2) }}</td>
            </tr>
            <tr class="h-14">
                <td class="px-2">$ 500.00</td>
                <td class="px-2 text-right">{{ $b500 }}</td>
                <td class="px-2 text-right">${{ number_format($b500 * 500, 2) }}</td>
            </tr>
            <tr class="bg-gray-100 h-14">
                <td class="px-2">$ 200.00</td>
                <td class="px-2 text-right">{{ $b200 }}</td>
                <td class="px-2 text-right">${{ number_format($b200 * 200, 2) }}</td>
            </tr>
            <tr class="h-14">
                <td class="px-2">$ 100.00</td>
                <td class="px-2 text-right">{{ $b100 }}</td>
                <td class="px-2 text-right">${{ number_format($b100 * 100, 2) }}</td>
            </tr>
            <tr class="bg-gray-100 h-14">
                <td class="px-2">$ 50.00</td>
                <td class="px-2 text-right">{{ $b50 }}</td>
                <td class="px-2 text-right">${{ number_format($b50 * 50, 2) }}</td>
            </tr>
            <tr class="h-14">
                <td class="px-2">$ 20.00</td>
                <td class="px-2 text-right">{{ $b20 }}</td>
                <td class="px-2 text-right">${{ number_format($b20 * 20, 2) }}</td>
            </tr>
            {{-- <tr class="bg-gray-100 h-14">
                <td class="px-2">Monedas</td>
                <td class="px-2 text-right">{{ $desglose_efectivo['cantidad_monedas'] }}</td>
                <td class="px-2 text-right">${{ number_format($desglose_efectivo['monedas'], 2) }}</td>
            </tr> --}}
        </table>
        <table class="rob-font">
            <tr style="font-weight: bold; background-color:#f2f2f2;">
                <td style="font-weight: bold;" colspan="4">Total efectivo</td>
                <td style="font-weight: bold; float: right;"><span style="float: right; transform: translate(0,50%);">${{ number_format($total_cajas, 2) }}</span></td>
            </tr>
        </table>


        <br><br><br>

    </div>
 </body>
 </html>
