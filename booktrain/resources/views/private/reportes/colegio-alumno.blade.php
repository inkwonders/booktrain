@extends('layouts.logged')
@section('title')
    Reporte del colegio {{ $colegio->nombre }}
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <div class="w-full my-5 border rounded-md min-h-custom border-color-gray--BK">
        <span class="p-2 rob-bold color-red--bk font-18">REPORTE DE {{ $inicio }} A {{ $fin }}, {{ $colegio->nombre }}, {{ $grado }} {{ $isbn }}, ({{ $resultados }} resultados)</span>

        <div class="p-2 table-responsive">

            <div class="block w-full h-auto mt-4 overflow-x-auto scrool_azul">
                <div class="flex flex-row w-auto h-auto min-w-full">
                    <div class="flex flex-col min-w-full">
                        <table class="text-center color-gray--bk espacio_tablas">
                            <thead class="h-12 border-t border-b border-color-gray--BK">
                                <tr>
                                    <th class="border border-l-2 border-r-2" colspan="8">PEDIDO</th>
                                    <th class="border border-l-2 border-r-2" colspan="3">CONTACTO</th>
                                    <th class="border border-l-2 border-r-2" colspan="3">FACTURACIÓN</th>
                                    <th class="border border-l-2 border-r-2" colspan="1">DISTRIBUCIÓN</th>
                                </tr>
                                <tr>
                                    <th><span>No. Pedido</span></th>
                                    <th><span>Fecha</span></th>
                                    <th><span>Colegio</span></th>
                                    <th><span>Grado</span></th>
                                    <th><span>Sección</span></th>
                                    <th><span>ISBN</span></th>
                                    <th><span>Título</span></th>
                                    <th><span>Piezas</span></th>

                                    <th><span>Nombre alumno</span></th>
                                    <th><span>Estado pedido</span></th>
                                    <th><span>Nombre contacto</span></th>

                                    <th><span>Precio libro</span></th>
                                    <th><span>Método de pago</span></th>
                                    <th><span>Forma de pago</span></th>

                                    <th><span>Tipo de entrega</span></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($pedidos as $index_pedido => $pedido)
                                @foreach ($pedido->resumenes as $index_resumen => $resumen)
                                    @foreach ($resumen->detalles as $index_detalle => $detalle)
                                    <tr>
                                        <td><span>{{ $pedido->serie }}{{ $pedido->folio }}</span></td>
                                        <td><span>{{ $pedido->created_at->format('d / m / Y') }}</span></td>
                                        <td><span>{{ $pedido->colegio->nombre }}</span></td>
                                        <td><span>{{ $resumen->paquete->nivel->nombre }}</span></td>
                                        <td><span>{{ $resumen->paquete->nivel->seccion->nombre }}</span></td>
                                        <td><span>{{ $detalle->libro->isbn }}</span></td>
                                        <td><span>{{ $detalle->libro->nombre }}</span></td>
                                        <td><span>{{ $detalle->cantidad }}</span></td>

                                        <td><span>{{ $resumen->nombre_completo_alumno }}</span></td>
                                        <td><span>{{ $pedido->status }}</span></td>
                                        <td><span>{{ $pedido->nombre_contacto }} {{ $pedido->apellidos_contacto }}</span></td>

                                        <td><span>${{ number_format($detalle->precio_libro, 2) }}</span></td>
                                        <td><span>{{ $pedido->metodo_pago }}</span></td>
                                        <td><span>{{ $pedido->forma_pago }}</span></td>

                                        <td><span>{{ $pedido->descripcion_tipo_entrega }}</span></td>
                                    </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="flex flex-col items-center w-full h-40 mt-4 justify-evenly">
        <a href="{{ route('colegio.reportes.alumno') }}" class="inline-block w-56 py-2 font-semibold text-center text-gray-400 align-middle bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
    </div>
@endsection

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#tabla_reporte_colegio").DataTable({
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            colReorder: false,
            scrollY: false,
            scrollX: false,
        })
    })
</script>
@endsection


