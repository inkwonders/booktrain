@extends('layouts.logged')
@section('title')
    Cierre de caja
@endsection
@section('reportesSelected')
    border-b-green--BK
@endsection
@section('css')
@endsection
@section('contenido')

<section class="flex flex-col items-center justify-center w-full h-auto px-4 py-4 min-h-custom">
    <div class="relative flex flex-col justify-center w-full h-auto border rounded-md min-h-interno border-color-gray--BK">
        <div class="flex justify-center w-full min-h-full p-10 text-center">
            <div class="flex flex-col w-full max-w-3xl">
                <div class="inline-block w-full text-center">
                    <img class="inline-block w-2/3 max-w-xs" src="{{ $logo_colegio }}" alt="">
                </div>
                <br>
                <br>
                <br>
                <div class="inline-block w-full text-left">
                    <p class="text-xl font-bold text-naranja">REPORTE CIERRE DEL DÍA DE {{ $nombre_colegio }} {{ $fecha }}</p>
                </div>
                <br>
                <div class="block w-full text-left">
                    <br>
                    <br>
                    <table class="w-full text-lg font-light text-grayBT">
                        <tr class="h-14">
                            <td class="px-2">Pago total</td>
                            <td class="px-2 text-right">${{ number_format($pago_efectivo, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14" style="color: #e2492d;">
                            <td class="px-2">Devoluciones</td>
                            <td class="px-2 text-right">${{ number_format($devoluciones, 2) }}</td>
                        </tr>
                        <tr class="h-14">
                            <td class="px-2">Total efectivo</td>
                            <td class="px-2 text-right">${{ number_format($total_efectivo, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14">
                            <td class="px-2">Pago con tarjeta</td>
                            <td class="px-2 text-right">${{ number_format($pago_tarjeta, 2) }}</td>
                        </tr>
                        <tr class="h-14">
                            <td class="px-2">Pago con terminal</td>
                            <td class="px-2 text-right">${{ number_format($pago_terminal, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14">
                            <td class="px-2">Tienda de conveniencia</td>
                            <td class="px-2 text-right">${{ number_format($pago_tienda, 2) }}</td>
                        </tr>
                        <tr class="h-14">
                            <td class="px-2">Pago en practicaja</td>
                            <td class="px-2 text-right">${{ number_format($pago_practicaja, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14">
                            <td class="px-2">Pago con transferencia</td>
                            <td class="px-2 text-right">${{ number_format($pago_transferencia, 2) }}</td>
                        </tr>
                        <tr class="font-bold h-14">
                            <td class="px-2">Total</td>
                            <td class="px-2 text-right">${{ number_format($total, 2) }}</td>
                        </tr>
                    </table>
                    <br>
                    <table class="w-full text-lg font-bold text-grayBT">
                        <tr class="bg-gray-100 h-14 print:h-auto">
                            <td class="px-2">Total de libros</td>
                            <td class="px-2 text-right">{{ $total_libros }}</td>
                        </tr>
                    </table>
                    <br>
                    <div class="block w-full text-left">
                        <br>
                        <br>
                        <table class="w-full text-lg font-light text-grayBT">
                            <tr>
                                <td class="px-2 font-bold">Cajero</td>
                                <td class="px-2 font-bold">Colegio</td>
                                <td class="px-2 font-bold">Apertura</td>
                                <td class="px-2 font-bold">Cierre</td>
                                <td class="px-2 font-bold">Cupon de descuento</td>
                                <td class="px-2 font-bold text-right">Efectivo</td>
                            </tr>

                            @foreach ($cajas as $key => $caja)
                                <tr class="h-14 {{ $key % 2 == 0? 'bg-gray-100':'' }}">
                                    <td class="px-2">{{ $caja->usuario->name }}</td>
                                    <td class="px-2">{{ $caja->colegio->codigo }}</td>
                                    <td class="px-2">{{ $caja->fecha_apertura}}</td>
                                    <td class="px-2">{{ $caja->fecha_cierre }}</td>
                                    <td class="px-2 text-right">${{ $caja->operaciones->last()? number_format($caja->operaciones->last()->total, 2) : 0.00 }}</td>
                                </tr>
                            @endforeach

                        </table>
                        <br>

                        <table class="w-full text-lg font-light text-grayBT">
                            <tr>
                                <td class="px-2 font-bold">Desglose efectivo:</td>
                                <td class="px-2 font-bold text-right">Cant.</td>
                                <td class="px-2"></td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 1000.00</td>
                                <td class="px-2 text-right">{{ $b1000 }}</td>
                                <td class="px-2 text-right">${{ number_format($b1000 * 1000, 2) }}</td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 500.00</td>
                                <td class="px-2 text-right">{{ $b500 }}</td>
                                <td class="px-2 text-right">${{ number_format($b500 * 500, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">$ 200.00</td>
                                <td class="px-2 text-right">{{ $b200 }}</td>
                                <td class="px-2 text-right">${{ number_format($b200 * 200, 2) }}</td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 100.00</td>
                                <td class="px-2 text-right">{{ $b100 }}</td>
                                <td class="px-2 text-right">${{ number_format($b100 * 100, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">$ 50.00</td>
                                <td class="px-2 text-right">{{ $b50 }}</td>
                                <td class="px-2 text-right">${{ number_format($b50 * 50, 2) }}</td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 20.00</td>
                                <td class="px-2 text-right">{{ $b20 }}</td>
                                <td class="px-2 text-right">${{ number_format($b20 * 20, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">Monedas</td>
                                <td class="px-2 text-right">{{ $conteo_monedas }}</td>
                                <td class="px-2 text-right">${{ number_format($monto_monedas, 2) }}</td>
                            </tr>
                        </table>

                        <table class="w-full text-lg font-bold text-grayBT">
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">Total efectivo</td>
                                <td class="px-2 text-right">${{ number_format($total_cajas, 2) }}</td>
                            </tr>
                        </table>
                        <br>
                        <div class="flex flex-col items-center w-full mt-20">
                            <form action="/cierre_del_dia/print" method="POST">
                                <input type="hidden" name="colegio_id" value="{{ $colegio_id }}">
                                <input type="hidden" name="fin" value="{{ $fecha_raw }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <button type="submit" formtarget="_blank" class="max-w-full mb-4 font-bold border rounded-md cursor-pointer h-11 w-60 border-greenBT text-greenBT hover:bg-greenBT hover:text-white">
                                    IMPRIMIR
                                </button>
                            </form>


                            <button class="max-w-full font-bold border rounded-md cursor-pointer h-11 w-60 border-grayBT text-grayBT hover:bg-grayBT hover:text-white" onclick="window.history.back()">
                                REGRESAR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
