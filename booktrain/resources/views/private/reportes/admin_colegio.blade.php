@extends('layouts.logged')
@section('title')
    Reporte del colegio {{ $colegio->nombre }}
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection
@section('contenido')
    <div class="w-full my-5 border rounded-md min-h-custom border-color-gray--BK">
        <h2 class="m-5 text-xl font-bold color-red--bk">REPORTE DE {{ $colegio->nombre }}, DEL {{ $inicio }} AL {{ $fin }}, GRADOS: {{ $grado }}, ISBN: {{ $isbn }}.</h2>

        <div class="table-responsive">
        <section class="flex flex-col w-full h-12 h-full px-4 py-4 pr-2 mb-5">

                <table id="tabla_reporte_colegio" class="stripe row-border hover order-column compact ">
                    <thead>
                        <tr>
                            <td>ISBN</td>
                            <td>Titulo</td>
                            <td>Grado</td>
                            <td>Sección</td>
                            <td>Obligatorio / Opcional</td>
                            <td>Precio a alumno</td>
                            <td>Pedidos (Cantidad)</td>
                            <td>Punto de reorden</td>
                            <td>Entrega (Cantidad)</td>
                            <td>Venta de mostrador</td>
                            <td>Devoluciones (Cantidad)</td>
                            <td>Venta real</td>
                            <td>Valor del pedido</td>
                            <td>Valor de devolución</td>
                            <td>Valor real de venta</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($libros as $libro)
                    <tr>
                        <td>{{ $libro->isbn }}</td>
                        <td>{{ $libro->titulo }}</td>
                        <td>{{ $libro->grado }}</td>
                        <td>{{ $libro->seccion }}</td>
                        <td>{{ $libro->obligatorio ? 'Si' : 'No' }}</td>
                        <td>${{ number_format($libro->precio, 2) }}</td>
                        <td>{{ $libro->pedidos }}</td>
                        <td>{{ $libro->reorden }}</td>
                        <td>{{ $libro->entrega }}</td>
                        <td>{{ $libro->mostrador }}</td>
                        <td>{{ $libro->devoluciones }}</td>
                        <td>{{ $libro->venta_real }}</td>
                        <td>${{ number_format($libro->valor_pedido, 2) }}</td>
                        <td>${{ number_format($libro->valor_devolucion, 2) }}</td>
                        <td>${{ number_format($libro->valor_venta_real, 2) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
    <div class="flex flex-col items-center w-full h-40 mt-4 justify-evenly">
        <a href="{{ route('colegio.reportes.general') }}" class="inline-block w-56 py-2 font-semibold text-center text-gray-400 align-middle bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
    </div>
@endsection

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#tabla_reporte_colegio").DataTable({
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            colReorder: false,
            scrollY: false,
            scrollX: false,
        })
    })
</script>
@endsection
