@extends('layouts.logged')
@section('title')
    Cierre de caja
@endsection
@section('reportesSelected')
    border-b-green--BK
@endsection
@section('css')
@endsection
@section('contenido')

<section class="w-full flex flex-col items-center justify-center h-auto min-h-custom px-4 py-4">
    <div class="relative flex flex-col justify-center w-full h-auto border rounded-md min-h-interno border-color-gray--BK">
        <div class="w-full min-h-full p-10 text-center flex justify-center">
            <div class="flex flex-col w-full max-w-3xl">
                <div class="inline-block w-full text-center">
                    <img class="inline-block w-2/3 max-w-xs" src="{{ $logo_colegio }}" alt="">
                </div>
                <br>
                <br>
                <br>
                <div class="inline-block w-full text-left">
                    <p class="text-xl font-bold text-naranja">REPORTE {{ $nombre_colegio }} {{ $fecha }}</p>
                </div>
                <br>
                <div class="block w-full text-left">
                    <br>
                    <br>
                    <table class="w-full text-lg font-light text-grayBT">
                        <tr class="h-14">
                            <td class="px-2">Pago efectivo</td>
                            <td class="px-2 text-right">${{ number_format($pago_efectivo, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14" style="color: #e2492d;">
                            <td class="px-2">Devoluciones</td>
                            <td class="px-2 text-right">${{ number_format($devoluciones, 2) }}</td>
                        </tr>
                        <tr class="h-14">
                            <td class="px-2">Total efectivo</td>
                            <td class="px-2 text-right">${{ number_format($total_efectivo, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14">
                            <td class="px-2">Pago con tarjeta</td>
                            <td class="px-2 text-right">${{ number_format($pago_tarjeta, 2) }}</td>
                        </tr>
                        {{-- <tr class="h-14">
                            <td class="px-2">Tienda de conveniencia (esto va aquí?)</td>
                            <td class="px-2 text-right">${{ number_format($pago_tienda, 2) }}</td>
                        </tr>
                        <tr class="bg-gray-100 h-14">
                            <td class="px-2">Pago en practicaja (esto va aquí?)</td>
                            <td class="px-2 text-right">${{ number_format($pago_practicaja, 2) }}</td>
                        </tr> --}}
                        <tr class="h-14">
                            <td class="px-2">Pago con transferencia</td>
                            <td class="px-2 text-right">${{ number_format($pago_transferencia, 2) }}</td>
                        </tr>
                        <tr class="font-bold bg-gray-100 h-14">
                            <td class="px-2">Total</td>
                            <td class="px-2 text-right">${{ number_format($total, 2) }}</td>
                        </tr>
                    </table>
                    <br>
                    <table class="w-full text-lg font-bold text-grayBT">
                        <tr class="bg-gray-100 h-14 print:h-auto">
                            <td class="px-2">Total de libros</td>
                            <td class="px-2 text-right">{{ $total_libros }}</td>
                        </tr>
                    </table>
                    <br>
                    <div class="block w-full text-left">
                        <br>
                        <br>
                        <table class="w-full text-lg font-light text-grayBT">
                            <tr>
                                <td class="px-2 font-bold">Desglose efectivo:</td>
                                <td class="px-2 font-bold text-right">Cant.</td>
                                <td class="px-2"></td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 500.00</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['500'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['500'] * 500, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">$ 200.00</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['200'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['200'] * 200, 2) }}</td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 100.00</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['100'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['100'] * 100, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">$ 50.00</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['50'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['50'] * 50, 2) }}</td>
                            </tr>
                            <tr class="h-14">
                                <td class="px-2">$ 20.00</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['20'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['20'] * 20, 2) }}</td>
                            </tr>
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">Monedas</td>
                                <td class="px-2 text-right">{{ $desglose_efectivo['cantidad_monedas'] }}</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['monedas'], 2) }}</td>
                            </tr>
                        </table>
                        <br>
                        <table class="w-full text-lg font-bold text-grayBT">
                            <tr class="bg-gray-100 h-14">
                                <td class="px-2">Total desglosado</td>
                                <td class="px-2 text-right">${{ number_format($desglose_efectivo['total_desglosado'], 2) }}</td>
                            </tr>
                        </table>
                        <br>
                        <div class="flex flex-col items-center w-full mt-20">
                            <form action="/cierre_caja/print" method="POST">
                                <input type="hidden" name="colegio" value="{{ $colegio }}">
                                <input type="hidden" name="fecha" value="{{ $fecha_raw }}">
                                <input type="hidden" name="caja" value="{{ $caja }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <button type="submit" formtarget="_blank" class="max-w-full mb-4 font-bold border rounded-md cursor-pointer h-11 w-60 border-greenBT text-greenBT hover:bg-greenBT hover:text-white">
                                    IMPRIMIR
                                </button>
                            </form>


                            <button class="max-w-full font-bold border rounded-md cursor-pointer h-11 w-60 border-grayBT text-grayBT hover:bg-grayBT hover:text-white" onclick="window.history.back()">
                                REGRESAR
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
