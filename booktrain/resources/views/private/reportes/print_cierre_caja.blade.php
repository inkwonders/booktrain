<!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <style>
         @font-face {
            font-family: 'Roboto', sans-serif;
            font-style: normal;
            font-weight: normal;
            src: url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');
        }
        .rob-font {
            font-family: 'Roboto', sans-serif;
        }
        body{
            width: 100%;
        }
        #padre{
            width: 65%;
            margin:auto;
        }
        table{
            width: 100%;
            position: relative;
            font-size: 20px;
        }
        td{
            height: 55px;
            font-weight: lighter;
            color: #808080;
        }
        .center{
            position: relative;          /* where the next element will be automatically positioned */
            display: inline-block;       /* causes element width to shrink to fit content */
            left: 50%;                   /* moves left side of image/element to center of parent element */
            transform: translate(50%)
        }
        img{
            max-width: 400px;
        }

     </style>

 </head>

 <body>
    <div id="padre">
        <br>
        <br>
        <br>
        <img src="{{$logo_colegio}}" alt="" class="center" height="100px">
        <p class="rob-font" style="font-size: 24px; font-weight: bold; color: #e2492d; text-transform:capitalize;">REPORTE {{ $nombre_colegio }} {{ $fecha }}</p>
        <br>
        <table class="rob-font">
            <tr>
                <td colspan="2">
                    <span>Pago efectivo</span>
                    <span style="float: right; transform: translate(0,50%);"> ${{ number_format($pago_efectivo, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2" style="color: #e2492d;"">
                    <span>Devoluciones</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($devoluciones, 2) }}</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span>Total efectivo</span>
                    <span  style="float: right; transform: translate(0,50%);">${{ number_format($total_efectivo, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Pago con tarjeta</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_tarjeta, 2) }}</span>
                </td>
            </tr>
            {{-- <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Tienda de conveniencia</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_tarjeta, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2">
                    <span>Pago en practicaja</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_practicaja, 2) }}</span>
                </td>
            </tr> --}}
            <tr>
                <td colspan="2">
                    <span>Pago con transferencia</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($pago_tarjeta, 2) }}</span>
                </td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td colspan="2" style="font-weight: bold">
                    <span>Total</span>
                    <span style="float: right; transform: translate(0,50%);">${{ number_format($total, 2) }}</span>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <br>

        <table class="rob-font">
            <tr>
                <td style="background-color:#f2f2f2; font-weight: bold">
                    <span>Total de libros</span>
                    <span style="float: right; transform: translate(0,50%);">{{ $total_libros }}</span>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <br>

        <table class="rob-font" cellspacing="0" cellpadding="0">
            <thead>
                <tr style="font-weight: bold; color:#808080;">
                    <th style="text-align:left;">Desglose efectivo</th>
                    <th style="text-align:left;">Cant.</th>
                    <th></th>
                </tr>
            </thead>
            <tr>
                <td>$500.00 </td>
                <td>{{ $desglose_efectivo['500'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['500'] * 500, 2)}}</td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td>$200.00 </td>
                <td>{{ $desglose_efectivo['200'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['200'] * 200, 2) }}</td>
            </tr>
            <tr>
                <td>$100.00 </td>
                <td>{{ $desglose_efectivo['100'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['100'] * 100, 2) }}</td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td>$50.00 </td>
                <td>{{ $desglose_efectivo['50'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['50'] * 50, 2) }}</td>
            </tr>
            <tr>
                <td>$20.00 </td>
                <td>{{ $desglose_efectivo['20'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['20'] * 20, 2) }}</td>
            </tr>
            <tr style="background-color:#f2f2f2;">
                <td>Monedas </td>
                <td>{{ $desglose_efectivo['cantidad_monedas'] }}</td>
                <td style="text-align: right;">${{ number_format($desglose_efectivo['monedas'], 2) }}</td>
            </tr>
        </table>
        <br><br><br>


        <table class="rob-font">
            <tr>
                <td style="background-color:#f2f2f2; font-weight: bold">
                    <span>Total desglosado</span>
                    <span style="float: right; transform: translate(0,50%);"> ${{ $desglose_efectivo['total_desglosado'] }}</span>
                </td>
            </tr>
        </table>
    </div>
 </body>
 </html>
