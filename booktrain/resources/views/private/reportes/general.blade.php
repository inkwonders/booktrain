@extends('layouts.logged')

@section('contenido')
<div class="relative flex flex-col justify-center w-full h-auto min-h-interno">
    <section class="w-full h-auto px-4 py-4">
        <div class="flex flex-col justify-between w-full h-auto px-8 py-6 border rounded-lg border-color-gray--BK">
            <div class="w-full h-auto">
                <span class="rob-bold color-red--bk font-18">REPORTE DE {{ $fecha_inicial }} A {{ $fecha_final }}, {{ $nombre_colegio }}, {{ $nombre_grado }} {{ $nombre_isbn }}, ({{ $resultados }} resultados)</span>

                <div class="block w-full h-auto mt-4 overflow-x-auto scrool_azul">
                    <div class="flex flex-row w-auto h-auto min-w-full">
                        <div class="flex flex-col min-w-full">
                            <table class="text-center color-gray--bk espacio_tablas">
                                <thead class="h-12 border-t border-b border-color-gray--BK">
                                    <tr>
                                        <th class="border border-l-2 border-r-2" colspan="8">PEDIDO</th>
                                        <th class="border border-l-2 border-r-2" colspan="3">CONTACTO</th>
                                        <th class="border border-l-2 border-r-2" colspan="3">FACTURACIÓN</th>
                                        <th class="border border-l-2 border-r-2" colspan="1">DISTRIBUCIÓN</th>
                                        {{-- <th class="border border-l-2 border-r-2" colspan="2">COLEGIO / PICK UP</th> --}}
                                    </tr>
                                    <tr>
                                        <th><span>No. Pedido</span></th>
                                        <th><span>Fecha</span></th>
                                        <th><span>Colegio</span></th>
                                        <th><span>Grado</span></th>
                                        <th><span>Sección</span></th>
                                        <th><span>ISBN</span></th>
                                        <th><span>Título</span></th>
                                        <th><span>Piezas</span></th>
                                        <th><span>Nombre alumno</span></th>
                                        <th><span>Estado pedido</span></th>
                                        <th><span>Nombre contacto</span></th>
                                        {{-- <th><span>Teléfono</span></th>
                                        <th><span>Correo</span></th>
                                        <th><span>RFC</span></th>
                                        <th><span>Razón social</span></th>
                                        <th><span>Correo</span></th> --}}
                                        <th><span>Precio libro</span></th>
                                        <th><span>Método de pago</span></th>
                                        <th><span>Forma de pago</span></th>
                                        {{-- <th><span>Comisión</span></th>
                                        <th><span>Costo envío</span></th>
                                        <th class="fondo_amarillo"><span>Factura</span></th> --}}
                                        <th><span>Tipo de entrega</span></th>
                                        {{-- <th><span>Dirección</span></th>
                                        <th><span>Colonia</span></th>
                                        <th><span>C.P</span></th>
                                        <th><span>Estado</span></th>
                                        <th><span>Municipio</span></th>
                                        <th><span>Referencia</span></th>
                                        <th><span>Fecha de envío</span></th>
                                        <th><span>Tipo de envío</span></th>
                                        <th><span>No. de guía</span></th>
                                        <th><span>Estado del envío</span></th>
                                        <th class="fondo_amarillo"><span>Nombre</span></th>
                                        <th class="fondo_amarillo"><span>Fecha</span></th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($pedidos as $index_pedido => $pedido)
                                    @foreach ($pedido->resumenes as $index_resumen => $resumen)
                                        @foreach ($resumen->detalles as $index_detalle => $detalle)
                                        <tr>
                                            <td><span>{{ $pedido->serie }}{{ $pedido->folio }}</span></td>
                                            <td><span>{{ $pedido->created_at->format('d / m / Y') }}</span></td>
                                            <td><span>{{ $pedido->colegio->nombre }}</span></td>
                                            <td><span>{{ $resumen->paquete->nivel->nombre }}</span></td>
                                            <td><span>{{ $resumen->paquete->nivel->seccion->nombre }}</span></td>
                                            <td><span>{{ $detalle->libro->isbn }}</span></td>
                                            <td><span>{{ $detalle->libro->nombre }}</span></td>
                                            <td><span>{{ $detalle->cantidad }}</span></td>
                                            <td><span>{{ $resumen->nombre_completo_alumno }}</span></td>
                                            <td><span>{{ $pedido->status }}</span></td>
                                            <td><span>{{ $pedido->nombre_contacto }} {{ $pedido->apellidos_contacto }}</span></td>
                                            {{-- <td><span>{{ $pedido->celular_contacto }}</span></td>
                                            <td><span>{{ $pedido->user->email }}</span></td>
                                            <td><span>{{ isset($pedido->datosFactura) ? $pedido->datosFactura->rfc : '' }}</span></td>
                                            <td><span>{{ isset($pedido->datosFactura) ? $pedido->datosFactura->razon_social : '' }}</span></td>
                                            <td><span>{{ isset($pedido->datosFactura) ? $pedido->datosFactura->correo : '' }}</span></td> --}}
                                            <td><span>${{ number_format($detalle->precio_libro, 2) }}</span></td>
                                            <td><span>{{ $pedido->metodo_pago }}</span></td>
                                            <td><span>{{ $pedido->forma_pago }}</span></td>
                                            {{-- <td><span>{{ $index_detalle == 0 ? '$' . number_format($pedido->comision, 2) : '' }}</span></td>
                                            <td><span>{{ $index_detalle == 0 ? (isset($pedido->envio) ? '$' . number_format($pedido->envio->costo, 2) : '') : ''}}</span></td>
                                            <td class="fondo_amarillo"></td> --}}
                                            <td><span>{{ $pedido->descripcion_tipo_entrega }}</span></td>
                                            {{-- <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->calle . ' #' . $pedido->direccion->no_exterior . ($pedido->direccion->no_interior != '' ? ', Int. ' . $pedido->direccion->no_interior : '' ) : '' }}</span></td>
                                            <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->colonia : '' }}</span></td>
                                            <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->cp : '' }}</span></td>
                                            <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->estado->descripcion : ''}}</span></td>
                                            <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->municipio->descripcion : ''}}</span></td>
                                            <td><span>{{ isset($pedido->direccion) ? $pedido->direccion->referencia : '' }}</span></td>
                                            <td><span>{{ isset($pedido->envio) ? (isset($pedido->envio->fecha_envio) ? $pedido->envio->fecha_envio->format('d / m / Y') : '') : '' }}</span></td>
                                            <td><span>{{ isset($pedido->envio) ? $pedido->envio->paqueteria  : ''}}</span></td>
                                            <td><span>{{ isset($pedido->envio) ? $pedido->envio->guia  : ''}}</span></td>
                                            <td><span>{{ isset($pedido->envio) ? $pedido->envio->status  : ''}}</span></td>
                                            <td class="fondo_amarillo"></td>
                                            <td class="fondo_amarillo"></td> --}}
                                        </tr>
                                        @endforeach
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
