<div class="relative flex justify-between w-full mb-4">
    <div class="relative flex justify-between w-full py-4 mb-5">
        <div class="relative flex items-center w-auto ">
            <input wire:model="fecha_inicial" placeholder="FECHA INICIAL" class="relative w-40 p-2 mr-2 border rounded-md color-gray--bk" type="text" id="inicio">
            <input wire:model="fecha_final" placeholder="FECHA FINAL" class="relative w-40 px-2 mr-2 border rounded-md h-11 color-gray--bk" type="text" id="fin">
        </div>
        <div class="relative flex items-center w-auto ">
            <a href="{{ route('pedidos_enviados') }}" class="flex items-center justify-center w-48 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold h-11">+ PEDIDOS ENVIADOS</a>
            &nbsp;<a href="{{ route('envios_parciales') }}" class="flex items-center justify-center w-48 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold h-11">ENVÍOS PARCIALES</a>
        </div>
    </div>
</div>
