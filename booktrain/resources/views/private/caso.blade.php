@extends('layouts.logged')

@section('title')
    Caso soporte
@endsection

@section('css')
@endsection

@section('soporteSelected')
    border-b-green--BK
@endsection

@section('contenido')

<form action="/caso" method="POST" class="w-full min-h-custom relative px-4 pt-4 pb-10" enctype="multipart/form-data">
    <div class="p-4 py-6 border border-color-gray--BK rounded-lg text-center">
        <div class="text-left">
            <div class="w-full text-center">
                <h4 class="color-blue--bk font-bold text-xl">¿ENVIAR EL CASO?</h4>
            </div>

            @isset($caso)
                <p class="mt-8 color-gray--bk"><b>CASO:</b> {{ $caso }}</p>
            @endisset

            @isset($motivo)
                <p class="mt-8 color-gray--bk"><b>MOTIVO:</b> {{ $motivo }}</p>
            @endisset

            @isset($descripcion)
                <p class="mt-8 color-gray--bk">
                    <b>DESCRIPCIÓN:</b>
                    <br>
                    {{ $descripcion }}
                </p>
            @endisset

            @isset($archivos)
                <p class="mt-8 color-gray--bk">
                    <b>ARCHIVOS ADJUNTOS:</b>
                    <br>
                    202100029_img.jpg / 202100029_img.jpg / 202100029_img.jpg / 202100029_img.jpg
                </p>
            @endisset
        </div>
    </div>
    <button class="w-full mt-6 mb-3 text-white font-bold text-xl border border-gray-200 rounded-md background-green--bk p-3 hover:bg-gray-400 hover:text-white cursor-pointer">SI</button>
    <a href="{{ route('soporte') }}" class="flex justify-center w-full mb-8 font-bold text-xl border border-gray-200 rounded-md color-gray--bk p-3 hover:bg-gray-400 hover:text-white hover:no-underline cursor-pointer">NO</a>
</form>

@endsection

@section('js-interno')
@endsection
