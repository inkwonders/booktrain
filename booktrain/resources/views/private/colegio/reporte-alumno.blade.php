@extends('layouts.logged')
@section('title', 'Colegio | Reporte por alumno')

@section('contenido')
@hasrole("Colegio")
<div class="relative flex flex-col w-full p-12 border rounded-md h-custom min-h-custom px border-color-gray--BK">
    <span class="text-2xl text-red-500 font-rob-bold">{{ $colegio->nombre }}</span>
    <div class="relative flex flex-row w-full h-full mt-6">
        <!-- Parte izq -->
        <div class="relative flex items-center justify-center w-1/2">
            <img class="w-72" src="{{ $colegio->logo }}" alt="Logo del colegio {{ $colegio->nombre }}">
        </div>
        <!-- División -->
        <div class="border-r border-color-gray--BK"></div>
        <!-- Parte derecha -->
        <div class="relative flex justify-center w-1/2">
            <reportes-colegio-alumno csrf="{{ csrf_token() }}"></reportes-colegio-alumno>
        </div>
    </div>
    <div class="relative flex flex-row">
        @include('errors.validation')
    </div>
</div>
@endhasrole
@endsection

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection

