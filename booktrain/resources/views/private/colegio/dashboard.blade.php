@extends('layouts.logged')
@section('title', 'Colegio | Reportes')

@section('contenido')
@hasrole("Colegio")
<div class="relative flex flex-col w-full p-12 border rounded-md h-custom min-h-custom px border-color-gray--BK">
    <span class="text-2xl text-red-500 font-rob-bold">BIENVENIDO | {{ $colegio->nombre }}</span>
    <div class="relative flex flex-row w-full h-full mt-6">
        <!-- Parte izq -->
        <div class="relative flex items-center justify-center w-1/2">
            <img class="w-72" src="{{ $colegio->logo }}" alt="Logo del colegio {{ $colegio->nombre }}">
        </div>
        <!-- División -->
        <div class="border-r border-color-gray--BK"></div>
        <!-- Parte derecha -->
        <div class="relative flex justify-center w-1/2">
            <div class="flex items-center justify-center w-1/2">
                <a href="{{ route('colegio.reportes.general') }}" class="boton-reporte appearance-none  text-white relative block text-center py-1 px-3 w-9/12 my-1.5 rounded-md color-gray--bk border border-solid border-gray-400 hover:border-green-500 hover:text-white hover:bg-greenBT lg:text-lg md:text-base sm:text-sm font-bold uppercase justify-center flex flex-col align-center items-center">
                    <img src="{{ asset('/img/svg/documento.svg') }}" class="relative w-1/3 mb-4 documento h-1/3 "/>
                    Reporte del colegio
                </a>
            </div>

            <div class="flex items-center justify-center w-1/2">
                <a href="{{ route('colegio.reportes.alumno') }}" class="boton-reporte appearance-none  text-white relative block text-center py-1 px-3 w-9/12 my-1.5 rounded-md color-gray--bk border border-solid border-gray-400 hover:border-green-500 hover:text-white hover:bg-greenBT lg:text-lg md:text-base sm:text-sm font-bold uppercase justify-center flex flex-col align-center items-center">
                    <img src="{{ asset('/img/svg/documento.svg') }}" class="relative w-1/3 mb-4 documento h-1/3 "/>
                    Reporte por alumno
                </a>
            </div>
        </div>
    </div>
</div>
@endhasrole
@endsection
