@extends('layouts.logged')

@section('title')
    Inicio
@endsection

@section('css')
<style>
.activado{
    background-color: #81b84e;
}
</style>


@endsection

@section('inicioSelected')
    border-b-green--BK
@endsection

@section('contenido')

<div class="w-full min-h-custom flex justify-center items-center">

    <div class="flex flex-col w-10/12 lg:w-4/12 items-center">

        <h1 class="text-2xl text-grayBT font-rob-medium lg:font-bold">CÓDIGO DE COLEGIO</h1>
        <form action="{{ route('colegio-informacion') }}" method="POST" class="w-full">
            @csrf
            <div class="relative text-gray-600 focus-within:text-gray-400 w-full">

                <input type="text" name="codigo" id="codigo" class="py-2 text-sm text-gray-600 rounded-md lg:rounded-xl pl-10 focus:outline-none focus:bg-white border-2 border-gray-400 focus:border-gray-500 w-full focus:z-10 lg:text-lg sm:text-sm md:text-base my-5 focus:ring-0" autocomplete="off" onkeyup="habilitar();">

            </div>

            <!-- ERROR SPAN -->
            <div class="w-full px-4 text-center">

                @if (session('error'))
                    <span class="text-sm color-red--bk font-bold">{{ session('error') }}</span>
                @else
                    <span class="text-sm color-red--bk font-bold">&nbsp;</span>
                @endif

            </div>

            <div class="flex justify-center content-center w-full mt-2">
                <button id="boton_entrar" type="submit" class="outline-none appearance-none relative block text-center w-full bg-greenBT py-3 lg:py-3 lg:px-3 lg:bg-gray-400 lg:w-6/12 lg:my-5 rounded-md color-white--bk border border-solid border-transparent hover:background-white--bk lg:text-lg sm:text-sm md:text-base font-bold">
                    ENTRAR
                </button>
            </div>

        </form>

    </div>
</div>


@endsection

@section('js-interno')

<script>
    function habilitar(event) {
        var element = document.getElementById("boton_entrar");
        if(document.getElementById("codigo").value.length>=3){
            element.classList.add("activado");
        }else{
            element.classList.remove("activado");
        }

    }
</script>


@endsection

