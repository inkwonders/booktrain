@extends('layouts.logged')
@section('title')
    Impresion Pedidos
@endsection

@section('impresionPedidosSelected')
    border-b-green--BK
@endsection

{{-- @section('reportesSelected')
    border-b-green--BK
@endsection --}}

@section('contenido')
    <section class="w-full flex flex-col h-auto min-h-custom px-4 py-4">
        @hasrole('Admin|Super Admin')
        {{-- @if (auth()->user()->hasDirectPermission('menu_reportes')) --}}
        <impresion-pedidos></impresion-pedidos>
        {{-- @endif --}}
        @endhasrole
    </section>
@endsection

@section('js-interno')
<script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



@endsection





