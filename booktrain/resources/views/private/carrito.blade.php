@extends('layouts.logged')

@section('title')
    Carrito
@endsection

@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('/css/login.css') }}">     --}}

@endsection

@section('carritoSelected')
    background-green--bk
@endsection
                                                                      

@section('contenido')
@if (isset($pedido_id))
    <carrito :pedido_id="{{$pedido_id}}" csrf="{{ csrf_token() }}"></carrito>
@else
    <carrito-vacio></carrito-vacio>
@endif




@endsection

@section('js-interno')
@endsection
