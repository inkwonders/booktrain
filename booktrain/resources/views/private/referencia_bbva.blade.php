@hasrole('Admin|Super Admin')
@extends('layouts.logged')

@section('title')
    Referencia BBVA
@endsection

@section('referencia_bbvaSelected')
    border-b-green--BK
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>

        #example2_info{
            padding: 1.3em;
        }
                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }
        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }
        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }

    </style>
@endsection

@section('contenido')
<section class="w-full flex flex-col min-h-custom h-full pl-2 py-4">
    <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK">
        <div class="w-11/12 flex justify-between p-10"><h2 class="color-red--bk font-bold text-xl">REFERENCIA BBVA</h2></div>
        <referencia-bbva csrf="{{ csrf_token() }}"></referencia-bbva>
    </div>
</section>


@endsection

@section('js-interno')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>

    <script>

        // $(document).ready(function() {

        // });

        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            // responsive: {
            //     details: {
            //         type: 'column'
            //     }
            // },
            // columnDefs: [
            //     { responsivePriority: 1, targets: -1 },
            //     { className: 'dtr-control', orderable: false }
            // ],
            colReorder: false,
            scrollY: false,
            scrollX: false,
        } );


    </script>

@endsection
@endhasrole
