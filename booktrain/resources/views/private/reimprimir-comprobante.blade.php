@extends('layouts.logged')

@section('title')
    Recibo de Pago
@endsection

@section('css')

@endsection

@section('misPedidosSelected')
    border-b-green--BK
@endsection

@section('contenido')
    <reimprimir-comprobante pedido="{{$id}}"></reimprimir-comprobante>
@endsection

@section('js-interno')
@endsection
