@extends('layouts.logged')

@section('title')
Pagar pedido
@endsection

@section('inicioSelected')
    border-b-green--BK
@endsection

@section('contenido')
@hasrole('Cliente')

    @if(env('NETPAY_ENABLED', 0) == 1 && env('NETPAY_SANDBOX_MODE', 1) == 1)
    <span class="px-6 py-3 mt-2 font-bold text-white uppercase bg-red-600 rounded-full">SANDBOX NETPAY MODE ON</span>
    @endif

    @if(env('OPENPAY_ENABLED', 0) == 1 && env('OPENPAY_SANDBOX_MODE', 1) == 1)
    <span class="px-6 py-3 mt-2 font-bold text-white uppercase bg-red-600 rounded-full">SANDBOX OPENPAY MODE ON</span>
    @endif

<form-pagos id_pedido="{{ $pedido->id }}" pasarelas_json='{!! json_encode( $pasarelas ) !!}'></form-pagos>
@endhasrole

@hasrole('Venta Movil')
    <form-venta-movil id_pedido="{{ $pedido->id }}"></form-venta-movil>
@endhasrole

@endsection

@push('script_stack')
<script src="https://sdk.mercadopago.com/js/v2"></script>
@if(env('NETPAY_ENABLED', 0) == 1)
    @if(env('NETPAY_SANDBOX_MODE', 1) == 0)
    <script type="text/javascript" src="{{ env('NETPAY_JS_PROD', 'https://docs.netpay.mx/cdn/v1.3/netpay.min.js') }}"></script>
    <script type="text/javascript">NetPay.setApiKey("{{ env('NETPAY_PK') }}");NetPay.setSandboxMode(false);</script>
    @else
    <script type="text/javascript" src="{{ env('NETPAY_JS_SANDBOX', 'https://gateway-154.netpaydev.com') }}"></script>
    <script type="text/javascript">NetPay.setApiKey("{{ env('NETPAY_PK_SANDBOX') }}");NetPay.setSandboxMode(true);</script>
    @endif
@endif

@if(env('OPENPAY_ENABLED', 0) == 1)

{{-- habilitado: {{ env('OPENPAY_ENABLED', 0) }}
habilitado: {{ env('OPENPAY_SANDBOX_MODE', 0) }} --}}
<script type="text/javascript" src="{{ env('OPENPAY_JS', 'https://js.openpay.mx/openpay.v1.min.js') }}"></script>
<script type="text/javascript" src="{{ env('OPENPAY_JS_DATA', 'https://js.openpay.mx/openpay-data.v1.min.js') }}"></script>
@endif

@endpush
