@extends('layouts.logged')
@section('title')
    Reporte Envios
@endsection
@section('referencia_envios_generalSelected')
    border-b-green--BK
@endsection

@section('contenido')
    <section class="flex flex-col w-full h-auto px-2 py-4">
        @hasrole("Admin|Super Admin")
        <div class="relative flex flex-col items-center w-full border rounded-md h-autos border-color-gray--BK min-h-interno">
            <div class="relative flex flex-col w-full p-10">
                <span class="text-xl uppercase color-red--bk font-rob-bold">Envíos</span>
                <br>

                <div class="data-table-diseño">
                    <livewire:envios-reporte-general/>
                </div>
            </div>
        </div>

        @endhasrole
    </section>
@endsection

@section('js-interno')

    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>

        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                element_input = document.querySelector('[placeholder="Buscar"]');
                element_input.focus();
            })
        });

        $(document).ready(function() {
            // Datepicker
            $("#inicio").datepicker({
                dateFormat: 'yy-mm-dd',
                beforeShow: function() {
                    $("#inicio").datepicker("option", "maxDate", $("#fin").datepicker("getDate"))
                },
                onSelect: function(dateText, inst) {
                    Livewire.emit('setFecha', 'inicial', dateText)
                }
            });

            $("#fin").datepicker({
                dateFormat: 'yy-mm-dd',
                beforeShow: function() {
                    $("#fin").datepicker("option", "minDate", $("#inicio").datepicker("getDate"))
                },
                onSelect: function(dateText, inst) {
                    Livewire.emit('setFecha', 'inicial', dateText)
                }
            });
        });

    </script>
@endsection
