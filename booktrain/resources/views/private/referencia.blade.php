@extends('layouts.logged')

@section('title')
    Referencia Bancaria
@endsection

@section('css')
    <style>
        .list-line li {
            position: relative;
        }
        .list-line li::before {
            content: "";
            position: absolute;
            top: 0.5rem;
            left: -6px;
            width: 3px;
            height: 1px;
            background-color: var(--main-gray--bk);
        }


        /* Estilos que se aplican en la impresión del documento */
        @media print {
            html * {
                visibility: hidden;
            }
            #print, #print * {
                visibility: visible;
            }
            #print {
                margin: auto;
                padding: 0;
            }
            .list-line li {
                position: relative;
            }
            .list-line li::before {
                content: "";
                position: absolute;
                top: 0.5rem;
                left: -6px;
                width: 3px;
                height: 1px;
                background-color: var(--main-gray--bk);
            }
            .boton_imprimir {
                display: none !important;
                visibility: hidden !important;
            }
            footer, #booktrain {
                display: none !important;
                visibility: hidden !important;
            }
            .min-h-custom {
                min-height: 100vh !important;
                width: 100% !important;
                padding-left: 4rem !important;
                padding-right: 4rem !important;
                display: flex !important;
                justify-content: center !important;
                align-items: center !important;
            }
            .contenido_menu {
                display: none !important;
            }
        }


    </style>
@endsection

@section('inicioSelected')
    border-b-green--BK
@endsection

@section('contenido')

@if(session('error') != null)
    <span>{{ session('error') }}</span>
@endif

<referencia-pago/>

@endsection

@section('js-interno')
@endsection
