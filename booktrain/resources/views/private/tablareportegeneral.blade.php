@extends('layouts.logged')

@section('title')
    Reporte General Administrador
@endsection
@section('reportesSelected')
    border-b-green--BK
@endsection

@section('reportesSelected')
    border-b-green--BK
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>

        #example2_info{
            padding: 1.3em;
        }
                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }
        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }
        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }

    </style>
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.css">
     <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
@endsection

@section('contenido')

    @if(session('error') != null)
        <span>{{ session('error') }}</span>
    @endif

    <section class="w-full h-auto py-4">
        <div class="w-full h-auto py-6 px-8 border border-color-gray--BK rounded-lg flex flex-col justify-between">
            <div class="w-full h-auto">

                <h2 class="color-red--bk font-bold text-xl">REPORTE DE {{$inicio}} A {{$fin}}, {{($colegio != null ? $colegio->nombre : 'Todos')}}, GRADOS: @if($grado_id == 0 || $grado_id == '0'){{$grado = 'TODOS'}}@else{{$grado = \App\Models\Paquete::where('nivel_id', $grado_id)->first()->nombre}}@endif, ISBN: {{ ($isbn_req != 0 ? $isbn_req : 'TODOS')}}.</h2>

                <div class="w-full h-auto overflow-x-auto scrool_azul block mt-4">
                    <div class="w-auto min-w-full h-auto flex flex-row">
                        <div class="table-responsive">
                        <table class="min-w-full tabla-p-0">
                            <tr>
                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="table-responsive">
                                        <table id="example2" class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>No. Pedido</span>
                                                    </th>
                                                    <th>
                                                        <span>Fecha</span>
                                                    </th>
                                                    <th >
                                                        <span>Colegio</span>
                                                    </th>
                                                    <th>
                                                        <span>Grado</span>
                                                    </th>
                                                    <th>
                                                        <span>ISBN</span>
                                                    </th>
                                                    <th>
                                                        <span>Título</span>
                                                    </th>
                                                    <th>
                                                        <span>Piezas</span>
                                                    </th>
                                                    <th>
                                                        <span>Nombre del alumno</span>
                                                    </th>
                                                    <th>
                                                        <span>Estatus</span>
                                                    </th>
                                                    <th>
                                                        <span>Nombre del contacto</span>
                                                    </th>
                                                    <th>
                                                        <span>Teléfono</span>
                                                    </th>
                                                    <th>
                                                        <span>Correo</span>
                                                    </th>
                                                    <th>
                                                        <span>RFC</span>
                                                    </th>
                                                    <th>
                                                        <span>Razón social</span>
                                                    </th>
                                                    <th>
                                                        <span>Monto</span>
                                                    </th>
                                                    <th>
                                                        <span>Método de pago</span>
                                                    </th>
                                                    <th>
                                                        <span>Forma de pago</span>
                                                    </th>
                                                    <th>
                                                        <span>Comisión</span>
                                                    </th>
                                                    <th>
                                                        <span>Costo de envío</span>
                                                    </th>
                                                    <th class="fondo_amarillo">
                                                        <span>Factura</span>
                                                    </th>
                                                    <th>
                                                        <span>Tipo de entrega</span>
                                                    </th>
                                                    <th>
                                                        <span>Calle</span>
                                                    </th>
                                                    <th>
                                                        <span>No exterior</span>
                                                    </th>
                                                    <th>
                                                        <span>No interior</span>
                                                    </th>
                                                    <th>
                                                        <span>Colonia</span>
                                                    </th>
                                                    <th>
                                                        <span>C.P.</span>
                                                    </th>
                                                    <th>
                                                        <span>Estado</span>
                                                    </th>
                                                    <th>
                                                        <span>Municipio</span>
                                                    </th>
                                                    <th>
                                                        <span>Referencia</span>
                                                    </th>
                                                    <th>
                                                        <span>Fecha de envío</span>
                                                    </th>
                                                    <th>
                                                        <span>Tipo de envío</span>
                                                    </th>
                                                    <th>
                                                        <span>No de Guía</span>
                                                    </th>
                                                    <th>
                                                        <span>Estatus</span>
                                                    </th>
                                                    <th class="fondo_amarillo">
                                                        <span>Nombre</span>
                                                    </th>
                                                    <th class="fondo_amarillo">
                                                        <span>Fecha</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                    @foreach ($detalles as $detalle)
                                                        @if ($isbn_req != "0" && $detalle->libro->isbn != $isbn_req)
                                                            @continue
                                                        @endif

                                                        @if($detalle->resumenPedido->pedido->status != 'CARRITO')

                                                            <tr>
                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->pedido->serie . $detalle->resumenPedido->pedido->folio }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ date_format($detalle->resumenPedido->pedido->created_at,"d / m / Y") }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->pedido->colegio->nombre }}</span>
                                                                </td>

                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->paquete->nivelColegio->nombre }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->libro->isbn }}</span>
                                                                </td>
                                                                <td >
                                                                    <span>{{ $detalle->libro->nombre }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->cantidad }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->nombre_alumno.' '.$detalle->resumenPedido->paterno_alumno.' '.$detalle->resumenPedido->materno_alumno }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->status) }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->pedido->nombre_contacto.' '.$detalle->resumenPedido->pedido->apellidos_contacto }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->celular_contacto != null ? $detalle->resumenPedido->pedido->celular_contacto : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->correo : '') }}</span> --}}
                                                                    <span>{{ $detalle->resumenPedido->pedido->user->email }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->rfc : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->datosFactura != null ? $detalle->resumenPedido->pedido->datosFactura->razon_social : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ number_format($detalle->resumenPedido->pedido->total, 2) }}</span> --}}
                                                                    <span>{{ number_format($detalle->precio_libro, 2) }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->metodo_pago  != null ? $detalle->resumenPedido->pedido->metodo_pago : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ $detalle->resumenPedido->pedido->forma_pago }}</span>
                                                                </td>
                                                                <td>
                                                                    @if($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id)
                                                                    <span>{{ $detalle->resumenPedido->pedido->comision }}</span>
                                                                    @else
                                                                    @endif
                                                                </td>

                                                                <td>

                                                                    @if($detalle->resumenPedido->pedido->getLibrosAttribute()->first()->id == $detalle->libro->id)
                                                                    <span>{{ ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->costo : '') }}</span>
                                                                    @else
                                                                    @endif
                                                                </td>

                                                                <td class="fondo_amarillo">
                                                                    <span></span>
                                                                </td>
                                                                <td>
                                                                    @if($detalle->resumenPedido->pedido->tipo_entrega  == 1)
                                                                    <span>Colegio</span>
                                                                    @elseif ($detalle->resumenPedido->pedido->tipo_entrega  == 2)
                                                                    <span>Domicilio</span>
                                                                    @elseif ($detalle->resumenPedido->pedido->tipo_entrega  == 3)
                                                                    <span>Pick-Up</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->calle : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_exterior : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->no_interior : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->colonia : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->cp : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->estado->descripcion : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->municipio->descripcion : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    <span>{{ ($detalle->resumenPedido->pedido->direccion != null ? $detalle->resumenPedido->pedido->direccion->referencia : '') }}</span>
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ ($detalle->enviado == 1 ? $detalle->envio->fecha_envio :
                                                                             ($detalle->resumenPedido->pedido->envio != null ? $detalle->resumenPedido->pedido->envio->fecha_envio : '' )) }}</span> --}}


                                                                    {{-- @if($detalle->envio != null) --}}
                                                                    <span>{{ ($detalle->enviado == 1 ? $detalle->envio->fecha_envio :
                                                                              ($detalle->resumenPedido->pedido->envio != null ?
                                                                              $detalle->resumenPedido->pedido->envio->fecha_envio : '' )) }}</span>
                                                                    {{-- @else
                                                                    @endif --}}
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ ($detalle->envio != null ? $detalle->envio->paqueteria : '') }}</span> --}}
                                                                    <span>{{ ($detalle->enviado == 1 ? $detalle->envio->paqueteria :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->paqueteria : '' )) }}</span>
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ ($detalle->envio != null ? $detalle->envio->guia : '') }}</span> --}}
                                                                    <span>{{ ($detalle->enviado == 1 ? $detalle->envio->guia :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->guia : '' )) }}</span>
                                                                </td>
                                                                <td>
                                                                    {{-- <span>{{ ($detalle->envio != null ? $detalle->envio->status : '') }}</span> --}}
                                                                    <span>{{ ($detalle->enviado == 1 ? $detalle->envio->status :
                                                                        ($detalle->resumenPedido->pedido->envio != null ?
                                                                        $detalle->resumenPedido->pedido->envio->status : '' )) }}</span>
                                                                </td>
                                                                <td class="fondo_amarillo">
                                                                    <span ></span>
                                                                </td>
                                                                <td class="fondo_amarillo">
                                                                    <span ></span>
                                                                </td>
                                                            </tr>
                                                        @else

                                                        @endif
                                                    @endforeach



                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full h-40 flex flex-col justify-evenly items-center mt-4">
                <form action="api_web/excel/reporte-admin-general" method="POST">
                    <input type="hidden" name="inicio"  id="inicio"  value="{{$inicio}}">
                    <input type="hidden" name="fin" id="fin" value="{{$fin}}">
                    <input type="hidden" name="grado" id="grado" value="{{$grado_id}}">
                    <input type="hidden" name="isbn" id="isbn" value="{{$isbn_req}}">
                    {{-- <input type="hidden" name="colegio" id="colegio" value="{{$colegio->id}}"> --}}
                    <input type="hidden" name="colegio" id="colegio" value="{{($colegio != null ? $colegio->id : '')}}">
                    <input type="hidden" name="estatus" id="estatus" value="{{$estatus}}">
                    <input type="hidden" name="_token" id="_token" value="{!! csrf_token() !!}"/>
                    <button type="submit" class="h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">EXPORTAR EXCEL</button>
                </form>
                <form action="reportes/" method="POST">
                <button class="h-12 w-56 bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</button>
                </form>
            </div>
        </div>
    </section>

@endsection

@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

<script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

    $(document).ready(function() {

            // var table = $('#example2').DataTable();
            // table.order( [ 0, 'desc' ] ).draw();


        });
        $.extend( true, $.fn.dataTable.defaults, {
            "processing": true,
            "language": {
                "decimal": ",",
                "thousands": ".",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoPostFix": "",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "loadingRecords": "Cargando...",
                'processing': "Cargando datos ...",
                "lengthMenu": "<div class=\"flex items-center\">Mostrar <div class=\"relative inline-block border border-gray-200 rounded-md cursor-pointer overflow-hidden cursor-pointer mx-2\"><label class=\"select_motivos cursor-pointer\"></label> _MENU_ </div> registros</div>",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "search": "Buscar:",
                "searchPlaceholder": "",
                "zeroRecords": "No se encontraron resultados",
                "emptyTable": "Ningún dato disponible en esta tabla",
                "aria": {
                    "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                //only works for built-in buttons, not for custom buttons
                "buttons": {
                    "create": "Nuevo",
                    "edit": "Cambiar",
                    "remove": "Borrar",
                    "copy": "Copiar",
                    "csv": "fichero CSV",
                    "excel": "tabla Excel",
                    "pdf": "documento PDF",
                    "print": "Imprimir",
                    "colvis": "Visibilidad columnas",
                    "collection": "Colección",
                    "upload": "Seleccione fichero...."
                },
                "select": {
                    "rows": {
                        _: '%d filas seleccionadas',
                        0: 'clic fila para seleccionar',
                        1: 'una fila seleccionada'
                    }
                }
            },
            // responsive: {
            //     details: {
            //         type: 'column'
            //     }
            // },
            // columnDefs: [
            //     { responsivePriority: 1, targets: -1 },
            //     { className: 'dtr-control', orderable: false }
            // ],
            colReorder: false,
            scrollY: false,
            scrollX: false,
        } );


    </script>

@endsection
