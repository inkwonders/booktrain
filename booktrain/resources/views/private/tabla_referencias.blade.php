@extends('layouts.logged')
@section('title')
    Referencias BBVA
@endsection
@section('tabla_referencia_pedidosEnviadosSelected')
    border-b-green--BK
@endsection


@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    {{-- <style>
        p{
            font-family: roboto-medium;
            color: var(--main-gray--bk);
            /* font-weight: bold; */
         }
         .wrapper {
        overflow: hidden;
        position: relative;
        width: 10rem; /* real length of dropdown */
        }

        .wrapper:after {
        content: "▼";
        font-size: 0.8rem;
        pointer-events: none; /* ▼ click triggers dropdown */
        position: absolute;
        right: 0.2rem;
        top: 0.3rem;
        z-index: 1;
        }
        select {
        /* remove default caret */
        -webkit-appearance: none; /* webkit browsers */
        -moz-appearance: none; /* firefox */
        appearance: none; /* modern browsers */
        width: 100%;
        }

        select::-ms-expand {
        display: none; /* remove default caret for ie */
        }

                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }

        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }

        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }

    </style> --}}

    {{-- <style>
        .data-table-diseño {
            width: 100%;
        }
        .data-table-diseño [wire\:id] {
            width: 100%;
        }
        .data-table-diseño span[aria-current] > span {
            background-color: var(--main-blue--bk);
            color: white;
        }
        .data-table-diseño span[wire\:key] > button:hover {
            background-color: var(--main-green--bk);
            color: white;
        }
        .data-table-diseño input {
            padding-left: 0.5rem;
            padding-right: 2rem;
        }
    </style> --}}
    @livewireStyles
@endsection

@section('contenido')
    <section class="w-full flex flex-col h-auto px-2 py-4">
        @hasrole("Admin|Super Admin")
            <div class="w-full flex flex-col relative h-auto border rounded-md border-color-gray--BK min-h-interno">
                @livewire('referencias-filtrado-contenedor')
            </div>
        @endhasrole
    </section>
@endsection

@section('js-interno')

    {{-- <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script> --}}
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    {{-- <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script> --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.2/dist/sweetalert2.min.js"></script> --}}

    {{-- //cdn para el modal de confirmacion sweetalert2 --}}

    {{-- <script type="text/javascript">
          $(document).on('submit', '.formulario-cambio', function(e) {
           e.preventDefault();
           Swal.fire({
                    title: '¿Deseas cambiar el estatus de este pedido?',
                    showDenyButton: true,
                    // showCancelButton: true,
                    confirmButtonText: `Cambiar`,
                    denyButtonText: `Cancelar`,
                    denyButtonColor: '#908888',
                    }).then((result) => {
                    // if (result.isConfirmed) {
                    //     Swal.fire('Se ha cambiado!', '', 'success')
                    //     $('#btnSend').click();
                    // } else if (result.isDenied) {
                    //     Swal.fire('Los cambios no se efectuaron', '', 'info')
                    // }
                    if(result.value){
                        Swal.fire('Cambiando..!', '', 'success')
                        this.submit();
                    }
                })
          });

    </script> --}}

    <script>
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                element_input = document.querySelector('[placeholder="Buscar"]');
                element_input.focus();
            })
        });

        let fecha_inicio = '', fecha_fin = '';
        function emit_fechas(){
            if (fecha_inicio != '') {
                Livewire.emit('inicio', fecha_inicio)
            }
            if (fecha_fin != '') {
                Livewire.emit('fin', fecha_fin)
            }
        }

        $(document).ready(function() {
            $("#inicio").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    fecha_inicio = date;
                },
                beforeShow: function() {
                    $("#inicio").datepicker("option", "maxDate", $("#fin").datepicker("getDate"))
                }
            });

            $("#fin").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    fecha_fin = date
                },
                beforeShow: function() {
                    $("#fin").datepicker("option", "minDate", $("#inicio").datepicker("getDate"))
                }
            });

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;
            // let url_referencia = window.location.href;
            // if(url_referencia.includes('?')){
            //     var url_string = window.location.href
            //     var url = new URL(url_string);
            //     var inicio = url.searchParams.get("inicio");
            //     var fin = url.searchParams.get("fin");
            //     // console.log(inicio);
            //     $("#inicio").val(inicio);
            //     $("#fin").val(fin);

            // }else{
               $("#inicio").val(today);
               $("#fin").val(today);
            // }

        });

    </script>



@endsection


