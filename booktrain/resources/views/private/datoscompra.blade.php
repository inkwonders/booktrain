@extends('layouts.logged')

@section('title')
    Datos Compra
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">

@endsection

@section('misPedidosSelected')
    border-b-green--BK
@endsection

@section('contenido')

<div class="w-11/12 my-5 sm:mx-16 sm:min-h-custom sm:py-1 font-rob-regular">
    <div class="w-full p-5 border-2 border-gray-300 rounded-lg lg:border-transparent lg:p-0">
    <!-- 1° Sección -->
    <div class="w-full border-b-2">

        <div class="flex flex-wrap justify-between pb-5 text-left lg:mt-12 lg:pb-0">

            <p class="w-1/2 text-xl uppercase color-blue--bk font-rob-bold">{{ $pedido->serie.$pedido->folio }}</p>

            @if($pedido->status == "PROCESADO" &&  !is_null($pedido->referencia))
            <a href="/datos-compra/{{$pedido->id}}/recibo" class="relative w-full px-3 py-4 mt-4 text-sm text-center uppercase border border-gray-400 border-solid rounded-md lg:w-3/12 lg:mt-0 color-white--bk background-green--bk hover:bg-white hover:border-color-green--BK hover:color-green--bk">Reimprimir Comprobante</a>
            @endif

        </div>

        <div class="flex flex-wrap">

            <div class="w-full text-left lg:w-1/4">

                <span class="text-base uppercase color-gray--bk font-rob-bold">Fecha de pedido: </span>
                <span class="text-base color-gray--bk"> {{ date('d / m / Y', strtotime($pedido->created_at)) }} </span>

            </div>

            <div class="w-full mb-4 text-left lg:w-1/4">
                @if($venta_movil)
                    <span class="text-base uppercase color-gray--bk font-rob-bold">Referencia: </span>
                    <span class="text-base color-gray--bk"> {{ $referencia->referencia }} </span>

                @else
                    <span class="text-base uppercase color-gray--bk font-rob-bold">Fecha de entrega: </span>
                    @if (!is_null($pedido->envio) && !is_null($pedido->envio->fecha_recepcion))
                    <span class="text-base color-gray--bk"> {{ date('d / m / Y', strtotime($pedido->envio->fecha_recepcion)) }} </span>
                    @else
                    <span class="text-base color-gray--bk"> </span>
                    @endif
                @endif


            </div>

        </div>

    </div>

    <!-- 2° Sección -->
    <div class="w-full border-b-2">

        <div class="flex mt-6 flex-column">

            <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>

        </div>

        <div class="flex flex-wrap text-left">

            <div class="w-full lg:w-1/4 lg:mt-4">

                <span class="text-base uppercase color-gray--bk font-rob-bold">nombre: </span>
                <span class="text-base uppercase color-gray--bk"> {{ $pedido->nombre_contacto }} </span>

            </div>

            <div class="w-full lg:w-1/4 lg:mt-4">

                <span class="text-base uppercase color-gray--bk font-rob-bold">apellido(s): </span>
                <span class="text-base uppercase color-gray--bk"> {{ $pedido->apellidos_contacto }} </span>

            </div>

            <div class="w-full mb-4 lg:w-1/4 lg:mt-4">

                <span class="text-base uppercase color-gray--bk font-rob-bold">Celular: </span>
                <span class="text-base uppercase color-gray--bk"> {{ $pedido->celular_contacto }} </span>

            </div>

        </div>

        <div class="w-full mb-4 lg:w-1/4 lg:mt-4">

            <span class="text-base uppercase color-gray--bk font-rob-bold">Correo para recepción de licencias digitales: </span>
            <span class="text-base color-gray--bk"> {{ $pedido->correo_contacto == null ? '----' : $pedido->correo_contacto }} </span>

        </div>

    </div>

    <!-- 3° Sección -->
    <div class="w-full border-b-2">

        <div class="flex mt-6 flex-column">

            <p class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">ENTREGA</p>

        </div>

        <div class="flex mt-4 flex-column">

            {{-- @if (!is_null($direccion_entrega )) --}}
            <span class="text-base uppercase color-gray--bk">
                @if ($pedido->tipo_entrega == 1)
                <span class="font-rob-bold">COLEGIO:</span>
                @elseif (($pedido->tipo_entrega == 2))
                <span class="font-rob-bold">DOMICILIO:</span>
                @elseif (($pedido->tipo_entrega == 3))
                <span class="font-rob-bold">PICKUP:</span>
                @else
                <span class="font-rob-bold">DOMICILIO:</span>
                @endif
                {{ is_null($direccion_entrega)? '' : $direccion_entrega->direccion_completa }}
            </span>
            {{-- @else
            <span class="text-base uppercase color-gray--bk font-rob-bold">DOMICILIO:</span>
            @endif --}}


            <span class="text-base uppercase color-gray--bk">   </span>

        </div>

        @if($guia_pedido && empty($envios_parciales))
        <div class="mt-4 mb-4 text-left">
                <span class="text-base uppercase color-gray--bk font-rob-bold">Guía de paquetería: </span>
                <span class="text-base uppercase color-gray--bk">{{ $guia_pedido->guia }}</span>
        </div>
        <div class="mt-4 mb-4 text-left">
                <span class="text-base uppercase color-gray--bk font-rob-bold">Paquetería:</span>
                <a target="_blank" href='{{ $guia_pedido->paqueteria()->first() != null ? $guia_pedido->paqueteria()->first()->url :'' }}' class="text-base color-gray--bk ">{{ $guia_pedido->paqueteria()->first() != null ? $guia_pedido->paqueteria()->first()->url :'' }}</span></a>
        </div>
        @endif

        @if(!empty($envios_parciales))
            @foreach ($envios_parciales as $envio)
                <div class="mt-4 mb-4 text-left">
                    <span class="text-base uppercase color-gray--bk font-rob-bold">Guía de paquetería: </span>
                    <span class="text-base uppercase color-gray--bk">{{ $envio->guia }}</span>
                </div>
                <div class="mt-4 mb-4 text-left">
                    <span class="text-base uppercase color-gray--bk font-rob-bold">Paquetería:</span>
                    <a target="_blank" href='{{ $envio->paqueteria()->first() != null ? $envio->paqueteria()->first()->url : '' }}' class="text-base color-gray--bk ">{{ $envio->paqueteria()->first() != null ? $envio->paqueteria()->first()->nombre : '' }}</span></a>
                </div>
            @endforeach
        @endif

    </div>

    <!-- 4° Sección -->
    <div class="w-full border-b-2">

        <div class="flex mt-6 flex-column">

            <p class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">datos de facturación del último pedido</p>

        </div>

        @if (!is_null($datos_facturacion ))
        <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
            <thead>
                <tr class="font-bold uppercase align-top">
                    <th class="pb-4 pr-4">Nombre o razón social</th>
                    <th class="pb-4 pr-4">RFC</th>
                    <th class="pb-4 pr-4">Correo</th>
                    <th class="pb-4 pr-4">Uso de CFDI</th>
                    <th class="pb-4 pr-4">Método de pago</th>
                    <th class="pb-4 pr-4">Forma de pago</th>
                </tr>
            </thead>
            <tbody>
                <tr class="align-top">
                    <td class="pr-4">{{ $datos_facturacion->razon_social }}</td>
                    <td class="pr-4">{{ $datos_facturacion->rfc }}</td>
                    <td class="pr-4 uppercase">{{ $datos_facturacion->correo }}</td>
                    <td class="pr-4">{{ $datos_facturacion->cfdis->descripcion }}</td>
                    <td class="pr-4">{{ $datos_facturacion->metodo_pago }}</td>
                    <td class="pr-4">{{ $datos_facturacion->forma_pago }}</td>
                </tr>

            </tbody>
        </table>

        <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
            <thead>
                <tr class="font-bold uppercase align-top">
                    <th class="pb-4 pr-4">Calle</th>
                    <th class="pb-4 pr-4">Num. exterior</th>
                    <th class="pb-4 pr-4">Num. interior</th>
                    <th class="pb-4 pr-4">Colonia</th>
                    <th class="pb-4 pr-4">Municipio</th>
                    <th class="pb-4 pr-4">Estado</th>
                </tr>
            </thead>
            <tbody>
                <tr class="align-top">
                    <td class="pr-4">{{ $datos_facturacion->calle }}</td>
                    <td class="pr-4">{{ $datos_facturacion->num_exterior }}</td>
                    <td class="pr-4">{{ $datos_facturacion->num_interior }}</td>
                    <td class="pr-4">{{ $datos_facturacion->colonia }}</td>
                    <td class="pr-4">{{ $datos_facturacion->municipio }}</td>
                    <td class="pr-4">{{ $datos_facturacion->estado }}</td>
                </tr>
            </tbody>
        </table>

        <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
            <thead>
                <tr class="font-bold uppercase align-top">
                    <th class="pb-4 pr-4">Codigo postal</th>
                </tr>
            </thead>
            <tbody>
                <tr class="align-top">
                    <td class="pr-4">{{ $datos_facturacion->cp }}</td>
                </tr>
            </tbody>
        </table>

        <div class="pb-4 mt-4 lg:hidden">

            <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
            <div class="text-base uppercase color-gray--bk break-word">{{ $datos_facturacion->razon_social }}</div>
            <div class="text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
            <div class="text-base uppercase color-gray--bk break-word">{{ $datos_facturacion->rfc }}</div>
            <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
            <div class="text-base color-gray--bk break-word">{{ $datos_facturacion->correo }}</div>
            <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
            <div class="text-base uppercase color-gray--bk break-word">{{ $datos_facturacion->cfdis->descripcion }}</div>
            <div class="text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
            <div class="text-base color-gray--bk break-word">{{ $datos_facturacion->metodo_pago }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->forma_pago }}</div>

            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Calle</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->calle }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. exterior</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->num_exterior }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. interior</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->num_interior }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colonia</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->colonia }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Municipio</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->municipio }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Estado</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->estado }}</div>
            <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Codigo postal</div>
            <div class="col-span-2 text-base color-gray--bk break-word">{{ $datos_facturacion->cp }}</div>
        </div>

        @endif

    </div>

    @foreach($pedido->resumenes as $resumen_pedido_alumno)

        <!-- 5° Sección -->
        <div class="flex flex-col w-full lg:flex-row">

            <!-- 5° Sección izq -->
            <div class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">

                <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">

                    <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                    <span class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido->colegio->nombre }} </span>

                    <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado: </span>
                    <span class="col-span-4 text-base uppercase color-gray--bk"> {{ $resumen_pedido_alumno->paquete->nivelColegio->nombre }} - {{ $resumen_pedido_alumno->paquete->nivelColegio->seccion->nombre }} </span>

                </div>

                <div class="w-full mt-4 text-left">

                    <span class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE DEL ALUMNO:* </span>

                </div>

                <div class="w-full mt-2 mb-5 text-left">

                    <span class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_alumno." ".$resumen_pedido_alumno->paterno_alumno." ".$resumen_pedido_alumno->materno_alumno }} </span>

                </div>

            </div>

            <!-- 5° Sección der -->

            <div class="lg:w-2/3 lg:border-b-2">

                <div class="flex flex-col mt-10 mb-6 lg:grid lg:grid-cols-11 lg:gap-4">

                    @foreach ($resumen_pedido_alumno->detallesResumenPedidos as $detalles_resumen)

                        {{-- {{ $detalles_resumen->cantidad }} --}}

                        <span class="col-span-7 text-base text-left color-gray--bk lg:text-right">
                            <span class="col-span-1 text-base text-left color-gray--bk">
                                @if($detalles_resumen->devuelto == 1)
                                <span class="inline-flex items-center justify-center px-2 py-1 mr-2 text-xs font-bold leading-none text-red-100 bg-red-600 rounded-full">Devuelto</span>
                                @endif
                            </span>
                            {{ $detalles_resumen->libro->nombre }}
                            {{ ($detalles_resumen->cantidad > 1? " (".$detalles_resumen->cantidad.")" : "" ) }}
                        </span>

                        <span class="col-span-2 text-base text-left color-gray--bk"><label class="text-base text-gray-500 lg:hidden">ISBN </label>{{ $detalles_resumen->libro->isbn }}</span>

                        {{-- <span class="flex justify-between w-full col-span-2 text-base text-right color-green--bk lg:inline-block"> <label class="text-base text-gray-500 lg:hidden">PRECIO</label> ${{ number_format(intval($detalles_resumen->precio_libro)*intval($detalles_resumen->cantidad), 2) }} MXN </span> --}}
                        <span class="flex justify-between w-full col-span-2 text-base text-right color-green--bk lg:inline-block rob-light font-extralight"> <label class="text-base text-gray-500 lg:hidden">PRECIO</label> ${{ number_format($detalles_resumen->precio_libro, 2) }} MXN </span>

                        <div class="w-full my-4 border-b-2 lg:hidden"></div>

                    @endforeach

                </div>

            </div>

        </div>
    @endforeach

    <!-- 6° Sección -->

    <div class="flex w-full">

        <!-- 6° Sección izq -->
        <div class="hidden lg:w-1/3 lg:block">

        </div>

        <!-- 6° Sección der -->

        <div class="w-full lg:w-2/3">

            <div class="grid grid-cols-11 mt-10 mb-6 lg:gap-4 font-rob-regular">

                <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">SUBTOTAL</span>

                <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3"> ${{ number_format($subtotal, 2) }} MXN </span>

                <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">ENVIO</span>

                @if ($pedido->tipo_entrega == 2 && !is_null($guia_pedido))
                <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3"> ${{ number_format($guia_pedido->costo, 2) }} MXN </span>
                @else
                <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3">${{ number_format(0, 2) }} MXN</span>
                @endif

                <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">COMISIÓN DE PAGO</span>

                <span class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3 rob-regular"> ${{ number_format($pedido->comision, 2) }} MXN </span>

                <span class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">TOTAL</span>

                <span class="col-span-5 font-bold text-right lg:text-md text-md lg:col-span-3 rob-bold"> ${{ number_format($pedido->total, 2) }} MXN </span>

            </div>

        </div>

    </div>

    </div>

    <!-- Botón regresar -->

    <div class="flex justify-center w-full my-12">

        <a @if($venta_movil) href="/venta_movil/pedidos" @else href="/mis-pedidos" @endif class="appearance-none relative block text-center lg:py-1 py-3 px-3 lg:w-2/12 w-full lg:my-1.5 rounded-md color-gray--bk border border-solid border-gray-400 hover:bg-white hover:border-color-blue--BK hover:color-blue--bk lg:text-lg md:text-base sm:text-sm">REGRESAR</a>

    </div>

</div>

@endsection

@section('js-interno')
@endsection
