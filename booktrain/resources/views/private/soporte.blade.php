@extends('layouts.logged')

@section('title')
    Soporte
@endsection

@section('css')
@endsection

@section('soporteSelected')
    border-b-green--BK
@endsection

@section('contenido')

@if(session('error') != null)
    <span>{{ session('error') }}</span>
@endif

<form-casos csrf="{{ csrf_token() }}"/>

@endsection

@section('js-interno')
@endsection
