@extends('layouts.logged')

@section('title')
    Detalles del caso
@endsection

@section('css')

@endsection

@section('soporteSelected')
    border-b-green--BK
@endsection

@section('contenido')

    <div class="w-full flex flex-col justify-center items-center">
        <div class="w-full h-full flex flex-col items-center">
            <ticket ticketId="{{ $ticketInformacion->id }}" csrf="{{ csrf_token() }}" rol="{{ Auth::user()->roles->pluck('name')->first() }}"/>
        </div>
        <div class="w-full flex justify-center items-center my-12">
            <a href="{{ route('ticket.index') }}" class="hover:bg-gray-400 hover:text-white appearance-none rounded-none relative block text-center py-1 px-3 lg:w-2/12 w-11/12 my-1.5 rounded-md color-gray--bk border border-solid border-gray-400 lg:text-lg md:text-base sm:text-sm">REGRESAR</a>
        </div>
    </div>

@endsection

@section('js-interno')

@endsection

