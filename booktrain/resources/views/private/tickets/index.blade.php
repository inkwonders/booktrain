@extends('layouts.logged')
@section('title')
    Tickets
@endsection
@section('soporteSelected')
    border-b-green--BK
@endsection
@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>
                /*Form fields*/
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568;             /*text-gray-700*/
            padding-left: 1rem;         /*pl-4*/
            padding-right: 1rem;        /*pl-4*/
            padding-top: .5rem;         /*pl-2*/
            padding-bottom: .5rem;      /*pl-2*/
            line-height: 1.25;          /*leading-tight*/
            /*border-width: 1px;*/          /*border-2*/
            border: 1px solid rgba(229, 231, 235, var(--tw-border-opacity));
            --tw-border-opacity: 1;
            border-radius: .25rem;
            /*border-color: #edf2f7;*/      /*border-gray-200*/
            /*background-color: #edf2f7;*/  /*bg-gray-200*/
            margin-left: 10px;
            outline: none;
        }
        .dataTables_wrapper .dataTables_length select {
            color: #4a5568;
            padding-left: 0.5rem;
            padding-right: 2rem;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            line-height: inherit;
            border-width: 0;
            border-radius: 0;
            cursor: pointer;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #edf2f7;  /*bg-indigo-100*/
        }
        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button       {
            font-weight: 700;               /*font-bold*/
            border-radius: .25rem;          /*rounded*/
            border: 1px solid transparent;  /*border border-transparent*/
            margin: 10px;
        }
        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current   {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);    /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-blue--bk) !important;        /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
            padding: 5px;
            display: inline-block;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }
        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover     {
            color: #fff !important;             /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);     /*shadow*/
            font-weight: 700;                   /*font-bold*/
            border-radius: .25rem;              /*rounded*/
            background: var(--main-green--bk) !important;       /*bg-indigo-500*/
            border: 1px solid transparent;      /*border border-transparent*/
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
            color: #fff !important;
            cursor: pointer;
        }
        /*Add padding to bottom border */
        table.dataTable.no-footer {
            /*border-bottom: 1px solid #e2e8f0;*/   /*border-b-1 border-gray-300*/
            /* margin-top: 0.75em; */
            /* margin-bottom: 0.75em; */
        }
        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr>th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }
        #example_filter{
            float: right;
            padding-bottom: 40px;
        }
        #example_paginate{
            float: right;
        }
        /* .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
            color: #fff !important;
        } */
        .dataTables_wrapper {
            width: 100%;
        }
        .dataTables_wrapper {
            color: var(--main-gray--bk);
        }
        table.dataTable thead th, table.dataTable thead td, .dataTables_wrapper.no-footer .dataTables_scrollBody {
            border-bottom: 1px solid #d1d5db;
        }
        .dataTables_scrollBody {
            /* display: flex;
            flex-direction: column;
            min-height: calc(100vh - 488px); */
            min-height: 250px;
            /* min-height: calc(100vh - 512px); */
            /* height: auto; */
        }
        table.dataTable thead .sorting {
            background-image: none;
        }
        table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
            background-color: #f2f2f2;
        }
        .nuevo {
            color: var(--main-green--bk);
        }
        .cerrado {
            color: var(--main-red--bk);
        }
        .cliente, .soporte {
            color: #f7931e;
        }
        .contenedor_bt {
            height: 100px;
        }
        .dataTables_empty {
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #f2f2f2;
        }
        .c_cuadro {
            position: relative;
        }
        .c_cuadro::after {
            content: "";
            height: 15px;
            width: 15px;
            position: absolute;
            background-color: #000;
            margin: auto;
            left: -30px;
            top: 0;
            bottom: 0;
        }
        .c_positivos::after {
            background-color: #109618;
        }
        .c_negativo::after {
            background-color: #ff5733;
        }
        .c_abiertos::after {
            background-color: #ff9900;
        }
        .c_cerrados::after {
            background-color: #3366cc;
        }


        /* Diseño personalizado del datatable */
        .data-table-diseño {
            width: 100%;
        }
        .data-table-diseño [wire\:id] {
            width: 100%;
        }
        .data-table-diseño span[aria-current] > span {
            background-color: var(--main-blue--bk);
            color: white;
        }
        .data-table-diseño span[wire\:key] > button:hover {
            background-color: var(--main-green--bk);
            color: white;
        }
        .data-table-diseño input {
            padding-left: 0.5rem;
            padding-right: 2rem;
        }

    </style>
@endsection

@section('contenido')
    <section class="flex flex-col items-center w-full pl-10 min-h-custom">
        <div class="flex justify-between w-full py-10">
            <h2 class="text-xl font-bold color-red--bk">QUEJAS Y SUGERENCIAS</h2>

            @if($errors->any())
            <div id="modal-cerrar" class="fixed bottom-0 left-0 z-30 flex items-center justify-center w-full h-full bg-gray-600 bg-opacity-50 modal">
                <div class="w-1/3 bg-white rounded-lg">
                    <div class="flex flex-col items-start p-4">
                        <div class="flex items-center justify-center w-full">
                            <div class="pb-3 text-lg font-medium text-gray-900">
                                <center> <p  class="mt-8 color-gray--bk">{{ implode('', $errors->all(':message')) }}</p> </center>
                            </div>
                        </div>
                        <div class="w-full">
                            <div class="flex justify-around w-full pt-5">
                                <div id="btn_cerrar" data-dismiss="modal" class="flex items-center justify-center text-white uppercase border border-solid rounded-md cursor-pointer  close w-3/2 background-red--bk font-rob-bold hover:bg-white hover:border-red-500 hover:text-redBT">
                                    Cerrar
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @hasrole("Cliente")
                <form-casos csrf="{{ csrf_token() }}"/>
            @endhasrole
        </div>
        @hasrole("Admin")
        @if (auth()->user()->hasDirectPermission('Soporte'))
            <div class="flex justify-between w-full">
                <graficas />
            </div>
        @endif
        @endhasrole

        <div class="w-full flex lg:pt-16 mt-6 lg:mt-0 rounded bg-white justify-center items-center data-table-diseño">
            <livewire:tikets />
        </div>

        <div class="w-full h-20"></div>

    </section>
@endsection
@section('js-interno')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>

    <script>
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                element_input = document.querySelector('[placeholder="Buscar"]');
                element_input.focus();
            })
        });

        $(document).ready(function() {
            $("#btn_cerrar").on("click", function(){
                $('#modal-cerrar').addClass("hidden");
            })
        });
    </script>
@endsection
