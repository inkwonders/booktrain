@extends('layouts.logged')

@section('title', 'Reporte por colegio')

@section('contenido')
<section class="flex flex-col items-center justify-center w-full h-auto px-4 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
    @if (auth()->user()->hasDirectPermission('Reportes'))
        @livewire('selector-reporte-colegio')
    @endif
    @endhasrole
</section>
@endsection
