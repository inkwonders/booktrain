{{-- timbrado --}}
<x-livewire-tables::table.cell>
    @if (!empty($row->pedido->facturama_invoice_id) )


        @if ($row->pedido->facturama_status == 'active')

            <span>

                Timbrado

            </span>

        @else

            <span class="cursor-pointer color-blue--bk" wire:click="timbrado(1,{{ $row->id }})">

                No Timbrado

            </span>

        @endif

    @else

        <span class="cursor-pointer color-blue--bk" wire:click="timbrado(1,{{ $row->id }})">

            No Timbrado

        </span>

    @endif

    @if ($modal_facturama != false)
        <div
            class="fixed top-0 left-0 z-99999 flex items-center justify-center w-full h-screen transition-transform duration-300 transform scale-0 bg-gray-400  bg-opacity-5 @if ($modal_facturama != false) scale-100 @endif">

            <div class="relative flex flex-col p-12 overflow-y-auto bg-white rounded-lg ">

                <div class="items-center w-full py-4 text-center">

                    <h1 class="text-2xl font-bold color-red--bk">Errores</h1>

                </div>

                <div class="grid items-center justify-around gap-4 pb-4">

                    <span class="text-base font-medium">

                        @php

                        $errores_facturama = explode(";", $error_facturama );

                        @endphp

                        @foreach ($errores_facturama  as $error )

                            {{ $error }} <br>

                        @endforeach

                    </span>

                </div>

                <div class="flex justify-center">

                    <a wire:click="$set('modal_facturama', false)"
                        class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-blueBT">
                        Aceptar
                    </a>

                </div>

            </div>

        </div>
    @endif


</x-livewire-tables::table.cell>

{{-- Referencia Factura --}}
<x-livewire-tables::table.cell>


    {{-- {{ dd($row->pedido->referencia_factura) }} --}}
    @if (!empty($row->pedido->factura))
        <span>

            <button wire:click="asignarReferenciaFactura({{ $row->pedido->id }})"
                @if ($row->pedido->referencia_factura == '') class="color-blue--bk" @endif>
                {{ $row->pedido->referencia_factura == '' ? '+ Agregar' : $row->pedido->referencia_factura }}</button>

        </span>
    @endif

    {{-- {{ dd($row->pedido) }} --}}

</x-livewire-tables::table.cell>

{{-- folio --}}
<x-livewire-tables::table.cell>
    @if (!empty($row->pedido->serie))

    {{ $row->pedido->serie.$row->pedido->folio }}

    @endif

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @if (!empty($row->pedido->status))

        <span
            @if ($row->pedido->status == 'PAGO RECHAZADO' || $row->pedido->status == 'CANCELADO') class="color-red--bk" @elseif ($row->pedido->status == 'PROCESADO') class="color-gray--bk"  @elseif ($row->pedido->status == 'PAGO EN REVISION') class="color-blue--bk" @else class="color-green--bk" @endif>{{ $row->pedido->status }}
        </span>

    @endif

</x-livewire-tables::table.cell>

{{-- Razon social --}}
<x-livewire-tables::table.cell>

    {{ $row->razon_social }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    {{ $row->rfc }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    {{ $row->correo }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    {{ $row->calle }} {{ $row->num_exterior }} {{ $row->num_interior }} {{ $row->colonia }}
    {{ $row->municipio }} {{ $row->estado }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    {{ $row->cp }}


</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    {{ $row->cfdis->descripcion }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @if (!empty($row->pedido->metodo_pago))

        {{ $row->pedido->metodo_pago }}

    @endif



</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @if (!empty($row->pedido->formaPago->descripcion))

        {{ $row->pedido->formaPago->descripcion }}

    @endif


</x-livewire-tables::table.cell>


<x-livewire-tables::table.cell>

    {{ $row->created_at != null ? date_format($row->created_at, 'd / m / Y') : '' }}

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @if (!empty($row->pedido->facturama_invoice_id))

        <div class="flex gap-4">

            <div wire:click="imprimir(1,'{{ $row->pedido->facturama_invoice_id }}')"
                class="flex items-center justify-center gap-2 p-2 px-4 font-bold text-center text-white uppercase rounded-sm appearance-none cursor-pointer w-28 enviar-reporte bg-greenBT">

                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor">

                    <path fill-rule="evenodd"
                        d="M3 17a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm3.293-7.707a1 1 0 011.414 0L9 10.586V3a1 1 0 112 0v7.586l1.293-1.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                        clip-rule="evenodd" />

                </svg>

                PDF

            </div>

            <div wire:click="imprimir(2,'{{ $row->pedido->facturama_invoice_id }}')"
                class="flex items-center justify-center gap-2 px-4 py-2 font-bold text-center text-white uppercase rounded-sm appearance-none cursor-pointer w-28 enviar-reporte bg-greenBT">

                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor">

                    <path fill-rule="evenodd"
                        d="M3 17a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm3.293-7.707a1 1 0 011.414 0L9 10.586V3a1 1 0 112 0v7.586l1.293-1.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z"
                        clip-rule="evenodd" />

                </svg>

                XML

            </div>


                @if ($row->pedido->factura == 1 && $row->pedido->facturama_invoice_id != null && $row->pedido->facturama_status != 'active')

                    <div wire:click="imprimir(3,'{{ $row->pedido->facturama_invoice_id }}')" class="flex items-center justify-center gap-2 px-4 py-2 font-bold text-center text-white uppercase rounded-sm appearance-none cursor-pointer w-28 enviar-reporte bg-greenBT">

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4" viewBox="0 0 20 20" fill="currentColor">

                            <path fill-rule="evenodd" d="M3 17a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm3.293-7.707a1 1 0 011.414 0L9 10.586V3a1 1 0 112 0v7.586l1.293-1.293a1 1 0 111.414 1.414l-3 3a1 1 0 01-1.414 0l-3-3a1 1 0 010-1.414z" clip-rule="evenodd" />

                        </svg>

                        Acuse

                    </div>

                @else

                    <div wire:click='cancelar({{ $row->pedido_id }})' class="flex items-center justify-center gap-2 px-4 py-2 font-bold text-center text-white uppercase rounded-sm appearance-none cursor-pointer w-28 enviar-reporte bg-redBT">

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">

                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />

                        </svg>

                        Cancelar

                    </div>

                @endif

        </div>

    @endif

        @if ($modal_descarga != false)
            <div class="fixed top-0 left-0 z-99999 flex items-center justify-center w-full h-screen transition-transform duration-300 transform scale-0 bg-gray-400  bg-opacity-5 @if ($modal_descarga != false) scale-100 @endif">

                <div class="relative flex flex-col p-12 overflow-y-auto bg-white rounded-lg ">

                    <div class="items-center w-full py-4 text-center">

                        <h1 class="text-2xl font-bold color-green--bk">¿Estas seguro que quieres descargar el archivo?</h1>

                    </div>

                    <div class="grid items-center justify-around gap-4 pb-4 color-blue--bk">

                        <svg xmlns="http://www.w3.org/2000/svg" class="w-32 h-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">

                            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />

                        </svg>

                    </div>

                    <div class="flex justify-center gap-8">

                        <a wire:click="$set('modal_descarga', false)" class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-redBT">

                            No

                        </a>

                        <a download target="_blank" wire:click='download' class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-greenBT">

                            Si

                        </a>

                    </div>

                </div>

            </div>
        @endif

        @if ($modal_cancelar != false)
            <div class="fixed top-0 left-0 z-99999 flex items-center justify-center w-full h-screen transition-transform duration-300 transform scale-0 bg-gray-400  bg-opacity-5 @if ($modal_cancelar != false) scale-100 @endif">

                @if ($cancelacion_exitosa != false)

                    <div class="relative flex flex-col p-12 overflow-y-auto bg-white rounded-lg ">

                        <h2 class="text-2xl font-bold color-blue--bk">

                            Factura cancelada con éxito

                        </h2>

                        <div class="flex flex-col items-center justify-center gap-8 pt-8">

                            <svg xmlns="http://www.w3.org/2000/svg"class="w-32 h-32 text-greenBT" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">

                                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />

                            </svg>

                            <div>

                                <a wire:click="$set('modal_cancelar', false)" class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-blueBT">

                                    Aceptar

                                </a>

                            </div>

                        </div>

                    </div>

                @else

                    <div class="relative flex flex-col p-12 overflow-y-auto bg-white rounded-lg ">

                        <div class="items-center w-full py-4 text-center">

                            <h1 class="text-2xl font-bold color-green--bk">¿Estas seguro que quieres cancelar esta factura?</h1>

                        </div>

                        <div class="flex items-center justify-around gap-4 py-4 color-blue--bk">

                            <svg xmlns="http://www.w3.org/2000/svg" class="w-32 h-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">

                                <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />

                            </svg>

                        </div>

                        <div class="flex items-center justify-around w-full gap-4 py-4 color-blue--bk" >

                            <select wire:model="razon_cancelacion" class="relative w-full h-10 p-2 select_cancelacion" style="border: 3px solid;border-color: #50b9e7 !important;appearance: auto;width: 15rem;background-color: transparent;border-radius: 5px;">

                                <option selected value="">Razón Cancelación</option>

                                <option value="01">01 - Comprobantes emitidos con errores con relación</option>

                                <option value="02">02 - Comprobantes emitidos con errores sin relación</option>

                                <option value="03">03 - No se llevó a cabo la operación</option>

                                <option value="04">04 - Operación nominativa relacionada en una factura global</option>

                            </select>

                        </div>


                        <div class="flex justify-center gap-8 pt-4">

                            <a wire:click="$set('modal_cancelar', false)" class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-redBT">

                                No

                            </a>

                            <a  wire:click='delete({{ $pedido_id }})' class="px-6 py-2 text-xl text-white rounded-md cursor-pointer bg-greenBT transition-all duration-500  @if ($razon_cancelacion == '') hidden  @endif  ">
                                Si

                            </a>

                            <a class="px-6 py-2 text-xl bg-grayBT text-white rounded-md cursor-pointer transition-all duration-500  @if ($razon_cancelacion != '') hidden  @else  @endif  ">

                                Si

                            </a>

                        </div>

                    </div>

                @endif

            </div>

        @endif


</x-livewire-tables::table.cell>
