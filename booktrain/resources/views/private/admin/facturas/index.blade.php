@extends('layouts.logged')

@section('title')
    Facturas
@endsection

@section('facturasSelected')
    border-b-green--BK font-bold
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">

    <style>
        .data-table-diseño {

            width: 100%;

        }

        .data-table-diseño [wire\:id] {

            width: 100%;

        }

        .data-table-diseño span[aria-current]>span {

            background-color: var(--main-blue--bk);

            color: white;

        }

        .data-table-diseño span[wire\:key]>button:hover {

            background-color: var(--main-green--bk);

            color: white;

        }

        .data-table-diseño input {

            padding-left: 0.5rem;

            padding-right: 2rem;

        }

    </style>

    @livewireStyles
@endsection


@section('contenido')

    <section class="flex flex-col items-center justify-start w-full h-auto py-4 pl-2 min-h-custom">

        @hasrole('Admin|Super Admin')
            @if (auth()->user()->hasDirectPermission('Libros'))
                <div
                    class="relative flex flex-col justify-start w-full h-auto border rounded-md border-color-gray--BK min-h-interno">

                    <div class="relative flex flex-col items-center w-full h-auto p-10">

                        <div class="relative flex justify-between w-full pb-4 mb-5">

                            <h2 class="text-xl uppercase color-red--bk font-rob-bold">Facturas</h2>

                        </div>

                        <div class="w-full">

                            <div class="data-table-diseño">

                                <livewire:facturas-table />

                            </div>

                        </div>

                    </div>

                </div>

            @endif

        @endhasrole

    </section>

@endsection



@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">


    window.addEventListener('cerrar_modal', event => {
          $("#pedido_id").val("");
       });

    $(document).ready(function() {
        $(".cerrar").on("click", function(){

            $("#pedido_id").val("");
        });

        // Datepicker
        $("#inicio").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText, inst) {
                Livewire.emit('setFecha', 'inicio', dateText)
            }
        }).datepicker('setDate', "{{ date('Y-m-d') }}");

        $("#fin").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText, inst) {
                Livewire.emit('setFecha', 'fin', dateText)
            }
        }).datepicker('setDate', "{{ date('Y-m-d') }}");

    });
</script>
@endsection
