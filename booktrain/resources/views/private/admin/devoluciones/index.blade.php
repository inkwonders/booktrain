@extends('layouts.logged')
@section('title', 'Devoluciones')

@hasrole('Admin')
@section('contenido')
    @livewire('devoluciones')
@endsection
@endhasrole

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    // Datepicker
    $("#inicio").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: (date, obj) => {
            if (date != '')
                Livewire.emit('inicio', date)
        },
        beforeShow: function() {
            $("#inicio").datepicker("option", "maxDate", $("#fin").datepicker("getDate"))
        }
    });

    $("#fin").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: (date, obj) => {
            if (date != '')
                Livewire.emit('fin', date)
        },
        beforeShow: function() {
            $("#fin").datepicker("option", "minDate", $("#inicio").datepicker("getDate"))
        }
    });
});
</script>
@endsection
