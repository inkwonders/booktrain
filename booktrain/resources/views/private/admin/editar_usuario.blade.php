@extends('layouts.logged')
@section('title')
    Usuarios
@endsection
@section('usuariosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
    <section class="flex flex-col items-center justify-center w-full h-full py-4 pl-2 min-h-custom">
        @hasrole('Admin|Super Admin')
            <div class="relative flex flex-col items-center justify-center w-full h-full border rounded-md border-color-gray--BK">

                {{-- <livewire:editar-usuarios id="{{$user->id}}"> --}}
                {{-- @livewire('editar-usuario', ['user' => $user]) --}}
                @livewire('editar-usuario', ['user' => $user])

            </div>

            {{-- {{$user}}; --}}
        @endhasrole
    </section>
@endsection

@section('js')

<script>
    window.addEventListener('editar_correcto', event => {
        Swal.fire('Usuario actualizado con éxito.', '', 'success').then(function(){
            location.reload();
        });
    });
</script>

@endsection
