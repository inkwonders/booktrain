@extends('layouts.logged')

@section('title', 'Pedidos')

@section('contenido')
<section class="flex flex-col items-center justify-start w-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
    @if (auth()->user()->hasDirectPermission('Pedidos'))

        <div class="relative flex flex-col justify-start w-full h-auto p-10 border rounded-md border-color-gray--BK min-h-interno">
            <div class="relative flex items-start justify-between w-full mb-4">
                <h2 class="text-xl font-bold uppercase color-red--bk">Pedidos</h2>
            </div>
            <livewire:pedidos/>
            <livewire:contenedor-reporte-pedidos-general>
        </div>
    @endif
    @endhasrole
</section>
@endsection

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">


    window.addEventListener('cerrar_modal', event => {
          $("#pedido_id").val("");
       });

    $(document).ready(function() {
        $(".cerrar").on("click", function(){

            $("#pedido_id").val("");
        });

        // Datepicker
        $("#inicio").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText, inst) {
                Livewire.emit('setFecha', 'inicio', dateText)
            }
        }).datepicker('setDate', "{{ date('Y-m-d') }}");

        $("#fin").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText, inst) {
                Livewire.emit('setFecha', 'fin', dateText)
            }
        }).datepicker('setDate', "{{ date('Y-m-d') }}");

    });
</script>
@endsection
