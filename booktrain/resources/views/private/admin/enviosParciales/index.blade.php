{{-- <livewire:form-reporte-pedidos/> --}}


@extends('layouts.logged')
@section('title')
    Envios Parciales
@endsection
@section('referencia_envios_generalSelected')
border-b-green--BK font-bold
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>
        /* Estilos switch */
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 25px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            border-radius: 5px;
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: red;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 16px;
            width: 12px;
            left: 4px;
            bottom: 4px;
            border-radius: 3px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #81b84e;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #81b84e;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(36px);
            -ms-transform: translateX(36px);
            transform: translateX(36px);
        }
        .text-blue-1000 {
            --tw-text-opacity: 1;
            color: rgba(80, 185, 231, var(--tw-text-opacity));
        }
        .text-green-1000 {
            --tw-text-opacity: 1;
            color: rgba(129, 184, 78, var(--tw-text-opacity));
        }
        .border-blue-1000
        {
            border-color: rgb(80, 185, 231);
        }
    </style>
@livewireStyles
@endsection

@hasrole('Admin')
@section('contenido')
    @livewire('contenedor-envios-parciales')
@endsection
@endhasrole


@section('js-interno')
<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">


        let fecha_inicio = '', fecha_fin = '';
        function emit_fechas(){
            if (fecha_inicio != '') {
                Livewire.emit('inicio', fecha_inicio)
            }
            if (fecha_fin != '') {
                Livewire.emit('fin', fecha_fin)
            }
        }

        $(document).on('submit', '.formulario-cambio', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas cambiar el estatus de este pedido?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Cambiar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Cambiando..!', '', 'success')
                    this.submit();
                }
            })
        });

        $(document).on('submit', '.formulario-envio-mail', function(e) {
            e.preventDefault();
            Swal.fire({
                title: '¿Deseas reenviar el correo de confirmación de pago al cliente?',
                showDenyButton: true,
                // showCancelButton: true,
                confirmButtonText: `Enviar`,
                denyButtonText: `Cancelar`,
                denyButtonColor: '#908888',
                }).then((result) => {

                if(result.value){
                    Swal.fire('Enviando correo..!', '', 'success')
                    this.submit();
                }
            })
        });
        $(document).ready(function() {
            // Datepicker
            $("#inicio").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    fecha_inicio = date;
                },
                beforeShow: function() {
                    $("#inicio").datepicker("option", "maxDate", $("#fin").datepicker("getDate"))
                }
            });

            $("#fin").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    fecha_fin = date
                },
                beforeShow: function() {
                    $("#fin").datepicker("option", "minDate", $("#inicio").datepicker("getDate"))
                }
            });

            // Datatable
            // var table = $('#example2').DataTable();
            // table.order( [ 0, 'desc' ] ).draw();
            // ingresar datos de hoy
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;
            let url_referencia = window.location.href;
            if(url_referencia.includes('?')){
                var url_string = window.location.href
                var url = new URL(url_string);
                var inicio = url.searchParams.get("inicio");
                var fin = url.searchParams.get("fin");
                // console.log(inicio);
                $("#inicio").val(inicio);
                $("#fin").val(fin);

            } else {
                // $("#inicio").val(today);
                // $("#fin").val(today);
                // $("#btn_buscar").click();
                $("#inicio").val(today);
                $("#fin").val(today);
            }
        });
    </script>


@endsection


