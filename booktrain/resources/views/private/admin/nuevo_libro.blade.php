@extends('layouts.logged')
@section('title')
    Libros
@endsection
@section('librosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
    <section class="relative flex w-full py-4 pl-2">
        @hasrole('Admin|Super Admin')
                @livewire('nuevo-libro')
        @endhasrole
    </section>
@endsection

@section('js')
<script>
    window.addEventListener('creado_correcto', event => {
        Swal.fire('Libro creado con éxito.', '', 'success')
        this.submit();
    })
</script>

@endsection
