@extends('layouts.logged')

@section('title', 'Métodos de pago')

@section('contenido')
<section class="flex flex-col items-center justify-start w-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col justify-start w-full h-auto p-10 border rounded-md border-color-gray--BK min-h-interno">
            <div class="relative flex items-start justify-between w-full mb-4">
                <h2 class="text-xl font-bold uppercase color-red--bk">Métodos de pago</h2>
            </div>

            @livewire('metodos-pago')
        </div>
    @endhasrole
</section>
@endsection
