@extends('layouts.logged')

@section('title', 'Almacén')

@section('menu_almacen_selected', 'border-b-green--BK font-bold')

@section('contenido')
<section class="w-full flex flex-col items-center justify-center h-auto min-h-custom pl-2 py-4">
    @hasrole('Admin|Super Admin') @if (auth()->user()->hasPermissionTo('Almacen'))
    <div class="w-full flex flex-col relative h-interno border rounded-md border-color-gray--BK justify-center">

        <div class="flex flex-col items-center p-4 px-6 2xl:px-12 relative w-full h-full">
            @livewire('nuevo-movimiento-almacen')
        </div>
    </div>
    @endif @endhasrole
</section>
@endsection
