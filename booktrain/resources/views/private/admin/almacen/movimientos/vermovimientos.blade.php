@extends('layouts.logged')

@section('title')
    Almacén
@endsection

@section('menu_almacen_selected')
    border-b-green--BK font-bold
@endsection

@section('css')
    @livewireStyles
@endsection

@section('contenido')
    <section class="w-full flex flex-col items-center justify-center h-auto pl-2 py-4">
        @hasrole('Admin|Super Admin')
            @if (auth()->user()->hasPermissionTo('Almacen'))
                <div class="w-full flex flex-col relative h-auto min-h-interno border rounded-md border-color-gray--BK justify-start">
                    <div class="flex flex-col items-center p-10 relative w-full h-auto">
                        <div class="relative flex justify-between w-full pb-4 mb-5">
                            <h2 class="text-greenBT font-rob-bold uppercase text-xl">Movimientos de almacén</h2>
                            <div class="flex flex-row">
                                <a href="javascript:window.history.back(0)" class="bg-greenBT text-white font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer">REGRESAR</a>
                            </div>
                        </div>
                        <div class="w-full data-table-diseño">
                            <livewire:contenido-movimientos />
                        </div>
                    </div>
                </div>
            @endif
        @endhasrole
    </section>
@endsection

@section('js')
@endsection
