<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        @font-face {
            font-family: 'Roboto', sans-serif;
            font-style: normal;
            font-weight: normal;
            src: url('https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap');
        }
        .rob-font {
            font-family: 'Roboto', sans-serif;
            font-size: 16px;
        }
        /*#header,
        #footer {
        position: fixed;
        left: 0;
            right: 0;
            color: #aaa;
            font-size: 0.9em;
        }
        #header {
        top: 0;
            border-bottom: 0.1pt solid #aaa;
        }
        #footer {
        bottom: 0;
        border-top: 0.1pt solid #aaa;
        }*/
        .page-number:before {
        content: "Page " counter(page);
        }
        .page-break {
            page-break-after: always;
        }

        h1 {
            color:#000;
            float:left;
            font-size:1.3em;
            margin:0 1em 0 0;
            border-radius:3px;
        }
        .box {
            padding:5px;
            /*border-bottom:1px solid #999;*/
        }
        .box .alt {
            margin:5px;
            text-align:right;
            font-size:22px;
            position:relative;
            top:5px;
        }
    </style>

</head>
<body class="rob-font">

    @foreach ($pedidos as $index => $pedido)

    <div class="box">
        <h1 style="font-size:35px;">{{ $pedido->serie . $pedido->folio }} ({{ $pedido->status }})</h1>
        <div class="alt">Fecha de orden {{ $pedido->created_at }}</div>
    </div>
    <hr>

    <h1>{{ $pedido->colegio->nombre }}</h1>
    <br>
    <hr>

    <h1>DATOS DE CONTACTO</h1>
    <br>
    <hr>
    <table style="width:100%; margin-top:10px; vertical-align: text-top;">
        <tr style="background-color:#f2f2f2; vertical-align: text-top;">
            <td style="width:100%;" colspan="2">
                <strong style="width: 100px;">NOMBRE: </strong> <span>{{ $pedido->nombre_contacto }} {{ $pedido->apellidos_contacto }}</span>
            </td>
        </tr>
        <tr style="background-color:#fcfcfc; vertical-align: text-top;">
            <td style="width:70%;">
                <strong style="width: 100px;">EMAIL: </strong> <span>{{ $pedido->user->email }}</span>
            </td>
            <td style="width:30%;">
                <strong style="width: 100px;">TEL: </strong> <span>{{ $pedido->celular_contacto }}</span>
            </td>
        </tr>
    </table>
    <hr>

    <h1>
        @if ($pedido->tipo_entrega == 1)
        ENTREGA EN COLEGIO
        @elseif (($pedido->tipo_entrega == 2))
        ENVÍO A DOMICILIO
        @elseif (($pedido->tipo_entrega == 3))
        ENTREGA PICKUP
        @endif
    </h1>
    <br>
    <hr>
    <table style="width:100%; margin-top:10px; vertical-align: text-top;">
        <!--<tr style="background-color:#fcfcfc; vertical-align: text-top;">
            <td style="text-align:center;" colspan="4">
                @if ($pedido->tipo_entrega == 1)
                <strong>ENTREGA EN COLEGIO</strong>
                @elseif (($pedido->tipo_entrega == 2))
                <strong>ENVÍO A DOMICILIO</strong>
                @elseif (($pedido->tipo_entrega == 3))
                <strong>ENTREGA PICKUP</strong>
                @else
                <strong>ENVÍO A DOMICILIO</strong>
                @endif
            </td>
        </tr>-->
        <tr style="background-color:#f2f2f2; vertical-align: text-top;">
            <td style="width:60%;" colspan="2">
                <strong style="width: 100px;">CALLE: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->calle }}</span>
            </td>
            <td style="width:20%;">
                <strong style="width: 100px;">NO EXT: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->no_exterior }}</span>
            </td>
            <td style="width:20%;">
                <strong style="width: 100px;">NO INT: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->no_interior }}</span>
            </td>
        </tr>
        <tr style="background-color:#fcfcfc; vertical-align: text-top;">
            <td style="width:70%;" colspan="2">
                <strong style="width: 100px;">COL: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->colonia }}</span>
            </td>
            <td style="width:30%;" colspan="2">
                <strong style="width: 100px;">CP: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->cp }}</span>
            </td>
        </tr>
        <tr style="background-color:#f2f2f2; vertical-align: text-top;">
            <td style="width:70%;" colspan="2">
                <strong style="width: 100px;">ESTADO: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->estado->descripcion }}</span>
            </td>
            <td style="width:30%;" colspan="2">
                <strong style="width: 100px;">MUNICIPIO: </strong> <span>{{ is_null($pedido->direccion) ? '' : $pedido->direccion->municipio->descripcion }}</span>
            </td>
        </tr>
    </table>
    <hr>

    @if (!is_null($pedido->datosFactura))
    <h1>DATOS DE FACTURACIÓN</h1>
    <br>
    <hr>
    <table style="width:100%; margin-top:10px; vertical-align: text-top;">
        <tr style="background-color:#f2f2f2; vertical-align: text-top;">
            <td style="width:70%;" colspan="2">
                <strong style="width: 100px;">RAZÓN SOCIAL: </strong> <span>{{ $pedido->datosFactura->razon_social }}</span>
            </td>
            <td style="width:30%;">
                <strong style="width: 100px;">RFC: </strong> <span>{{ $pedido->datosFactura->rfc }}</span>
            </td>
        </tr>
        <tr style="background-color:#fcfcfc; vertical-align: text-top;">
            <td style="width:100%;" colspan="3">
                <strong style="width: 100px;">EMAIL: </strong> <span>{{ $pedido->datosFactura->correo }}</span>
            </td>
        </tr>
        <tr style="background-color:#f2f2f2; vertical-align: text-top;">
            <td style="width:38%;">
                <strong style="width: 100px;">USO DE CFDI: </strong> <span>{{ $pedido->datosFactura->cfdis->codigo }}-{{ $pedido->datosFactura->cfdis->descripcion }}</span>
            </td>
            <td style="width:27%;">
                <strong style="width: 100px;">MÉTODO DE PAGO </strong> <span>{{ $pedido->datosFactura->metodo_pago }}</span>
            </td>
            <td style="width:35%;">
                <strong style="width: 100px;">FORMA DE PAGO: </strong> <span>{{ $pedido->datosFactura->forma_pago }}</span>
            </td>
        </tr>
        <tr style="background-color:#fcfcfc; vertical-align: text-top;">
            <td>
                <strong style="width: 100px;">ESTADO DEL PAGO: </strong> <span>{{ is_null($pedido->pagos()->where('status', 'SUCCESS')->first())? 'ERROR' : 'SUCCESS' }}</span>
            </td>
            <td colspan="2">
                <strong style="width: 100px;">FECHA DE PAGO: </strong> <span>{{ is_null($pedido->pagos()->where('status', 'SUCCESS')->first())? 'ERROR' : $pedido->pagos()->where('status', 'SUCCESS')->first()->created_at }}</span>
            </td>
        </tr>
    </table>
    <hr>
    @endif

    @foreach ($pedido->resumenes as $resumen)

    <div class="box" style="text-align:center;">
        <h1>{{ $resumen->paquete->nivel->nombre }} - {{ $resumen->paquete->nivel->seccion->nombre }}  &nbsp; &nbsp; | &nbsp; &nbsp; {{ $resumen->nombre_alumno}} {{ $resumen->paterno_alumno}} {{ $resumen->materno_alumno}}</h1>
    </div>
    <br/>
    <br/>
    <table style="width:100%;">
        <tr>
            <th>LIBRO</th>
            <th>ISBN</th>
            <th>PRECIO<th
        </tr>
        @foreach ($resumen->detalles as $index2 => $detalle)
        @if ($index2 % 2 == 1)
        <tr style="background-color:#fcfcfc;">
        @else
        <tr style="background-color:#f2f2f2;">
        @endif

            <td style="width:60%;">{{ $detalle->libro->nombre }} {{ $detalle->devuelto == 1 ? '(**devuelto)' : '' }}</td>
            <td style="width:20%;">{{ $detalle->libro->isbn }}</td>
            <td style="width:20%;"><label style="position:relative; float:right;">{{ number_format($detalle->precio_libro, 2) }} MXN</label></td>
        </tr>
        @endforeach
    </table>
    @endforeach

    <br/>
    <table style="width:30%; position:relative; float:right;">
        <tr style="background-color:#f2f2f2;">
            <td><strong style="position:relative; float:right;">SUBTOTAL: </strong>  </td>
            <td style="text-align: right;"><label>{{ number_format($pedido->resumenes->sum('subtotal'), 2) }} MXN</label></td>
        </tr>
        <tr style="background-color:#fcfcfc;">
            <td><strong style="position:relative; float:right;">ENVIO: </strong>  </td>
            <td style="text-align: right;">{{ $pedido->tipo_entrega == 2 && !is_null($pedido->envio) ? number_format($pedido->envio->costo, 2) : number_format(0, 2) }} MXN</td>
        </tr>
        <tr style="background-color:#f2f2f2;">
            <td><strong style="position:relative; float:right;">COMISION: </strong>  </td>
            <td style="text-align: right;">{{ number_format($pedido->comision, 2) }} MXN</td>
        </tr>
        <tr style="background-color:#fcfcfc;">
            <td><strong style="position:relative; float:right;">TOTAL: </strong>  </td>
            <td style="text-align: right;"><strong>{{ number_format($pedido->total, 2) }} MXN</strong></td>
        </tr>
    </table>

    @if ($index < $pedidos->count() - 1)
    <div class="page-break"></div>
    @endif
    @endforeach

    <!--<div id="footer">
        <div class="page-number"></div>
    </div>-->

    <script type="text/php">
        if (isset($pdf)) {
            $text = "Página {PAGE_NUM} de {PAGE_COUNT}";
            $size = 10;
            $font = $fontMetrics->getFont("Verdana");
            $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
            $x = ($pdf->get_width() - $width) / 2;
            $y = $pdf->get_height() - 35;
            $pdf->page_text($x, $y, $text, $font, $size);
        }
    </script>

</body>
</html>
