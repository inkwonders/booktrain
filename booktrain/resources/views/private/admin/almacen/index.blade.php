@extends('layouts.logged')

@section('title')
    Almacén
@endsection

@section('menu_almacen_selected')
    border-b-green--BK font-bold
@endsection

@section('css')
    @livewireStyles
@endsection


@section('contenido')
<section class="w-full flex flex-col items-center justify-center h-auto pl-2 py-4">
    @hasrole('Admin|Super Admin')
    @if (auth()->user()->hasPermissionTo('Almacen'))
        <div class="w-full flex flex-col relative h-auto min-h-interno border rounded-md border-color-gray--BK justify-start">

            <div class="flex flex-col items-center p-10 relative w-full h-auto">
                <div class="relative flex justify-between w-full pb-4 mb-5">
                    <h2 class="text-greenBT font-rob-bold uppercase text-xl">Almacén</h2>
                    <div class="flex flex-row">
                        <a href="{{ route('admin.almacen.vermovimientos') }}" class="bg-greenBT text-white font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer mr-2">VER MOVIMIENTOS</a>
                        <a href="{{ route('admin.almacen.movimientos') }}" class="background-blue--bk text-white font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer">+ NUEVO MOVIMIENTO</a>
                    </div>
                </div>
                <div class="w-full">
                    @if (session()->has('message'))
                        <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
                            <div class="bg-white rounded-lg w-1/3">
                                <div class="flex flex-col items-start p-4">
                                    <div class="flex items-center w-full justify-center">
                                        <div class="text-gray-900 font-medium text-lg pb-3">
                                            <h2 class="rob-bold color-blue--bk text-2xl">{{ session('message') }}</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @livewire('inventario')
                </div>
            </div>
        </div>
    @endif
    @endhasrole
</section>

@endsection

@section('js')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
@endsection

