{{-- Inicio modal --}}
<input type="hidden" id="pedido_id" value="">

<div class="fixed inset-0 z-10 hidden overflow-y-auto" id="modalStatus" aria-labelledby="modal-title" role="dialog" aria-modal="true" >
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
            <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                    <div class="pb-4 sm:flex sm:items-start">
                        <div class="mt-3 text-center">
                            <span class="text-lg text-center font-rob-medium">
                            Selecciona el estado al que quieres cambiar este pedido:
                            </span>
                            <div class="relative w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                                <label for="select_motivos" class="cursor-pointer select_motivos"></label>
                                <select id="nuevo_status" required class="relative w-full p-2 mr-2 border appearance-none color-gray--bk">
                                    <option disabled selected value="0">Selecciona</option>|
                                    <option value="PAGADO">PAGADO</option>
                                    <option value="CANCELADO">CANCELADO</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="relative flex justify-between w-full">
                        <button id="modificar_status" class="w-1/2 p-2 m-4 text-white rounded-md bg-greenBT font-rob-bold">Modificar</button>
                        <button type="button" class="w-1/2 p-2 m-4 text-white rounded-md cerrar bg-grayBT font-rob-bold">Cancelar</button>
                    </div>
            </div>
        </div>
    </div>
</div>
{{-- Fin modal --}}

{{-- Inicio modal --}}
<div class="fixed inset-0 z-10 hidden overflow-y-auto" id="modalFacturaRelacionada" aria-labelledby="modal-title" role="dialog" aria-modal="true" >
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                <div class="pb-4 sm:flex sm:items-start">
                    <div class="w-full mt-3 text-center">
                        <span class="text-lg text-center font-rob-medium">
                        Indica el folio de la factura:
                        </span>
                        <div class="relative w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <input type="text" id="f_referencia_factura" class="relative w-full p-2 mr-2 appearance-none" value="">
                            <input type="hidden" id="f_pedido_id" value="">
                        </div>
                    </div>
                </div>
                <div class="relative flex justify-between w-full">
                    <button id="modificar_referencia_factura" class="w-1/2 p-2 m-4 text-white rounded-md bg-greenBT font-rob-bold">Modificar</button>
                    <button type="button" class="w-1/2 p-2 m-4 text-white rounded-md cerrar bg-grayBT font-rob-bold">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Fin modal --}}

{{-- Inicio modal (Activar Cancelar) --}}
<div class="fixed inset-0 z-10 hidden overflow-y-auto" id="modalActivarCancelar" aria-labelledby="modal-title" role="dialog" aria-modal="true" >
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                <div class="pb-4 sm:flex sm:items-start">
                    <div class="mt-3 text-center">
                        <span class="text-lg text-center font-rob-medium">
                        ¿Desea cambiar el estado de este pedido?
                        </span>

                    </div>
                </div>
                <div class="relative flex justify-between w-full">
                    <button id="activar_cancelar_status" class="w-1/2 p-2 m-4 text-white rounded-md bg-greenBT font-rob-bold">Modificar</button>
                    <button type="button" class="w-1/2 p-2 m-4 text-white rounded-md cerrar bg-grayBT font-rob-bold">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Fin modal --}}

{{-- Inicio modal envio de correo --}}
<div class="fixed inset-0 z-10 hidden overflow-y-auto" id="modalEnvioCorreo" aria-labelledby="modal-title" role="dialog" aria-modal="true" >
    <div class="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" aria-hidden="true"></div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
        <div class="inline-block overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
            <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                <div class="pb-4 sm:flex sm:items-start">
                    <div class="mt-3 text-center">
                        <span class="text-lg text-center font-rob-medium">
                        ¿Deseas enviar el correo de confirmación para éste pedido?
                        </span>

                    </div>
                </div>
                <div class="relative flex justify-between w-full">
                    <button id="enviar_correo" class="w-1/2 p-2 m-4 text-white rounded-md bg-greenBT font-rob-bold">Enviar</button>
                    <button type="button" class="w-1/2 p-2 m-4 text-white rounded-md cerrar bg-grayBT font-rob-bold">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Fin modal --}}
