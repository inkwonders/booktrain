@extends('layouts.logged')
@section('title')
    Libros
@endsection
@section('librosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <style>
        .data-table-diseño {
            width: 100%;
        }
        .data-table-diseño [wire\:id] {
            width: 100%;
        }
        .data-table-diseño span[aria-current] > span {
            background-color: var(--main-blue--bk);
            color: white;
        }
        .data-table-diseño span[wire\:key] > button:hover {
            background-color: var(--main-green--bk);
            color: white;
        }
        .data-table-diseño input {
            padding-left: 0.5rem;
            padding-right: 2rem;
        }
    </style>
@livewireStyles
@endsection


@section('contenido')
    <section class="flex flex-col items-center justify-start w-full h-auto py-4 pl-2 min-h-custom">
        @hasrole('Admin|Super Admin')
        @if (auth()->user()->hasDirectPermission('Libros'))
            <div class="relative flex flex-col justify-start w-full h-auto border rounded-md border-color-gray--BK min-h-interno">
                <div class="relative flex flex-col items-center w-full h-auto p-10">
                    <div class="relative flex justify-between w-full pb-4 mb-5">
                        <h2 class="text-xl uppercase color-red--bk font-rob-bold">LIBROS</h2>
                        <label class="relative flex justify-between w-1/4">
                            <a href="nuevo_libro" class="flex items-center justify-center w-1/2 p-2 mr-4 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold">+ NUEVO LIBRO</a>
                            <a href="/carga_libros" class="flex items-center justify-center w-1/2 p-2 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold">+ IMPORTAR LIBROS</a>
                        </label>
                    </div>
                    <div class="w-full">
                        <div class="data-table-diseño">
                            <livewire:lista-libros />
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @endhasrole
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>

    <script>

        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('element.updated', (el, component) => {
                element_input = document.querySelector('[placeholder="Buscar"]');
                element_input.focus();
            })
        });
    </script>
@endsection
