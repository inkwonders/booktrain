
<div class="flex items-center justify-center w-full">
    <div class="relative flex flex-col items-center w-4/5 mt-12 ">
        <div class="relative flex flex-row w-full">
            <input id="inicio" wire:model="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 px-2 border rounded-md h-11 text-grayBT" type="text" >
            <input id="fin" wire:model="fin" required placeholder="FECHA FINAL" class="relative w-1/3 px-3 mx-3 border rounded-md h-11 text-grayBT" type="text">
            <button wire:click="exportar" class="w-1/3 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">EXPORTAR A EXCEL</button>

            <img class="max-h-10" src="{{ asset('img/spinner.gif') }}" wire:loading wire:target="exportar">
        </div>

        <div class="flex flex-col items-center w-full mt-4 justify-evenly">
        </div>
    </div>
</div>
