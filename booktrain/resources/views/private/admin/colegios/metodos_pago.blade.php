@extends('layouts.logged')
@section('title', 'CONFIGURACIÓN DE PAGO')

@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col w-full h-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col justify-center w-full h-full border rounded-md border-color-gray--BK">
            @livewire('configuracion-pago', ['colegio' => $colegio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection
