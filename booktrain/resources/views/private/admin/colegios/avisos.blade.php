@extends('layouts.logged')
@section('title')
    Colegios
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
    {{-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> --}}
    <style>
        .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 25px;
        }

        /* Hide default HTML checkbox */
        .switch input {
        opacity: 0;
        width: 0;
        height: 0;
        }

        /* The slider */
        .slider {
        border-radius: 5px;
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: red;
        -webkit-transition: .4s;
        transition: .4s;
        }

        .slider:before {
        position: absolute;
        content: "";
        height: 16px;
        width: 12px;
        left: 4px;
        bottom: 4px;
        border-radius: 3px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #81b84e;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #81b84e;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(36px);
        -ms-transform: translateX(36px);
        transform: translateX(36px);
        }

    </style>
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="inline-flex w-full py-4 pl-2 min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col w-full border rounded-md border-color-gray--BK">
            <div class="relative flex flex-col w-full p-10">

                <div>
                    @if (session()->has('message'))
                        <div class="fixed bottom-0 left-0 z-30 flex items-center justify-center w-full h-full bg-gray-600 bg-opacity-50">
                            <div class="w-1/3 bg-white rounded-lg">
                                <div class="flex flex-col items-start p-4">
                                    <div class="flex items-center justify-center w-full">
                                        <div class="pb-3 text-lg font-medium text-gray-900">
                                            <h2 class="text-2xl rob-bold color-blue--bk">{{ session('message') }}</h2>
                                        </div>
                                    </div>
                                    <div class="w-full">
                                        <div class="flex justify-around w-full pt-5">
                                            <div wire:click.prevent="delete({{$notificacion_elegida}})" class="flex items-center justify-center w-1/4 text-white uppercase border border-solid rounded-md cursor-pointer background-red--bk font-rob-bold hover:bg-white hover:border-red-500 hover:text-redBT">
                                                Eliminar
                                            </div>
                                            <div wire:click="cancelar()" class="flex items-center justify-center w-1/4 px-4 py-2 border border-solid rounded cursor-pointer roboto-bold text-grayBT hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm">
                                                Cancelar
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="relative flex justify-between w-full mb-4">
                    <h2 class="text-xl font-bold color-green--bk">AVISOS | {{$colegio->nombre}}</h2>
                    <a href="nuevo/{{$colegio->id}}" class="flex items-center justify-center w-1/6 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold h-11">+ NUEVO AVISO</a>
                </div>
                @livewire('lista-avisos-colegio-editando', ['colegio' => $colegio])
                <div class="relative flex items-center justify-center w-full mt-14">
                    <a href="{{ route('admin.colegios') }}" class="relative flex items-center justify-center w-1/6 p-3 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
                </div>
            </div>
        </div>
        </div>
    @endhasrole
</section>
@endhasrole
@endsection
