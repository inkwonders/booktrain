@extends('layouts.logged')
@section('title')
    Catálogo de libros de BookTrain
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
<style>

</style>

@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col w-full h-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col justify-center w-full h-full border rounded-md border-color-gray--BK">
            @livewire('libros-colegio-editando', ['colegio' => $colegio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')

<script>
    window.addEventListener('asignar_correcto', event => {
        Swal.fire('', 'El libro se ha asignado correctamente.', 'success').then(function(){
            location.reload();
        });
    })
    window.addEventListener('editar_correcto', event => {
        Swal.fire('', 'El libro se ha editado correctamente.', 'success').then(function(){
            location.reload();
        });
    })
    window.addEventListener('libro_ya_asignado', event => {
        Swal.fire('Error', 'El libro ya ha sido asignado a este grado.', 'error').then(function(){
            location.reload();
        });
    })
</script>

@endsection


