@extends('layouts.logged')
@section('title')
    Editar sección
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
<style>
    .table-striped>tbody>tr:nth-child(odd)>td,
    .table-striped>tbody>tr:nth-child(odd)>th {
     background-color: #F2F2F2;
    }
    .table-striped>tbody>tr:nth-child(even)>td,
    .table-striped>tbody>tr:nth-child(even)>th {
     background-color: white;
    }
</style>
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col items-center justify-center w-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col justify-center w-full h-full border rounded-md border-color-gray--BK">
            @livewire('editar-seccion-colegio', ['colegio' => $colegio, 'seccion_id' => $seccion_id])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')

<script>
    window.addEventListener('editar_correcto', event => {
        Swal.fire('Se ha actualizado correctamente.', '', 'success').then(function(){
            location.reload();
        });
    })
    window.addEventListener('relaciones', event => {
        Swal.fire('No se puede eliminar el grado debido a que tiene libros o pedidos asociados.', '', 'error').then(function(){
            location.reload();
        });
    })
    window.addEventListener('eliminar', event => {
        Swal.fire('Grado eliminado correctamente.', '', 'success').then(function(){
            location.reload();
        });
    })
</script>

@endsection
