@extends('layouts.logged')

@section('title', isset($colegio) ? 'Editando colegio' : 'Nuevo colegio')

@section('contenido')
@hasrole('Admin|Super Admin')
<section class="w-full inline-flex pl-2 py-4">
    @hasrole('Admin|Super Admin')
    <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK justify-center p-6">
        @livewire('editar-colegio', ['colegio' => $colegio])
    </div>
    @endhasrole
</section>
@endhasrole
@endsection
