@extends('layouts.logged')
@section('title')
    Agregar método de envio
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col w-full px-2 py-4">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col w-full border rounded-md border-color-gray--BK">
            @livewire('agregar-metodo-envio', ['colegio' => $colegio, 'metodo_envio' => $metodo_envio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection
