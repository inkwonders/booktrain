@extends('layouts.logged')
@section('title')
    Editando seccion
@endsection
@section('colegiosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="w-full inline-flex pl-2 py-4">
    @hasrole('Admin|Super Admin')
    <div class="inline-block w-full px-1 py-1">
        @livewire('seccion', ['id_colegio' => $colegio_id, 'id_seccion' => $seccion_id])
    </div>
    @endhasrole
</section>
@endhasrole
@endsection
