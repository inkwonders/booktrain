@extends('layouts.logged')
@section('title')
    EDITAR CONFIGURACIÓN DE PAGO
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
    <section class="inline-flex w-full h-full min-h-full py-4 pl-2">
        @hasrole('Admin|Super Admin')
            <div class="inline-block w-full px-1 py-1">
                @livewire('editar-forma-pago-editando', ['colegio' => $colegio, 'metodo_pago_id' => $metodo_pago, 'forma_pago_id' => $forma_pago])
            </div>
        @endhasrole
    </section>
@endsection

