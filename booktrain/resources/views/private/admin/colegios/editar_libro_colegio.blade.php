@extends('layouts.logged')
@section('title')
    EDITAR LIBRO COLEGIO
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col items-center justify-center w-full px-2 py-4">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col justify-center w-full border rounded-md border-color-gray--BK">
            @livewire('editar-libro-colegio', ['colegio' => $colegio, 'libro_id' => $libro_id, 'paquete_id' => $paquete_id])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection

