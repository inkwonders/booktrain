@extends('layouts.logged')
@section('title')
    Nuevo Aviso
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="flex flex-col w-full px-2 py-4">
    @hasrole('Admin|Super Admin')
        <div class="relative flex flex-col w-full border rounded-md min-h-custom border-color-gray--BK">
            @livewire('nuevo-aviso-colegio-editando', ['colegio' => $colegio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection
