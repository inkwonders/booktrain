@extends('layouts.logged')
@section('title')
    Editar método de envio
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="w-full flex flex-col items-center justify-center min-h-custom px-2 py-4">
    @hasrole('Admin|Super Admin')
        <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK justify-center">
            @livewire('editar-metodo-envio', ['colegio' => $colegio, 'metodo_envio' => $metodo_envio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')
<script>
    window.addEventListener('editar_correcto', event => {
        Swal.fire('Método de envío editado con éxito.', '', 'success')
        this.submit();
    })
</script>
@endsection
