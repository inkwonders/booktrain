@extends('layouts.logged')
@section('title')
    Colegios (editando)
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection


@section('contenido')
@hasrole('Admin|Super Admin')
<section class="w-full flex flex-col h-full min-h-custom px-2 py-4">
    @hasrole('Admin|Super Admin')
        <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK justify-center">
            @livewire('editar-colegio-editando', ['colegio' => $colegio])
        </div>
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')
<script>
    window.addEventListener('editar_correcto', event => {
        Swal.fire('Colegio actualizado con éxito.', '', 'success')
        this.submit();
    })
</script>
@endsection
