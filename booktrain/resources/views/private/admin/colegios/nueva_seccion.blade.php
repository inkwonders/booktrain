@extends('layouts.logged')
@section('title')
    Nueva Sección
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
@hasrole('Admin|Super Admin')
<section class="inline-flex w-full min-h-full py-4 pl-2">
    @hasrole('Admin|Super Admin')
    @livewire('nueva-seccion-editando', ['colegio' => $colegio])
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')
<script>
    window.addEventListener('agregar_correcto', event => {
        Swal.fire('Sección agregada correctamente.', '', 'success')
        this.submit();
    })
</script>

@endsection
