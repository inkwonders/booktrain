@extends('layouts.logged')
@section('title')
    Editando colegio
@endsection
@section('colegios2Selected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
@hasrole('Admin|Super Admin')
<section class="inline-flex w-full py-4 pl-2 min-h-custom">
    @hasrole('Admin|Super Admin')
        @livewire('lista-secciones-editando', ['colegio' => $colegio])
    @endhasrole
</section>
@endhasrole
@endsection

@section('js')
<script>
    window.addEventListener('eliminar', event => {
        Swal.fire('Grado eliminado correctamente.', '', 'success').then(function(){
            location.reload();
        });
    })
</script>
@endsection
