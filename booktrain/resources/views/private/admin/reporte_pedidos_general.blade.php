@extends('layouts.logged')

@section('title', 'Pedidos')

@section('referenciasSelected', 'border-b-green--BK font-bold')

@section('contenido')
<section class="flex flex-col items-center justify-start w-full px-2 py-4 min-h-custom">
    @hasrole('Admin|Super Admin')
    @if (auth()->user()->hasDirectPermission('Pedidos'))

        <div class="relative flex flex-col justify-start w-full h-auto p-10 border rounded-md border-color-gray--BK min-h-interno">
            <div class="relative flex items-start justify-between w-full mb-4">
                <h2 class="text-xl font-bold uppercase color-red--bk">Pedidos</h2>
            </div>
            <form action="/reporte_pedidos/pedidos-referencias" method="GET" >
            <div class="flex items-center justify-center w-full">
                <div class="relative flex flex-col items-center w-4/5 mt-12 ">

                    <div class="relative w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                        <select name="status" id="status" class="relative w-full p-2 appearance-none text-grayBT">
                            <option selected value="0">Cualquier estatus</option>
                            <option value="ENTREGADO">ENTREGADO</option>
                            <option value="PAGADO">PAGADO</option>
                            <option value="PROCESADO">PROCESADO</option>
                            <option value="ENVIADO">ENVIADO</option>
                            <option value="CANCELADO">CANCELADO</option>
                            <option value="PAGO RECHAZADO">PAGO RECHAZADO</option>
                            <option value="PAGO EN REVISION">PAGO EN REVISION</option>
                            <option value="ENVIO PARCIAL">ENVIO PARCIAL</option>
                            <option value="ENVIO LIQUIDADO">ENVIO LIQUIDADO</option>
                            <option value="ENTREGA PARCIAL">ENTREGA PARCIAL</option>
                            <option value="ENTREGA LIQUIDADA">ENTREGA LIQUIDADA</option>
                        </select>
                    </div>
                    <div class="relative flex flex-row w-full">
                        <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <select name="factura" id="factura" class="p-2 appearance-none text-grayBT" >
                                <option selected value="0">Facturas Todo</option>
                                <option value="1">SI solicita</option>
                                <option value="2">NO solicita</option>
                            </select>
                        </div>
                        <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <select name="metodo_pagos" id="metodo_pagos" class="p-2 appearance-none text-grayBT">
                                <option selected value="0">Cualquier Método de pago</option>
                                <option value="CREDITO">CREDITO</option>
                                <option value="DEBITO">DEBITO</option>
                                <option value="AMEX">AMEX</option>
                                <option value="BANCO">BANCO</option>
                                <option value="TIENDA">TIENDA</option>
                            </select>
                        </div>
                        <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <select name="forma_pagos" id="forma_pagos" class="p-2 appearance-none text-grayBT">
                                <option selected value="0">Cualquier Forma de pago</option>
                                <option value="PUE">PUE</option>
                                <option value="DIFERIDO3M">Diferido 3m</option>
                                <option value="DIFERIDO6M">Diferido 6m</option>
                                <option value="DIFERIDO9">Diferido 9m</option>
                            </select>
                        </div>
                        <div class="relative w-1/4 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <select name="forma_entrega" id="forma_entrega" class="p-2 appearance-none text-grayBT">
                                <option selected value="0">Cualquier Forma de entrega</option>
                                <option value="1">Colegio</option>
                                <option value="2">Domicilio</option>
                                <option value="3">Pick-Up</option>
                            </select>
                        </div>
                    </div>
                    <div class="relative flex flex-row w-full px-2 my-2">
                        <input name="inicio" id="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 px-2 mr-4 border rounded-md h-11 text-grayBT" type="text" >
                        <input name="fin" id="fin" required placeholder="FECHA FINAL" class="relative w-1/3 px-2 border rounded-md h-11 text-grayBT" type="text">

                    <button type="button" id="buscar" class="relative w-1/3 ml-4 font-bold text-white border border-gray-400 border-solid rounded h-11 background-green--bk hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm">BUSCAR</button>
                    </div>

                    <div class="flex flex-col items-center w-full mt-4 justify-evenly">
                        <button type="submit" id="exportar" class="w-56 h-12 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">EXPORTAR EXCEL</button>
                    </div>

                </div>

            </div>
            </form>
            <livewire:pedidos-generales-rebuild/>
            <livewire:contenedor-reporte-pedidos-general />
        </div>
    @endif
    @endhasrole
</section>
@endsection

@section('js-interno')
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    window.addEventListener('cerrar_modal', event => {
        $("#modalStatus").addClass("hidden");
        $("#pedido_id").val("");
    });

    $(document).ready(function() {
        $(".cerrar").on("click", function(){
            $("#modalStatus").addClass("hidden");
            $("#pedido_id").val("");
        });

        // Datepicker
        $("#inicio").datepicker({ dateFormat: 'dd-mm-yy' });
        $("#fin").datepicker({ dateFormat: 'dd-mm-yy' });

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' + dd;

        $("#inicio").val(today);
        $("#fin").val(today);
    });
</script>
@endsection
