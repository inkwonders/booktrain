@extends('layouts.logged')
    @section('title')
        Log
    @endsection
    @section('logSelected')
    border-b-green--BK font-bold
    @endsection

    @section('css')
    @endsection


    @section('contenido')
        <section class="flex flex-col items-center justify-start w-full h-auto py-4 pl-2 min-h-custom">
            @hasrole('Admin|Super Admin')
            @if (auth()->user()->hasDirectPermission('Log'))
                <div class="relative flex flex-col justify-start w-full h-auto border rounded-md border-color-gray--BK min-h-interno">
                    <div class="relative flex flex-col items-center w-full h-auto p-10">
                        <div class="relative flex justify-between w-full pb-4 mb-5">
                            <h2 class="text-xl uppercase color-red--bk font-rob-bold">LOG</h2>
                        </div>
                        <div class="w-full">
                            <div class="data-table-diseño">
                                <div class="relative flex flex-col w-full h-auto p-4">
                                    <div  class="relative w-full h-auto">
                                        @livewire('datatable-log', ['inicio' => $inicio, 'fin' => $fin, 'usuario' => $usuario])
                                    </div>
                                    <div class="relative flex justify-center w-full mt-6">
                                        <div class="relative flex justify-around w-1/4">
                                            <a href="/admin/log" class="flex items-center justify-center w-full px-4 py-2 text-white uppercase bg-white border border-solid rounded roboto-bold color-gray--bk border-grayBT hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm">
                                            Regresar
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @endhasrole
        </section>
    @endsection

    @section('js')
    @endsection

