@extends('layouts.logged')

@section('title')
    Paquetería
@endsection

@section('paqueteriaSelected')
    border-b-green--BK
@endsection

@section('contenido')
<div class="w-full min-h-custom py-4">
@livewire('servicios-paqueteria')
</div>
@endsection
