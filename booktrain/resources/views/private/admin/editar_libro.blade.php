@extends('layouts.logged')

@section('title', 'Libros')

@section('librosSelected')
border-b-green--BK font-bold
@endsection

@section('contenido')
    <section class="w-full min-h-full h-full inline-flex pl-2 py-4">
        @hasrole('Admin|Super Admin')
            <div class="inline-block w-full px-1 py-1">
                @livewire('editar-libro', ['libro' => $libro])
            </div>
        @endhasrole
    </section>
@endsection
