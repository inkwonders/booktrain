@extends('layouts.logged')
@section('title')
    Impresion Pedidos
@endsection

@section('impresionPedidosSelected')
    border-b-green--BK
@endsection

@hasrole('Admin')
@section('contenido')
<section class="w-full flex flex-col items-center justify-center min-h-custom px-2 py-4">
    <div class="w-full flex flex-col relative h-auto min-h-interno border rounded-md border-color-gray--BK justify-start p-10">
        <div class="relative flex flex-col justify-between w-full mb-1">
            <h2 class="color-red--bk font-bold text-xl">IMPRESION DE PEDIDOS</h2>
        </div>
        <div class="data-table-diseño">
            <livewire:datatable-impresion-pedidos  searchable="folio, nombre_contacto, apellidos_contacto, email" exportable />
        </div>
    </div>
 </section>
@endsection
@endhasrole

@section('css')
<style>
    /* .space-y-4 > .p-6 {
        display: none;
    } */

    a.disabled {
        pointer-events: none !important;
        cursor: default !important;
    }
</style>
@endsection

@section('js-interno')
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">

    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.7/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">

        let filtro_fecha = '';

        function emit_filtro_fecha(){
            if (filtro_fecha != '') {
                Livewire.emit('filtroFecha', filtro_fecha)
            }
        }

        $(document).ready(function() {
            // Datepicker
            $("#filtro_fecha").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    filtro_fecha = date
                    // Livewire.emit('filtroFecha', date)
                }
            });

            $("#fecha_impresion").datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: (date, obj) => {
                    Livewire.emit('fechaImpresion', date)
                }
            });
        });
    </script>
@endsection



