@extends('layouts.logged')

@section('contenido')
    <section class="flex flex-col items-center justify-start w-full h-auto py-4 pl-2 min-h-custom">
        @hasrole('Admin|Super Admin')
            <div class="relative flex flex-col justify-start w-full h-auto border rounded-md border-color-gray--BK min-h-interno">
                <div class="relative flex flex-col items-center w-full h-auto p-10">
                    <div class="relative flex justify-between w-full pb-4 mb-5">
                        <h2 class="text-xl uppercase color-red--bk font-rob-bold">CONCILIACIÓN DE PEDIDOS</h2>
                    </div>
                    <div class="w-full">
                        <div class="data-table-diseño">
                            <form action="{{ route('admin.pedidos.conciliacion') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <p>
                                    <label for="archivo">Cargue un archivo de excel, la primer columna son los pedidos</label>
                                <br>
                                <input type="file" name="archivo" id="archivo">
                                <button type="submit" class="px-4 py-2 mt-5 border border-green-500 rounded rounded-md">Cargar</button>
                                </p>
                                @include('errors.validation')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endhasrole
    </section>
@endsection
