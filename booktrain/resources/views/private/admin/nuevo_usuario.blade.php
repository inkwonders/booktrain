@extends('layouts.logged')
@section('title')
    Nuevo Usuario
@endsection
@section('usuariosSelected')
border-b-green--BK font-bold
@endsection

@section('css')
@livewireStyles
@endsection

@section('contenido')
    <section class="w-full inline-flex px-4 py-4">
        @hasrole('Admin|Super Admin')
            <div class="inline-block w-full px-1 py-1">
                @livewire('nuevo-usuario')
            </div>
        @endhasrole
    </section>
@endsection
