
<div class="flex items-center justify-center w-full">
    <div class="relative flex flex-col items-center w-4/5 mt-12 ">

        <div class="relative flex flex-row w-full mb-3 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
            <select wire:model="status" class="relative w-full h-10 p-2 appearance-none text-grayBT">
                <option selected value="">CUALQUIER ESTADO DEL PEDIDO</option>
                <option value="ENTREGADO">ENTREGADO</option>
                <option value="PAGADO">PAGADO</option>
                <option value="PROCESADO">PROCESADO</option>
                <option value="ENVIADO">ENVIADO</option>
                <option value="CANCELADO">CANCELADO</option>
                <option value="PAGO RECHAZADO">PAGO RECHAZADO</option>
                <option value="PAGO EN REVISION">PAGO EN REVISION</option>
                <option value="ENVIO PARCIAL">ENVIO PARCIAL</option>
                <option value="ENVIO LIQUIDADO">ENVIO LIQUIDADO</option>
                <option value="ENTREGA PARCIAL">ENTREGA PARCIAL</option>
                <option value="ENTREGA LIQUIDADA">ENTREGA LIQUIDADA</option>
            </select>
        </div>
        <div class="relative flex flex-row w-full mb-3">
            <div class="relative w-1/4 mr-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                <select wire:model="facturacion" class="w-full p-2 appearance-none text-grayBT" >
                    <option selected value="">FACTURACIÓN INDIFERENTE</option>
                    <option value="1">Solicitó facturación</option>
                    <option value="2">No solicitó facturación</option>
                </select>
            </div>
            <div class="relative w-1/4 mr-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                <select wire:model="metodo_pago" class="w-full p-2 appearance-none text-grayBT">
                    <option selected value="">CUALQUIER MÉTODO DE PAGO</option>
                    @foreach ($this->metodos_pago as $metodo_pago)
                    <option value="{{ $metodo_pago->codigo }}">{{ $metodo_pago->descripcion }}</option>
                    @endforeach
                </select>
            </div>
            <div class="relative w-1/4 mr-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                <select wire:model="forma_pago" class="w-full p-2 appearance-none text-grayBT">
                    <option selected value="">CUALQUIER FORMA DE PAGO</option>
                    @foreach ($this->formas_pago as $forma_pago)
                    <option value="{{ $forma_pago->codigo }}">{{ $forma_pago->descripcion }}</option>
                    @endforeach
                </select>
            </div>
            <div class="relative w-1/4 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                <select wire:model="tipo_entrega" class="w-full p-2 appearance-none text-grayBT">
                    <option selected value="0">CUALQUIER FORMA DE ENTREGA</option>
                    <option value="1">Entrega en colegio</option>
                    <option value="2">Envío a domicilio</option>
                    <option value="3">Pick-Up en sucursal</option>
                </select>
            </div>
        </div>
        <div class="relative flex flex-row w-full">
            <input id="inicio" wire:model="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 px-2 border rounded-md h-11 text-grayBT" type="text" >
            <input id="fin" wire:model="fin" required placeholder="FECHA FINAL" class="relative w-1/3 px-3 mx-3 border rounded-md h-11 text-grayBT" type="text">
            <button wire:click="exportar" class="w-1/3 font-bold text-white border border-gray-400 border-solid rounded background-green--bk hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm">EXPORTAR A EXCEL</button>
            
            <img class="max-h-10" src="{{ asset('img/spinner.gif') }}" wire:loading wire:target="exportar">
        </div>

        <div class="flex flex-col items-center w-full mt-4 justify-evenly">
        </div>
    </div>
</div>
