@extends('layouts.logged')

@section('title')
    Exportar Exel
@endsection

@section('reportesSelected')
    border-b-green--BK
@endsection

@section('css')
@endsection

@section('contenido')

    @if(session('error') != null)
        <span>{{ session('error') }}</span>
    @endif

    <section class="w-full h-auto py-4">
        <div class="w-full h-auto py-6 px-8 border border-color-gray--BK rounded-lg flex flex-col justify-between">
            <div class="w-full h-auto">
                <span class="rob-bold color-red--bk font-18">REPORTE GENERAL 01/01/2021 A 31/01/2021, COLEGIO CEDROS, TODOS LOS GRADOS, TODOS LOS ISBN, SURTIDO.</span>

                <div class="w-full h-auto overflow-x-auto scrool_azul block mt-4">
                    <div class="w-auto min-w-full h-auto flex flex-row">
                        <table class="min-w-full tabla-p-0">
                            <tr>




                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="w-auto min-w-full h-8 my-2 flex justify-center items-center border-r border-color-gray--BK">
                                            <span class="rob-bold color-green--bk font-18">PEDIDO</span>
                                        </div>
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>No. Pedido</span>
                                                    </th>
                                                    <th>
                                                        <span>Fecha</span>
                                                    </th>
                                                    <th>
                                                        <span>Colegio</span>
                                                    </th>
                                                    <th>
                                                        <span>Grado</span>
                                                    </th>
                                                    <th>
                                                        <span>ISBN</span>
                                                    </th>
                                                    <th>
                                                        <span>Título</span>
                                                    </th>
                                                    <th>
                                                        <span>Piezas</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>PR50722</span>
                                                    </td>
                                                    <td>
                                                        <span>29/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>CEDROS</span>
                                                    </td>
                                                    <td>
                                                        <span>SEGUNDO</span>
                                                    </td>
                                                    <td>
                                                        <span>9729072429314</span>
                                                    </td>
                                                    <td>
                                                        <span>Historia 2. Secundaria. Conecta más</span>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>

                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="w-auto min-w-full h-8 my-2 flex justify-center items-center border-r border-color-gray--BK">
                                            <span class="rob-bold color-green--bk font-18">CONTACTO</span>
                                        </div>
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>Nombre del alumno</span>
                                                    </th>
                                                    <th>
                                                        <span>Estatus</span>
                                                    </th>
                                                    <th>
                                                        <span>Nombre de contacto</span>
                                                    </th>
                                                    <th>
                                                        <span>Teléfono</span>
                                                    </th>
                                                    <th>
                                                        <span>Correo</span>
                                                    </th>
                                                    <th>
                                                        <span>RFC</span>
                                                    </th>
                                                    <th>
                                                        <span>Razón social</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>MASI900102P78</span>
                                                    </td>
                                                    <td>
                                                        <span>Pablo Hernández</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>MASI900102P78</span>
                                                    </td>
                                                    <td>
                                                        <span>Pablo Hernández</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Liliana Gómez</span>
                                                    </td>
                                                    <td>
                                                        <span>Por Surtir</span>
                                                    </td>
                                                    <td>
                                                        <span>Luz María Martínez</span>
                                                    </td>
                                                    <td>
                                                        <span>4422292929</span>
                                                    </td>
                                                    <td>
                                                        <span>luzma@gmail.com</span>
                                                    </td>
                                                    <td>
                                                        <span>MASI900102P78</span>
                                                    </td>
                                                    <td>
                                                        <span>Pablo Hernández</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>

                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="w-auto min-w-full h-8 my-2 flex justify-center items-center border-r border-color-gray--BK">
                                            <span class="rob-bold color-green--bk font-18">FACTURACIÓN</span>
                                        </div>
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>Monto</span>
                                                    </th>
                                                    <th>
                                                        <span>Método de pago</span>
                                                    </th>
                                                    <th>
                                                        <span>Forma de pago</span>
                                                    </th>
                                                    <th>
                                                        <span>&nbsp;</span>
                                                    </th>
                                                    <th>
                                                        <span>Costo de envío</span>
                                                    </th>
                                                    <th class="fondo_amarillo">
                                                        <span>Factura</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>6 meses diferidos</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>$160.00</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>Pago en una sola exhibición</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>Pago en una sola exhibición</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>6 meses diferidos</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>$160.00</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>Pago en una sola exhibición</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>Pago en una sola exhibición</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>$600.00</span>
                                                    </td>
                                                    <td>
                                                        <span>Tarjeta de crédito</span>
                                                    </td>
                                                    <td>
                                                        <span>6 meses diferidos</span>
                                                    </td>
                                                    <td>
                                                        <span>$25.00</span>
                                                    </td>
                                                    <td>
                                                        <span>$160.00</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>

                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="w-auto min-w-full h-8 my-2 flex justify-center items-center border-r border-color-gray--BK">
                                            <span class="rob-bold color-green--bk font-18">DISTRIBUCIÓN</span>
                                        </div>
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th>
                                                        <span>Tipo de entrega</span>
                                                    </th>
                                                    <th>
                                                        <span>Calle</span>
                                                    </th>
                                                    <th>
                                                        <span>Número exterior</span>
                                                    </th>
                                                    <th>
                                                        <span>Número interior</span>
                                                    </th>
                                                    <th>
                                                        <span>Colonia</span>
                                                    </th>
                                                    <th>
                                                        <span>C.P.</span>
                                                    </th>
                                                    <th>
                                                        <span>Estado</span>
                                                    </th>
                                                    <th>
                                                        <span>&nbsp;</span>
                                                    </th>
                                                    <th>
                                                        <span>Referencia</span>
                                                    </th>
                                                    <th>
                                                        <span>Fecha de envío</span>
                                                    </th>
                                                    <th>
                                                        <span>Tipo de envío</span>
                                                    </th>
                                                    <th>
                                                        <span>Número de Guía</span>
                                                    </th>
                                                    <th>
                                                        <span>Estatus</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>21/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>FedEx</span>
                                                    </td>
                                                    <td>
                                                        <span>772159213259</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>11</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Entre Arteaga y Zaragoza</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>11</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Entre Arteaga y Zaragoza</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>21/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>FedEx</span>
                                                    </td>
                                                    <td>
                                                        <span>772159213259</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>11</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Entre Arteaga y Zaragoza</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>11</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Entre Arteaga y Zaragoza</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Domicilio</span>
                                                    </td>
                                                    <td>
                                                        <span>Cerro del Tesoro</span>
                                                    </td>
                                                    <td>
                                                        <span>141</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>Colinas del Cimatario</span>
                                                    </td>
                                                    <td>
                                                        <span>76903</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>Querétaro</span>
                                                    </td>
                                                    <td>
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td>
                                                        <span>21/01/2021</span>
                                                    </td>
                                                    <td>
                                                        <span>FedEx</span>
                                                    </td>
                                                    <td>
                                                        <span>772159213259</span>
                                                    </td>
                                                    <td>
                                                        <span>Entregado apaquetería</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>

                                <td>
                                    <div class="flex flex-col min-w-full">
                                        <div class="w-auto min-w-full h-8 my-2 flex justify-center items-center border-r border-color-gray--BK">
                                            <span class="rob-bold color-green--bk font-18">COLEGIO / PICK UP</span>
                                        </div>
                                        <table class="text-center color-gray--bk espacio_tablas">
                                            <thead class="border-b border-t border-color-gray--BK h-12">
                                                <tr>
                                                    <th class="fondo_amarillo">
                                                        <span>Nombre</span>
                                                    </th>
                                                    <th class="fondo_amarillo">
                                                        <span>Fecha</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                    <td class="fondo_amarillo">
                                                        <span>&nbsp;</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>




                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="w-full h-40 flex flex-col justify-evenly items-center mt-4">
                <button class="h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">EXPORTAR EXCEL</button>
                <button class="h-12 w-56 bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</button>
            </div>
        </div>
    </section>

@endsection

@section('js-interno')
@endsection
