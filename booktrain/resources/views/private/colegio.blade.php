@extends('layouts.logged')

@section('title')
    Colegio
@endsection

@section('inicioSelected')
    border-b-green--BK
@endsection

@section('css')
    {{-- <link rel="stylesheet" href="{{ asset('/css/login.css') }}">   --}}
@endsection

@section('contenido')
    <colegio-seccion codigo='{{ $colegio->codigo }}' csrf="{{ csrf_token() }}"></colegio-seccion>
@endsection

@section('js-interno')
@endsection
