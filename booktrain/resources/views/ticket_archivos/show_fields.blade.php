<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $ticketArchivo->id }}</p>
</div>

<!-- Ticket Usuario Id Field -->
<div class="col-sm-12">
    {!! Form::label('ticket_usuario_id', 'Ticket Usuario Id:') !!}
    <p>{{ $ticketArchivo->ticket_usuario_id }}</p>
</div>

<!-- Archivo Field -->
<div class="col-sm-12">
    {!! Form::label('archivo', 'Archivo:') !!}
    <p>{{ $ticketArchivo->archivo }}</p>
</div>

<!-- Archivo Nombre Original Field -->
<div class="col-sm-12">
    {!! Form::label('archivo_nombre_original', 'Archivo Nombre Original:') !!}
    <p>{{ $ticketArchivo->archivo_nombre_original }}</p>
</div>

<!-- Orden Field -->
<div class="col-sm-12">
    {!! Form::label('orden', 'Orden:') !!}
    <p>{{ $ticketArchivo->orden }}</p>
</div>

<!-- Tipo Archivo Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_archivo', 'Tipo Archivo:') !!}
    <p>{{ $ticketArchivo->tipo_archivo }}</p>
</div>

<!-- Url Field -->
<div class="col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    <p>{{ $ticketArchivo->url }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ticketArchivo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ticketArchivo->updated_at }}</p>
</div>

