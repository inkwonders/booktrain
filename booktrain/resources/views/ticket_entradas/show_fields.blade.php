<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $ticketEntrada->id }}</p>
</div>

<!-- Ticket Usuario Id Field -->
<div class="col-sm-12">
    {!! Form::label('ticket_usuario_id', 'Ticket Usuario Id:') !!}
    <p>{{ $ticketEntrada->ticket_usuario_id }}</p>
</div>

<!-- Usuario Id Field -->
<div class="col-sm-12">
    {!! Form::label('usuario_id', 'Usuario Id:') !!}
    <p>{{ $ticketEntrada->usuario_id }}</p>
</div>

<!-- Tipo Entrada Field -->
<div class="col-sm-12">
    {!! Form::label('tipo_entrada', 'Tipo Entrada:') !!}
    <p>{{ $ticketEntrada->tipo_entrada }}</p>
</div>

<!-- Comentario Field -->
<div class="col-sm-12">
    {!! Form::label('comentario', 'Comentario:') !!}
    <p>{{ $ticketEntrada->comentario }}</p>
</div>

<!-- Carpeta Field -->
<div class="col-sm-12">
    {!! Form::label('carpeta', 'Carpeta:') !!}
    <p>{{ $ticketEntrada->carpeta }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ticketEntrada->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ticketEntrada->updated_at }}</p>
</div>

