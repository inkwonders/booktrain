<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $libroPaquete->id }}</p>
</div>

<!-- Libro Id Field -->
<div class="col-sm-12">
    {!! Form::label('libro_id', 'Libro Id:') !!}
    <p>{{ $libroPaquete->libro_id }}</p>
</div>

<!-- Paquete Id Field -->
<div class="col-sm-12">
    {!! Form::label('paquete_id', 'Paquete Id:') !!}
    <p>{{ $libroPaquete->paquete_id }}</p>
</div>

<!-- Obligatorio Field -->
<div class="col-sm-12">
    {!! Form::label('obligatorio', 'Obligatorio:') !!}
    <p>{{ $libroPaquete->obligatorio }}</p>
</div>

<!-- Activo Field -->
<div class="col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{{ $libroPaquete->activo }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $libroPaquete->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $libroPaquete->updated_at }}</p>
</div>

