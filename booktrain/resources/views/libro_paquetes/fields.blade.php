<!-- Libro Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libro_id', 'Libro Id:') !!}
    {!! Form::number('libro_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Paquete Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paquete_id', 'Paquete Id:') !!}
    {!! Form::number('paquete_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Obligatorio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obligatorio', 'Obligatorio:') !!}
    {!! Form::number('obligatorio', null, ['class' => 'form-control']) !!}
</div>

<!-- Activo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activo', 'Activo:') !!}
    {!! Form::number('activo', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush