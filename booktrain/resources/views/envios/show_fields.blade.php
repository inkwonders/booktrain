<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $envio->id }}</p>
</div>

<!-- Pedido Id Field -->
<div class="col-sm-12">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    <p>{{ $envio->pedido_id }}</p>
</div>

<!-- Fecha Recepcion Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_recepcion', 'Fecha Recepcion:') !!}
    <p>{{ $envio->fecha_recepcion }}</p>
</div>

<!-- Fecha Estimada Field -->
<div class="col-sm-12">
    {!! Form::label('fecha_estimada', 'Fecha Estimada:') !!}
    <p>{{ $envio->fecha_estimada }}</p>
</div>

<!-- Horarios Entrega Field -->
<div class="col-sm-12">
    {!! Form::label('horarios_entrega', 'Horarios Entrega:') !!}
    <p>{{ $envio->horarios_entrega }}</p>
</div>

<!-- Costo Field -->
<div class="col-sm-12">
    {!! Form::label('costo', 'Costo:') !!}
    <p>{{ $envio->costo }}</p>
</div>

<!-- Guia Field -->
<div class="col-sm-12">
    {!! Form::label('guia', 'Guia:') !!}
    <p>{{ $envio->guia }}</p>
</div>

<!-- Paqueteria Field -->
<div class="col-sm-12">
    {!! Form::label('paqueteria', 'Paqueteria:') !!}
    <p>{{ $envio->paqueteria }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $envio->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $envio->updated_at }}</p>
</div>

