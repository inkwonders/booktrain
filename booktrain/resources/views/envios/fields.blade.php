<!-- Pedido Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    {!! Form::number('pedido_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Recepcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_recepcion', 'Fecha Recepcion:') !!}
    {!! Form::text('fecha_recepcion', null, ['class' => 'form-control','id'=>'fecha_recepcion']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_recepcion').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Fecha Estimada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_estimada', 'Fecha Estimada:') !!}
    {!! Form::text('fecha_estimada', null, ['class' => 'form-control','id'=>'fecha_estimada']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#fecha_estimada').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Horarios Entrega Field -->
<div class="form-group col-sm-6">
    {!! Form::label('horarios_entrega', 'Horarios Entrega:') !!}
    {!! Form::text('horarios_entrega', null, ['class' => 'form-control']) !!}
</div>

<!-- Costo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('costo', 'Costo:') !!}
    {!! Form::number('costo', null, ['class' => 'form-control']) !!}
</div>

<!-- Guia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('guia', 'Guia:') !!}
    {!! Form::text('guia', null, ['class' => 'form-control']) !!}
</div>

<!-- Paqueteria Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paqueteria', 'Paqueteria:') !!}
    {!! Form::text('paqueteria', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush