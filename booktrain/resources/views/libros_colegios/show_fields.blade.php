<!-- Libro Id Field -->
<div class="col-sm-12">
    {!! Form::label('libro_id', 'Libro Id:') !!}
    <p>{{ $librosColegios->libro_id }}</p>
</div>

<!-- Colegio Id Field -->
<div class="col-sm-12">
    {!! Form::label('colegio_id', 'Colegio Id:') !!}
    <p>{{ $librosColegios->colegio_id }}</p>
</div>

<!-- Precio Field -->
<div class="col-sm-12">
    {!! Form::label('precio', 'Precio:') !!}
    <p>{{ $librosColegios->precio }}</p>
</div>

<!-- Paquete Id Field -->
<div class="col-sm-12">
    {!! Form::label('paquete_id', 'Paquete Id:') !!}
    <p>{{ $librosColegios->paquete_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $librosColegios->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $librosColegios->updated_at }}</p>
</div>

