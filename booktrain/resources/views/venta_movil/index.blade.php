@extends('layouts.logged')

@section('title', 'Colegio')

@section('comprarVentaMovil', 'border-b-green--BK')

@section('contenido')
@hasrole('Venta Movil')
    <div class="flex flex-col items-center w-full h-auto lg:px-16">
        <div class="box-border flex flex-col items-center w-full h-auto overflow-hidden lg:min-h-custom lg:flex-row">
            <colegio-seccion codigo='{{ auth()->user()->getDirectPermissions()->first()->name }}' csrf="{{ csrf_token() }}"></colegio-seccion>
        </div>
    </div>
@endhasrole
@endsection
