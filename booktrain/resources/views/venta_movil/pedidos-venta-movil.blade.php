@extends('layouts.logged')

@section('title')
    Pedidos
@endsection

@section('css')
@endsection

@section('pedidosSelected')
    border-b-green--BK
@endsection

@section('contenido')

@livewire('pedidos-venta-movil')


@endsection

@section('js-interno')
@endsection
