@extends('layouts.logged')

@section('title')
    Colegio
@endsection

@section('movimientoCajaSelected')
    border-b-green--BK
@endsection

@section('contenido')
@hasrole('Venta Movil')

<section class="w-full flex flex-col h-full relative min-h-custom p-6 lg:px-16">
    @livewire('movimiento-caja')
</section>
@endhasrole
@endsection

@section('js-interno')
<script>
    window.addEventListener('apertura_correcto', event => {
        Swal.fire('Caja abierta correctamente.', '', 'success').then(function(){
            window.location.href = "/venta_movil";
        })
    });
    window.addEventListener('cierre_correcto', event => {
        Swal.fire('Caja cerrada correctamente.', '', 'success').then(function(){
            window.location.href = "/venta_movil/movimiento_caja";
        });
    })
</script>
@endsection
