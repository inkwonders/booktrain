<div>
    @if (session()->has('message'))
        <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
            <div class="bg-white rounded-lg w-1/3">
                <div class="flex flex-col items-start p-4">
                    <div class="flex items-center w-full flex justify-center">
                        <div class="text-gray-900 font-medium text-lg pb-3">
                            <h2 class="rob-bold color-blue--bk text-2xl">{{ session('message') }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
