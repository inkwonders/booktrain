@extends('errors::minimal')

{{-- @section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __('Service Unavailable')) --}}

@section('title', __('Por favor regresa a partir de las 10:15am del día de hoy.'))
@section('code', '  Tienda en actualización.')
@section('message', __('Estamos actualizando nuestra tienda en línea para brindarte una mejor experiencia.'))
