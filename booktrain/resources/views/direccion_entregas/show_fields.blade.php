<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $direccionEntrega->id }}</p>
</div>

<!-- Pedido Id Field -->
<div class="col-sm-12">
    {!! Form::label('pedido_id', 'Pedido Id:') !!}
    <p>{{ $direccionEntrega->pedido_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $direccionEntrega->user_id }}</p>
</div>

<!-- Calle Field -->
<div class="col-sm-12">
    {!! Form::label('calle', 'Calle:') !!}
    <p>{{ $direccionEntrega->calle }}</p>
</div>

<!-- No Exterior Field -->
<div class="col-sm-12">
    {!! Form::label('no_exterior', 'No Exterior:') !!}
    <p>{{ $direccionEntrega->no_exterior }}</p>
</div>

<!-- No Interior Field -->
<div class="col-sm-12">
    {!! Form::label('no_interior', 'No Interior:') !!}
    <p>{{ $direccionEntrega->no_interior }}</p>
</div>

<!-- Colonia Field -->
<div class="col-sm-12">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{{ $direccionEntrega->colonia }}</p>
</div>

<!-- Cp Field -->
<div class="col-sm-12">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{{ $direccionEntrega->cp }}</p>
</div>

<!-- Estado Id Field -->
<div class="col-sm-12">
    {!! Form::label('estado_id', 'Estado Id:') !!}
    <p>{{ $direccionEntrega->estado_id }}</p>
</div>

<!-- Municipio Id Field -->
<div class="col-sm-12">
    {!! Form::label('municipio_id', 'Municipio Id:') !!}
    <p>{{ $direccionEntrega->municipio_id }}</p>
</div>

<!-- Referencia Field -->
<div class="col-sm-12">
    {!! Form::label('referencia', 'Referencia:') !!}
    <p>{{ $direccionEntrega->referencia }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $direccionEntrega->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $direccionEntrega->updated_at }}</p>
</div>

