@extends('layouts.base')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
<livewire:styles />
@endsection

@section('container')
<div class="relative w-full h-24 bg-white print:hidden" id="booktrain">
    <div class="fixed z-20 w-full h-24 mx-auto bg-white sm:px-4 xl:px-16">
        <div class="flex items-center justify-between h-full border-b-0 border-gray-300 lg:border-b-2">

            <header-bt

                nombre_usuario=" {{  strtoupper(Auth::user()->nombre_completo) }}"
                tipo="movil"
                @hasrole('Cliente')
                    tipo_usuario="cliente"
                @endhasrole
                @hasrole('Admin|Colegio')
                    tipo_usuario="admin"
                @endhasrole
            ></header-bt>

            @hasrole('Cliente')
                <div class="flex justify-start w-auto py-2">
                    <a href="/inicio">
                        <span class="sr-only">Workflow</span>
                        <img class="w-auto h-10 sm:h-12" src="{{ asset('img/svg/logo_principal.svg') }}" alt="">
                    </a>
                </div>
            @endhasrole

            @hasrole('Admin|Super Admin')
                <div class="flex justify-start w-auto py-2">
                    <a href="/referencias/reporte_referencias">
                        <span class="sr-only">Workflow</span>
                        <img class="w-auto h-10 sm:h-12" src="{{ asset('img/svg/logo_principal.svg') }}" alt="">
                    </a>
                </div>
            @endhasrole

            @hasrole('Colegio')
                <div class="flex justify-start w-auto py-2">
                    <a href="{{ route('colegio.dashboard') }}">
                        <span class="sr-only">Workflow</span>
                        <img class="w-auto h-10 sm:h-12" src="{{ asset('img/svg/logo_principal.svg') }}" alt="">
                    </a>
                </div>
            @endhasrole

            @hasrole('Venta Movil')

                    <nav class="items-center hidden space-x-10 xl:flex 2xl:space-x-32 2xl:ml-40">
                        <a href="/venta_movil">
                            <span class="sr-only">Workflow</span>
                            <img class="w-auto h-10 sm:h-12" src="{{ asset('img/svg/logo_principal.svg') }}" alt="">
                        </a>
                        @if(Auth()->user()->cajas->count() == 0||Auth()->user()->cajas->last()->status=="CERRADA")

                        @else
                        <a href="/venta_movil/pedidos" class="@yield('pedidosSelected') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">PEDIDOS</a>
                        @endif
                        <a href="/venta_movil/movimiento_caja" class="@yield('movimientoCajaSelected') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">
                            @if(Auth()->user()->cajas()->get()->count() == 0||Auth()->user()->cajas()->get()->last()->status=="CERRADA")ABRIR CAJA @else CERRAR CAJA @endif</a>

                        @if(Auth()->user()->cajas()->get()->count() == 0||Auth()->user()->cajas()->get()->last()->status=="CERRADA")

                        @else
                        <a href="/venta_movil" class="@yield('comprarVentaMovil') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">VENDER</a>
                        @endif
                    </nav>
            @endhasrole

            @hasrole('Cliente')
                <nav class="items-center hidden space-x-10 xl:flex 2xl:space-x-32 2xl:ml-40">
                    <a href="/mis-pedidos" class="@yield('misPedidosSelected') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">MIS PEDIDOS</a>
                    <a href="/tickets" class="@yield('soporteSelected') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">SOPORTE</a>
                    <a href="/inicio" class="@yield('inicioSelected') font-15 rob-light w-36 text-center h-24 py-7 flex items-center justify-center text-grayBT hover:text-greenBT">COMPRAR</a>
                </nav>
            @endhasrole

            @hasrole('Colegio')
                <nav class="items-center hidden space-x-10 xl:flex 2xl:space-x-32 2xl:ml-40">
                </nav>
            @endhasrole

            <div class="relative items-center justify-end w-auto xl:flex xl:space-x-10">

                @hasrole('Cliente')
                    <a href="{{ route('carrito') }}" class="relative text-base font-medium h-24 w-16 xl:w-28 hidden xl:flex justify-center items-center text-grayBT hover:text-gray-900 background-blue--bk @yield('carritoSelected')">
                        <img class="w-auto h-8 sm:h-10" src="{{ asset('img/svg/carrito.svg') }}" alt="">
                        @php
                            $pedidos = Auth::user()->pedidos()->whereStatus("CARRITO")->count();
                            if($pedidos != null){
                                $pedidos = Auth::user()->pedidos()->where('status', 'CARRITO')->first()->libros->count();
                            }
                        @endphp
                        @if ($pedidos>0)
                            <div class="absolute top-0 right-0 w-6 h-6 text-sm text-center text-white bg-red-600 rounded-full">{{$pedidos}}</div>
                        @endif
                    </a>
                    <a href="{{ route('carrito') }}" class="flex items-center justify-center w-16 h-24 text-base font-medium xl:w-28 xl:hidden text-grayBT hover:text-gray-900">
                        <img class="w-auto h-8 sm:h-10" src="{{ asset('img/svg/carrito_movil.svg') }}" alt="">
                        @php
                            $pedidos = Auth::user()->pedidos()->whereStatus("CARRITO")->count();
                            if($pedidos != null){
                                $pedidos = Auth::user()->pedidos()->where('status', 'CARRITO')->first()->libros->count();
                            }
                        @endphp
                        @if ($pedidos>0)
                            <div class="absolute top-0 right-0 w-6 h-6 text-sm text-center text-white bg-red-600 rounded-full">{{$pedidos}}</div>
                        @endif
                    </a>
                @endhasrole

                @hasrole('Venta Movil')
                    @if(Auth()->user()->cajas()->get()->count() == 0||Auth()->user()->cajas()->get()->last()->status=="CERRADA")
                    @else
                    <a href="{{ route('carrito') }}" class="relative text-base font-medium h-24 w-16 xl:w-28 hidden xl:flex justify-center items-center text-grayBT hover:text-gray-900 background-blue--bk @yield('carritoSelected')">
                        <img class="w-auto h-8 sm:h-10" src="{{ asset('img/svg/carrito.svg') }}" alt="">
                        @php
                            $pedidos = Auth::user()->pedidos()->whereStatus("CARRITO")->count();
                            if($pedidos != null){
                                $pedidos = Auth::user()->pedidos()->whereStatus('CARRITO')->first()->libros->count();
                            }
                        @endphp
                        @if ($pedidos>0)
                            <div class="absolute top-0 right-0 w-6 h-6 text-sm text-center text-white bg-red-600 rounded-full">{{$pedidos}}</div>
                        @endif
                    </a>
                    <a href="{{ route('carrito') }}" class="flex items-center justify-center w-16 h-24 text-base font-medium xl:w-28 xl:hidden text-grayBT hover:text-gray-900">
                        <img class="w-auto h-8 sm:h-10" src="{{ asset('img/svg/carrito_movil.svg') }}" alt="">
                        @php
                            $pedidos = Auth::user()->pedidos()->whereStatus("CARRITO")->count();
                            if($pedidos != null){
                                $pedidos = Auth::user()->pedidos()->where('status', 'CARRITO')->first()->libros->count();
                            }
                        @endphp
                        @if ($pedidos>0)
                            <div class="absolute top-0 right-0 w-6 h-6 text-sm text-center text-white bg-red-600 rounded-full">{{$pedidos}}</div>
                        @endif
                    </a>
                    @endif
                @endhasrole

                <header-bt
                    nombre_usuario=" {{  strtoupper(ucwords(Auth::user()->name)." ".ucwords(Auth::user()->apellidos)) }}"
                    {{-- nombre_usuario=" {{  strtoupper(empty(Auth::user()->nombre_completo) ? $modelo->nombre_completo : "" ) }}" --}}
                    tipo="desktop"
                    @hasrole('Cliente')
                        tipo_usuario="cliente"
                    @endhasrole
                    @hasrole('Admin|Colegio')
                        tipo_usuario="admin"
                    @endhasrole
                ></header-bt>

            </div>


        </div>
    </div>
</div>

<section id="bt-pb" class="flex flex-row justify-between w-full min-h-custom">
    @hasrole('Admin|Super Admin')
        <div class="relative py-4 pl-4 pr-2 overflow-x-hidden overflow-y-auto contenido_menu xl:pl-16 print:hidden">
            <div class="fixed z-10 p-2 m-auto overflow-x-hidden overflow-y-auto border rounded-lg border-color-gray--BK w-menu max-h-menu">
                @if (auth()->user()->hasPermissionTo('Usuarios'))
                <a href="/admin/usuarios" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('usuariosSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Usuarios</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Pedidos'))
                <a href="{{ route('admin.pedidos') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('referenciasSelected') @if(request()->is('admin/pedidos*')) border-b-green--BK font-bold @endif font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Pedidos</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Pedidos'))
                <a  href="{{ route('admin.facturas') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('facturasSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Facturas</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Libros'))
                <a href="/admin/libros" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('librosSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Libros</div>
                </a>
                @endif
                @if (auth()->user()->hasDirectPermission('Colegios'))
                <a href="{{ route('admin.colegios') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('colegios2Selected') @if(request()->routeIs('admin.colegios*')) border-b-green--BK font-bold @endif  font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Colegios</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Envíos'))
                <a href="/envios_general/tablareporteenvios" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('referencia_envios_generalSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">ENVÍOS</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Entregas'))
                <a href="/tabla_entregas_parciales" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('tabla_entregasParcialesSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Entregas</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Referencias BBVA'))
                <a href="/tabla_referencias" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('tabla_referencia_pedidosEnviadosSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">referencias bbva</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Almacen'))
                <a href="{{ route('admin.almacen') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('menu_almacen_selected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Almacén</div>
                </a>
                @endif

                @if(auth()->user()->hasDirectPermission('Paquetería'))
                <a href="/admin/paqueteria" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('paqueteriaSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Paquetería</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Soporte'))
                <a href="{{ route('ticket.index') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('soporteSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Soporte</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Reportes'))
                <a href="{{ route('reportes') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@if(request()->is('reportes') || request()->is('reportes/*')) border-b-green--BK font-bold @endif font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Reportes</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Impresión de pedidos'))
                <a href="{{ route('private.admin.pedidos.imprimir') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('impresionPedidosSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Impresión de pedidos</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Devoluciones'))
                <a href="{{ route('admin.devoluciones') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@if(request()->is('admin/devoluciones*')) border-b-green--BK font-bold @endif font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Devoluciones</div>
                </a>
                @endif

                @if (auth()->user()->hasDirectPermission('Log'))
                <a href="{{ route('private.admin.log') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@yield('logSelected') font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Log</div>
                </a>
                @endif

                {{-- @if (auth()->user()->hasDirectPermission('Conciliacion'))
                <a href="{{ route('admin.pedidos.conciliacion') }}" class="flex justify-center w-full border-b cursor-pointer text-grayBT hover:text-gray-900">
                    <div class="@if(request()->is('admin.pedidos.conciliacion') || request()->is('admin/pedidos/conciliacion')) border-b-green--BK font-bold @endif font-medium text-base w-3/5 text-center h-14 py-8 flex items-center justify-center uppercase">Conciliación</div>
                </a>
                @endif --}}

            </div>
        </div>
    @endhasrole

    @hasrole('Admin|Super Admin')
    <div class="flex flex-col items-center h-auto py-0 pl-2 pr-4 w-libros print:px-0 min-h-custom xl:pr-16 print:w-full">
    @endhasrole

    @hasrole('Cliente|Colegio')
    <div class="flex flex-col items-center w-full h-auto lg:px-16">

        <div>
            {{-- WA-BUTTON --}}
            <a rel="noreferrer" class="wa-contenedor" href="https://wa.link/0rz6ds" target="_blank">
                <div class="wa-contenedor__icon"></div>
                <div class="wa-contenedor__banner">
                    <span class="wa-contenedor__texto font-rob-regular">Contáctanos</span>
                    <div class="wa-contenedor__barra"></div>
                </div>
            </a>
        </div>
    @endhasrole

        @yield('contenido')
    </div>
</section>

<footer class="relative w-full bg-white footer sm:px-4 xl:px-16 print:hidden">
    <div class="flex justify-center w-full border-t-2 border-gray-300">
        <div class="flex flex-col items-center w-full py-2 md:flex-row justify-evenly">
            <p class="text-xs leading-7 text-center md:leading-4 text-grayBT font-rob-light">
                <a href="/aviso-de-privacidad" target="_blank">Aviso de privacidad</a> / <a href="/politicas" target="_blank">Políticas de compra, entrega y devolución</a>
            </p>
            <p class="px-3 text-xs leading-7 md:leading-4 text-grayBT font-rob-light">
                © 2021 PROVESA EDICIONES S.A. DE C.V. Av. Campo Militar No. 25 Int. A2, CP. 76137, Querétaro, Querétaro
            </p>
            <p class="px-3 text-xs leading-7 md:leading-4 text-grayBT font-rob-light">
                Desarrollado por <a href="https://inkwonders.com/" target="_blank"><span class="font-bold text-grayBT">INK WONDERS</span></a>
            </p>
        </div>
    </div>
</footer>

@endsection

@push('script_stack')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <livewire:scripts />

    @yield('js-interno')

    <script>
        Livewire.on('swalAlert', (msg, callbackID) => {
            Swal.fire(msg)
            .then(result => {
                Livewire.emit('onSwalResult', result, callbackID)
            })
        })

        Livewire.on('swalShowLoading', (msg, callbackID) => {
            Swal.showLoading()
        })

        Livewire.on('swalClose', (msg, callbackID) => {
            Swal.close()
        })

        // Start of LiveChat (www.livechatinc.com) code
    //     window.__lc = window.__lc || {};
    //     window.__lc.license = 12812103;
    //     ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
    </script>
@endpush
