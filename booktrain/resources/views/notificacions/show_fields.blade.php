<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $notificacion->id }}</p>
</div>

<!-- Colegio Id Field -->
<div class="col-sm-12">
    {!! Form::label('colegio_id', 'Colegio Id:') !!}
    <p>{{ $notificacion->colegio_id }}</p>
</div>

<!-- Titulo Field -->
<div class="col-sm-12">
    {!! Form::label('titulo', 'Titulo:') !!}
    <p>{{ $notificacion->titulo }}</p>
</div>

<!-- Aviso Field -->
<div class="col-sm-12">
    {!! Form::label('aviso', 'Aviso:') !!}
    <p>{{ $notificacion->aviso }}</p>
</div>

<!-- Activo Field -->
<div class="col-sm-12">
    {!! Form::label('activo', 'Activo:') !!}
    <p>{{ $notificacion->activo }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $notificacion->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $notificacion->updated_at }}</p>
</div>

