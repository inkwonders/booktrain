
<h1> Lista de usuarios </h1>

{{-- @hasrole nos sirve para saber si el usuario logueado tiene un determinado rol, se puede usar para ocultar paginas o secciones o en
su defecto reedireccionar para que el usuario que no tenga ese permiso no pueda acceder a esta vista --}}
@hasrole('Admin')
<h1> Soy un administrador </h1>
    {{-- La directiva @can se refiere a si un usuario puede tener acceso, por ejemplo, este blade llamado admin.home esta descrito en el seeder RoleSeeder
    y llena la tabla permissions, el id 1 con el name admin.home está en la tabla permisions y en la tabla role_has_permissions esta descrito el id
    del rol con el id del permiso relacionados, de esta forma sabemos si tiene o no permisos para entrar a distintas vistas de blade --}}
        @can('admin.home')
        <a href="{{'home'}}" class ="block px-4 py-2 text-sm text-gray-700"> Puede entrar al admin.home</a>
        @endcan
@else
<h1>No soy administrador </h1>
        @can('admin.home')
        <a href="{{'home'}}" class ="block px-4 py-2 text-sm text-gray-700"> Puede entrar a admin.home</a>
        @endcan
 @endhasrole
