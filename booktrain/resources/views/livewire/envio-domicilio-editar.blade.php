<div class="w-full flex flex-col relative h-full px-12 py-6">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-green--bk font-bold text-xl">EDITAR ENVIO A DOMICILIO | {{$colegio->nombre}}</h2>
    </div>
    <div class="relative w-full h-full flex flex-row">
        <div class="w-1/2 relative justify-center items-center flex">
            <img class="relative w-1/2" src="/assets/img/svg/colegios/metodos_envio.svg" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion"></div>
        <div class="w-1/2 h-full relative flex flex-col items-center justify-center">
            <form wire:submit.prevent="save" class="relative w-1/2 flex flex-col items-center justify-center">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">COSTO DE ENVIO</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none focus:outline-none" type="text" id="calle" wire:model="costo_envio">
                <button class="relative background-green--bk text-white h-12 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit">Guardar</button>
            </form>
        </div>
    </div>
    <div class="w-full relative flex justify-center items-center my-6">
        <a href="/admin/colegios_envio/{{$colegio->id}}" class="w-1/6 p-3 flex justify-center items-center relative bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</a>
    </div>
</div>
