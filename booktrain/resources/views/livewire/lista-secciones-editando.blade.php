<div class="w-full flex flex-col relative h-auto border rounded-md border-color-gray--BK">
    <div class="flex flex-col p-10 relative w-full h-auto min-h-interno">

    <div>
        @if (session()->has('message'))
            <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
                <div class="bg-white rounded-lg w-1/3">
                    <div class="flex flex-col items-start p-4">
                        <div class="flex items-center w-full justify-center">
                            <div class="text-gray-900 font-medium text-lg pb-3">
                                <h2 class="rob-bold color-blue--bk text-2xl">{{ session('message') }}</h2>
                            </div>
                        </div>
                        <div class="w-full">

                            <div class="flex justify-around w-full pt-5">
                                @if (!$ocultar_botones)
                                <div wire:click.prevent="delete()" class="w-1/4 background-red--bk text-white rounded-md uppercase font-rob-bold cursor-pointer flex justify-center items-center hover:bg-white hover:border-red-500 hover:text-redBT border border-solid">
                                    Eliminar
                                </div>
                                @endif
                                <a href="/admin/colegios_editando/secciones/{{$colegio->id}}" class="w-1/4 roboto-bold py-2 px-4 rounded text-grayBT hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm border border-solid cursor-pointer flex justify-center items-center">
                                    @if (!$ocultar_botones) Cancelar @else Aceptar @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>



    <div class="w-full flex flex-row justify-between mb-14">
        <span class="inline-block color-green--bk font-rob-bold text-xl uppercase">SECCIONES ACADÉMICAS | {{$colegio->nombre}}</span>
        <a href="{{ '/admin/colegios_editando/nueva/'.$colegio->id }}" class="background-blue--bk text-white font-rob-bold w-1/6 p-2.5 flex justify-center items-center rounded-md cursor-pointer">+ NUEVA SECCIÓN</a>
    </div>
    <table class="border-0 rounded-md w-full mt-3">
        <thead class="border-t border-b border-color-gray--BK font-rob-regular text-grayBT">
            <th class="text-left">
                <span class="py-2 inline-block">Nombre</span>
            </th>

            <th colspan="{{ $max_cantidad_niveles }}"><span class="py-2 inline-block">Grados</span></th>
            <th>
                <span class="py-2 inline-block">Activo</span>
            </th>
            <th>
                <span>Editar</span>
            </th>
            <th>
                <span>Eliminar</span>
            </th>
        </thead>
        <tbody class="w-full text-grayBT text-sm">
        @foreach ($secciones->reverse() as $key => $seccion)
            <tr class="border-0 rounded-md border-color-gray--BK h-16 {{ $key%2 == 0? '' : 'bg-gris-tabla' }}" wire:dblclick="$emit('editSeccion', {{ $seccion->id }})">
                <td>{{ $seccion->nombre }}</td>

                @for ($id_nivel = 0; $id_nivel < $max_cantidad_niveles; $id_nivel++)
                    @if(isset($seccion->niveles[$id_nivel]))
                        <td class="text-center">
                            <span class="px-1">{{ $seccion->niveles[$id_nivel]->nombre }}</span>
                        </td>
                    @else
                        <td></td>
                    @endif
                @endfor
                <td class="text-center">{{ $seccion->activo == '1'? 'Activo' : 'Inactivo' }}</td>
                <td class="text-center">
                        <a class="text-purple-500 inline-block" href="/admin/editar_seccion/{{$colegio->id}}/{{$seccion->id}}/"><img class="w-6" src="{{ asset('img/svg/editar_seccion.svg') }}" alt="Editar"></a>
                </td>
                <td class="text-center">
                        <img class="w-6 inline cursor-pointer" src="{{ asset('img/svg/eliminar_seccion.svg') }}" alt="Eliminar" wire:click="beforeDelete({{ $seccion->id }})">
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="w-full mt-14 relative flex justify-center items-center">
        <a href="{{ route('admin.colegios') }}" class="w-1/6 p-3 relative flex justify-center items-center bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</a>
    </div>
</div>
</div>
