
@if (session()->has('message'))
    <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
        <div class="bg-white rounded-lg w-1/3">
            <div class="flex flex-col items-start p-4">
                <div class="flex items-center w-full justify-center">
                    <div class="text-gray-900 font-medium text-lg pb-3">
                        <h2 class="rob-bold color-blue--bk text-2xl">{{ session('message') }}</h2>
                    </div>
                </div>
                <div class="w-full">
                    <div class="flex justify-around w-full pt-5">
                        <div wire:click.prevent="delete({{$notificacion_elegida}})" class="w-1/4 background-red--bk text-white rounded-md uppercase font-rob-bold cursor-pointer flex justify-center items-center hover:bg-white hover:border-red-500 hover:text-redBT border border-solid">
                            Eliminar
                        </div>
                        <div wire:click="cancelar()" class="w-1/4 roboto-bold py-2 px-4 rounded text-grayBT hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm border border-solid cursor-pointer flex justify-center items-center">
                            Cancelar
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<x-livewire-tables::table.cell>
    <p>{{ $row->titulo }}</p>
</x-livewire-tables::table.cell><x-livewire-tables::table.cell>
    <div class="w-72 whitespace-normal">{{ $row->aviso }}</div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<label class='switch'>
    <input id='switch_activo' type='checkbox' @if ($row->activo == 1) checked @endif wire:click='toggleActivo({{ $row->id }})'>
    <span class='slider'></span>
</label>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <a href='/admin/avisos_colegio/editar/{{$colegio->id}}/{{$row->id}}'><img src="/assets/img/svg/colegios/editar.svg" alt="" class="w-6"></a>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <img wire:click="beforeDelete({{$row->id}})" src="/assets/img/svg/colegios/eliminar.svg" alt="" class="w-5 cursor-pointer">
</x-livewire-tables::table.cell>
