<div class="flex flex-col items-center p-4 relative w-full h-full justify-center">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-red--bk font-bold text-xl">PEDIDOS</h2>
    </div>
    <table id="lista-pedidos" class="stripe tabla_quejas" style="width:100%">
        <thead>
            <td>Serie</td>
            <td>Folio</td>
            <td>Status</td>
            <td>Colegio</td>
            <td>Nombre</td>
            <td>Entrega</td>
            <td>Facturación</td>
            <td>Método pago</td>
            <td>Forma pago</td>
            <td>Envio</td>
            <td>Comisión</td>
            <td>Total</td>
            <td>Pagos</td>
            <td>Fecha</td>
            <td>Errores</td>
            <td>Opciones</td>
        </thead>
        <tbody>
        @foreach ($pedidos->reverse() as $pedido)
        <tr>
            <td title="{{ $pedido->id }}">{{ $pedido->serie }}</td>
            <td>{{ $pedido->folio }}</td>
            <td>{{ $pedido->status }}</td>
            <td>{{ $pedido->colegio->nombre }}</td>
            <td>{{ $pedido->user->nombre_completo }}</td>
            <td>{{ $pedido->descripcion_tipo_entrega }}</td>
            <td>{{ $pedido->factura?'Si': 'No' }}</td>
            <td>{{ $pedido->metodo_pago }}</td>
            <td>{{ $pedido->forma_pago }}</td>
            <td>@if($pedido->tipo_entrega == 2){{ $pedido->envio->status }}@else N/A @endif</td>
            <td title="Comisión: ${{ number_format($pedido->comision, 2) }}">${{ number_format($pedido->comision, 2) }}</td>
            <td title="Total: {{ $pedido->total }}">{{ $pedido->total }}</td>
            <td title="{{ $pedido->pagos->pluck('status')->join(', ', ' y ') }}">{{ $pedido->pagos->count() }}</td>
            <td title="{{ $pedido->created_at }}">{{ $pedido->created_at->format('d-m-Y') }}</td>
            <td title="{{ $pedido->tiene_errores->pluck('descripcion')->join(',', ' y ') }}">{{ $pedido->tiene_errores->count() }}</td>
            <td>
                @if($pedido->status == 'CANCELADO')
                <button class="border text-white h-auto w-28 m-4 py-1.5 px-2 bg-redDark rounded-md font-rob-bold uppercase" wire:click="procesar({{ $pedido->id }});">ACTIVAR</button>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
