{{-- <input type="text" wire:model="filters.search"> --}}

<x-livewire-tables::table.cell>
    <p>{{ $row->name }} {{$row->apellidos}}</p>
</x-livewire-tables::table.cell><x-livewire-tables::table.cell>
    <p>{{ $row->email }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<label class='switch'>
    <input id='switch_activo' type='checkbox' @if($row->activo) checked @endif wire:click='activo( {{ $row->id }})'>
    <span class='slider'></span>
</label>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p>{{ $row->roles_name }}</p>
{{-- @foreach ($row->getRoleNames() as $rol)
{{ $rol }}
@if(in_array($rol, ['Colegio', 'Venta Movil']))
    @if ($row->permissions()->first())
    [{{ $row->permissions()->first()->name }}]
    @else
    [No asignado]
    @endif
@endif
<br>
@endforeach --}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if ($row->email_verified_at == null)
    <button class='border text-white h-auto w-28 m-4 py-1.5 px-2 bg-redDark rounded-md font-rob-bold uppercase' wire:click='verificar({{$row->id}})'>Confirmar</button>
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<a href="{{ route('admin.usuarios.editar', [ $row->id ]) }}"><div class='background-green--bk text-white h-9 w-28 m-4 rounded-md uppercase font-rob-bold justify-center flex items-center'>Editar</div></a>
</x-livewire-tables::table.cell>

