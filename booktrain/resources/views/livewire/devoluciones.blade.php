<section class="flex flex-col items-center justify-center w-full px-2 py-4 min-h-custom">
    <div class="relative flex flex-col justify-start w-full h-auto p-10 border rounded-md min-h-interno border-color-gray--BK">
        <div class="relative flex flex-col justify-between w-full mb-1">
            <h2 class="text-xl font-bold color-red--bk">DEVOLUCIONES</h2>
        </div>
        <div class="data-table-diseño">
            <livewire:datatable-devoluciones searchable="folio, nombre_contacto, apellidos_contacto, email" exportable />
        </div>
    </div>
 </section>
