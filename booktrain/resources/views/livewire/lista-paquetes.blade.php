<div class="pt-5">
    <span class="inline-block">Lista de paquetes</span>
    <table class="border border-gray-200 w-full mt-3">
        <thead>
            <td></td>
            <td>Nombre</td>
            <td>Activo</td>
            <td>Libros</td>
            <td></td>
        </thead>
        <tbody>
        @foreach ($paquetes->reverse() as $paquete)
        <tr class="border border-gray-300">
            <td><button class="text-purple-500" wire:click="$emit('editPaquete', {{ $paquete->id }})">Editar</button></td>
            <td>{{ $paquete->nombre }}</td>
            <td>{{ $paquete->activo }}</td>
            <td>{{ $paquete->libros()->wherePivot('colegio_id', $paquete->nivel->seccion->colegio->id)->count() }}</td>
            <td>
                <button class="text-red-500" wire:click="delete({{ $paquete->id }})">Eliminar</button>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
