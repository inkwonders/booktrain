<div class="w-full relative h-full">
    <div class="relative flex justify-between w-full py-4 mb-4">
        <h2 class="color-green--bk font-rob-bold uppercase text-xl">NUEVO MOVIMIENTO</h2>
        {{-- @if(!empty($libros_movimiento))
            <div class="h-10 w-80 overflow-x-hidden rounded-md border border-grayBT text-grayBT relative">
                <img class="absolute left-2.5 bottom-0 top-0 m-auto h-5 w-5 pointer-events-none" src="{{ asset('img/svg/lupa_buscar_colegio_gris.svg') }}" alt="">
                <input class="w-full h-full px-2 pl-10" type="text" wire:model="filtro" placeholder="Buscar">
            </div>
        @endif --}}

        <a class="h-10 w-60 overflow-x-hidden right-0 top-0 bottom-0 m-auto mr-0 bg-greenBT rounded cursor-pointer flex justify-center items-center relative text-white" href="{{ route('admin.almacen') }}">VER ALMACÉN</a>
    </div>
    <div class="w-full min-h-secciones h-secciones">
        @if (session()->has('message'))
        <div class="flex items-center justify-center fixed left-0 bottom-0 w-full h-full bg-gray-600 bg-opacity-50 z-30">
            <div class="bg-white rounded-lg w-1/3">
                <div class="flex flex-col items-start p-4">
                    <div class="flex items-center w-full justify-center">
                        <div class="text-gray-900 font-medium text-lg pb-3">
                            <h2 class="rob-bold color-blue--bk text-2xl">{{ session('message') }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="w-full h-full flex justify-between">
            <div class="flex flex-col h-full w-1/3 pr-6 2xl:pr-12 items-center justify-center">
                <div class="w-full pb-12">

                    <div class="h-12 w-full relative overflow-hidden border rounded border-gray-300 cursor-pointer">
                        <select id="select_tipo_movimiento" wire:model="select_tipo_movimiento" class="w-full h-full p-2 pr-8 appearance-none cursor-pointer text-grayBT">
                            <option value="0" selected disabled="disabled">Seleccione una operación</option>
                        @foreach ($tipos_movimientos as $tipo_movimiento)
                            @if($tipo_movimiento->privado == false)
                                <option value="{{ $tipo_movimiento->id }}">{{ $tipo_movimiento->descripcion }}</option>
                            @endif
                        @endforeach
                            <option value="99">Traspaso</option>
                        </select>
                        <label for="select_tipo_movimiento" class="select_motivos"></label>
                    </div>
                </div>

                @if($select_tipo_movimiento == 99)
                <div class="w-full relative">
                    <span class="font-bold text-grayBT">COLEGIO ORIGEN</span>
                    <div class="h-12 w-full relative overflow-hidden border rounded border-gray-300 cursor-pointer">
                        <select id="select_colegio_1" wire:model="select_colegio_1" class="w-full h-full p-2 pr-8 appearance-none cursor-pointer text-grayBT">
                            <option>Seleccione un colegio</option>
                        @foreach ($colegios as $colegio)
                            <option value="{{ $colegio->id }}">{{ $colegio->nombre }}</option>
                        @endforeach
                        </select>
                        <label for="select_colegio_1" class="select_motivos"></label>
                    </div>
                </div>
                @endif

                <div class="w-full relative pt-12">
                    @if($select_tipo_movimiento == 99)
                    <span class="font-bold text-grayBT">COLEGIO DESTINO</span> @endif

                    <div class="w-full pr-14 relative">
                        <div class="h-12 w-full relative overflow-hidden border rounded border-gray-300 cursor-pointer">
                            <select id="select_colegio_2" wire:model="select_colegio_2" class="w-full h-full p-2 pr-8 appearance-none cursor-pointer text-grayBT">
                                <option>Seleccione un colegio</option>
                            @foreach ($colegios as $colegio)
                                <option value="{{ $colegio->id }}">{{ $colegio->nombre }}</option>
                            @endforeach
                            </select>
                            <label for="select_colegio_2" class="select_motivos"></label>
                        </div>
                        <button wire:click="filtrarLibros" class="w-12 h-12 absolute right-0 top-0 bottom-0 m-auto bg-greenBT rounded cursor-pointer flex justify-center items-center">
                            <img class="w-6" src="{{ asset('img/svg/lupa_buscar_colegio.svg') }}" alt="">
                        </button>
                    </div>
                </div>
            </div>
            <div class="h-full w-px bg-grayBT"></div>
            <div class="flex flex-col h-full pl-6 2xl:pl-12 w-2/3 items-center justify-center">
                @if(!empty($libros_movimiento))

                <div class="w-full mb-4">
                    <div class="h-10 w-80 overflow-x-hidden rounded-md border border-grayBT text-grayBT relative">
                        <img class="absolute left-2.5 bottom-0 top-0 m-auto h-5 w-5 pointer-events-none" src="{{ asset('img/svg/lupa_buscar_colegio_gris.svg') }}" alt="">
                        <input class="w-full h-full px-2 pl-10" type="text" wire:model="filtro" placeholder="Buscar">
                    </div>
                </div>

                <div class="w-full relative border-t border-grayBT h-tabla-interna overflow-y-auto overflow-x-hidden text-grayBT max-h-full">
                    <table class="w-full text-grayBT">
                        <thead class="border-b border-grayBT">
                            <tr>
                                <th class="px-4 py-3 text-left">ISBN</th>
                                <th class="px-4 py-3 text-left">Título</th>
                                <th class="px-4 py-3 text-center">Stock actual</th>
                                <th class="px-4 py-3 text-center">Seleccionar</th>
                                <th class="px-4 py-3 text-center">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($libros_movimiento as $libro)
                            @if($filtro == '' || Str::contains($libro['titulo'], $filtro) || Str::contains($libro['isbn'], $filtro))
                            <tr class=" {{ $loop->odd? 'bg-gray-100' : '' }}">
                                <td class="px-4 py-2 text-left">{{ $libro['isbn'] }}</td>
                                <td class="px-4 py-2 text-left">{{ $libro['titulo'] }}</td>
                                <td class="px-4 py-2 text-center">{{ $libro['stock'] }}</td>
                                <td class="px-4 py-2 text-center">
                                    <input class="form-checkbox rounded text-blueBT" type="checkbox" wire:model="libros_movimiento.{{ $loop->index }}.seleccionado">
                                </td>
                                <td class="px-4 py-2 text-center">
                                    <button wire:click="disminuirCantidadMovimiento({{ $loop->index }})">-</button>
                                    <input class="border border-gray-400 w-10 text-center rounded" wire:model="libros_movimiento.{{ $loop->index }}.cantidad">
                                    <button wire:click="aumentarCantidadMovimiento({{ $loop->index }})">+</button>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <button wire:click="$set('modal_confirmar_movimiento', true)" class="bg-white text-greenBT font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer mt-4 mb-2 border border-greenBT hover:bg-greenBT hover:text-white hover:border-transparent">VISTA PREVIA</button>                @else
                <img class="w-96" src="{{ asset('img/svg/libros_genera_movimiento.svg') }}" alt=""> @endif
            </div>
        </div>
    </div>

    @if($modal_confirmar_movimiento)
    <div class="fixed top-0 bottom-0 left-0 right-0 flex items-center justify-center z-20">
        <div wire:click="$set('modal_confirmar_movimiento', false)" class="absolute top-0 bottom-0 left-0 right-0 -z-1 bg-gray-500 opacity-50"></div>
        <div class="relative w-11/12 max-w-5xl h-5/6 max-h-secciones bg-white rounded-lg border border-grayBT text-grayBT p-6 pt-8 flex flex-col justify-between">
            <div class="h-full w-full overflow-x-hidden overflow-y-auto">
                <div class="w-full block">
                    @if($select_tipo_movimiento == 99)
                        <span class="inline-block mb-4 w-1/3"><b>TIPO DE MOVIMIENTO:</b> TRASPASO</span>
                        <span class="inline-block mb-4 w-1/3"><b>COLEGIO ORIGEN:</b> {{ $colegios->firstWhere('id', $select_colegio_1)->nombre }}</span>
                        <span class="inline-block mb-4 w-1/3"><b>CANTIDAD DE OPERACIONES:</b> {{ collect($libros_movimiento)->where('seleccionado', 1)->count() }}</span>
                        <span class="inline-block mb-4 w-auto"><b>COLEGIO DESTINO:</b> {{ $colegios->firstWhere('id', $select_colegio_2)->nombre }}</span>
                    @else
                        <span class="inline-block mb-4 w-1/3"><b>TIPO DE MOVIMIENTO:</b> {{ $tipos_movimientos->firstWhere('id', $select_tipo_movimiento)->naturaleza }}</span>
                        <span class="inline-block mb-4 w-1/3"><b>TIPO DE ENTRADA:</b> {{ $tipos_movimientos->firstWhere('id', $select_tipo_movimiento)->descripcion }}</span>
                        <span class="inline-block mb-4 w-1/3"><b>CANTIDAD DE OPERACIONES:</b> {{ collect($libros_movimiento)->where('seleccionado', 1)->count() }}</span>
                        <span class="inline-block mb-4 w-auto"><b>COLEGIO:</b> {{ $colegios->firstWhere('id', $select_colegio_2)->nombre }}</span>
                    @endif
                </div>
                <div class="w-full relative border-b border-t border-grayBT h-full max-h-custom-modal overflow-y-auto overflow-x-hidden">
                    <table class="w-full text-grayDarkBT">
                        <thead class="border-b border-grayBT">
                            <tr>
                                <th class="px-6 py-3 text-left">ISBN</th>
                                <th class="px-6 py-3 text-left">Título</th>
                                <th class="px-6 py-3 text-center">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $class_row = false;
                            @endphp
                            @foreach ($libros_movimiento as $libro)
                                @if($libro['seleccionado'])
                                    @php
                                        $class_row = !$class_row;
                                    @endphp
                                    <tr class=" {{ $class_row? 'bg-gray-100' : '' }}">
                                        <td class="px-6 py-2 text-left">{{ $libro['isbn'] }}</td>
                                        <td class="px-6 py-2 text-left">{{ $libro['titulo'] }}</td>
                                        <td class="px-6 py-2 text-center">{{ $libro['cantidad'] }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="py-5 h-32">
                    <textarea class="px-2 py-1 w-full relative border rounded-md border-grayBT h-full h-30 resize-none outline-none overflow-y-auto overflow-x-hidden" wire:model.lazy="comentarios" placeholder="Comentarios sobre la transacción"></textarea>
                </div>
                <div class="w-full h-28 flex flex-col justify-between items-center pb-3">
                    <button wire:click="confirmarMovimiento" class="bg-white text-greenBT font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer border border-greenBT hover:bg-greenBT hover:text-white hover:border-transparent">CONFIRMAR</button>
                    <button wire:click="$set('modal_confirmar_movimiento', false)" class="bg-white text-grayBT text-white font-rob-bold w-60 h-11 flex justify-center items-center rounded-md cursor-pointer border border-grayBT hover:bg-grayBT hover:text-white">CANCELAR</button>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
