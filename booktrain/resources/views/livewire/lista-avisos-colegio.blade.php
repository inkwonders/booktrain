<div>
    <span>Lista de avisos del colegio</span>
    @if($avisos->isEmpty())
    <h4><b>No hay avisos activos</b></h4>
    @else
    <table class="w-full">
        <thead>
            <td></td>
            <td>Titulo</td>
            <td>Aviso</td>
            <td>Activo</td>
            <td></td>
        </thead>
        <tbody>
            @foreach ($avisos as $aviso)
            <tr class="p-1 border border-gray-100">
                <td>
                    <button wire:click="$emit('editAvisoColegio', {{ $aviso->id }})" class="text-purple-500">Editar</button>
                </td>
                <td class="p-1 border border-gray-100">{{ $aviso->titulo }}</td>
                <td class="p-1 border border-gray-100">{{ $aviso->aviso }}</td>
                <td class="p-1 border border-gray-100">{{ $aviso->activo }}</td>
                <td>
                    <button wire:click="delete({{ $aviso->id }})" class="text-red-500">Eliminar</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
