<div class="relative flex flex-col w-full h-auto p-10 min-h-interno">

    @include('errors.sessionMessages')

    <div class="relative flex justify-between w-full pb-4 mb-5">
        <h2 class="text-2xl font-bold color-green--bk">COLEGIOS</h2>
        <a href="{{ route('admin.colegios.create') }}" class="background-blue--bk text-white font-rob-bold w-1/6 p-2.5 flex justify-center items-center rounded-md cursor-pointer">+ NUEVO COLEGIO</a>
    </div>
    <div class="block w-full h-auto mt-4 overflow-x-auto scrool_azul">
        <div class="flex flex-row w-auto h-auto min-w-full">
            <div class="table-responsive">
                <table id="example2" class="stripe tabla_quejas">
                    <thead class="text-center font-rob-bold">
                        <td>Nombre</td>
                        <td>Código</td>
                        <td>Activo o Inactivo</td>
                        <td>Secciones académicas</td>
                        <td>Avisos</td>
                        <td>Métodos de envío/Recolección</td>
                        <td>Configuración de pago</td>
                        <td>Libros</td>
                        <td>Editar</td>
                        <td>Eliminar</td>
                    </thead>
                    <tbody>
                        @foreach ($colegios->reverse() as $colegio)
                            <tr class="text-center">
                                <td>{{ $colegio->nombre }}</td>
                                <td>{{ $colegio->codigo }}</td>
                                <td>{{ $colegio->activo ? 'ACTIVO' : 'INACTIVO' }}</td>
                                <td align="center">
                                    @if ($colegio->secciones->count() > 0)
                                        <a href="/admin/colegios_editando/secciones/{{ $colegio->id }}" class="relative inline-block w-6">
                                            <img src="/assets/img/svg/colegios/documento_rojo.svg" alt="" class="w-full">
                                            <div class="absolute top-0 flex items-center justify-center w-4 h-4 text-xs text-white bg-red-500 rounded-full left-3">{{$colegio->secciones->count()}}</div>
                                        </a>
                                    @else
                                        <a href="/admin/colegios_editando/nueva/{{ $colegio->id }}" class="relative">
                                            <img src="/assets/img/svg/colegios/documento_verde.svg" alt="" class="w-6">
                                        </a>
                                    @endif
                                </td>
                                <td align="center">
                                    @if ($colegio->notificaciones->count() > 0)
                                        <a href="avisos_colegio/{{$colegio->id}}" class="relative inline-block w-6">
                                            <img src="/assets/img/svg/colegios/campana_roja.svg" alt="" class="w-full">
                                            <div class="absolute top-0 flex items-center justify-center w-4 h-4 text-xs text-white bg-red-500 rounded-full left-3">{{$colegio->notificaciones->count()}}</div>
                                        </a>
                                    @else
                                        <a href="avisos_colegio/nuevo/{{$colegio->id}}">
                                            <img src="/assets/img/svg/colegios/campana_verde.svg" alt="" class="w-6">
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="/admin/colegios_envio/{{$colegio->id}}" class="relative inline-block w-8">
                                        <img src="/assets/img/svg/colegios/icon_metodos_envio.svg" alt="" class="w-full">
                                        @if ($this->metodos_envio_activos($colegio)==0)
                                            <div class="absolute top-0 left-0 flex items-center justify-center w-4 h-4 text-white rounded-full bg-greenBT">
                                                <span>+</span>
                                            </div>
                                        @else
                                            <div class="absolute top-0 left-0 flex items-center justify-center w-4 h-4 text-xs text-white bg-red-500 rounded-full">
                                                {{$this->metodos_envio_activos($colegio)}}
                                            </div>
                                        @endif

                                    </a>

                                </td>
                                <td><a href="{{ route('admin.colegios.metodos_pago', $colegio->id) }}">{{ $colegio->formasPago()->where('activo', 1)->count() > 0 ? 'SI' : 'NO' }}</a></td>
                                <td>
                                    @if ($colegio->libros()->wherePivot('paquete_id', null)->count() > 0)
                                        <a href="{{ route('admin.colegios.libros', $colegio->id) }}">{{$colegio->libros()->wherePivot('paquete_id', '!=' ,null)->count()}}</a>
                                    @else
                                        {{-- <a id="{{ $colegio->id }}" href="/admin/listado_libros/{{$colegio->id}}/"><span class="text-sm font-rob-bold">+ AGREGAR</span></a> --}}
                                        <a id="{{ $colegio->id }}" href="{{ route('admin.colegios.libros', $colegio->id) }}"><span class="text-sm font-rob-bold">+ AGREGAR</span></a>
                                    @endif
                                </td>
                                <td align="center"><a href="{{ route('admin.colegios.edit', $colegio->id) }}"><img src="{{ asset('img/svg/colegios/editar.svg') }}" alt="" class="w-6"></a></td>
                                <td align="center"><img src="{{ asset('img/svg/colegios/eliminar.svg') }}" alt="" class="w-6 cursor-pointer" wire:click="delete({{ $colegio->id }})"></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
