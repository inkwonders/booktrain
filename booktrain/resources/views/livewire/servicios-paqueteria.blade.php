<div class="relative flex flex-col justify-between w-full p-10 mx-2 border rounded-md h-interno border-color-gray--BK">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="text-xl font-bold color-green--bk">PAQUETERÍA</h2>
    </div>
    <div class="relative flex flex-row justify-between w-full h-secciones">
        <div class="relative flex items-start justify-center w-2/3 h-full pr-8 2xl:w-1/2 max-h-paqueteria">
            <div class="w-full h-auto max-h-full overflow-x-hidden overflow-y-auto border-b border-grayBT">
                <table class="w-full text-grayBT">
                    <thead>
                        <tr class="border-t border-b h-14 border-grayBT">
                            <th class="px-3 text-left">Nombre</th>
                            <th class="px-3 text-left">Url</th>
                            <th class="px-3 text-center">Envíos</th>
                            <th class="px-3 text-center">Activo</th>
                            <th class="px-3 text-center"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($servicios_paqueteria as $paqueteria)
                        <tr class="h-10 {{ $loop->odd? 'bg-gray-100' : '' }}">
                            <td class="px-3 py-3 text-left">{{ $paqueteria->nombre }}</td>
                            <td class="px-3 py-3 text-left">
                                <span class="inline-block max-w-full break-all whitespace-normal">
                                    {{ $paqueteria->url }}
                                </span>
                            </td>
                            <td class="px-3 py-3 text-center">{{ $paqueteria->envios->count() }}</td>
                            <td class="px-3 py-3 text-center">
                                <img class="inline-block w-6" src="{{ $paqueteria->activo=='1'? asset('img/svg/eye.svg') : asset('img/svg/eye-blocked.svg') }}" alt="">
                            </td>
                            <td class="px-3 py-3 text-center">
                                <button wire:click="$emit('editarServicioPaqueteria', {{ $paqueteria->id }})">Editar</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="block h-full border-r border-color-gray--BK separacion"></div>
        <div class="relative flex flex-col items-center justify-center h-full pl-8 w-1/1 2xl:w-1/2">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center w-11/12 2xl:w-1/2">
                @include('errors.sessionMessages')
                <label class="self-start mb-6 text-lg uppercase color-blue--bk font-rob-bold" for="">{{ $editando?'Editando':'Nueva' }} paquetería</label>
                <label class="relative self-start color-gray--bk font-rob-light" for="nombre">NOMBRE</label>
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" type="text" id="nombre" wire:model="nueva_paqueteria.nombre">
                <label class="relative self-start color-gray--bk font-rob-light" for="url">URL</label>
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" type="text" id="url" wire:model="nueva_paqueteria.url">
                <label class="flex self-start my-4 ml-4">
                        <input id="one" class="custom-check--bk" type="checkbox" wire:model="nueva_paqueteria.activo">
                        <label class="flex self-start custom-check-label--bk" for="one">
                            <span class="custom-check-span--bk"></span>
                        </label>
                <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">Activo</span>
                </label>
                @foreach ($errors->all() as $error)
                <span class="text-center color-red--bk font-rob-bold">{{ $error }}</span> @endforeach
                <button class="relative w-1/2 h-12 m-4 text-white uppercase rounded-md background-green--bk font-rob-bold" type="submit">Guardar</button>
            </form>

        </div>
    </div>
</div>
