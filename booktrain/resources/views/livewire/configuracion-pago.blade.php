<div class="relative flex flex-col items-center justify-start w-full h-full p-10">
    <div class="relative flex justify-between w-full pb-4 mb-5">
        <h2 class="text-xl uppercase color-green--bk font-rob-bold">Configuración de pago | {{ $colegio->nombre }}</h2>
        <a href="{{ route('admin.metodos_pago') }}" class="px-4 py-2 border border-gray-300 rounded-md hover:bg-greenBT hover:text-white">Editar métodos de pago</a>
    </div>
    <div class="relative flex flex-col w-full h-auto">
        <table class="w-full mt-3 border-0 rounded-md">
            <thead class="border-t border-b border-color-gray--BK font-rob-regular text-grayBT">
                <tr>
                    <th class="py-2">Método de pago</th>
                    <th class="py-2">Comision %</th>
                    <th class="py-2">Monto mínimo de operacion</th>
                    <th class="py-2">Proveedor</th>
                    <th class="py-2">Activo o Inactivo</th>
                    <th class="py-2">Editar</th>
                </tr>
            </thead>
            <tbody class="font-rob-light">
            @foreach ($this->metodos_pago as $id_metodo_pago => $metodo_pago)
                @foreach ($metodo_pago['formas_pago'] as $id_forma_pago => $forma_pago)
                <tr class="@if($forma_pago['index'] % 2 == 0) bg-gray-50 @endif mb-2 h-10">
                    <td>{{ $metodo_pago['nombre'] }} - {{ $forma_pago['nombre'] }}</td>
                    <td class="text-center">{{ number_format($forma_pago['comision'], 1) }} %</td>
                    <td class="text-center">${{ number_format($forma_pago['minimo'], 2) }}</td>
                    <td class="text-center">{{ $metodo_pago['proveedor'] }} | {{ $metodo_pago['contexto'] }}</td>
                    <td class="text-center">
                        <label class="switch">
                            <input type="checkbox" wire:model="metodos_pago.{{ $id_metodo_pago }}.formas_pago.{{ $id_forma_pago }}.activo" @if($forma_pago['activo'] == 1) checked @endif>
                            <span class="slider"></span>
                        </label>
                    </td>
                    <td align="center">
                        <a href="{{ route('admin.colegios.configuracion.pagos.forma', [$colegio->id, $metodo_pago['id'], $forma_pago['id']]) }}">
                            <img src="{{ asset('img/svg/colegios/editar.svg') }}" alt="" class="relative w-6 cursor-pointer">
                        </a>
                    </td>
                </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
        <div class="relative flex items-center justify-center w-full mt-14">
            <a href="{{ route('admin.colegios') }}" class="relative flex items-center justify-center w-1/6 p-3 uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk"><div>Regresar</div></a>
        </div>
    </div>
</div>
