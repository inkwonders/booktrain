<div class="relative flex flex-col items-center w-full h-auto p-10">

    <div>
        @if (session()->has('message'))
            <div class="fixed bottom-0 left-0 z-30 flex items-center justify-center w-full h-full bg-gray-600 bg-opacity-50">
                <div class="w-1/3 bg-white rounded-lg">
                    <div class="flex flex-col items-start p-4">
                        <div class="flex items-center justify-center w-full">
                            <div class="pb-3 text-lg font-medium text-gray-900">
                                <h2 class="text-2xl rob-bold color-blue--bk">{{ session('message') }}</h2>
                            </div>
                        </div>
                        <div class="w-full">
                            <div class="flex justify-around w-full pt-5">
                                <div wire:click.prevent="delete()" class="flex items-center justify-center w-1/4 text-white uppercase border border-solid rounded-md cursor-pointer background-red--bk font-rob-bold hover:bg-white hover:border-red-500 hover:text-redBT">
                                    Eliminar
                                </div>
                                <div wire:click="cancelar()" class="flex items-center justify-center w-1/4 px-4 py-2 border border-solid rounded cursor-pointer roboto-bold text-grayBT hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm">
                                    Cancelar
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="relative flex justify-between w-full mb-14">
        <h2 class="text-xl font-bold color-green--bk">LIBROS | {{$colegio->nombre}}</h2>
        <a href="{{ route('admin.colegios.libros.asignar', $colegio->id) }}" class="flex items-center justify-center p-2 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold">+ ASIGNAR LIBROS</a>
    </div>
        <div class="relative w-full h-auto">
            <table id="example2" class="stripe tabla_quejas" style="width:100%">
                <thead class="text-center">
                    <td>ISBN</td>
                    <td>Título</td>
                    <td>Sección académica</td>
                    <td>Grado</td>
                    <td>Precio</td>
                    <td>Obligatorio</td>
                    <td>Agotado</td>
                    <td>Activo</td>
                    <td>Editar</td>
                    <td>Eliminar</td>
                </thead>
                <tbody>
                    @foreach ($catalogo_libros as $libro)
                    <tr class="text-center">
                        <td>{{$libro->isbn}}</td>
                        <td>{{$libro->nombre}}</td>
                        <td>{{$this->seccion($libro->pivot->paquete_id)->nombre}}</td>
                        <td>{{$this->grado($libro->pivot->paquete_id)->nombre}}</td>
                        <td>${{number_format($libro->pivot->precio, 2, '.', ',')}}</td>
                        <td>{{ $libro->pivot->obligatorio ? 'Si' : 'No' }}</td>
                        <td>{{ $this->libroPivote($libro->id, $colegio->id)->bajo_pedido ? 'DISPONIBLE BAJO PEDIDO' : 'AGOTADO' }}</td>
                        <td>{{ $this->libroColegio($libro->id, $colegio->id, $libro->pivot->paquete_id)->activo ? 'Si' : 'No' }}</td>
                        <td align="center">
                            <a href="{{ route('admin.colegios.libros.editar', [ $colegio->id, $libro->id, $libro->pivot->paquete_id ]) }}">
                                <img src="{{ asset('img/svg/colegios/editar.svg') }}" alt="" class="w-6 cursor-pointer">
                            </a>
                        </td>
                        <td align="center"><img wire:click="beforeDelete({{$libro->id}}, {{$libro->pivot->paquete_id}})" src="{{ asset('img/svg/colegios/eliminar.svg') }}" alt="" class="w-6 cursor-pointer"></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    <div class="relative flex items-center justify-center w-full mt-4"><a href="{{ route('admin.colegios') }}" class="relative flex items-center justify-center w-1/6 p-3 uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk"><div>Regresar</div></a></div>
</div>
