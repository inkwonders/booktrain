<div class="w-full flex flex-col relative h-full p-10">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-green--bk font-bold text-xl">{{ $colegio->exists ? 'EDITAR COLEGIO' : 'NUEVO COLEGIO' }}</h2>
    </div>
    <div class="relative w-full h-full flex flex-row">
        <div class="w-1/2 relative h-full flex justify-center items-center">
            <img src="@if($logo) {{ $logo->temporaryUrl() }} @else {{$colegio->logo}} @endif" alt="" class="relative w-1/2">

            <div class="w-1/6 absolute bottom-3 right-3">
                <label for="file-input" class="relative w-full cursor-pointer">
                    <img src="/assets/img/svg/colegios/subir_logo_colegio.svg" alt="" class="relative w-full">
                </label>
                <input class="mb-4 w-full block rounded my-2 form-input hidden" id="file-input" type="file" wire:model="logo">
            </div>
        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="w-1/2 relative h-full flex justify-center items-center">
            <form wire:submit.prevent="save" class="w-1/2 relative h-full flex-col justify-center items-center flex">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">NOMBRE</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="colegio.nombre">

                <label class="color-gray--bk relative self-start font-rob-light" for="codigo">CODIGO</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="colegio.codigo">

                <label class="color-gray--bk relative self-start font-rob-light" for="codigo">Correo del coordinador del colegio</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="correo_coordinador_colegio">

                <label class="color-gray--bk relative self-start font-rob-light" for="codigo">Correo comercial PROVESA</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="correo_comercial_provesa">

                <label class="color-gray--bk relative self-start font-rob-light" for="codigo">Correo coordinador PROVESA</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="correo_coordinador_provesa">

                <label class="flex self-start my-6">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="colegio.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                </label>
                <button class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit">Guardar</button>
                @include('errors.validation')
            </form>
        </div>
    </div>
    <div class="w-full relative flex justify-center items-center my-6">
        <a href="{{ route('admin.colegios') }}" class="w-1/6 relative flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk p-2 cursor-pointer">REGRESAR</a>
    </div>
</div>
