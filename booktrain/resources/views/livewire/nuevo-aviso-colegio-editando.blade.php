<div class="relative flex flex-col w-full h-full p-10">
    <h2 class="text-xl font-bold color-green--bk">NUEVO AVISO | {{$colegio->nombre}}</h2>
    <div class="relative flex flex-row w-full h-full">
        <div class="relative flex items-center justify-center w-1/2">
            <img src="/assets/img/svg/colegios/campana_amarilla.svg" class="relative w-1/3" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="relative flex items-center justify-center w-1/2">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center justify-center w-1/2">
                <label class="relative self-start uppercase color-gray--bk font-rob-light" for="titulo">Nombre</label>
                <input type="text" class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" id="titulo" wire:model='notificacion.titulo'/>
                <label class="flex self-start my-6">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="notificacion.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">Activo</span>
                </label>
                <label class="relative self-start color-gray--bk font-rob-light" for="mensaje">Mensaje</label>
                <textarea id="mensaje" type="text"  class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none resize-none h-36" wire:model='notificacion.aviso'></textarea>
                <button class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit" wire:click="$emit('refreshList')">Guardar</button>
            </form>
        </div>

    </div>
    <div class="relative flex items-center justify-center w-full mt-14">
        <a href="/admin/avisos_colegio/{{$colegio->id}}" class="relative flex items-center justify-center w-1/6 p-3 uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk"><div>Regresar</div></a>
    </div>
</div>
