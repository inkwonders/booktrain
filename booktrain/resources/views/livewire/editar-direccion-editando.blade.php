<div class="block border border-gray-300 p-2 mb-2">
    @foreach ($errors->toArray() as $rule => $error_descriptions)
        @foreach ($error_descriptions as $error)
            <h5 class="text-red-500">{{ $error }}</h5>
        @endforeach
    @endforeach

    <form wire:submit.prevent="save">
    <span class="inline-block">DIRECCIÓN TIPO {{ $direccion->pivot['tipo'] }}</span>
    <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="direccion.calle" placeholder="Calle">
    <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="direccion.colonia" placeholder="Colonia">
    <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="direccion.no_exterior" placeholder="Número exterior">
    <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="direccion.no_interior" placeholder="Número interior">
    <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="direccion.referencia" placeholder="Referencia">
    <select class="w-auto block rounded-sm my-2 form-select" wire:model="direccion.estado_id" wire:change="$emit('selectEstado')">
        @foreach ($estados as $estado)
        <option value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
        @endforeach
    </select>
    <select class="w-auto block rounded-sm my-2 form-select" wire:model="direccion.municipio_id">
        @foreach ($municipios as $municipio)
        <option value="{{ $municipio->id }}">{{ $municipio->descripcion }}</option>
        @endforeach
    </select>
    <select class="w-auto block rounded-sm my-2 form-select" wire:model="direccion.pivot.tipo">
        <option value="PICK UP">PICK UP EN TIENDA</option>
        <option value="COLEGIO">RECOGER EN COLEGIO</option>
    </select>
    <button class="block bg-blue-600 text-white py-3 px-6 rounded-md" type="submit">Guardar</button>
    </form>
</div>
