<div>
<form wire:submit.prevent='save()'>
    <span class="block font-rob-bold color-gray--bk font-rob-bold text-base uppercase">{{ $nombre }}</span>
    <table class="w-full">
        <thead class="font-rob-light color-blue--bk font-rob-bold text-sm">
            <td>Meses sin Intereses</td>
            <td>Comisión (%)</td>
            <td>Mínimo</td>
            <td>Activo</td>
            <td></td>
        </thead>
        <tbody>
            <tr>
                <td>@if($meses == 0) <span>PUE</span> @else <span>MSI: {{ $meses }}</span> @endif</td>
                <td><input class="block border rounded-md border-color-gray--BK my-2 form-input w-full" type="text" wire:model.defer="comision" placeholder="Comisión"></td>
                <td><input class="block border rounded-md border-color-gray--BK my-2 form-input w-full" type="text" wire:model.defer="minimo" placeholder="Mínimo"></td>
                <td><input class="block border rounded-md border-color-gray--BK my-2 p-2 form-checkbox w-full" type="checkbox" wire:model.defer="activo" placeholder="Activo"></td>
                <td>
                    <button class="block m-1 py-1 px-2 rounded-md background-blue--bk text-white uppercase font-rob-bold justify-center flex items-center" type="submit">Guardar</button>
                </td>
            </tr>
        </tbody>
    </table>

</form>
</div>
