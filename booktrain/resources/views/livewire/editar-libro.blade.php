<div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK p-10">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-green--bk font-bold text-xl">@if($modo_edicion) EDITANDO LIBRO @else NUEVO LIBRO @endif</h2>
        <a href="/admin/libros" class="background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">LISTA DE LIBROS</a>
    </div>
    <div class="relative w-full h-full flex flex-row">
        <div class="w-1/2 relative justify-center items-center flex">
            <img class="relative w-1/2" src="/assets/img/svg/libros_admin.svg" alt="">
        </div>
        <div data-v-703695a9="" class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="w-1/2 h-full relative flex flex-col justify-center items-center">
            <form wire:submit.prevent="save" class="relative w-1/2 flex flex-col items-center">
                <label class="color-blue--bk font-rob-bold text-lg uppercase self-start mb-6" for="">@if($modo_edicion) EDITAR LIBRO @else NUEVO LIBRO @endif</label>
                {{-- {{$libro}} --}}
                <label class="color-gray--bk relative self-start font-rob-light" for="isbn">ISBN</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" id="isbn" wire:model="libro.isbn">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">NOMBRE</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" id="nombre" wire:model="libro.nombre">
                {{-- <label class="relative self-start" for="">
                    <input class="inline-block rounded-sm my-2 form-checkbox" type="checkbox" wire:model="libro.activo">
                    <span class="w-auto m-4 color-gray--bk uppercase font-rob-light">Activo</span>
                </label> --}}
                <label class="flex self-start ml-4 my-4">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="libro.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                </label>
                @include('errors.validation')
                <button class="relative background-green--bk text-white h-12 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit" wire:click="$emit('refreshList')">Guardar</button>
            </form>
        </div>
    </div>
</div>
