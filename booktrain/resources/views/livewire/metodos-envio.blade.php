<div class="relative flex flex-col items-center justify-start w-full p-10">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="text-xl font-bold uppercase color-green--bk">Métodos de envio | {{$colegio->nombre}}</h2>
        <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/pick_up" class="flex items-center justify-center w-1/6 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold h-11">+ NUEVO PICK UP</a>
    </div>

                <table id="example2" class="stripe tabla_quejas mt-14" style="width:100%">
                    <thead class="">
                        <tr>
                            <th>Nombre</th>
                            <th>Calle</th>
                            <th>#Ext</th>
                            <th>#Int</th>
                            <th>Colonia</th>
                            <th>Estado</th>
                            <th>Municipio</th>
                            <th>Referencia</th>
                            <th>Activo o Inactivo</th>
                            <th>Editar/Agregar</th>
                        </tr>

                    </thead>
                    <tbody>
                        <tr class="text-center">
                            <td>ENVÍO A DOMICILIO</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" @if($this->colegio->configuraciones()->where('etiqueta', 'envio_domicilio_disponible')->first()->valor == 1) checked @endif
                                    wire:click="statusEnvioDomicilio({{$this->colegio->configuraciones()->where('etiqueta', 'envio_domicilio_disponible')->first()->id}}, {{$this->colegio->configuraciones()->where('etiqueta', 'envio_domicilio_disponible')->first()->valor}})">
                                    <span class="slider"></span>
                                </label>
                            </td>
                            <td align="center">
                                <a href="/admin/envio_domicilio/{{$colegio->id}}"><img src="/assets/img/svg/colegios/editar.svg" alt="" class="relative w-6"></a>
                            </td>
                        </tr>
                        @if ($colegio->direccionesEntrega->count() > 0)
                            @php
                                $tipo_colegio = 0;
                                $tipo_pick_up = 0;
                            @endphp
                            @if ($colegio->direccionesEntrega->count()>=2)
                                @foreach ($colegio->direccionesEntrega as $direccionEntrega)
                                @php
                                if($direccionEntrega->pivot->tipo == 'COLEGIO') {
                                    $tipo_colegio++;
                                }else{
                                    $tipo_pick_up++;
                                }
                                @endphp
                                    <tr class="text-center">
                                        <td>{{$direccionEntrega->pivot->tipo}}</td>
                                        <td>{{$direccionEntrega->calle}}</td>
                                        <td>{{$direccionEntrega->no_exterior}}</td>
                                        <td>{{$direccionEntrega->no_interior}}</td>
                                        <td>{{$direccionEntrega->colonia}}</td>
                                        <td>{{$this->estado($direccionEntrega->estado_id)}}</td>
                                        <td>{{$this->municipio($direccionEntrega->municipio_id)}}</td>
                                        <td>{{$direccionEntrega->referencia}}</td>
                                        <td>
                                            <label class="switch">
                                            <input type="checkbox" @if($direccionEntrega->pivot->activo == 1) checked @endif wire:click="status({{$direccionEntrega->id}})">
                                            <span class="slider"></span>
                                            </label>
                                        </td>
                                        <td align="center">
                                            <a href="/admin/colegios_envio/{{$colegio->id}}/{{$direccionEntrega->id}}"><img src="/assets/img/svg/colegios/editar.svg" alt="" class="relative w-6"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @if ($tipo_colegio == 0)
                                <tr class="text-center">
                                    <td>COLEGIO</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/colegio"><span class="font-rob-bold">+ AGREGAR</span></a>
                                    </td>
                                </tr>

                                @endif
                                @if ($tipo_pick_up == 0)

                                <tr class="text-center">
                                    <td>PICK UP</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/pick_up"><span class="font-rob-bold">+ AGREGAR</span></a>
                                    </td>
                                </tr>

                                @endif
                            @else
                                <tr class="text-center">
                                    <td>{{$colegio->direccionesEntrega[0]->pivot->tipo}}</td>
                                    <td>{{$colegio->direccionesEntrega[0]->calle}}</td>
                                    <td>{{$colegio->direccionesEntrega[0]->no_exterior}}</td>
                                    <td>{{$colegio->direccionesEntrega[0]->no_interior}}</td>
                                    <td>{{$colegio->direccionesEntrega[0]->colonia}}</td>
                                    <td>{{$this->estado($colegio->direccionesEntrega[0]->estado_id)}}</td>
                                    <td>{{$this->municipio($colegio->direccionesEntrega[0]->municipio_id)}}</td>
                                    <td>{{$colegio->direccionesEntrega[0]->referencia}}</td>
                                    <td>
                                        <label class="switch">
                                        <input type="checkbox" @if($colegio->direccionesEntrega[0]->pivot->activo == 1) checked @endif wire:click="status({{$colegio->direccionesEntrega[0]->id}})">
                                        <span class="slider"></span>
                                        </label>
                                    </td>
                                    <td align="center">
                                        <a href="/admin/colegios_envio/{{$colegio->id}}/{{$colegio->direccionesEntrega[0]->id}}"><img src="/assets/img/svg/colegios/editar.svg" alt="" class="relative w-6"></a>
                                    </td>
                                </tr>
                                <tr class="text-center">
                                    <td>
                                        @if ($colegio->direccionesEntrega[0]->pivot->tipo == 'COLEGIO')
                                            PICK UP
                                        @else
                                            COLEGIO
                                        @endif
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        @if ($colegio->direccionesEntrega[0]->pivot->tipo == 'COLEGIO')
                                            <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/pick_up"><span class="font-rob-bold">+ AGREGAR</span></a>
                                        @else
                                            <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/colegio"><span class="font-rob-bold">+ AGREGAR</span></a>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @else
                        <tr class="text-center">
                            <td>PICK UP</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/pick_up"><span class="font-rob-bold">+ AGREGAR</span></a>
                            </td>
                        </tr>
                        <tr class="text-center">
                            <td>COLEGIO</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="/admin/agregar_metodo_envio/{{$colegio->id}}/colegio"><span class="font-rob-bold">+ AGREGAR</span></a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>

    <div class="relative flex items-center justify-center w-full">
        <a href="{{ route('admin.colegios') }}" class="relative flex items-center justify-center w-1/6 p-3 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
    </div>
</div>
