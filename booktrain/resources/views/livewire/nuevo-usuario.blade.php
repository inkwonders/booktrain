<div class="w-full flex flex-col relative min-h-full h-full border rounded-md border-color-gray--BK p-10">
    <div class="relative flex justify-between w-full mb-8">
        <h2 class="color-green--bk font-bold text-xl uppercase">Nuevo Usuario</h2>
        {{-- <a href="/admin/usuarios" class="background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">LISTA DE USUARIOS</a> --}}
    </div>
    <form wire:submit.prevent="save" class="relative w-full h-full flex flex-col justify-center">
        <div class="relative w-full h-full flex flex-row border-t-2 justify-between">
            <div class="w-3/12 relative flex mt-8 flex-col">
                {{-- {{$user}} --}}
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">NOMBRE *</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" wire:model="user.name">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">APELLIDOS *</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" wire:model="user.apellidos">
                <label class="color-gray--bk relative self-start font-rob-light" for="isbn">CORREO *</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" wire:model="user.email">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">CONTRASEÑA *</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="password"  wire:model="new_password">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">REPETIR CONTRASEÑA *</label>
                <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="password"  wire:model="password_confirmation">
            </div>
            <div class="w-8/12 h-auto relative flex mt-8 flex-col">
                <div class="w-full h-1/2 relative flex-flex-col">
                    <span class="uppercase font-rob-bold color-gray--bk">ROLES</span>
                    <div class="mt-4 relative w-1/2 h-1/2 flex flex-col flex-wrap">
                        @foreach ($roles as $rol)
                            <label class="relative self-start" for="">
                                    <input class="inline-block rounded-sm my-2" value="{{$rol->name}}" type="radio" wire:model="roles_elegidos">
                                    <span class="w-auto m-4 color-gray--bk uppercase font-rob-light">{{$rol->name}}</span>
                            </label>
                        @endforeach
                    </div>
                </div>
                @if ($mostrar_permisos_admin)
                <div class="w-full h-1/2 relative flex-flex-col">
                    <span class="uppercase font-rob-bold color-gray--bk">PERMISOS</span>
                    <div class="mt-4 relative w-1/2 h-1/2 flex flex-col flex-wrap">
                        @foreach ($permisos as $permiso)
                            <label class="relative self-start" for="">
                                    <input class="inline-block rounded-sm my-2" value="{{$permiso->name}}" type="checkbox" wire:model="permisos_elegidos">
                                    <span class="w-auto m-4 color-gray--bk uppercase font-rob-light">{{$permiso->name}}</span>
                            </label>
                        @endforeach
                    </div>
                </div>
                @endif
                @if($elegir_colegio)
                <div class="w-full h-1/2 relative flex-flex-col">
                    <span class="uppercase font-rob-bold color-gray--bk">ELEGIR COLEGIO:</span>
                    <select wire:model="permisos_elegidos" class="block appearance-none w-1/2 bg-grey-lighter border border-grey-lighter text-grey-darker py-3 px-4 pr-8 rounded">
                        <option value="0" selected disabled>Elige un colegio</option>
                        @foreach ($colegios as $colegio)
                            <option value="{{$colegio->codigo}}">{{$colegio->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
            </div>
        </div>
        <div class="uppercase font-rob-bold color-red--bk">{{$mensaje_error}}</div>
        <button class="w-1/6 relative background-green--bk self-center text-white p-2.5 m-2 rounded-md uppercase font-rob-bold" type="submit">Agregar</button>
        <a href="/admin/usuarios#/" class="w-1/6 relative border rounded-md border-color-gray--BK self-center text-grayBT p-2.5 m-2 cursor-pointer rounded-md uppercase font-rob-bold justify-center text-center"><div class="">Regresar</div></a>
    </form>
</div>
