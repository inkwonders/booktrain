<div class="w-full flex flex-col relative h-full px-12 py-6">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-red--bk font-bold text-xl">NUEVO COLEGIO</h2>
        <a href="{{ route('admin.colegios') }}" class="background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">LISTA DE COLEGIOS</a>
    </div>
    <div class="relative w-full h-full flex flex-row">
        <div class="w-1/2 relative h-full">

        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="w-1/2 relative h-full flex justify-center items-center">
            <form wire:submit.prevent="save" class="w-1/2 relative h-full flex-col justify-center items-center flex">
                <label class="color-blue--bk font-rob-bold text-lg uppercase self-start mb-6" for="">Nuevo colegio</label>
                <label class="color-gray--bk relative self-start font-rob-light" for="codigo">CODIGO</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="colegio.codigo">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">NOMBRE</label>
                <input class="w-full block rounded my-2 form-input" type="text" wire:model="colegio.nombre">
                <label class="color-gray--bk relative self-start font-rob-light" for="nombre">LOGO</label>
                <input class="mb-4 w-full block rounded my-2 form-input" type="text" wire:model="colegio.logo">
                <label for="" class="self-start">
                    <input class="inline-block rounded my-2 form-checkbox" type="checkbox" wire:model="colegio.activo">
                    <span class="w-auto m-4 color-gray--bk uppercase font-rob-light">Activo</span>
                </label>
                <button class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit" wire:click="$emit('refreshList')">Guardar</button>
                <a href="/admin/colegios_editando" class="w-1/2 relative p-2.5 flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk cursor-pointer"><div>Regresar</div></a>
            </form>
        </div>
    </div>
</div>
