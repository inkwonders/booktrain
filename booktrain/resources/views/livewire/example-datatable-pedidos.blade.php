



<x-livewire-tables::table.cell>
    {{$row->serie }}{{$row->folio}}
    </x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->created_at != null ? (date_format($row->created_at,"d / m / Y")) : ''}}
    </x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
    </x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->user->email }}
    </x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->celular_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
@php
    if ( $row->factura == 1){
       echo "Si";
    }
    else
    {
        echo "No";
    }
@endphp
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->referencia != null ? $row->referencia->referencia : '' }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->metodo_pago }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->forma_pago }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    ${{ number_format($row->total,2) }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    ${{ number_format($row->pagos->where('status','SUCCESS')->pluck('amount')->first(), 2) }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
     {{ $row->pagos->where('status','SUCCESS')->pluck('created_at')->first() }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @php
    if ($row->tipo_entrega == 1){
    echo "<span>Colegio</span>";
    }
    else if ($row->tipo_entrega == 2){
    echo "<span>Domicilio</span>";
    }
    else{
    echo "<span>PickUp</span>";
    }
    @endphp
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @php
    if ($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO'){
    echo "<span class'color-red--bk'>$row->status</span>";
    }
    else if ($row->status == 'PROCESADO'){
    echo " <span class='color-gray--bk'>$row->status</span>";
    }
    else if ( $row->status == 'PAGO EN REVISION'){
    echo " <span class='color-blue--bk'>$row->status</span>";
    }
    else{
    echo " <span class='color-green--bk'>$row->status</span>";
    }
    @endphp
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <span>
        <form action="/referencias/reporte_referencias_status/" method="GET" class="relative w-full items-center formulario-cambio" id="formulario-cambio" name="formulario-cambio" >
            @php if($row->status  == 'PROCESADO'){
            echo '
            <a style="height: 47.65px;line-height: 43.65px;border-color: #81b84e;"type="submit" data-userid="'.$row->id.'" class="focus:outline-none openModal h-12 w-56 background-white--bk text-green-1000 font-bold rounded hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border border-solid border-green-500">Modificar</a>
            ';
            }
            @endphp
            @php if($row->status  == 'PAGADO'){
            echo '<input type="hidden" name="estatus" id="estatus" value="CANCELADO">
            <input type="hidden" name="row_id" id="row_id" value="'.$row->id.'">
            <button type="submit"  id="btnSend" name="btnSend"  class="h-12 w-56 background-white--bk text-red-500 font-bold rounded hover:bg-red-500 hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border border-solid border-red-400">Cancelar</button>
            ';
            }
            @endphp
            @php if($row->status  == 'CANCELADO'){
            echo '<input type="hidden" name="estatus" id="estatus" value="PROCESADO">
            <input type="hidden" name="row_id" id="row_id" value="'.$row->id.'">
            <button type="submit"  id="btnSend" name="btnSend"  class="h-12 w-56 background-white--bk text-green-1000 font-bold rounded hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border border-solid border-green-500" style="border-color: #81b84e;">Activar</button>
            ';
            }
            @endphp
        </form>
    </span>
</x-livewire-tables::table.cell>
<x-livewire-tables::table.cell>
    <span>
        <form action="/referencias/envio_confirmacion_pago/" method="GET" class="relative w-full items-center formulario-envio-mail" id="formulario-envio-mail" name="formulario-envio-mail" >
            @php if($row->status  == 'PAGADO'){
            echo '<input type="hidden" name="row_id" id="row_id" value="{{ $row->id }}">
            <button style="border-color: #50b9e7;" type="submit"  id="btnSend" name="btnSend" class="h-12 w-56 background-white--bk text-blue-1000 font-bold rounded hover:background-blue--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border border-solid border-blue-1000">Enviar</button>
            ';
            }
            else
            {

            }
            @endphp
        </form>
    </span>
</x-livewire-tables::table.cell>
