<div>
    <div class="flex">
        <div class="inline-block border border-gray-300 m-2 p-2 w-1/2">
            <span>Seccion</span>
            {{ $seccion->nombre }}
            @livewire('nuevo-nivel', ['seccion' => $seccion])
            @livewire('lista-niveles', ['seccion' => $seccion])
        </div>
        <div class="inline-block border border-gray-300 m-2 p-2 w-1/2">
        @if($nivel)
            @livewire('nuevo-paquete', ['seccion' => $seccion, 'nivel' => $nivel])
            @livewire('lista-paquetes', ['seccion' => $seccion, 'nivel' => $nivel])
        @else
            <span>Seleccione un nivel</span>
        @endif
        </div>
    </div>
    <div class="flex border border-gray-300 p-2 m-2">
        <div class="block w-full">
            @if($paquete)
            @livewire('libros-paquetes', ['paquete' => $paquete])
            @else
            <span>Seleccione un nivel</span>
            @endif
        </div>
    </div>

</div>
