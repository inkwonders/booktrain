<div class="pt-5">
    <span class="inline-block color-blue--bk font-rob-bold text-lg uppercase">Lista de secciones</span> <br>
    <p class="inline-block color-gray--bk font-rob-light uppercase">FILTRO</p><br>
    <input class="inline-block border rounded-md border-color-gray--BK p-2" wire:model="filtro" wire:change="$emit('refreshList')">
    <table class="border rounded-md border-color-gray--BK w-full mt-3">
        <thead>
            <td></td>
            <td>Nombre</td>
            <td>Activo</td>
            <td>Niveles</td>
            <td>Libros</td>
            <td></td>
        </thead>
        <tbody>
        @foreach ($secciones->reverse() as $seccion)
        <tr class="border rounded-md border-color-gray--BK" wire:dblclick="$emit('editSeccion', {{ $seccion->id }})">
            <td><a class="text-purple-500" href="{{ route('admin.colegios.secciones', [$colegio->id, $seccion->id]) }}">Editar</a></td>
            <td>{{ $seccion->nombre }}</td>
            <td>{{ $seccion->activo }}</td>
            <td>{{ $seccion->niveles->count() }}</td>
            <td>{{ $seccion->niveles->pluck('paquetes')->collapse()->pluck('libros')->collapse()->count() }}</td>
            <td>
                <button class="text-red-500" wire:click="delete({{ $seccion->id }})">Eliminar</button>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
