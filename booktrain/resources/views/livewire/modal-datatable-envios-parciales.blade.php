@if($modal_envio_parcial)
<div class="fixed top-0 bottom-0 left-0 right-0 z-20 p-12">
   <div class="flex items-start justify-center w-full h-full overflow-x-hidden overscroll-y-auto">
       <div class="absolute top-0 bottom-0 left-0 right-0 z-0 bg-gray-500 opacity-50" wire:click="$set('modal_envio_parcial', false)"></div>
       <div class="relative z-10 flex flex-col items-center justify-between w-full h-auto max-w-6xl p-10 pt-5 bg-white border rounded-lg border-grayBT">
           <p class="text-xl font-bold text-grayBT">{{ $pedido_modal->serie }}-{{ $pedido_modal->folio }}</p>
           <div class="flex items-center justify-between pb-20">

           <div class="w-full">
              <div class="w-full border-b-2"></div>
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>
                 </div>
                 <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">nombre: </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->nombre_contacto }} </span>
                    </div>
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">apellido(s): </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->apellidos_contacto }} </span>
                    </div>
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Celular: </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->celular_contacto }} </span>
                    </div>
                 </div>
              </div>
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">ENTREGA</p>
                 </div>
                 <div class="flex mt-1 flex-column">
                    <span class="text-base uppercase color-gray--bk">
                    @if ($pedido_modal->tipo_entrega == 1)
                    <span class="font-rob-bold">COLEGIO:</span>
                    @elseif (($pedido_modal->tipo_entrega == 2))
                    <span class="font-rob-bold">DOMICILIO:</span>
                    @elseif (($pedido_modal->tipo_entrega == 3))
                    <span class="font-rob-bold">PICKUP:</span>
                    @else
                    <span class="font-rob-bold">DOMICILIO:</span>
                    @endif
                    {{ is_null($pedido_modal->direccion)? '' : $pedido_modal->direccion->direccion_completa }}
                    </span>
                    <span class="text-base uppercase color-gray--bk"></span>
                 </div>
                 <div class="flex mt-4 flex-column">
                    <span class="text-base color-gray--bk ">
                        @if ($pedido_modal->tipo_entrega == 2)
                            @if ($pedido_modal->envio->paqueteria()->first() != null)
                                <span class="font-rob-bold">Guia:</span> {{ $pedido_modal->envio->guia }}</br>
                                <span class="font-rob-bold">Paquetería:</span> {{ $pedido_modal->envio->paqueteria()->first()->nombre }}</br>
                                <span class="font-rob-bold">Url: </span><a target="_blank" href='{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}'>{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}</a></br>
                            @else

                            @endif
                        @else

                        @endif
                    </span>
                    <span class="text-base uppercase color-gray--bk"></span>
                </div>
                 <div class="mt-1 mb-1 text-left">
                    <span class="col-span-7 mt-3 text-base text-left uppercase color-gray--bk font-rob-bold lg:text-right">Quedan {{ $libros_pendientes_envio }} libros por enviar de un total de {{ $total_libros_pedido }}</span>
                 </div>
              </div>
              @if (isset($pedido_modal->datosFactura ))
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">datos de facturación del último pedido</p>
                 </div>
                 <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Nombre o razón social</th>
                            <th class="pb-4 pr-4">RFC</th>
                            <th class="pb-4 pr-4">Correo</th>
                            <th class="pb-4 pr-4">Uso de CFDI</th>
                            <th class="pb-4 pr-4">método de pago</th>
                            <th class="pb-4 pr-4">Forma de pago</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->razon_social }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->rfc }}</td>
                            <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->correo }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->metodo_pago }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->forma_pago }}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Calle</th>
                            <th class="pb-4 pr-4">Num. exterior</th>
                            <th class="pb-4 pr-4">Num. interior</th>
                            <th class="pb-4 pr-4">Colonia</th>
                            <th class="pb-4 pr-4">Municipio</th>
                            <th class="pb-4 pr-4">Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->calle }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->num_exterior }}</td>
                            <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->num_interior }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->colonia }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->municipio }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->estado }}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Codigo postal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->cp }}</td>
                        </tr>
                    </tbody>
                </table>

                 <div class="pb-4 mt-1 lg:hidden">
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->razon_social }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->rfc }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
                    <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->correo }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
                    <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->metodo_pago }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->forma_pago }}</div>


                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Calle</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->calle }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. exterior</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_exterior }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. interior</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_interior }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colonia</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->colonia }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Municipio</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->municipio }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Estado</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->estado }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Codigo postal</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->cp }}</div>

                 </div>
              </div>
              @endif
              @foreach($pedido_modal->resumenes as $resumen_pedido_alumno)
              <div class="flex flex-col w-full lg:flex-row">
                 <div class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">
                    <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">
                       <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                       <span class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido_modal->colegio->nombre }} </span>
                       <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado: </span>
                       <span class="col-span-4 text-base uppercase color-gray--bk"> {{ $resumen_pedido_alumno->paquete->nivel->nombre }} - {{ $resumen_pedido_alumno->paquete->nivel->seccion->nombre }} </span>
                    </div>
                    <div class="w-full mt-1 text-left">
                       <span class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE DEL ALUMNO:* </span>
                    </div>
                    <div class="w-full mt-1 mb-5 text-left">
                       <span class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_completo_alumno }} </span>
                    </div>
                 </div>
                 <div class="lg:w-2/3 lg:border-b-2">
                    <div class="flex flex-col mt-10 mb-10 lg:grid lg:grid-cols-11 lg:gap-1">
                       @foreach ($libros_envio[ $resumen_pedido_alumno->id ] as $id => $detalle)
                       <span class="col-span-7 text-base text-left  color-gray--bk lg:text-right">{{ $detalle['libro'] }}</span>
                       <span class="col-span-2 text-base text-left  color-gray--bk"><label class="text-base text-gray-500 lg:hidden">ISBN </label>{{ $detalle['isbn'] }}</span>
                       @if($detalle['enviado'] == 1)
                        <span class="col-span-2 text-base text-greenBT">Ya fue enviado</span>
                       @else
                       <label class='switch' >
                            <input  type='checkbox' wire:model="libros_envio.{{ $resumen_pedido_alumno->id }}.{{ $id }}.seleccionado">
                            <span class='slider'></span>
                       </label>
                       @endif
                       <div class="w-full my-4 border-b-2 lg:hidden"></div>
                       @endforeach
                    </div>
                 </div>
              </div>

              @endforeach
              <div class="w-full">
               <div class="w-full pb-3 border-b-2">
                  <div class="flex mt-1 flex-column">
                     <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Guía y paqueteria</p>
                  </div>
                  <div class="flex justify-between">
                     <div class="mt-1 w-max">
                        <span class="text-base uppercase color-gray--bk font-rob-bold">No. de Guía: </span>
                        <input require  wire:model="no_guia" class="relative px-2 mr-2 border rounded-md w-50 h-11 color-gray--bk" type="text" id="no_guia" name="no_guia" placeholder="Ingresa el número de guía">
                        </br>@error('no_guia') <span class="error" style="color: red">{{ $message }}</span> @enderror

                     </div>
                     <div class="mt-1 w-max">
                        <span class="text-base uppercase color-gray--bk font-rob-bold">Selecciona la paqueteria: </span>
                        <select require wire:model="paqueteria" name="paqueteria" id="paqueteria" class="relative px-2 mr-2 border rounded-md appearance-none w-60 h-11 color-gray--bk">
                            <option value="">Selecciona</option>
                            @foreach ($this->paqueterias as $paqueteria)
                                <option  value="{{ $paqueteria->id }}">{{ $paqueteria->nombre }}</option>
                            @endforeach
                        </select>
                       </br>@error('paqueteria') <span class="error" style="color: red">{{ $message }}</span> @enderror
                     </div>

                  </div>
               </div>
           </div>
           </div>

        </div>
        <div class="grid grid-cols-3 gap-4">
            <div class="...">
                <button  class="font-bold text-xl text-white rounded h-11 w-48 mb-2 border border-transparent  @if($libros_pendientes_envio > 0) hover:bg-white bg-greenBT hover:border-greenBT hover:text-greenBT @else bg-gray-400 cursor-not-allowed @endif " wire:click="save">Enviar</button>
            </div>
            <div class="...">

            </div>
            <div class="...">
                <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-grayBT h-11 hover:border-grayBT hover:bg-white hover:text-grayBT" wire:click="$set('modal_envio_parcial', false)">Cancelar</button>
            </div>

          </div>

        </div>
    </div>
</div>
@endif
