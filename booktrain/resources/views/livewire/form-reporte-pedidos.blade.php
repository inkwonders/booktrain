<div class="relative flex justify-between w-full mb-4">
<h2 class="color-red--bk font-bold text-xl">Pedidos</h2>
 <form  class="relative w-full items-center ">
                <div class="relative w-4/5 flex flex-col items-center mt-12 ">
                <p>Busquedas sobre los pedidos</p>
                <div class="relative w-full border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2" style=" width: 98.5%;">
                    <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                    <select wire:model="status"  wire:change="$emit('status')"  name="status" wire:model="status" class=" p-2 border color-gray--bk appearance-none ">
                        <label for="status">Selecciona un estatus</label>
                        <option value="0">Cualquier estatus</option>
                        <option value="ENTREGADO">ENTREGADO</option>
                        <option value="PAGADO">PAGADO</option>
                        <option value="PROCESADO">PROCESADO</option>
                        <option value="ENVIADO">ENVIADO</option>
                        <option value="CANCELADO">CANCELADO</option>
                        <option value="PAGO RECHAZADO">PAGO RECHAZADO</option>
                        <option value="PAGO EN REVISION">PAGO EN REVISION</option>
                    </select>
                </div>

                <div class="w-full relative flex flex-row">
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                        <select wire:model="factura" name="factura" class="p-2 border color-gray--bk appearance-none" >
                            <label for="factura">Facturas Si y No</label>
                            <option value="0">Facturas Todo</option>
                            <option value="1">SI solicita</option>
                            <option value="2">NO solicita</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                        <select wire:model="metodo_pagos" name="metodo_pagos" class="p-2 border color-gray--bk appearance-none" style="right: 2.5px;">
                            <label for="Métodos pagos">Métodos pagos</label>
                            <option value="0">Cualquier Método de pago</option>
                            <option value="CREDITO">CREDITO</option>
                            <option value="DEBITO">DEBITO</option>
                            <option value="AMEX">AMEX</option>
                            <option value="BANCO">BANCO</option>
                            <option value="TIENDA">TIENDA</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                        <select wire:model="forma_pagos" name="forma_pagos" class="p-2 border color-gray--bk appearance-none" style="left: 2.5px;">
                            <label for="forma_pagos">Forma pagos</label>
                            <option value="0">Cualquier Forma de pago</option>
                            <option value="PUE">PUE</option>
                            <option value="DIFERIDO3M">Diferido 3m</option>
                            <option value="DIFERIDO6M">Diferido 6m</option>
                            <option value="DIFERIDO9">Diferido 9m</option>
                        </select>
                    </div>
                    <div class="relative w-1/4  border border-gray-200 rounded-md cursor-pointer overflow-hidden m-2">
                        <label for="select_motivos" class="select_motivos cursor-pointer"></label>
                        <select wire:model="forma_entrega" name="forma_entrega" class="p-2 border color-gray--bk appearance-none">
                            <label for="forma_entrega">Forma de entrega</label>
                            <option value="0">Cualquier Forma de entrega</option>
                            <option value="1">Colegio</option>
                            <option value="2">Domicilio</option>
                            <option value="3">Pick-Up</option>
                        </select>
                    </div>
                </div>
                <p style="padding-top: 9px;">Rango de fechas</p>
                <div class="w-full relative flex flex-row my-2">
                    <input name="inicio" wire:change="inicio('inicio')" wire:model="inicio" id="inicio" required placeholder="FECHA INICIAL" class="relative w-1/3 mr-2 p-2 border color-gray--bk" type="text">
                    <input name="fin"  wire:change="fin('fin')" wire:model="fin" id="fin" required placeholder="FECHA FINAL" class="relative w-1/3 p-2 border color-gray--bk" type="text">

                <button style="left: 8px;width: 31%;" type="submit" id="btn_buscar" class="relative w-1/3 h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">BUSCAR</button>
                </div>
            </div>
</form>
</div>
