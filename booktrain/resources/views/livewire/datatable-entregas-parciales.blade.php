<x-livewire-tables::table.cell>
    {{$row->serie }}{{$row->folio}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <span>@if ($row->tipo_entrega == 1) Colegio @elseif ($row->tipo_entrega == 2) Domicilio @else PickUp @endif </span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->created_at != null ? (date_format($row->created_at,"d / m / Y")) : ''}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if($row->status == "ENTREGA LIQUIDADA")

    @elseif($row->status == "PAGADO")
    <div class="px-4 w-max">
        <form action="tabla_entregas_parciales/cambio_status" method="POST" class="relative items-center w-full formulario-cambio" id="formulario-cambio" name="formulario-cambio">
        @csrf
        @foreach ($row->resumenes as $resumen_pedido_alumno )
        @foreach ( $resumen_pedido_alumno->detallesResumenPedidos as $detalles_resumen )
        <input type="hidden" name="id_detalle_pedido[]" id="id_detalle_pedido[]" value="{{ $detalles_resumen->id }}">
        @endforeach
        @endforeach
        <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $row->id }}">
        <input type="hidden" name="estatus" id="estatus" value="ENTREGADO">
        <button type="submit"  id="btnSend" name="btnSend"  class="w-32 h-12 font-bold border border-solid rounded background-white--bk text-blue-1000 hover:background-blue--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border-blue-1000">Entregar </button>
        </form>
    </div>
    @elseif($row->status == "ENTREGADO")

    @else
    <div class="px-4 w-max">
        <form action="tabla_entregas_parciales/cambio_status" method="POST" class="relative items-center w-full formulario-cambio" id="formulario-cambio" name="formulario-cambio">
        @csrf
        <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $row->id }}">
        <input type="hidden" name="estatus" id="estatus" value="ENTREGA LIQUIDADA">
        <button type="submit"  id="btnSend" name="btnSend"  class="w-32 h-12 border border-green-500 border-solid rounded background-white--bk text-green-1000 hover:background-green--bk hover:border-white hover:text-white md:text-md md:text-base sm:text-sm" style="border-color: #81b84e;">Liquidar</button>
        </form>
    </div>
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if ($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO')
    <span class'color-red--bk'>
    @elseif ($row->status == 'PROCESADO')
    <span class='color-gray--bk'>
    @elseif ( $row->status == 'PAGO EN REVISION')
    <span class='color-blue--bk'>
    @else
    <span class='color-green--bk'>
    @endif
    {{ $row->status }}</span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="px-4 w-max">
        <div class="relative h-auto cursor-pointer w-14" wire:click="$emit('modalDetallesPedido', {{ $row->id }})">
            <img src="{{ asset('img/svg/caja_cantidad.svg') }}" class="w-full">
            <div class="absolute bottom-0 left-0 right-0 flex items-center justify-center top-2 text-grayBT">
                <p>Parcial</p>
            </div>
        </div>
   </div>
</x-livewire-tables::table.cell>
