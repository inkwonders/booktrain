<section class="w-full flex flex-col items-center justify-center min-h-custom px-2 py-4">
    <div class="w-full flex flex-col relative h-auto min-h-interno border rounded-md border-color-gray--BK justify-start p-10">
       <div class="relative flex flex-col justify-between w-full mb-1">
          <h2 class="color-red--bk font-bold text-xl">ENVIOS PARCIALES</h2>
          <div class="relative w-full flex flex-col items-center">


             <div class="relative flex justify-between w-full py-4 mb-5">
                 <div class="relative w-auto items-center ">
                     <input name="inicio"    id="inicio" required placeholder="FECHA INICIAL"   class="relative w-40 mr-2 h-11 px-2 border color-gray--bk rounded-md"    type="text">
                     <input name="fin"       id="fin"    required placeholder="FECHA FINAL"     class="relative w-40 mr-2 h-11 px-2 border color-gray--bk rounded-md"    type="text">
                     <button onclick="emit_fechas()" id="buscar" class="relative w-36 h-11 background-green--bk text-white font-bold rounded hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">BUSCAR</button>
                 </div>
             </div>
          </div>
       </div>

       <div class="data-table-diseño">
             <livewire:datatable-envios-parciales  searchable="folio, nombre_contacto, apellidos_contacto, email" exportable />
       </div>
    </div>

 </section>
