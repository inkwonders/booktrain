<div class="w-full h-custom relative flex flex-col p-6">
    <h2 class="color-green--bk font-bold text-xl uppercase">Editar sección | {{$colegio->nombre}}</h2>
    <div class="w-full h-full relative flex flex-row">
        <div class="w-1/2 h-full relative flex flex-col border-r border-grayBT">
            <table class="w-full relative table-striped mt-8">
                <thead class="border-b border-t border-gray-400">
                    <th class="py-5 color-gray--bk">Grado</th>
                    <th class="color-gray--bk">Activo</th>
                    <th class="color-gray--bk">Editar</th>
                    <th class="color-gray--bk">Eliminar</th>
                </thead>
                <tbody class="border-b border-gray-400 text-center">
                    @foreach ($seccion->niveles()->get() as $nivel)
                    <tr>
                        <td class="py-7">{{$nivel->nombre}}</td>
                        <td>
                            @if ($nivel->activo == 1)
                                ACTIVO
                            @else
                                INACTIVO
                            @endif
                        </td>
                        <td align="center"><img wire:click="editar({{$nivel->id}})" src="/assets/img/svg/colegios/editar.svg" alt="" class="relative w-6 cursor-pointer" ></td>
                        <td align="center"><img wire:click="delete({{$nivel->id}})" src="/assets/img/svg/colegios/eliminar.svg" alt="" class="relative w-6 cursor-pointer" ></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- HEADER TABLA --}}
            {{-- <div class="w-full flex flex-row relative border-r-2 border-t-2 border-b-2 border-color-gray--BK h-12 font-rob-bold color-gray--bk justify-center items-center mt-12">
                <div class="w-2/6 relative">Grado</div>
                <div class="w-3/6 relative">Activo</div>
                <div class="w-1/6 relative">Editar</div>
            </div> --}}
            {{-- Cuerpo de tabla --}}
            {{-- <div class="w-full justify-start border-r-2 border-color-gray--BK flex flex-col relative font-rob-light color-gray--bk items-center lg:4/6 xl:h-5/6 overflow-y-auto">
                @foreach ($seccion->niveles()->get() as $nivel)
                <div class="border-b items-center border-color-gray--BK relative py-4 flex flex-row w-full">
                    <div class="w-2/6 relative break-all">{{$nivel->nombre}}</div>
                    <div class="w-3/6 relative break-words">
                        @if ($nivel->activo == 1)
                            ACTIVO
                        @else
                            INACTIVO
                        @endif
                    </div>
                    <div class="w-1/6 relative flex justify-between items-center">
                        <img wire:click="editar({{$nivel->id}})" src="/assets/img/svg/colegios/editar.svg" alt="" class="relative w-6 cursor-pointer" >
                    </div>
                </div>
                @endforeach
            </div> --}}
        </div>
        <div class="relative w-1/2 flex justify-center items-center">
            @if (!$editar_nivel)
            <form wire:submit.prevent="save_seccion" class="w-1/2 relative h-full flex flex-col justify-center items-center">
                <h3 class="color-blue--bk font-bold text-xl uppercase self-start ml-4 pl-2">Editar nivel</h3>
                <div class="relative w-full flex mt-6 flex-col">
                    <span class="font-rob-light color-gray--bk uppercase ml-4 pl-2">Nombre</span>
                    <input type="text" wire:model="nombre_seccion" class="relative w-full m-4 p-2 color-gray--bk border">
                </div>
                <div class="relative w-full flex mt-4">

                    <label class="flex self-start ml-4 my-4">
                        <input id="one" class="custom-check--bk" type="checkbox" wire:model="activo_seccion">
                        <label class="flex self-start custom-check-label--bk" for="one">
                            <span class="custom-check-span--bk"></span>
                        </label>
                        <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                    </label>
                    {{-- <input wire:model="activo_seccion" type="checkbox" class="relative m-4 p-2 rounded form-checkbox">
                    <span class="relative m-4 w-auto color-gray--bk uppercase font-rob-light">Activo</span> --}}
                </div>
                <div class="w-full relative h-auto">
                    {{-- <span>Niveles: {{ json_encode($niveles) }}</span> --}}
                    @if (!empty($niveles_seccion_nuevos))
                        @foreach ($niveles_seccion_nuevos as $id => $nivel)
                            <div class="relative flex">
                                <span class="w-full block border rounded-md border-color-gray--BK form-input h-11 mb-3">{{ $nivel }}</span>
                                {{-- <button wire:click="quitarNivel({{ $id }})">Quitar</button> --}}
                                <div wire:click="quitarNivel({{ $id }})" class="w-11 h-11 bg-redBT absolute -right-12 bottom-3 rounded-md cursor-pointer">
                                    <div class="h-1 w-4 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <input class="w-full block border rounded-md border-color-gray--BK form-input h-11 mb-3" wire:model="nuevo_nivel" type="text">
                    <div class="w-11 h-11 bg-greenBT absolute -right-12 bottom-3 rounded-md cursor-pointer" wire:click="guardarNivel">
                        <div class="h-1 w-4 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                        <div class="h-4 w-1 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                    </div>
                </div>
                <div class="font-bold color-red--bk">{{$error}}</div>
                <div class="relative w-full flex flex-col items-center justify-center pt-12">
                    <button type="submit" class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold">
                        GUARDAR
                    </button>
                    <a href="/admin/colegios_editando/secciones/{{$colegio->id}}" class="w-1/2 relative p-2.5 flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk cursor-pointer">
                        CANCELAR
                    </a>
                </div>
            </form>

            @endif
            @if($editar_nivel)
                <form wire:submit.prevent="save" class="w-1/2 relative h-full flex flex-col justify-center items-center">
                    <h3 class="color-blue--bk font-bold text-xl uppercase self-start ml-4 pl-2">Editar grado</h3>
                    <div class="relative w-full flex mt-6 flex-col">
                        <span class="font-rob-light color-gray--bk uppercase ml-4 pl-2">Nuevo nombre</span>
                        <input type="text" wire:model="nombre" class="relative w-full m-4 p-2 color-gray--bk border">
                    </div>
                    <div class="relative w-full flex mt-4">
                        <label class="flex self-start ml-4 my-4">
                            <input id="one" class="custom-check--bk" type="checkbox" wire:model="activo">
                            <label class="flex self-start custom-check-label--bk" for="one">
                                <span class="custom-check-span--bk"></span>
                            </label>
                            <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                        </label>
                        {{-- <input wire:model="activo" type="checkbox" class="relative m-4 p-2 rounded form-checkbox">
                        <span class="relative m-4 w-auto color-gray--bk uppercase font-rob-light">Activo</span> --}}
                    </div>

                    <div class="font-bold color-red--bk">{{$error}}</div>
                    <div class="relative w-full flex flex-col items-center justify-center pt-12">
                        <button type="submit" class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold">
                            GUARDAR
                        </button>
                        <a href="/admin/editar_seccion/{{$colegio->id}}/{{$seccion_id}}" class="w-1/2 relative p-2.5 flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk cursor-pointer">
                            TERMINAR
                        </a>
                    </div>
                </form>
            @endif
            @include('errors.validation')
        </div>
    </div>
    <div class="w-full justify-center items-center relative flex my-6">
        <a href="/admin/colegios_editando/secciones/{{$colegio->id}}" class="appearance-none relative block text-center lg:py-1 py-3 px-3 lg:w-2/12 w-full lg:my-1.5 rounded-md color-gray--bk border border-solid border-gray-400 hover:bg-white hover:border-color-blue--BK hover:color-blue--bk lg:text-lg md:text-base sm:text-sm">REGRESAR</a>
    </div>
</div>
