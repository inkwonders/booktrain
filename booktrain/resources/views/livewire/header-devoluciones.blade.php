<div class="relative flex flex-col items-center w-full">
    <div class="relative flex justify-between w-full py-4 mb-5">
        <div class="flex items-center w-full ">

            <input name="inicio"    oninput="emit_fechas()" wire:model="inicio" id="inicio" required placeholder="FECHA INICIAL"   class="relative w-40 px-2 mr-2 border rounded-md h-11 color-gray--bk"    type="text">

            <input name="fin"       oninput="emit_fechas()" wire:model="fin" id="fin"    required placeholder="FECHA FINAL"     class="relative w-40 px-2 mr-2 border rounded-md h-11 color-gray--bk"    type="text">

            <div class="inline-flex ml-2">

                <label class="flex justify-end items-center">
                    <span class="color-gray--bk font-rob-regular">Con devoluciones</span>
                    <div class="relative ml-2">
                    <input class="radioBt" type="radio" name="filtro_devuelto" wire:model='filtro_devuelto' value="1" />
                    <span class="fakeRadio"></span>
                    </div>
                 </label>

                 <label class="flex justify-end items-center ml-4">
                    <span class="color-gray--bk font-rob-regular">Todos</span>
                    <div class="relative ml-2">
                    <input class="radioBt" type="radio" name="filtro_devuelto" wire:model='filtro_devuelto' value="0" />
                    <span class="fakeRadio"></span>
                    </div>
                 </label>

                 {{-- Todos --}}

                {{-- <label class="items-center inline p-6 m-5 color-gray--bk">
                    <input type="radio" name="filtro_devuelto" wire:model='filtro_devuelto' value="1">
                    <span class="ml-2">Con devoluciones</span>
                </label>
                <label class="items-center inline p-6 m-5 color-gray--bk">
                    <input type="radio" name="filtro_devuelto" wire:model='filtro_devuelto' value="0">
                    <span class="ml-2">Todos</span>
                </label> --}}
            </div>

        </div>
    </div>
</div>
