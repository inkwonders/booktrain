<div>
    <x-livewire-tables::table.cell>
        <p>{{ $row->serie . $row->folio }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->status }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->nombre_contacto }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->apellidos_contacto }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->celular_contacto }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->referencia->referencia }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ $row->referencia->fecha_pago }}</p>
    </x-livewire-tables::table.cell>

    <x-livewire-tables::table.cell>
        <p>{{ number_format($row->total, 2) }}</p>
    </x-livewire-tables::table.cell>
</div>
