<div class="m-2 pt-5">
    @if($colegio)
        <div class="flex w-full">
            <input class="flex w-1/8 px-4 outline-none border border-gray-200 h-11 text-grayBT rounded" wire:model="filtro" wire:change="$emit('refreshList')" placeholder="Buscar">
        </div>

        @if($modal_reorden)
            <div class="fixed top-0 left-0 bottom-0 right-0 flex items-center justify-center z-20">
                <div class="absolute top-0 bottom-0 left-0 right-0 -z-1 bg-gray-500 opacity-50" wire:click="cerrarModal"></div>
                <div class="relative h-60 w-96 bg-white rounded-lg z-10 border border-grayBT flex items-center justify-between flex-col p-4">
                    <p class="font-bold text-xl text-grayBT">EDITAR PUNTO DE REORDEN</p>
                    <div class="flex justify-between items-center">
                        <button class="relative w-8 h-8 cursor-pointer rounded mx-2" wire:click="decrementaPuntoReorden">
                            <div class="absolute h-1 w-3 bg-grayBT m-auto top-0 left-0 right-0 bottom-0"></div>
                        </button>
                        <input type="number" name="reorden" wire:model.debounce.500ms="punto_reorden" class="border border-grayBT rounded h-12 w-28 text-center text-grayBT text-2xl appearance-none">
                        <button class="relative w-8 h-8 cursor-pointer rounded mx-2" wire:click="incrementaPuntoReorden">
                            <div class="absolute h-1 w-4 bg-grayBT m-auto top-0 left-0 right-0 bottom-0"></div>
                            <div class="absolute h-1 w-4 bg-grayBT m-auto top-0 left-0 right-0 bottom-0 transform rotate-90"></div>
                        </button>
                    </div>
                    @if($punto_reorden < 0)
                    <h4 class="text-red-600">Solo se admiten valores positivos</h4>
                    @endif
                    <button class="font-bold text-xl text-white bg-greenBT rounded h-11 w-48 mb-2 border border-transparent hover:border-greenBT hover:bg-white hover:text-greenBT" wire:click="guardarPuntoReorden">GUARDAR</button>
                </div>
            </div>
        @endif

    <table class="w-full mt-3">
        <thead class="border-grayBT border-t border-b h-12 font-bold text-grayBT">
            {{-- <td class="px-2 text-center">ID</td> --}}
            <td class="px-2 ">ISBN</td>
            <td class="px-2 ">Nombre</td>
            <td class="px-2 text-center">Activo</td>
            <td class="px-2 text-center">Stock</td>
            <td class="px-2 text-center">Bajo pedido</td>
            <td class="px-2 text-center">Punto de reorden</td>
        </thead>
        <tbody class="text-grayBT">
            @foreach ($this->libros as $libro)
            <tr class="h-14 {{ $loop->even? " bg-gray-100 " : " " }}">
                {{-- <td class="px-2 text-center" wire:click="editarPuntoReorden({{ $libro->id }})">{{ $libro->id }}</td> --}}
                <td class="px-2 ">{{ $libro->isbn }}</td>
                <td class="px-2 ">{{ $libro->nombre }}</td>

                <td class="px-2 text-center">{{ $libro->pivot->activo ? 'Si' : 'No' }}</td>
                <td class="px-2 text-center">{{ $libro->pivot->stock }}</td>
                <td class="px-2 text-center">{{ $libro->pivot->bajo_pedido ? 'Si' : 'No' }}</td>
                <td class="px-2 text-center">
                    <button class="h-auto w-14 relative" wire:click="editarPuntoReorden({{ $libro->id }})">
                        <img src="{{ asset('img/svg/caja_cantidad.svg') }}" class="w-full">
                        <div class="absolute top-2 bottom-0 left-0 right-0 flex justify-center items-center text-grayBT">
                            <p>{{ $libro->pivot->punto_reorden }}</p>
                        </div>
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>
