<div class="flex flex-col border rounded-md border-color-gray--BK p-10 justify-between w-full relative">
    <div class="w-full flex flex-row justify-between min-h-secciones">
        <div class="w-1/2 flex flex-col justify-between items-center pr-8">
            <div class="w-full">
                <label class="color-green--bk font-rob-bold text-xl" for="">NUEVA SECCIÓN ACADEMICA | {{$colegio->nombre}}</label>
            </div>
            <img class="w-4/5 my-4" src="{{ asset('img/svg/imagen_seccion.svg') }}" alt="">
            <div></div>
        </div>
        <div class="h-full w-px bg-grayDarkBT"></div>
        <form wire:submit.prevent="save" class="w-1/2 flex justify-center items-center pl-8 py-6">
            <div class="max-w-md w-full pr-12">
                <div class="w-full pb-8">
                    <label class="color-blue--bk font-rob-bold text-xl" for="">NUEVA SECCIÓN ACADEMICA</label>
                </div>

                <p class="inline-block color-gray--bk font-rob-light uppercase">NOMBRE</p>
                <div class="w-full relative h-11 mt-1 mb-4">
                    {{-- {{ $seccion }} --}}
                    <input class="w-full block border rounded-md border-color-gray--BK form-input h-full" type="text" wire:model="seccion.nombre">
                </div>

                {{-- <div class="w-full block text-left">
                    <label for="activo" class="inline-flex items-center min-w-max">
                        <input id="activo" class="mr-2 inline-block h-6 w-6 rounded-sm my-2 form-checkbox" type="checkbox" wire:model="seccion.activo">
                        <span class="w-auto color-gray--bk uppercase font-rob-light select-none">Activo</span>
                    </label>
                </div> --}}
                <label class="flex self-start mt-6">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="seccion.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                </label>
                <div class="w-full pt-6">
                    <p class="inline-block color-gray--bk font-rob-light uppercase">GRADO</p>
                    <div class="w-full relative h-auto">
                        {{-- <span>Niveles: {{ json_encode($niveles) }}</span> --}}
                        @foreach ($niveles as $id => $nivel)
                            <div class="relative flex">
                                <span class="w-full block border rounded-md border-color-gray--BK form-input h-11 mb-3">{{ $nivel }}</span>
                                {{-- <button wire:click="quitarNivel({{ $id }})">Quitar</button> --}}
                                <div class="w-11 h-11 bg-redBT absolute -right-12 bottom-3 rounded-md cursor-pointer" wire:click="quitarNivel({{ $id }})">
                                    <div class="h-1 w-4 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                                    {{-- <div class="h-4 w-1 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div> --}}
                                </div>
                            </div>

                        @endforeach
                        <input class="w-full block border rounded-md border-color-gray--BK form-input h-11 mb-3" wire:model="nuevo_nivel" type="text">
                        <div class="w-11 h-11 bg-greenBT absolute -right-12 bottom-0 rounded-md cursor-pointer" wire:click="guardarNivel">
                            <div class="h-1 w-4 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                            <div class="h-4 w-1 bg-white absolute m-auto top-0 bottom-0 left-0 right-0"></div>
                        </div>
                    </div>
                </div>
                @foreach ($errors->toArray() as $rule => $error_descriptions)
                    @foreach ($error_descriptions as $error)
                        <h5 class="text-red-500">{{ $error }}</h5>
                    @endforeach
                @endforeach
                <div class="w-full flex justify-center pt-12">
                    <button class="block py-3 px-6 rounded-md relative background-green--bk text-white uppercase font-rob-bold w-60 max-w-full" type="submit" wire:click="$emit('save')">Guardar</button>
                </div>
            </div>
        </form>
    </div>
    <div class="w-full flex justify-center items-center pt-8">
        <a href="/admin/colegios_editando/secciones/{{$colegio->id}}" class="w-1/6 p-3 relative flex justify-center items-center bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</a>
    </div>
</div>
