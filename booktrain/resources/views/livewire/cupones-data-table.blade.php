<x-livewire-tables::table.cell>
    {{ $row->codigo }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->status }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
@if($row->caducidad)
    {{ $row->caducidad->format('d/m/Y') }}
@else N/A @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->valor }}%
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
@if($row->pedido)
{{ $row->pedido->serie }}-{{ $row->pedido->folio }}
@else N/A @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->created_at->format('d/m/Y') }}
</x-livewire-tables::table.cell>
