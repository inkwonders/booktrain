<x-livewire-tables::table.cell>
@if($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO')
    <span class'color-red--bk'>{{ $row->status }}</span>
@elseif ($row->status == 'PROCESADO')
    <span class='color-gray--bk'>{{ $row->status }}</span>
@elseif ( $row->status == 'PAGO EN REVISION')
    <span class='color-blue--bk'>{{ $row->status }}</span>
@else
    <span class='color-green--bk'>{{ $row->status }}</span>
@endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{$row->serie }}{{$row->folio}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->created_at->format("d / m / Y") }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<div class="px-4 w-max">
    <div class="relative inline-block h-auto cursor-pointer w-14" wire:click="modalEnvioParcial({{ $row->id }})">
        <img src="{{ asset('img/svg/caja_cantidad.svg') }}" class="w-full">
        <div class="absolute bottom-0 left-0 right-0 flex items-center justify-center top-2 text-grayBT font-special">
            <p>Devolver</p>
            @if($row->libros_devueltos > 0)
            <span class="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">{{ $row->libros_devueltos }}</span>
            @endif
        </div>
    </div>
</div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>
