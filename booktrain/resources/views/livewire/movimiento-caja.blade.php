<div class="w-full flex flex-col relative h-full min-h-custom border rounded-md border-color-gray--BK">
    <div class="w-full h-full min-h-custom relative flex flex-col lg:px-16">
        <div class="w-full min-h-custom relative flex lg:flex-row flex-col items-center box-border overflow-hidden">
            <div class="w-full h-full relative flex flex-col p-6">
                <label class="color-green--bk font-rob-bold text-2xl">@if($this->caja->fecha_apertura == null) APERTURA DE CAJA @else CIERRE DE CAJA @endif | {{ $colegio->nombre }}</label>

                <div class="w-full h-full relative flex flex-row">
                    <div class="relative w-1/2 h-full flex justify-center items-center">
                        <img src="{{ asset('img/svg/colegios/monedas.svg') }}" class="w-1/3 relative" alt="">
                    </div>
                    <div class="border-r border-color-gray--BK separacion mt-14"></div>
                    <form class="relative w-1/2 h-full flex flex-row justify-center">
                        <div class="relative w-2/3 mt-14 h-full flex flex-col" wire:submit.prevent="save">
                            <label class="color-blue--bk font-rob-bold text-2xl m-6">Billetes</label>
                            <div class="w-full h-1/2 relative flex flex-wrap">
                                <label class="w-5/12 relative mx-6">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$20</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_20" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>

                                <label class="w-5/12 relative">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$50</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_50" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>

                                <label class="w-5/12 relative mx-6">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$100</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_100" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>

                                <label class="w-5/12 relative">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$200</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_200" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>

                                <label class="w-5/12 relative mx-6">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$500</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_500" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>

                                <label class="w-5/12 relative">
                                    <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">$1000</label>
                                    <input wire:model.debounce.1000ms="operacion_caja.billete_1000" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                                </label>
                                <div class="w-full color-gray--bk relative self-start font-rob-bold uppercase mx-6 mt-6">
                                    @if($caja->status == 'ABIERTA')
                                        <span class="color-gray--bk font-rob-bold">OBSERVACIONES:</span>
                                        <textarea wire:model.debounce.1000ms="operacion_caja.observaciones" class="w-full h-20 resize-none border border-gray-200 focus:border-gray-200 text-gray-700 rounded-md p-2 focus:outline-none focus:ring-0" id=""></textarea>
                                    @endif
                                </div>
                                <div class="w-1/2 color-gray--bk relative self-start font-rob-bold uppercase mx-6 mt-6">
                                    <span class="">Total: ${{ number_format($operacion_caja->total, 2) }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="relative w-1/3 mt-14 h-full flex flex-col">
                            <label class="color-blue--bk font-rob-bold text-2xl my-6">Monedas</label>

                            <label class="w-full relative">
                                <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">Cantidad</label>
                                <input wire:model.debounce.1000ms="operacion_caja.conteo_monedas" type="number" min="1" step="1" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                            </label>

                            <label class="w-full relative">
                                <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">Monto</label>
                                <input wire:model.debounce.1000ms="operacion_caja.monto_monedas" type="number" min="1" step="any" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none"/>
                            </label>

                        </div>


                    </form>

                </div>


                <div class="w-full relative justify-center items-center flex flex-col mt-4">
                    <a wire:click="save" class="w-1/6 mt-4 relative flex justify-center uppercase rounded-md p-3 cursor-pointer background-green--bk text-white  font-rob-bold items-center "><div>REGISTRAR</div></a>
                    <a href="" class="w-1/6 mt-4 relative flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk p-3 cursor-pointer"><div>Regresar</div></a>
                </div>
            </div>

        </div>
    </div>
</div>
