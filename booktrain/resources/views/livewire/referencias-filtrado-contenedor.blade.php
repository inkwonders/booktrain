<div class="flex flex-col items-center p-10 relative w-full h-auto justify-start">
    <div class="relative flex flex-col justify-between w-full mb-4 pb-4">
        <h2 class="color-red--bk font-rob-bold uppercase text-xl">Referencias BBVA</h2>
        <div class="relative w-full items-center justify-between my-4 flex">
            <div class="w-auto relative flex">
                <input name="inicio" id="inicio" placeholder="FECHA INICIAL" class="relative w-40 mr-2 p-2 border color-gray--bk rounded-md" type="text">
                <input name="fin" id="fin" placeholder="FECHA FINAL" class="relative w-40 mr-2 p-2 border color-gray--bk rounded-md" type="text">
                <a onclick="emit_fechas()" class="w-40 relative bg-greenBT hover:bg-white text-white hover:text-greenBT border border-greenBT font-rob-bold h-11 flex justify-center items-center rounded-md cursor-pointer">BUSCAR</a>
            </div>
            <a href="/referencia_bbva" class="w-56 min-w-max relative background-blue--bk text-white font-rob-bold h-11 flex justify-center items-center rounded-md cursor-pointer">+ CARGAR REFERENCIAS</a>
        </div>
    </div>
    <div class="w-full data-table-diseño">
        <livewire:referencias-filtrado/>
    </div>
    <div class="w-full h-10"></div>
</div>
