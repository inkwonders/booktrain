<section class="flex flex-col w-full h-auto px-2 py-4">
    @hasrole('Admin|Super Admin')
        @if ($modal_reorden)
            <div>
                <div class="fixed inset-0 z-10 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true"
                    name="{{ $pedido_modal->id }}" id="{{ $pedido_modal->id }}">

                    <div class="items-end justify-center block min-h-screen px-4 pb-20 text-center sm:p-0 pt-28 sm:pt-28">
                        <div class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75 -z-1" aria-hidden="true">
                        </div>
                        <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
                        <div
                            class="inline-block w-10/12 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle max-w-7xl">
                            <form action="/reporte_pedidos/reporte_general_status/" method="GET"
                                class="relative items-center w-full form-login" id="form-login" name="form-login">
                                <div class="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                                    <div class="pb-4 sm:flex sm:items-start">
                                        <div class="w-full mt-3 text-center sm:mt-0 sm:px-4 sm:text-left">
                                            <h2 class="pb-4 swal2-title" id="modal-title">
                                                {{ $pedido_modal->serie }}{{ $pedido_modal->folio }}
                                            </h2>
                                            <div class="w-full">

                                                <div class="w-full border-b-2"></div>
                                                <div class="w-full border-b-2">


                                                    <div class="flex mt-6 flex-column">
                                                        <p
                                                            class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">
                                                            Datos generales 3</p>
                                                    </div>

                                                    <div class="flex justify-between">
                                                        <div class="mt-4 w-max">
                                                            <span
                                                                class="text-base uppercase color-gray--bk font-rob-bold">nombre:
                                                            </span>
                                                            <span class="text-base uppercase color-gray--bk">
                                                                {{ $pedido_modal->nombre_contacto }} </span>
                                                        </div>
                                                        <div class="mt-4 w-max">
                                                            <span
                                                                class="text-base uppercase color-gray--bk font-rob-bold">apellido(s):
                                                            </span>
                                                            <span class="text-base uppercase color-gray--bk">
                                                                {{ $pedido_modal->apellidos_contacto }} </span>
                                                        </div>
                                                        <div class="mt-4 w-max">
                                                            <span
                                                                class="text-base uppercase color-gray--bk font-rob-bold">Celular:
                                                            </span>
                                                            <span class="text-base uppercase color-gray--bk">
                                                                {{ $pedido_modal->celular_contacto }} </span>
                                                        </div>

                                                    </div>
                                                    <div class="mt-4 w-max">
                                                        <span
                                                            class="text-base uppercase color-gray--bk font-rob-bold">Correo
                                                            para recepción de licencias digitales: </span>
                                                        <span class="text-base color-gray--bk">
                                                            {{ $pedido_modal->correo_contacto == null ? '---' : $pedido_modal->correo_contacto }}
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="w-full border-b-2">
                                                    <div class="flex mt-6 flex-column">
                                                        <p
                                                            class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">
                                                            ENTREGA</p>
                                                    </div>
                                                    <div class="flex mt-4 flex-column">
                                                        <span class="text-base uppercase color-gray--bk">
                                                            @if ($pedido_modal->tipo_entrega == 1)
                                                                <span class="font-rob-bold">COLEGIO:</span>
                                                            @elseif ($pedido_modal->tipo_entrega == 2)
                                                                <span class="font-rob-bold">DOMICILIO:</span>
                                                            @elseif ($pedido_modal->tipo_entrega == 3)
                                                                <span class="font-rob-bold">PICKUP:</span>
                                                            @else
                                                                <span class="font-rob-bold">DOMICILIO:</span>
                                                            @endif
                                                            {{ is_null($pedido_modal->direccion) ? '' : $pedido_modal->direccion->direccion_completa }}
                                                        </span>
                                                        <span class="text-base uppercase color-gray--bk"></span>
                                                    </div>
                                                    <div class="flex mt-4 flex-column">
                                                        <span class="text-base color-gray--bk ">
                                                            @if ($pedido_modal->tipo_entrega == 2)
                                                                @if ($pedido_modal->envio != null)
                                                                    @if ($pedido_modal->envio->paqueteria()->first() != null)
                                                                        <span class="font-rob-bold">Guia:</span>
                                                                        {{ $pedido_modal->envio->guia }}</br>
                                                                        <span class="font-rob-bold">Paquetería:</span>
                                                                        {{ $pedido_modal->envio->paqueteria()->first()->nombre }}</br>
                                                                        <span class="font-rob-bold">Url: </span><a
                                                                            target="_blank"
                                                                            href='{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url : '' }}'>{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url : '' }}</a></br>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                            @if (!empty($envios_parciales))
                                                                @foreach ($envios_parciales as $envio)
                                                                    <span class="font-rob-bold">Guia:</span>
                                                                    {{ $envio['guia'] }}</br>
                                                                    <span class="font-rob-bold">Paquetería:</span> <a
                                                                        target="_blank"
                                                                        href='{{ $envio->paqueteria()->first() != null ? $envio->paqueteria()->first()->url : '' }}'
                                                                        class="text-base color-gray--bk ">{{ $envio->paqueteria()->first() != null ? $envio->paqueteria()->first()->nombre : '' }}</a></br>
                                                                @endforeach
                                                            @endif
                                                        </span>
                                                        <span class="text-base uppercase color-gray--bk"></span>
                                                    </div>
                                                    <div class="mt-4 mb-4 text-left">
                                                    </div>
                                                </div>
                                                @if (!is_null($pedido_modal->datosFactura))
                                                    <div class="w-full border-b-2">
                                                        <div class="flex mt-6 flex-column">
                                                            <p
                                                                class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">
                                                                datos de facturación del último pedido</p>
                                                        </div>

                                                        <table
                                                            class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                                                            <thead>
                                                                <tr class="font-bold uppercase align-top">
                                                                    <th class="pb-4 pr-4">Nombre o razón social</th>
                                                                    <th class="pb-4 pr-4">RFC</th>
                                                                    <th class="pb-4 pr-4">Correo</th>
                                                                    <th class="pb-4 pr-4">Uso de CFDI</th>
                                                                    <th class="pb-4 pr-4">método de pago</th>
                                                                    <th class="pb-4 pr-4">Forma de pago</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="align-top">
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->razon_social }}
                                                                    </td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->rfc }}</td>
                                                                    <td class="pr-4 uppercase">
                                                                        {{ $pedido_modal->datosFactura->correo }}</td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->cfdis->descripcion }}
                                                                    </td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->metodo_pago }}
                                                                    </td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->forma_pago }}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table
                                                            class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                                                            <thead>
                                                                <tr class="font-bold uppercase align-top">
                                                                    <th class="pb-4 pr-4">Calle</th>
                                                                    <th class="pb-4 pr-4">Num. exterior</th>
                                                                    <th class="pb-4 pr-4">Num. interior</th>
                                                                    <th class="pb-4 pr-4">Colonia</th>
                                                                    <th class="pb-4 pr-4">Municipio</th>
                                                                    <th class="pb-4 pr-4">Estado</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="align-top">
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->calle }}</td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->num_exterior }}
                                                                    </td>
                                                                    <td class="pr-4 uppercase">
                                                                        {{ $pedido_modal->datosFactura->num_interior }}
                                                                    </td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->colonia }}</td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->municipio }}</td>
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->estado }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <table
                                                            class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk ">
                                                            <thead>
                                                                <tr class="font-bold uppercase align-top">
                                                                    <th class="pb-4 pr-4">Codigo postal</th>
                                                                    <th class="pb-4 pr-4">Regimen Fiscal</th>
                                                                    <th class="pb-4 pr-4 ">Estatus </th>
                                                                    <th class="invisible pb-4 pr-4">Regimen</th>
                                                                    <th class="invisible pb-4 pr-4">Regimen</th>
                                                                    <th class="invisible pb-4 pr-4">Regimen</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="align-top">
                                                                    <td class="pr-4">
                                                                        {{ $pedido_modal->datosFactura->cp }}</td>
                                                                    <td class="pr-4">
                                                                        @switch($pedido_modal->datosFactura->regimen_fiscal)

                                                                            @case(601)

                                                                                601 - General de Ley Personas Morales

                                                                            @break

                                                                            @case(603)

                                                                                603 - Personas Morales con Fines no Lucrativos

                                                                            @break

                                                                            @case(605)

                                                                                605 - Sueldos y Salarios e Ingresos Asimilados a Salarios

                                                                            @break

                                                                        @endswitch
                                                                    </td>
                                                                    <td class="pr-4">


                                                                            @if (!empty($pedido_modal->facturama_invoice_id ))

                                                                                @if ($pedido_modal->facturama_status != 'active')

                                                                                    No timbrado con los siguientes errores:<br>

                                                                                    @php

                                                                                        $errores_facturama = explode(";", $pedido_modal->datosFactura->facturama_error );

                                                                                    @endphp

                                                                                    @foreach ($errores_facturama  as $error )

                                                                                        {{ $error }} <br>

                                                                                    @endforeach

                                                                                @else

                                                                                    Timbrado

                                                                                @endif

                                                                            @else

                                                                                No timbrado

                                                                            @endif


                                                                    </td>
                                                                    <td class="pr-4">
                                                                    </td>
                                                                    <td class="pr-4">
                                                                    </td>
                                                                    <td class="pr-4">
                                                                    </td>

                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <div class="pb-4 mt-4 lg:hidden">
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Nombre o razón social</div>
                                                            <div class="text-base uppercase color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->razon_social }}</div>
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                RFC</div>
                                                            <div class="text-base uppercase color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->rfc }}</div>
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Regimen Fiscal</div>
                                                            <div class="text-base uppercase color-gray--bk break-word">

                                                                @switch($pedido_modal->datosFactura->regimen_fiscal)

                                                                    @case(601)

                                                                        601 - General de Ley Personas Morales

                                                                    @break

                                                                    @case(603)

                                                                        603 - Personas Morales con Fines no Lucrativos

                                                                    @break

                                                                    @case(605)

                                                                        605 - Sueldos y Salarios e Ingresos Asimilados a Salarios

                                                                    @break

                                                                @endswitch

                                                            </div>
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Correo</div>
                                                            <div class="text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->correo }}</div>
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Uso de CFDI</div>
                                                            <div class="text-base uppercase color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->cfdis->descripcion }}
                                                            </div>
                                                            <div
                                                                class="text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                método de pago</div>
                                                            <div class="text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->metodo_pago }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Forma de pago</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->forma_pago }}</div>

                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Calle</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->calle }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Num. exterior</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->num_exterior }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Num. interior</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->num_interior }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Colonia</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->colonia }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Municipio</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->municipio }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Estado</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->estado }}</div>
                                                            <div
                                                                class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">
                                                                Codigo postal</div>
                                                            <div class="col-span-2 text-base color-gray--bk break-word">
                                                                {{ $pedido_modal->datosFactura->cp }}</div>

                                                        </div>
                                                    </div>
                                                @endif
                                                @foreach ($pedido_modal->resumenes as $resumen_pedido_alumno)
                                                    <div class="flex flex-col w-full lg:flex-row">
                                                        <div
                                                            class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">
                                                            <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">
                                                                <span
                                                                    class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                                                                <span
                                                                    class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido_modal->colegio->nombre }}
                                                                </span>
                                                                <span
                                                                    class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado:
                                                                </span>
                                                                <span
                                                                    class="col-span-4 text-base uppercase color-gray--bk">
                                                                    {{ $resumen_pedido_alumno->paquete->nivelColegio->nombre }}
                                                                    -
                                                                    {{ $resumen_pedido_alumno->paquete->nivelColegio->seccion->nombre }}
                                                                </span>
                                                            </div>
                                                            <div class="w-full mt-4 text-left">
                                                                <span
                                                                    class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE
                                                                    DEL ALUMNO:* </span>
                                                            </div>
                                                            <div class="w-full mt-2 mb-5 text-left">
                                                                <span
                                                                    class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_alumno . ' ' . $resumen_pedido_alumno->paterno_alumno . ' ' . $resumen_pedido_alumno->materno_alumno }}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="lg:w-2/3 lg:border-b-2">
                                                            <div
                                                                class="flex flex-col mt-10 mb-6 lg:grid lg:grid-cols-12 lg:gap-4">
                                                                @foreach ($resumen_pedido_alumno->detallesResumenPedidos as $detalles_resumen)
                                                                    <span
                                                                        class="col-span-7 text-base text-left color-gray--bk lg:text-right">
                                                                        <span
                                                                            class="col-span-1 text-base text-left color-gray--bk">
                                                                            @if ($detalles_resumen->devuelto == 1)
                                                                                <span
                                                                                    class="inline-flex items-center justify-center px-2 py-1 mr-2 text-xs font-bold leading-none text-red-100 bg-red-600 rounded-full">Devuelto</span>
                                                                            @endif
                                                                        </span>
                                                                        {{ $detalles_resumen->libro->nombre }}
                                                                        {{ $detalles_resumen->cantidad > 1 ? ' (' . $detalles_resumen->cantidad . ')' : '' }}
                                                                    </span>
                                                                    <span
                                                                        class="col-span-2 text-base text-left color-gray--bk"><label
                                                                            class="text-base text-gray-500 lg:hidden">ISBN
                                                                        </label>{{ $detalles_resumen->libro->isbn }}</span>
                                                                    <span
                                                                        class="flex justify-between w-full col-span-2 text-base text-right color-green--bk lg:inline-block rob-light font-extralight">
                                                                        <label
                                                                            class="text-base text-gray-500 lg:hidden">PRECIO</label>
                                                                        ${{ number_format($detalles_resumen->precio_libro, 2) }}
                                                                        MXN </span>
                                                                    <div class="w-full my-4 border-b-2 lg:hidden"></div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <div class="flex w-full">
                                                    <div class="hidden lg:w-1/3 lg:block">
                                                    </div>
                                                    <div class="w-full lg:w-2/3">
                                                        <div
                                                            class="grid grid-cols-11 mt-10 mb-6 lg:gap-4 font-rob-regular">
                                                            @php
                                                                $subtotal = $pedido_modal->resumenes->sum('subtotal');
                                                                $guia_pedido = $pedido_modal->envio;
                                                                $costo_envio = 0;
                                                                if ($guia_pedido) {
                                                                    $costo_envio = $guia_pedido->costo;
                                                                }
                                                                //$total = ($subtotal + $costo_envio) + $pedido_modal->comision; //* 1.16;
                                                                $total = $pedido_modal->total;
                                                            @endphp
                                                            <span
                                                                class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">SUBTOTAL</span>
                                                            <span
                                                                class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3">
                                                                ${{ number_format($subtotal, 2) }} MXN </span>
                                                            <span
                                                                class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">ENVIO</span>
                                                            @if ($pedido_modal->tipo_entrega == 2 && !is_null($guia_pedido))
                                                                <span
                                                                    class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3">
                                                                    ${{ number_format($guia_pedido->costo, 2) }} MXN
                                                                </span>
                                                            @else
                                                                <span
                                                                    class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3">${{ number_format(0, 2) }}
                                                                    MXN</span>
                                                            @endif
                                                            <span
                                                                class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">COMISIÓN
                                                                DE PAGO</span>
                                                            <span
                                                                class="col-span-5 text-right color-green--bk lg:text-md text-md lg:col-span-3 rob-regular">
                                                                ${{ number_format($pedido_modal->comision, 2) }} MXN
                                                            </span>
                                                            <span
                                                                class="col-span-6 text-right color-gray--bk lg:text-md text-md lg:col-span-8 font-rob-bold">TOTAL</span>
                                                            <span
                                                                class="col-span-5 font-bold text-right lg:text-md text-md lg:col-span-3 rob-bold">
                                                                ${{ number_format($total, 2) }} MXN </span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-full pt-6">
                                        <div class="flex items-center justify-center w-full">
                                            <button type="button" style="display: inline-block;"
                                                wire:click="$set('modal_reorden', null)"
                                                class=" swal2-confirm swal2-styled">Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endhasrole
</section>
