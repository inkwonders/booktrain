<div class="w-full">
    <div class="w-full">

        <div class="h-11 w-60 relative overflow-hidden border rounded border-gray-200 cursor-pointer">
            <label class="select_motivos" for="select_colegio"></label>

            <select class="w-full h-full p-2 pr-8 appearance-none cursor-pointer text-grayBT" id="select_colegio" wire:model="colegio">
            <option value="0">Seleccione un colegio</option>
            @foreach ($colegios as $colegio)
                <option value="{{ $colegio->id }}">{{ $colegio->nombre }}</option>
            @endforeach
            </select>

        </div>
    </div>
    <br>
    <div class="w-full">
        @livewire('libros-colegio')
    </div>
</div>
