<div class="relative flex flex-col w-full p-10 border rounded-md min-h-custom border-color-gray--BK">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="text-xl font-bold color-green--bk">NUEVO LIBRO</h2>
        <a href="libros" class="flex items-center justify-center w-1/6 text-white rounded-md cursor-pointer background-blue--bk font-rob-bold h-11">LISTA DE LIBROS</a>
    </div>
    <div class="relative flex flex-row w-full h-2/3">
        <div class="relative flex items-center justify-center w-1/2">
            <img class="relative w-1/2" src="/assets/img/svg/libros_admin.svg" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="relative flex flex-col items-center justify-center w-1/2">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center w-1/2">
                <label class="self-start mb-6 text-lg uppercase color-blue--bk font-rob-bold" for="">Nuevo libro</label>
                <label class="relative self-start color-gray--bk font-rob-light" for="isbn">ISBN</label>
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" type="text" id="isbn" wire:model="libro.isbn">
                <label class="relative self-start color-gray--bk font-rob-light" for="nombre">NOMBRE</label>
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" type="text" id="nombre" wire:model="libro.nombre">

                <label class="flex self-start my-4 ml-4">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="libro.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">Activo</span>
                </label>
                @include('errors.validation')
                <button class="relative w-1/2 h-12 m-4 text-white uppercase rounded-md background-green--bk font-rob-bold" type="submit">Guardar</button>
            </form>

        </div>
    </div>
    <div class="relative flex items-center justify-center w-full mt-4"><a href="/admin/libros" class="relative flex items-center justify-center w-1/6 p-3 uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk"><div>Regresar</div></a></div>
</div>
