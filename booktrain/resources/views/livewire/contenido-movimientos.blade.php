<x-livewire-tables::table.cell>
    <p>{{ $row->created_at != null ? (date_format($row->created_at,"d / m / Y")) : '' }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p class="whitespace-normal">{{ $row->tipo->descripcion }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p class="whitespace-normal">{{ $row->usuario->name." ".$row->usuario->apellidos }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p>{{ $row->cantidad }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p class="whitespace-normal">{{ $row->libro->nombre }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p class="whitespace-normal">{{ $row->comentarios }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p class="whitespace-normal">{{ $row->descripcion }}</p>
</x-livewire-tables::table.cell>
