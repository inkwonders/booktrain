<x-livewire-tables::table.cell>
{{ $row->codigo }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
{{ $row->descripcion }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
{{ $row->proveedor ? $row->proveedor : 'N/A' }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
{{ $row->formasPago->unique()->count() }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
{{ $row->pedidos->count() }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
@if($row->disponible)
<span class="text-green-700 text-bold">Si</span>
@else
<span class="text-red-700 text-bold">No</span>
@endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<span class="px-4 py-2 border border-gray-300 cursor-pointer" wire:click="editar({{ $row->id }})">Editar</span>
</x-livewire-tables::table.cell>

