<div class="pt-5">
    <span class="inline-block">Lista de niveles</span>
    <table class="border border-gray-200 w-full mt-3">
        <thead>
            <td></td>
            <td>Nombre</td>
            <td>Activo</td>
            <td>Paquetes</td>
            <td>Libros</td>
            <td></td>
        </thead>
        <tbody>
        @foreach ($niveles->reverse() as $nivel)
        <tr class="border border-gray-300">
            <td><button class="text-purple-500" wire:click="editNivel({{ $nivel->id }})">Editar</button></td>
            <td>{{ $nivel->nombre }}</td>
            <td>{{ $nivel->activo }}</td>
            <td>{{ $nivel->paquetes->count() }}</td>
            <td>{{ $nivel->paquetes->pluck('libros')->collapse()->unique()->count() }}</td>
            <td>
                <button class="text-red-500" wire:click="delete({{ $nivel->id }})">Eliminar</button>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
