{{-- Precio, obligatorio --}}
<div class="relative flex flex-col w-full p-6">
    <h2 class="p-4 text-xl font-bold color-green--bk">EDITAR LIBRO COLEGIO | {{ $colegio->nombre }}</h2>
    <div class="relative flex flex-row w-full h-2/3">
        <div class="relative flex items-center justify-center w-1/2">
            <img src="/assets/img/svg/colegios/libros.svg" class="relative w-1/3" alt="">
        </div>

        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="relative flex items-center justify-center w-1/2">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center justify-center w-1/2">
                <span class="my-6 text-center uppercase font-rob-bold color-blue--bk">{{ $libro['nombre'] }}</span>
                <label class="relative self-start uppercase color-gray--bk font-rob-light" for="titulo">Precio</label>
                <input type="text" class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none" id="titulo" wire:model="libro.precio"/>

                <label class="flex self-start mt-4 ml-4">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="libro.obligatorio">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">OBLIGATORIO</span>
                </label>
                <label class="flex self-start ml-4">
                    <input id="activo" class="custom-check--bk" type="checkbox" wire:model="libro.activo">
                    <label class="flex self-start custom-check-label--bk" for="activo">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">Activo</span>
                </label>

                <label class="flex self-start mb-4 ml-4">
                    <input id="two" class="custom-check--bk" type="checkbox" wire:model.defer="libro.bajo_pedido">
                    <label class="flex self-start custom-check-label--bk" for="two">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">BAJO PEDIDO</span>
                </label>

                @include('errors.validation')

                <button class="relative w-1/2 h-12 m-4 text-white uppercase rounded-md background-green--bk font-rob-bold" type="submit" >Guardar</button>
            </form>
        </div>

    </div>
    <div class="relative flex items-center justify-center w-full mt-4">
        <a href="{{ route('admin.colegios.libros', $colegio->id ) }}" class="relative flex items-center justify-center w-1/6 p-3 uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk">
            <div>Regresar</div>
        </a>
    </div>
</div>
