@if($modal_devoluciones)
<div class="fixed top-0 bottom-0 left-0 right-0 z-20 p-12">
   <div class="flex items-start justify-center w-full h-full overflow-x-hidden overscroll-y-auto">
       <div class="absolute top-0 bottom-0 left-0 right-0 z-0 bg-gray-500 opacity-50" wire:click="$set('modal_devoluciones', false)"></div>
       <div class="relative z-10 flex flex-col items-center justify-between w-full h-auto max-w-6xl p-10 pt-5 bg-white border rounded-lg border-grayBT">
           <p class="text-xl font-bold text-grayBT">{{ $pedido_modal->serie }}-{{ $pedido_modal->folio }}</p>
           <div class="flex items-center justify-between pb-14">

           <div class="w-full">
              <div class="w-full border-b-2"></div>
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>
                 </div>
                 <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">nombre: </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->nombre_contacto }} </span>
                    </div>
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">apellido(s): </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->apellidos_contacto }} </span>
                    </div>
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Celular: </span>
                       <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->celular_contacto }} </span>
                    </div>
                 </div>
              </div>
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">ENTREGA</p>
                 </div>
                 <div class="flex mt-1 flex-column">
                    <span class="text-base uppercase color-gray--bk">
                    @if ($pedido_modal->tipo_entrega == 1)
                    <span class="font-rob-bold">COLEGIO:</span>
                    @elseif (($pedido_modal->tipo_entrega == 2))
                    <span class="font-rob-bold">DOMICILIO:</span>
                    @elseif (($pedido_modal->tipo_entrega == 3))
                    <span class="font-rob-bold">PICKUP:</span>
                    @else
                    <span class="font-rob-bold">DOMICILIO:</span>
                    @endif
                    {{ is_null($pedido_modal->direccion)? '' : $pedido_modal->direccion->direccion_completa }}
                    </span>
                    <span class="text-base uppercase color-gray--bk"></span>
                 </div>
                 <div class="flex mt-4 flex-column">
                    <span class="text-base color-gray--bk ">
                        @if ($pedido_modal->tipo_entrega == 2)
                            @if ($pedido_modal->envio->paqueteria()->first() != null)
                                <span class="font-rob-bold">Guia:</span> {{ $pedido_modal->envio->guia }}</br>
                                <span class="font-rob-bold">Paquetería:</span> {{ $pedido_modal->envio->paqueteria()->first()->nombre }}</br>
                                <span class="font-rob-bold">Url: </span><a target="_blank" href='{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}'>{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}</a></br>
                            @else

                            @endif
                        @else

                        @endif
                    </span>
                    <span class="text-base uppercase color-gray--bk"></span>
                </div>

              </div>
              @if (isset($pedido_modal->datosFactura ))
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">datos de facturación del último pedido</p>
                 </div>
                 <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Nombre o razón social</th>
                            <th class="pb-4 pr-4">RFC</th>
                            <th class="pb-4 pr-4">Correo</th>
                            <th class="pb-4 pr-4">Uso de CFDI</th>
                            <th class="pb-4 pr-4">método de pago</th>
                            <th class="pb-4 pr-4">Forma de pago</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->razon_social }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->rfc }}</td>
                            <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->correo }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->metodo_pago }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->forma_pago }}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Calle</th>
                            <th class="pb-4 pr-4">Num. exterior</th>
                            <th class="pb-4 pr-4">Num. interior</th>
                            <th class="pb-4 pr-4">Colonia</th>
                            <th class="pb-4 pr-4">Municipio</th>
                            <th class="pb-4 pr-4">Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->calle }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->num_exterior }}</td>
                            <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->num_interior }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->colonia }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->municipio }}</td>
                            <td class="pr-4">{{ $pedido_modal->datosFactura->estado }}</td>
                        </tr>
                    </tbody>
                </table>

                <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                    <thead>
                        <tr class="font-bold uppercase align-top">
                            <th class="pb-4 pr-4">Codigo postal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="align-top">
                            <td class="pr-4">{{ $pedido_modal->datosFactura->cp }}</td>
                        </tr>
                    </tbody>
                </table>

                 <div class="pb-4 mt-1 lg:hidden">
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->razon_social }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->rfc }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
                    <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->correo }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
                    <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</div>
                    <div class="text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
                    <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->metodo_pago }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->forma_pago }}</div>

                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Calle</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->calle }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. exterior</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_exterior }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. interior</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_interior }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colonia</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->colonia }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Municipio</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->municipio }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Estado</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->estado }}</div>
                    <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Codigo postal</div>
                    <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->cp }}</div>

                 </div>
              </div>
              @endif
              @foreach($pedido_modal->resumenes as $resumen_pedido_alumno)
              <div class="flex flex-col w-full lg:flex-row">
                 <div class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">
                    <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">
                       <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                       <span class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido_modal->colegio->nombre }} </span>
                       <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado: </span>
                       <span class="col-span-4 text-base uppercase color-gray--bk"> {{ $resumen_pedido_alumno->paquete->nivel->nombre }} - {{ $resumen_pedido_alumno->paquete->nivel->seccion->nombre }} </span>
                    </div>
                    <div class="w-full mt-1 text-left">
                       <span class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE DEL ALUMNO:</span>
                    </div>
                    <div class="w-full mt-1 mb-5 text-left">
                       <span class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_completo_alumno }} </span>
                    </div>
                 </div>
                 <div class="lg:w-2/3 lg:border-b-2">
                    <div class="flex flex-col mt-10 mb-10 lg:grid lg:grid-cols-12 lg:gap-1">
                       @foreach ($libros_devueltos[ $resumen_pedido_alumno->id ] as $id => $detalle)

                        <span class="col-span-4 text-base text-left color-gray--bk lg:text-right">{{ $detalle['libro'] }}</span>
                        <span class="col-span-2 text-base text-left color-gray--bk"><label class="text-base text-gray-500 lg:hidden">ISBN </label>{{ $detalle['isbn'] }}</span>

                        @if($detalle['devuelto'] == 1)
                            <span class="col-span-4 text-base text-left color-gray--bk lg:text-right">Devuelto el {{ $detalle['fecha_devolucion'] }}
                            <br>({{ $detalle['causa_devolucion'] }})
                            </span>
                            <span class="col-span-2 text-base text-left color-gray--bk lg:text-right">${{ number_format($detalle['precio_libro'], 2) }} MXN</span>
                        @else
                            <div class="col-span-4 text-center">
                                <label class="switch">
                                    <input type='checkbox' wire:model="libros_devueltos.{{ $resumen_pedido_alumno->id }}.{{ $id }}.seleccionado">
                                    <span class='slider'></span>
                                </label>
                            </div>

                            @if($libros_devueltos[$resumen_pedido_alumno->id][$id]['seleccionado'])
                                @if($selectFaltaMaterial)
                                    <span class="col-span-2 text-base text-left color-gray--bk lg:text-right">$0.00 MXN</span>
                                @else
                                <span class="col-span-2 text-base text-left color-redBT lg:text-right">${{ number_format($detalle['comision_devolucion'], 2) }} MXN</span>
                                @endif
                            @else
                                <span class="col-span-2 text-base text-left color-greenBT lg:text-right ">${{ number_format($detalle['precio_libro'], 2) }} MXN</span>
                            @endif
                        @endif

                       @endforeach
                    </div>
                 </div>
              </div>

              @endforeach

           </div>

        </div>

        <div class="flex flex-col items-start justify-start w-full">

            <div class="grid items-center w-full grid-cols-3 gap-4 my-1">
                {{ $mensaje_devolucion }}
                <div class="">
                    <span class="text-base text-left uppercase color-gray--bk">La penalización por devolución es: </span><span class="text-base color-red--bk font-rob-regular">${{ number_format($this->comision_devolucion, 2) }}</span>
                </div>
                <div class=""></div>
                <div>
                    {{-- <label class="container relative flex w-full mt-2 ml-6 uppercase color-gray--bk">Devolución por falta de material
                        <input class="ml-1 " type="radio" name="selectFaltaMaterial" value="1" wire:model="selectFaltaMaterial">
                        <span class="checkmark"></span>
                    </label> --}}
                    {{-- <br> --}}
                    {{-- <label class="container relative flex w-full mt-2 ml-6 uppercase color-gray--bk">Devolución por parte del usuario
                        <input type="radio" name="selectFaltaMaterial" value="0" wire:model="selectFaltaMaterial">
                        <span class="checkmark"></span>
                    </label> --}}
                    <label class="flex items-center justify-end">
                        <span class="uppercase color-gray--bk font-rob-regular">Devolución por falta de material</span>
                        <div class="relative ml-2">
                        <input class="radioBt" type="radio" name="selectFaltaMaterial" value="1" wire:model="selectFaltaMaterial" />
                        <span class="fakeRadio"></span>
                        </div>
                     </label>

                    <br>

                    <label class="flex items-center justify-end">
                        <span class="uppercase color-gray--bk font-rob-regular">Devolución por parte del usuario</span>
                        <div class="relative ml-2">
                          <input class="radioBt" type="radio" name="selectFaltaMaterial" value="0" wire:model="selectFaltaMaterial" />
                          <span class="fakeRadio"></span>
                        </div>
                      </label>

                </div>
            </div>

            <div class="grid w-full grid-cols-2 gap-4 my-3">
                <div class="flex flex-col">
                    <span class="mb-1 text-base text-left uppercase color-gray--bk">Comentarios</span>
                    <div class="w-full">
                        <textarea type="textarea" wire:model="comentariosDevolucion" class="w-3/5 border border-gray-300 rounded-md resize-none"></textarea>
                    </div>
                </div>

            </div>

        </div>

        <div class=""></div>
        <div class="grid grid-cols-3 gap-4 mt-5">
            <div class="...">
                <button  class="@if($this->devolucion_disponible) bg-greenBT hover:border-greenBT hover:text-greenBT hover:bg-white @else bg-grayBT cursor-not-allowed @endif w-48 mb-2 text-xl font-bold text-white border border-transparent rounded h-11" wire:click="save">Aceptar</button>
            </div>
            <div class="...">

            </div>
            <div class="...">
                <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-grayBT h-11 hover:border-grayBT hover:bg-white hover:text-grayBT" wire:click="$set('modal_devoluciones', false)">Cancelar</button>
            </div>

          </div>

        </div>
    </div>
</div>
@endif
