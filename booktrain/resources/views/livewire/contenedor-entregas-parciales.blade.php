

<section class="flex flex-col items-center justify-center w-full px-2 py-4 min-h-custom">
   <div class="relative flex flex-col justify-start w-full h-auto p-10 border rounded-md min-h-interno border-color-gray--BK">
      <div class="relative flex flex-col justify-between w-full mb-1">
         <h2 class="text-xl font-bold color-red--bk">ENTREGAS</h2> {{ $modal_reorden }}
         <div class="relative flex flex-col items-center w-full">


            <div class="relative flex justify-between w-full py-4 mb-5">
                <div class="relative items-center w-auto ">
                    <input name="inicio"    id="inicio" required placeholder="FECHA INICIAL"   class="relative w-40 px-2 mr-2 border rounded-md h-11 color-gray--bk"    type="text">
                    <input name="fin"       id="fin"    required placeholder="FECHA FINAL"     class="relative w-40 px-2 mr-2 border rounded-md h-11 color-gray--bk"    type="text">
                    <button onclick="emit_fechas()" id="buscar" class="relative font-bold text-white border border-gray-400 border-solid rounded w-36 h-11 background-green--bk hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm">BUSCAR</button>
                </div>
                <a href="/pedidos_entregados" class="flex items-center justify-center text-white rounded-md cursor-pointer background-blue--bk font-rob-bold w-52 h-11">+ PEDIDOS ENTREGADOS</a>

            </div>
         </div>
      </div>

      <div class="data-table-diseño">
            <livewire:datatable-entregas-parciales  searchable="folio, nombre_contacto, apellidos_contacto, email" exportable />
      </div>
   </div>
   <br><br><br>

   <div>
    @if($modal_reorden)
    <div class="fixed top-0 bottom-0 left-0 right-0 z-20 p-12">
       <div class="flex items-start justify-center w-full h-full overflow-x-hidden overscroll-y-auto">
           <div class="absolute top-0 bottom-0 left-0 right-0 z-0 bg-gray-500 opacity-50" wire:click="cerrarModal"></div>
           <div class="relative z-10 flex flex-col items-center justify-between w-full h-auto max-w-6xl p-10 pt-5 bg-white border rounded-lg border-grayBT">
               <p class="text-xl font-bold text-grayBT">{{ $pedido_modal->serie }}{{ $pedido_modal->folio }}</p>
               <div class="flex items-center justify-between pb-20">

               <div class="w-full">
                  <div class="w-full border-b-2"></div>
                  <div class="w-full border-b-2">
                     <div class="flex mt-1 flex-column">
                        <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>
                     </div>
                     <div class="flex justify-between">
                        <div class="mt-1 w-max">
                           <span class="text-base uppercase color-gray--bk font-rob-bold">nombre: </span>
                           <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->nombre_contacto }} </span>
                        </div>
                        <div class="mt-1 w-max">
                           <span class="text-base uppercase color-gray--bk font-rob-bold">apellido(s): </span>
                           <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->apellidos_contacto }} </span>
                        </div>
                        <div class="mt-1 w-max">
                           <span class="text-base uppercase color-gray--bk font-rob-bold">Celular: </span>
                           <span class="text-base uppercase color-gray--bk"> {{ $pedido_modal->celular_contacto }} </span>
                        </div>
                     </div>
                  </div>
                  <div class="w-full border-b-2">
                     <div class="flex mt-1 flex-column">
                        <p class="w-1/2 text-base text-left uppercase color-blue--bk font-rob-bold">ENTREGA</p>
                     </div>
                     <div class="flex mt-1 flex-column">
                        <span class="text-base uppercase color-gray--bk">
                        @if ($pedido_modal->tipo_entrega == 1)
                        <span class="font-rob-bold">COLEGIO:</span>
                        @elseif (($pedido_modal->tipo_entrega == 2))
                        <span class="font-rob-bold">DOMICILIO:</span>
                        @elseif (($pedido_modal->tipo_entrega == 3))
                        <span class="font-rob-bold">PICKUP:</span>
                        @else
                        <span class="font-rob-bold">DOMICILIO:</span>
                        @endif
                        {{ is_null($pedido_modal->direccion)? '' : $pedido_modal->direccion->direccion_completa }}
                        </span>
                        <span class="text-base uppercase color-gray--bk"></span>
                     </div>
                     <div class="flex mt-4 flex-column">
                        <span class="text-base color-gray--bk ">
                            @if ($pedido_modal->tipo_entrega == 2)
                                 @if ($pedido_modal->envio != null)
                                    @if ($pedido_modal->envio->paqueteria()->first() != null)
                                        <span class="font-rob-bold">Guia:</span> {{ $pedido_modal->envio->guia }}</br>
                                        <span class="font-rob-bold">Paquetería:</span> {{ $pedido_modal->envio->paqueteria()->first()->nombre }}</br>
                                        <span class="font-rob-bold">Url: </span><a target="_blank" href='{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}'>{{ $pedido_modal->envio->paqueteria()->first() != null ? $pedido_modal->envio->paqueteria()->first()->url :'' }}</a></br>
                                    @else

                                    @endif
                                @else

                                @endif
                            @else

                            @endif
                        </span>
                        <span class="text-base uppercase color-gray--bk"></span>
                    </div>


                     <div class="mt-1 mb-1 text-left">
                     </div>
                  </div>
                  @if (!is_null($pedido_modal->datosFactura ))
                  <div class="w-full border-b-2">
                     <div class="flex mt-1 flex-column">
                        <p class="w-full text-base text-left uppercase color-blue--bk font-rob-bold lg:w-1/2">datos de facturación del último pedido</p>
                     </div>
                     <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                        <thead>
                            <tr class="font-bold uppercase align-top">
                                <th class="pb-4 pr-4">Nombre o razón social</th>
                                <th class="pb-4 pr-4">RFC</th>
                                <th class="pb-4 pr-4">Correo</th>
                                <th class="pb-4 pr-4">Uso de CFDI</th>
                                <th class="pb-4 pr-4">método de pago</th>
                                <th class="pb-4 pr-4">Forma de pago</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="align-top">
                                <td class="pr-4">{{ $pedido_modal->datosFactura->razon_social }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->rfc }}</td>
                                <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->correo }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->metodo_pago }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->forma_pago }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                        <thead>
                            <tr class="font-bold uppercase align-top">
                                <th class="pb-4 pr-4">Calle</th>
                                <th class="pb-4 pr-4">Num. exterior</th>
                                <th class="pb-4 pr-4">Num. interior</th>
                                <th class="pb-4 pr-4">Colonia</th>
                                <th class="pb-4 pr-4">Municipio</th>
                                <th class="pb-4 pr-4">Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="align-top">
                                <td class="pr-4">{{ $pedido_modal->datosFactura->calle }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->num_exterior }}</td>
                                <td class="pr-4 uppercase">{{ $pedido_modal->datosFactura->num_interior }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->colonia }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->municipio }}</td>
                                <td class="pr-4">{{ $pedido_modal->datosFactura->estado }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="hidden w-full h-auto mt-4 mb-4 text-base text-left align-top lg:table color-gray--bk">
                        <thead>
                            <tr class="font-bold uppercase align-top">
                                <th class="pb-4 pr-4">Codigo postal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="align-top">
                                <td class="pr-4">{{ $pedido_modal->datosFactura->cp }}</td>
                            </tr>
                        </tbody>
                    </table>

                     <div class="pb-4 mt-1 lg:hidden">
                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Nombre o razón social</div>
                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->razon_social }}</div>
                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">RFC</div>
                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->rfc }}</div>
                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Correo</div>
                        <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->correo }}</div>
                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">Uso de CFDI</div>
                        <div class="text-base uppercase color-gray--bk break-word">{{ $pedido_modal->datosFactura->cfdis->descripcion }}</div>
                        <div class="text-base uppercase color-gray--bk font-rob-bold break-word">método de pago</div>
                        <div class="text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->metodo_pago }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Forma de pago</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->forma_pago }}</div>

                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Calle</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->calle }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. exterior</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_exterior }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Num. interior</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->num_interior }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colonia</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->colonia }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Municipio</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->municipio }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Estado</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->estado }}</div>
                        <div class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Codigo postal</div>
                        <div class="col-span-2 text-base color-gray--bk break-word">{{ $pedido_modal->datosFactura->cp }}</div>

                     </div>
                  </div>
                  @endif
                  @foreach($pedido_modal->resumenes as $resumen_pedido_alumno)
                  <div class="flex flex-col w-full lg:flex-row">
                     <div class="flex flex-col w-full border-b-2 lg:w-1/3 lg:border-r-2">
                        <div class="grid grid-cols-6 gap-4 mt-5 text-left lg:mt-10">
                           <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold break-word">Colegio:</span>
                           <span class="col-span-4 text-base text-left uppercase color-gray--bk">{{ $pedido_modal->colegio->nombre }} </span>
                           <span class="col-span-2 text-base uppercase color-gray--bk font-rob-bold">Grado: </span>
                           <span class="col-span-4 text-base uppercase color-gray--bk"> {{ $resumen_pedido_alumno->paquete->nivelColegio->nombre }} - {{ $resumen_pedido_alumno->paquete->nivelColegio->seccion->nombre }} </span>
                        </div>
                        <div class="w-full mt-1 text-left">
                           <span class="text-base uppercase color-blue--bk font-rob-bold">NOMBRE DEL ALUMNO:* </span>
                        </div>
                        <div class="w-full mt-1 mb-5 text-left">
                           <span class="text-base uppercase color-gray--bk">{{ $resumen_pedido_alumno->nombre_alumno." ".$resumen_pedido_alumno->paterno_alumno." ".$resumen_pedido_alumno->materno_alumno }} </span>
                        </div>
                     </div>
                     <div class="lg:w-2/3 lg:border-b-2">
                        <div class="flex flex-col mt-10 mb-10 lg:grid lg:grid-cols-11 lg:gap-1">
                           @foreach ($resumen_pedido_alumno->detallesResumenPedidos as $detalles_resumen)
                           <span class="col-span-7 text-base text-left color-gray--bk lg:text-right">
                           {{ $detalles_resumen->libro->nombre }}
                           {{ ($detalles_resumen->cantidad > 1? " (".$detalles_resumen->cantidad.")" : "" ) }}
                           </span>
                           <span class="col-span-2 text-base text-left color-gray--bk"><label class="text-base text-gray-500 lg:hidden">ISBN </label>{{ $detalles_resumen->libro->isbn }}</span>


                           <label class='switch'>
                           <input id='switch_activo' name='switch_activo[]' type='checkbox'  @if ($detalles_resumen->entregado == 1)  disabled checked @endif wire:click="libros_entregar({{ $detalles_resumen->id }})">
                           <span class='slider'></span>
                           </label>
                           {{-- @endif --}}

                           <div class="w-full my-4 border-b-2 lg:hidden"></div>
                           @endforeach
                        </div>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
            <div class="grid grid-cols-3 gap-4">
                <div class="...">
                    <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-greenBT h-11 hover:border-greenBT hover:bg-white hover:text-greenBT" wire:click="save">Entregar</button>
                </div>
                <div class="...">

                </div>
                <div class="...">
                    <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-grayBT h-11 hover:border-grayBT hover:bg-white hover:text-grayBT" wire:click="cerrarModal">Cancelar</button>
                </div>

              </div>

            </div>
        </div>
   </div>
@endif
</div>
</section>
