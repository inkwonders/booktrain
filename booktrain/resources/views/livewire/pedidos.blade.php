{{-- Estatus --}}
<x-livewire-tables::table.cell>
    <span
        @if ($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO') class="color-red--bk" @elseif ($row->status == 'PROCESADO') class="color-gray--bk"  @elseif ($row->status == 'PAGO EN REVISION') class="color-blue--bk" @else class="color-green--bk" @endif>{{ $row->status }}</span>
</x-livewire-tables::table.cell>

{{-- Folio --}}
<x-livewire-tables::table.cell>
    {{ $row->serie }}{{ $row->folio }}
</x-livewire-tables::table.cell>

{{-- Fecha --}}
<x-livewire-tables::table.cell>

    @if (!empty($row->created_at))

        {{ $row->created_at != null ? date_format($row->created_at, 'd / m / Y') : '' }}

    @endif
</x-livewire-tables::table.cell>

{{-- Nombre --}}
<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
</x-livewire-tables::table.cell>

{{-- Correo --}}
<x-livewire-tables::table.cell>
    {{ $row->email_pedido == null ? $row->user->email : $row->email_pedido }}
</x-livewire-tables::table.cell>

{{-- Teléfono --}}
<x-livewire-tables::table.cell>
    {{ $row->celular_contacto }}
</x-livewire-tables::table.cell>

{{-- Factura --}}
<x-livewire-tables::table.cell>
    <span> {{ $row->factura ? 'Sí' : 'No' }} </span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @if ($row->factura == 1)


    @if ($row->facturama_invoice_id != null)

        @if ($row->facturama_status != 'active')

            No timbrado


        @else

            Timbrado

        @endif



    @else

        No Timbrado

    @endif

    @endif


</x-livewire-tables::table.cell>


{{-- Referencia Factura --}}
{{-- <x-livewire-tables::table.cell>

    @if ($row->factura)
        <span>
            <button wire:click="asignarReferenciaFactura({{ $row->id }})"
                @if ($row->referencia_factura == '') class="color-blue--bk" @endif>{{ $row->referencia_factura == '' ? '+ Agregar' : $row->referencia_factura }}</button>
        </span>

    @endif

</x-livewire-tables::table.cell> --}}

{{-- Referencia --}}
{{-- <x-livewire-tables::table.cell>
    {{ $row->referencia != null ? $row->referencia->referencia : '' }}
</x-livewire-tables::table.cell> --}}

{{-- Método de pago --}}
<x-livewire-tables::table.cell>
    {{ $row->metodo_pago }}
</x-livewire-tables::table.cell>

{{-- Forma de pago --}}
<x-livewire-tables::table.cell>
    {{ $row->forma_pago }}
</x-livewire-tables::table.cell>

{{-- Monto cobrado --}}
<x-livewire-tables::table.cell>
    ${{ number_format($row->total, 2) }}
</x-livewire-tables::table.cell>

{{-- Monto recibido --}}
<x-livewire-tables::table.cell>
    @if ($row->status == 'PAGADO')

        @if (!empty($row->pagos()->where('status', 'SUCCESS')->first()->amount))

        ${{ number_format($row->pagos()->where('status', 'SUCCESS')->first()->amount,2) }}

        @endif

    @endif
</x-livewire-tables::table.cell>

{{-- Colegio --}}
<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>

{{-- Fecha de pago --}}
<x-livewire-tables::table.cell>
    @if ($row->status == 'PAGADO')

    @if (!empty($row->pagos()->where('status', 'SUCCESS')->first()->created_at ))

        {{ $row->pagos()->where('status', 'SUCCESS')->first()->created_at }}

    @endif

    @endif
</x-livewire-tables::table.cell>

{{-- Entrega --}}
<x-livewire-tables::table.cell>
    <span>{{ $row->descripcion_tipo_entrega }}</span>
</x-livewire-tables::table.cell>

{{-- Cambio de estatus --}}
<x-livewire-tables::table.cell>
    <span>
        @switch($row->status)
            @case('PROCESADO')
                {{-- <button wire:click="modalCambioStatus({{ $row->id }})" class="w-56 p-2 font-bold border border-solid rounded border-greenBT focus:outline-none openModal background-white--bk text-greenBT hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Modificar</button> --}}
                <button wire:click="cambiarEstadoPedido({{ $row->id }})"
                    class="w-56 p-2 font-bold border border-solid rounded border-greenBT focus:outline-none openModal background-white--bk text-greenBT hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Modificar</button>
            @break

            @case('PAGADO')
                <button wire:click="cambiarEstadoPedido({{ $row->id }})"
                    class="w-56 p-2 font-bold border border-solid rounded text-redBT border-redBT background-white--bk hover:bg-redBT hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Cancelar</button>
                {{-- <button wire:click="modalActivarCancelar({{ $row->id }})" class="w-56 p-2 font-bold border border-solid rounded text-redBT border-redBT background-white--bk hover:bg-redBT hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Cancelar</button> --}}
            @break

            @case('CANCELADO')
                <button wire:click="cambiarEstadoPedido({{ $row->id }})"
                    class="w-56 p-2 font-bold border border-solid rounded border-greenBT background-white--bk text-greenBT hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Activar</button>
                {{-- <button wire:click="modalActivarCancelar({{ $row->id }})" class="w-56 p-2 font-bold border border-solid rounded border-greenBT background-white--bk text-greenBT hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Activar</button> --}}
            @break
        @endswitch
    </span>
</x-livewire-tables::table.cell>

{{-- Correo de confirmación --}}
<x-livewire-tables::table.cell>
    @if ($row->status == 'PAGADO')
        <span>
            <button wire:click="enviarCorreoConfirmacionPago({{ $row->id }})"
                title="Enviar correo de confirmación de pago"
                class="w-56 p-2 font-bold border border-solid rounded text-blueBT border-blueBT background-white--bk text-blue-1000 hover:background-blue--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border-blue-1000">Enviar</button>
        </span>
    @endif
</x-livewire-tables::table.cell>

{{-- Pedido --}}
<x-livewire-tables::table.cell>
    <div class="px-4 w-max">
        <button class="w-max hover:underline" wire:click="$emit('modalDetallesPedido', {{ $row->id }})">Ver
            detalles</button>
    </div>


</x-livewire-tables::table.cell>
