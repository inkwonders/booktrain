<div class="relative flex flex-col w-full p-10 h-custom">
    <h2 class="text-xl font-bold color-green--bk">LISTADO CON TODO EL CATÁLOGO DE LIBROS DE BOOK TRAIN | {{ $colegio->nombre }}</h2>
    <div class="relative flex flex-row w-full h-full">
        <div class="relative flex flex-col w-1/2 h-full">
            <div id="input_container" class="relative flex self-end w-1/2 p-2 my-4 border rounded-md border-color-gray--BK">
                <input class="focus:outline-none" id="input" wire:model="buscar">
                <img src="/assets/img/svg/colegios/lupa.svg" id="input_img">
            </div>
            <div class="relative flex w-full h-full py-6 overflow-y-auto border-r-2 border-color-gray--BK">
                <table class="relative w-full h-full">
                    <thead>
                        <tr class="h-12 text-left border-t-2 border-b-2 color-gray--bk border-color-gray--BK">
                            <th>ISBN</th>
                            <th>Título</th>
                            <th>Editar</th>
                            <th>Asignar</th>
                        </tr>
                    </thead>
                    <tbody class="list-table">
                        @foreach ($this->libros as $libro)
                        <tr class="relative w-full border-b border-color-gray--BK font-rob-light color-gray--bk">
                            <td class="py-6">{{ $libro->isbn }}</td>
                            <td>{{ $libro->nombre }}</td>
                            <td align="center">
                                @if($libro->asignado_a_colegio > 0)
                                <div class="relative top-0 bottom-0 py-2 rounded-md cursor-pointer w-11 h-11">
                                    <img class="w-6" src="/assets/img/svg/colegios/editar.svg" wire:click="editarLibroAsignado({{ $libro->id }})">
                                </div>
                                @endif
                            </td>
                            <td align="center">
                                <div class="relative top-0 bottom-0 py-2 text-white rounded-md cursor-pointer w-9 h-9 bg-greenBT" wire:click="asignarLibro({{ $libro->id }})">
                                    <img src="/assets/img/svg/plus.svg" class="w-5">
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="relative flex flex-col items-center justify-center w-1/2">
            @if (!$asignar_libro && !$editar_libro_asignado)
                <img src="/assets/img/svg/colegios/libros.svg" alt="" class="w-1/2">
                <div class="relative flex items-center justify-center w-full mt-6">
                    <a href="{{ route('admin.colegios.libros', [$colegio->id]) }}" class="relative flex items-center justify-center w-1/6 p-3 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
                </div>
            @endif
            @if($asignar_libro)
                <div class="relative flex flex-col items-center justify-center w-1/2 h-full">
                    <h3 class="text-xl font-bold text-center uppercase color-blue--bk">{{ $this->libro_asignado->nombre }}</h3>
                    <h4 class="text-xl font-bold text-center uppercase color-blue--bk">ISBN: {{ $this->libro_asignado->isbn }}</h4>

                    <div class="relative flex flex-col w-full mt-4">
                        <label class="relative w-full ml-4 color-gray--bk">Asignado actualmente a: </label>
                        <ul>
                        @forelse ($libro_asignado->paquetes()->wherePivot('colegio_id', $colegio->id)->get() as $paquete)
                        <li>{{ $paquete->nivel->seccion->nombre }} - {{ $paquete->nivel->nombre }}</li>
                        @empty
                        <h5>No se encuentra asignado aún</h5>
                        @endforelse
                        </ul>
                    </div>

                    <div class="relative flex flex-col w-full mt-4">
                        <label class="relative w-full ml-4 color-gray--bk">SECCION ACADEMICA</label>
                        <select wire:model="seccion_elegida" name="seccion" class="relative w-full p-2 m-4 border appearance-none color-gray--bk">
                            <option disabled="disabled" value="0" selected="selected">Seleccione una sección</option>
                            @foreach ($this->secciones as $seccion)
                                <option value="{{$seccion->id}}">{{$seccion->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="relative flex w-full mt-4">
                        <div class="relative w-1/2 ml-4">
                            <label class="relative w-full uppercase color-gray--bk">Grado</label>
                            <select wire:model="nivel_elegido" name="grado" class="relative w-full p-2 border appearance-none color-gray--bk">
                                <option disabled="disabled" value="0" selected="selected">Seleccione un nivel</option>
                                @foreach ($this->niveles as $nivel)
                                    <option value="{{$nivel->id}}">{{$nivel->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="relative w-1/2 ml-4">
                            <label class="relative w-full uppercase color-gray--bk">Precio</label>
                            <input type="text" wire:model="precio" name="precio" class="relative w-full p-2 border color-gray--bk focus:outline-none">
                        </div>
                    </div>
                    <label class="relative flex w-full mt-4 ml-6">
                        <input id="one" class="custom-check--bk" type="checkbox" wire:model="obligatorio">
                        <label class="flex self-start custom-check-label--bk" for="one">
                            <span class="custom-check-span--bk"></span>
                        </label>
                        <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">OBLIGATORIO</span>
                    </label>

                    <label class="container relative flex w-full mt-4 ml-6 uppercase color-gray--bk">Agotado
                        <input type="radio" name="bajo_pedido" value="0" wire:model="bajo_pedido">
                        <span class="checkmark"></span>
                    </label>
                    <label class="container relative flex w-full mt-2 ml-6 uppercase color-gray--bk">Disponible bajo pedido
                        <input type="radio" name="bajo_pedido" value="1" wire:model="bajo_pedido">
                        <span class="checkmark"></span>
                    </label>

                    <div class="relative flex flex-col items-center justify-center w-full pt-12">
                        <div wire:click="save" class="w-5/12 p-2 font-bold text-center text-white uppercase rounded-sm cursor-pointer bg-greenBT">
                        GUARDAR
                        </div>
                        @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="font-rob-bold text-redBT">{{$error}}</div>
                        @endforeach
                    @endif
                    </div>
                </div>
            @endif
            @if($editar_libro_asignado)
                <div class="relative flex flex-col items-center justify-center w-1/2 h-full">
                    <h3 class="text-xl font-bold text-center uppercase color-blue--bk">{{ $libro_asignado->nombre }}</h3>
                    <h4 class="text-xl font-bold text-center uppercase color-blue--bk">ISBN: {{ $libro_asignado->isbn }}</h4>
                    <label class="relative flex w-full mt-4">
                        <input id="one" class="custom-check--bk" type="checkbox" wire:model="activo">
                        <label class="flex self-start custom-check-label--bk" for="one">
                            <span class="custom-check-span--bk"></span>
                        </label>
                        <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">Activo</span>
                    </label>

                    <label class="relative flex w-full mt-4">
                        <input id="two" class="custom-check--bk" type="checkbox" wire:model.defer="bajo_pedido">
                        <label class="flex self-start custom-check-label--bk" for="two">
                            <span class="custom-check-span--bk"></span>
                        </label>
                        <span class="flex self-start w-auto uppercase color-gray--bk font-rob-light">BAJO PEDIDO</span>

                    </label>
                    @isset($error)
                    <div class="relative flex justify-center w-full">{{$error}}</div>
                    @endisset
                    <div class="relative flex flex-col items-center justify-center w-full pt-12">
                        <div wire:click="save" class="w-5/12 p-2 font-bold text-center text-white uppercase rounded-sm cursor-pointer bg-greenBT">
                        GUARDAR
                        </div>
                    @include('errors.validation')
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
