@if (session()->has('message_'.$row->id))
    <div class="fixed bottom-0 left-0 z-30 flex items-center justify-center w-full h-full bg-gray-600 bg-opacity-50">
        <div class="w-1/3 bg-white rounded-lg">
            <div class="flex flex-col items-start p-4">
                <div class="flex items-center justify-center w-full">
                    <div class="pb-3 text-lg font-medium text-gray-900">
                        <h2 class="text-2xl rob-bold color-blue--bk">{{ session('message_'.$row->id) }}</h2>
                    </div>
                </div>
                <div class="w-full">
                    <div class="flex justify-around w-full pt-5">
                        @if (!$ocultar_boton)
                            <div wire:click.prevent="delete()" class="flex items-center justify-center w-1/4 text-white uppercase border border-solid rounded-md cursor-pointer background-red--bk font-rob-bold hover:bg-white hover:border-red-500 hover:text-redBT">
                                Eliminar
                            </div>
                        @endif
                        <a href="/admin/libros" class="flex items-center justify-center w-1/4 px-4 py-2 border border-solid rounded cursor-pointer roboto-bold text-grayBT hover:bg-white hover:border-color-green--BK hover:color-green--bk lg:text-lg md:text-base sm:text-sm">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif


<x-livewire-tables::table.cell>
    <p>{{ $row->isbn }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="whitespace-normal w-28 2xl:w-auto">
        <p>{{ $row->nombre }}</p>
    </div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <p>{{ $row->activo ? 'SI' : 'NO' }}</p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<span title="{{ $row->colegios()->wherePivot('paquete_id', null)->pluck('nombre')->join(', ', ' y ') }}">{{ $row->colegios()->wherePivot('paquete_id', null)->count() }}</span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <a href="editar_libro/{{$row->id}}"><div class="flex items-center justify-center m-4 text-white uppercase rounded-md background-green--bk h-9 w-28 font-rob-bold">Editar</div></a>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <button class="text-white uppercase rounded-md background-red--bk h-9 w-28 font-rob-bold" wire:click="before_delete({{$row->id}})">Eliminar</button>
</x-livewire-tables::table.cell>
