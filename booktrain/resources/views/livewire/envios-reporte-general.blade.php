<x-livewire-tables::table.cell>
@switch($row->estado)
    @case('ENTREGADO') <span class="text-blue-400">ENTREGADO</span> @break
    @case('ENVIADO') <span class="text-greenBT">ENVIADO</span> @break
    @case('CANCELADO') <span class="text-red-500">CANCELADO</span> @break
    @case('ENVIO PARCIAL') <span class="text-yellow-500">ENVIO PARCIAL</span> @break
    @default <span class="text-gray-500">{{ $row->status }}</span>
@endswitch
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->guia }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->folio }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->tipo_envio }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ isset($row->fecha_envio) ? $row->fecha_envio->format('d / m / Y') : $row->created_at->format('d / m / Y') }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if($row->paqueteria == '')
    {{ $row->paqueteria()->first()->nombre }}
    @else
    {{ $row->paqueteria }}
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
<span class="border border-gray-300 rounded px-3 py-2 cursor-pointer" wire:click="modalDetalles({{ $row->id }})">Detalles</span>
</x-livewire-tables::table.cell>
