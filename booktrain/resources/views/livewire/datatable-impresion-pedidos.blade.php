<x-livewire-tables::table.cell>
    @if ($row->total_pedidos == $row->pedidos_impresos)
    <span class="color-green--bk">IMPRESO</span>
    @elseif ($row->pedidos_impresos == 0)
    <span class="color-red--bk">NO IMPRESO</span>
    @elseif ($row->total_pedidos > $row->pedidos_impresos)
    <span class="color-red--bk">IMPRESION PARCIAL</span>
    @else
    <span class="color-red--bk">UNDEFINED</span>
    @endif
</x-livewire-tables::table.cell>

{{-- <x-livewire-tables::table.cell>
{{ $row->total_pedidos }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->pedidos_impresos }}
</x-livewire-tables::table.cell>--}}

<x-livewire-tables::table.cell>
    {{-- {{ \App\Http\Models\Colegio::where('id',$row->colegio_id)->first()->nombre }} --}}
    {{ $row->colegio_nombre}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->fecha }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="flex flex-row-reverse">


    @if ($row->pedidos_impresos == 0)
        <a href="{{ route('admin.pedidos.imprimir.colegio', ['fecha' =>$row->fecha,'colegio_id'=> $row->colegio_id]) }}" target="__blank" class="relative w-56 h-11 background-green--bk text-white font-bold text-center rounded hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400 flex items-center justify-center">
            IMPRIMIR REPORTE
        </a>
    @else
        <a href="{{ route('admin.pedidos.imprimir.colegio', ['fecha' =>$row->fecha,'colegio_id'=> $row->colegio_id]) }}" target="__blank" class="relative w-56 h-11 background-red--bk text-white font-bold text-center rounded hover:bg-white hover:border-redBT hover:text-redBT lg:text-lg md:text-base sm:text-sm border border-solid border-red-400 flex items-center justify-center">
            REIMPRIMIR REPORTE
        </a>
    @endif
    </div>
</x-livewire-tables::table.cell>
