
<x-livewire-tables::table.cell>
    <p class="text-blue-400 truncate">
        <a href="mailto:{{ $row->name }}" class="hover:underline">{{ $row->name }}</a>
    </p>
</x-livewire-tables::table.cell><x-livewire-tables::table.cell>
    <p class="text-blue-400 truncate">
        <a href="mailto:{{ $row->email }}" class="hover:underline">{{ $row->email }}</a>
    </p>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @php
    if ($row->activo = 1) {
        echo "<label class='switch'>
                    <input id='switch_activo' type='checkbox' checked wire:click='activo(" . $row->id . ")'>
                    <span class='slider'></span>
                </label>";
    } else {
        echo "<label class='switch'>
                    <input id='switch_activo' type='checkbox' wire:click='activo(" . $row->id . ")'>
                    <span class='slider'></span>
                /label>";
    }
    @endphp
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>

    @foreach ($row->getRole($row->id)->roles as $rol)
        {{$rol->name}}<br>
    @endforeach
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if ($row->email_verified_at == null)
    <button class='border text-white h-auto w-28 m-4 py-1.5 px-2 bg-redDark rounded-md font-rob-bold uppercase' wire:click='verificar({{$row->id}})'>Confirmar</button>
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <a href='/admin/editar_usuario/{{$row->id}}'><div class='flex items-center justify-center m-4 text-white uppercase rounded-md background-green--bk h-9 w-28 font-rob-bold'>Editar</div></a>
</x-livewire-tables::table.cell>

