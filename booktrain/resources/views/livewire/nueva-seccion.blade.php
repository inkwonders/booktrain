<div class="flex border rounded-md border-color-gray--BK block p-3">
    <form wire:submit.prevent="save">
        <label class="color-green--bk font-rob-bold text-base" for="">Nueva seccion</label> <br>
        <p class="inline-block color-gray--bk font-rob-light uppercase">NOMBRE</p>
        <input class="w-auto block border rounded-md border-color-gray--BK my-2 form-input" type="text" wire:model="seccion.nombre">
        <label for="">
            <input class="mr-2 inline-block rounded-sm my-2 form-checkbox" type="checkbox" wire:model="seccion.activo">
            <span class="w-auto color-gray--bk uppercase font-rob-light">Activo</span>

        </label>
        <button class="block py-3 px-6 rounded-md relative background-green--bk text-white uppercase font-rob-bold" type="submit" wire:click="$emit('refreshList')">Guardar</button>
    </form>
</div>
