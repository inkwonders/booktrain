<div class="flex flex-col items-center p-4 relative w-full h-full justify-start">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="color-red--bk font-rob-bold uppercase text-2xl mt-3">Usuarios</h2>
        <a href="nuevo_usuario" class="background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">+ NUEVO USUARIO</a>
    </div>
    {{-- <div class="relative flex justify-between w-full mb-4">
        <input class="mb-4 w-1/4 block p-2 rounded-md my-2 border border-gray-300 appearance-none" wire:model="filtro" wire:change="$emit('refreshList')" placeholder="Buscar">

    </div> --}}
    <div class="w-full">
        <table id="example2" class="stripe tabla_quejas" style="width:100%">
            <thead class="">
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    {{-- <th>Teléfono</th> --}}
                    <th>Activo</th>
                    <th>Permisos</th>
                    <th></th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
            @foreach ($usuarios->reverse() as $usuario)
            <tr class="text-center">
                {{-- <td><span>{{$usuario->name}} {{$usuario->apellidos}}</span></td> --}}
                <td><span>{{$usuario->getFullName()}}</span></td>
                <td><span>{{$usuario->email}}</span></td>
                <td>
                    <label class="switch">
                        <input id="switch_activo" type="checkbox" @if ($usuario->activo == 1) checked @endif wire:click="activo({{$usuario}})">
                        <span class="slider"></span>
                    </label>
                </td>
                <td>
                    @php
                        $roles = $usuario->getRole($usuario->id)->roles;
                    @endphp
                    @foreach ($roles as $rol)
                        {{$rol->name}}
                        @if (count($roles)>1)
                            /
                        @endif
                    @endforeach
                </td>
                <td>
                    @if($usuario->email_verified_at == null)
                        <button class="border text-white h-auto w-28 m-4 py-1.5 px-2 bg-redDark rounded-md font-rob-bold uppercase" wire:click="verificar({{ $usuario->id }});">Confirmar</button>
                    @endif
                </td>
                <td>
                    <a href="editar_usuario/{{$usuario->id}}"><div class="background-green--bk text-white h-9 w-28 m-4 rounded-md uppercase font-rob-bold justify-center flex items-center">Editar</div></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
