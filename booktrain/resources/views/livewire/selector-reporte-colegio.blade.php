<div class="relative flex flex-col justify-center w-full h-auto border rounded-md min-h-interno border-color-gray--BK">

    <div class="relative flex flex-col justify-center w-full h-interno">
        <div class="relative flex flex-row justify-center w-full h-full">
            <!-- Parte izq -->
            <div class="relative flex flex-col w-1/2 h-full p-12">
                <span class="text-xl font-bold uppercase color-red--bk">REPORTE PARA EL COLEGIO</span>
                <div class="relative flex items-end justify-center w-full h-full">
                    <img src="{{ asset('img/svg/documento.svg') }}" alt="" class="w-1/3 h-5/6">
                </div>
            </div>

            <!-- Div separación -->
            <div class="border-r border-color-gray--BK separacion mt-14">
            </div>

            <div class="relative flex flex-col items-center justify-center w-1/2 h-full p-12">
                <div class="flex flex-col justify-center w-full h-5/6 align-center xl:w-2/3">
                    <div class="relative flex w-full">
                        <input id="fecha_inicial" placeholder="FECHA INICIAL" class="relative w-1/2 p-2 m-4 border appearance-none color-gray--bk" wire:model="fecha_inicial" type="text">
                        <input id="fecha_final"  placeholder="FECHA FINAL" class="relative w-1/2 p-2 m-4 border appearance-none color-gray--bk" wire:model="fecha_final" type="text">
                    </div>

                    <div class="relative flex w-full mt-4">
                        <div class="relative flex w-full m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <label class="cursor-pointer select_motivos"></label>
                            <select class="p-2 border pr-8 appearance-none w-full cursor-pointer color-gray--bk" wire:model="colegio_id">
                                <option value="" selected>SELECCIONE UN COLEGIO</option>
                                @foreach ($colegios as $colegio)
                                <option value="{{ $colegio->id }}">{{ $colegio->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="relative flex w-full mt-4">
                        <div class="relative flex w-1/2 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <label class="cursor-pointer select_motivos"></label>
                            <select name="grado" class="p-2 pr-8 appearance-none w-full cursor-pointer color-gray--bk" wire:model="grado_id">
                                <option value="" selected>TODOS LOS GRADOS</option>
                                @foreach ($this->grados as $grado)
                                <option value={{ $grado->id }}>{{ $grado->nombre }} - {{ $grado->seccion->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="relative flex w-1/2 m-2 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                            <label class="cursor-pointer select_motivos"></label>
                             <select class="p-2 pr-8 appearance-none w-full cursor-pointer color-gray--bk" wire:model="libro_id">
                                <option value="" selected>TODOS LOS ISBN</option>
                                @foreach ($this->libros as $libro)
                                <option value="{{ $libro->id }}">{{ $libro->isbn }} - {{ $libro->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="relative flex justify-center w-full mt-4">
                        <span wire:click="descargarExcel" wire:loading.class="cursor-wait pointer-events-none" class="text-sm mx-2 border @if($this->validaFormulario || $cargando_reporte) border-greenBT bg-greenBT text-white cursor-pointer hover:bg-white hover:border-greenBT hover:border hover:text-greenBT @else bg-grayBT text-whiteBT cursor-not-allowed @endif appearance-none w-5/12 uppercase rounded text-center p-2 font-bold">
                            Descargar Excel
                        </span>
                        <span wire:click="mostrarReporte" wire:loading.class="cursor-wait pointer-events-none" class="text-sm mx-2 border @if($this->validaFormulario || $cargando_reporte) border-greenBT bg-greenBT text-white cursor-pointer hover:bg-white hover:border-greenBT hover:border hover:text-greenBT @else bg-grayBT text-whiteBT cursor-not-allowed @endif appearance-none w-5/12 uppercase rounded text-center p-2 font-bold">
                            ver reporte
                        </span>
                    </div>

                    <div class="relative flex justify-center w-full mt-4">
                        <img class="max-h-10" src="{{ asset('img/spinner.gif') }}" wire:loading wire:target="descargarExcel">
                    </div>

                </div>
            </div>

        </div>
        <div class="relative flex items-center justify-center w-full h-1/6">
            <a href="{{ route('reportes') }}" class="relative flex items-center justify-center w-1/6 p-5 uppercase border border-gray-400 rounded-md cursor-pointer h-1/3 color-gray--bk">
                <div>Regresar</div>
            </a>
        </div>
    </div>
</div>

@section('js')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="//code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function() {
    $("#fecha_inicial").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText, inst) {
            Livewire.emit('setFecha', 'inicial', dateText)
        }
    }).datepicker("setDate",'{{ date('Y-m') }}-1');

    $("#fecha_final").datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText, inst) {
            Livewire.emit('setFecha', 'final', dateText)
        }
    }).datepicker("setDate",'now');
});
</script>
@endsection
