<section class="relative w-full p-6 min-h-custom lg:px-16">
    <div class="w-full p-4 border border-gray-400 rounded-md lg:border-0">
        <div class="flex items-center justify-between w-full py-8 text-left border-gray-300 lg:border-b">
            <span class="text-base font-rob-bold color-blue--bk">PEDIDOS</span>
            <div class="relative w-1/4 overflow-hidden border border-gray-200 rounded-md cursor-pointer">
                <select wire:model="filtrar_caja" name="status" id="status" class="relative w-full p-2 appearance-none text-grayBT">
                    <option selected value="0">Todas las cajas</option>
                    @foreach ($usuarios_cajas as $usuario_caja)
                        <option value="{{ $usuario_caja["id"] }}">{{ $usuario_caja["name"] }} {{ $usuario_caja["apellidos"] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @forelse ($pedidos as $pedido)
            <div class="items-center justify-between inline-block w-auto h-auto max-w-full py-4 text-base text-left text-gray-500 border-b border-gray-300 lg:py-0 lg:w-full xl:h-32 xl:flex xl:flex-row md:text-center rob-light">

                <div class="inline-flex flex-col justify-between h-auto py-4 text-left xl:h-16 sm:p-4 xl:py-0 lg:w-24 xl:w-1/12">
                    <span class="font-medium font-rob-bold text-grayDarkBT">PEDIDO</span>
                    <span class="font-light">{{ $pedido->serie.$pedido->folio }}</span>
                </div>
                <div class="inline-flex flex-col justify-between h-auto p-4 pl-20 pr-0 text-left xl:h-16 sm:pl-4 xl:py-0 sm:pr-4 lg:w-24 xl:w-1/12">
                    <span class="font-medium font-rob-bold text-grayDarkBT">TOTAL</span>
                    <span class="font-light">${{ number_format($pedido->total, 2) }}</span>
                </div>

                <br class="sm:hidden">

                <div class="inline-flex flex-col justify-between w-auto h-auto py-4 pr-6 xl:h-16 sm:p-4 xl:py-0 md:w-1/4 lg:w-1/5 xl:w-auto">
                    <span class="font-medium font-rob-bold text-grayDarkBT">FECHA PEDIDO</span>
                    <span class="font-light">{{ date('d / m / Y', strtotime($pedido->created_at)) }}</span>
                </div>
                <div class="inline-flex flex-col justify-between h-auto py-4 xl:h-16 lg:hidden xl:py-0">
                    <span class="font-medium font-rob-bold text-grayDarkBT">FECHA ENTREGA</span>
                    @if (!is_null($pedido->envio) && !is_null($pedido->envio->fecha_recepcion))
                        <span class="font-light">{{ date_format($pedido->envio->fecha_recepcion,"d / m / Y") }}</span>
                    @else
                        <span class="font-light">&nbsp;</span>
                    @endif
                </div>

                <br class="md:hidden">
                <div class="inline-flex flex-col justify-between w-full h-auto p-4 text-left xl:h-16 sm:py-4 mt-7 md:mt-0 xl:py-0 sm:w-auto">
                    <span class="font-medium font-rob-bold text-grayDarkBT">ESTATUS</span>
                    <div class="relative flex flex-col items-center justify-center h-10">
                        <div class="flex items-center justify-center w-full md:w-80">
                            <div class="flex items-center justify-between max-w-full w-60">
                            @if ($pedido->status != 'Carrito')
                                @switch($res = $pedido->status)
                                    @case('PROCESADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero actual"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break
                                    @case('PAGADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break
                                    @case('ENVIADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break
                                    @case('ENVIO PARCIAL')
                                        <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                        <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                        <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                        <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break
                                    @case('ENVIO LIQUIDADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break
                                    @case('ENTREGADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                        @break
                                    @case('ENTREGA PARCIAL')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                        @break
                                    @case('ENTREGA LIQUIDADA')
                                        <div class="h-2.5 w-2.5 rounded-full avance activado primero"></div>
                                        <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                        <div class="h-2.5 w-2.5 rounded-full avance activado"></div>
                                        <div class="h-2.5 w-2.5 rounded-full avance activado actual"></div>
                                    @break
                                    @case('CANCELADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado primero actual-cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>
                                        @break
                                    @case('PAGO RECHAZADO')
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado primero actual-cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>
                                            <div class="h-2.5 w-2.5 rounded-full avance cancelado"></div>

                                        @break
                                    @case('PAGO EN REVISION')
                                            <div class="h-2.5 w-2.5 rounded-full avance activado primero actual"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                            <div class="bg-gray-400 h-2.5 w-2.5 rounded-full avance"></div>
                                        @break

                                    @default

                                @endswitch
                            @endif

                            </div>
                        </div>
                        @if ($pedido->status == "CANCELADO" || $pedido->status == "PAGO RECHAZADO")
                            <span class="absolute inline text-xs font-light text-gray-400 -bottom-3">{{$pedido->status}}</span>
                        @endif
                        @if ($pedido->status != "CANCELADO" && $pedido->status != "PAGO RECHAZADO" && $pedido->status != "PAGO EN REVISION")
                            <span class="absolute hidden text-xs font-light text-gray-400 lg:inline -bottom-3">PROCESADO - PAGADO - ENVIADO - ENTREGADO</span>
                        @endif
                        @if ($pedido->status == "PAGO EN REVISION")
                            <span class="absolute text-xs font-light text-gray-400 lg:inline -bottom-3">{{$pedido->status}}</span>
                        @endif

                    </div>
                </div>
                <div class="flex-col justify-between hidden h-auto py-4 xl:h-16 lg:inline-flex xl:py-0">
                    <span class="font-medium font-rob-bold text-grayDarkBT">FECHA ENTREGA</span>
                    @if (!is_null($pedido->envio) && !is_null($pedido->envio->fecha_recepcion))
                        <span class="font-light">{{ date_format($pedido->envio->fecha_recepcion,"d / m / Y") }}</span>
                    @else
                        <span class="font-light">&nbsp;</span>
                    @endif
                </div>

                <br class="lg:hidden">
                <div class="inline-flex flex-col justify-between w-full py-4 text-center sm:p-4 xl:w-auto xl:py-0">
                    <a href="/datos-compra/{{ $pedido->id }}" class="font-rob-bold text-blueBT">VER MÁS DETALLES</a>
                </div>

            </div>
        @empty
            <div class="relative flex items-center justify-center w-full h-full text-4xl font-thin uppercase center min-h-custom color-gray--bk">
                Sin pedidos.
            </div>
        @endforelse
    </div>
</section>
