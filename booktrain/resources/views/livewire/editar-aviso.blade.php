<div class="w-full h-full relative flex flex-col p-6">
    <h2 class="color-green--bk font-bold text-xl">EDITAR AVISO | {{$colegio->nombre}}</h2>
    <div class="relative w-full h-full relative flex flex-row">
        <div class="relative w-1/2 h-full flex justify-center items-center">
            <img src="/assets/img/svg/colegios/campana_amarilla.svg" class="w-1/3 relative" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="relative w-1/2 h-full flex justify-center items-center">
            <form wire:submit.prevent="save" class="flex flex-col relative w-1/2 h-full justify-center items-center">
                <label class="color-gray--bk relative self-start font-rob-light uppercase" for="titulo">Nombre</label>
                <input type="text" class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" id="titulo" wire:model='notificacion.titulo'/>
                {{-- <label class="relative self-start my-4" for="">
                    <input type="checkbox" class="inline-block rounded-sm form-checkbox" wire:model='notificacion.activo'/>
                    <span class="w-auto mx-4 color-gray--bk uppercase font-rob-light">Activo</span>
                </label> --}}
                <label class="flex self-start my-6">
                    <input id="one" class="custom-check--bk" type="checkbox" wire:model="notificacion.activo">
                    <label class="flex self-start custom-check-label--bk" for="one">
                        <span class="custom-check-span--bk"></span>
                    </label>
                    <span class="w-auto self-start flex color-gray--bk uppercase font-rob-light">Activo</span>
                </label>
                <label class="color-gray--bk relative self-start font-rob-light" for="mensaje">Mensaje</label>
                <textarea id="mensaje" type="text"  class="h-36 resize-none mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" wire:model='notificacion.aviso'></textarea>
                <button class="relative background-green--bk text-white p-2.5 w-1/2 m-4 rounded-md uppercase font-rob-bold" type="submit" >Guardar</button>
            </form>
        </div>

    </div>
    <div class="w-full relative justify-center items-center flex mt-4">
        <a href="/admin/avisos_colegio/{{$colegio->id}}" class="w-1/6 relative flex justify-center uppercase items-center border border-gray-400 rounded-md color-gray--bk p-3 cursor-pointer"><div>Regresar</div></a>
    </div>
</div>
