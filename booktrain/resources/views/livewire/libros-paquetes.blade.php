<div class="pt-5">
    <span class="inline-block">Lista de libros del paquete {{ $paquete->nombre }}</span>
    <input class="flex w-1/8 form-input" wire:model="filtro" wire:change="$emit('refreshListaPaquetes')" placeholder="Filtro">
    <div class="flex w-3/4 ml-10">
        @livewire('editar-libro-paquete', ['paquete' => $paquete])
    </div>
    <table class="border border-gray-200 w-full mt-3">
        <thead>
            <td>ID</td>
            <td>Asignado</td>
            <td>ISBN</td>
            <td>Nombre</td>
            <td>Precio</td>
            <td>Obligatorio</td>
            <td>Asignar</td>
            <td></td>
        </thead>
        <tbody>
        @foreach ($paquete->nivel->seccion->colegio->libros()->wherePivot('paquete_id', null)->where('nombre', 'like', '%' . $this->filtro . '%')->get()->reverse() as $libro)
        <tr class="border border-gray-300">
            <td>{{ $libro->id }}</td>
            <td>@if($libro->paquetes()->wherePivot('paquete_id', $paquete->id)->first()) <span class="text-green-500">SI</span> @else <span class="text-red-500">NO</span> @endif</td>
            <td>{{ $libro->isbn }}</td>
            <td>{{ $libro->nombre }}</td>
            @if($libro->paquetes()->wherePivot('paquete_id', $paquete->id)->first())
            <td>{{ $libro->paquetes()->wherePivot('paquete_id', $paquete->id)->first()->pivot->precio }}</td>
            @if($libro->paquetes()->wherePivot('colegio_id', $paquete->nivel->seccion->colegio->id)->wherePivot('paquete_id', $paquete->id)->wherePivot('libro_id', $libro->id)->first()->pivot->obligatorio)
            <td><button class="text-green-500" wire:click="obligatorio({{ $libro->id }}, 0)">Obligatorio</button></td>
            @else
            <td><button class="text-green-800" wire:click="obligatorio({{ $libro->id }}, 1)">Opcional</button></td>
            @endif
            <td><button class="text-red-500" wire:click="remover({{ $libro }})">Remover</button></td>
            <td><button class="text-purple-500" wire:click="$emit('editarLibro', {{ $libro->id }})">Editar</button></td>
            @else
            <td>N/A</td>
            <td>N/A</td>
            <td><button class="text-blue-500" wire:click="asignar({{ $libro }})">Asignar</button></td>
            <td></td>
            @endif
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
