@php
    $datos_status = $row->textoEstatus($row->status);
@endphp
<x-livewire-tables::table.cell>
    <p>{{ $row->id }}</p>
</x-livewire-tables::table.cell>


<x-livewire-tables::table.cell class="font-rob-bold text-sm">
    <div class="w-20 2xl:w-auto max-w-full whitespace-normal">
        <p class="{{ $datos_status['clase'] }}">
            {{ $datos_status['texto'] }}
        </p>
    </div>
</x-livewire-tables::table.cell>


<x-livewire-tables::table.cell>
    <div class="w-28 2xl:w-auto max-w-full whitespace-normal">
        <p>{{ $row->titulo }}</p>
    </div>
</x-livewire-tables::table.cell>


<x-livewire-tables::table.cell>
    <div class="w-20 2xl:w-auto max-w-full whitespace-normal">
        <p>{{ $row->ticketMotivo->nombre }}</p>
    </div>
</x-livewire-tables::table.cell>


@if ($row->entradas->count() == 0)
    <x-livewire-tables::table.cell>
        <div class="w-24 2xl:w-auto max-w-full whitespace-normal">
            <p>{{ date_format($row->created_at, "d / m / Y") }}</p>
        </div>
    </x-livewire-tables::table.cell>


    <x-livewire-tables::table.cell>
        <div class="w-20 2xl:w-auto max-w-full whitespace-normal">
            <p>{{ $row->user->name." ".$row->user->apellidos }}</p>
        </div>
    </x-livewire-tables::table.cell>
@else
    <x-livewire-tables::table.cell>
        <div class="w-24 2xl:w-auto max-w-full whitespace-normal">
            <p>{{ date_format($row->entradas->last()->created_at, "d / m / Y") }}</p>
        </div>
    </x-livewire-tables::table.cell>


    <x-livewire-tables::table.cell>
        <div class="w-20 2xl:w-auto max-w-full whitespace-normal">
            <p>{{ $row->entradas->last()->user->name." ".$row->entradas->last()->user->apellidos }}</p>
        </div>
    </x-livewire-tables::table.cell>
@endif


<x-livewire-tables::table.cell>
    @if ($datos_status['texto'] == "Cerrado" && $row->calificacion == 0)
        <div class="flex justify-around items-center">
            <a href="/tickets/{{ $row->id }}" class="flex justify-center items-center">
                <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
            </a>
            @hasrole('Admin')
            @else
                <boton-calificar-ticket caso="{{ $row->id }}" class="icono_calificar"/>
            @endhasrole
        </div>
    @elseif ($datos_status['texto'] == "Cerrado" && $row->calificacion != 0)
        <a href="/tickets/{{ $row->id }}" class="flex justify-center items-center">
            <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
        </a>
    @else
        <a href="/tickets/{{ $row->id }}" class="flex justify-center items-center">
            <img class="w-6" src="/assets/img/svg/lupa.svg" alt="">
        </a>
    @endif
</x-livewire-tables::table.cell>
