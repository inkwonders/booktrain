<div class="w-full relative min-h-custom block h-auto py-4 px-2">
    <div class="min-h-interno w-full block border border-grayBT rounded-md p-10">
        <div class="relative flex justify-between w-full py-4 mb-4 pt-0">
            <h2 class="color-green--bk font-rob-bold uppercase text-xl">CUPONES DE DESCUENTO</h2>

            <button wire:click="$set('modal_nuevo_cupon', true)" class="h-10 w-60 overflow-x-hidden right-0 top-0 bottom-0 m-auto mr-0 font-bold bg-greenBT rounded cursor-pointer flex justify-center items-center relative text-white">NUEVO CUPÓN</button>
        </div>
        <div class="w-full h-auto">
            <div class="w-full h-auto flex justify-between data-table-diseño">
                @livewire('cupones-data-table')
            </div>
        </div>

        @if($modal_nuevo_cupon)
        <div class="fixed top-0 bottom-0 left-0 right-0 flex items-center justify-center z-20">
            <div wire:click="$set('modal_nuevo_cupon', false)" class="absolute top-0 bottom-0 left-0 right-0 -z-1 bg-gray-500 opacity-50"></div>
            <div class="relative w-11/12 max-w-xl h-auto max-h-secciones bg-white rounded-lg border border-grayBT text-grayBT p-6 pt-8 flex flex-col justify-between">
                <div class="h-auto w-full overflow-x-hidden overflow-y-auto flex flex-col justify-between">
                    <div class="w-full block">
                            <span class="inline-block mb-4 w-1/3"><b>Nuevo cupón</b></span>
                    </div>
                    <div class="w-full relative border-b border-t border-grayBT h-32 overflow-y-auto overflow-x-hidden flex justify-center items-center">
                        <div class="px-2 w-1/2">
                            <label class="color-gray--bk relative self-start font-rob-light" for="valor">VALOR %</label>
                            <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="text" id="valor" wire:model="valor">
                        </div>
                        <div class="px-2 w-1/2">
                            <label class="color-gray--bk relative self-start font-rob-light" for="caducidad">CADUCIDAD</label>
                            <input class="mb-4 w-full block p-2 rounded-md my-2 border border-gray-300 appearance-none" type="date" id="caducidad" wire:model="caducidad">
                        </div>
                    </div>

                    <div class="w-full flex flex-col justify-between items-start h-auto">
                        <div class="w-full py-4">
                            @include('errors.validation')
                        </div>
                        <div class="w-full h-auto flex flex-row justify-center items-center">
                            <button wire:click="crearCupon" class="bg-white text-greenBT font-rob-bold w-1/2 h-11 flex justify-center items-center rounded-md cursor-pointer border border-greenBT hover:bg-greenBT hover:text-white hover:border-transparent mx-2">CONFIRMAR</button>
                            <button wire:click="$set('modal_nuevo_cupon', false)" class="bg-white text-grayBT text-white font-rob-bold w-1/2 h-11 flex justify-center items-center rounded-md cursor-pointer border border-grayBT hover:bg-grayBT hover:text-white mx-2">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
