<div class="block">
    <form wire:submit.prevent="save" class="mb-3">
        @foreach ($errors->toArray() as $rule => $error_descriptions)
            @foreach ($error_descriptions as $error)
                <h5 class="text-red-500">{{ $error }}</h5>
            @endforeach
        @endforeach
        <span class="color-blue--bk font-rob-bold text-lg uppercase self-start mb-6">LOGO DEL COLEGIO</span>
        <img class="max-w-full p-3 border rounded-md border-color-gray--BK" src="{{ $colegio->logo }}" alt="Logo del colegio" >
        <span class="inline-block  border rounded-md border-color-gray--BK p-5 mb-3 mt-3 w-full">CAMBIAR LOGO DEL COLEGIO<br>
            <input type="file" wire:model="logo">
        </span>
        <span class="inline-block color-blue--bk font-rob-bold text-lg uppercase">NOMBRE</span>
        <input class="block  border rounded-md border-color-gray--BK my-2 form-input w-full" type="text" wire:model="colegio.nombre" placeholder="nombre">
        <span class="inline-block color-blue--bk font-rob-bold text-lg uppercase">CÓDIGO</span>
        <input class="block  border rounded-md border-color-gray--BK my-2 form-input w-full" type="text" wire:model="colegio.codigo" placeholder="codigo">
        <span class="inline-block color-blue--bk font-rob-bold text-lg uppercase">COSTO ENVÍO</span>
        <input class="block  border rounded-md border-color-gray--BK my-2 form-input w-full" type="text" wire:model="costo_envio" placeholder="0 para desactivar">
        <label for="">
            <input class="inline-block  border rounded-md border-color-gray--BK my-2 form-checkbox mr-2" type="checkbox" wire:model="colegio.activo">
            <span class="w-auto color-gray--bk uppercase font-rob-light">Activo</span>
        </label>
        <button class="block py-3 px-6 rounded-md relative background-green--bk text-white uppercase font-rob-bold" type="submit">Guardar</button>
    </form>
</div>
