<div class="relative w-full flex flex-col items-center">
    <div class="relative flex justify-between w-full py-4 mb-5">
        <div class="flex">
            <input name="filtro_fecha" wire:model="filtro_fecha" id="filtro_fecha" placeholder="FILTRO FECHA" class="relative w-40 mr-2 h-11 px-2 border color-gray--bk rounded-md" type="text">
            <button onclick="emit_filtro_fecha()" id="buscar" class="relative w-36 h-11 background-green--bk text-white font-bold rounded hover:bg-white hover:border-greenBT hover:text-greenBT lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400">BUSCAR</button>
        </div>
        {{-- <div class="flex items-end">
            <input name="fecha_impresion" on="emit_fechas()" wire:model="fecha_impresion" id="fecha_impresion" placeholder="FECHA IMPRESION" class="relative w-40 mr-1 h-11 px-2 border color-gray--bk rounded-md" type="text">
            <a href="{{ route('admin.pedidos.imprimir', $fecha_impresion) }}" target="__blank" class="relative w-56 h-11 text-white font-bold text-center rounded hover:bg-white lg:text-lg md:text-base sm:text-sm border border-solid flex items-center justify-center @if($pedido_impreso ) disabled border-gray-400 bg-gray-400 @else border-gray-400 background-green--bk hover:border-greenBT hover:text-greenBT @endif }}">IMPRIMIR REPORTE</a>
        </div> --}}
    </div>
</div>
