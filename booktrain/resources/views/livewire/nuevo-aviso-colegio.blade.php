<div class="border border-gray-300 p-3">
    <form wire:submit.prevent="save">
        <p>
            <span>Titulo</span>
            <input type="text" class="form-input" wire:model='notificacion.titulo' placeholder='Titulo'/>
        </p>
        <p>
            <span>Aviso</span>
            <input type="text" class="form-input" wire:model='notificacion.aviso' placeholder='Aviso'/>
        </p>
        <p>
            <label>
                Activo
                <input type="checkbox" class="form-checkbox" wire:model='notificacion.activo'/>
            </label>
            <button class="block bg-blue-600 text-white py-2 px-4 rounded-md" type="submit" wire:click="$emit('refreshList')">Guardar</button>
        </p>
    </form>
</div>
