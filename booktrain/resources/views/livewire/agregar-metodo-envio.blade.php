<div class="relative flex flex-col w-full p-10">
    <div class="relative flex justify-between w-full">
        <h2 class="text-xl font-bold color-green--bk">AGREGAR MÉTODO DE ENVÍO | {{$colegio->nombre}}</h2>
    </div>
    <div class="relative flex flex-row w-full">
        <div class="relative flex items-center justify-center w-1/2">
            <img class="relative w-1/2" src="/assets/img/svg/colegios/metodos_envio.svg" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion"></div>
        <div class="relative flex flex-col items-center w-1/2">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center w-1/2">
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none focus:outline-none" type="text" wire:model="direccion.calle" placeholder="CALLE" id="calle">

                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none focus:outline-none" type="text" wire:model="direccion.no_exterior" id="nombre" placeholder="#EXT">
                <div class="flex w-full mb-4">
                    <input class="w-1/2 p-2 my-2 mb-4 mr-2 border border-gray-300 rounded-md appearance-none focus:outline-none" type="text" id="nombre" wire:model="direccion.no_interior" placeholder="#INT">
                    <input class="w-1/2 p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none focus:outline-none" type="text" id="nombre" wire:model="direccion.cp" placeholder="C.P">
                </div>
                <input class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none focus:outline-none" type="text" id="nombre" wire:model="direccion.colonia" placeholder="COLONIA">
                <select class="block w-full p-2 my-2 mb-4 text-gray-400 border border-gray-300 rounded-md appearance-none focus:outline-none" wire:model="direccion.estado_id" wire:change="selectEstado">
                    <option value="0" disabled selected>ESTADO</option>
                    @foreach ($estados as $estado)
                    <option value="{{ $estado->id }}">{{ $estado->descripcion }}</option>
                    @endforeach
                </select>
                <select class="block w-full p-2 my-2 mb-4 text-gray-400 border border-gray-300 rounded-md appearance-none" wire:model="direccion.municipio_id">
                    <option value="0" disabled selected>MUNICIPIO</option>
                    @if (!empty($municipios))
                        @foreach ($municipios as $municipio)
                        <option value="{{ $municipio->id }}">{{ $municipio->descripcion }}</option>
                        @endforeach
                    @endif

                </select>
                <label class="relative self-start color-gray--bk font-rob-light" for="nombre">REFERENCIA</label>
                <textarea class="block w-full p-2 my-2 mb-4 border border-gray-300 rounded-md appearance-none resize-none focus:outline-none" type="text" id="nombre"  wire:model="direccion.referencia" placeholder=""></textarea>

                <button class="relative w-1/2 p-2 m-4 text-white uppercase rounded-md background-green--bk font-rob-bold" type="submit">Guardar</button>
            </form>
            @include('errors.validation')
        </div>
    </div>
    <div class="relative flex items-center justify-center w-full my-6">
        <a href="/admin/colegios_envio/{{$colegio->id}}" class="relative flex items-center justify-center w-1/6 p-3 font-semibold text-gray-400 bg-transparent border border-gray-300 rounded hover:bg-gray-500 hover:text-white hover:border-transparent">REGRESAR</a>
    </div>
</div>

