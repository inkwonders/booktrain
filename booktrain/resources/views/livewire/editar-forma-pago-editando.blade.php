


<div class="relative flex flex-col w-full h-full p-10 border rounded-md border-color-gray--BK">
    <div class="relative flex justify-between w-full mb-4">
        <h2 class="text-xl font-bold color-green--bk">EDITAR CONFIGURACIÓN DE PAGO | {{$colegio->nombre}}</h2>
    </div>
    <div class="relative flex flex-row w-full h-full">
        <div class="relative flex items-center justify-center w-1/2">
            <img class="relative w-1/2" src="{{ asset('img/svg/colegios/monedas.svg') }}" alt="">
        </div>
        <div class="border-r border-color-gray--BK separacion mt-14"></div>
        <div class="relative flex flex-col items-center justify-center w-1/2 h-11/12">
            <form wire:submit.prevent="save" class="relative flex flex-col items-center w-1/2">
                <label class="self-start mb-6 text-lg uppercase color-blue--bk font-rob-bold" for="">{{ $nombre }}</label>

                <span class="flex self-start color-gray--bk font-rob-light">PORCENTAJE</span>
                <input class="block w-full my-2 border rounded-md border-color-gray--BK form-input" type="text" wire:model="forma_pago.comision">
                <span class="flex self-start color-gray--bk font-rob-light">MONTO MÍNIMO</span>
                <input class="block w-full my-2 border rounded-md border-color-gray--BK form-input" type="text" wire:model="forma_pago.minimo">

                @include('errors.validation')
                <button class="relative w-1/2 h-12 m-4 text-white uppercase rounded-md background-green--bk font-rob-bold" type="submit" wire:click="save">Guardar</button>
                <a href="{{ route('admin.colegios.metodos_pago', $colegio->id) }}" class="relative w-1/2 h-12 p-3 m-4 text-center uppercase border border-gray-400 rounded-md cursor-pointer color-gray--bk font-rob-bold"><div>Regresar</div></a>
            </form>
        </div>
    </div>
</div>
