<x-livewire-tables::table.cell>
    {{$row->serie }}{{$row->folio}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->created_at != null ? (date_format($row->created_at,"d / m / Y")) : '' }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if($row->email_pedido == null)
    {{ $row->user->email }}
    @else
    {{ $row->email_pedido }}
    @endif

</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->celular_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if ( $row->factura == 1)
        <span>Si</span>
    @else
        <span>No</span>
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->referencia != null ? $row->referencia->referencia : '' }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->metodo_pago }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->forma_pago }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    ${{ number_format($row->total,2) }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    ${{ number_format($row->pagos->where('status','SUCCESS')->pluck('amount')->first(), 2) }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
     {{ $row->pagos->where('status','SUCCESS')->pluck('created_at')->first() }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if($row->tipo_entrega == 1)
        <span>Colegio</span>
    @elseif ($row->tipo_entrega == 2)
        <span>Domicilio</span>
    @else
        <span>PickUp</span>
    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    @if ($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO')
    <span class="color-red--bk">{{ $row->status }}</span>

    @elseif ($row->status == 'PROCESADO')
     <span class="color-gray--bk">{{ $row->status }}</span>

    @elseif ( $row->status == 'PAGO EN REVISION')
     <span class="color-blue--bk">{{ $row->status }}</span>

    @else
     <span class="color-green--bk">{{ $row->status }}</span>

    @endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <span>
        @if($row->status  == 'PROCESADO')
            <button style="height: 47.65px;line-height: 43.65px;border-color: #81b84e;" class="w-56 h-12 font-bold border border-green-500 border-solid rounded focus:outline-none openModal background-white--bk text-green-1000 hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm" name="{{ $row->id }}" wire:click="$emit('modalCambioEstatus', {{ $row->id }})">Modificar</button>
        @endif
        @if($row->status  == 'PAGADO')
            <form action="/reporte_pedidos/pedidos_general_status/" method="GET" class="relative items-center w-full formulario-cambio" id="formulario-cambio" name="formulario-cambio" >

                <input type="hidden" name="estatus" id="estatus" value="CANCELADO">
                <input type="hidden" name="pedido_id" id="pedido_id" value="{{$row->id}}">
                <button type="submit"  id="btnCancelar{{$row->id}}" name="btnCancelar{{$row->id}}"  class="w-56 h-12 font-bold text-red-500 border border-red-400 border-solid rounded background-white--bk hover:bg-red-500 hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm">Cancelar</button>
            </form>
        @endif
        @if($row->status  == 'CANCELADO')
            <form action="/reporte_pedidos/pedidos_general_status/" method="GET" class="relative items-center w-full formulario-cambio" id="formulario-cambio" name="formulario-cambio" >

                <input type="hidden" name="estatus" id="estatus" value="PROCESADO">
                <input type="hidden" name="pedido_id" id="pedido_id" value="{{$row->id}}">
                <button type="submit"  id="btnProcesar{{$row->id}}" name="btnProcesar{{$row->id}}"  class="w-56 h-12 font-bold border border-green-500 border-solid rounded background-white--bk text-green-1000 hover:background-green--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm" style="border-color: #81b84e;">Activar</button>
            </form>
         @endif
         @if($row->status  != 'CANCELADO' || $row->status  == 'PAGADO' || $row->status  == 'PROCESADO')

         @endif
    </span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <span>
        <form action="/reporte_pedidos/pedidos_general_confirmacion_pago/" method="GET" class="relative items-center w-full formulario-envio-mail" id="formulario-envio-mail" name="formulario-envio-mail" >
            @if($row->status  == 'PAGADO')
            <input type="hidden" name="pedido_id" id="pedido_id" value="{{ $row->id }}">
            <button style="border-color: #50b9e7;" type="submit"  id="btnSend{{ $row->id }}" name="btnSend{{ $row->id }}" class="w-56 h-12 font-bold border border-solid rounded background-white--bk text-blue-1000 hover:background-blue--bk hover:border-white hover:text-white lg:text-lg md:text-base sm:text-sm border-blue-1000">Enviar</button>
            @endif
        </form>
    </span>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="px-4 w-max">
        <button class=" w-max hover:underline" name="{{ $row->id }}" wire:click="$emit('modalDetallesPedido', {{ $row->id }})">Ver detalles</button>
    </div>
</x-livewire-tables::table.cell>
