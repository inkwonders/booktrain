<div class="border-gray-200 flex border block p-3">

    <form wire:submit.prevent="save">
        <label for="">Nuevo nivel</label>
        <input class="w-auto block rounded-sm my-2 form-input" type="text" wire:model="nivel.nombre" placeholder="nombre">
        <label for="">
            <span class="w-auto mr-2">Activo</span>
            <input class="inline-block rounded-sm my-2 form-checkbox" type="checkbox" wire:model="nivel.activo">
        </label>
        <button class="block bg-blue-600 py-3 px-6 rounded-md" type="submit" wire:click="$emit('refresh')">Guardar</button>
    </form>
</div>
