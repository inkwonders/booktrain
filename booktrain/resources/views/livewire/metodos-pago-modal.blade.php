@if($modal_metodo_pago)
<div class="fixed top-0 bottom-0 left-0 right-0 z-20 p-12">
   <div class="flex items-start justify-center w-full h-full overflow-x-hidden overscroll-y-auto">
       <div class="absolute top-0 bottom-0 left-0 right-0 z-0 bg-gray-500 opacity-50" wire:click="$set('modal_metodo_pago', null)"></div>
       <div class="relative z-10 flex flex-col items-center justify-between w-full h-auto max-w-6xl p-10 pt-5 bg-white border rounded-lg border-grayBT">
           <p class="text-xl font-bold text-grayBT">{{ $metodo_pago->codigo }}</p>
           <div class="flex items-center justify-between pb-14">

           <div class="w-full">
              <div class="w-full border-b-2"></div>
              <div class="w-full border-b-2">
                 <div class="flex mt-1 flex-column">
                    <p class="w-1/2 text-base text-left uppercase color-green--bk font-rob-bold">Datos generales</p>
                 </div>
                 <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Proveedor</span>
                       <span class="text-base uppercase color-gray--bk"> {{ $metodo_pago->proveedor }} </span>
                    </div>
                </div>
                <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Descripción</span>
                       <span class="text-base uppercase color-gray--bk">
                           <input type="text" wire:model.lazy='metodo_pago.descripcion' class="w-full px-2 border border-gray-400">
                        </span>
                    </div>
                </div>
                <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Disponible</span>
                       <span class="text-base uppercase color-gray--bk">
                           <input type="checkbox" wire:model.defer="metodo_pago.disponible">
                       </span>
                    </div>
                </div>
                <div class="flex justify-between">
                    <div class="mt-1 w-max">
                       <span class="text-base uppercase color-gray--bk font-rob-bold">Formas de pago</span>
                       <span class="text-base uppercase color-gray--bk"> {{ $metodo_pago->formasPago->unique()->pluck('descripcion')->join(', ', ' y ') }} </span>
                    </div>
                 </div>
              </div>
           </div>
        </div>

        <div class=""></div>
        <div class="grid grid-cols-3 gap-4 mt-5">
            <div class="...">
                <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-greenBT hover:border-greenBT hover:text-greenBT hover:bg-white h-11" wire:click="guardar">Aceptar</button>
            </div>
            <div class="...">

            </div>
            <div class="...">
                <button  class="w-48 mb-2 text-xl font-bold text-white border border-transparent rounded bg-grayBT h-11 hover:border-grayBT hover:bg-white hover:text-grayBT" wire:click="$set('modal_metodo_pago', null)">Cancelar</button>
            </div>
          </div>
        </div>
    </div>
</div>
@endif
