<x-livewire-tables::table.cell>
@if($row->status == 'PAGO RECHAZADO' || $row->status == 'CANCELADO')
    <span class'color-red--bk'>{{ $row->status }}</span>
@elseif ($row->status == 'PROCESADO')
    <span class='color-gray--bk'>{{ $row->status }}</span>
@elseif ( $row->status == 'PAGO EN REVISION')
    <span class='color-blue--bk'>{{ $row->status }}</span>
@else
    <span class='color-green--bk'>{{ $row->status }}</span>
@endif
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{$row->serie }}{{$row->folio}}
</x-livewire-tables::table.cell>


<x-livewire-tables::table.cell>
    {{ $row->created_at != null ? (date_format($row->created_at,"d / m / Y")) : ''}}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    <div class="w-max px-4">
        <div class="h-auto w-14 relative cursor-pointer" wire:click="modalEnvioParcial({{ $row->id }})">
            <img src="{{ asset('img/svg/caja_cantidad.svg') }}" class="w-full">
            <div class="absolute top-2 bottom-0 left-0 right-0 flex justify-center items-center text-grayBT">
                <p>Parcial</p>
            </div>
        </div>
   </div>
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->nombre_contacto }} {{ $row->apellidos_contacto }}
</x-livewire-tables::table.cell>

<x-livewire-tables::table.cell>
    {{ $row->colegio->nombre }}
</x-livewire-tables::table.cell>
