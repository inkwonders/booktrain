<div>
    @if($libro)
    <form wire:submit.prevent="save">
        <span>Editando {{ $libro->nombre }}:</span>
        <label for="">Precio</label>
        <input class="w-auto inline-block rounded-sm my-2 form-input" type="text" wire:model="libro.pivot.precio" placeholder="Precio">
        <label for="">
            <span class="w-auto mr-2">Obligatorio</span>
            <input class="inline-block rounded-sm my-2 form-checkbox" type="checkbox" wire:model="libro.pivot.obligatorio">
        </label>
        <button class="inline-block bg-blue-600 py-3 px-6 rounded-md" type="submit">Guardar</button>
    </form>
    @endif
</div>
