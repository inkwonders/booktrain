<section class="w-full flex flex-col items-center justify-center min-h-custom px-4 py-4">
        @hasrole("Admin|Super Admin")
        <div class="w-full flex flex-col relative h-full border rounded-md border-color-gray--BK items-center">
            <div class="relative w-full flex flex-col p-5">
                <span class="color-red--bk font-rob-bold uppercase text-2xl mt-4">Tabla referencias bbva</span>
                <div class="w-full relative justify-end flex">
                    <a href="/referencia_bbva" class="relative background-blue--bk text-white font-rob-bold w-1/6 h-11 flex justify-center items-center rounded-md cursor-pointer">+ SUBIR ARCHIVO</a>
                </div>
                <div class="relative w-full items-center justify-end my-4 flex">
                    <div class="relative w-1/2">
                        <input wire:ignore name="inicio" id="inicio" placeholder="FECHA INICIAL" class="relative w-1/3 mr-2 p-2 border color-gray--bk" type="text" wire:click="$emitTo('fecha_inicio', 'hola)">
                        <input name="fin" id="fin" placeholder="FECHA FINAL" class="relative w-1/3 p-2 border color-gray--bk" type="text" wire:model="fin">
                        <button style="left: 8px;width: 31%;" id="btn_buscar" class="relative w-1/3 h-12 w-56 background-green--bk text-white font-bold rounded hover:bg-white hover:border-green-500 hover:text-green-800 lg:text-lg md:text-base sm:text-sm border border-solid border-gray-400" wire:click="buscar">BUSCAR</button>
                    </div>
                </div>
                <table id="example2" class="stripe tabla_quejas" style="width:100%">
                    <thead>
                        <tr>
                            <th><span>Folio</span></th>
                            <th><span>Status</span></th>
                            <th><span>Nombre</span></th>
                            <th><span>Apellido</span></th>
                            <th><span>Celular</span></th>
                            <th><span>Referencia</span></th>
                            <th><span>Fecha de pago</span></th>
                            <th><span>Importe total</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center font-rob-light h-24">
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <a href="/tickets"><button class="h-12 w-56 bg-transparent hover:bg-gray-500 text-gray-400 font-semibold hover:text-white border border-gray-300 hover:border-transparent rounded">REGRESAR</button></a>
        </div>
        @endhasrole
</section>
