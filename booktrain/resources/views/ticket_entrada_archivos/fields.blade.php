<!-- Entrada Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('entrada_id', 'Entrada Id:') !!}
    {!! Form::number('entrada_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Archivo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('archivo_id', 'Archivo Id:') !!}
    {!! Form::number('archivo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Archivo Nombre Original Field -->
<div class="form-group col-sm-6">
    {!! Form::label('archivo_nombre_original', 'Archivo Nombre Original:') !!}
    {!! Form::text('archivo_nombre_original', null, ['class' => 'form-control']) !!}
</div>

<!-- Orden Field -->
<div class="form-group col-sm-6">
    {!! Form::label('orden', 'Orden:') !!}
    {!! Form::number('orden', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::text('created_at', null, ['class' => 'form-control','id'=>'created_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#created_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::text('updated_at', null, ['class' => 'form-control','id'=>'updated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#updated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush