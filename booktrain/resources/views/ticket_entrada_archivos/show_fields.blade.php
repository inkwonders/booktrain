<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $ticketEntradaArchivo->id }}</p>
</div>

<!-- Entrada Id Field -->
<div class="col-sm-12">
    {!! Form::label('entrada_id', 'Entrada Id:') !!}
    <p>{{ $ticketEntradaArchivo->entrada_id }}</p>
</div>

<!-- Archivo Id Field -->
<div class="col-sm-12">
    {!! Form::label('archivo_id', 'Archivo Id:') !!}
    <p>{{ $ticketEntradaArchivo->archivo_id }}</p>
</div>

<!-- Archivo Nombre Original Field -->
<div class="col-sm-12">
    {!! Form::label('archivo_nombre_original', 'Archivo Nombre Original:') !!}
    <p>{{ $ticketEntradaArchivo->archivo_nombre_original }}</p>
</div>

<!-- Orden Field -->
<div class="col-sm-12">
    {!! Form::label('orden', 'Orden:') !!}
    <p>{{ $ticketEntradaArchivo->orden }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $ticketEntradaArchivo->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $ticketEntradaArchivo->updated_at }}</p>
</div>

